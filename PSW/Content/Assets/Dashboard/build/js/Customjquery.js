﻿// #region CUSTOM ALIDATIONS
CheckStatus();
function CheckStatus() {
    var BdaStatus = $("#EIF_BDA_STATUS").val();
    if (BdaStatus == "APPROVED") {
        $('[name="BdaSubmit"]').hide();
    }
    var BcaStatus = $("#BCAD_STATUS").val();
    if (BcaStatus == "APPROVED") {
        $('[name="BcaSubmit"]').hide();
    }
}
$(document).on("click", '.save-img', function (event) {
    //const element = document.getElementById("card-body");
    //element.remove();
    //var panel = $("#card-body");
    //var element = panel.find("div");
    //alert(element);
    //window.print("Hello WOrld");
});
// #region Remove Newline From Client Address
$("#C_ADDRESS").change(function () {
    var Address = $("#C_ADDRESS").val();
    var NewAddress = Address.replace(/\n|\r/g, "");
    $("#C_ADDRESS").val('');
    $("#C_ADDRESS").val(NewAddress);
});
// #endregion

// Decimal Input Only
$("#searchInvoiceValueEEF").keypress(function (button) {
    return DecimalsOnly(event);
});
$("#searchQtyEEF").keypress(function (button) {
    return DecimalsOnly(event);
});
$("#searchAmountAgainstHSCodeEEF").keypress(function (button) {
    return DecimalsOnly(event);
});
$("#searchAmountAgainstHsCode").keypress(function (button) {
    return DecimalsOnly(event);
});
$("#searchQty").keypress(function (button) {
    return DecimalsOnly(event);
});
$("#searchSampleValue").keypress(function (button) {
    return DecimalsOnly(event);
});
$(".Allow-only-three-digit").keypress(function (e) {
    var arr = [];
    var kk = e.which;

    for (i = 48; i < 58; i++)
        arr.push(i);

    if (!(arr.indexOf(kk) >= 0))
        e.preventDefault();
});
function DecimalsOnly(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
// Decimal Input Only

//#region Custom Date Picker
$(function () {
    $(".CustomDatePicker").datepicker();
});
//#endregion

//#region EIF Approve
$("[name='ApproveEIF']").click(function (button) {
    if ($('#SendAsAmendment').is(':checked')) {
        $("#ApproveAsAmend")[0].click();
    } else {
        $("#ApproveAsNew")[0].click();
    }
});
//#endregion

//#region EEF Approve
$("[name='ApproveEEF']").click(function (button) {
    if ($('#SendAsAmendment').is(':checked')) {
        $("#ApproveAsAmendEEF")[0].click();
    } else {
        $("#ApproveAsNewEEF")[0].click();
    }
});
//#endregion

//#region EIF ALL PopUp
//#region EIF GD PopUp
$("[name='ViewEIFGD']").click(function (button) {
    var GD_ID = $(this).attr("value");
    $(".View_EIFGD").load("/EIFRequest/GetGDView?GD_ID=" + GD_ID);
});
//#endregion

//#region EIF BDA PopUp
$("[name='ViewEIFBDA']").click(function (button) {
    var BDA_ID = $(this).attr("value");
    $(".View_EIFBDA").load("/EIFRequest/GetBDAViewMode?BDA_ID=" + BDA_ID);
});
//#endregion

//#region EIF GD From BDA PopUp
$(document).on("click", '.GetEIFGD', function (event) {
    var GD_ID = $(this).attr("value");
    $(".View_EIFGD").load("/EIFRequest/GetGDView?GD_ID=" + GD_ID);
});
//#endregion

//#region View EIF BDA From BDA PopUp
$(document).on("click", '.EIFBDA', function (event) {
    var BDA_ID = $(this).attr("value");
    $(".View_EIFBDA").load("/EIFRequest/GetBDAViewMode?BDA_ID=" + BDA_ID);
});
//#endregion
//#endregion

//#region ALL EEF PopUp
//#region EEF GD PopUp
$("[name='ViewEEFGD']").click(function (button) {
    var GD_ID = $(this).attr("value");
    $(".View_EEFGD").load("/EEFRequest/GetEEFGDView?GD_ID=" + GD_ID);
});
//#endregion

//#region EEF BCA PopUp
$("[name='ViewEEFBCA']").click(function (button) {
    var BCA_ID = $(this).attr("value");
    $(".View_EEFBCA").load("/EEFRequest/GetBCAViewMode?BCA_ID=" + BCA_ID);
});
//#endregion

//#region EIF GD From BCA PopUp
$(document).on("click", '.GetEEFGD', function (event) {
    var GD_ID = $(this).attr("value");
    $(".View_EEFGD").load("/EEFRequest/GetEEFGDView?GD_ID=" + GD_ID);
});
//#endregion

//#region EIF BCA View From BCA PopUp
$(document).on("click", '.EEFBCA', function (event) {
    var BCA_ID = $(this).attr("value");
    $(".View_EEFBCA").load("/EEFRequest/GetBCAViewMode?BCA_ID=" + BCA_ID);
});
//#endregion
//#endregion

//#region Client Detail PopUp
$("[name='GetClientReview']").click(function (button) {
    var cid = $(this).attr("value");
    $("#ViewSelectedClient").load("/ClientInfo/LoadPreviewResult?ClientID=" + cid);
});
//#endregion

//#region EIF History Popup
$("[name='GetEIF_History']").click(function (button) {
    var EIF_REQUEST_NO = $(this).attr("value");
    $.ajax({
        url: "/MISReports/LoadEIFHistory?RequestNo=" + EIF_REQUEST_NO,
        type: "POST",
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            $("#ViewSelectedEIFHistory").load("/MISReports/LoadEIFHistory?RequestNo=" + EIF_REQUEST_NO);
        },
        error: function (error) {
            alert(error);
        }
    });
});
//#endregion

//#region EIF BDA History Popup
$("[name='GetEIFBDAHistory']").click(function (button) {
    var EIF_REQUEST_NO = $(this).attr("value");
    $.ajax({
        url: "/MISReports/LoadEIFBDAHistory?RequestNo=" + EIF_REQUEST_NO,
        type: "POST",
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            $("#ViewSelectedEIFBDAHistory").load("/MISReports/LoadEIFBDAHistory?RequestNo=" + EIF_REQUEST_NO);
        },
        error: function (error) {
            alert(error);
        }
    });
});
//#endregion

//#region EEF History Popup
$("[name='GetEEFHistory']").click(function (button) {
    var FormE_No = $(this).attr("value");
    $.ajax({
        url: "/MISReports/LoadEEFHistory?RequestNo=" + FormE_No,
        type: "POST",
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            $("#ViewSelectedEEFHistory").load("/MISReports/LoadEEFHistory?RequestNo=" + FormE_No);
        },
        error: function (error) {
            alert(error);
        }
    });
});
//#endregion

$('#EEFHsCodes').on('update', function () {
    GetSUMHsCodesEEF();
});

function GetSUMHsCodesEEF() {
    var REQ = $("#DocInfo_FDI_FORM_E_NO").val();
    var AMEND = $("#DocInfo_FDI_AMENDMENT_NO").val();
    if (REQ != 0) {
        $.ajax({
            type: "POST",
            url: "/EEFRequest/GetHsCodeSUM?E=" + REQ + "&AM=" + AMEND,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                $("#DocRequestNo_FDRI_TOTAL_VALUE_OF_SHIPMENT").val(data.InVoiceAmount.toFixed(5));
            }
        });
    }
}

// #endregion

$(function () {

    $(document).ready(function () {
        $("#ShowClientForBDA").text('');

        if ($('#EEFHsCodes').length) {
            GetSUMHsCodesEEF();
        }
        if ($('#basic_EIF_BI_BUSSINESS_NAME').length) {
            GetSelectedParametersImport();
        }
        if ($('#DocInfo_FDI_TRADER_NAME').length) {
            GetSelectedParametersExport();
        }

        if ($('#EIF_BDA_EIF_REQUEST_NO').length) {
            GetEIFInformationForBDA();
        }

        if ($('#BCAD_FORM_E_NO').length) {
            GetEEFInformationForBCA();
        }

        if ($('#searchIsSample').length) {
            $('#searchIsSample').prop("checked", false);
            IsSampleImport(this);
        }

        if ($('#DocRequestNo_FDRI_TOTAL_VALUE_OF_SHIPMENT').length) {
            EEFCalculation();
        }

    });

    // #region Get EIF on status Change
    $('#EIF_BASIC_INFO_EIF_BI_APPROVAL_STATUS').change(function () {
        $("#FromDate").val('');
        $("#ToDate").val('');
        $("#SubmitStatus").click();
    });
    // #endregion
    // #region Get EEF on status Change
    $('#EEF_FDI_STATUS').change(function () {
        $("#FromDate").val('');
        $("#ToDate").val('');
        $("#EEFStatus").click();
    });
    // #endregion

    $("[name='FetchBCADetails']").click(function (button) {
        var BankRefNo = $("#BCAD_EDS_UNIQUE_BANK_REF_NO").val();
        if(BankRefNo == "" || BankRefNo == null){
            GetBCAUniqueBankReferneceNo();
        }
        GetEEFInformationForBCA();
    });

    //$("#BCAD_FORM_E_NO").change(function () {
    //    GetEEFInformationForBCA();
    //});

    $("#BCAD_ALLOWED_DISCOUNT").change(function () {
        GetDiscountPercentEEF();
    });

    $("#BCAD_ALLOWED_DISCOUNT_PERCENT").change(function () {
        GetDiscountValueEEF();
    });

    function GetDiscountPercentEEF() {
        var ToTalValue = $("#BCAD_FORM_AMOUNT_REALIZED").val();
        var AmountInserted = $("#BCAD_ALLOWED_DISCOUNT").val();
        var DiscountPercent = parseFloat(parseFloat(parseFloat(AmountInserted) / parseFloat(ToTalValue)) * 100);
        $("#BCAD_ALLOWED_DISCOUNT_PERCENT").val(DiscountPercent.toFixed(0));
    }


    function GetDiscountValueEEF() {
        var ToTalValue = $("#BCAD_FORM_AMOUNT_REALIZED").val();
        var PercentInserted = $("#BCAD_ALLOWED_DISCOUNT_PERCENT").val();
        var DicountAmount = parseFloat(parseFloat(parseFloat(PercentInserted) / parseFloat(100)) * parseFloat(ToTalValue)).toFixed(5);
        $("#BCAD_ALLOWED_DISCOUNT").val(DicountAmount);
    }


    $("#BCAD_BILL_AMOUNT").change(function () {
        BCACalculation();
    });

    $("#BCAD_FCY_EXCHANGE_RATE").change(function () {
        BCACalculation();
    });

    $("#BCAD_BCA_FC").change(function () {
        BCACalculation();
    });

    $("[name='FetchBDADetails']").click(function (button) {
        GetEIFInformationForBDA();
    });

    //$("#EIF_BDA_EIF_REQUEST_NO").change(function () {
    //    GetEIFInformationForBDA();
    //});

    $("#EIF_BDA_SAMPLE_AMOUNT_EXCLUDE").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_AMOUNT_FCY").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_EXCHANGE_RATE_FCY").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_COMMMISSION_FCY").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_FED_AMOUNT_FCY").change(function () {
        BDAOnPageCalculations();
    });

    $("#basic_EIF_BI_BUSSINESS_NAME").change(function () {
        var EIF_BusinessName = $("#basic_EIF_BI_BUSSINESS_NAME option:selected").val();
        GetAuthorizePayModesIMPORT(EIF_BusinessName);
        GetBusinessDetailsIMPORT(EIF_BusinessName);
    });

    $("#DocInfo_FDI_TRADER_NAME").change(function () {
        var EIF_BusinessName = $("#DocInfo_FDI_TRADER_NAME option:selected").val();
        GetAuthorizePayModesEXPORT(EIF_BusinessName);
        GetBusinessDetailsEXPORT(EIF_BusinessName);
    });

    $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT").change(function () {
        GetSelectedParametersImport();
        EifPaymentModeCalculation();
    });


    $('#searchIsSample').change(function () {
        IsSampleImport(this);
    });

    $('#searchIsSampleEEF').change(function () {
        IsSampleExport(this);
    });

    function IsSampleImport(elem) {
        if (elem.checked) {
            $(this).prop("checked", true);
            $("#searchSampleValue").attr('readonly', false);
            $("#searchSampleValue").val('0.00');
        }
        else {
            $(elem).prop("checked", false);
            $("#searchSampleValue").attr('readonly', true);
            $("#searchSampleValue").val('0.00');
        }
        $('#searchIsSample').val(elem.checked);
    }

    function GetSelectedParametersImport() {
        if ($("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT option:selected").val() != 0) {
            var ModeOfImportPayment = $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT option:selected").text();
            var code = ModeOfImportPayment.substr(ModeOfImportPayment.length - 3);
            switch (code) {
                case "301":
                    $("#payparamifLC301").show();
                    $("#payparamifCont304").hide();
                    $("#payparamifcashmargin").hide();
                    $("#payparamopenaccount").hide();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").show();
                    $("#payparamcommisionCharges").show();
                    break;
                case "302":
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").hide();
                    $("#payparamopenaccount").show();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").hide();
                    break;
                case "303":
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").hide();
                    $("#payparamopenaccount").hide();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").hide();
                    $("#payparamcommisionCharges").show();
                    break;
                case "304":
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").show();
                    $("#payparamopenaccount").hide();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").show();
                    $("#payparamcommisionCharges").show();
                    break;
                case "309":
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").hide();
                    $("#payparamopenaccount").hide();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").hide();
                    $("#payparamcommisionCharges").show();
                    break;
                case "310":
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").hide();
                    $("#payparamopenaccount").hide();
                    $("#payparamifcashmargin").show();
                    $("#payparamDays").hide();
                    $("#payparamcommisionCharges").show();
                    break;
                default:
                    alert("Invalid Payment Mode Selected");
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").hide();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").hide();
                    $("#payparamcommisionCharges").show();
                    break;
            }
        }
    }

    //#region Eif Payment Mode Calculation
    function EifPaymentModeCalculation() {
        if ($("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT option:selected").val() != 0) {
            var ModeOfImportPayment = $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT option:selected").text();
            var code = ModeOfImportPayment.substr(ModeOfImportPayment.length - 3);
            $("#PayParam_EIF_PMP_SIGHT_PERCENT").val('');
            $("#PayParam_EIF_PMP_USANCE_PERCENT").val('');
            $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
            $("#PayParam_EIF_PMP_DAYS").val('');
            $("#PayParam_EIF_PMP_PERCENTAGE").val('');
            $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val('');
            $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val('');
            switch (code) {
                case "301":
                    jQuery("#PayParam_EIF_PMP_SIGHT_PERCENT").blur(function () {
                        var SIGHT_PERCENT = $("#PayParam_EIF_PMP_SIGHT_PERCENT").val();
                        $("#PayParam_EIF_PMP_USANCE_PERCENT").val('');
                        $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
                        if (SIGHT_PERCENT == '') {
                            $("#PayParam_EIF_PMP_SIGHT_PERCENT").val(0);
                        }
                        EifPaymentMode301(code, SIGHT_PERCENT, USANCE_PERCENT = 0, ADV_PAYPERCENT = 0, PMP_DAYS = 0);
                    });
                    jQuery("#PayParam_EIF_PMP_USANCE_PERCENT").blur(function () {
                        var SIGHT_PERCENT = $("#PayParam_EIF_PMP_SIGHT_PERCENT").val();
                        var USANCE_PERCENT = $("#PayParam_EIF_PMP_USANCE_PERCENT").val();
                        $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
                        if (USANCE_PERCENT == '') {
                            $("#PayParam_EIF_PMP_USANCE_PERCENT").val(0);
                        }
                        EifPaymentMode301(code, SIGHT_PERCENT, USANCE_PERCENT, ADV_PAYPERCENT = 0, PMP_DAYS = 0);
                    });
                    jQuery("#PayParam_EIF_PMP_ADV_PAYPERCENT").blur(function () {
                        var SIGHT_PERCENT = $("#PayParam_EIF_PMP_SIGHT_PERCENT").val();
                        var USANCE_PERCENT = $("#PayParam_EIF_PMP_USANCE_PERCENT").val();
                        var ADV_PAYPERCENT = $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val();
                        if (ADV_PAYPERCENT == '') {
                            $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val(0);
                        }
                        EifPaymentMode301(code, SIGHT_PERCENT, USANCE_PERCENT, ADV_PAYPERCENT);
                    });
                    break;
                case "304":
                    jQuery("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").blur(function () {
                        var DOC_PAYMENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val();
                        $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val('');
                        $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
                        if (DOC_PAYMENT == '') {
                            $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val(0);
                        }
                        EifPaymentMode304(code, DOC_PAYMENT, DOC_PERCENT = 0, ADV_PAYPERCENT = 0, PMP_DAYS = 0);
                    });
                    jQuery("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").blur(function () {
                        var DOC_PAYMENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val();
                        var DOC_PERCENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val();
                        $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
                        if (DOC_PERCENT == '') {
                            $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val(0);
                        }
                        EifPaymentMode304(code, DOC_PAYMENT, DOC_PERCENT, ADV_PAYPERCENT = 0, PMP_DAYS = 0);
                    });
                    jQuery("#PayParam_EIF_PMP_ADV_PAYPERCENT").blur(function () {
                        var DOC_PAYMENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val();
                        var DOC_PERCENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val();
                        var ADV_PAYPERCENT = $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val();
                        if (ADV_PAYPERCENT == '') {
                            $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val(0);
                        }
                        EifPaymentMode304(code, DOC_PAYMENT, DOC_PERCENT, ADV_PAYPERCENT);
                    });
                    break;
            }
        }
    }

    function EifPaymentMode301(code, SIGHT_PERCENT, USANCE_PERCENT, ADV_PAYPERCENT) {
        var TotalPercentage = 0;
        Sight_Percent = SIGHT_PERCENT;
        UsancePercent = USANCE_PERCENT;
        AdvPercent = ADV_PAYPERCENT;
        $("#PayParam_EIF_PMP_PERCENTAGE").val('');
        if (Sight_Percent != '' && UsancePercent != '' && AdvPercent != '') {
            TotalPercentage = (parseFloat(Sight_Percent) + parseFloat(UsancePercent) + parseFloat(AdvPercent));
            if (TotalPercentage == 100) {
                $("#PayParam_EIF_PMP_PERCENTAGE").val(TotalPercentage);
            }
            else {
                alert("Total Percentage should be 100 \n Calculated Percentage is " + TotalPercentage + "%");
                $("#PayParam_EIF_PMP_PERCENTAGE").val('');
            }
        }
    }

    function EifPaymentMode304(code, DOC_PAYMENT, DOC_PERCENT, ADV_PAYPERCENT) {
        var TotalPercentage = 0;
        DocPayment = DOC_PAYMENT;
        DocPercent = DOC_PERCENT;
        AdvPercent = ADV_PAYPERCENT;
        $("#PayParam_EIF_PMP_PERCENTAGE").val('');
        if (DocPayment != '' && DocPercent != '' && AdvPercent != '') {
            TotalPercentage = (parseFloat(DocPayment) + parseFloat(DocPercent) + parseFloat(AdvPercent));
            if (TotalPercentage == 100) {
                $("#PayParam_EIF_PMP_PERCENTAGE").val(TotalPercentage);
            }
            else {
                alert("Total Percentage should be 100 \n Calculated Percentage is " + TotalPercentage + "%");
                $("#PayParam_EIF_PMP_PERCENTAGE").val('');
            }
        }
    }
    //#endregion

    //#region EEF Payment Mode Calculation
    function EefPaymentModeCalculation() {
        if ($("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT option:selected").val() != 0) {
            var ModeOfExportPayment = $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT option:selected").text();
            var code = ModeOfExportPayment.substr(ModeOfExportPayment.length - 3);
            $("#PayParam_EIF_PMP_SIGHT_PERCENT").val('');
            $("#PayParam_EIF_PMP_USANCE_PERCENT").val('');
            $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
            $("#PayParam_EIF_PMP_DAYS").val('');
            $("#PayParam_EIF_PMP_PERCENTAGE").val('');
            $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val('');
            $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val('');
            switch (code) {
                case "307":
                    jQuery("#PayParam_EIF_PMP_SIGHT_PERCENT").blur(function () {
                        var SIGHT_PERCENT = $("#PayParam_EIF_PMP_SIGHT_PERCENT").val();
                        $("#PayParam_EIF_PMP_USANCE_PERCENT").val('');
                        $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
                        if (SIGHT_PERCENT == '') {
                            $("#PayParam_EIF_PMP_SIGHT_PERCENT").val(0);
                        }
                        EefPaymentMode307(code, SIGHT_PERCENT, USANCE_PERCENT = 0, ADV_PAYPERCENT = 0, PMP_DAYS = 0);
                    });
                    jQuery("#PayParam_EIF_PMP_USANCE_PERCENT").blur(function () {
                        var SIGHT_PERCENT = $("#PayParam_EIF_PMP_SIGHT_PERCENT").val();
                        var USANCE_PERCENT = $("#PayParam_EIF_PMP_USANCE_PERCENT").val();
                        $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
                        if (USANCE_PERCENT == '') {
                            $("#PayParam_EIF_PMP_USANCE_PERCENT").val(0);
                        }
                        EefPaymentMode307(code, SIGHT_PERCENT, USANCE_PERCENT, ADV_PAYPERCENT = 0, PMP_DAYS = 0);
                    });
                    jQuery("#PayParam_EIF_PMP_ADV_PAYPERCENT").blur(function () {
                        var SIGHT_PERCENT = $("#PayParam_EIF_PMP_SIGHT_PERCENT").val();
                        var USANCE_PERCENT = $("#PayParam_EIF_PMP_USANCE_PERCENT").val();
                        var ADV_PAYPERCENT = $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val();
                        if (ADV_PAYPERCENT == '') {
                            $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val(0);
                        }
                        EefPaymentMode307(code, SIGHT_PERCENT, USANCE_PERCENT, ADV_PAYPERCENT);
                    });
                    break;
                case "308":
                    jQuery("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").blur(function () {
                        var DOC_PAYMENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val();
                        $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val('');
                        $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
                        if (DOC_PAYMENT == '') {
                            $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val(0);
                        }
                        EefPaymentMode308(code, DOC_PAYMENT, DOC_PERCENT = 0, ADV_PAYPERCENT = 0, PMP_DAYS = 0);
                    });
                    jQuery("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").blur(function () {
                        var DOC_PAYMENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val();
                        var DOC_PERCENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val();
                        $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val('');
                        if (DOC_PERCENT == '') {
                            $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val(0);
                        }
                        EefPaymentMode308(code, DOC_PAYMENT, DOC_PERCENT, ADV_PAYPERCENT = 0, PMP_DAYS = 0);
                    });
                    jQuery("#PayParam_EIF_PMP_ADV_PAYPERCENT").blur(function () {
                        var DOC_PAYMENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT").val();
                        var DOC_PERCENT = $("#PayParam_EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT").val();
                        var ADV_PAYPERCENT = $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val();
                        if (ADV_PAYPERCENT == '') {
                            $("#PayParam_EIF_PMP_ADV_PAYPERCENT").val(0);
                        }
                        EefPaymentMode308(code, DOC_PAYMENT, DOC_PERCENT, ADV_PAYPERCENT);
                    });
                    break;
            }
        }
    }

    function EefPaymentMode307(code, SIGHT_PERCENT, USANCE_PERCENT, ADV_PAYPERCENT) {
        var TotalPercentage = 0;
        Sight_Percent = SIGHT_PERCENT;
        UsancePercent = USANCE_PERCENT;
        AdvPercent = ADV_PAYPERCENT;
        $("#PayParam_EIF_PMP_PERCENTAGE").val('');
        if (Sight_Percent != '' && UsancePercent != '' && AdvPercent != '') {
            TotalPercentage = (parseFloat(Sight_Percent) + parseFloat(UsancePercent) + parseFloat(AdvPercent));
            if (TotalPercentage == 100) {
                $("#PayParam_EIF_PMP_PERCENTAGE").val(TotalPercentage);
            }
            else {
                alert("Total Percentage should be 100 \n Calculated Percentage is " + TotalPercentage + "%");
                $("#PayParam_EIF_PMP_PERCENTAGE").val('');
            }
        }
    }

    function EefPaymentMode308(code, DOC_PAYMENT, DOC_PERCENT, ADV_PAYPERCENT, PMP_DAYS) {
        var TotalPercentage = 0;
        DocPayment = DOC_PAYMENT;
        DocPercent = DOC_PERCENT;
        AdvPercent = ADV_PAYPERCENT;
        $("#PayParam_EIF_PMP_PERCENTAGE").val('');
        if (DocPayment != '' && DocPercent != '' && AdvPercent != '') {
            TotalPercentage = (parseFloat(DocPayment) + parseFloat(DocPercent) + parseFloat(AdvPercent));
            if (TotalPercentage == 100) {
                $("#PayParam_EIF_PMP_PERCENTAGE").val(TotalPercentage);
            }
            else {
                alert("Total Percentage should be 100 \n Calculated Percentage is " + TotalPercentage + "%");
                $("#PayParam_EIF_PMP_PERCENTAGE").val('');
            }
        }
    }
    //#endregion

    $("#documents_EIF_DRI_TOTAL_INVOICE_VALUE").change(function () {
        var BusId = $("#basic_EIF_BI_BUSSINESS_NAME option:selected").val();
        var InvVal = $("#documents_EIF_DRI_TOTAL_INVOICE_VALUE").val();
        var ModeOfImportPayment = $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT option:selected").text();
        var Paycode = ModeOfImportPayment.substr(ModeOfImportPayment.length - 3);
        if (BusId != 0 && InvVal > 0 && Paycode != "") {
            PerformCalculationImport(BusId, InvVal, Paycode);
        }
    });

    function PerformCalculationImport(BusId, InvVal, Paycode) {
        $.ajax({
            type: "POST",
            url: "/EIFRequest/GetCharges",
            data: { BID: BusId, IVal: InvVal, PM: Paycode },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                $("#PayParam_EIF_PMP_COMMISION_CHARGES").val(data.CommCharges);
                $("#PayParam_EIF_PMP_FED_CHARGES").val(data.FEDCharges);
            }
        });
    }



    $("#DocRequestNo_FDRI_TOTAL_VALUE_OF_SHIPMENT").change(function () {
        EEFCalculation();
    });

    function EEFCalculation() {

        var BusId = $("#DocInfo_FDI_TRADER_NAME option:selected").val();
        var InvVal = $("#DocRequestNo_FDRI_TOTAL_VALUE_OF_SHIPMENT").val();
        var ModeOfExportPayment = $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT option:selected").text();
        var RequestNo = $("#DocInfo_FDI_FORM_E_NO").val();
        var Paycode = ModeOfExportPayment.substr(ModeOfExportPayment.length - 3);
        //alert(BusId + "---" + InvVal + "----" + Paycode);
        if (BusId != 0 && InvVal > 0 && Paycode != "") {
            PerformCalculationExport(BusId, InvVal, Paycode, RequestNo);
        }
    }

    function PerformCalculationExport(BusId, InvVal, Paycode, Requ) {
        $.ajax({
            type: "POST",
            url: "/EEFRequest/GetCharges",
            data: { BID: BusId, IVal: InvVal, PM: Paycode, Req: Requ },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                $("#PayParam_EIF_PMP_COMMISION_CHARGES").val(data.CommCharges);
                $("#PayParam_EIF_PMP_FED_CHARGES").val(data.FEDCharges);
            }
        });
    }


    $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT").change(function () {
        GetSelectedParametersExport();
        EefPaymentModeCalculation();
    });

    function GetSelectedParametersExport() {
        if ($("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT option:selected").val() != 0) {
            var ModeOfExportPayment = $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT option:selected").text();
            var code = ModeOfExportPayment.substr(ModeOfExportPayment.length - 3);
            switch (code) {
                case "305":
                    $("#payparamifOA305").show();
                    $("#payparamifLC307").hide();
                    $("#payparamifCont308").hide();
                    $("#payparamDays").hide();
                    $("#payparamcommisionChargesExport").show();
                    break;
                case "307":
                    $("#payparamifOA305").hide();
                    $("#payparamifLC307").show();
                    $("#payparamifCont308").hide();
                    $("#payparamDays").show();
                    $("#payparamcommisionChargesExport").show();
                    break;
                case "308":
                    $("#payparamifOA305").hide();
                    $("#payparamifLC307").hide();
                    $("#payparamifCont308").show();
                    $("#payparamcommisionChargesExport").show();
                    $("#payparamDays").show();
                    break;
                default:
                    $("#payparamifOA305").hide();
                    $("#payparamifLC307").hide();
                    $("#payparamifCont308").hide();
                    $("#payparamDays").hide();
                    $("#payparamcommisionChargesExport").show();
                    break;
            }
        }
    }

    function GetEIFInformationForBDA() {
        var REQ = $("#EIF_BDA_EIF_REQUEST_NO").val();
        var bda_id = $("#EIF_BDA_ID").val();
        //alert("BDA NO : " + bda_id + " REQUEST NO : " + REQ);
        if (REQ != 0) {
            if (bda_id == null || bda_id == 0 || bda_id == "") {
                $.ajax({
                    type: "POST",
                    url: "/EIFRequest/GetEIFInformationForBDA?E=" + REQ,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (data) {
                        $("#EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY").val(data.TOTALAMOUNT.toFixed(5));
                        $("#EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY").val(data.AUTCCY).change();
                        $("#EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY").val(data.AUTCCY).change();
                        $("#EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY").val(data.AUTCCY).change();
                        $("#EIF_BDA_CURRENCY_FCY").val(data.AUTCCY).change();
                        $("#EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY").val(data.EXCHAGERATE);
                        $("#EIF_BDA_EXCHANGE_RATE_FCY").val(data.EXCHAGERATE);
                        $("#EIF_BDA_SAMPLE_AMOUNT_EXCLUDE").val(data.SampleAmount.toFixed(5));
                        $("#EIF_BDA_Balance_Amount_FCY").val(data.CurrentBalance.toFixed(5));
                        $("#ShowClientForBDA").text("Client : " + data.ClientName);
                        BDAOnPageCalculations();
                        $.ajax({
                            type: "POST",
                            url: "/EIFRequest/GetEIFGDNumber?REQ=" + REQ,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $('#loading').show();
                            },
                            complete: function () {
                                $('#loading').hide();
                            },
                            success: function (result) {
                                $("#EIF_BDA_GD_NO").html("");
                                $.each($.parseJSON(result), function (i, Data) {
                                    if (Data.EIGDGI_GD_NO != '') {
                                        $("#EIF_BDA_GD_NO").append($('<option></option>').val(Data.EIGDGI_GD_NO).html(Data.EIGDGI_GD_NO));
                                    }
                                });
                                $('#LoadGDAgainstBDA').load('/EIFRequest/GetGDAgainstBDA?REQ=' + REQ);
                            }
                        });
                    }
                });
                $('#LoadBDAsInfoByEIF').load('/EIFRequest/EIFBDAList?E=' + REQ);
            }
            else {
                $('#LoadBDAsInfoByEIF').load('/EIFRequest/EIFBDAList?E=' + REQ);
            }
        }
    }


    function GetEEFInformationForBCA() {
        var REQ = $("#BCAD_FORM_E_NO").val();
        var bca_id = $("#BCAD_ID").val();
        if (REQ != 0) {
            if (bca_id == null || bca_id == 0 || bca_id == "") {
                $.ajax({
                    type: "POST",
                    url: "/EEFRequest/GetEEFInformationForBCA?E=" + REQ,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (data) {
                        $("#BCAD_CURRENCY").val(data.AUTCCY).change();
                        $("#BCAD_BALANCE").val(data.CurrentBalance.toFixed(5)).change();
                        $("#TotalBCAmount").val(data.TotalHSCodeValue.toFixed(5)).change();
                        BCACalculation();
                        $.ajax({
                            type: "POST",
                            url: "/EEFRequest/GetEEFGDNumber?REQ=" + REQ,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $('#loading').show();
                            },
                            complete: function () {
                                $('#loading').hide();
                            },
                            success: function (data) {
                                $("#BCAD_GD_NO").html("");
                                $.each($.parseJSON(data), function (i, Data) {
                                    if (Data.EIGDGI_GD_NO != '') {
                                        $("#BCAD_GD_NO").append($('<option></option>').val(Data.EFGDGI_GD_NO).html(Data.EFGDGI_GD_NO));
                                    }
                                });
                                $('#LoadGDAgainstBCA').load('/EEFRequest/GetAllGDAgainstBCA?REQ=' + REQ);
                            }
                        });
                    }
                });
                $('#LoadBCAsInfoByEEF').load('/EEFRequest/BCAList?E=' + REQ);
            }
            else {
                $('#LoadBCAsInfoByEEF').load('/EEFRequest/BCAList?E=' + REQ);
            }
        }
    }

    function GetBCAUniqueBankReferneceNo() {
        var REQ = $("#BCAD_FORM_E_NO").val();
           $.ajax({
                type: "POST",
                url: "/EEFRequest/GetUniuqBankRefBCA?E=" + REQ,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    if(data.UniqueBankRefNo != ""){
                        $("#BCAD_EDS_UNIQUE_BANK_REF_NO").val(data).change();
                    }
                    else{
                        $("#BCAD_EDS_UNIQUE_BANK_REF_NO").val("").change();
                    }
                }
            });
    }

    function BCACalculation() {
        var REQ = $("#BCAD_FORM_E_NO").val();
        var BCABillAmount = $("#BCAD_BILL_AMOUNT").val();
        if (BCABillAmount.length) {
            $.ajax({
                type: "POST",
                url: "/EEFRequest/GetEEFInformationForBCA?E=" + REQ,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $("#BCAD_CURRENCY").val(data.AUTCCY).change();
                    var previousBalance = data.CurrentBalance.toFixed(5);
                    var BalanceAmount = (parseFloat(previousBalance) - parseFloat(BCABillAmount));
                    if (BalanceAmount >= 0) {
                        $("#BCAD_BALANCE").val(BalanceAmount.toFixed(5)).change();
                    }
                    else {
                        alert("Invalid BCA Bill Amount, Sould be Less than BCA Balance.");
                        $("#BCAD_BILL_AMOUNT").val("");
                        $("#BCAD_BALANCE").val("");
                    }
                }
            });
        }
        var REQ = $("#BCAD_FORM_E_NO").val();
        var ExchangeRate = $("#BCAD_FCY_EXCHANGE_RATE").val();
        var BCAFCYAmount = $("#BCAD_BCA_FC").val();
        var BCAPKRAmount = parseFloat(parseFloat(ExchangeRate) * parseFloat(BCAFCYAmount)).toFixed(5);
        $("#BCAD_BCA_PKR").val(BCAPKRAmount);
    }


    function BDAOnPageCalculations() {
        var totalBDA = $("#EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY").val();
        var SampleBDA = $("#EIF_BDA_SAMPLE_AMOUNT_EXCLUDE").val();
        var ExchageRate = $("#EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY").val();
        var NetBDAAmount = (totalBDA - (parseFloat(SampleBDA)));
        var NetBDAAmountPKR = (NetBDAAmount * ExchageRate);

        $("#EIF_BDA_NET_BDA_AMOUNT_EIF_FCY").val(NetBDAAmount.toFixed(5));
        $("#EIF_BDA_NET_BDA_AMOUNT_PKR").val(NetBDAAmountPKR.toFixed(4));
        $.ajax({
            type: "POST",
            url: "/EIFRequest/AmountInWords?Amount=" + NetBDAAmountPKR,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                $("#EIF_BDA_AMOUNT_IN_WORDS").val(data.AmountInWordS);
            }
        });

        var BDAAmountFCY = $("#EIF_BDA_AMOUNT_FCY").val();

        if (BDAAmountFCY.length) {
            var REQ = $("#EIF_BDA_EIF_REQUEST_NO").val();
            $.ajax({
                type: "POST",
                url: "/EIFRequest/GetEIFInformationForBDA?E=" + REQ,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var previousBalance = data.CurrentBalance.toFixed(5);
                    var BalanceAmount = (parseFloat(previousBalance) - parseFloat(BDAAmountFCY));
                    if (BalanceAmount >= 0) {
                        $("#EIF_BDA_Balance_Amount_FCY").val(BalanceAmount.toFixed(5));
                    }
                    else {
                        alert("Invalid BDA Amount FCY, Sould be Less than BDA Balance.");
                        $("#EIF_BDA_AMOUNT_FCY").val("");
                        $("#EIF_BDA_Balance_Amount_FCY").val("");
                    }
                }
            });
        }

        var CommisionAmountFCY = $("#EIF_BDA_COMMMISSION_FCY").val();
        if (CommisionAmountFCY != 0 && CommisionAmountFCY != "") {
            var ExchageRateFCY = $("#EIF_BDA_EXCHANGE_RATE_FCY").val();
            var CommisionAmountPKR = (CommisionAmountFCY * ExchageRateFCY);
            $("#EIF_BDA_COMMISSION_PKR").val(CommisionAmountPKR.toFixed(5));
        }
        else {
            $("#EIF_BDA_COMMISSION_PKR").val(0);
        }

        var FEDAmountFCY = $("#EIF_BDA_FED_AMOUNT_FCY").val();

        if (FEDAmountFCY != 0 && FEDAmountFCY != "") {
            var ExchageRateFCY = $("#EIF_BDA_EXCHANGE_RATE_FCY").val();
            var FEDAmountPKR = (FEDAmountFCY * ExchageRateFCY);
            $("#EIF_BDA_FED_AMOUNT_PKR").val(FEDAmountPKR.toFixed(5));
        }
        else {
            $("#EIF_BDA_FED_AMOUNT_PKR").val(0);
        }


        if (BDAAmountFCY != 0 && BDAAmountFCY != "") {
            var ExchageRateFCY = $("#EIF_BDA_EXCHANGE_RATE_FCY").val();
            BDAAmountPKR = (BDAAmountFCY * ExchageRateFCY);
            $("#EIF_BDA_AMOUNT_PKR").val(BDAAmountPKR.toFixed(5));
        }
        else {
            $("#EIF_BDA_AMOUNT_PKR").val(0);
        }




    }



    function GetAuthorizePayModesIMPORT(BusId) {
        if (BusId != 0) {
            $.ajax({
                type: "POST",
                url: "/EIFRequest/ListOfAuthPayModes?clientId=" + BusId,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    var rowcount = 0;
                    $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT").empty();
                    $.each(data, function (idx, elem) {
                        rowcount += 1;
                        if (rowcount == 1) {
                            $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT").append("<option value='0'> --Select Mode of Import Payment-- </option>");
                        }
                        {
                            $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT").append("<option value=" + elem.Value + ">" + elem.Text + "</option>");
                        }
                    });
                }
            });
        }
    }
    function GetBusinessDetailsIMPORT(BusId) {
        if (BusId != 0) {
            $.ajax({
                type: "POST",
                url: "/EIFRequest/GetBusinessDetails?clientId=" + BusId,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $.each(data, function (idx, elem) {
                        $("#basic_EIF_BI_NTN").val(elem.NTN);
                        $("#basic_EIF_BI_BUSSINESS_ADDRESS").val(elem.ADDRESS);
                        $("#basic_EIF_BI_STRN").val(elem.STRN);
                        $("#bank_EIF_BA_IBAN").val(elem.IBAN);
                    });
                }
            });
        }
    }

    function GetAuthorizePayModesEXPORT(BusId) {
        if (BusId != 0) {
            $.ajax({
                type: "POST",
                url: "/EEFRequest/ListOfAuthPayModes?clientId=" + BusId,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    var rowcount = 0;
                    $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT").empty();
                    $.each(data, function (idx, elem) {
                        rowcount += 1;
                        if (rowcount == 1) {
                            $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT").append("<option value='0'> --Select Mode of Export Payment-- </option>");
                        }
                        {
                            $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT").append("<option value=" + elem.Value + ">" + elem.Text + "</option>");
                        }
                    });
                }
            });
        }
    }
    function GetBusinessDetailsEXPORT(BusId) {
        if (BusId != 0) {
            $.ajax({
                type: "POST",
                url: "/EEFRequest/GetBusinessDetails?clientId=" + BusId,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $.each(data, function (idx, elem) {
                        $("#DocInfo_FDI_NTN").val(elem.NTN);
                        $("#DocInfo_FDI_TRADER_ADDRESS").val(elem.ADDRESS);
                        $("#DocRequestNo_FDRI_CONSIGNEE_IBAN").val(elem.IBAN);
                        $("#DocRequestNo_FDRI_CONSIG_NAME").val('');
                        $("#DocRequestNo_FDRI_CONSID_ADDRESS").val('');
                    });
                }
            });
        }
    }

    $('#documents_EIF_DRI_PORT_OF_SHIPMENT').change(function () {
        var InputPort = $("#documents_EIF_DRI_PORT_OF_SHIPMENT").val();
        $.ajax({
            type: "POST",
            url: "/EIFRequest/VerifyPortOfShipment",
            data: { EnteredPort: InputPort },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                if (result == "Invalid Port Name") {
                    alert(result);
                    $('#loading').hide();
                    $("#documents_EIF_DRI_PORT_OF_SHIPMENT").val('');
                    $("[name='PortDesc']").val('');
                    $("#documents_EIF_DRI_PORT_OF_SHIPMENT").focus();
                }
                else {
                    $("#documents_EIF_DRI_PORT_OF_SHIPMENT").val(result.Code);
                    $("[name='PortDesc']").val(result.Name);
                }
            }
        });
    });

    $('#DocRequestNo_FDRI_PORT_OF_DISCHARGE').change(function () {
        var InputPort = $("#DocRequestNo_FDRI_PORT_OF_DISCHARGE").val();
        $.ajax({
            type: "POST",
            url: "/EEFRequest/VerifyPortOfDischarge",
            data: { enteredPort: InputPort },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                if (result == "Invalid Port Name") {
                    alert(result);
                    $('#loading').hide();
                    $("#DocRequestNo_FDRI_PORT_OF_DISCHARGE").val('');
                    $("[name='PortDischarge']").val(result.Name);
                    $("#DocRequestNo_FDRI_PORT_OF_DISCHARGE").focus();
                }
                else {
                    $("#DocRequestNo_FDRI_PORT_OF_DISCHARGE").val(result.Code);
                    $("[name='PortDischarge']").val(result.Name);
                }
            }
        });
    });

    $('#searchHsCode').change(function () {
        var InputHSCode = $("#searchHsCode").val();
        $.ajax({
            type: "POST",
            url: "/EIFRequest/GetEifHsCodeDescription",
            data: { HsCode: InputHSCode },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                if (result == "Invalid Hs Code") {
                    alert(result);
                    $("#searchHsCode").val('');
                    $("#searchHsCode").focus();
                    $("#searchHsCodeDescription").val('');
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "/EIFRequest/CheckBannedHsCode",
                        data: { HsCode: InputHSCode },
                        beforeSend: function () {
                            $('#loading').show();
                        },
                        complete: function () {
                            $('#loading').hide();
                        },
                        success: function (data) {
                            if (data == "Hs Code exist in the banned item.") {
                                var check = confirm("Are you sure you want to add banned Hs Code?");
                                if (check == true) {
                                    $("#searchHsCodeDescription").val(result);
                                }
                                else
                                {
                                    $("#searchHsCode").val('');
                                    $("#searchHsCode").focus();
                                    $("#searchHsCodeDescription").val('');
                                }
                            }
                            else
                            {
                                $("#searchHsCodeDescription").val(result);
                            }
                        }
                    });
                }
            }
        });
    });

    $('#searchHsCodeEEF').change(function () {
        var InputHSCode = $("#searchHsCodeEEF").val();
        $.ajax({
            type: "POST",
            url: "/EEFRequest/GetEefHsCodeDescription",
            data: { HsCode: InputHSCode },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                if (result == "Invalid Hs Code") {
                    alert(result);
                    $("#searchHsCodeEEF").val('');
                    $("#searchHsCodeEEF").focus();
                    $("#searchDescriptionEEF").val('');
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "/EEFRequest/CheckBannedHsCode",
                        data: { HsCode: InputHSCode },
                        beforeSend: function () {
                            $('#loading').show();
                        },
                        complete: function () {
                            $('#loading').hide();
                        },
                        success: function (data) {
                            if (data == "Hs Code exist in the banned item.") {
                                var check = confirm("Are you sure you want to add banned Hs Code?");
                                if (check == true) {
                                    $("#searchDescriptionEEF").val(result);
                                }
                                else {
                                    $("#searchHsCodeEEF").val('');
                                    $("#searchHsCodeEEF").focus();
                                    $("#searchDescriptionEEF").val('');
                                }
                            }
                            else {
                                $("#searchDescriptionEEF").val(result);
                            }
                        }
                    });
                }
            }
        });
    });

    function AddHsCodeToEIF() {

        var InputHSCode = $("#searchHsCode").val();
        var InputHSCodeDes = $("#searchHsCodeDescription").val();
        var EIF_REQ = $("#basic_EIF_BI_EIF_REQUEST_NO").val();
        var InputQTY = $("#searchQty").val();
        var InputUOM = $("#searchUOM").val();
        var InputOrigin = $("#searchOrigin").val();
        var HC_AMENDMENT_NO = $("#basic_EIF_BI_AMENDMENT_NO").val();
        var IsSample = $('#searchIsSample').val();
        var Samplevalue = $("#searchSampleValue").val();
        var AmountAgainstEifHsCode = $("#searchAmountAgainstHsCode").val();
        if (InputQTY != "" && InputUOM != "" && InputOrigin != "" && Samplevalue != "" && AmountAgainstEifHsCode != "") {
            var hscode = {};
            hscode.EIF_HC_ID = 0;
            hscode.EIF_HC_EIF_REQUEST_NO = EIF_REQ;
            hscode.EIF_HC_CODE = InputHSCode;
            hscode.EIF_HC_DESCRIPTION = InputHSCodeDes;
            hscode.EIF_HC_QUANTITY = InputQTY;
            hscode.EIF_HC_UOM = InputUOM;
            hscode.EIF_HC_ORGIN = InputOrigin;
            hscode.EIF_HC_AMENDMENT_NO = HC_AMENDMENT_NO;
            hscode.EIF_HC_AMOUNT_AGAINST_HS_CODE = AmountAgainstEifHsCode;
            if (IsSample == "true") {
                hscode.EIF_HC_IS_SAMPLE = "Y";
                hscode.EIF_HC_SAMPLE_VALUE = Samplevalue;
            }
            else {
                hscode.EIF_HC_IS_SAMPLE = "N";
                hscode.EIF_HC_SAMPLE_VALUE = 0;
            }
            $.ajax({
                type: "POST",
                url: "/EIFRequest/AddHsCodeToEIF",
                data: JSON.stringify(hscode),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (r) {
                    if (r != "") {
                        if (r.exp != "") {
                            alert(r.exp);
                        }
                        else
                            alert(r.ImportPolicy);
                        //alert("Added Successfully.");
                        $('#HsGoodsDetailsAJ').load('/EIFRequest/EIFHSCodeList?H=' + EIF_REQ + '&M=' + HC_AMENDMENT_NO);
                    }
                }
            });
        }
        else {
            if (InputQTY == "") {
                alert("Quantity is Required !!");
            }
            if (InputUOM == "") {
                alert("Please Select UOM");
            }
            if (InputOrigin == "") {
                alert("Please Select Origin Of Goods");
            }
            if (Samplevalue == "") {
                alert("Sample Value is Required !!");
            }
            if (AmountAgainstEifHsCode == "") {
                alert("Item Invoice Value is Required !!");
            }
        }
    }

    function AddHsCodeToEEF() {

        var InputHSCode = $("#searchHsCodeEEF").val();
        var InputDescription = $("#searchDescriptionEEF").val();
        var EIF_REQ = $("#DocInfo_FDI_FORM_E_NO").val();
        var InputQTY = $("#searchQtyEEF").val();
        var InputUOM = $("#searchUOMEEF").val();
        var InputOrigin = $("#searchOriginEEF").val();
        var HC_AMENDMENT_NO = $("#DocInfo_FDI_AMENDMENT_NO").val();
        var InvoiceValue = $("#searchInvoiceValueEEF").val();
        var AmountAgainstHSCodeEEF = $("#searchAmountAgainstHSCodeEEF").val();
        if (InputQTY != "" && InputUOM != "" && InputOrigin != "" && InvoiceValue != "" && AmountAgainstHSCodeEEF != "") {
            //alert(HC_AMENDMENT_NO);
            var hscode = {};
            hscode.EEF_HC_ID = 0;
            hscode.EEF_HC_EEF_REQUEST_NO = EIF_REQ;
            hscode.EEF_HC_CODE = InputHSCode;
            hscode.EEF_HC_DESCRIPTION = InputDescription;
            hscode.EEF_HC_QUANTITY = InputQTY;
            hscode.EEF_HC_UOM = InputUOM;
            hscode.EEF_HC_ORGIN = InputOrigin;
            hscode.EEF_HC_INVOICE_VALUE = InvoiceValue;
            hscode.EEF_HC_AMENDMENT_NO = HC_AMENDMENT_NO;
            hscode.EEF_HC_AMOUNT_AGAINST_HS_CODE = AmountAgainstHSCodeEEF;
            $.ajax({
                type: "POST",
                url: "/EEFRequest/AddHsCodeToEEF",
                data: JSON.stringify(hscode),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (r) {
                    if (r != "") {
                        if (r.exp != "") {
                            alert(r.exp);
                        }
                        //alert(r);
                        else
                            alert(r.ImportPolicy);
                        //alert("Added Successfully.");
                        $('#HsGoodsDetailsEEF').load('/EEFRequest/EEFHSCodeList?H=' + EIF_REQ + '&M=' + HC_AMENDMENT_NO);
                        GetSUMHsCodesEEF();

                    }
                }
            });
        }
        else {
            if (InputQTY == "") {
                alert("Quantity is Required !!");
            }
            if (InputUOM == "") {
                alert("Please Select UOM");
            }
            if (InputOrigin == "") {
                alert("Please Select Origin Of Goods");
            }
            if (InvoiceValue == "") {
                alert("Invoice Value is Required !!");
            }
            if (AmountAgainstHSCodeEEF == "") {
                alert("Item Invoice Value is Required !!");
            }
        }
    }

    function GetPreviousEIFHSCode() {
        var EIF_REQ = $("#basic_EIF_BI_EIF_REQUEST_NO").val();
        var HC_AMENDMENT_NO = $("#basic_EIF_BI_AMENDMENT_NO").val();
        //alert("Amend No : " + HC_AMENDMENT_NO + " Prev AM : " + GetPrevAmendNo);
        $.ajax({
            type: "POST",
            url: "/EIFRequest/GetPreviousEIFHsCode?E=" + EIF_REQ + "&AM=" + HC_AMENDMENT_NO,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (r) {
                if (r != "") {
                    alert(r);
                }
                else {
                    alert("Previous HS Code fetched sucesssfully.");
                    $('#HsGoodsDetailsAJ').load('/EIFRequest/EIFHSCodeList?H=' + EIF_REQ + '&M=' + HC_AMENDMENT_NO);
                }
            }
        });
        //$('#HsGoodsDetailsAJ').load('/EIFRequest/EIFHSCodeList?H=' + EIF_REQ + '&M=' + GetPrevAmendNo);
    }

    function GetPreviousEEFHSCode() {
        var EEF_REQ = $("#DocInfo_FDI_FORM_E_NO").val();
        var HC_AMENDMENT_NO = $("#DocInfo_FDI_AMENDMENT_NO").val();
        //alert("FormE No : " + EEF_REQ + " Amendment No : " + HC_AMENDMENT_NO);
        $.ajax({
            type: "POST",
            url: "/EEFRequest/GetPreviousEEFHsCode?E=" + EEF_REQ + "&AM=" + HC_AMENDMENT_NO,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (r) {
                if (r != "") {
                    alert(r);
                }
                else {
                    alert("Previous HS Code fetched sucesssfully.");
                    $('#HsGoodsDetailsEEF').load('/EEFRequest/EEFHSCodeList?H=' + EEF_REQ + '&M=' + HC_AMENDMENT_NO);
                    GetSUMHsCodesEEF();
                }
            }
        });
        //$('#HsGoodsDetailsAJ').load('/EIFRequest/EIFHSCodeList?H=' + EIF_REQ + '&M=' + GetPrevAmendNo);
    }

    $("body").on("click", "#AddHsCode", function () {
        var EIF_REQ = $("#basic_EIF_BI_EIF_REQUEST_NO").val();
        if (EIF_REQ == "") {
            $.ajax({
                type: "POST",
                url: "/EIFRequest/GenerateNewEIFReqNo",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (Data) {
                    $('#basic_EIF_BI_EIF_REQUEST_NO').val(Data.EIF_NO);
                    $('#basic_EIF_BI_INTERNAL_REF').val(Data.IR_NO);
                    AddHsCodeToEIF();
                }
            });
        }
        else {
            AddHsCodeToEIF();
        }
    });

    if ($("#GetPreviousEifHsCode").length > 0) {
        GetPreviousEIFHSCode();
    }

    if ($("#GetPreviousEefHsCode").length > 0) {
        GetPreviousEEFHSCode();
    }

    $("body").on("click", "#AddHsCodeEEF", function () {
        var EEF_REQ = $("#DocInfo_FDI_FORM_E_NO").val();
        if (EEF_REQ == "") {
            $.ajax({
                type: "POST",
                url: "/EEFRequest/GenerateNewEEFReqNo",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (Data) {
                    $('#DocInfo_FDI_FORM_E_NO').val(Data.EEF_NO);
                    $('#DocInfo_FDI_INTERNAL_REF').val(Data.IR_NO);
                    AddHsCodeToEEF();
                }
            });
        }
        else {
            AddHsCodeToEEF();
        }
    });
});
//#region To remove Decimal from Textbox
jQuery("#BCAD_RUNNING_SERIAL_DETAIL").blur(function () {
    if ($("#BCAD_RUNNING_SERIAL_DETAIL").val() != 0) {
        RestrictToInt("BCAD_RUNNING_SERIAL_DETAIL");
    }
    else {
        $("#BCAD_RUNNING_SERIAL_DETAIL").val(0);
    }
});


function RestrictToInt(textboxId) {
    var text1 = $("#" + textboxId).val();
    var newstring = "";
    for (var x = 0; x < text1.length; x++) {
        var c = text1.charAt(x);
        if ($.isNumeric(c)) {
            newstring += c;
        }
        else {
            if (c == '.') {
                newstring += c;
            }
        }
    }
    var convertedtonumber = parseInt(newstring);
    $("#" + textboxId).val(convertedtonumber);
}
//#endregion
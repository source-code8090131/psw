﻿using PSW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PSW.Controllers
{
    public class AuthenticationController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: Authentication
        public string GetIP()
        {
            string Str = "";
            Str = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Str);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
        public ActionResult Index()
        {
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return View();
        }
        [HttpPost]
        public ActionResult Index(CustomLogin db_table)
        {
            try
            {
                Session["IP"] = GetIP();
                PSW_LOGIN_MASTER login = context.PSW_LOGIN_MASTER.Where(m => m.LOG_USER_ID == db_table.LOG_USER_ID).FirstOrDefault();
                if (login != null)
                {
                    PSW_LOGIN loginview = context.PSW_LOGIN.Where(m => m.LOG_ID == login.LOG_ID).FirstOrDefault();

                    PSW_SYSTEM_SETUP SystemSetup = context.PSW_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                    if (SystemSetup != null)
                    {
                        Session["IP"] = GetIP();
                        if (loginview != null && login.ASSIGNED_ROLES != null)
                        {
                            if (loginview.LOG_USER_STATUS == "Active")
                            {
                                if (!loginview.LOG_USER_ID.ToLower().Contains("admin"))
                                {
                                    if (loginview.LOG_USER_ID == db_table.LOG_USER_ID && loginview.LOG_ISAUTH == true)
                                    {
                                        string adPath = "";
                                        string adurl = "";
                                        if (db_table.DOMAIN_NAME == "APAC")
                                        {
                                            adPath = "LDAP://apac.nsroot.net"; //Fully-qualified Domain Name
                                            adurl = "apac.nsroot.net";
                                        }
                                        else if (db_table.DOMAIN_NAME == "EUR")
                                        {
                                            adPath = "LDAP://eur.nsroot.net"; //Fully-qualified Domain Name
                                            adurl = "eur.nsroot.net";
                                        }
                                        else if (db_table.DOMAIN_NAME == "NAM")
                                        {
                                            adPath = "LDAP://nam.nsroot.net"; //Fully-qualified Domain Name
                                            adurl = "nam.nsroot.net";
                                        }
                                        else
                                        {
                                            ViewBag.message = "Select the domain to continue.";
                                            return View();
                                        }
                                        //string adPath = "LDAP://eur.nsroot.net"; //Fully-qualified Domain Name
                                        LdapAuthentication adAuth = new LdapAuthentication(adPath);
                                        bool CheckAuth = adAuth.IsAuthenticated(adurl, db_table.LOG_USER_ID, db_table.LOG_PASSWORD);
                                        if (CheckAuth == true)
                                        {
                                            bool isCookiePersistent = User.Identity.IsAuthenticated;
                                            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                                                (1, db_table.LOG_USER_ID, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, null);
                                            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                                            HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                                            if (true == isCookiePersistent)
                                                authCookie.Expires = authTicket.Expiration;
                                            Response.Cookies.Add(authCookie);
                                            PSW_LOGIN_LOG Log = new PSW_LOGIN_LOG();
                                            Log.LL_ID = Convert.ToInt32(context.PSW_LOGIN_LOG.Max(m => (decimal?)m.LL_ID)) + 1;
                                            Log.LL_USER_ID = loginview.LOG_USER_ID;
                                            Log.LL_LAST_LOGIN = DateTime.Now;
                                            context.PSW_LOGIN_LOG.Add(Log);
                                            context.SaveChanges();
                                            Session["USER_ID"] = loginview.LOG_USER_ID;
                                            Session["USER_NAME"] = loginview.LOG_USER_NAME;
                                            //Session["USER_TYPE"] = loginview.R_NAME;
                                            return RedirectToAction("Index", "Home");

                                        }
                                        else
                                        {
                                            ViewBag.message = "Authentication Failed from the Domain.";
                                        }
                                    }
                                    else
                                    {
                                        if (loginview.LOG_ISAUTH != true)
                                            ViewBag.message = "Unauthorize User : " + db_table.LOG_USER_ID + " is unauthorize";
                                        else if (loginview.LOG_USER_ID != db_table.LOG_USER_ID)
                                            ViewBag.message = "Invalid UserId : " + db_table.LOG_USER_ID + " is case sensetive";
                                        return View(db_table);
                                    }
                                }
                                else
                                {
                                    if (loginview.LOG_USER_ID == db_table.LOG_USER_ID && SystemSetup.SS_HASH == DAL.Encrypt(db_table.LOG_PASSWORD) && loginview.LOG_ISAUTH == true)
                                    {
                                        bool isCookiePersistent = User.Identity.IsAuthenticated;
                                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                                               (1, db_table.LOG_USER_ID, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, null);
                                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                                        HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                                        if (true == isCookiePersistent)
                                            authCookie.Expires = authTicket.Expiration;
                                        Response.Cookies.Add(authCookie);
                                        PSW_LOGIN_LOG Log = new PSW_LOGIN_LOG();
                                        Log.LL_ID = Convert.ToInt32(context.PSW_LOGIN_LOG.Max(m => (decimal?)m.LL_ID)) + 1;
                                        Log.LL_USER_ID = loginview.LOG_USER_ID;
                                        Log.LL_LAST_LOGIN = DateTime.Now;
                                        context.PSW_LOGIN_LOG.Add(Log);
                                        context.SaveChanges();
                                        Session["USER_ID"] = loginview.LOG_USER_ID;
                                        Session["USER_NAME"] = loginview.LOG_USER_NAME;
                                        //Session["USER_TYPE"] = loginview.R_NAME;
                                        return RedirectToAction("Index", "Home");
                                    }
                                    else
                                    {
                                        if (SystemSetup.SS_HASH != DAL.Encrypt(db_table.LOG_PASSWORD))
                                            ViewBag.message = "Invalid Password : Invalid Password Provided for the User " + db_table.LOG_USER_ID;
                                        else if (loginview.LOG_ISAUTH != true)
                                            ViewBag.message = "Unauthorize User : " + db_table.LOG_USER_ID + " is unauthorize";
                                        else if (loginview.LOG_USER_ID != db_table.LOG_USER_ID)
                                            ViewBag.message = "Invalid UserId : " + db_table.LOG_USER_ID + " is case sensetive";
                                        return View(db_table);
                                    }
                                }
                            }
                            else
                            {
                                ViewBag.message = "User Id " + loginview.LOG_USER_ID + " is in de-active status.";
                                return View(db_table);
                            }
                        }
                        else
                        {
                            ViewBag.message = "Login Was Successfull but No Rights Assign to the User";
                            return View(db_table);
                        }
                    }
                    else
                        ViewBag.message = "No data found in the system setup";
                }
                else
                {
                    ViewBag.message = "Invalid Email : System doesnt recognize your email";
                    return View(db_table);
                }
            }
            catch (Exception ex)
            {
                ViewBag.message = DAL.LogException("Authentication", "Index", db_table.LOG_USER_ID, ex);
            }
            return View();
        }
        public ActionResult Logout()
        {
            if (Session["USER_ID"] != null)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
            }
            FormsAuthentication.SignOut();
            Session.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ExpiresAbsolute = DateTime.UtcNow.AddDays(-1d);
            Response.Expires = -1500;
            Response.CacheControl = "no-Cache";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return RedirectToAction("Index", "Authentication");
        }
    }
}
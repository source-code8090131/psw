﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Controllers
{
    public class BCATransferController : Controller
    {
        // GET: BCATransfer
        public ActionResult Index()
        {
            if (Session["USER_ID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
    }
}
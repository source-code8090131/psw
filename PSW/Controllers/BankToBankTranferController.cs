﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PSW.Models;

namespace PSW.Controllers
{
    public class BankToBankTranferController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: BankToBankTranfer
        #region Update Status Of Bank Request
        public ActionResult UpdateStatus(int B = 0)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    if (B != 0)
                    {
                        PSW_BANK_TO_BANK_TRANSFER edit = context.PSW_BANK_TO_BANK_TRANSFER.Where(m => m.BTBT_ID == B).FirstOrDefault();
                        if (edit != null)
                        {
                            return View(edit);
                        }
                        else
                        {
                            message = "Exception Occur while fetching your record on User Id = " + B;
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    DAL.LogException("BankToBankTransfer", "UpdateStatus", Session["USER_ID"].ToString(), ex);
                    message = ex.Message;
                }
                finally
                {
                    List<SelectListItem> EIF_Info = new SelectList(context.PSW_EIF_BASIC_INFO.ToList(), "EIF_BI_ID", "EIF_BI_EIF_REQUEST_NO", 0).ToList();
                    EIF_Info.Insert(0, (new SelectListItem { Text = "--Select Request Number--", Value = "0" }));
                    ViewBag.BTBT_REQUEST_NO = EIF_Info;
                    List<SelectListItem> Banks = new SelectList(context.PSW_LIST_OF_BANKS.ToList(), "LB_BANK_ID", "LB_BANK_NAME", 0).ToList();
                    Banks.Insert(0, (new SelectListItem { Text = "--Select Bank--", Value = "0" }));
                    ViewBag.BTBT_BANK = Banks;
                    List<SelectListItem> Cities = new SelectList(context.PSW_LIST_OF_CITIES.ToList(), "LOCT_ID", "LOCT_NAME", 0).ToList();
                    Cities.Insert(0, (new SelectListItem { Text = "--Select City--", Value = "0" }));
                    ViewBag.BTBT_CITY = Cities;
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UpdateStatus(PSW_BANK_TO_BANK_TRANSFER db_table)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    PSW_BANK_TO_BANK_TRANSFER UpdateEntity = new PSW_BANK_TO_BANK_TRANSFER();

                    PSW_BANK_TO_BANK_TRANSFER CheckIfExist = new PSW_BANK_TO_BANK_TRANSFER();
                    if (db_table.BTBT_ID == 0)
                    {
                        UpdateEntity.BTBT_ID = Convert.ToInt32(context.PSW_BANK_TO_BANK_TRANSFER.Max(m => (decimal?)m.BTBT_ID)) + 1;
                        UpdateEntity.BTBT_ENTRY_DATETIME = DateTime.Now;
                        CheckIfExist = null;
                    }
                    else
                    {
                        UpdateEntity = context.PSW_BANK_TO_BANK_TRANSFER.Where(m => m.BTBT_ID == db_table.BTBT_ID).FirstOrDefault();
                        UpdateEntity.BTBT_EDIT_DATETIME = DateTime.Now;
                        CheckIfExist = UpdateEntity;
                    }
                    UpdateEntity.BTBT_REQUEST_NO = db_table.BTBT_REQUEST_NO;
                    UpdateEntity.BTBT_BANK = db_table.BTBT_BANK;
                    UpdateEntity.BTBT_CITY = db_table.BTBT_CITY;
                    UpdateEntity.BTBT_BRANCH = db_table.BTBT_BRANCH;
                    UpdateEntity.BTBT_REMARKS = db_table.BTBT_REMARKS;
                    UpdateEntity.BTBT_MAKER_ID = "1";
                    UpdateEntity.BTBT_CHECKER_ID = "1";
                    if (CheckIfExist == null)
                        context.PSW_BANK_TO_BANK_TRANSFER.Add(UpdateEntity);
                    else
                        context.Entry(UpdateEntity).State = EntityState.Modified;
                    int RowsAffected = context.SaveChanges();
                    if (RowsAffected > 0)
                    {
                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                        ModelState.Clear();
                    }
                    else
                    {
                        message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                    }
                }
                catch (Exception ex)
                {
                    DAL.LogException("BankToBankTransfer", "UpdateStatus", Session["USER_ID"].ToString(), ex);
                    message = ex.Message;
                }
                finally
                {
                    List<SelectListItem> EIF_Info = new SelectList(context.PSW_EIF_BASIC_INFO.ToList(), "EIF_BI_ID", "EIF_BI_EIF_REQUEST_NO", 0).ToList();
                    EIF_Info.Insert(0, (new SelectListItem { Text = "--Select Request Number--", Value = "0" }));
                    ViewBag.BTBT_REQUEST_NO = EIF_Info;
                    List<SelectListItem> Banks = new SelectList(context.PSW_LIST_OF_BANKS.ToList(), "LB_BANK_ID", "LB_BANK_NAME", 0).ToList();
                    Banks.Insert(0, (new SelectListItem { Text = "--Select Bank--", Value = "0" }));
                    ViewBag.BTBT_BANK = Banks;
                    List<SelectListItem> Cities = new SelectList(context.PSW_LIST_OF_CITIES.ToList(), "LOCT_ID", "LOCT_NAME", 0).ToList();
                    Cities.Insert(0, (new SelectListItem { Text = "--Select City--", Value = "0" }));
                    ViewBag.BTBT_CITY = Cities;
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult UpdateStatusList()
        {
            return PartialView(context.PSW_BANK_TO_BANK_TRANSFER_VIEW.ToList());
        }
        #endregion

        #region Bank To Bank Transfer
        public ActionResult Index(COBFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("BankToBankTranfer", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CHANGE_OF_BANK_REQUEST> List = context.PSW_CHANGE_OF_BANK_REQUEST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.COB_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.COB_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.COB_ID).ToList();
                            if (List.Count() == 0)
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_CHANGE_OF_BANK_REQUEST.Where(m => m.COB_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("BankToBankTransfer", "Index", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(COBFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("BankToBankTranfer", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_CHANGE_OF_BANK_REQUEST CheckIFExist = context.PSW_CHANGE_OF_BANK_REQUEST.Where(m => m.COB_ID == db_table.Entity.COB_ID).FirstOrDefault();
                        if (CheckIFExist != null)
                        {
                            if (db_table.Entity.COB_STATUS != null)
                            {
                                PSW_CHANGE_OF_BANK_REQUEST EntityToUpdate = new PSW_CHANGE_OF_BANK_REQUEST();
                                EntityToUpdate = CheckIFExist;
                                if (EntityToUpdate != null && db_table.Entity.COB_STATUS != null)//&& PreviousStatus != db_table.COB_STATUS)
                                {
                                    if (DAL.DO_EDI == "true")
                                    {
                                        string COBStatus = db_table.Entity.COB_STATUS;
                                        message += DAL.MethodId1538(EntityToUpdate.COB_UNIQUE_NO, COBStatus) + Environment.NewLine;
                                        //message += "  RAW REQUEST DETAILS  -------------------- " + Environment.NewLine;
                                        //message += DAL.message;
                                        if (message.Contains("200"))
                                        {
                                            string PreviousStatus = EntityToUpdate.COB_STATUS;
                                            EntityToUpdate.COB_STATUS = db_table.Entity.COB_STATUS;
                                            EntityToUpdate.COB_DATA_EDIT_DATETIME = DateTime.Now;
                                            context.Entry(EntityToUpdate).State = EntityState.Modified;
                                            int RowCount = context.SaveChanges();
                                            if(RowCount > 0)
                                                message += "Bank Sucessfully changed" + Environment.NewLine;
                                            else
                                                message += "Unable to change bank status" + Environment.NewLine;
                                        }
                                    }
                                    else
                                        message += "EDI is turned off from system configuration";
                                }
                            }
                            else
                            {
                                message = "Select status to continue";
                            }
                        }
                        else
                        {
                            message = "Select bank request to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("BankToBankTransfer", "Index", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }


        public ActionResult IndexFilter(COBFilter edit,int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("BankToBankTranfer", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CHANGE_OF_BANK_REQUEST> List = context.PSW_CHANGE_OF_BANK_REQUEST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.COB_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.COB_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.COB_ID).ToList();
                            if (List.Count() == 0)
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Index", edit);
        }

        [HttpPost]
        public ActionResult IndexFilter(COBFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("BankToBankTranfer", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CHANGE_OF_BANK_REQUEST> List = context.PSW_CHANGE_OF_BANK_REQUEST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.COB_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.COB_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.COB_ID).ToList();
                            if (List.Count() == 0)
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Index", edit);
        }

        [ChildActionOnly]
        public PartialViewResult IndexList(COBFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                List<PSW_CHANGE_OF_BANK_REQUEST> ViewData = new List<PSW_CHANGE_OF_BANK_REQUEST>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                    ViewData = context.PSW_CHANGE_OF_BANK_REQUEST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.COB_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.COB_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.COB_ID).ToList();
                else
                    ViewData = context.PSW_CHANGE_OF_BANK_REQUEST.OrderByDescending(m => m.COB_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        #endregion
    }
}
﻿using PSW.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using System.Data;
using System.Text;

namespace PSW.Controllers
{
    public class ClientInfoController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: ClientInfo
        #region Manage Client
        public ActionResult Index(ClientFilter edit,int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CLIENT_MASTER_DETAIL> List = context.PSW_CLIENT_MASTER_DETAIL.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.C_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.C_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.C_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.C_ISAUTH == true || m.C_MAKER_ID != SessionUser).OrderByDescending(m => m.C_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_CLIENT_INFO.Where(m => m.C_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                        else
                        {
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "Index", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> AccountStatus = new SelectList(context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ISAUTH == true).ToList(), "AS_ID", "AS_STATUS_NAME", 0).ToList();
                        //AccountStatus.Insert(0, (new SelectListItem { Text = "--Select Account Status--", Value = "0" }));
                        //ViewBag.C_ACCOUNT_STATUS_ID = AccountStatus;
                        //List<SelectListItem> AccountType = new SelectList(context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ISAUTH == true).ToList(), "AT_ID", "AT_STATUS_NAME", 0).ToList();
                        //AccountType.Insert(0, (new SelectListItem { Text = "--Select Account Type--", Value = "0" }));
                        //ViewBag.C_ACCOUNT_TYPE_ID = AccountType;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(ClientFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowsAffected = 0;
                    bool MessageToSent = false;
                    int? PreviousStatus = 0;

                    try
                    {
                        PSW_CLIENT_INFO insert_data = new PSW_CLIENT_INFO();

                        PSW_CLIENT_MASTER_DETAIL Check_Entity = new PSW_CLIENT_MASTER_DETAIL();

                        string[] emails = db_table.Entity.C_EMAIL_ADDRESS == null ? null : db_table.Entity.C_EMAIL_ADDRESS.Split(',');
                        string[] mobilenumber = db_table.Entity.C_MOBILE_NO == null ? null : db_table.Entity.C_MOBILE_NO.Split(',');
                        if (db_table.Entity.C_ID == 0)
                        {
                            Check_Entity = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == db_table.Entity.C_IBAN_NO || m.C_BANK_ACCOUNT_NO == db_table.Entity.C_BANK_ACCOUNT_NO /*|| m.C_NTN_NO == db_table.Entity.C_NTN_NO*/ /*|| emails.Any(x => m.C_EMAIL_ADDRESS.Contains(x))*/ /*|| mobilenumber.Any(x => m.C_MOBILE_NO.Contains(x))*/).FirstOrDefault();
                        }
                        else
                            Check_Entity = null;

                        if (Check_Entity == null)
                        {
                            Check_Entity = new PSW_CLIENT_MASTER_DETAIL();
                            Check_Entity = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_ID == db_table.Entity.C_ID && m.C_AMENDMENT_NO == db_table.Entity.C_AMENDMENT_NO).FirstOrDefault();

                            if (Check_Entity == null)
                            {
                                insert_data.C_ID = Convert.ToInt32(context.PSW_CLIENT_INFO.Max(m => (decimal?)m.C_ID)) + 1;
                                insert_data.C_ENTRY_DATETIME = DateTime.Now;
                                insert_data.C_NAME = db_table.Entity.C_NAME;
                                insert_data.C_ADDRESS = db_table.Entity.C_ADDRESS;
                                insert_data.C_BANK_ACCOUNT_NO = db_table.Entity.C_BANK_ACCOUNT_NO;
                                insert_data.C_NTN_NO = db_table.Entity.C_NTN_NO;
                                insert_data.C_IBAN_NO = db_table.Entity.C_IBAN_NO;
                                insert_data.C_ACCOUNT_STATUS_ID = db_table.Entity.C_ACCOUNT_STATUS_ID;
                                insert_data.C_ACCOUNT_TYPE_ID = db_table.Entity.C_ACCOUNT_TYPE_ID;
                                insert_data.C_EMAIL_ADDRESS = db_table.Entity.C_EMAIL_ADDRESS;
                                insert_data.C_MOBILE_NO = db_table.Entity.C_MOBILE_NO;
                                insert_data.C_ACCOUNT_BALANCE = db_table.Entity.C_ACCOUNT_BALANCE;
                                insert_data.C_STATUS = db_table.Entity.C_STATUS;
                                insert_data.C_STRN = db_table.Entity.C_STRN;
                                insert_data.C_CONTRACT_COMMISION = db_table.Entity.C_CONTRACT_COMMISION;
                                insert_data.C_LC_COMMISIOIN = db_table.Entity.C_LC_COMMISIOIN;
                                insert_data.C_EXPORT_COMMISION = db_table.Entity.C_EXPORT_COMMISION;
                                insert_data.C_BRANCH = db_table.Entity.C_BRANCH;
                                insert_data.C_FED_RATE = db_table.Entity.C_FED_RATE;
                                insert_data.C_AMENDMENT_NO = 0;
                                insert_data.C_VERIFIRD_BY_PSW = "Pending";
                                insert_data.C_TO_CLIENT_EMAIL = db_table.Entity.C_TO_CLIENT_EMAIL;
                                insert_data.C_MAKER_ID = Session["USER_ID"].ToString();
                                if (insert_data.C_MAKER_ID == "Admin123")
                                {
                                    insert_data.C_CHECKER_ID = insert_data.C_MAKER_ID;
                                    insert_data.C_ISAUTH = true;
                                }
                                else
                                {
                                    insert_data.C_CHECKER_ID = null;
                                    insert_data.C_ISAUTH = false;
                                }
                                context.PSW_CLIENT_INFO.Add(insert_data);
                                RowsAffected += context.SaveChanges();
                                // Passing Updated Model To View
                                db_table.Entity = insert_data;
                            }
                            else
                            {
                                insert_data.C_ID = Convert.ToInt32(context.PSW_CLIENT_INFO.Max(m => (decimal?)m.C_ID)) + 1;
                                insert_data.C_ENTRY_DATETIME = DateTime.Now;
                                insert_data.C_NAME = db_table.Entity.C_NAME;
                                insert_data.C_ADDRESS = db_table.Entity.C_ADDRESS;
                                insert_data.C_BANK_ACCOUNT_NO = db_table.Entity.C_BANK_ACCOUNT_NO;
                                insert_data.C_NTN_NO = db_table.Entity.C_NTN_NO;
                                insert_data.C_IBAN_NO = db_table.Entity.C_IBAN_NO;
                                PreviousStatus += Convert.ToInt32(Check_Entity.C_ACCOUNT_STATUS_ID);
                                insert_data.C_ACCOUNT_STATUS_ID = db_table.Entity.C_ACCOUNT_STATUS_ID;
                                insert_data.C_ACCOUNT_TYPE_ID = db_table.Entity.C_ACCOUNT_TYPE_ID;
                                insert_data.C_EMAIL_ADDRESS = db_table.Entity.C_EMAIL_ADDRESS;
                                insert_data.C_MOBILE_NO = db_table.Entity.C_MOBILE_NO;
                                insert_data.C_ACCOUNT_BALANCE = db_table.Entity.C_ACCOUNT_BALANCE;
                                insert_data.C_STATUS = db_table.Entity.C_STATUS;
                                insert_data.C_STRN = db_table.Entity.C_STRN;
                                insert_data.C_CONTRACT_COMMISION = db_table.Entity.C_CONTRACT_COMMISION;
                                insert_data.C_LC_COMMISIOIN = db_table.Entity.C_LC_COMMISIOIN;
                                insert_data.C_EXPORT_COMMISION = db_table.Entity.C_EXPORT_COMMISION;
                                insert_data.C_BRANCH = db_table.Entity.C_BRANCH;
                                insert_data.C_FED_RATE = db_table.Entity.C_FED_RATE;
                                insert_data.C_AMENDMENT_NO = Convert.ToInt32(context.PSW_CLIENT_INFO.Where(m => m.C_ID == db_table.Entity.C_ID).Max(m => (decimal?)m.C_AMENDMENT_NO)) + 1;
                                insert_data.C_VERIFIRD_BY_PSW = Check_Entity.C_VERIFIRD_BY_PSW;
                                insert_data.C_TO_CLIENT_EMAIL = db_table.Entity.C_TO_CLIENT_EMAIL;
                                insert_data.C_MAKER_ID = Session["USER_ID"].ToString();
                                if (insert_data.C_MAKER_ID == "Admin123")
                                {
                                    insert_data.C_CHECKER_ID = insert_data.C_MAKER_ID;
                                    insert_data.C_ISAUTH = true;
                                }
                                else
                                {
                                    insert_data.C_CHECKER_ID = null;
                                    insert_data.C_ISAUTH = false;
                                }
                                context.PSW_CLIENT_INFO.Add(insert_data);
                                RowsAffected += context.SaveChanges();
                                // Passing Updated Model To View
                                db_table.Entity = insert_data;
                                if (RowsAffected > 0)
                                {
                                    #region Get Previous Profile Type & Payment Mode
                                    List<PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT> GetPrevProfileType = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_CLIENT_ID == Check_Entity.C_ID && m.APT_STATUS == true).ToList();
                                    if (GetPrevProfileType.Count > 0)
                                    {
                                        RowsAffected = 0;
                                        foreach (PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT PrevPT in GetPrevProfileType)
                                        {
                                            PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT UpdateEntity = new PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT();
                                            UpdateEntity.APT_ID = Convert.ToInt32(context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Max(m => m.APT_ID)) + 1;
                                            UpdateEntity.APT_CLIENT_ID = insert_data.C_ID;
                                            UpdateEntity.APT_PROFILE_ID = PrevPT.APT_PROFILE_ID;
                                            UpdateEntity.APT_STATUS = PrevPT.APT_STATUS;
                                            UpdateEntity.APT_ENTRY_DATETIME = DateTime.Now;
                                            UpdateEntity.APT_EDIT_DATETIME = DateTime.Now;
                                            UpdateEntity.APT_MAKER_ID = Session["USER_ID"].ToString();
                                            UpdateEntity.APT_CHECKER_ID = Session["USER_ID"].ToString();
                                            UpdateEntity.APT_ISAUTH = true;
                                            context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Add(UpdateEntity);
                                            RowsAffected += context.SaveChanges();
                                        }
                                    }
                                    List<PSW_ASSIGN_PAY_MODE_TO_CLIENT> GetPrevPayMode = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == Check_Entity.C_ID && m.APMC_STATUS == true).ToList();
                                    if (GetPrevPayMode.Count > 0)
                                    {
                                        RowsAffected = 0;
                                        foreach (PSW_ASSIGN_PAY_MODE_TO_CLIENT PrevPT in GetPrevPayMode)
                                        {
                                            PSW_ASSIGN_PAY_MODE_TO_CLIENT UpdateEntity = new PSW_ASSIGN_PAY_MODE_TO_CLIENT();
                                            UpdateEntity.APMC_ID = Convert.ToInt32(context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Max(m => m.APMC_ID)) + 1;
                                            UpdateEntity.APMC_CLIENT_ID = insert_data.C_ID;
                                            UpdateEntity.APMC_PAY_MODE_ID = PrevPT.APMC_PAY_MODE_ID;
                                            UpdateEntity.APMC_STATUS = PrevPT.APMC_STATUS;
                                            UpdateEntity.APMC_ENTRY_DATETIME = DateTime.Now;
                                            UpdateEntity.APMC_EDIT_DATETIME = DateTime.Now;
                                            UpdateEntity.APMC_MAKER_ID = Session["USER_ID"].ToString();
                                            UpdateEntity.APMC_CHECKER_ID = Session["USER_ID"].ToString();
                                            UpdateEntity.APMC_IS_SHARED = PrevPT.APMC_IS_SHARED;
                                            UpdateEntity.APMC_ISAUTH = true;
                                            context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Add(UpdateEntity);
                                            RowsAffected += context.SaveChanges();
                                        }
                                    }
                                    #endregion
                                }
                                MessageToSent = true;
                            }
                            if (RowsAffected > 0)
                            {
                                if (MessageToSent == true && PreviousStatus != db_table.Entity.C_ACCOUNT_STATUS_ID)
                                {
                                    if (DAL.DO_EDI == "true")
                                    {
                                        message += DAL.MethodId1516(insert_data.C_ID) + Environment.NewLine;
                                        //message += "RAW REQUEST DETAILS  -------------------- " + Environment.NewLine;
                                        //message += DAL.message;
                                    }
                                    else
                                        message += "EDI is turned off from system configuration";
                                }
                                RowsAffected = 1;
                                message += "Data Inserted Successfully " + RowsAffected + " Affected";
                                //ModelState.Clear();
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                        else
                        {
                            if (Check_Entity.C_BANK_ACCOUNT_NO == db_table.Entity.C_BANK_ACCOUNT_NO)
                                message = "Account Number " + db_table.Entity.C_BANK_ACCOUNT_NO + " Alredy Exist";
                            //else if (Check_Entity.C_NTN_NO == db_table.Entity.C_NTN_NO)
                            //    message = "NTN Number " + db_table.Entity.C_NTN_NO + " Alredy Exist";
                            else if (Check_Entity.C_IBAN_NO == db_table.Entity.C_IBAN_NO)
                                message = "IBAN Number " + db_table.Entity.C_IBAN_NO + " Alredy Exist";
                            //else if (emails.Any(x => Check_Entity.C_EMAIL_ADDRESS.Contains(x)))
                            //    message = "Email Address " + Check_Entity.C_EMAIL_ADDRESS + " Alredy Exist";
                            //else if (mobilenumber.Any(x => Check_Entity.C_MOBILE_NO.Contains(x)))
                            //    message = "Mobile Number " + Check_Entity.C_MOBILE_NO + " Alredy Exist";
                            return View(db_table);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "Index", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> AccountStatus = new SelectList(context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ISAUTH == true).ToList(), "AS_ID", "AS_STATUS_NAME", 0).ToList();
                        //AccountStatus.Insert(0, (new SelectListItem { Text = "--Select Account Status--", Value = "0" }));
                        //ViewBag.C_ACCOUNT_STATUS_ID = AccountStatus;
                        //List<SelectListItem> AccountType = new SelectList(context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ISAUTH == true).ToList(), "AT_ID", "AT_STATUS_NAME", 0).ToList();
                        //AccountType.Insert(0, (new SelectListItem { Text = "--Select Account Type--", Value = "0" }));
                        //ViewBag.C_ACCOUNT_TYPE_ID = AccountType;
                        ViewBag.message = message;
                    }
                    return View(db_table);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult IndexFilter(ClientFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CLIENT_MASTER_DETAIL> List = context.PSW_CLIENT_MASTER_DETAIL.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.C_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.C_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.C_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.C_ISAUTH == true || m.C_MAKER_ID != SessionUser).OrderByDescending(m => m.C_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Index", edit);
        }
        [HttpPost]
        public ActionResult IndexFilter(ClientFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CLIENT_MASTER_DETAIL> List = context.PSW_CLIENT_MASTER_DETAIL.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.C_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.C_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.C_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.C_ISAUTH == true || m.C_MAKER_ID != SessionUser).OrderByDescending(m => m.C_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Index", edit);
        }
        [HttpPost]
        public FileStreamResult BulkClientPortfolio(HttpPostedFileBase postedFile)
        {
            string message = "";
            try
            {
                string filePath = string.Empty;
                if (postedFile != null)
                {
                    string path = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filePath = path + Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName);
                    if (extension == ".xlsx" || extension == ".xls")
                    {
                        postedFile.SaveAs(filePath);
                        var Scanner = new AntiVirus.Scanner();
                        var result = Scanner.ScanAndClean(filePath);
                        if (result != AntiVirus.ScanResult.VirusNotFound)
                        {
                            message += "malicious file detected Unable to Upload File To the Server.";
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                            return CreateLogFile(message);
                        }
                    }
                    else
                    {
                        message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                        return CreateLogFile(message);
                    } 
                    DataTable dt = new DataTable();
                    try
                    {
                        dt = BulkOptions.ReadAsDataTable(filePath);
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                        return CreateLogFile(message);
                    }
                    if (dt.Rows.Count > 0)
                    {
                        int RowCount = 0;
                        foreach (DataRow row in dt.Rows)
                        {
                            PSW_CLIENT_INFO client = new PSW_CLIENT_INFO();
                            if (row["AccountNumber"].ToString().Length > 2)
                            {
                                if (row["AccountNumber"].ToString().Length == 10 || row["AccountNumber"].ToString().Length == 9)
                                {
                                    string AccountNo = row["AccountNumber"].ToString();
                                    if (AccountNo.Length == 9)
                                        AccountNo = "0" + AccountNo;

                                    bool ClientExist = false;
                                    PSW_CLIENT_MASTER_DETAIL CheckClient = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_BANK_ACCOUNT_NO == AccountNo).FirstOrDefault();
                                    client = new PSW_CLIENT_INFO();
                                    client.C_ID = Convert.ToInt32(context.PSW_CLIENT_INFO.Max(m => (decimal?)m.C_ID)) + 1;
                                    client.C_BANK_ACCOUNT_NO = AccountNo;
                                    if (row["Client Name & Address"] != null && row["Client Name & Address"].ToString() != "")
                                    {
                                        string Client_Name = row["Client Name & Address"].ToString();
                                        client.C_NAME = Client_Name;
                                        if (row["Bank Account No/IBAN"].ToString().Length == 24)
                                        {
                                            string IBAN = row["Bank Account No/IBAN"].ToString();
                                            client.C_IBAN_NO = IBAN;
                                            if (row["Email Address"] != null && row["Email Address"].ToString() != "")
                                            {
                                                string Email = row["Email Address"].ToString();
                                                client.C_EMAIL_ADDRESS = Email;
                                                if (row["Active Mobile Number"] != null && row["Active Mobile Number"].ToString() != "")
                                                {
                                                    string Mobilnumber = row["Active Mobile Number"].ToString();
                                                    client.C_MOBILE_NO = Mobilnumber;
                                                    if (row["NTN"] != null && row["NTN"].ToString() != "")
                                                    {
                                                        string NTN = row["NTN"].ToString();
                                                        client.C_NTN_NO = NTN;
                                                        client.C_CNIC = NTN;
                                                        if (row["Type of Profile(import/export)"] != null && row["Type of Profile(import/export)"].ToString() != "")
                                                        {
                                                            var profileTypesUpdate = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_CLIENT_ID == client.C_ID).ToList();
                                                            profileTypesUpdate.ForEach(a => a.APT_STATUS = false);
                                                            context.SaveChanges();

                                                            string[] PROFILESTYPESTRING = row["Type of Profile(import/export)"].ToString().Split('/');
                                                            int isimporter = 0;
                                                            PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT profileTypeImporter = new PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT();
                                                            if (PROFILESTYPESTRING.Contains("IMPORTER"))
                                                            {
                                                                profileTypeImporter = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_CLIENT_ID == client.C_ID && m.APT_PROFILE_ID == 1).FirstOrDefault();
                                                                if (profileTypeImporter == null)
                                                                {
                                                                    profileTypeImporter = new PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT();
                                                                    profileTypeImporter.APT_ID = Convert.ToInt32(context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Max(m => (decimal?)m.APT_ID)) + 1;
                                                                    profileTypeImporter.APT_CLIENT_ID = client.C_ID;
                                                                    profileTypeImporter.APT_PROFILE_ID = 1;
                                                                    profileTypeImporter.APT_STATUS = true;
                                                                    profileTypeImporter.APT_ENTRY_DATETIME = DateTime.Now;
                                                                    profileTypeImporter.APT_MAKER_ID = Session["USER_ID"].ToString();
                                                                    profileTypeImporter.APT_CHECKER_ID = null;
                                                                    profileTypeImporter.APT_ISAUTH = true;
                                                                    isimporter = 1;
                                                                    context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Add(profileTypeImporter);
                                                                    RowCount += context.SaveChanges();
                                                                }
                                                                else
                                                                {
                                                                    profileTypeImporter.APT_STATUS = true;
                                                                    context.Entry(profileTypeImporter).State = EntityState.Modified;
                                                                    RowCount += context.SaveChanges();
                                                                }
                                                            }
                                                            PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT profileTypeExporter = new PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT();
                                                            if (PROFILESTYPESTRING.Contains("EXPORTER"))
                                                            {
                                                                profileTypeExporter = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_CLIENT_ID == client.C_ID && m.APT_PROFILE_ID == 2).FirstOrDefault();
                                                                if (profileTypeExporter == null)
                                                                {
                                                                    profileTypeExporter = new PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT();
                                                                    profileTypeExporter.APT_ID = (Convert.ToInt32(context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Max(m => (decimal?)m.APT_ID)) + 1) + isimporter;
                                                                    profileTypeExporter.APT_CLIENT_ID = client.C_ID;
                                                                    profileTypeExporter.APT_PROFILE_ID = 2;
                                                                    profileTypeExporter.APT_STATUS = true;
                                                                    profileTypeExporter.APT_ENTRY_DATETIME = DateTime.Now;
                                                                    profileTypeExporter.APT_MAKER_ID = Session["USER_ID"].ToString();
                                                                    profileTypeExporter.APT_CHECKER_ID = null;
                                                                    profileTypeExporter.APT_ISAUTH = true;
                                                                    context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Add(profileTypeExporter);
                                                                    RowCount += context.SaveChanges();
                                                                }
                                                                else
                                                                {
                                                                    profileTypeExporter.APT_STATUS = true;
                                                                    context.Entry(profileTypeExporter).State = EntityState.Modified;
                                                                    RowCount += context.SaveChanges();
                                                                }
                                                            }
                                                            if (row["Payment Terms"] != null && row["Payment Terms"].ToString() != "")
                                                            {
                                                                string[] PAYMENTTERMSSTRING = row["Payment Terms"].ToString().Split('/');
                                                                int PayModeCount = 0;
                                                                foreach (string PayTerm in PAYMENTTERMSSTRING)
                                                                {
                                                                    int PayTermCode = Convert.ToInt32(PayTerm);
                                                                    if (PayTerm != "" && PayTerm.Length > 2)
                                                                    {
                                                                        PSW_AUTH_PAY_MODES PayModeInfo = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_CODE == PayTermCode).FirstOrDefault();
                                                                        if (PayModeInfo != null)
                                                                        {
                                                                            PSW_ASSIGN_PAY_MODE_TO_CLIENT PayModes = new PSW_ASSIGN_PAY_MODE_TO_CLIENT();
                                                                            PayModes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == client.C_ID && m.APMC_PAY_MODE_ID == PayModeInfo.APM_ID).FirstOrDefault();
                                                                            if (PayModes == null)
                                                                            {
                                                                                PayModes = new PSW_ASSIGN_PAY_MODE_TO_CLIENT();
                                                                                PayModes.APMC_ID = (Convert.ToInt32(context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Max(m => (decimal?)m.APMC_ID)) + 1) + PayModeCount;
                                                                                PayModes.APMC_CLIENT_ID = client.C_ID;
                                                                                PayModes.APMC_PAY_MODE_ID = PayModeInfo.APM_ID;
                                                                                PayModes.APMC_STATUS = true;
                                                                                PayModes.APMC_ENTRY_DATETIME = DateTime.Now;
                                                                                PayModes.APMC_MAKER_ID = Session["USER_ID"].ToString();
                                                                                PayModes.APMC_CHECKER_ID = null;
                                                                                PayModes.APMC_ISAUTH = true;
                                                                                PayModes.APMC_IS_SHARED = false;
                                                                                context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Add(PayModes);
                                                                                PayModeCount += 1;
                                                                                RowCount += context.SaveChanges();
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "The Given Payment Term " + PayTerm + "  At Client : " + row["Client Name & Address"].ToString() + " Does Not Exist In System." + Environment.NewLine;
                                                                        }
                                                                    }
                                                                }

                                                                if (row["Account Status"] != null && row["Account Status"].ToString() != "")
                                                                {
                                                                    string AccountStatus = row["Account Status"].ToString();
                                                                    if (AccountStatus == "Active")
                                                                    {
                                                                        client.C_ACCOUNT_STATUS_ID = 1;
                                                                    }
                                                                    else if (AccountStatus == "InActive")
                                                                    {
                                                                        client.C_ACCOUNT_STATUS_ID = 2;
                                                                    }
                                                                    if (row["Account Type"] != null && row["Account Type"].ToString() != "")
                                                                    {
                                                                        string AccountType = row["Account Type"].ToString();
                                                                        PSW_ACCOUNT_TYPE accountTypeInfo = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_STATUS_NAME == AccountType).FirstOrDefault();
                                                                        if (accountTypeInfo != null)
                                                                        {
                                                                            client.C_ACCOUNT_TYPE_ID = accountTypeInfo.AT_ID;

                                                                            if (row["Balance"] != null && row["Balance"].ToString() != "")
                                                                            {
                                                                                string Balance = row["Balance"].ToString();
                                                                                client.C_ACCOUNT_BALANCE = Convert.ToDecimal(Balance);
                                                                                if (row["STRN"] != null && row["STRN"].ToString() != "")
                                                                                {
                                                                                    string strn = row["STRN"].ToString();
                                                                                    client.C_STRN = strn;
                                                                                    if (row["Contract Commision"] != null && row["Contract Commision"].ToString() != "")
                                                                                    {
                                                                                        string Contract_commission = row["Contract Commision"].ToString();
                                                                                        client.C_CONTRACT_COMMISION = Convert.ToDecimal(Contract_commission);

                                                                                        string LC_commission = row["LC Commission"].ToString();
                                                                                        if (row["LC Commission"] != null && row["LC Commission"].ToString() != "")
                                                                                            LC_commission = row["LC Commission"].ToString();
                                                                                        client.C_LC_COMMISIOIN = Convert.ToDecimal(LC_commission);

                                                                                        string EXPORT_commission = row["Export Commision"].ToString();
                                                                                        if (row["Export Commision"] != null && row["Export Commision"].ToString() != "")
                                                                                            EXPORT_commission = row["Export Commision"].ToString();
                                                                                        client.C_EXPORT_COMMISION = Convert.ToDecimal(EXPORT_commission);

                                                                                        string FED_RATE = row["FED %"].ToString();
                                                                                        if (row["FED %"] != null && row["FED %"].ToString() != "")
                                                                                            FED_RATE = row["FED %"].ToString();
                                                                                        client.C_FED_RATE = Convert.ToDecimal(FED_RATE);

                                                                                        string BRANCH = row["Branch"].ToString();
                                                                                        if (row["Branch"] != null && row["Branch"].ToString() != "")
                                                                                            BRANCH = row["Branch"].ToString();
                                                                                        client.C_BRANCH = BRANCH;

                                                                                        if (row["Address"] != null && row["Address"].ToString() != "")
                                                                                        {
                                                                                            string Address = row["Address"].ToString();
                                                                                            client.C_ADDRESS = Address;
                                                                                            if (row["Notification Email"] != null && row["Notification Email"].ToString() != "")
                                                                                            {
                                                                                                string Notification_Email = row["Notification Email"].ToString();
                                                                                                client.C_TO_CLIENT_EMAIL = Notification_Email;
                                                                                                client.C_VERIFIRD_BY_PSW = "Pending";
                                                                                                client.C_ENTRY_DATETIME = DateTime.Now;
                                                                                                client.C_STATUS = true;
                                                                                                client.C_MAKER_ID = Session["USER_ID"].ToString();
                                                                                                client.C_CHECKER_ID = null;
                                                                                                client.C_ISAUTH = true;
                                                                                                client.C_AMENDMENT_NO = Convert.ToInt32(context.PSW_CLIENT_INFO.Where(m => m.C_BANK_ACCOUNT_NO == client.C_BANK_ACCOUNT_NO).Max(m => (decimal?)m.C_AMENDMENT_NO)) + 1;
                                                                                                context.PSW_CLIENT_INFO.Add(client);
                                                                                                RowCount += context.SaveChanges();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                message += "The Email " + row["Notification Email"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + " is in incorrect form." + Environment.NewLine;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            message += "The Given Address " + row["Address"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + " Does Not Exist In System." + Environment.NewLine;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        message += "The Given Contract Rate " + row["Contract Commision"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + " Does Not Exist In System." + Environment.NewLine;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    message += "The Given STRN " + row["STRN"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + " Does Not Exist In System." + Environment.NewLine;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                message += "The Given Balance " + row["Balance"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + " Does Not Exist In System." + Environment.NewLine;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "The Given Account Type " + row["Account Type"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + " Does Not Exist In System." + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid Account Type " + row["Account Type"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid Account Status " + row["Account Status"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid Payment Terms " + row["Payment Terms"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Profile Type  " + row["Type of Profile(import/export)"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid NTN  " + row["NTN"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid Mobile Number  " + row["Active Mobile Number"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Email Address  " + row["Email Address"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid IBAN  " + row["Bank Account No/IBAN"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    else
                                    {
                                        message += "Invalid Client Name  " + row["Client Name & Address"].ToString() + "  At Account No : " + row["AccountNumber"].ToString() + Environment.NewLine;
                                    }
                                }
                                else
                                {
                                    message += "Invalid Account No. " + row["AccountNumber"].ToString() + "  At Client : " + row["Client Name & Address"].ToString() + Environment.NewLine;
                                }
                            }
                        }
                        RowCount += context.SaveChanges();
                        if (RowCount > 0)
                        {
                            message += "Data Uploaded Successfully." + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message = "No Data Found In The File." + Environment.NewLine;
                    }
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("BulkClientPortfolio", "Index", Session["USER_ID"].ToString(), ex);
                message += "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
            }
            return CreateLogFile(message);
        }
        public FileStreamResult CreateLogFile(string Message)
        {
            var byteArray = Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
        [HttpPost]
        public ActionResult ShareUpdatedInfo(ShareInfoSBP dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_TOKENS token = DAL.CheckTokenAuthentication();
                        if (token != null && token.PT_TOKEN != null)
                        {

                            switch (dbtable.mid)
                            {
                                case 1512:
                                    if (dbtable.cid != 0)
                                    {
                                        PSW_CLIENT_INFO client = context.PSW_CLIENT_INFO.Where(c => c.C_ID == dbtable.cid).FirstOrDefault();
                                        if (client != null)
                                        {
                                            if (DAL.DO_EDI == "true")
                                            {
                                                message = DAL.MethodId1512(client.C_ID) + Environment.NewLine;
                                            }
                                            else
                                                message += "EDI is turned off from system configuration";
                                        }
                                        else
                                        {
                                            message += "Client not found on provided id " + dbtable.cid.ToString() + Environment.NewLine;
                                        }
                                    }
                                    else
                                    {
                                        message += "Invalid client id, please try to come from client again or contact to software administrator" + Environment.NewLine;
                                    }
                                    break;
                                case 1554:
                                    if (DAL.DO_EDI == "true")
                                    {
                                        message = DAL.MethodId1554_or_1557() + Environment.NewLine;
                                    }
                                    else
                                        message += "EDI is turned off from system configuration";
                                    break;
                                case 1555:
                                    if (DAL.DO_EDI == "true")
                                    {
                                        message = DAL.MethodId1555_or_M1558() + Environment.NewLine;
                                    }
                                    else
                                        message += "EDI is turned off from system configuration";
                                    break;
                                case 1556:
                                    if (DAL.DO_EDI == "true")
                                    {
                                        message = DAL.MethodId1556_or_1559() + Environment.NewLine;
                                    }
                                    else
                                        message += "EDI is turned off from system configuration";
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            message = "Error While Generating Tokens.";
                        }

                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "ShareUpdateInfo", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        switch (dbtable.mid)
                        {
                            case 1512:
                                List<SelectListItem> PayMode = new SelectList(context.PSW_AUTH_PAY_MODES.ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                                PayMode.Insert(0, (new SelectListItem { Text = "--Select Pay Mode--", Value = "0" }));
                                ViewBag.APMC_PAY_MODE_ID = PayMode;
                                break;
                            case 1554:
                                List<SelectListItem> Country = new SelectList(context.PSW_LIST_OF_COUNTRIES.ToList(), "LC_ID", "LC_NAME", 0).ToList();
                                Country.Insert(0, (new SelectListItem { Text = "--Select Country--", Value = "0" }));
                                ViewBag.NLOC_COUNTRY_ID = Country;
                                List<SelectListItem> ProfileCount = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                                ProfileCount.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                                ViewBag.NLOC_PROFILE_TYPE = ProfileCount;
                                break;
                            case 1555:
                                List<SelectListItem> Commodity = new SelectList(context.PSW_LIST_OF_COMMODITIES.ToList(), "LCOM_ID", "LCOM_NAME", 0).ToList();
                                Commodity.Insert(0, (new SelectListItem { Text = "--Select Commodity--", Value = "0" }));
                                ViewBag.NLCOM_LCOM_ID = Commodity;
                                List<SelectListItem> Profile = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                                Profile.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                                ViewBag.NLCOM_PROFILE_TYPE = Profile;
                                break;
                            case 1556:
                                List<SelectListItem> Supplier = new SelectList(context.PSW_LIST_OF_SUPPLIERS.ToList(), "LOS_ID", "LOS_NAME", 0).ToList();
                                Supplier.Insert(0, (new SelectListItem { Text = "--Select Supplier--", Value = "0" }));
                                ViewBag.NLOS_LOS_ID = Supplier;
                                List<SelectListItem> ProfileSup = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                                ProfileSup.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                                ViewBag.NLOS_PROFILE_TYPE = ProfileSup;
                                break;
                            default:
                                break;
                        }
                        ViewBag.message = message;
                    }
                    switch (dbtable.mid)
                    {
                        case 1512:
                            AssignPayMode_Custom Entity = new AssignPayMode_Custom();
                            Entity.Entity = new PSW_ASSIGN_PAY_MODE_TO_CLIENT();
                            Entity.Entity.APMC_CLIENT_ID = dbtable.cid;
                            List<SelectListItem> PayModesImport
                                                = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 1).Select(f => new
                                                {
                                                    f.APM_ID,
                                                    APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                                                }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                            List<SelectListItem> PayModesExport
                            = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 2).Select(f => new
                            {
                                f.APM_ID,
                                APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                            }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                            Entity.PayModeListImport = PayModesImport;
                            Entity.PayModeListExport = PayModesExport;
                            return View("AssignPayMode", Entity);
                        case 1554:
                            NegativeCountriesFilter country = new NegativeCountriesFilter();
                            return View("NegativeListOfCountries", country);
                        case 1555:
                            NegativeCommoditiesFilter commodity = new NegativeCommoditiesFilter();
                            return View("NegativeListOfCommodities", commodity);
                        case 1556:
                            NegativeSuppliersFilter suppliers = new NegativeSuppliersFilter();
                            return View("NegativeListOfSuppliers", suppliers);
                        default:
                            break;
                    }
                    return View("Index");
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [ChildActionOnly]
        public PartialViewResult Client_Grid_View(DateTime? FromDate, DateTime? ToDate)
        {
            string SessionUser = Session["USER_ID"].ToString();
            List<PSW_CLIENT_MASTER_DETAIL> ViewData = new List<PSW_CLIENT_MASTER_DETAIL>();
            if (FromDate != null && ToDate != null)
            {
                ViewData = context.PSW_CLIENT_MASTER_DETAIL.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.C_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.C_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).OrderByDescending(m => m.C_ID).ToList();
                if (ViewData.Count() > 0)
                {
                    ViewData = ViewData.Where(m => m.C_MAKER_ID != SessionUser || m.C_ISAUTH == true).OrderByDescending(m => m.C_ID).ToList();
                }
            }
            else
            {
                ViewData = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_MAKER_ID != SessionUser || m.C_ISAUTH == true).OrderByDescending(m => m.C_ID).Take(10).ToList();
            }
            return PartialView(ViewData);
        }
        public PartialViewResult LoadPreviewResult(int ClientID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string ClientProfileType = "";
                    string ClientPaymentMode = "";
                    PSW_CLIENT_INFO_VIEW GetClient = context.PSW_CLIENT_INFO_VIEW.Where(m => m.C_ID == ClientID).FirstOrDefault();
                    List<PSW_CLIENT_ASSIGN_PAY_MODE_VIEW> GetAssignPayMode = context.PSW_CLIENT_ASSIGN_PAY_MODE_VIEW.Where(m => m.APMC_CLIENT_ID == ClientID && m.APMC_STATUS == true && m.APT_STATUS == true).ToList();
                    if (GetAssignPayMode.Count > 0)
                    {
                        foreach (PSW_CLIENT_ASSIGN_PAY_MODE_VIEW PayMode in GetAssignPayMode)
                        {
                            ClientPaymentMode += PayMode.APM_STATUS + " , ";
                        }
                    }
                    List<PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT_VIEW> GetProfileType = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT_VIEW.Where(m => m.APT_CLIENT_ID == ClientID && m.APT_STATUS == true).ToList();
                    if (GetProfileType.Count > 0)
                    {
                        foreach (PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT_VIEW ProfileType in GetProfileType)
                        {
                            ClientProfileType += ProfileType.P_NAME + Environment.NewLine;
                        }
                    }
                    ViewBag.ClientProfileType = ClientProfileType;
                    ViewBag.ClientPaymentMode = ClientPaymentMode;
                    return PartialView(GetClient);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeClient(int C_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_CLIENT_INFO UpdateEntity = context.PSW_CLIENT_INFO.Where(m => m.C_ID == C_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.C_ISAUTH = true;
                            UpdateEntity.C_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.C_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Client : " + UpdateEntity.C_NAME + " is successfully Authorized";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + C_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectAuthorizeClient(int C_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    try
                    {
                        PSW_CLIENT_INFO UpdateEntity = context.PSW_CLIENT_INFO.Where(m => m.C_ID == C_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_CLIENT_INFO.Remove(UpdateEntity);
                            RowCount += context.SaveChanges();
                            if (RowCount > 0)
                            {
                                List<PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT> CheckProfileType = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_CLIENT_ID == C_ID).ToList();
                                if (CheckProfileType.Count > 0)
                                {
                                    RowCount = 0;
                                    foreach (PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT DelProfType in CheckProfileType)
                                    {
                                        context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Remove(DelProfType);
                                        RowCount += context.SaveChanges();
                                    }
                                }
                                List<PSW_ASSIGN_PAY_MODE_TO_CLIENT> CheckPayMode = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == C_ID).ToList();
                                if (CheckPayMode.Count > 0)
                                {
                                    RowCount = 0;
                                    foreach (PSW_ASSIGN_PAY_MODE_TO_CLIENT DelPayMode in CheckPayMode)
                                    {
                                        context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Remove(DelPayMode);
                                        RowCount += context.SaveChanges();
                                    }
                                }
                                if (RowCount > 0)
                                {
                                    message = "Client " + UpdateEntity.C_NAME + " rejected successfully" + Environment.NewLine;
                                    return Json(message);
                                }
                                else
                                    message = "Unable to Update Database";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + C_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Assign Payment Mode 
        public ActionResult AssignPayMode(string C)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        int APM_CLIENT = Convert.ToInt32(C);
                        if (APM_CLIENT != 0)
                        {
                            List<int?> PreviousPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == APM_CLIENT && m.APMC_STATUS == true).Select(m => m.APMC_PAY_MODE_ID).ToList();
                            AssignPayMode_Custom entity = new AssignPayMode_Custom();
                            entity.Entity = new PSW_ASSIGN_PAY_MODE_TO_CLIENT();
                            entity.Entity.APMC_CLIENT_ID = APM_CLIENT;
                            List<PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT> ClientProfileType = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_CLIENT_ID == APM_CLIENT).ToList();
                            List<SelectListItem> PayModesImport = new List<SelectListItem>();
                            List<SelectListItem> PayModesExport = new List<SelectListItem>();
                            foreach (PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT CheckProfileType in ClientProfileType)
                            {
                                if (CheckProfileType.APT_PROFILE_ID == 1 && CheckProfileType.APT_STATUS == true && CheckProfileType.APT_ISAUTH == true)
                                {
                                    PayModesImport = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 1 && m.APM_ISAUTH == true).Select(f => new
                                    {
                                        f.APM_ID,
                                        APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                                    }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                                }
                                if (CheckProfileType.APT_PROFILE_ID == 2 && CheckProfileType.APT_STATUS == true && CheckProfileType.APT_ISAUTH == true)
                                {
                                    PayModesExport = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 2 && m.APM_ISAUTH == true).Select(f => new
                                    {
                                        f.APM_ID,
                                        APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                                    }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                                }
                            }
                            foreach (SelectListItem item in PayModesImport)
                            {
                                int? CurrentFId = Convert.ToInt32(item.Value);
                                if (PreviousPaymodes.Contains(CurrentFId))
                                    item.Selected = true;
                            }
                            foreach (SelectListItem item in PayModesExport)
                            {
                                int? CurrentFId = Convert.ToInt32(item.Value);
                                if (PreviousPaymodes.Contains(CurrentFId))
                                    item.Selected = true;
                            }
                            entity.PayModeListImport = PayModesImport;
                            entity.PayModeListExport = PayModesExport;
                            return View(entity);
                        }
                        else
                        {
                            return RedirectToAction("Index", "ClientInfo");
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "AssignPayMode", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }                
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AssignPayMode(AssignPayMode_Custom dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    bool ImportPaymode = false;
                    bool ExportPaymode = false;
                    string message = "";
                    try
                    {
                        int RowsAffected = 0;
                        /// Authorize Payment Modes For Import
                        if (dbtable.SelectedPayModeImport != null)
                        {
                            List<PSW_ASSIGN_PAY_MODE_TO_CLIENT> DeActivatedPayModesimport = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == dbtable.Entity.APMC_CLIENT_ID && !dbtable.SelectedPayModeImport.Contains(m.APMC_PAY_MODE_ID)).ToList();
                            if (DeActivatedPayModesimport.Count > 0)
                            {
                                foreach (PSW_ASSIGN_PAY_MODE_TO_CLIENT EachModeForUpdateImport in DeActivatedPayModesimport)
                                {
                                    EachModeForUpdateImport.APMC_STATUS = false;
                                    context.Entry(EachModeForUpdateImport).State = EntityState.Modified;
                                }
                                RowsAffected = context.SaveChanges();
                            }
                            int AssignModesIdImport = 0;
                            foreach (int? SelectedModesIdImport in dbtable.SelectedPayModeImport)
                            {
                                if (SelectedModesIdImport != null)
                                {
                                    PSW_ASSIGN_PAY_MODE_TO_CLIENT EachModeToUpdateImport = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == dbtable.Entity.APMC_CLIENT_ID && m.APMC_PAY_MODE_ID == SelectedModesIdImport).FirstOrDefault();
                                    if (EachModeToUpdateImport == null)
                                    {
                                        EachModeToUpdateImport = new PSW_ASSIGN_PAY_MODE_TO_CLIENT();
                                        if (AssignModesIdImport == 0)
                                        {
                                            EachModeToUpdateImport.APMC_ID = Convert.ToInt32(context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Max(m => (decimal?)m.APMC_ID)) + 1;
                                            AssignModesIdImport = EachModeToUpdateImport.APMC_ID;
                                        }
                                        else
                                        {
                                            AssignModesIdImport = (AssignModesIdImport + 1);
                                            EachModeToUpdateImport.APMC_ID = AssignModesIdImport;
                                        }
                                        EachModeToUpdateImport.APMC_CLIENT_ID = dbtable.Entity.APMC_CLIENT_ID;
                                        EachModeToUpdateImport.APMC_PAY_MODE_ID = SelectedModesIdImport;
                                        EachModeToUpdateImport.APMC_STATUS = true;
                                        EachModeToUpdateImport.APMC_ENTRY_DATETIME = DateTime.Now;
                                        EachModeToUpdateImport.APMC_MAKER_ID = Session["USER_ID"].ToString();
                                        if (EachModeToUpdateImport.APMC_MAKER_ID == "Admin123")
                                        {
                                            EachModeToUpdateImport.APMC_CHECKER_ID = EachModeToUpdateImport.APMC_MAKER_ID;
                                            EachModeToUpdateImport.APMC_ISAUTH = true;
                                        }
                                        else
                                        {
                                            EachModeToUpdateImport.APMC_CHECKER_ID = null;
                                            EachModeToUpdateImport.APMC_ISAUTH = false;
                                        }
                                        EachModeToUpdateImport.APMC_IS_SHARED = false;
                                        context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Add(EachModeToUpdateImport);
                                        //RowsAffected = context.SaveChanges();
                                    }
                                    else
                                    {
                                        EachModeToUpdateImport.APMC_STATUS = true;
                                        EachModeToUpdateImport.APMC_MAKER_ID = Session["USER_ID"].ToString();
                                        if (EachModeToUpdateImport.APMC_MAKER_ID == "Admin123")
                                        {
                                            EachModeToUpdateImport.APMC_CHECKER_ID = EachModeToUpdateImport.APMC_MAKER_ID;
                                            EachModeToUpdateImport.APMC_ISAUTH = true;
                                        }
                                        else
                                        {
                                            EachModeToUpdateImport.APMC_CHECKER_ID = null;
                                            EachModeToUpdateImport.APMC_ISAUTH = false;
                                        }
                                        EachModeToUpdateImport.APMC_EDIT_DATETIME = DateTime.Now;
                                        context.Entry(EachModeToUpdateImport).State = EntityState.Modified;
                                    }
                                }
                            }
                            RowsAffected = context.SaveChanges();
                        }
                        else
                        {
                            PSW_ASSIGN_PAY_MODE_TO_CLIENT UpdateEntity = new PSW_ASSIGN_PAY_MODE_TO_CLIENT();
                            List<PSW_CLIENT_ASSIGN_PAY_MODE_VIEW> PayModeImport = context.PSW_CLIENT_ASSIGN_PAY_MODE_VIEW.Where(m => m.APMC_CLIENT_ID == dbtable.Entity.APMC_CLIENT_ID && m.APMC_STATUS == true && m.APM_TYPE_ID == 1).ToList();
                            if (PayModeImport.Count > 0)
                            {
                                foreach (PSW_CLIENT_ASSIGN_PAY_MODE_VIEW item in PayModeImport)
                                {
                                    PSW_ASSIGN_PAY_MODE_TO_CLIENT AssignPayMode = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_ID == item.APMC_ID).FirstOrDefault();
                                    UpdateEntity = AssignPayMode;
                                    UpdateEntity.APMC_STATUS = false;
                                    UpdateEntity.APMC_EDIT_DATETIME = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    RowsAffected += context.SaveChanges();
                                }
                                if (RowsAffected > 0)
                                {
                                    ImportPaymode = true;
                                    message += "Import ";
                                }
                            }
                            else
                                RowsAffected = 1;
                        }
                        /// Authorize Payment Modes For Export
                        if (RowsAffected > 0)
                        {
                            if (dbtable.SelectedPayModeExport != null)
                            {
                                RowsAffected = 0;
                                List<PSW_ASSIGN_PAY_MODE_TO_CLIENT> DeActivatedPayModesExport = new List<PSW_ASSIGN_PAY_MODE_TO_CLIENT>();
                                if (dbtable.SelectedPayModeImport != null)
                                    DeActivatedPayModesExport = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == dbtable.Entity.APMC_CLIENT_ID && !dbtable.SelectedPayModeImport.Contains(m.APMC_PAY_MODE_ID) && !dbtable.SelectedPayModeExport.Contains(m.APMC_PAY_MODE_ID)).ToList();
                                else
                                    DeActivatedPayModesExport = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == dbtable.Entity.APMC_CLIENT_ID && !dbtable.SelectedPayModeExport.Contains(m.APMC_PAY_MODE_ID)).ToList();
                                if (DeActivatedPayModesExport.Count > 0)
                                {
                                    foreach (PSW_ASSIGN_PAY_MODE_TO_CLIENT EachModeForUpdateExport in DeActivatedPayModesExport)
                                    {
                                        EachModeForUpdateExport.APMC_STATUS = false;
                                        context.Entry(EachModeForUpdateExport).State = EntityState.Modified;
                                    }
                                    RowsAffected = context.SaveChanges();
                                }
                                int AssignModesIdExport = 0;
                                foreach (int? SelectedModesIdExport in dbtable.SelectedPayModeExport)
                                {
                                    if (SelectedModesIdExport != null)
                                    {
                                        PSW_ASSIGN_PAY_MODE_TO_CLIENT EachModeToUpdateExport = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == dbtable.Entity.APMC_CLIENT_ID && m.APMC_PAY_MODE_ID == SelectedModesIdExport).FirstOrDefault();
                                        if (EachModeToUpdateExport == null)
                                        {
                                            EachModeToUpdateExport = new PSW_ASSIGN_PAY_MODE_TO_CLIENT();
                                            if (AssignModesIdExport == 0)
                                            {
                                                EachModeToUpdateExport.APMC_ID = Convert.ToInt32(context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Max(m => (decimal?)m.APMC_ID)) + 1;
                                                AssignModesIdExport = EachModeToUpdateExport.APMC_ID;
                                            }
                                            else
                                            {
                                                AssignModesIdExport = (AssignModesIdExport + 1);
                                                EachModeToUpdateExport.APMC_ID = AssignModesIdExport;
                                            }
                                            EachModeToUpdateExport.APMC_CLIENT_ID = dbtable.Entity.APMC_CLIENT_ID;
                                            EachModeToUpdateExport.APMC_PAY_MODE_ID = SelectedModesIdExport;
                                            EachModeToUpdateExport.APMC_STATUS = true;
                                            EachModeToUpdateExport.APMC_ENTRY_DATETIME = DateTime.Now;
                                            EachModeToUpdateExport.APMC_MAKER_ID = Session["USER_ID"].ToString();
                                            if (EachModeToUpdateExport.APMC_MAKER_ID == "Admin123")
                                            {
                                                EachModeToUpdateExport.APMC_CHECKER_ID = EachModeToUpdateExport.APMC_MAKER_ID;
                                                EachModeToUpdateExport.APMC_ISAUTH = true;
                                            }
                                            else
                                            {
                                                EachModeToUpdateExport.APMC_CHECKER_ID = null;
                                                EachModeToUpdateExport.APMC_ISAUTH = false;
                                            }
                                            EachModeToUpdateExport.APMC_IS_SHARED = false;
                                            context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Add(EachModeToUpdateExport);
                                        }
                                        else
                                        {
                                            EachModeToUpdateExport.APMC_STATUS = true;
                                            EachModeToUpdateExport.APMC_MAKER_ID = Session["USER_ID"].ToString();
                                            if (EachModeToUpdateExport.APMC_MAKER_ID == "Admin123")
                                            {
                                                EachModeToUpdateExport.APMC_CHECKER_ID = EachModeToUpdateExport.APMC_MAKER_ID;
                                                EachModeToUpdateExport.APMC_ISAUTH = true;
                                            }
                                            else
                                            {
                                                EachModeToUpdateExport.APMC_CHECKER_ID = null;
                                                EachModeToUpdateExport.APMC_ISAUTH = false;
                                            }
                                            EachModeToUpdateExport.APMC_EDIT_DATETIME = DateTime.Now;
                                            context.Entry(EachModeToUpdateExport).State = EntityState.Modified;
                                        }
                                    }
                                }

                                RowsAffected = context.SaveChanges();
                            }
                            else
                            {
                                PSW_ASSIGN_PAY_MODE_TO_CLIENT UpdateEntity = new PSW_ASSIGN_PAY_MODE_TO_CLIENT();
                                List<PSW_CLIENT_ASSIGN_PAY_MODE_VIEW> PayModeExport = context.PSW_CLIENT_ASSIGN_PAY_MODE_VIEW.Where(m => m.APMC_CLIENT_ID == dbtable.Entity.APMC_CLIENT_ID && m.APMC_STATUS == true && m.APM_TYPE_ID == 2).ToList();
                                if (PayModeExport.Count > 0)
                                {
                                    foreach (PSW_CLIENT_ASSIGN_PAY_MODE_VIEW item in PayModeExport)
                                    {
                                        PSW_ASSIGN_PAY_MODE_TO_CLIENT AssignPayMode = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_ID == item.APMC_ID).FirstOrDefault();
                                        UpdateEntity = AssignPayMode;
                                        UpdateEntity.APMC_STATUS = false;
                                        UpdateEntity.APMC_EDIT_DATETIME = DateTime.Now;
                                        context.Entry(UpdateEntity).State = EntityState.Modified;
                                        RowsAffected += context.SaveChanges();
                                    }
                                    if (RowsAffected > 0)
                                    {
                                        ExportPaymode = true;
                                        message += "Export ";
                                    }
                                }
                                else
                                    RowsAffected = 1;
                            }
                            if (RowsAffected > 0)
                            {
                                if (ImportPaymode == true || ExportPaymode == true)
                                    message = "Payment Mode : " + message + "is sucessfully removed";

                                message = "Data Inserted Successfully " + Environment.NewLine + message;
                            }
                            else
                            {
                                message += "Error While Updating Payment Modes For Export";
                            }
                        }
                        else
                        {
                            message += "Error While Updating Payment Modes For Import";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "AssignPayMode", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        List<int?> PreviousPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == dbtable.Entity.APMC_CLIENT_ID && m.APMC_STATUS == true).Select(m => m.APMC_PAY_MODE_ID).ToList();
                        List<PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT> ClientProfileType = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_CLIENT_ID == dbtable.Entity.APMC_CLIENT_ID).ToList();
                        List<SelectListItem> PayModesImport = new List<SelectListItem>();
                        List<SelectListItem> PayModesExport = new List<SelectListItem>();
                        foreach (PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT CheckProfileType in ClientProfileType)
                        {
                            if (CheckProfileType.APT_PROFILE_ID == 1 && CheckProfileType.APT_STATUS == true && CheckProfileType.APT_ISAUTH == true)
                            {
                                PayModesImport = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 1 && m.APM_ISAUTH == true).Select(f => new
                                {
                                    f.APM_ID,
                                    APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                                }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                            }
                            if (CheckProfileType.APT_PROFILE_ID == 2 && CheckProfileType.APT_STATUS == true && CheckProfileType.APT_ISAUTH == true)
                            {
                                PayModesExport = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 2 && m.APM_ISAUTH == true).Select(f => new
                                {
                                    f.APM_ID,
                                    APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                                }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                            }
                        }
                        foreach (SelectListItem item in PayModesImport)
                        {
                            int? CurrentFId = Convert.ToInt32(item.Value);
                            if (PreviousPaymodes.Contains(CurrentFId))
                                item.Selected = true;
                        }
                        foreach (SelectListItem item in PayModesExport)
                        {
                            int? CurrentFId = Convert.ToInt32(item.Value);
                            if (PreviousPaymodes.Contains(CurrentFId))
                                item.Selected = true;
                        }
                        dbtable.PayModeListImport = PayModesImport;
                        dbtable.PayModeListExport = PayModesExport;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View(dbtable);
        }
        [ChildActionOnly]
        public PartialViewResult AssignPayModeList(int? C)
        {
            string SessionUser = Session["USER_ID"].ToString();
            List<PSW_CLIENT_ASSIGN_PAY_MODE_VIEW> list = context.PSW_CLIENT_ASSIGN_PAY_MODE_VIEW.Where(m => m.APMC_CLIENT_ID == C && m.APMC_STATUS == true && m.APT_STATUS == true && (m.APMC_ISAUTH == true || m.APMC_MAKER_ID != SessionUser)).ToList();
            return PartialView(list);
        }
        [HttpPost]
        public JsonResult AuthorizeAssignPayMode(int APMC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ASSIGN_PAY_MODE_TO_CLIENT UpdateEntity = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_ID == APMC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.APMC_ISAUTH = true;
                            UpdateEntity.APMC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.APMC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Payment Mode is successfully Authorized";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + APMC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectAuthorizeAssignPayMode(int APMC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ASSIGN_PAY_MODE_TO_CLIENT UpdateEntity = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_ID == APMC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Selected payment mode is rejected successfully";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + APMC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Negative List of Countries
        public ActionResult NegativeListOfCountries(string I, NegativeCountriesFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW> List = context.PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLOC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLOC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.NLOC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.NLOC_ISAUTH == true || m.NLOC_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        int NLOC_ID_PARAM = Convert.ToInt32(I);
                        if (NLOC_ID_PARAM != 0)
                        {
                            edit.Entity = context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Where(m => m.NLOC_ID == NLOC_ID_PARAM).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on ID = " + NLOC_ID_PARAM;
                                return View();
                            }
                        }
                        else
                        {
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "NegativeListOfCountries", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> Country = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ISAUTH == true && m.LC_STATUS == true).ToList(), "LC_ID", "LC_NAME", 0).ToList();
                        //Country.Insert(0, (new SelectListItem { Text = "--Select Country--", Value = "0" }));
                        //ViewBag.NLOC_COUNTRY_ID = Country;
                        //List<SelectListItem> Profile = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        //Profile.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                        //ViewBag.NLOC_PROFILE_TYPE = Profile;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult NegativeListOfCountries(NegativeCountriesFilter dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_NEGATIVE_LIST_OF_COUNTRIES UpdateEntity = new PSW_NEGATIVE_LIST_OF_COUNTRIES();

                        PSW_NEGATIVE_LIST_OF_COUNTRIES CheckIfExist = new PSW_NEGATIVE_LIST_OF_COUNTRIES();
                        if (dbtable.Entity.NLOC_ID == 0)
                        {
                            UpdateEntity.NLOC_ID = Convert.ToInt32(context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Max(m => (decimal?)m.NLOC_ID)) + 1;
                            UpdateEntity.NLOC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Where(m => m.NLOC_ID == dbtable.Entity.NLOC_ID).FirstOrDefault();
                            UpdateEntity.NLOC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.NLOC_PROFILE_TYPE = dbtable.Entity.NLOC_PROFILE_TYPE;
                        UpdateEntity.NLOC_COUNTRY_ID = dbtable.Entity.NLOC_COUNTRY_ID;
                        UpdateEntity.NLOC_STATUS = dbtable.Entity.NLOC_STATUS;
                        UpdateEntity.NLOC_MAKER_ID = Session["USER_ID"].ToString(); ;
                        if (UpdateEntity.NLOC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.NLOC_CHECKER_ID = UpdateEntity.NLOC_MAKER_ID;
                            UpdateEntity.NLOC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.NLOC_CHECKER_ID = null;
                            UpdateEntity.NLOC_ISAUTH = false;
                        }
                        UpdateEntity.NLOC_IS_SHARED = false;
                        if (CheckIfExist == null)
                            context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "NegativeListOfCountries", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        //List<SelectListItem> Country = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ISAUTH == true && m.LC_STATUS == true).ToList(), "LC_ID", "LC_NAME", 0).ToList();
                        //Country.Insert(0, (new SelectListItem { Text = "--Select Country--", Value = "0" }));
                        //ViewBag.NLOC_COUNTRY_ID = Country;
                        //List<SelectListItem> Profile = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        //Profile.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                        //ViewBag.NLOC_PROFILE_TYPE = Profile;
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult NegativeListOfCountriesFilter(string I, NegativeCountriesFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW> List = context.PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLOC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLOC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.NLOC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.NLOC_ISAUTH == true || m.NLOC_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("NegativeListOfCountries", edit);
        }
        [HttpPost]
        public ActionResult NegativeListOfCountriesFilter(NegativeCountriesFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW> List = context.PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLOC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLOC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.NLOC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.NLOC_ISAUTH == true || m.NLOC_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("NegativeListOfCountries",edit);
        }
        [ChildActionOnly]
        public PartialViewResult NegativeListOfCountriesView(NegativeCountriesFilter Filter)
        {
            string SessionUser = Session["USER_ID"].ToString();
            List<PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW> ViewData = new List<PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW>();
            if (Filter.FromDate != null && Filter.ToDate != null)
            {
                ViewData = context.PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLOC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLOC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.NLOC_ID).ToList();
                if (ViewData.Count() > 0)
                {
                    ViewData = ViewData.Where(m => m.NLOC_ISAUTH == true || m.NLOC_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOC_ID).ToList();
                }
            }
            else
                ViewData = context.PSW_NEGATIVE_LIST_OF_COUNTRIES_VIEW.Where(m => m.NLOC_ISAUTH == true || m.NLOC_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOC_ID).Take(10).ToList();
            return PartialView(ViewData);
        }
        [HttpPost]
        public JsonResult AuthorizeNegativeListOfCountries(int NLOC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_NEGATIVE_LIST_OF_COUNTRIES UpdateEntity = context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Where(m => m.NLOC_ID == NLOC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.NLOC_ISAUTH = true;
                            UpdateEntity.NLOC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.NLOC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Selected Country is successfully Authorized";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + NLOC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectAuthorizeNegativeListOfCountries(int NLOC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_NEGATIVE_LIST_OF_COUNTRIES UpdateEntity = context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Where(m => m.NLOC_ID == NLOC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Selected country is rejected successfully";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + NLOC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Negative List of Commodities
        public ActionResult NegativeListOfCommodities(string I,NegativeCommoditiesFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW> List = context.PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLCOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLCOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.NLCOM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.NLCOM_ISAUTH == true || m.NLCOM_MAKER_ID != SessionUser).OrderByDescending(m => m.NLCOM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        int NLCOM_ID_PARAM = Convert.ToInt32(I);
                        if (NLCOM_ID_PARAM != 0)
                        {
                            edit.Entity = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_ID == NLCOM_ID_PARAM).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on ID = " + NLCOM_ID_PARAM;
                                return View();
                            }
                        }
                        else
                        {
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "NegativeListOfCommodities", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> Country = new SelectList(context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ISAUTH == true && m.LCOM_STATUS == true).Select(f => new
                        //{
                        //    f.LCOM_ID,
                        //    LCOM_NAME = f.LCOM_CODE + " -- " + f.LCOM_NAME
                        //}).ToList(), "LCOM_ID", "LCOM_NAME", 0).ToList();
                        //Country.Insert(0, (new SelectListItem { Text = "--Select Commodity--", Value = "0" }));
                        //ViewBag.NLCOM_LCOM_ID = Country;
                        //List<SelectListItem> Profile = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        //Profile.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                        //ViewBag.NLCOM_PROFILE_TYPE = Profile;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult NegativeListOfCommodities(NegativeCommoditiesFilter dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_NEGATIVE_LIST_OF_COMMODITIES UpdateEntity = new PSW_NEGATIVE_LIST_OF_COMMODITIES();

                        PSW_NEGATIVE_LIST_OF_COMMODITIES CheckIfExist = new PSW_NEGATIVE_LIST_OF_COMMODITIES();
                        if (dbtable.Entity.NLCOM_ID == 0)
                        {
                            UpdateEntity.NLCOM_ID = Convert.ToInt32(context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Max(m => (decimal?)m.NLCOM_ID)) + 1;
                            UpdateEntity.NLCOM_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_ID == dbtable.Entity.NLCOM_ID).FirstOrDefault();
                            UpdateEntity.NLCOM_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.NLCOM_LCOM_ID = dbtable.Entity.NLCOM_LCOM_ID;
                        UpdateEntity.NLCOM_PROFILE_TYPE = dbtable.Entity.NLCOM_PROFILE_TYPE;
                        UpdateEntity.NLCOM_STATUS = dbtable.Entity.NLCOM_STATUS;
                        UpdateEntity.NLCOM_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.NLCOM_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.NLCOM_CHECKER_ID = UpdateEntity.NLCOM_MAKER_ID;
                            UpdateEntity.NLCOM_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.NLCOM_CHECKER_ID = null;
                            UpdateEntity.NLCOM_ISAUTH = false;
                        }
                        UpdateEntity.NLCOM_IS_SHARED = false;
                        if (CheckIfExist == null)
                            context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "NegativeListOfCommodities", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> Country = new SelectList(context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ISAUTH == true && m.LCOM_STATUS == true).ToList(), "LCOM_ID", "LCOM_NAME", 0).ToList();
                        //Country.Insert(0, (new SelectListItem { Text = "--Select Commodity--", Value = "0" }));
                        //ViewBag.NLCOM_LCOM_ID = Country;
                        //List<SelectListItem> Profile = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        //Profile.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                        //ViewBag.NLCOM_PROFILE_TYPE = Profile;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public ActionResult NegativeListOfCommoditiesFilter(string I, NegativeCommoditiesFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW> List = context.PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLCOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLCOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.NLCOM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.NLCOM_ISAUTH == true || m.NLCOM_MAKER_ID != SessionUser).OrderByDescending(m => m.NLCOM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("NegativeListOfCommodities",edit);
        }
        [HttpPost]
        public ActionResult NegativeListOfCommoditiesFilter(NegativeCommoditiesFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW> List = context.PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLCOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLCOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.NLCOM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.NLCOM_ISAUTH == true || m.NLCOM_MAKER_ID != SessionUser).OrderByDescending(m => m.NLCOM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("NegativeListOfCommodities",edit);
        }
        [ChildActionOnly]
        public PartialViewResult NegativeListOfCommoditiesList(NegativeCommoditiesFilter Filter)
        {
            string SessionUser = Session["USER_ID"].ToString();
            List<PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW> ViewData = new List<PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW>();
            if (Filter.FromDate != null && Filter.ToDate != null)
            {
                ViewData = context.PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLCOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLCOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.NLCOM_ID).ToList();
                if (ViewData.Count() > 0)
                {
                    ViewData = ViewData.Where(m => m.NLCOM_MAKER_ID != SessionUser || m.NLCOM_ISAUTH == true).OrderByDescending(m => m.NLCOM_ID).ToList();
                }
            }
            else
                ViewData = context.PSW_NEGATIVE_LIST_OF_COMMODITIES_VIEW.Where(m => m.NLCOM_MAKER_ID != SessionUser || m.NLCOM_ISAUTH == true).OrderByDescending(m => m.NLCOM_ID).Take(10).ToList();
            return PartialView(ViewData);
        }
        [HttpPost]
        public JsonResult AuthorizeNegativeListOfCommodities(int NLCOM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_NEGATIVE_LIST_OF_COMMODITIES UpdateEntity = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_ID == NLCOM_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.NLCOM_ISAUTH = true;
                            UpdateEntity.NLCOM_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.NLCOM_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Selected Commodity is successfully Authorized";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + NLCOM_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectAuthorizeNegativeListOfCommodities(int NLCOM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_NEGATIVE_LIST_OF_COMMODITIES UpdateEntity = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_ID == NLCOM_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Selected commodity is rejected successfully";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + NLCOM_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Negative List of Suppliers
        public ActionResult NegativeListOfSuppliers(string I, NegativeSuppliersFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW> Suppliers = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLOS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLOS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.NLOS_ID).ToList();
                            if (Suppliers.Count() > 0)
                            {
                                Suppliers = Suppliers.Where(m => m.NLOS_ISAUTH == true || m.NLOS_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOS_ID).ToList();
                                if (Suppliers.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if(edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        int NLCOS_ID_PARAM = Convert.ToInt32(I);
                        if (NLCOS_ID_PARAM != 0)
                        {
                            edit.Entity = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_ID == NLCOS_ID_PARAM).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on ID = " + NLCOS_ID_PARAM;
                                return View();
                            }
                        }
                        else
                        {
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "NegativeListOfSuppliers", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> Supplier = new SelectList(context.PSW_LIST_OF_SUPPLIERS.Where(m => m.LOS_STATUS == true && m.LOS_ISAUTH == true).ToList(), "LOS_ID", "LOS_NAME", 0).ToList();
                        //Supplier.Insert(0, (new SelectListItem { Text = "--Select Supplier--", Value = "0" }));
                        //ViewBag.NLOS_LOS_ID = Supplier;
                        //List<SelectListItem> Profile = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        //Profile.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                        //ViewBag.NLOS_PROFILE_TYPE = Profile;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult NegativeListOfSuppliers(NegativeSuppliersFilter dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_NEGATIVE_LIST_OF_SUPPLIERS UpdateEntity = new PSW_NEGATIVE_LIST_OF_SUPPLIERS();

                        PSW_NEGATIVE_LIST_OF_SUPPLIERS CheckIfExist = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_LOS_ID == dbtable.Entity.NLOS_LOS_ID && m.NLOS_PROFILE_TYPE == dbtable.Entity.NLOS_PROFILE_TYPE).FirstOrDefault();

                        if (dbtable.Entity.NLOS_ID == 0)
                        {
                            UpdateEntity.NLOS_ID = Convert.ToInt32(context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Max(m => (decimal?)m.NLOS_ID)) + 1;
                            UpdateEntity.NLOS_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_ID == dbtable.Entity.NLOS_ID).FirstOrDefault();
                            UpdateEntity.NLOS_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.NLOS_LOS_ID = dbtable.Entity.NLOS_LOS_ID;
                        UpdateEntity.NLOS_PROFILE_TYPE = dbtable.Entity.NLOS_PROFILE_TYPE;
                        UpdateEntity.NLOS_STATUS = dbtable.Entity.NLOS_STATUS;
                        UpdateEntity.NLOS_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.NLOS_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.NLOS_CHECKER_ID = UpdateEntity.NLOS_MAKER_ID;
                            UpdateEntity.NLOS_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.NLOS_CHECKER_ID = null;
                            UpdateEntity.NLOS_ISAUTH = false;
                        }
                        UpdateEntity.NLOS_IS_SHARED = false;
                        if (CheckIfExist == null)
                            context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "NegativeListOfSuppliers", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> Supplier = new SelectList(context.PSW_LIST_OF_SUPPLIERS.Where(m => m.LOS_STATUS == true && m.LOS_ISAUTH == true).ToList(), "LOS_ID", "LOS_NAME", 0).ToList();
                        //Supplier.Insert(0, (new SelectListItem { Text = "--Select Supplier--", Value = "0" }));
                        //ViewBag.NLOS_LOS_ID = Supplier;
                        //List<SelectListItem> Profile = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        //Profile.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                        //ViewBag.NLOS_PROFILE_TYPE = Profile;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public ActionResult NegativeListOfSuppliersFilter(string I, NegativeSuppliersFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW> Suppliers = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLOS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLOS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.NLOS_ID).ToList();
                            if (Suppliers.Count() > 0)
                            {
                                Suppliers = Suppliers.Where(m => m.NLOS_ISAUTH == true || m.NLOS_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOS_ID).ToList();
                                if (Suppliers.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("NegativeListOfSuppliers", edit);
        }
        [HttpPost]
        public ActionResult NegativeListOfSuppliersFilter(NegativeSuppliersFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW> Suppliers = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLOS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLOS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.NLOS_ID).ToList();
                            if (Suppliers.Count() > 0)
                            {
                                Suppliers = Suppliers.Where(m => m.NLOS_ISAUTH == true || m.NLOS_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOS_ID).ToList();
                                if (Suppliers.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("NegativeListOfSuppliers", edit);
        }
        [ChildActionOnly]
        public PartialViewResult NegativeListOfSuppliersList(NegativeSuppliersFilter Filter)
        {
            string SessionUser = Session["USER_ID"].ToString();
            List<PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW> Suppliers = new List<PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW>();
            if (Filter.FromDate != null && Filter.ToDate != null)
            {
                Suppliers = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.NLOS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.NLOS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.NLOS_ID).ToList();
                if (Suppliers.Count() > 0)
                {
                    Suppliers = Suppliers.Where(m => m.NLOS_ISAUTH == true || m.NLOS_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOS_ID).ToList();
                }
            }
            else
                Suppliers = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW.Where(m => m.NLOS_ISAUTH == true || m.NLOS_MAKER_ID != SessionUser).OrderByDescending(m => m.NLOS_ID).Take(10).ToList();
            return PartialView(Suppliers);
        }
        [HttpPost]
        public JsonResult AuthorizeNegativeListOfSuppliers(int NLOS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_NEGATIVE_LIST_OF_SUPPLIERS UpdateEntity = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_ID == NLOS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.NLOS_ISAUTH = true;
                            UpdateEntity.NLOS_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.NLOS_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Selected Supplier is successfully Authorized";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + NLOS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectAuthorizeNegativeListOfSuppliers(int NLOS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_NEGATIVE_LIST_OF_SUPPLIERS UpdateEntity = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_ID == NLOS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Selected supplier is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + NLOS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Assign Profile Type
        public ActionResult AssignProfileType(string I, string C)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        int APT_ID_PARAM = Convert.ToInt32(I);
                        int APT_CLIENT = Convert.ToInt32(C);

                        if (APT_ID_PARAM != 0)
                        {
                            PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT edit = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_ID == APT_ID_PARAM).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on ID = " + APT_ID_PARAM;
                                return View();
                            }
                        }
                        else
                        {
                            if (APT_CLIENT != 0)
                            {
                                PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT entity = new PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT();
                                entity.APT_CLIENT_ID = APT_CLIENT;
                                return View(entity);
                            }
                            else
                            {
                                return RedirectToAction("Index", "ClientInfo");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "AssignProfileType", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> Profile = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        Profile.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                        ViewBag.APT_PROFILE_ID = Profile;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AssignProfileType(PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT UpdateEntity = new PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT();
                        PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT CheckIfExist = new PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT();
                        CheckIfExist = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_CLIENT_ID == dbtable.APT_CLIENT_ID && (m.APT_STATUS == dbtable.APT_STATUS && m.APT_PROFILE_ID == dbtable.APT_PROFILE_ID)).FirstOrDefault();
                        if (CheckIfExist == null)
                        {
                            if (dbtable.APT_ID == 0)
                            {
                                UpdateEntity.APT_ID = Convert.ToInt32(context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Max(m => (decimal?)m.APT_ID)) + 1;
                                UpdateEntity.APT_ENTRY_DATETIME = DateTime.Now;
                                CheckIfExist = null;
                            }
                            else
                            {
                                UpdateEntity = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_ID == dbtable.APT_ID).FirstOrDefault();
                                UpdateEntity.APT_EDIT_DATETIME = DateTime.Now;
                                CheckIfExist = UpdateEntity;
                            }
                            UpdateEntity.APT_CLIENT_ID = dbtable.APT_CLIENT_ID;
                            UpdateEntity.APT_PROFILE_ID = dbtable.APT_PROFILE_ID;
                            UpdateEntity.APT_STATUS = dbtable.APT_STATUS;
                            UpdateEntity.APT_MAKER_ID = Session["USER_ID"].ToString();
                            if (UpdateEntity.APT_MAKER_ID == "Admin123")
                            {
                                UpdateEntity.APT_CHECKER_ID = UpdateEntity.APT_MAKER_ID;
                                UpdateEntity.APT_ISAUTH = true;
                            }
                            else
                            {
                                UpdateEntity.APT_CHECKER_ID = null;
                                UpdateEntity.APT_ISAUTH = false;
                            }
                            if (CheckIfExist == null)
                                context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Add(UpdateEntity);
                            else
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                        else
                        {
                            message = "Selected Profile Type is Already Assigned To The Client";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientInfo", "AssignProfileType", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> Profile = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        Profile.Insert(0, (new SelectListItem { Text = "--Select Profile Type--", Value = "0" }));
                        ViewBag.APT_PROFILE_ID = Profile;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT entity = new PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT();
            entity.APT_CLIENT_ID = dbtable.APT_CLIENT_ID;
            return View(entity);
        }
        [ChildActionOnly]
        public PartialViewResult AssignProfileTypeList(int? C)
        {
            string SessionUser = Session["USER_ID"].ToString();
            List<PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT_VIEW> list = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT_VIEW.Where(m => m.CLIENT_ID == C && (m.APT_ISAUTH == true || m.APT_MAKER_ID != SessionUser)).ToList();
            return PartialView(list);
        }
        [HttpPost]
        public JsonResult AuthorizeAssignProfileType(int APT_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT UpdateEntity = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_ID == APT_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.APT_ISAUTH = true;
                            UpdateEntity.APT_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.APT_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Profile Type is successfully Authorized";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on of Authorize payment mode ID# " + APT_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectAuthorizeAssignProfileType(int? APT_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT UpdateEntity = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(m => m.APT_ID == APT_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Selected profile type is rejected successfully";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + APT_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion
    }
}
﻿using CrystalDecisions.CrystalReports.Engine;
using PSW.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace PSW.Controllers
{
    public class EEFRequestController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: EEFRequest

        #region EEF 
        public ActionResult Index(EEFFilter dbtable)
        {
            string DisplayStatus = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (dbtable.EEF != null)
                        {
                            if (dbtable.EEF.FDI_STATUS != null && dbtable.EEF.FDI_STATUS != "0")
                            {
                                DisplayStatus = DAL.GetFilterationStatus("EEF").Where(m => m.Value == dbtable.EEF.FDI_STATUS).FirstOrDefault().Text;
                                return View(dbtable);
                            }
                        }
                        else
                        {
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                            if (ex.InnerException.InnerException != null)
                            {
                                message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                            }
                        }
                    }
                    finally
                    {
                        List<SelectListItem> Status = DAL.GetFilterationStatus("EEF");
                        Status.Insert(0, (new SelectListItem { Text = "--Select Status--", Value = "0" }));
                        ViewBag.FDI_STATUS = Status;
                        ViewBag.DisplayStatus = DisplayStatus;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }

        public ActionResult ManageEEF(string E = "", int M = 0)
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
            {
                string message = "";
                EEF LoadEEF = new EEF();
                try
                {
                    if (E != "")
                    {
                        ViewBag.InitiateNewEEF = "Edit";
                        LoadEEF.DocInfo = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == M).FirstOrDefault();
                        LoadEEF.DocRequestNo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == E && m.FDRI_AMENDMENT_NO == M).FirstOrDefault();
                        LoadEEF.hscodes = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == E && m.EEF_HC_AMENDMENT_NO == M).ToList();
                        LoadEEF.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();
                        LoadEEF.CustomNew = false;
                        return View(LoadEEF);
                    }
                    else
                    {
                        string REQ = "";
                        ViewBag.InitiateNewEEF = "New";
                        try
                        {
                            using (PSWEntities cont = new PSWEntities())
                            {
                                PSW_FORM_E_DOCUMENT_INFO docInfo = new PSW_FORM_E_DOCUMENT_INFO();
                                docInfo.FDI_ID = Convert.ToInt32(cont.PSW_FORM_E_DOCUMENT_INFO.Max(m => (decimal?)m.FDI_ID)) + 1;
                                docInfo.FDI_FORM_E_NO = "";
                                docInfo.FDI_INTERNAL_REF = "";

                                REQ = docInfo.FDI_FORM_E_NO;
                                docInfo.FDI_STATUS = "Initiated";
                                docInfo.FDI_STATUS_DATE = DateTime.Now.AddHours(5);
                                docInfo.FDI_MAKER_ID = Session["USER_ID"].ToString();
                                docInfo.FDI_AMENDMENT_NO = 0;
                                docInfo.FDI_PHASE_OF_ISSUE = "Karachi";
                                docInfo.FDI_ISSUE_BANK = "Citibank N.A. Pakistan";
                                docInfo.FDI_CERTIFIED = true;
                                docInfo.FDI_DOC_SUBMISSION = true;

                                PSW_FORM_E_DOCUMENT_REQUEST_INFO docReqinfo = new PSW_FORM_E_DOCUMENT_REQUEST_INFO();
                                docReqinfo.FDRI_ID = Convert.ToInt32(cont.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Max(m => (decimal?)m.FDRI_ID)) + 1;
                                docReqinfo.FDRI_FORM_E_NO = docInfo.FDI_FORM_E_NO;
                                docReqinfo.FDRI_ENTRY_DATETIME = DateTime.Now.AddHours(5);
                                docReqinfo.FDRI_MAKER_ID = docInfo.FDI_MAKER_ID;
                                docReqinfo.FDRI_AMENDMENT_NO = 0;

                                PSW_PAYMENT_MODES_PARAMETERS Param = new PSW_PAYMENT_MODES_PARAMETERS();
                                Param.EIF_PMP_ID = Convert.ToInt32(cont.PSW_PAYMENT_MODES_PARAMETERS.Max(m => (decimal?)m.EIF_PMP_ID)) + 1;
                                Param.EIF_PMP_EIF_REQUEST_NO = docInfo.FDI_FORM_E_NO;
                                Param.EIF_PMP_ENTRY_DATETIME = docInfo.FDI_ENTRY_DATETIME;
                                Param.EIF_PMP_MAKER_ID = docInfo.FDI_MAKER_ID;
                                Param.EIF_PMP_AMENDMENT_NO = 0;
                                LoadEEF.DocInfo = docInfo;
                                LoadEEF.DocRequestNo = docReqinfo;
                                LoadEEF.PayParam = Param;
                                LoadEEF.CustomNew = true;
                                return View(LoadEEF);

                            }
                        }
                        catch (Exception ex)
                        {
                            message = ex.Message;
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("EEFRequest", "ManageEEF", Session["USER_ID"].ToString(), ex);
                    //message = ex.Message;
                }
                finally
                {
                    List<SelectListItem> PayMode = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 2 && m.APM_ISAUTH == true).Select(f => new
                    {
                        f.APM_ID,
                        APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                    }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                    PayMode.Insert(0, (new SelectListItem { Text = "--Select Mode of Export Payment--", Value = "0" }));
                    LoadEEF.AuthPayModes = PayMode;
                    int?[] ClientIds = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(x => x.APT_PROFILE_ID == 1).Select(x => x.APT_CLIENT_ID).ToArray();
                    List<SelectListItem> Customers = new SelectList(context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_STATUS == true && x.C_ISAUTH == true && x.C_ACCOUNT_STATUS_ID == 1 && ClientIds.Contains(x.C_ID)).Select(f => new
                    {
                        f.C_IBAN_NO,
                        C_NAME = f.C_NAME + " -- (" + f.C_IBAN_NO + ")"
                    }).ToList(), "C_IBAN_NO", "C_NAME", 0).ToList();
                    Customers.Insert(0, (new SelectListItem { Text = "--Select Trader Name For Export --", Value = "0" }));
                    LoadEEF.ListCustomers = Customers;
                    ViewBag.message = message;
                }
                return View();
            }
            else
            {
                return RedirectToAction("UnAuthorizedUrl", "Error");
            }
        }

        [HttpPost]
        public ActionResult ManageEEF(EEF eef)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    EEF loadInfo = eef;
                    string message = "";
                    try
                    {
                        int rowcount = 0;
                        using (PSWEntities cont = new PSWEntities())
                        {
                            using (var transaction = cont.Database.BeginTransaction())
                            {
                                try
                                {
                                    if (eef.DocInfo.FDI_FORM_E_NO != null)
                                    {
                                        List<PSW_EEF_HS_CODE> checkHsCode = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == eef.DocInfo.FDI_FORM_E_NO).ToList();
                                        if (checkHsCode.Count() == 0)
                                        {
                                            message = "No Hs Code Found in the FI (Export)";
                                            return View(eef);
                                        }
                                    }
                                    else
                                    {
                                        message = "No Hs Code Found in the FI (Export)";
                                        return View(eef);
                                    }
                                    PSW_FORM_E_DOCUMENT_INFO docInfo = cont.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == eef.DocInfo.FDI_FORM_E_NO && m.FDI_AMENDMENT_NO == eef.DocInfo.FDI_AMENDMENT_NO).FirstOrDefault();
                                    int ClientId = 0;
                                    if (eef.DocInfo.FDI_TRADER_NAME != "0")
                                    {
                                        ClientId = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eef.DocInfo.FDI_TRADER_NAME).FirstOrDefault().C_ID;
                                    }
                                    else
                                    {
                                        message = "Invalid Client";
                                        return View(eef);
                                    }
                                    PSW_CLIENT_INFO client = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientId).FirstOrDefault();
                                    PSW_FORM_E_DOCUMENT_REQUEST_INFO docreqinfo = cont.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == eef.DocInfo.FDI_FORM_E_NO && m.FDRI_AMENDMENT_NO == eef.DocInfo.FDI_AMENDMENT_NO).FirstOrDefault();
                                    PSW_AUTH_PAY_MODES PayMode = null;
                                    PSW_PAYMENT_MODES_PARAMETERS PayParam = null;
                                    if (eef.DocInfo.FDI_MODE_OF_EXPORT_PAYMENT != 0)
                                    {
                                        PayMode = new PSW_AUTH_PAY_MODES();
                                        PayParam = new PSW_PAYMENT_MODES_PARAMETERS();
                                        PayMode = cont.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eef.DocInfo.FDI_MODE_OF_EXPORT_PAYMENT).FirstOrDefault();
                                        PayParam = cont.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == eef.DocInfo.FDI_FORM_E_NO).FirstOrDefault();
                                    }
                                    else
                                    {
                                        message = "Invalid Mode of Payment";
                                        return View(eef);
                                    }
                                    if (docInfo == null)
                                    {
                                        docInfo = new PSW_FORM_E_DOCUMENT_INFO();
                                        docInfo.FDI_ID = Convert.ToInt32(cont.PSW_FORM_E_DOCUMENT_INFO.Max(m => (decimal?)m.FDI_ID)) + 1;
                                        docInfo.FDI_FORM_E_NO = eef.DocInfo.FDI_FORM_E_NO;
                                        docInfo.FDI_INTERNAL_REF = eef.DocInfo.FDI_INTERNAL_REF;
                                        docInfo.FDI_STATUS = "Initiated";
                                        docInfo.FDI_STATUS_DATE = DateTime.Now.AddHours(5);
                                        docInfo.FDI_MODE_OF_EXPORT_PAYMENT = eef.DocInfo.FDI_MODE_OF_EXPORT_PAYMENT;
                                        docInfo.FDI_PHASE_OF_ISSUE = eef.DocInfo.FDI_PHASE_OF_ISSUE;
                                        docInfo.FDI_ISSUE_BANK = eef.DocInfo.FDI_ISSUE_BANK;
                                        docInfo.FDI_NTN = client.C_NTN_NO;
                                        docInfo.FDI_TRADER_NAME = eef.DocInfo.FDI_TRADER_NAME;
                                        docInfo.FDI_TRADER_ADDRESS = eef.DocInfo.FDI_TRADER_ADDRESS;
                                        docInfo.FDI_ENTRY_DATETIME = DateTime.Now.AddHours(5);
                                        docInfo.FDI_MAKER_ID = Session["USER_ID"].ToString();
                                        if (docInfo.FDI_MAKER_ID == "Admin123")
                                            docInfo.FDI_CHECKER_ID = docInfo.FDI_MAKER_ID;
                                        else
                                            docInfo.FDI_CHECKER_ID = null;
                                        docInfo.FDI_REMARKS = eef.DocInfo.FDI_REMARKS;
                                        docInfo.FDI_LC_CONTRACT_NO = eef.DocInfo.FDI_LC_CONTRACT_NO;
                                        docInfo.FDI_AMENDMENT_NO = 0;
                                        docInfo.FDI_DOC_SUBMISSION = eef.DocInfo.FDI_DOC_SUBMISSION;
                                        docInfo.FDI_CERTIFIED = eef.DocInfo.FDI_CERTIFIED;
                                        if (eef.DocInfo.FDI_CERTIFIED == true)
                                            docInfo.FDI_CERTIFICATION_DATE = DateTime.Now;
                                        cont.PSW_FORM_E_DOCUMENT_INFO.Add(docInfo);
                                        rowcount += cont.SaveChanges();
                                    }
                                    else
                                    {
                                        DateTime EEFEntryDate = DateTime.Now.AddHours(5);
                                        if (docInfo.FDI_ENTRY_DATETIME != null)
                                            EEFEntryDate = (DateTime)docInfo.FDI_ENTRY_DATETIME;
                                        if (eef.CustomNew)
                                        {
                                            message = "EEF Request No Already Exist Now, Please Try to Initiate a new EEF";
                                            return View(eef);
                                        }
                                        docInfo = new PSW_FORM_E_DOCUMENT_INFO();
                                        docInfo.FDI_ID = Convert.ToInt32(cont.PSW_FORM_E_DOCUMENT_INFO.Max(m => (decimal?)m.FDI_ID)) + 1;
                                        docInfo.FDI_FORM_E_NO = eef.DocInfo.FDI_FORM_E_NO;
                                        docInfo.FDI_INTERNAL_REF = eef.DocInfo.FDI_INTERNAL_REF;
                                        docInfo.FDI_STATUS = "Initiated";
                                        docInfo.FDI_STATUS_DATE = DateTime.Now.AddHours(5);
                                        docInfo.FDI_MODE_OF_EXPORT_PAYMENT = eef.DocInfo.FDI_MODE_OF_EXPORT_PAYMENT;
                                        docInfo.FDI_PHASE_OF_ISSUE = eef.DocInfo.FDI_PHASE_OF_ISSUE;
                                        docInfo.FDI_ISSUE_BANK = eef.DocInfo.FDI_ISSUE_BANK;
                                        docInfo.FDI_NTN = client.C_NTN_NO;
                                        docInfo.FDI_TRADER_NAME = eef.DocInfo.FDI_TRADER_NAME;
                                        docInfo.FDI_TRADER_ADDRESS = eef.DocInfo.FDI_TRADER_ADDRESS;
                                        docInfo.FDI_ENTRY_DATETIME = EEFEntryDate;
                                        docInfo.FDI_EDIT_DATETIME = DateTime.Now.AddHours(5);
                                        docInfo.FDI_MAKER_ID = Session["USER_ID"].ToString();
                                        if (docInfo.FDI_MAKER_ID == "Admin123")
                                            docInfo.FDI_CHECKER_ID = docInfo.FDI_MAKER_ID;
                                        else
                                            docInfo.FDI_CHECKER_ID = null;
                                        docInfo.FDI_REMARKS = eef.DocInfo.FDI_REMARKS;
                                        docInfo.FDI_LC_CONTRACT_NO = eef.DocInfo.FDI_LC_CONTRACT_NO;
                                        docInfo.FDI_AMENDMENT_NO = Convert.ToInt32(cont.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == eef.DocInfo.FDI_FORM_E_NO).Max(m => m.FDI_AMENDMENT_NO)) + 1;
                                        docInfo.FDI_DOC_SUBMISSION = eef.DocInfo.FDI_DOC_SUBMISSION;
                                        docInfo.FDI_CERTIFIED = eef.DocInfo.FDI_CERTIFIED;
                                        if (eef.DocInfo.FDI_CERTIFIED == true)
                                            docInfo.FDI_CERTIFICATION_DATE = DateTime.Now;
                                        cont.PSW_FORM_E_DOCUMENT_INFO.Add(docInfo);
                                        rowcount += cont.SaveChanges();
                                    }

                                    if (PayParam == null)
                                    {
                                        PayParam = new PSW_PAYMENT_MODES_PARAMETERS();
                                        PayParam.EIF_PMP_ID = Convert.ToInt32(cont.PSW_PAYMENT_MODES_PARAMETERS.Max(m => (decimal?)m.EIF_PMP_ID)) + 1;
                                        PayParam.EIF_PMP_EIF_REQUEST_NO = eef.DocInfo.FDI_FORM_E_NO;
                                        switch (PayMode.APM_CODE.ToString())
                                        {
                                            case "305":
                                                PayParam.EIF_PMP_COMMISION_CHARGES = eef.PayParam.EIF_PMP_COMMISION_CHARGES;
                                                PayParam.EIF_PMP_FED_CHARGES = eef.PayParam.EIF_PMP_FED_CHARGES;
                                                PayParam.EIF_PMP_GD_NO = eef.PayParam.EIF_PMP_GD_NO;
                                                PSW_GD_ASSIGN_EFORMS a = cont.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_GD_NO == eef.PayParam.EIF_PMP_GD_NO && (m.GDAEF_E_FORM == "" || m.GDAEF_E_FORM == null)).OrderByDescending(m => m.GDAEF_AMENDMENT_NO).FirstOrDefault();
                                                if (a != null)
                                                {
                                                    a.GDAEF_E_FORM = eef.DocInfo.FDI_FORM_E_NO;
                                                    cont.Entry(a).State = EntityState.Modified;
                                                    rowcount += cont.SaveChanges();
                                                }
                                                else
                                                {
                                                    message = "Invalid GD Number";
                                                    break;
                                                }
                                                break;
                                            case "307": /// LC 
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = eef.PayParam.EIF_PMP_ADV_PAYPERCENT == null ? 0 : eef.PayParam.EIF_PMP_ADV_PAYPERCENT;
                                                PayParam.EIF_PMP_SIGHT_PERCENT = eef.PayParam.EIF_PMP_SIGHT_PERCENT == null ? 0 : eef.PayParam.EIF_PMP_SIGHT_PERCENT;
                                                PayParam.EIF_PMP_USANCE_PERCENT = eef.PayParam.EIF_PMP_USANCE_PERCENT == null ? 0 : eef.PayParam.EIF_PMP_USANCE_PERCENT;
                                                PayParam.EIF_PMP_DAYS = eef.PayParam.EIF_PMP_DAYS == null ? 0 : eef.PayParam.EIF_PMP_DAYS;
                                                PayParam.EIF_PMP_PERCENTAGE = eef.PayParam.EIF_PMP_PERCENTAGE == null ? 0 : eef.PayParam.EIF_PMP_PERCENTAGE;
                                                break;
                                            case "308": /// Contract Collection
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = eef.PayParam.EIF_PMP_ADV_PAYPERCENT == null ? 0 : eef.PayParam.EIF_PMP_ADV_PAYPERCENT;
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = eef.PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT == null ? 0 : eef.PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT;
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = eef.PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT == null ? 0 : eef.PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT;
                                                PayParam.EIF_PMP_DAYS = eef.PayParam.EIF_PMP_DAYS == null ? 0 : eef.PayParam.EIF_PMP_DAYS;
                                                PayParam.EIF_PMP_PERCENTAGE = eef.PayParam.EIF_PMP_PERCENTAGE == null ? 0 : eef.PayParam.EIF_PMP_PERCENTAGE;
                                                break;
                                            default:
                                                break;
                                        }
                                        PayParam.EIF_PMP_COMMISION_CHARGES = eef.PayParam.EIF_PMP_COMMISION_CHARGES;
                                        PayParam.EIF_PMP_FED_CHARGES = eef.PayParam.EIF_PMP_FED_CHARGES;
                                        PayParam.EIF_PMP_ENTRY_DATETIME = eef.DocInfo.FDI_ENTRY_DATETIME;
                                        PayParam.EIF_PMP_MAKER_ID = eef.DocInfo.FDI_MAKER_ID;
                                        PayParam.EIF_PMP_AMENDMENT_NO = 0;
                                        cont.PSW_PAYMENT_MODES_PARAMETERS.Add(PayParam);
                                        rowcount += cont.SaveChanges();
                                    }
                                    else
                                    {
                                        PayParam = new PSW_PAYMENT_MODES_PARAMETERS();
                                        PayParam.EIF_PMP_ID = Convert.ToInt32(cont.PSW_PAYMENT_MODES_PARAMETERS.Max(m => (decimal?)m.EIF_PMP_ID)) + 1;
                                        PayParam.EIF_PMP_EIF_REQUEST_NO = eef.DocInfo.FDI_FORM_E_NO;
                                        switch (PayMode.APM_CODE.ToString())
                                        {
                                            case "305":
                                                PayParam.EIF_PMP_COMMISION_CHARGES = eef.PayParam.EIF_PMP_COMMISION_CHARGES;
                                                PayParam.EIF_PMP_FED_CHARGES = eef.PayParam.EIF_PMP_FED_CHARGES;
                                                PayParam.EIF_PMP_GD_NO = eef.PayParam.EIF_PMP_GD_NO;
                                                PSW_GD_ASSIGN_EFORMS a = cont.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_GD_NO == eef.PayParam.EIF_PMP_GD_NO && (m.GDAEF_E_FORM == "" || m.GDAEF_E_FORM == null)).OrderByDescending(m => m.GDAEF_AMENDMENT_NO).FirstOrDefault();
                                                if (a != null)
                                                {
                                                    a.GDAEF_E_FORM = eef.DocInfo.FDI_FORM_E_NO;
                                                    cont.Entry(a).State = EntityState.Modified;
                                                    rowcount += cont.SaveChanges();
                                                }
                                                else
                                                {
                                                    message = "Invalid GD Number";
                                                    break;
                                                }
                                                break;
                                            case "307": /// LC 
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = eef.PayParam.EIF_PMP_ADV_PAYPERCENT == null ? 0 : eef.PayParam.EIF_PMP_ADV_PAYPERCENT;
                                                PayParam.EIF_PMP_SIGHT_PERCENT = eef.PayParam.EIF_PMP_SIGHT_PERCENT == null ? 0 : eef.PayParam.EIF_PMP_SIGHT_PERCENT;
                                                PayParam.EIF_PMP_USANCE_PERCENT = eef.PayParam.EIF_PMP_USANCE_PERCENT == null ? 0 : eef.PayParam.EIF_PMP_USANCE_PERCENT;
                                                PayParam.EIF_PMP_DAYS = eef.PayParam.EIF_PMP_DAYS == null ? 0 : eef.PayParam.EIF_PMP_DAYS;
                                                PayParam.EIF_PMP_PERCENTAGE = eef.PayParam.EIF_PMP_PERCENTAGE == null ? 0 : eef.PayParam.EIF_PMP_PERCENTAGE;
                                                break;
                                            case "308": /// Contract Collection
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = eef.PayParam.EIF_PMP_ADV_PAYPERCENT == null ? 0 : eef.PayParam.EIF_PMP_ADV_PAYPERCENT;
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = eef.PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT == null ? 0 : eef.PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT;
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = eef.PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT == null ? 0 : eef.PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT;
                                                PayParam.EIF_PMP_DAYS = eef.PayParam.EIF_PMP_DAYS == null ? 0 : eef.PayParam.EIF_PMP_DAYS;
                                                PayParam.EIF_PMP_PERCENTAGE = eef.PayParam.EIF_PMP_PERCENTAGE == null ? 0 : eef.PayParam.EIF_PMP_PERCENTAGE;
                                                break;
                                            default:
                                                break;
                                        }
                                        PayParam.EIF_PMP_COMMISION_CHARGES = eef.PayParam.EIF_PMP_COMMISION_CHARGES;
                                        PayParam.EIF_PMP_FED_CHARGES = eef.PayParam.EIF_PMP_FED_CHARGES;
                                        PayParam.EIF_PMP_MAKER_ID = Session["USER_ID"].ToString();
                                        if (PayParam.EIF_PMP_MAKER_ID == "Admin123")
                                            PayParam.EIF_PMP_CHECKER_ID = PayParam.EIF_PMP_MAKER_ID;
                                        else
                                            PayParam.EIF_PMP_CHECKER_ID = null;
                                        PayParam.EIF_PMP_AMENDMENT_NO = docInfo.FDI_AMENDMENT_NO;
                                        PayParam.EIF_PMP_ENTRY_DATETIME = DateTime.Now;
                                        cont.PSW_PAYMENT_MODES_PARAMETERS.Add(PayParam);
                                        rowcount += cont.SaveChanges();
                                    }

                                    if (docreqinfo == null)
                                    {
                                        docreqinfo = new PSW_FORM_E_DOCUMENT_REQUEST_INFO();
                                        docreqinfo.FDRI_ID = Convert.ToInt32(cont.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Max(m => (decimal?)m.FDRI_ID)) + 1;
                                        docreqinfo.FDRI_FORM_E_NO = docInfo.FDI_FORM_E_NO;
                                        docreqinfo.FDRI_CONSIG_NAME = eef.DocRequestNo.FDRI_CONSIG_NAME;
                                        docreqinfo.FDRI_CONSID_ADDRESS = eef.DocRequestNo.FDRI_CONSID_ADDRESS;
                                        docreqinfo.FDRI_CONSIGNEE_IBAN = eef.DocRequestNo.FDRI_CONSIGNEE_IBAN;
                                        docreqinfo.FDRI_COUNTRY = eef.DocRequestNo.FDRI_COUNTRY;
                                        docreqinfo.FDRI_PORT_OF_DISCHARGE = eef.DocRequestNo.FDRI_PORT_OF_DISCHARGE;
                                        docreqinfo.FDRI_CURRENCY = eef.DocRequestNo.FDRI_CURRENCY;
                                        docreqinfo.FDRI_DELIVERY_TERMS = eef.DocRequestNo.FDRI_DELIVERY_TERMS;
                                        docreqinfo.FDRI_TOTAL_VALUE_OF_SHIPMENT = eef.DocRequestNo.FDRI_TOTAL_VALUE_OF_SHIPMENT;
                                        docreqinfo.FDRI_FINAN_EXPIRY = eef.DocRequestNo.FDRI_FINAN_EXPIRY;
                                        docreqinfo.FDRI_ENTRY_DATETIME = DateTime.Now;
                                        docreqinfo.FDRI_MAKER_ID = Session["USER_ID"].ToString();
                                        if (docreqinfo.FDRI_MAKER_ID == "Admin123")
                                            docreqinfo.FDRI_CHECKER_ID = docreqinfo.FDRI_MAKER_ID;
                                        else
                                            docreqinfo.FDRI_CHECKER_ID = null;
                                        cont.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Add(docreqinfo);
                                        rowcount += cont.SaveChanges();
                                        if (rowcount > 0)
                                        {
                                            PSW_EXPORT_CONSIGNEE_LIST Entity = new PSW_EXPORT_CONSIGNEE_LIST();
                                            Entity.ECL_ID = Convert.ToInt32(context.PSW_EXPORT_CONSIGNEE_LIST.Max(m => (decimal?)m.ECL_ID)) + 1;
                                            Entity.ECL_FI_NUMBER = docInfo.FDI_FORM_E_NO;
                                            Entity.ECL_CONSIGNEE_NAME = docreqinfo.FDRI_CONSIG_NAME;
                                            Entity.ECL_MAKER_ID = Session["USER_ID"].ToString();
                                            Entity.ECL_IS_SHARED = false;
                                            Entity.ECL_ISAUTH = false;
                                            Entity.ECL_ENTRY_DATETIME = DateTime.Now;
                                            Entity.ECL_EDIT_DATETIME = null;
                                            context.PSW_EXPORT_CONSIGNEE_LIST.Add(Entity);
                                            rowcount += context.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        docreqinfo = new PSW_FORM_E_DOCUMENT_REQUEST_INFO();
                                        docreqinfo.FDRI_ID = Convert.ToInt32(cont.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Max(m => (decimal?)m.FDRI_ID)) + 1;
                                        docreqinfo.FDRI_FORM_E_NO = docInfo.FDI_FORM_E_NO;
                                        docreqinfo.FDRI_CONSIG_NAME = eef.DocRequestNo.FDRI_CONSIG_NAME;
                                        docreqinfo.FDRI_CONSID_ADDRESS = eef.DocRequestNo.FDRI_CONSID_ADDRESS;
                                        docreqinfo.FDRI_CONSIGNEE_IBAN = eef.DocRequestNo.FDRI_CONSIGNEE_IBAN;
                                        docreqinfo.FDRI_COUNTRY = eef.DocRequestNo.FDRI_COUNTRY;
                                        docreqinfo.FDRI_PORT_OF_DISCHARGE = eef.DocRequestNo.FDRI_PORT_OF_DISCHARGE;
                                        docreqinfo.FDRI_CURRENCY = eef.DocRequestNo.FDRI_CURRENCY;
                                        docreqinfo.FDRI_DELIVERY_TERMS = eef.DocRequestNo.FDRI_DELIVERY_TERMS;
                                        docreqinfo.FDRI_TOTAL_VALUE_OF_SHIPMENT = eef.DocRequestNo.FDRI_TOTAL_VALUE_OF_SHIPMENT;
                                        docreqinfo.FDRI_FINAN_EXPIRY = eef.DocRequestNo.FDRI_FINAN_EXPIRY;
                                        docreqinfo.FDRI_ENTRY_DATETIME = DateTime.Now;
                                        docreqinfo.FDRI_MAKER_ID = Session["USER_ID"].ToString();
                                        if (docreqinfo.FDRI_MAKER_ID == "Admin123")
                                            docreqinfo.FDRI_CHECKER_ID = docreqinfo.FDRI_MAKER_ID;
                                        else
                                            docreqinfo.FDRI_CHECKER_ID = null;
                                        docreqinfo.FDRI_AMENDMENT_NO = docInfo.FDI_AMENDMENT_NO;
                                        cont.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Add(docreqinfo);
                                        rowcount += cont.SaveChanges();
                                        if (rowcount > 0)
                                        {
                                            PSW_EXPORT_CONSIGNEE_LIST Entity = new PSW_EXPORT_CONSIGNEE_LIST();
                                            Entity = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_FI_NUMBER == docInfo.FDI_FORM_E_NO && m.ECL_CONSIGNEE_NAME == docreqinfo.FDRI_CONSIG_NAME).FirstOrDefault();
                                            if (Entity == null)
                                            {
                                                Entity = new PSW_EXPORT_CONSIGNEE_LIST();
                                                Entity.ECL_ID = Convert.ToInt32(context.PSW_EXPORT_CONSIGNEE_LIST.Max(m => (decimal?)m.ECL_ID)) + 1;
                                                Entity.ECL_FI_NUMBER = docInfo.FDI_FORM_E_NO;
                                                Entity.ECL_CONSIGNEE_NAME = docreqinfo.FDRI_CONSIG_NAME;
                                                Entity.ECL_MAKER_ID = Session["USER_ID"].ToString();
                                                Entity.ECL_IS_SHARED = false;
                                                Entity.ECL_ISAUTH = false;
                                                Entity.ECL_ENTRY_DATETIME = DateTime.Now;
                                                Entity.ECL_EDIT_DATETIME = null;
                                                context.PSW_EXPORT_CONSIGNEE_LIST.Add(Entity);
                                                rowcount += context.SaveChanges();
                                            }
                                            else
                                            {
                                                Entity.ECL_FI_NUMBER = docInfo.FDI_FORM_E_NO;
                                                Entity.ECL_CONSIGNEE_NAME = docreqinfo.FDRI_CONSIG_NAME;
                                                Entity.ECL_MAKER_ID = Session["USER_ID"].ToString();
                                                Entity.ECL_IS_SHARED = false;
                                                Entity.ECL_ISAUTH = false;
                                                Entity.ECL_EDIT_DATETIME = DateTime.Now;
                                                context.Entry(Entity).State = EntityState.Modified;
                                                rowcount += context.SaveChanges();
                                            }
                                        }
                                    }
                                    transaction.Commit();

                                    message = "Transaction Saved Successfully";

                                }
                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    rowcount = 0;
                                    message += "Exception Occured " + ex.Message + Environment.NewLine;
                                    if (ex.InnerException != null)
                                    {
                                        message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                                        if (ex.InnerException.InnerException != null)
                                        {
                                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "ManageEEF", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> PayMode = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 2 && m.APM_ISAUTH == true).Select(f => new
                        {
                            f.APM_ID,
                            APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                        }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                        PayMode.Insert(0, (new SelectListItem { Text = "--Select Mode of Export Payment--", Value = "0" }));
                        eef.AuthPayModes = PayMode;
                        int?[] ClientIds = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(x => x.APT_PROFILE_ID == 1).Select(x => x.APT_CLIENT_ID).ToArray();
                        List<SelectListItem> Customers = new SelectList(context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_STATUS == true && x.C_ISAUTH == true && x.C_ACCOUNT_STATUS_ID == 1 && ClientIds.Contains(x.C_ID)).Select(f => new
                        {
                            f.C_IBAN_NO,
                            C_NAME = f.C_NAME + " -- (" + f.C_IBAN_NO + ")"
                        }).ToList(), "C_IBAN_NO", "C_NAME", 0).ToList();
                        Customers.Insert(0, (new SelectListItem { Text = "--Select Trader Name For Export --", Value = "0" }));
                        eef.ListCustomers = Customers;
                        ViewBag.message = message;
                    }
                    return View(loadInfo);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [HttpPost]
        public JsonResult GenerateNewEEFReqNo()
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string eef_no = "";
                    string ir_no = "";
                    int MAXID = Convert.ToInt32(context.PSW_FORM_E_DOCUMENT_INFO.Max(m => (decimal?)m.FDI_ID)) + 1;
                    eef_no = "CBN-EXP-" + String.Format("{0:000000}", MAXID) + "-" + DateTime.Now.ToString("ddMMyyyy");
                    ir_no = "CITI-EXP-" + String.Format("{0:000000}", MAXID) + "-" + DateTime.Now.ToString("ddMMyyyy");
                    PSW_EEF_HS_CODE CheckEifNo = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == eef_no).FirstOrDefault();
                    if (CheckEifNo != null)
                    {
                        int Iteration = 1;
                        while (CheckEifNo != null)
                        {
                            int FinalMaxId = MAXID + Iteration;
                            eef_no = "CBN-EXP-" + String.Format("{0:000000}", FinalMaxId) + "-" + DateTime.Now.ToString("ddMMyyyy");
                            ir_no = "CITI-EXP-" + String.Format("{0:000000}", FinalMaxId) + "-" + DateTime.Now.ToString("ddMMyyyy");
                            CheckEifNo = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == eef_no).FirstOrDefault();
                            Iteration += 1;
                            if (CheckEifNo == null)
                            {
                                var coverletters = new
                                {
                                    EEF_NO = eef_no,
                                    IR_NO = ir_no
                                };
                                return Json(coverletters, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    var coverletter = new
                    {
                        EEF_NO = eef_no,
                        IR_NO = ir_no
                    };
                    return Json(coverletter, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
                return Json("Invalid Request");
        }
        public ActionResult GetEEFViewMode(string E = "", string M = "", int AM = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        EEF eefInfo = new EEF();
                        if (E != "")
                        {
                            eefInfo.DocInfo = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == AM).FirstOrDefault();
                            eefInfo.PayMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eefInfo.DocInfo.FDI_MODE_OF_EXPORT_PAYMENT).FirstOrDefault();
                            eefInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == AM).FirstOrDefault();
                            if (eefInfo.DocInfo == null)
                            {
                                message += "No EEF Document Information Found on Form E # " + E + Environment.NewLine;
                            }
                            else
                            {
                                if (eefInfo.DocInfo.FDI_TRADER_NAME != null)
                                {
                                    eefInfo.DocInfo.FDI_TRADER_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eefInfo.DocInfo.FDI_TRADER_NAME).FirstOrDefault().C_NAME;
                                }
                                else
                                {
                                    message += "Invalid Trader Name";
                                }
                            }
                            eefInfo.DocRequestNo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == E && m.FDRI_AMENDMENT_NO == AM).FirstOrDefault();
                            if (eefInfo.DocRequestNo == null)
                            {
                                message += "No EEF Documents Request Information Found In The System For the EEF # " + E + Environment.NewLine;
                            }
                            else
                            {
                                if (eefInfo.DocRequestNo.FDRI_COUNTRY != null)
                                    eefInfo.DOC_REQ_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eefInfo.DocRequestNo.FDRI_COUNTRY).FirstOrDefault();
                                else
                                    message += "Invalid Country";
                                if (eefInfo.DocRequestNo.FDRI_CURRENCY != null)
                                    eefInfo.CCY = context.PSW_CURRENCY.Where(m => m.CUR_ID == eefInfo.DocRequestNo.FDRI_CURRENCY).FirstOrDefault();
                                else
                                    message += "Invalid Currency";
                                if (eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS != null)
                                    eefInfo.DELIVERY_TERM = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS).FirstOrDefault();
                                else
                                    message += "Invalid Delivery Term";
                                if (eefInfo.DocRequestNo.FDRI_PORT_OF_DISCHARGE != null)
                                    eefInfo.ShippingPort = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_CODE == eefInfo.DocRequestNo.FDRI_PORT_OF_DISCHARGE).FirstOrDefault();
                                else
                                    message += "Invalid Port";
                            }
                            eefInfo.hscodes = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == E && m.EEF_HC_AMENDMENT_NO == AM).ToList();
                            if (eefInfo.hscodes == null && eefInfo.hscodes.Count() <= 0)
                            {
                                message += "No Goods Declaration Found In The System For the EEF # " + E + Environment.NewLine;
                            }
                            eefInfo.EEFGD = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_FORM_E_NO.Contains(E)).ToList();
                            if (eefInfo.EEFGD == null && eefInfo.EEFGD.Count() <= 0)
                            {
                                message += "No EEF GD(Goods Declaration) Found In The System For the EEF # " + E + Environment.NewLine;
                            }
                            eefInfo.BCAList = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FORM_E_NO == E).ToList();
                            if (eefInfo.BCAList == null && eefInfo.BCAList.Count() <= 0)
                            {
                                message += "No BCA Found In The System For the EEF # " + E + Environment.NewLine;
                            }
                            return View(eefInfo);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                        ViewBag.SelectedOption = M;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public ActionResult AproveEEF(string E = "", int M = 0, int ASN = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != "")
                        {
                            EEF eefInfo = new EEF();
                            PSW_FORM_E_DOCUMENT_INFO basicInfoUpdate = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == M).FirstOrDefault();
                            if (basicInfoUpdate != null)
                            {
                                eefInfo.DocInfo = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == M).FirstOrDefault();
                                eefInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();
                                if (eefInfo.DocInfo == null)
                                {
                                    message += "No EEF Document Information Found on Form E # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eefInfo.DocInfo.FDI_TRADER_NAME != null)
                                    {
                                        eefInfo.DocInfo.FDI_TRADER_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eefInfo.DocInfo.FDI_TRADER_NAME).FirstOrDefault().C_IBAN_NO;
                                    }
                                    else
                                    {
                                        message += "Invalid Trader Name";
                                    }
                                }
                                eefInfo.DocRequestNo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == E && m.FDRI_AMENDMENT_NO == M).FirstOrDefault();
                                if (eefInfo.DocRequestNo == null)
                                {
                                    message += "No EEF Documents Request Information Found In The System For the EEF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eefInfo.DocRequestNo.FDRI_COUNTRY != null)
                                        eefInfo.DOC_REQ_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eefInfo.DocRequestNo.FDRI_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Country";
                                    if (eefInfo.DocRequestNo.FDRI_CURRENCY != null)
                                        eefInfo.CCY = context.PSW_CURRENCY.Where(m => m.CUR_ID == eefInfo.DocRequestNo.FDRI_CURRENCY).FirstOrDefault();
                                    else
                                        message += "Invalid Currency";
                                    if (eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS != null)
                                        eefInfo.DELIVERY_TERM = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS).FirstOrDefault();
                                    else
                                        message += "Invalid Delivery Term";
                                }
                                eefInfo.hscodes = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == E && m.EEF_HC_AMENDMENT_NO == M).ToList();
                                if (eefInfo.hscodes == null && eefInfo.hscodes.Count() <= 0)
                                {
                                    message += "No Goods Declaration Found In The System For the EEF # " + E + Environment.NewLine;
                                }
                                if (DAL.DO_EDI == "true")
                                {
                                    message += DAL.MethodId1524(E, basicInfoUpdate.FDI_AMENDMENT_NO, ASN) + Environment.NewLine;
                                    //message += "  RAW REQUEST  DETAILS FROM OUR SIDE  -------------------- " + Environment.NewLine;
                                    //message += DAL.message;
                                    if (message.Contains("200"))
                                    {
                                        basicInfoUpdate.FDI_STATUS = "APPROVED";
                                        basicInfoUpdate.FDI_STATUS_DATE = DateTime.Now;
                                        basicInfoUpdate.FDI_CHECKER_ID = Session["USER_ID"].ToString();
                                        context.Entry(basicInfoUpdate).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message += "EEF Approved Successfully." + Environment.NewLine;
                                            message += EMAILING.EEF_Approve(E);
                                        }
                                        else
                                            message += "Problem While Updating The Status.";
                                    }
                                }
                                else
                                    message += "EDI is turned off from system configuration";
                                return View("GetEEFViewMode", eefInfo);
                            }
                            else
                            {
                                message = " Error While Aproving the EEF # " + E;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "AporveEEF", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("GetEEFViewMode");
        }

        public ActionResult AproveBCA(int BA = 0, int ASN = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "BCA", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string FINo = "";
                    try
                    {
                        if (BA != 0)
                        {
                            PSW_BCA_INFO BCA = context.PSW_BCA_INFO.Where(m => m.BCAD_ID == BA).FirstOrDefault();
                            if (BCA != null)
                            {
                                if (DAL.DO_EDI == "true")
                                {
                                    message += DAL.MethodId1526(BA, ASN) + Environment.NewLine;
                                    //message += "  RAW REQUEST  DETAILS FROM OUR SIDE  -------------------- " + Environment.NewLine;
                                    //message += DAL.message;
                                    if (message.Contains("200"))
                                    {
                                        FINo = BCA.BCAD_FORM_E_NO;
                                        BCA.BCAD_STATUS = "APPROVED";
                                        BCA.BCAD_EDIT_DATETIME = DateTime.Now;
                                        BCA.BCAD_CHECKER_ID = Session["USER_ID"].ToString();
                                        context.Entry(BCA).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message += "BCA Approved Successfully." + Environment.NewLine;
                                            message += EMAILING.EEF_BCA_Approve(BA);
                                        }
                                        else
                                            message += "Problem While Updating The Status.";
                                    }
                                }
                                else
                                    message += "EDI is turned off from system configuration";
                                return View("BCA", BCA);
                            }
                            else
                            {
                                message = " Error While Aproving the BCA ";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "AproveBCA", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                        return View("BCA");
                    }
                    finally
                    {
                        List<SelectListItem> EefRequests = new SelectList(context.PSW_EEF_MASTER_DETAILS.ToList(), "FDI_FORM_E_NO", "FDI_FORM_E_NO", 0).ToList();
                        EefRequests.Insert(0, (new SelectListItem { Text = "--Select Request --", Value = "0" }));
                        ViewBag.BCAD_FORM_E_NO = EefRequests;
                        List<SelectListItem> Currency = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        Currency.Insert(0, (new SelectListItem { Text = "--Select Currency--", Value = "0" }));
                        ViewBag.BCAD_CURRENCY = Currency;
                        List<SelectListItem> BcaEvent = new SelectList(context.PSW_BCA_EVENT.ToList(), "BCAE_ID", "BCAE_NAME", 0).ToList();
                        BcaEvent.Insert(0, (new SelectListItem { Text = "--Select Event--", Value = "0" }));
                        ViewBag.BCAD_EVENTID = BcaEvent;
                        List<SelectListItem> PaymentCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_STATUS == true && m.LC_ISAUTH == true).ToList(), "LC_NAME", "LC_NAME", 0).ToList();
                        PaymentCountry.Insert(0, (new SelectListItem { Text = "--Select Payment Country--", Value = "0" }));
                        ViewBag.BCAD_PAYMENT_COUNTRY = PaymentCountry;
                        if (FINo != "")
                        {
                            List<string> GDNos = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM == FINo).Select(m => m.GDAEF_GD_NO).ToList();
                            List<SelectListItem> BcaGD = new SelectList(context.PSW_EEF_GD_GENERAL_INFO.Where(m => GDNos.Contains(m.EFGDGI_GD_NO)).ToList(), "EFGDGI_GD_NO", "EFGDGI_GD_NO", 0).ToList();
                            BcaGD.Insert(0, (new SelectListItem { Text = "--Select GD No--", Value = "0" }));
                            ViewBag.BCAD_GD_NO = BcaGD;
                        }
                        else
                        {
                            List<SelectListItem> BcaGD = new List<SelectListItem>();
                            BcaGD.Insert(0, (new SelectListItem { Text = "--No GD--", Value = "0" }));
                            ViewBag.BCAD_GD_NO = BcaGD;
                        }
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("BCA");
        }

        public ActionResult ReverseBCA(int BA = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "BCA", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string FINo = "";
                    try
                    {
                        if (BA != 0)
                        {
                            PSW_BCA_INFO BCA = context.PSW_BCA_INFO.Where(m => m.BCAD_ID == BA).FirstOrDefault();
                            if (BCA != null)
                            {
                                if (DAL.DO_EDI == "true")
                                {
                                    message += DAL.MethodId1536(BA, false) + Environment.NewLine;
                                    //message += "  RAW REQUEST  DETAILS FROM OUR SIDE  -------------------- " + Environment.NewLine;
                                    //message += DAL.message;
                                    if (message.Contains("212"))
                                    {
                                        FINo = BCA.BCAD_FORM_E_NO;
                                        BCA.BCAD_STATUS = "REVERSED";
                                        BCA.BCAD_EDIT_DATETIME = DateTime.Now;
                                        context.Entry(BCA).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message += "BCA Reversed Successfully." + Environment.NewLine;
                                            message += EMAILING.EEF_BCA_Reverse(BA);
                                        }
                                        else
                                            message += "Problem While Updating The Status.";
                                    }
                                }
                                else
                                    message += "EDI is turned off from system configuration";
                                return View("BCA", BCA);
                            }
                            else
                            {
                                message = " Error While Reversing the BDA ";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "ReverseBCA", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                        return View("BCA");
                    }
                    finally
                    {
                        List<SelectListItem> EefRequests = new SelectList(context.PSW_EEF_MASTER_DETAILS.ToList(), "FDI_FORM_E_NO", "FDI_FORM_E_NO", 0).ToList();
                        EefRequests.Insert(0, (new SelectListItem { Text = "--Select Request --", Value = "0" }));
                        ViewBag.BCAD_FORM_E_NO = EefRequests;
                        List<SelectListItem> Currency = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        Currency.Insert(0, (new SelectListItem { Text = "--Select Currency--", Value = "0" }));
                        ViewBag.BCAD_CURRENCY = Currency;
                        List<SelectListItem> BcaEvent = new SelectList(context.PSW_BCA_EVENT.ToList(), "BCAE_ID", "BCAE_NAME", 0).ToList();
                        BcaEvent.Insert(0, (new SelectListItem { Text = "--Select Event--", Value = "0" }));
                        ViewBag.BCAD_EVENTID = BcaEvent;
                        List<SelectListItem> PaymentCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_STATUS == true && m.LC_ISAUTH == true).ToList(), "LC_NAME", "LC_NAME", 0).ToList();
                        PaymentCountry.Insert(0, (new SelectListItem { Text = "--Select Payment Country--", Value = "0" }));
                        ViewBag.BCAD_PAYMENT_COUNTRY = PaymentCountry;
                        if (FINo != "")
                        {
                            List<string> GDNos = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM == FINo).Select(m => m.GDAEF_GD_NO).ToList();
                            List<SelectListItem> BcaGD = new SelectList(context.PSW_EEF_GD_GENERAL_INFO.Where(m => GDNos.Contains(m.EFGDGI_GD_NO)).ToList(), "EFGDGI_GD_NO", "EFGDGI_GD_NO", 0).ToList();
                            BcaGD.Insert(0, (new SelectListItem { Text = "--Select GD No--", Value = "0" }));
                            ViewBag.BCAD_GD_NO = BcaGD;
                        }
                        else
                        {
                            List<SelectListItem> BcaGD = new List<SelectListItem>();
                            BcaGD.Insert(0, (new SelectListItem { Text = "--No GD--", Value = "0" }));
                            ViewBag.BCAD_GD_NO = BcaGD;
                        }
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("BCA");
        }

        public ActionResult CancelEEF(string E = "", int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != "")
                        {
                            EEF eefInfo = new EEF();
                            PSW_FORM_E_DOCUMENT_INFO basicInfoUpdate = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == M).FirstOrDefault();
                            if (basicInfoUpdate != null)
                            {
                                eefInfo.DocInfo = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == M).FirstOrDefault();
                                eefInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();
                                if (eefInfo.DocInfo == null)
                                {
                                    message += "No EEF Document Information Found on Form E # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eefInfo.DocInfo.FDI_TRADER_NAME != null)
                                    {
                                        eefInfo.DocInfo.FDI_TRADER_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eefInfo.DocInfo.FDI_TRADER_NAME).FirstOrDefault().C_IBAN_NO;
                                    }
                                    else
                                    {
                                        message += "Invalid Trader Name";
                                    }
                                }
                                eefInfo.DocRequestNo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == E && m.FDRI_AMENDMENT_NO == M).FirstOrDefault();
                                if (eefInfo.DocRequestNo == null)
                                {
                                    message += "No EEF Documents Request Information Found In The System For the EEF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eefInfo.DocRequestNo.FDRI_COUNTRY != null)
                                        eefInfo.DOC_REQ_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eefInfo.DocRequestNo.FDRI_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Country";
                                    if (eefInfo.DocRequestNo.FDRI_CURRENCY != null)
                                        eefInfo.CCY = context.PSW_CURRENCY.Where(m => m.CUR_ID == eefInfo.DocRequestNo.FDRI_CURRENCY).FirstOrDefault();
                                    else
                                        message += "Invalid Currency";
                                    if (eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS != null)
                                        eefInfo.DELIVERY_TERM = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS).FirstOrDefault();
                                    else
                                        message += "Invalid Delivery Term";
                                }
                                eefInfo.hscodes = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == E && m.EEF_HC_AMENDMENT_NO == M).ToList();
                                if (eefInfo.hscodes == null && eefInfo.hscodes.Count() <= 0)
                                {
                                    message += "No Goods Declaration Found In The System For the EEF # " + E + Environment.NewLine;
                                }
                                if (DAL.DO_EDI == "true")
                                {
                                    message += DAL.MethodId1535(E, false) + Environment.NewLine;
                                    //message += "  RAW REQUEST  DETAILS FROM OUR SIDE  -------------------- " + Environment.NewLine;
                                    //message += DAL.message;
                                    if (message.Contains("211"))
                                    {
                                        basicInfoUpdate.FDI_STATUS = "CANCELLED";
                                        basicInfoUpdate.FDI_STATUS_DATE = DateTime.Now;
                                        context.Entry(basicInfoUpdate).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message += "EEF CANCELLED Successfully." + Environment.NewLine;
                                            message += EMAILING.EEF_Cancel(E);
                                        }
                                        else
                                            message += "Problem While Updating The Status.";
                                    }
                                }
                                else
                                    message += "EDI is turned off from system configuration";
                                return View("GetEEFViewMode", eefInfo);
                            }
                            else
                            {
                                message = " Error While Canceling the EEF # " + E;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "CancelEEF", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("GetEEFViewMode");
        }

        public ActionResult SettleEEF(string E = "", int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != "")
                        {
                            EEF eefInfo = new EEF();
                            PSW_FORM_E_DOCUMENT_INFO basicInfoUpdate = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == M).FirstOrDefault();
                            if (basicInfoUpdate != null)
                            {
                                eefInfo.DocInfo = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == 0).FirstOrDefault();
                                eefInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();
                                if (eefInfo.DocInfo == null)
                                {
                                    message += "No EEF Document Information Found on Form E # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eefInfo.DocInfo.FDI_TRADER_NAME != null)
                                    {
                                        eefInfo.DocInfo.FDI_TRADER_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eefInfo.DocInfo.FDI_TRADER_NAME).FirstOrDefault().C_IBAN_NO;
                                    }
                                    else
                                    {
                                        message += "Invalid Trader Name";
                                    }
                                }
                                eefInfo.DocRequestNo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == E && m.FDRI_AMENDMENT_NO == M).FirstOrDefault();
                                if (eefInfo.DocRequestNo == null)
                                {
                                    message += "No EEF Documents Request Information Found In The System For the EEF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eefInfo.DocRequestNo.FDRI_COUNTRY != null)
                                        eefInfo.DOC_REQ_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eefInfo.DocRequestNo.FDRI_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Country";
                                    if (eefInfo.DocRequestNo.FDRI_CURRENCY != null)
                                        eefInfo.CCY = context.PSW_CURRENCY.Where(m => m.CUR_ID == eefInfo.DocRequestNo.FDRI_CURRENCY).FirstOrDefault();
                                    else
                                        message += "Invalid Currency";
                                    if (eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS != null)
                                        eefInfo.DELIVERY_TERM = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS).FirstOrDefault();
                                    else
                                        message += "Invalid Delivery Term";
                                }
                                eefInfo.hscodes = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == E && m.EEF_HC_AMENDMENT_NO == M).ToList();
                                if (eefInfo.hscodes == null && eefInfo.hscodes.Count() <= 0)
                                {
                                    message += "No Goods Declaration Found In The System For the EEF # " + E + Environment.NewLine;
                                }
                                if (DAL.DO_EDI == "true")
                                {
                                    message += DAL.MethodId1537(E, false) + Environment.NewLine;
                                    //message += "  RAW REQUEST  DETAILS FROM OUR SIDE  -------------------- " + Environment.NewLine;
                                    //message += DAL.message;
                                    if (message.Contains("213"))
                                    {
                                        basicInfoUpdate.FDI_STATUS = "SETTLED";
                                        basicInfoUpdate.FDI_STATUS_DATE = DateTime.Now;
                                        context.Entry(basicInfoUpdate).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message += "EEF SETTLED Successfully.";
                                        }
                                        else
                                        {
                                            message += "Problem While Updating The Status.";
                                        }
                                    }
                                }
                                else
                                    message += "EDI is turned off from system configuration";
                                return View("GetEEFViewMode", eefInfo);
                            }
                            else
                            {
                                message = " Error While Settle the EEF # " + E;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "SettleEEF", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("GetEEFViewMode");
        }

        public ActionResult RejectEEF(string E = "", int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != "")
                        {
                            EEF eefInfo = new EEF();
                            PSW_FORM_E_DOCUMENT_INFO basicInfoUpdate = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == M).FirstOrDefault();
                            if (basicInfoUpdate != null)
                            {
                                eefInfo.DocInfo = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E && m.FDI_AMENDMENT_NO == M).FirstOrDefault();
                                eefInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();
                                if (eefInfo.DocInfo == null)
                                {
                                    message += "No EEF Document Information Found on Form E # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eefInfo.DocInfo.FDI_TRADER_NAME != null)
                                    {
                                        eefInfo.DocInfo.FDI_TRADER_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eefInfo.DocInfo.FDI_TRADER_NAME).FirstOrDefault().C_IBAN_NO;
                                    }
                                    else
                                    {
                                        message += "Invalid Trader Name";
                                    }
                                }
                                eefInfo.DocRequestNo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == E && m.FDRI_AMENDMENT_NO == M).FirstOrDefault();
                                if (eefInfo.DocRequestNo == null)
                                {
                                    message += "No EEF Documents Request Information Found In The System For the EEF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eefInfo.DocRequestNo.FDRI_COUNTRY != null)
                                        eefInfo.DOC_REQ_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eefInfo.DocRequestNo.FDRI_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Country";
                                    if (eefInfo.DocRequestNo.FDRI_CURRENCY != null)
                                        eefInfo.CCY = context.PSW_CURRENCY.Where(m => m.CUR_ID == eefInfo.DocRequestNo.FDRI_CURRENCY).FirstOrDefault();
                                    else
                                        message += "Invalid Currency";
                                    if (eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS != null)
                                        eefInfo.DELIVERY_TERM = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eefInfo.DocRequestNo.FDRI_DELIVERY_TERMS).FirstOrDefault();
                                    else
                                        message += "Invalid Delivery Term";
                                }
                                eefInfo.hscodes = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == E && m.EEF_HC_AMENDMENT_NO == M).ToList();
                                if (eefInfo.hscodes == null && eefInfo.hscodes.Count() <= 0)
                                {
                                    message += "No Goods Declaration Found In The System For the EEF # " + E + Environment.NewLine;
                                }
                                basicInfoUpdate.FDI_STATUS = "REJECTED";
                                basicInfoUpdate.FDI_STATUS_DATE = DateTime.Now;
                                context.Entry(basicInfoUpdate).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message += "EEF Rejected Successfully." + Environment.NewLine;
                                }
                                else
                                    message += "Problem While Updating The Status.";
                                return View("GetEEFViewMode", eefInfo);
                            }
                            else
                            {
                                message = " Error While Canceling the EEF # " + E;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "RejectEEF", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("GetEEFViewMode");
        }

        public PartialViewResult EEFHSCodeList(string H = "", int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    int NextAmendmentNo = 0;
                    PSW_EEF_MASTER_DETAILS CheckAmend = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == H && m.FDI_AMENDMENT_NO == M).FirstOrDefault();
                    if (CheckAmend != null)
                    {
                        NextAmendmentNo = CheckAmend.FDI_AMENDMENT_NO + 1;
                    }
                    if (H != "")
                    {
                        List<PSW_EEF_HS_CODE> hscodes = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == H && m.EEF_HC_AMENDMENT_NO == NextAmendmentNo).ToList();
                        return PartialView(hscodes);
                    }
                    else
                        return PartialView();
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }

        [HttpPost]
        public JsonResult AddHsCodeToEEF(PSW_EEF_HS_CODE dbtable)
        {
            bool IsHsCode = false;
            bool CashMargin = false;
            string ImpoPol = "";
            string message = "";
            dynamic Result = new decimal();
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_LIST_OF_COMMODITIES CheckCommodity = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == dbtable.EEF_HC_CODE).FirstOrDefault();
                        if (CheckCommodity != null)
                        {
                            PSW_CASH_MARGIN_HS_CODES CheckCashMargin = context.PSW_CASH_MARGIN_HS_CODES.Where(m => m.CM_ID == CheckCommodity.LCOM_ID).FirstOrDefault();
                            if (CheckCashMargin != null)
                            {
                                CashMargin = true;
                            }
                            PSW_IMPORT_POLICY CheckImpPol = context.PSW_IMPORT_POLICY.Where(m => m.IP_PCT_CODE == dbtable.EEF_HC_CODE).FirstOrDefault();
                            if (CheckImpPol != null)
                            {
                                ImpoPol += "Import Policy Exist." + Environment.NewLine + "Commodity Description : " + CheckImpPol.IP_COMMODITY_DESCRIPTION + Environment.NewLine + "Heading Description : " + CheckImpPol.IP_HEADING_DESCRIPTION + "." + Environment.NewLine;
                            }
                        }
                        string HSCODE = DAL.RemoveNewLineAndTabs(dbtable.EEF_HC_CODE).Trim();
                        PSW_LIST_OF_COMMODITIES CheckHsCode = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == HSCODE && m.LCOM_STATUS == true).FirstOrDefault();
                        if (CheckHsCode != null)
                        {
                            int NextAmendmentNo = 0;
                            PSW_EEF_MASTER_DETAILS CheckAmend = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == dbtable.EEF_HC_EEF_REQUEST_NO && m.FDI_AMENDMENT_NO == dbtable.EEF_HC_AMENDMENT_NO).FirstOrDefault();
                            if (CheckAmend != null)
                            {
                                NextAmendmentNo = CheckAmend.FDI_AMENDMENT_NO + 1;
                            }
                            //PSW_EEF_HS_CODE CurrentEntity = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_CODE == dbtable.EEF_HC_CODE && m.EEF_HC_EEF_REQUEST_NO == dbtable.EEF_HC_EEF_REQUEST_NO && m.EEF_HC_AMENDMENT_NO == NextAmendmentNo).FirstOrDefault();
                            //if (CurrentEntity == null)
                            //{
                            PSW_EEF_HS_CODE CurrentEntity = new PSW_EEF_HS_CODE();
                            CurrentEntity.EEF_HC_ID = Convert.ToInt32(context.PSW_EEF_HS_CODE.Max(m => (int?)m.EEF_HC_ID)) + 1;
                            CurrentEntity.EEF_HC_ENTRTY_DATETIME = DateTime.Now;
                            CurrentEntity.EEF_HC_CODE = dbtable.EEF_HC_CODE;
                            CurrentEntity.EEF_HC_DESCRIPTION = dbtable.EEF_HC_DESCRIPTION;
                            CurrentEntity.EEF_HC_QUANTITY = dbtable.EEF_HC_QUANTITY;
                            CurrentEntity.EEF_HC_UOM = dbtable.EEF_HC_UOM;
                            CurrentEntity.EEF_HC_ORGIN = dbtable.EEF_HC_ORGIN;
                            CurrentEntity.EEF_HC_MAKER_ID = Session["USER_ID"].ToString();
                            if (CurrentEntity.EEF_HC_MAKER_ID == "Admin123")
                                CurrentEntity.EEF_HC_CHECKER_ID = CurrentEntity.EEF_HC_MAKER_ID;
                            else
                                CurrentEntity.EEF_HC_CHECKER_ID = null;
                            CurrentEntity.EEF_HC_EEF_REQUEST_NO = dbtable.EEF_HC_EEF_REQUEST_NO;
                            CurrentEntity.EEF_HC_INVOICE_VALUE = dbtable.EEF_HC_INVOICE_VALUE;
                            CurrentEntity.EEF_HC_AMOUNT_AGAINST_HS_CODE = dbtable.EEF_HC_AMOUNT_AGAINST_HS_CODE;
                            CurrentEntity.EEF_HC_AMENDMENT_NO = NextAmendmentNo;
                            if (ImpoPol != "")
                                CurrentEntity.EEF_HC_POLICY = ImpoPol;
                            else
                                CurrentEntity.EEF_HC_POLICY = null;
                            context.PSW_EEF_HS_CODE.Add(CurrentEntity);
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                ImpoPol += "Added Sucessfully." + Environment.NewLine;
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                IsHsCode = false;
                            }
                            //}
                            //else
                            //{
                            //    message = "HS Code " + dbtable.EEF_HC_CODE + " Already Exist ";
                            //    IsHsCode = false;
                            //}
                        }
                        else
                        {
                            message = "No Hs Code Found In The System";
                            IsHsCode = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    Result = new
                    {
                        HsCodeAddSuccess = IsHsCode,
                        CashMarginFall = CashMargin,
                        ImportPolicy = ImpoPol,
                        exp = message
                    };
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                RedirectToAction("Index", "Authentication");
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetEefHsCodeDescription(string HsCode = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (HsCode != "")
                    {
                        PSW_LIST_OF_COMMODITIES CheckHsCode = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == HsCode && m.LCOM_STATUS == true && m.LCOM_ISAUTH == true).FirstOrDefault();
                        if (CheckHsCode != null)
                        {
                            var HsCodeDescription = CheckHsCode.LCOM_NAME;
                            return Json(HsCodeDescription, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json("Invalid Hs Code", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Hs Code", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult CheckBannedHsCode(string HsCode = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (HsCode != "")
                    {
                        PSW_LIST_OF_COMMODITIES CheckHsCode = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == HsCode && m.LCOM_STATUS == true && m.LCOM_ISAUTH == true).FirstOrDefault();
                        if (CheckHsCode != null)
                        {
                            PSW_NEGATIVE_LIST_OF_COMMODITIES CheckBannedHsCode = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_LCOM_ID == CheckHsCode.LCOM_ID).FirstOrDefault();
                            if (CheckBannedHsCode != null)
                                return Json("Hs Code exist in the banned item.", JsonRequestBehavior.AllowGet);
                            else
                                return Json("", JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json("Invalid Hs Code", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Hs Code", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult VerifyPortOfDischarge(string enteredPort = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (enteredPort != "")
                    {
                        PSW_PORT_OF_SHIPPING CheckPort = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_CODE == enteredPort && m.POS_ISAUTH == true).FirstOrDefault();
                        if (CheckPort != null)
                        {
                            var coverletters = new
                            {
                                Code = CheckPort.POS_CODE,
                                Name = CheckPort.POS_NAME
                            };
                            return Json(coverletters, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json("Invalid Port Name", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Port Name", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        public JsonResult RemoveHSCode(PSW_EEF_HS_CODE dbtable)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (dbtable.EEF_HC_EEF_REQUEST_NO != "" && dbtable.EEF_HC_CODE != "")
                        {
                            PSW_EEF_HS_CODE entry = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_ID == dbtable.EEF_HC_ID).FirstOrDefault();
                            if (entry != null)
                            {
                                context.Entry(entry).State = EntityState.Deleted;
                                int rowcount = context.SaveChanges();
                                if (rowcount > 0)
                                {
                                    message = "";
                                }
                                else
                                {
                                    message = "Error While Removing HS Code From The EIF";
                                }
                            }
                            else
                            {
                                message = "No Data Found To Remove";
                            }
                        }
                        else
                        {
                            message = "Invalid Data Passed.";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = "Exception Occurred : " + ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult GetPreviousEEFHsCode(string E = "", int AM = 0)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    try
                    {
                        if (E != "")
                        {
                            AM += 1;
                            int PAM = (AM - 1);
                            List<PSW_EEF_HS_CODE> CheckIfExist = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == E && m.EEF_HC_AMENDMENT_NO == AM).ToList();
                            if (CheckIfExist.Count > 0)
                            {
                                foreach (PSW_EEF_HS_CODE HSCodeToDelete in CheckIfExist)
                                {
                                    context.PSW_EEF_HS_CODE.Remove(HSCodeToDelete);
                                    RowCount += context.SaveChanges();
                                }
                            }


                            List<PSW_EEF_HS_CODE> gethscode = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == E && m.EEF_HC_AMENDMENT_NO == PAM).ToList();
                            if (gethscode.Count > 0)
                            {
                                RowCount = 0;
                                foreach (PSW_EEF_HS_CODE HSCode in gethscode)
                                {
                                    PSW_EEF_HS_CODE Entity = new PSW_EEF_HS_CODE();
                                    Entity.EEF_HC_ID = Convert.ToInt32(context.PSW_EEF_HS_CODE.Max(m => (decimal?)m.EEF_HC_ID)) + 1;
                                    Entity.EEF_HC_EEF_REQUEST_NO = HSCode.EEF_HC_EEF_REQUEST_NO;
                                    Entity.EEF_HC_CODE = HSCode.EEF_HC_CODE;
                                    Entity.EEF_HC_DESCRIPTION = HSCode.EEF_HC_DESCRIPTION;
                                    Entity.EEF_HC_QUANTITY = HSCode.EEF_HC_QUANTITY;
                                    Entity.EEF_HC_UOM = HSCode.EEF_HC_UOM;
                                    Entity.EEF_HC_ORGIN = HSCode.EEF_HC_ORGIN;
                                    Entity.EEF_HC_INVOICE_VALUE = HSCode.EEF_HC_INVOICE_VALUE;
                                    Entity.EEF_HC_ENTRTY_DATETIME = DateTime.Now;
                                    Entity.EEF_HC_MAKER_ID = Session["USER_ID"].ToString();
                                    if (Entity.EEF_HC_MAKER_ID == "Admin123")
                                        Entity.EEF_HC_CHECKER_ID = Entity.EEF_HC_MAKER_ID;
                                    else
                                        Entity.EEF_HC_CHECKER_ID = null;
                                    Entity.EEF_HC_AMENDMENT_NO = (HSCode.EEF_HC_AMENDMENT_NO + 1);
                                    Entity.EEF_HC_AMOUNT_AGAINST_HS_CODE = HSCode.EEF_HC_AMOUNT_AGAINST_HS_CODE;
                                    context.PSW_EEF_HS_CODE.Add(Entity);
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message = "";
                                }
                            }
                            else
                            {
                                message = "Unable to fetch previous HS Code on EEF Request No : " + E;
                            }
                        }
                        else
                        {
                            message = "No HS Code found";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        #region EEF Document Info
        public ActionResult EEFDocumentInfo(int F = 0)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    if (F != 0)
                    {
                        PSW_FORM_E_DOCUMENT_INFO edit = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_ID == F).FirstOrDefault();
                        if (edit != null)
                        {
                            return View(edit);
                        }
                        else
                        {
                            message = "Exception Occur while fetching your record on User Id = " + F;
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                finally
                {
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EEFDocumentInfo(PSW_FORM_E_DOCUMENT_INFO dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    PSW_FORM_E_DOCUMENT_INFO UpdateEntity = new PSW_FORM_E_DOCUMENT_INFO();

                    PSW_FORM_E_DOCUMENT_INFO CheckIFExist = new PSW_FORM_E_DOCUMENT_INFO();
                    if (dbtable.FDI_ID == 0)
                    {
                        UpdateEntity.FDI_ID = Convert.ToInt32(context.PSW_FORM_E_DOCUMENT_INFO.Max(m => (decimal?)m.FDI_ID)) + 1;
                        UpdateEntity.FDI_ENTRY_DATETIME = DateTime.Now;
                        CheckIFExist = null;
                    }
                    else
                    {
                        UpdateEntity = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_ID == dbtable.FDI_ID).FirstOrDefault();
                        UpdateEntity.FDI_EDIT_DATETIME = DateTime.Now;
                        CheckIFExist = UpdateEntity;
                    }
                    UpdateEntity.FDI_FORM_E_NO = dbtable.FDI_FORM_E_NO;
                    UpdateEntity.FDI_STATUS = dbtable.FDI_STATUS;
                    UpdateEntity.FDI_PHASE_OF_ISSUE = dbtable.FDI_PHASE_OF_ISSUE;
                    UpdateEntity.FDI_STATUS_DATE = dbtable.FDI_STATUS_DATE;
                    UpdateEntity.FDI_ISSUE_BANK = dbtable.FDI_ISSUE_BANK;
                    UpdateEntity.FDI_MODE_OF_EXPORT_PAYMENT = dbtable.FDI_MODE_OF_EXPORT_PAYMENT;
                    UpdateEntity.FDI_NTN = dbtable.FDI_NTN;
                    UpdateEntity.FDI_TRADER_NAME = dbtable.FDI_TRADER_NAME;
                    UpdateEntity.FDI_TRADER_ADDRESS = dbtable.FDI_TRADER_ADDRESS;
                    UpdateEntity.FDI_MAKER_ID = "1";
                    UpdateEntity.FDI_CHECKER_ID = "1";
                    if (CheckIFExist == null)
                        context.PSW_FORM_E_DOCUMENT_INFO.Add(UpdateEntity);
                    else
                        context.Entry(UpdateEntity).State = EntityState.Modified;
                    int RowsAffected = context.SaveChanges();
                    if (RowsAffected > 0)
                    {
                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                        ModelState.Clear();
                    }
                    else
                    {
                        message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                finally
                {

                    List<SelectListItem> Country = new SelectList(context.PSW_LIST_OF_COUNTRIES.ToList(), "LC_ID", "LC_NAME", 0).ToList();
                    Country.Insert(0, (new SelectListItem { Text = "--Select Country--", Value = "0" }));
                    ViewBag.FDRI_COUNTRY = Country;
                    List<SelectListItem> Currency = new SelectList(context.PSW_CURRENCY.ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                    Currency.Insert(0, (new SelectListItem { Text = "--Select Currency--", Value = "0" }));
                    ViewBag.FDRI_CURRENCY = Currency;
                    List<SelectListItem> PaymentTerm = new SelectList(context.PSW_PAYMENT_TERM.ToList(), "PT_ID", "PT_NAME", 0).ToList();
                    PaymentTerm.Insert(0, (new SelectListItem { Text = "--Select Payment Term--", Value = "0" }));
                    ViewBag.FDRI_PAYMENT_TERMS = PaymentTerm;
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult EEFDocumentInfoList()
        {
            return PartialView(context.PSW_FORM_E_DOCUMENT_INFO.ToList());
        }
        #region EEF Document Request Info
        public PartialViewResult EEFDocumentRequestInfoList()
        {
            return PartialView(context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.ToList());
        }
        #endregion
        #endregion

        [HttpPost]
        public JsonResult ListOfAuthPayModes(string clientId = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (clientId != "")
                    {
                        int cid = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == clientId).FirstOrDefault().C_ID;

                        int?[] AuthPayAids = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == cid && m.APMC_STATUS == true && m.APMC_ISAUTH == true).Select(m => m.APMC_PAY_MODE_ID).ToArray();
                        var coverletters = context.PSW_AUTH_PAY_MODES.Where(x => AuthPayAids.Contains(x.APM_ID) && x.APM_TYPE_ID == 2 && x.APM_ISAUTH == true).Select(x => new
                        {
                            Value = x.APM_ID,
                            Text = x.APM_STATUS + " -- " + x.APM_CODE
                        });
                        return Json(coverletters, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Client", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult GetBusinessDetails(string clientId = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (clientId != "")
                    {
                        int cid = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == clientId).FirstOrDefault().C_ID;

                        var coverletters = context.PSW_CLIENT_INFO.Where(x => x.C_ID == cid).Select(x => new
                        {
                            NTN = x.C_NTN_NO,
                            ADDRESS = x.C_ADDRESS,
                            IBAN = x.C_IBAN_NO,
                            NAME = x.C_NAME
                        });
                        return Json(coverletters, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invaliid Cliet", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }


        [HttpPost]
        public JsonResult GetCharges(string BID, decimal IVal, string PM, string Req)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    int cid = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == BID).FirstOrDefault().C_ID;
                    PSW_CLIENT_INFO client = context.PSW_CLIENT_INFO.Where(m => m.C_ID == cid).FirstOrDefault();
                    decimal ChargesPercent = (decimal)(client.C_EXPORT_COMMISION == null ? 0 : client.C_EXPORT_COMMISION);
                    decimal FEDPercent = (decimal)(client.C_FED_RATE == null ? 0 : client.C_FED_RATE);
                    decimal CommissionCharges = Math.Round((ChargesPercent / 100) * IVal, 2);
                    decimal FED = Math.Round((FEDPercent / 100) * CommissionCharges, 2);
                    var coverletters = new
                    {
                        CommCharges = CommissionCharges,
                        FEDCharges = FED
                    };
                    return Json(coverletters, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public ActionResult GetEEFGDNumber(string REQ)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        List<PSW_EEF_GD_GENERAL_INFO_LIST> GetEEFGD = new List<PSW_EEF_GD_GENERAL_INFO_LIST>();
                        GetEEFGD = (context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(x => x.EFGDGI_FORM_E_NO.Contains(REQ))).ToList();
                        JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                        string result = javaScriptSerializer.Serialize(GetEEFGD);
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return null;
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult GetHsCodeSUM(string E, int AM = 0)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    PSW_FORM_E_DOCUMENT_INFO CheckEEF = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E).FirstOrDefault();
                    int AmendmentNoTodel = 0;
                    if (CheckEEF != null)
                        AmendmentNoTodel = AM + 1;
                    decimal? SumGoodsInvoiceAmount = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == E && m.EEF_HC_AMENDMENT_NO == AmendmentNoTodel).Sum(m => m.EEF_HC_INVOICE_VALUE);
                    var coverletters = new
                    {
                        InVoiceAmount = SumGoodsInvoiceAmount == null ? 0 : SumGoodsInvoiceAmount
                    };
                    return Json(coverletters, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult GetEEFInformationForBCA(string E = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "BCA", Session["USER_ID"].ToString()))
                {
                    PSW_EEF_MASTER_DETAILS eef_info = context.PSW_EEF_MASTER_DETAILS.Where(x => x.FDI_FORM_E_NO == E).FirstOrDefault();
                    var HSCODE_SUM = eef_info.FDRI_TOTAL_VALUE_OF_SHIPMENT;
                    decimal? PreviousBCAAmounts = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FORM_E_NO == E && m.BCAD_STATUS == "APPROVED").Sum(m => m.BCAD_BILL_AMOUNT);
                    decimal? CurrentBalance = (eef_info.FDRI_TOTAL_VALUE_OF_SHIPMENT) - (PreviousBCAAmounts == null ? 0 : PreviousBCAAmounts);
                    var coverletters = new
                    {
                        TOTALAMOUNT = eef_info.FDRI_TOTAL_VALUE_OF_SHIPMENT,
                        AUTCCY = eef_info.FDRI_CURRENCY,
                        CurrentBalance = CurrentBalance == null ? 0 : CurrentBalance,
                        TotalHSCodeValue = HSCODE_SUM
                    };
                    return Json(coverletters, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult GetUniuqBankRefBCA(string E = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "BCA", Session["USER_ID"].ToString()))
                {
                    PSW_EEF_MASTER_DETAILS eef_info = context.PSW_EEF_MASTER_DETAILS.Where(x => x.FDI_FORM_E_NO == E).FirstOrDefault();
                    string UniqueBankReferencNo = GetUniqueBankReferenceNumber(eef_info.APM_STATUS, eef_info.FDI_FORM_E_NO);
                    return Json(UniqueBankReferencNo);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        private string GetUniqueBankReferenceNumber(string PaymentMode, string FINo)
        {
            string FinalNo = "";
            string BranchCode = "0001";
            string Year = DateTime.Now.ToString("yy");
            if (PaymentMode == "Advance Payment")
            {
                FinalNo = BranchCode + "ADP";
                string CheckUniqueRef = context.PSW_UNIQUE_BANK_REFERENCE_LOG.Where(m => m.URL_REFERENCE_NO.Contains(FinalNo)).OrderByDescending(m => m.URL_ID).Select(m => m.URL_REFERENCE_NO).FirstOrDefault();
                if (CheckUniqueRef != null)
                {
                    string TempYear = CheckUniqueRef.Remove(0, 7);
                    string FinalYear = new string(TempYear.Take(2).ToArray());
                    string FinalNumber = TempYear.Substring(2);
                    if (FinalYear == Year)
                    {
                        var SerialNo = int.Parse(FinalNumber) + 1;
                        FinalNo = BranchCode + "ADP" + Year + String.Format("{0:0000000}", SerialNo);
                        LogUniuqBankReferenceNumber(FinalNo, FINo);
                    }
                    else
                    {
                        FinalNo = BranchCode + "ADP" + Year + "0000001";
                        LogUniuqBankReferenceNumber(FinalNo, FINo);
                    }
                }
                else
                {
                    FinalNo = BranchCode + "ADP" + Year + "0000001";
                    LogUniuqBankReferenceNumber(FinalNo, FINo);
                }
            }
            else if (PaymentMode == "Open Account")
            {
                FinalNo = BranchCode + "OPA";
                string CheckUniqueRef = context.PSW_UNIQUE_BANK_REFERENCE_LOG.Where(m => m.URL_REFERENCE_NO.Contains(FinalNo)).OrderByDescending(m => m.URL_ID).Select(m => m.URL_REFERENCE_NO).FirstOrDefault();
                if (CheckUniqueRef != null)
                {
                    string TempYear = CheckUniqueRef.Remove(0, 7);
                    string FinalYear = new string(TempYear.Take(2).ToArray());
                    string FinalNumber = TempYear.Substring(2);
                    if (FinalYear == Year)
                    {
                        var SerialNo = int.Parse(FinalNumber) + 1;
                        FinalNo = BranchCode + "OPA" + Year + String.Format("{0:0000000}", SerialNo);
                        LogUniuqBankReferenceNumber(FinalNo, FINo);
                    }
                    else
                    {
                        FinalNo = BranchCode + "OPA" + Year + "0000001";
                        LogUniuqBankReferenceNumber(FinalNo, FINo);
                    }
                }
                else
                {
                    FinalNo = BranchCode + "OPA" + Year + "0000001";
                    LogUniuqBankReferenceNumber(FinalNo, FINo);
                }
            }
            else if (PaymentMode == "With LC")
            {
                FinalNo = BranchCode + "WLC";
                string CheckUniqueRef = context.PSW_UNIQUE_BANK_REFERENCE_LOG.Where(m => m.URL_REFERENCE_NO.Contains(FinalNo)).OrderByDescending(m => m.URL_ID).Select(m => m.URL_REFERENCE_NO).FirstOrDefault();
                if (CheckUniqueRef != null)
                {
                    string TempYear = CheckUniqueRef.Remove(0, 7);
                    string FinalYear = new string(TempYear.Take(2).ToArray());
                    string FinalNumber = TempYear.Substring(2);
                    if (FinalYear == Year)
                    {
                        var SerialNo = int.Parse(FinalNumber) + 1;
                        FinalNo = BranchCode + "WLC" + Year + String.Format("{0:0000000}", SerialNo);
                        LogUniuqBankReferenceNumber(FinalNo, FINo);
                    }
                    else
                    {
                        FinalNo = BranchCode + "WLC" + Year + "0000001";
                        LogUniuqBankReferenceNumber(FinalNo, FINo);
                    }
                }
                else
                {
                    FinalNo = BranchCode + "WLC" + Year + "0000001";
                    LogUniuqBankReferenceNumber(FinalNo, FINo);
                }
            }
            else if (PaymentMode == "Without LC")
            {
                FinalNo = BranchCode + "OLC";
                string CheckUniqueRef = context.PSW_UNIQUE_BANK_REFERENCE_LOG.Where(m => m.URL_REFERENCE_NO.Contains(FinalNo)).OrderByDescending(m => m.URL_ID).Select(m => m.URL_REFERENCE_NO).FirstOrDefault();
                if (CheckUniqueRef != null)
                {
                    string TempYear = CheckUniqueRef.Remove(0, 7);
                    string FinalYear = new string(TempYear.Take(2).ToArray());
                    string FinalNumber = TempYear.Substring(2);
                    if (FinalYear == Year)
                    {
                        var SerialNo = int.Parse(FinalNumber) + 1;
                        FinalNo = BranchCode + "OLC" + Year + String.Format("{0:0000000}", SerialNo);
                        LogUniuqBankReferenceNumber(FinalNo, FINo);
                    }
                    else
                    {
                        FinalNo = BranchCode + "OLC" + Year + "0000001";
                        LogUniuqBankReferenceNumber(FinalNo, FINo);
                    }
                }
                else
                {
                    FinalNo = BranchCode + "OLC" + Year + "0000001";
                    LogUniuqBankReferenceNumber(FinalNo, FINo);
                }
            }
            return FinalNo;
        }

        private void LogUniuqBankReferenceNumber(string finalNo, string FINo)
        {
            PSW_UNIQUE_BANK_REFERENCE_LOG log = new PSW_UNIQUE_BANK_REFERENCE_LOG();
            log.URL_ID = Convert.ToInt32(context.PSW_UNIQUE_BANK_REFERENCE_LOG.Max(m => (decimal?)m.URL_ID)) + 1;
            log.URL_EEF_REUEST_NO = FINo;
            log.URL_REFERENCE_NO = finalNo;
            log.URL_ADDED_BY = Session["USER_ID"].ToString();
            log.URL_ADDED_DATETIME = DateTime.Now;
            log.URL_STATUS = "Lost";
            context.PSW_UNIQUE_BANK_REFERENCE_LOG.Add(log);
            context.SaveChanges();
        }

        public PartialViewResult EEFRequests(string E, EEFFilter Entity)
        {
            List<PSW_EEF_MASTER_DETAILS> returnList = new List<PSW_EEF_MASTER_DETAILS>();
            string message = "";
            try
            {
                switch (E)
                {
                    case "1":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.FDI_STATUS == "Initiated").OrderByDescending(m => m.FDI_ID).ToList();
                        else
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_STATUS == "Initiated").OrderByDescending(m => m.FDI_ID).Take(10).ToList();
                        break;
                    case "2":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.FDI_STATUS == "APPROVED").OrderByDescending(m => m.FDI_ID).ToList();
                        else
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_STATUS == "APPROVED").OrderByDescending(m => m.FDI_ID).Take(10).ToList();
                        break;
                    case "3":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.FDI_STATUS == "APPROVED").OrderByDescending(m => m.FDI_ID).ToList();
                        else
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_STATUS == "APPROVED").OrderByDescending(m => m.FDI_ID).Take(10).ToList();
                        break;
                    case "4":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.FDI_STATUS == "APPROVED").OrderByDescending(m => m.FDI_ID).ToList();
                        else
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_STATUS == "APPROVED").OrderByDescending(m => m.FDI_ID).Take(10).ToList();
                        break;
                    case "5":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.FDI_STATUS == "APPROVED").OrderByDescending(m => m.FDI_ID).ToList();
                        else
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_STATUS == "APPROVED").OrderByDescending(m => m.FDI_ID).Take(10).ToList();
                        break;
                    case "6":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.FDI_STATUS == "BANK").OrderByDescending(m => m.FDI_ID).ToList();
                        else
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_STATUS.Contains("BANK")).OrderByDescending(m => m.FDI_ID).Take(10).ToList();
                        break;
                    case "7":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EEF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate)).OrderByDescending(m => m.FDI_ID).ToList();
                        else
                            returnList = context.PSW_EEF_MASTER_DETAILS.OrderByDescending(m => m.FDI_ID).Take(10).ToList();
                        break;
                    default:
                        message = " Invalid Action Performed.";
                        break;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.SelectedOption = E;
                ViewBag.message = message;
            }
            return PartialView(returnList);
        }
        #endregion

        #region EEF BCA
        public ActionResult BCA(int B = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "BCA", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string FINo = "";
                    try
                    {
                        if (B != 0)
                        {
                            PSW_BCA_INFO edit = context.PSW_BCA_INFO.Where(m => m.BCAD_ID == B).FirstOrDefault();
                            if (edit != null)
                            {
                                FINo = edit.BCAD_FORM_E_NO;
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + B;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "BCA", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> EefRequests = new SelectList(context.PSW_EEF_MASTER_DETAILS.ToList(), "FDI_FORM_E_NO", "FDI_FORM_E_NO", 0).ToList();
                        EefRequests.Insert(0, (new SelectListItem { Text = "--Select Request --", Value = "0" }));
                        ViewBag.BCAD_FORM_E_NO = EefRequests;
                        List<SelectListItem> Currency = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        Currency.Insert(0, (new SelectListItem { Text = "--Select Currency--", Value = "0" }));
                        ViewBag.BCAD_CURRENCY = Currency;
                        List<SelectListItem> BcaEvent = new SelectList(context.PSW_BCA_EVENT.ToList(), "BCAE_ID", "BCAE_NAME", 0).ToList();
                        BcaEvent.Insert(0, (new SelectListItem { Text = "--Select Event--", Value = "0" }));
                        ViewBag.BCAD_EVENTID = BcaEvent;
                        List<SelectListItem> PaymentCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_STATUS == true && m.LC_ISAUTH == true).ToList(), "LC_NAME", "LC_NAME", 0).ToList();
                        PaymentCountry.Insert(0, (new SelectListItem { Text = "--Select Payment Country--", Value = "0" }));
                        ViewBag.BCAD_PAYMENT_COUNTRY = PaymentCountry;
                        if (FINo != "")
                        {

                            List<string> GDNos = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM == FINo).Select(m => m.GDAEF_GD_NO).ToList();
                            List<SelectListItem> BcaGD = new SelectList(context.PSW_EEF_GD_GENERAL_INFO.Where(m => GDNos.Contains(m.EFGDGI_GD_NO)).ToList(), "EFGDGI_GD_NO", "EFGDGI_GD_NO", 0).ToList();
                            BcaGD.Insert(0, (new SelectListItem { Text = "--Select GD No--", Value = "0" }));
                            ViewBag.BCAD_GD_NO = BcaGD;
                        }
                        else
                        {
                            List<SelectListItem> BcaGD = new List<SelectListItem>();
                            BcaGD.Insert(0, (new SelectListItem { Text = "--No GD--", Value = "0" }));
                            ViewBag.BCAD_GD_NO = BcaGD;
                        }
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult BCA(PSW_BCA_INFO dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "BCA", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string FINo = "";
                    try
                    {
                        if (dbtable.BCAD_GD_NO == null)
                        {
                            message = "GD Number is required";
                            return View();
                        }
                        if (dbtable.BCAD_FORM_E_NO != "0")
                        {
                            FINo = dbtable.BCAD_FORM_E_NO;
                            PSW_BCA_INFO UpdateEntity = new PSW_BCA_INFO();
                            if (dbtable.BCAD_ID == 0)
                            {
                                UpdateEntity.BCAD_ID = Convert.ToInt32(context.PSW_BCA_INFO.Max(m => (decimal?)m.BCAD_ID)) + 1;
                                UpdateEntity.BCAD_ENTRY_DATETIME = DateTime.Now;
                                UpdateEntity.BCAD_BCA_NO = GenerateNewEEFBCANo();
                                UpdateEntity.BCAD_AMENDMENT_NO = 0;
                            }
                            else
                            {
                                PSW_BCA_INFO AmendmentNo = context.PSW_BCA_INFO.Where(m => m.BCAD_ID == dbtable.BCAD_ID).FirstOrDefault();
                                UpdateEntity = new PSW_BCA_INFO();
                                UpdateEntity.BCAD_ID = Convert.ToInt32(context.PSW_BCA_INFO.Max(m => (decimal?)m.BCAD_ID)) + 1;
                                UpdateEntity.BCAD_AMENDMENT_NO = Convert.ToInt32(context.PSW_BCA_INFO.Where(m => m.BCAD_BCA_NO == AmendmentNo.BCAD_BCA_NO).Max(m => m.BCAD_AMENDMENT_NO)) + 1;
                                UpdateEntity.BCAD_BCA_NO = AmendmentNo.BCAD_BCA_NO;
                                UpdateEntity.BCAD_ENTRY_DATETIME = DateTime.Now;
                            }
                            UpdateEntity.BCAD_FORM_E_NO = dbtable.BCAD_FORM_E_NO;
                            UpdateEntity.BCAD_EDS_UNIQUE_BANK_REF_NO = dbtable.BCAD_EDS_UNIQUE_BANK_REF_NO;
                            UpdateEntity.BCAD_RUNNING_SERIAL_DETAIL = dbtable.BCAD_RUNNING_SERIAL_DETAIL == null ? 0 : dbtable.BCAD_RUNNING_SERIAL_DETAIL;
                            UpdateEntity.BCAD_SWIFT_REFERENCES = dbtable.BCAD_SWIFT_REFERENCES;
                            UpdateEntity.BCAD_BILL_NO = dbtable.BCAD_BILL_NO == null ? null : dbtable.BCAD_BILL_NO;
                            UpdateEntity.BCAD_BILL_DATE = dbtable.BCAD_BILL_DATE;
                            UpdateEntity.BCAD_BILL_AMOUNT = dbtable.BCAD_BILL_AMOUNT;
                            UpdateEntity.BCAD_INVOICE_NO = dbtable.BCAD_INVOICE_NO;
                            UpdateEntity.BCAD_INVOICE_DATE = dbtable.BCAD_INVOICE_DATE;
                            UpdateEntity.BCAD_INVOICE_AMOUNT = dbtable.BCAD_INVOICE_AMOUNT;
                            UpdateEntity.BCAD_FORIEGN_BANK_CHARGES_FCY = dbtable.BCAD_FORIEGN_BANK_CHARGES_FCY;
                            UpdateEntity.BCAD_AGENT_BROKE_COMMISSION_FCY = dbtable.BCAD_AGENT_BROKE_COMMISSION_FCY;
                            UpdateEntity.BCAD_WITH_HOLDING_TAX_PKR = dbtable.BCAD_WITH_HOLDING_TAX_PKR;
                            UpdateEntity.BCAD_EDS_PKR = dbtable.BCAD_EDS_PKR;
                            UpdateEntity.BCAD_BCA_FC = dbtable.BCAD_BCA_FC;
                            UpdateEntity.BCAD_FCY_EXCHANGE_RATE = dbtable.BCAD_FCY_EXCHANGE_RATE;
                            UpdateEntity.BCAD_BCA_PKR = dbtable.BCAD_BCA_PKR;
                            UpdateEntity.BCAD_DATE_OF_REALIZED = dbtable.BCAD_DATE_OF_REALIZED;
                            UpdateEntity.BCAD_AFJUSTMENT_FROM_SPECIAL_FCY_AMOUNT = dbtable.BCAD_AFJUSTMENT_FROM_SPECIAL_FCY_AMOUNT;
                            UpdateEntity.BCAD_CURR_OF_REALIZATION = dbtable.BCAD_CURR_OF_REALIZATION;
                            UpdateEntity.BCAD_CURRENCY = dbtable.BCAD_CURRENCY;
                            UpdateEntity.BCAD_FULL_AMOUNT_IS_NOT_REALIZED = dbtable.BCAD_FULL_AMOUNT_IS_NOT_REALIZED;
                            UpdateEntity.BCAD_FORM_AMOUNT_REALIZED = dbtable.BCAD_FORM_AMOUNT_REALIZED;
                            UpdateEntity.BCAD_BALANCE = dbtable.BCAD_BALANCE;
                            UpdateEntity.BCAD_ALLOWED_DISCOUNT = dbtable.BCAD_ALLOWED_DISCOUNT == null ? 0 : dbtable.BCAD_ALLOWED_DISCOUNT;
                            UpdateEntity.BCAD_ALLOWED_DISCOUNT_PERCENT = dbtable.BCAD_ALLOWED_DISCOUNT_PERCENT == null ? 0 : dbtable.BCAD_ALLOWED_DISCOUNT_PERCENT;
                            UpdateEntity.BCAD_CERTIFIED = dbtable.BCAD_CERTIFIED;
                            UpdateEntity.BCAD_EXCHANGE_RATE_PKR_PER_USD = dbtable.BCAD_EXCHANGE_RATE_PKR_PER_USD;
                            UpdateEntity.BCAD_FLEX_REFERENCE = dbtable.BCAD_FLEX_REFERENCE;
                            UpdateEntity.BCAD_PAYMENT_COUNTRY = dbtable.BCAD_PAYMENT_COUNTRY;
                            UpdateEntity.BCAD_REMARKS = dbtable.BCAD_REMARKS;
                            UpdateEntity.BCAD_GD_NO = dbtable.BCAD_GD_NO == "0" || dbtable.BCAD_GD_NO == null ? null : dbtable.BCAD_GD_NO;
                            UpdateEntity.BCAD_MAKER_ID = Session["USER_ID"].ToString();
                            if (UpdateEntity.BCAD_MAKER_ID == "Admin123")
                                UpdateEntity.BCAD_CHECKER_ID = UpdateEntity.BCAD_MAKER_ID;
                            else
                                UpdateEntity.BCAD_CHECKER_ID = null;
                            UpdateEntity.BCAD_STATUS = "initiated";
                            UpdateEntity.BCAD_EVENTID = dbtable.BCAD_EVENTID;
                            UpdateEntity.BCAD_EVENT_DATE = dbtable.BCAD_EVENT_DATE;
                            UpdateEntity.BCAD_EVENT_Remarks = dbtable.BCAD_EVENT_Remarks;
                            context.PSW_BCA_INFO.Add(UpdateEntity);
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                PSW_UNIQUE_BANK_REFERENCE_LOG UpdateLog = context.PSW_UNIQUE_BANK_REFERENCE_LOG.Where(m => m.URL_REFERENCE_NO == UpdateEntity.BCAD_EDS_UNIQUE_BANK_REF_NO).FirstOrDefault();
                                if (UpdateLog != null)
                                {
                                    UpdateLog.URL_STATUS = "Initiated";
                                    context.Entry(UpdateLog).State = EntityState.Modified;
                                    RowsAffected = context.SaveChanges();
                                    if (RowsAffected > 0)
                                    {
                                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                        ModelState.Clear();
                                    }
                                    else
                                    {
                                        message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                    }
                                }
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                        else
                        {
                            message = "Please Select Request No.";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "BCA", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> EefRequests = new SelectList(context.PSW_EEF_MASTER_DETAILS.ToList(), "FDI_FORM_E_NO", "FDI_FORM_E_NO", 0).ToList();
                        EefRequests.Insert(0, (new SelectListItem { Text = "--Select Request --", Value = "0" }));
                        ViewBag.BCAD_FORM_E_NO = EefRequests;
                        List<SelectListItem> Currency = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        Currency.Insert(0, (new SelectListItem { Text = "--Select Currency--", Value = "0" }));
                        ViewBag.BCAD_CURRENCY = Currency;
                        List<SelectListItem> BcaEvent = new SelectList(context.PSW_BCA_EVENT.ToList(), "BCAE_ID", "BCAE_NAME", 0).ToList();
                        BcaEvent.Insert(0, (new SelectListItem { Text = "--Select Event--", Value = "0" }));
                        ViewBag.BCAD_EVENTID = BcaEvent;
                        List<SelectListItem> PaymentCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_STATUS == true && m.LC_ISAUTH == true).ToList(), "LC_NAME", "LC_NAME", 0).ToList();
                        PaymentCountry.Insert(0, (new SelectListItem { Text = "--Select Payment Country--", Value = "0" }));
                        ViewBag.BCAD_PAYMENT_COUNTRY = PaymentCountry;
                        if (FINo != "")
                        {
                            List<string> GDNos = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM == FINo).Select(m => m.GDAEF_GD_NO).ToList();
                            List<SelectListItem> BcaGD = new SelectList(context.PSW_EEF_GD_GENERAL_INFO.Where(m => GDNos.Contains(m.EFGDGI_GD_NO)).ToList(), "EFGDGI_GD_NO", "EFGDGI_GD_NO", 0).ToList();
                            BcaGD.Insert(0, (new SelectListItem { Text = "--Select GD No--", Value = "0" }));
                            ViewBag.BCAD_GD_NO = BcaGD;
                        }
                        else
                        {
                            List<SelectListItem> BcaGD = new List<SelectListItem>();
                            BcaGD.Insert(0, (new SelectListItem { Text = "--No GD--", Value = "0" }));
                            ViewBag.BCAD_GD_NO = BcaGD;
                        }
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult BCAList(string E = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "BCA", Session["USER_ID"].ToString()))
                {
                    List<PSW_EEF_BCA_LIST> DataList = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FORM_E_NO == E).OrderByDescending(m => m.BCAD_ID).ToList();
                    return PartialView(DataList);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult GetAllGDAgainstBCA(string REQ)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "BCA", Session["USER_ID"].ToString()))
                {
                    List<PSW_EEF_GD_GENERAL_INFO_LIST> Entity = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_FORM_E_NO.Contains(REQ)).ToList();
                    return PartialView(Entity);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult GetBCAViewMode(int BCA_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "BCA", Session["USER_ID"].ToString()))
                {
                    int CurrID = 0;
                    EEF entity = new EEF();
                    entity.BCA = context.PSW_BCA_INFO.Where(m => m.BCAD_ID == BCA_ID).FirstOrDefault();
                    if (entity.BCA.BCAD_CURRENCY != null && entity.BCA.BCAD_CURRENCY != 0)
                        CurrID = Convert.ToInt32(entity.BCA.BCAD_CURRENCY);
                    entity.CCY = context.PSW_CURRENCY.Where(m => m.CUR_ID == CurrID).FirstOrDefault();
                    if (entity.BCA.BCAD_EVENTID != null && entity.BCA.BCAD_EVENTID != 0)
                        entity.Event = context.PSW_BCA_EVENT.Where(m => m.BCAE_ID == entity.BCA.BCAD_EVENTID).FirstOrDefault();
                    return PartialView(entity);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }

        [HttpPost]
        public FileStreamResult EEFBCABulkPortfolio(HttpPostedFileBase postedFile)
        {
            string message = "";
            try
            {
                string filePath = string.Empty;
                if (postedFile != null)
                {
                    string path = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filePath = path + Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName);
                    if (extension == ".xlsx" || extension == ".xls")
                    {
                        postedFile.SaveAs(filePath);
                        var Scanner = new AntiVirus.Scanner();
                        var result = Scanner.ScanAndClean(filePath);
                        if (result != AntiVirus.ScanResult.VirusNotFound)
                        {
                            message += "malicious file detected Unable to Upload File To the Server.";
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                            return CreateLogFile(message);
                        }
                    }
                    else
                    {
                        message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                        return CreateLogFile(message);
                    }
                    DataTable dt = new DataTable();
                    try
                    {
                        dt = BulkOptions.ReadAsDataTable(filePath);
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                        return CreateLogFile(message);
                    }
                    if (dt.Rows.Count > 0)
                    {
                        int RowCount = 0;
                        foreach (DataRow row in dt.Rows)
                        {
                            PSW_BCA_INFO EEFBCA = new PSW_BCA_INFO();
                            if (row["BCA Form-E No"].ToString().Length > 2)
                            {
                                string EEFFormENo = row["BCA Form-E No"].ToString();

                                string UniqueBankRefNo = "";

                                bool EefFormENoExist = false;
                                PSW_EEF_MASTER_DETAILS checkEEF = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == EEFFormENo).FirstOrDefault();
                                if (checkEEF != null)
                                {
                                    UniqueBankRefNo = GetUniqueBankReferenceNumber(checkEEF.APM_STATUS, checkEEF.FDI_FORM_E_NO);

                                    PSW_EEF_BCA_LIST CheckBCA = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FORM_E_NO == EEFFormENo).FirstOrDefault();
                                    if (CheckBCA != null)
                                    {
                                        EEFBCA = context.PSW_BCA_INFO.Where(m => m.BCAD_FORM_E_NO == CheckBCA.BCAD_FORM_E_NO && m.BCAD_AMENDMENT_NO == CheckBCA.BCAD_AMENDMENT_NO).FirstOrDefault();

                                        if (EEFBCA != null)
                                        {
                                            EEFBCA = new PSW_BCA_INFO();
                                            EEFBCA.BCAD_ID = Convert.ToInt32(context.PSW_BCA_INFO.Max(m => (decimal?)m.BCAD_ID)) + 1;
                                            EEFBCA.BCAD_ENTRY_DATETIME = DateTime.Now;
                                            EEFBCA.BCAD_AMENDMENT_NO = 0;
                                            EefFormENoExist = false;
                                        }
                                        else
                                        {
                                            EEFBCA = new PSW_BCA_INFO();
                                            EEFBCA.BCAD_ID = Convert.ToInt32(context.PSW_BCA_INFO.Max(m => (decimal?)m.BCAD_ID)) + 1;
                                            EEFBCA.BCAD_ENTRY_DATETIME = DateTime.Now;
                                            EEFBCA.BCAD_AMENDMENT_NO = 0;
                                            EefFormENoExist = false;
                                            //EEFBCA.BCAD_EDIT_DATETIME = DateTime.Now;
                                            //EefFormENoExist = true;
                                        }
                                        if (row["BCA Form-E No"] != null && row["BCA Form-E No"].ToString() != "")
                                        {
                                            string FormENo = row["BCA Form-E No"].ToString();
                                            EEFBCA.BCAD_FORM_E_NO = FormENo;
                                            if (row["BCA Running Serial Detail"] != null && row["BCA Running Serial Detail"].ToString() != "")
                                            {
                                                string serialDetail = row["BCA Running Serial Detail"].ToString();
                                                decimal SerialNo = 0;
                                                decimal.TryParse(serialDetail, out SerialNo);
                                                EEFBCA.BCAD_RUNNING_SERIAL_DETAIL = SerialNo;
                                                if (row["BCA Swift Reference"] != null && row["BCA Swift Reference"].ToString() != "")
                                                {
                                                    string SwiftCharges = row["BCA Swift Reference"].ToString();
                                                    EEFBCA.BCAD_SWIFT_REFERENCES = SwiftCharges;
                                                    if (row["BCA Bill No"] != null && row["BCA Bill No"].ToString() != "")
                                                    {
                                                        string BCABillNo = row["BCA Bill No"].ToString();
                                                        EEFBCA.BCAD_BILL_NO = BCABillNo;
                                                        if (row["BCA Bill Date"] != null && row["BCA Bill Date"].ToString() != "")
                                                        {
                                                            string BCABillDate = row["BCA Bill Date"].ToString();
                                                            DateTime bcabilldate = DateTime.ParseExact(BCABillDate, "dd/MM/yyyy", null);
                                                            //EEFBCA.BCAD_BILL_DATE = DateTime.ParseExact(BCABillDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                            EEFBCA.BCAD_BILL_DATE = bcabilldate;
                                                            if (row["BCA Bill Amount"] != null && row["BCA Bill Amount"].ToString() != "")
                                                            {
                                                                string BCABillAmount = row["BCA Bill Amount"].ToString();
                                                                EEFBCA.BCAD_BILL_AMOUNT = Convert.ToDecimal(BCABillAmount);
                                                                if (row["BCA Invoice No"] != null && row["BCA Invoice No"].ToString() != "")
                                                                {
                                                                    string BCAinvoiceNo = row["BCA Invoice No"].ToString();
                                                                    EEFBCA.BCAD_INVOICE_NO = BCAinvoiceNo;
                                                                    if (row["BCA Invoice Date"] != null && row["BCA Invoice Date"].ToString() != "")
                                                                    {
                                                                        string BCAinvoiceDate = row["BCA Invoice Date"].ToString();
                                                                        EEFBCA.BCAD_INVOICE_DATE = DateTime.ParseExact(BCAinvoiceDate, "dd/MM/yyyy", null);
                                                                        if (row["BCA Invoice Amount"] != null && row["BCA Invoice Amount"].ToString() != "")
                                                                        {
                                                                            string BCAinvoiceAmount = row["BCA Invoice Amount"].ToString();
                                                                            EEFBCA.BCAD_INVOICE_AMOUNT = Convert.ToDecimal(BCAinvoiceAmount);
                                                                            if (row["BCA Foriegn Bank Charges(FCY)"] != null && row["BCA Foriegn Bank Charges(FCY)"].ToString() != "")
                                                                            {
                                                                                string ForBankCharges = row["BCA Foriegn Bank Charges(FCY)"].ToString();
                                                                                EEFBCA.BCAD_FORIEGN_BANK_CHARGES_FCY = Convert.ToDecimal(ForBankCharges);
                                                                                if (row["BCA Agent Broke Comission(FCY)"] != null && row["BCA Agent Broke Comission(FCY)"].ToString() != "")
                                                                                {
                                                                                    string AgentCommisssion = row["BCA Agent Broke Comission(FCY)"].ToString();
                                                                                    EEFBCA.BCAD_AGENT_BROKE_COMMISSION_FCY = Convert.ToDecimal(AgentCommisssion);
                                                                                    if (row["BCA With Holding Tax(PKR)"] != null && row["BCA With Holding Tax(PKR)"].ToString() != "")
                                                                                    {
                                                                                        string WithholdingTax = row["BCA With Holding Tax(PKR)"].ToString();
                                                                                        EEFBCA.BCAD_WITH_HOLDING_TAX_PKR = Convert.ToDecimal(WithholdingTax);
                                                                                        if (row["EDS on BCA (PKR)"] != null && row["EDS on BCA (PKR)"].ToString() != "")
                                                                                        {
                                                                                            string EdsBCA = row["EDS on BCA (PKR)"].ToString();
                                                                                            EEFBCA.BCAD_EDS_PKR = Convert.ToDecimal(EdsBCA);
                                                                                            if (row["BCA(FC)"] != null && row["BCA(FC)"].ToString() != "")
                                                                                            {
                                                                                                decimal BCAFC = Convert.ToDecimal(row["BCA(FC)"].ToString());
                                                                                                EEFBCA.BCAD_BCA_FC = BCAFC;
                                                                                                if (row["BCA FCY Exchange Rate"] != null && row["BCA FCY Exchange Rate"].ToString() != "")
                                                                                                {
                                                                                                    decimal FCYExchangeRate = Convert.ToDecimal(row["BCA FCY Exchange Rate"].ToString());
                                                                                                    EEFBCA.BCAD_FCY_EXCHANGE_RATE = FCYExchangeRate;
                                                                                                    if (row["BCA Adjustment Form Special FCY Amount"] != null && row["BCA Adjustment Form Special FCY Amount"].ToString() != "")
                                                                                                    {
                                                                                                        decimal AdjustformSpecial = Convert.ToDecimal(row["BCA Adjustment Form Special FCY Amount"].ToString());
                                                                                                        EEFBCA.BCAD_AFJUSTMENT_FROM_SPECIAL_FCY_AMOUNT = AdjustformSpecial;
                                                                                                        if (row["Currency Of Realization is different from Form-E"] != null && row["Currency Of Realization is different from Form-E"].ToString() != "")
                                                                                                        {
                                                                                                            string CurrencyOfRealized = row["Currency Of Realization is different from Form-E"].ToString();
                                                                                                            if (CurrencyOfRealized == "1")
                                                                                                            {
                                                                                                                EEFBCA.BCAD_CURR_OF_REALIZATION = true;
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                EEFBCA.BCAD_CURR_OF_REALIZATION = false;
                                                                                                            }
                                                                                                            //EEFBCA.BCAD_CURR_OF_REALIZATION = Convert.ToBoolean(CurrencyOfRealized);
                                                                                                            if (row["BCA Currecy"] != null && row["BCA Currecy"].ToString() != "")
                                                                                                            {
                                                                                                                string BCACurrency = row["BCA Currecy"].ToString();
                                                                                                                PSW_CURRENCY totalbdacurrency = new PSW_CURRENCY();
                                                                                                                totalbdacurrency = context.PSW_CURRENCY.Where(m => m.CUR_NAME == BCACurrency).FirstOrDefault();
                                                                                                                if (totalbdacurrency != null)
                                                                                                                {
                                                                                                                    EEFBCA.BCAD_CURRENCY = totalbdacurrency.CUR_ID;
                                                                                                                    if (row["Full Amount is not Realized against this Export"] != null && row["Full Amount is not Realized against this Export"].ToString() != "")
                                                                                                                    {
                                                                                                                        string IsnotRealized = row["Full Amount is not Realized against this Export"].ToString();
                                                                                                                        if (IsnotRealized == "1")
                                                                                                                        {
                                                                                                                            EEFBCA.BCAD_FULL_AMOUNT_IS_NOT_REALIZED = true;
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            EEFBCA.BCAD_FULL_AMOUNT_IS_NOT_REALIZED = false;
                                                                                                                        }
                                                                                                                        //EEFBCA.BCAD_FULL_AMOUNT_IS_NOT_REALIZED = Convert.ToBoolean(IsnotRealized);
                                                                                                                        if (row["BCA Form-E Amount Realized"] != null && row["BCA Form-E Amount Realized"].ToString() != "")
                                                                                                                        {
                                                                                                                            string AmountRealized = row["BCA Form-E Amount Realized"].ToString();
                                                                                                                            EEFBCA.BCAD_FORM_AMOUNT_REALIZED = Convert.ToDecimal(AmountRealized);
                                                                                                                            if (row["Date of Realization"] != null && row["Date of Realization"].ToString() != "")
                                                                                                                            {
                                                                                                                                string DateOfRealization = row["Date of Realization"].ToString();
                                                                                                                                EEFBCA.BCAD_DATE_OF_REALIZED = DateTime.ParseExact(DateOfRealization, "dd/MM/yyyy", null);
                                                                                                                                if (row["BCA Balance"] != null && row["BCA Balance"].ToString() != "")
                                                                                                                                {
                                                                                                                                    string BCABalance = row["BCA Balance"].ToString();
                                                                                                                                    EEFBCA.BCAD_BALANCE = Convert.ToDecimal(BCABalance);
                                                                                                                                    if (row["BCA Allowed Discount"] != null && row["BCA Allowed Discount"].ToString() != "")
                                                                                                                                    {
                                                                                                                                        string BCAAllowedDiscount = row["BCA Allowed Discount"].ToString();
                                                                                                                                        EEFBCA.BCAD_ALLOWED_DISCOUNT = Convert.ToDecimal(BCAAllowedDiscount);
                                                                                                                                        if (row["BCA Allowed Discount in Percent"] != null && row["BCA Allowed Discount in Percent"].ToString() != "")
                                                                                                                                        {
                                                                                                                                            int BCAAllowedDiscountPercent = Convert.ToInt32(row["BCA Allowed Discount in Percent"].ToString());
                                                                                                                                            EEFBCA.BCAD_ALLOWED_DISCOUNT_PERCENT = BCAAllowedDiscountPercent;
                                                                                                                                            if (row["Remarks"] != null && row["Remarks"].ToString() != "")
                                                                                                                                            {
                                                                                                                                                string Remark = row["Remarks"].ToString();
                                                                                                                                                EEFBCA.BCAD_REMARKS = Remark;
                                                                                                                                                if (row["BCA Event Name"] != null && row["BCA Event Name"].ToString() != "")
                                                                                                                                                {
                                                                                                                                                    string EventName = row["BCA Event Name"].ToString();
                                                                                                                                                    PSW_BCA_EVENT GetEventName = new PSW_BCA_EVENT();
                                                                                                                                                    GetEventName = context.PSW_BCA_EVENT.Where(m => m.BCAE_NAME == EventName).FirstOrDefault();
                                                                                                                                                    if (GetEventName != null)
                                                                                                                                                    {
                                                                                                                                                        EEFBCA.BCAD_EVENTID = GetEventName.BCAE_ID;
                                                                                                                                                        if (row["BCA Event Date"] != null && row["BCA Event Date"].ToString() != "")
                                                                                                                                                        {
                                                                                                                                                            string EventDate = row["BCA Event Date"].ToString();
                                                                                                                                                            EEFBCA.BCAD_EVENT_DATE = DateTime.ParseExact(EventDate, "dd/MM/yyyy", null);
                                                                                                                                                            if (row["BCA Event Remarks"] != null && row["BCA Event Remarks"].ToString() != "")
                                                                                                                                                            {
                                                                                                                                                                string EventREmarks = row["BCA Event Remarks"].ToString();
                                                                                                                                                                EEFBCA.BCAD_EVENT_Remarks = EventREmarks;
                                                                                                                                                                if (row["BCA (PKR)"] != null && row["BCA (PKR)"].ToString() != "")
                                                                                                                                                                {
                                                                                                                                                                    string BCAPKR = row["BCA (PKR)"].ToString();
                                                                                                                                                                    EEFBCA.BCAD_BCA_PKR = Convert.ToDecimal(BCAPKR); /*GD No*/
                                                                                                                                                                    if (row["Flex Reference"] != null && row["Flex Reference"].ToString() != "")
                                                                                                                                                                    {
                                                                                                                                                                        string FlexNo = row["Flex Reference"].ToString();
                                                                                                                                                                        EEFBCA.BCAD_FLEX_REFERENCE = FlexNo;
                                                                                                                                                                        if (row["GD No"] != null && row["GD No"].ToString() != "")
                                                                                                                                                                        {
                                                                                                                                                                            string GD_NO = row["GD No"].ToString();
                                                                                                                                                                            EEFBCA.BCAD_GD_NO = GD_NO;
                                                                                                                                                                            EEFBCA.BCAD_EDS_UNIQUE_BANK_REF_NO = UniqueBankRefNo;
                                                                                                                                                                            if (!EefFormENoExist)
                                                                                                                                                                            {
                                                                                                                                                                                EEFBCA.BCAD_MAKER_ID = Session["USER_ID"].ToString();
                                                                                                                                                                                EEFBCA.BCAD_CHECKER_ID = null;
                                                                                                                                                                                EEFBCA.BCAD_BCA_NO = GenerateNewEEFBCANo();
                                                                                                                                                                                EEFBCA.BCAD_STATUS = "initiated";
                                                                                                                                                                                context.PSW_BCA_INFO.Add(EEFBCA);

                                                                                                                                                                                PSW_UNIQUE_BANK_REFERENCE_LOG UpdateLog = context.PSW_UNIQUE_BANK_REFERENCE_LOG.Where(m => m.URL_REFERENCE_NO == EEFBCA.BCAD_EDS_UNIQUE_BANK_REF_NO).FirstOrDefault();
                                                                                                                                                                                if (UpdateLog != null)
                                                                                                                                                                                {
                                                                                                                                                                                    UpdateLog.URL_STATUS = "Initiated";
                                                                                                                                                                                    context.Entry(UpdateLog).State = EntityState.Modified;
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                            else
                                                                                                                                                                            {
                                                                                                                                                                                EEFBCA.BCAD_MAKER_ID = Session["USER_ID"].ToString();
                                                                                                                                                                                EEFBCA.BCAD_CHECKER_ID = null;
                                                                                                                                                                                context.Entry(EEFBCA).State = EntityState.Modified;

                                                                                                                                                                                PSW_UNIQUE_BANK_REFERENCE_LOG UpdateLog = context.PSW_UNIQUE_BANK_REFERENCE_LOG.Where(m => m.URL_REFERENCE_NO == EEFBCA.BCAD_EDS_UNIQUE_BANK_REF_NO).FirstOrDefault();
                                                                                                                                                                                if (UpdateLog != null)
                                                                                                                                                                                {
                                                                                                                                                                                    UpdateLog.URL_STATUS = "Initiated";
                                                                                                                                                                                    context.Entry(UpdateLog).State = EntityState.Modified;
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                            RowCount += context.SaveChanges();
                                                                                                                                                                        }
                                                                                                                                                                        else
                                                                                                                                                                        {
                                                                                                                                                                            message += "Given GD No " + row["GD No"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                    else
                                                                                                                                                                    {
                                                                                                                                                                        message += "Given Flex Reference " + row["Flex Reference"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                else
                                                                                                                                                                {
                                                                                                                                                                    message += "Given BCA (PKR) " + row["BCA (PKR)"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                            else
                                                                                                                                                            {
                                                                                                                                                                message += "Given BCA Event Remarks " + row["BCA Event Remarks"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                        else
                                                                                                                                                        {
                                                                                                                                                            message += "Given BCA Event Date " + row["BCA Event Date"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                    else
                                                                                                                                                    {
                                                                                                                                                        message += "Given BCA Event Name " + row["BCA Event Name"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            else
                                                                                                                                            {
                                                                                                                                                message += "Invalid Remarks " + row["Remarks"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                        else
                                                                                                                                        {
                                                                                                                                            message += "Invalid BCA Allowed Discount in Percentt " + row["BCA Allowed Discount in Percent"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    else
                                                                                                                                    {
                                                                                                                                        message += "Invalid BCA Allowed Discount " + row["BCA Allowed Discount"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                else
                                                                                                                                {
                                                                                                                                    message += "Invalid BCA Balance " + row["BCA Balance"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                                                }
                                                                                                                            }
                                                                                                                            else
                                                                                                                            {
                                                                                                                                message += "Invalid Date of Realization " + row["Date of Realization"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                                            }
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            message += "Invalid BCA Form-E Amount Realized " + row["BCA Form-E Amount Realized"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                                        }
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        message += "Invalid Full Amount is not Realized against this Export " + row["Full Amount is not Realized against this Export"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                                    }
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    message += "Given BCA Currecy " + row["BCA Currecy"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                                                                }
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                message += "Invalid BCA Currecy " + row["BCA Currecy"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                            }
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            message += "Invalid BCA Adjustment Form Special FCY Amount " + row["BCA Adjustment Form Special FCY Amount"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        message += "Invalid BCA Adjustment Form Special FCY Amount " + row["BCA Adjustment Form Special FCY Amount"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    message += "Invalid BCA FCY Exchange Rate " + row["BCA FCY Exchange Rate"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                message += "Invalid BCA(FC) " + row["BCA(FC)"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            message += "Invalid EDS on BCA (PKR) " + row["EDS on BCA (PKR)"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        message += "Invalid BCA With Holding Tax(PKR) " + row["BCA With Holding Tax(PKR)"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    message += "Invalid BCA Agent Broke Comission(FCY) " + row["BCA Agent Broke Comission(FCY)"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                message += "Invalid BCA Foriegn Bank Charges(FCY) " + row["BCA Foriegn Bank Charges(FCY)"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "Invalid BCA Invoice Amount " + row["BCA Invoice Amount"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid BCA Invoice Date " + row["BCA Invoice Date"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid BCA Invoice No " + row["BCA Invoice No"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid BCA Bill Amount " + row["BCA Bill Amount"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid BCA Bill Date " + row["BCA Bill Date"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid BCA Bill No " + row["BCA Bill No"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid BCA Swift Reference " + row["BCA Swift Reference"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid BCA Running Serial Detail " + row["BCA Running Serial Detail"].ToString() + "  At BCA Form-E No # " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid BCA Form-E No  " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    else
                                    {
                                        message += "Invalid BCA Form-E No  " + row["BCA Form-E No"].ToString() + Environment.NewLine;
                                    }
                                }
                                else
                                {
                                    message += "No EEF Found In the System With the given EEF Number # " + EEFFormENo;
                                }
                            }
                            RowCount += context.SaveChanges();
                        }
                        if (RowCount > 0)
                        {
                            message += "Data Uploaded Successfully." + Environment.NewLine + RowCount + " Rows Affected";
                        }
                    }
                    else
                    {
                        message = "No Data Found In The File." + Environment.NewLine;
                    }
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("EEFRequest", "EEFBCABulkPortfolio", Session["USER_ID"].ToString(), ex);
                message += "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
            }
            return CreateLogFile(message);
        }

        public FileStreamResult CreateLogFile(string Message)
        {
            var byteArray = Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
        public string GenerateNewEEFBCANo()
        {
            string BDA_NO = "";
            int MAXID = Convert.ToInt32(context.PSW_BCA_INFO.Max(m => (decimal?)m.BCAD_ID)) + 1;
            BDA_NO = "CBN-BCA-" + String.Format("{0:000000}", MAXID) + "-" + DateTime.Now.ToString("ddMMyyyy");
            return BDA_NO;
        }

        
        #endregion

        #region GD Method not in use
        public ActionResult EEFIGMInfo(int I = 0)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    if (I != 0)
                    {
                        PSW_EEF_GD_INFORMATION edit = context.PSW_EEF_GD_INFORMATION.Where(m => m.EFGDI_ID == I).FirstOrDefault();
                        if (edit != null)
                        {
                            return View(edit);
                        }
                        else
                        {
                            message = "Exception Occur while fetching your record on User Id = " + I;
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                finally
                {
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EEFIGMInfo(PSW_EEF_GD_INFORMATION dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    PSW_EEF_GD_INFORMATION UpdateEntity = new PSW_EEF_GD_INFORMATION();

                    PSW_EEF_GD_INFORMATION CheckIFExist = new PSW_EEF_GD_INFORMATION();
                    if (dbtable.EFGDI_ID == 0)
                    {
                        UpdateEntity.EFGDI_ID = Convert.ToInt32(context.PSW_EEF_GD_INFORMATION.Max(m => (decimal?)m.EFGDI_ID)) + 1;
                        UpdateEntity.EFGDI_ENTRY_DATETIME = DateTime.Now;
                        CheckIFExist = null;
                    }
                    else
                    {
                        UpdateEntity = context.PSW_EEF_GD_INFORMATION.Where(m => m.EFGDI_ID == dbtable.EFGDI_ID).FirstOrDefault();
                        UpdateEntity.EFGDI_EDIT_DATETIME = DateTime.Now;
                        CheckIFExist = UpdateEntity;
                    }
                    UpdateEntity.EFGDI_EGM_COLLECTORATE = dbtable.EFGDI_EGM_COLLECTORATE;
                    UpdateEntity.EFGDI_EGM_NUMBER = dbtable.EFGDI_EGM_NUMBER;
                    UpdateEntity.EFGDI_EGM_EXPORTER_NAME = dbtable.EFGDI_EGM_EXPORTER_NAME;
                    UpdateEntity.EFGDI_VESSEL_NAME = dbtable.EFGDI_VESSEL_NAME;
                    UpdateEntity.EFGDI_GROSS_WEIGHT = dbtable.EFGDI_GROSS_WEIGHT;
                    UpdateEntity.EFGDI_COSIGNMENT_TYPE = dbtable.EFGDI_COSIGNMENT_TYPE;
                    UpdateEntity.EFGDI_SECTION = dbtable.EFGDI_SECTION;
                    UpdateEntity.EFGDI_INDEX_NO = dbtable.EFGDI_INDEX_NO;
                    UpdateEntity.EFGDI_BL_NO = dbtable.EFGDI_BL_NO;
                    UpdateEntity.EFGDI_BL_DATE = dbtable.EFGDI_BL_DATE;
                    UpdateEntity.EFGDI_PORT_OF_SHIPMENT = dbtable.EFGDI_PORT_OF_SHIPMENT;
                    UpdateEntity.EFGDI_NET_WEIGHT = dbtable.EFGDI_NET_WEIGHT;
                    UpdateEntity.EFGDI_SHIPPING_LINE = dbtable.EFGDI_SHIPPING_LINE;
                    UpdateEntity.EFGDI_BERTHING_TERMINAL = dbtable.EFGDI_BERTHING_TERMINAL;
                    UpdateEntity.EFGDI_MAKER_ID = "1";
                    UpdateEntity.EFGDI_CHECKER_ID = "1";
                    if (CheckIFExist == null)
                        context.PSW_EEF_GD_INFORMATION.Add(UpdateEntity);
                    else
                        context.Entry(UpdateEntity).State = EntityState.Modified;
                    int RowsAffected = context.SaveChanges();
                    if (RowsAffected > 0)
                    {
                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                        ModelState.Clear();
                    }
                    else
                    {
                        message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                finally
                {
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult EEFIGMInfoList()
        {
            return PartialView(context.PSW_EEF_GD_INFORMATION.ToList());
        }
        public ActionResult EEFGDImportExportInfo(int G = 0)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    if (G != 0)
                    {
                        PSW_EEF_GD_IMPORT_EXPORT_INFO edit = context.PSW_EEF_GD_IMPORT_EXPORT_INFO.Where(m => m.EFGDIE_ID == G).FirstOrDefault();
                        if (edit != null)
                        {
                            return View(edit);
                        }
                        else
                        {
                            message = "Exception Occur while fetching your record on User Id = " + G;
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                finally
                {
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EEFGDImportExportInfo(PSW_EEF_GD_IMPORT_EXPORT_INFO dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    PSW_EEF_GD_IMPORT_EXPORT_INFO UpdateEntity = new PSW_EEF_GD_IMPORT_EXPORT_INFO();

                    PSW_EEF_GD_IMPORT_EXPORT_INFO CheckIFExist = new PSW_EEF_GD_IMPORT_EXPORT_INFO();
                    if (dbtable.EFGDIE_ID == 0)
                    {
                        UpdateEntity.EFGDIE_ID = Convert.ToInt32(context.PSW_EEF_GD_IMPORT_EXPORT_INFO.Max(m => (decimal?)m.EFGDIE_ID)) + 1;
                        UpdateEntity.EFGDIE_ENTRY_DATETIME = DateTime.Now;
                        CheckIFExist = null;
                    }
                    else
                    {
                        UpdateEntity = context.PSW_EEF_GD_IMPORT_EXPORT_INFO.Where(m => m.EFGDIE_ID == dbtable.EFGDIE_ID).FirstOrDefault();
                        UpdateEntity.EFGDIE_EDIT_DATETIME = DateTime.Now;
                        CheckIFExist = UpdateEntity;
                    }
                    UpdateEntity.EFGDIE_NTN_FTN = dbtable.EFGDIE_NTN_FTN;
                    UpdateEntity.EFGDIE_STRN = dbtable.EFGDIE_STRN;
                    UpdateEntity.EFGDIE_CONSIGNEE_NAME = dbtable.EFGDIE_CONSIGNEE_NAME;
                    UpdateEntity.EFGDIE_CONSIGNEE_ADDRESS = dbtable.EFGDIE_CONSIGNEE_ADDRESS;
                    UpdateEntity.EFGDIE_CONSIGNOR_NAME = dbtable.EFGDIE_CONSIGNOR_NAME;
                    UpdateEntity.EFGDIE_CONSIGNOR_ADDRESS = dbtable.EFGDIE_CONSIGNOR_ADDRESS;
                    UpdateEntity.EFGDIE_MAKER_ID = "1";
                    UpdateEntity.EFGDIE_CHECKER_ID = "1";
                    if (CheckIFExist == null)
                        context.PSW_EEF_GD_IMPORT_EXPORT_INFO.Add(UpdateEntity);
                    else
                        context.Entry(UpdateEntity).State = EntityState.Modified;
                    int RowsAffected = context.SaveChanges();
                    if (RowsAffected > 0)
                    {
                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                        ModelState.Clear();
                    }
                    else
                    {
                        message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                finally
                {
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult EEFGDImportExportInfoList()
        {
            return PartialView(context.PSW_EEF_GD_IMPORT_EXPORT_INFO.ToList());
        }
        public ActionResult EEFGDGeneralInfo(int G = 0)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    if (G != 0)
                    {
                        PSW_EEF_GD_GENERAL_INFO edit = context.PSW_EEF_GD_GENERAL_INFO.Where(m => m.EFGDGI_ID == G).FirstOrDefault();
                        if (edit != null)
                        {
                            return View(edit);
                        }
                        else
                        {
                            message = "Exception Occur while fetching your record on User Id = " + G;
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                finally
                {
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EEFGDGeneralInfo(PSW_EEF_GD_GENERAL_INFO dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    PSW_EEF_GD_GENERAL_INFO UpdateEntity = new PSW_EEF_GD_GENERAL_INFO();

                    PSW_EEF_GD_GENERAL_INFO CheckIFExist = new PSW_EEF_GD_GENERAL_INFO();
                    if (dbtable.EFGDGI_ID == 0)
                    {
                        UpdateEntity.EFGDGI_ID = Convert.ToInt32(context.PSW_EEF_GD_GENERAL_INFO.Max(m => (decimal?)m.EFGDGI_ID)) + 1;
                        UpdateEntity.EFGDGI_ENTRY_DATETIME = DateTime.Now;
                        CheckIFExist = null;
                    }
                    else
                    {
                        UpdateEntity = context.PSW_EEF_GD_GENERAL_INFO.Where(m => m.EFGDGI_ID == dbtable.EFGDGI_ID).FirstOrDefault();
                        UpdateEntity.EIGDGI_EDIT_DATETIME = DateTime.Now;
                        CheckIFExist = UpdateEntity;
                    }
                    UpdateEntity.EFGDGI_AGENT_NAME = dbtable.EFGDGI_AGENT_NAME;
                    UpdateEntity.EFGDGI_TRADE_TYPE = dbtable.EFGDGI_TRADE_TYPE;
                    UpdateEntity.EFGDGI_GD_NO = dbtable.EFGDGI_GD_NO;
                    UpdateEntity.EFGDGI_COLLECTORATE = dbtable.EFGDGI_COLLECTORATE;
                    UpdateEntity.EFGDGI_DESTINATION_COUNTRY = dbtable.EFGDGI_DESTINATION_COUNTRY;
                    UpdateEntity.EFGDGI_PLACE_OF_DELIVERY = dbtable.EFGDGI_PLACE_OF_DELIVERY;
                    UpdateEntity.EFGDGI_GENERAL_DESCRIPTION = dbtable.EFGDGI_GENERAL_DESCRIPTION;
                    UpdateEntity.EFGDGI_SHED_TERMINAL_LOCATION = dbtable.EFGDGI_SHED_TERMINAL_LOCATION;
                    UpdateEntity.EFGDGI_AGENT_LICENSE_NO = dbtable.EFGDGI_AGENT_LICENSE_NO;
                    UpdateEntity.EFGDGI_DECLARATION_TYPE = dbtable.EFGDGI_DECLARATION_TYPE;
                    UpdateEntity.EFGDGI_CONSIGNMENT_CATE = dbtable.EFGDGI_CONSIGNMENT_CATE;
                    UpdateEntity.EFGDGI_GD_TYPE = dbtable.EFGDGI_GD_TYPE;
                    UpdateEntity.EFGDGI_PORT_OF_DISCHARGE = dbtable.EFGDGI_PORT_OF_DISCHARGE;
                    UpdateEntity.EFGDGI_1ST_EXAMINATION = dbtable.EFGDGI_1ST_EXAMINATION;
                    UpdateEntity.EFGDGI_MAKER_ID = "1";
                    UpdateEntity.EFGDGI_CHECKER_ID = "1";
                    if (CheckIFExist == null)
                        context.PSW_EEF_GD_GENERAL_INFO.Add(UpdateEntity);
                    else
                        context.Entry(UpdateEntity).State = EntityState.Modified;
                    int RowsAffected = context.SaveChanges();
                    if (RowsAffected > 0)
                    {
                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                        ModelState.Clear();
                    }
                    else
                    {
                        message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                finally
                {
                    ViewBag.message = message;
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult EEFGDGeneralInfoList()
        {
            return PartialView(context.PSW_EEF_GD_GENERAL_INFO.ToList());
        }
        #endregion

        #region EEF GD Main View
        public ActionResult GDs()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "EEFGD", Session["USER_ID"].ToString()))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult EEFGD(EEFGDFilter edit, int ID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "EEFGD", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_EEF_GD_GENERAL_INFO_LIST> List = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).ToList();
                            if (List.Count() == 0)
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (ID != 0)
                        {
                            edit.Entity = context.PSW_EEF_GD_GENERAL_INFO.Where(m => m.EFGDGI_ID == ID).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + ID;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "EEFGD", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EEFGD(EEFGDFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "EEFGD", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string PreviousStatus = "";
                    try
                    {
                        if (db_table.Entity.EFGDGI_GD_STATUS != null)
                        {
                            PSW_EEF_GD_GENERAL_INFO UpdateEntity = context.PSW_EEF_GD_GENERAL_INFO.Where(m => m.EFGDGI_ID == db_table.Entity.EFGDGI_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                PSW_EEF_GD_GENERAL_INFO EntityToUpdate = new PSW_EEF_GD_GENERAL_INFO();
                                PreviousStatus = UpdateEntity.EFGDGI_GD_STATUS;
                                EntityToUpdate = UpdateEntity;
                                if (UpdateEntity != null && PreviousStatus != db_table.Entity.EFGDGI_GD_STATUS && db_table.Entity.EFGDGI_GD_STATUS != null)
                                {
                                    if (DAL.DO_EDI == "true")
                                    {
                                        message += DAL.MethodId1541(EntityToUpdate.EFGDGI_GD_NO, db_table.Entity.EFGDGI_GD_STATUS, false) + Environment.NewLine;
                                        //message += "  RAW REQUEST DETAILS  -------------------- " + Environment.NewLine;
                                        //message += DAL.message;
                                        if (message.Contains("200"))
                                        {
                                            EntityToUpdate = UpdateEntity;
                                            EntityToUpdate.EFGDGI_GD_STATUS = db_table.Entity.EFGDGI_GD_STATUS;
                                            EntityToUpdate.EIGDGI_EDIT_DATETIME = DateTime.Now;
                                            context.Entry(EntityToUpdate).State = EntityState.Modified;
                                            int RowCount = context.SaveChanges();
                                            if (RowCount > 0)
                                                message += "GD status updated successfully";
                                            else
                                                message += "Exception occur while updating GD status";
                                        }
                                    }
                                    else
                                        message += "EDI is turned off from system configuration";
                                }
                            }
                            else
                            {
                                message = "Exception Occur while fetching record";
                            }
                        }
                        else
                        {
                            message = "Select GD status to continue";
                            return View(db_table);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "EEFGD", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult EEFGDFilter(EEFGDFilter edit, int ID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "EEFGD", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_EEF_GD_GENERAL_INFO_LIST> List = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).ToList();
                            if (List.Count() == 0)
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("EEFGD", edit);
        }
        [HttpPost]
        public ActionResult EEFGDFilter(EEFGDFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "EEFGD", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_EEF_GD_GENERAL_INFO_LIST> List = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).ToList();
                            if (List.Count() == 0)
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("EEFGD", edit);
        }
        public PartialViewResult GetAllGDsList(EEFGDFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "EEFGD", Session["USER_ID"].ToString()))
                {
                    List<PSW_EEF_GD_GENERAL_INFO_LIST> Entity = new List<PSW_EEF_GD_GENERAL_INFO_LIST>();
                    if (Filter.FromDate != null && Filter.ToDate != null)
                        Entity = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).ToList();
                    else
                        Entity = context.PSW_EEF_GD_GENERAL_INFO_LIST.ToList();
                    return PartialView(Entity);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult GetEEFGDView(int GD_ID)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "EEFGD", Session["USER_ID"].ToString()))
                {
                    GD_Export entity = new GD_Export();
                    PSW_EEF_GD_GENERAL_INFO MasterEntity = context.PSW_EEF_GD_GENERAL_INFO.Where(m => m.EFGDGI_ID == GD_ID).FirstOrDefault();
                    entity.GeneralInfo = context.PSW_EEF_GD_GENERAL_INFO.Where(m => m.EFGDGI_GD_NO == MasterEntity.EFGDGI_GD_NO && m.EFGDGI_AMENDMENT_NO == MasterEntity.EFGDGI_AMENDMENT_NO).FirstOrDefault();
                    entity.AssignFormE = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_GD_NO == entity.GeneralInfo.EFGDGI_GD_NO && m.GDAEF_AMENDMENT_NO == entity.GeneralInfo.EFGDGI_AMENDMENT_NO).ToList();
                    entity.GD_Information = context.PSW_EEF_GD_INFORMATION.Where(m => m.EFGDI_GD_NO == entity.GeneralInfo.EFGDGI_GD_NO && m.EFGDI_AMENDMENT_NO == entity.GeneralInfo.EFGDGI_AMENDMENT_NO).FirstOrDefault();
                    entity.IE_Info = context.PSW_EEF_GD_IMPORT_EXPORT_INFO.Where(m => m.EFGDIE_GD_NO == entity.GeneralInfo.EFGDGI_GD_NO && m.EFGDIE_AMENDMENT_NO == entity.GeneralInfo.EFGDGI_AMENDMENT_NO).FirstOrDefault();
                    entity.Package_Info = context.PSW_GD_EXPORT_PACKAGE_INFORMATION.Where(m => m.EPI_GD_NO == entity.GeneralInfo.EFGDGI_GD_NO && m.EPI_AMENDMENT_NO == entity.GeneralInfo.EFGDGI_AMENDMENT_NO).ToList();
                    entity.Container_Info = context.PSW_GD_EXPORT_CONTAINER_INFORMATION.Where(m => m.CIE_GD_NO == entity.GeneralInfo.EFGDGI_GD_NO && m.CIE_AMENDMENT_NO == entity.GeneralInfo.EFGDGI_AMENDMENT_NO).ToList();
                    entity.Hs_Code = context.PSW_GD_EXPORT_HS_CODE.Where(m => m.EHC_GD_NO == entity.GeneralInfo.EFGDGI_GD_NO && m.EHC_AMENDMENT_NO == entity.GeneralInfo.EFGDGI_AMENDMENT_NO).ToList();
                    return PartialView(entity);
                }
                else
                {
                    return null;/*RedirectToAction("UnAuthorizedUrl", "Error");*/
                }
            }
            else
            {
                return null;/*RedirectToAction("Index", "Authentication");*/
            }
        }
        public PartialViewResult LoadUnpinnedGds()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    List<string> GD_NOs = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM == "" || m.GDAEF_E_FORM == null).Select(m => m.GDAEF_GD_NO).ToList();
                    List<PSW_EEF_GD_GENERAL_INFO_LIST> Entity = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => GD_NOs.Contains(m.EFGDGI_GD_NO)).ToList();
                    return PartialView(Entity);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public ActionResult ExportEEFGD(int AM = 0, string GD_NO = "")
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    var EEFGDMASTER = context.PSW_EEF_GD_FOR_DOWNLOAD.Where(m => m.EFGDGI_GD_NO == GD_NO && m.EFGDGI_AMENDMENT_NO == AM).FirstOrDefault();
                    var EEFGDHSCode = context.PSW_GD_EXPORT_HS_CODE_VIEW.Where(m => m.EHC_GD_NO == GD_NO && m.EHC_AMENDMENT_NO == AM).ToList();
                    ReportDocument rd = new ReportDocument();
                    if(EEFGDMASTER != null)
                    {
                        DateTime InnDate = Convert.ToDateTime("1900-01-01");
                        if (EEFGDMASTER.EFGDGI_CLEARANCE_DATE != null && EEFGDMASTER.EFGDGI_CLEARANCE_DATE != InnDate)
                            rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EEF_GD_FOR_DOWNLOAD.rpt"));
                        else
                            rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EEF_GD_FOR_DOWNLOAD_WITHOUT_CLEAR_DATE.rpt"));
                    }
                    else
                        rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EEF_GD_FOR_DOWNLOAD.rpt"));
                    rd.DataSourceConnections.Clear();
                    rd.SetDataSource(new[] { EEFGDMASTER });
                    rd.Subreports[0].DataSourceConnections.Clear();
                    rd.Subreports[0].SetDataSource(EEFGDHSCode);

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "PSW-EXPORT-GD(" + GD_NO + ").pdf");
                }
                else
                {
                    return RedirectToAction("Index", "Authentication");
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured :" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception : " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
                return CreateLogFile(message);
            }
        }
        #endregion

        #region Export Consignee List
        public ActionResult ExportConsignee(int E = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "ExportConsignee", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EXPORT_CONSIGNEE_LIST edit = new PSW_EXPORT_CONSIGNEE_LIST();
                        if (E != 0)
                        {
                            edit = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_ID == E).FirstOrDefault();
                            return View(edit);
                        }
                        else
                        {
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "ExportConsignee", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ExportConsignee(PSW_EXPORT_CONSIGNEE_LIST Entity)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "ExportConsignee", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    try
                    {
                        PSW_EXPORT_CONSIGNEE_LIST UpdateEntity = new PSW_EXPORT_CONSIGNEE_LIST();
                        PSW_EXPORT_CONSIGNEE_LIST CheckIfExist = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_FI_NUMBER == Entity.ECL_FI_NUMBER && m.ECL_CONSIGNEE_NAME == Entity.ECL_CONSIGNEE_NAME && m.ECL_ID != Entity.ECL_ID).FirstOrDefault();
                        if (CheckIfExist == null)
                        {
                            if (Entity.ECL_ID == 0)
                            {
                                UpdateEntity = new PSW_EXPORT_CONSIGNEE_LIST();
                                UpdateEntity.ECL_ID = Convert.ToInt32(context.PSW_EXPORT_CONSIGNEE_LIST.Max(m => (decimal?)m.ECL_ID)) + 1;
                                UpdateEntity.ECL_FI_NUMBER = Entity.ECL_FI_NUMBER;
                                UpdateEntity.ECL_CONSIGNEE_NAME = Entity.ECL_CONSIGNEE_NAME;
                                UpdateEntity.ECL_MAKER_ID = Session["USER_ID"].ToString();
                                if (UpdateEntity.ECL_MAKER_ID == "Admin123")
                                {
                                    UpdateEntity.ECL_ISAUTH = true;
                                    UpdateEntity.ECL_CHECKER_ID = UpdateEntity.ECL_MAKER_ID;
                                }
                                else
                                {
                                    UpdateEntity.ECL_ISAUTH = false;
                                    UpdateEntity.ECL_CHECKER_ID = null;
                                }
                                UpdateEntity.ECL_IS_SHARED = false;
                                UpdateEntity.ECL_ENTRY_DATETIME = DateTime.Now;
                                UpdateEntity.ECL_EDIT_DATETIME = null;

                                context.PSW_EXPORT_CONSIGNEE_LIST.Add(UpdateEntity);
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Data inserted successfully " + RowCount + " row affected.";
                                    ModelState.Clear();
                                }
                                else
                                    message = "Unable to update data of selected FI # " + Entity.ECL_FI_NUMBER;
                            }
                            else
                            {
                                UpdateEntity = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_ID == Entity.ECL_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.ECL_FI_NUMBER = Entity.ECL_FI_NUMBER;
                                    UpdateEntity.ECL_CONSIGNEE_NAME = Entity.ECL_CONSIGNEE_NAME;
                                    UpdateEntity.ECL_MAKER_ID = Session["USER_ID"].ToString();
                                    if (UpdateEntity.ECL_MAKER_ID == "Admin123")
                                    {
                                        UpdateEntity.ECL_ISAUTH = true;
                                        UpdateEntity.ECL_CHECKER_ID = UpdateEntity.ECL_MAKER_ID;
                                    }
                                    else
                                    {
                                        UpdateEntity.ECL_ISAUTH = false;
                                        UpdateEntity.ECL_CHECKER_ID = null;
                                    }
                                    UpdateEntity.ECL_IS_SHARED = false;
                                    UpdateEntity.ECL_EDIT_DATETIME = DateTime.Now;

                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        message = "FI # " + Entity.ECL_FI_NUMBER + " data is successfully updated.";
                                        ModelState.Clear();
                                    }
                                    else
                                        message = "Unable to update data of selected FI # " + Entity.ECL_FI_NUMBER;
                                }
                                else
                                {
                                    message = "Problem while fetching record.";
                                }
                            }
                        }
                        else
                        {
                            message = "Consignee Name : " + CheckIfExist.ECL_CONSIGNEE_NAME + " is already exist on Fi # " + CheckIfExist.ECL_FI_NUMBER;
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "ExportConsignee", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult ExportConsigneeList(string FiNumber)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EEFRequest", "ExportConsignee", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<PSW_EXPORT_CONSIGNEE_LIST> DataList = new List<PSW_EXPORT_CONSIGNEE_LIST>();
                    if (FiNumber != null && FiNumber != "")
                    {
                        DataList = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_MAKER_ID != SessionUser || m.ECL_ISAUTH == true).OrderByDescending(m => m.ECL_ID).ToList();
                        if (DataList.Count() > 0)
                            DataList = DataList.Where(m => m.ECL_FI_NUMBER == FiNumber).ToList();
                    }
                    else
                        DataList = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_MAKER_ID != SessionUser || m.ECL_ISAUTH == true).OrderByDescending(m => m.ECL_ID).ToList();
                    return PartialView(DataList);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeExportConsignee(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountType", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EXPORT_CONSIGNEE_LIST UpdateEntity = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.ECL_ISAUTH = true;
                            UpdateEntity.ECL_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.ECL_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Consignee Name : " + UpdateEntity.ECL_CONSIGNEE_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectExportConsignee(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountType", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EXPORT_CONSIGNEE_LIST UpdateEntity = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_EXPORT_CONSIGNEE_LIST.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Consignee Name : " + UpdateEntity.ECL_CONSIGNEE_NAME + "  on FI # " + UpdateEntity.ECL_FI_NUMBER + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public ActionResult ShareExportConsignee(string FiNumber)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientInfo", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_TOKENS token = DAL.CheckTokenAuthentication();
                        if (token != null && token.PT_TOKEN != null)
                        {
                            if (DAL.DO_EDI == "true")
                            {
                                message = DAL.MethodId1550_or_1551(FiNumber) + Environment.NewLine;
                                if (message.Contains("200"))
                                {
                                    List<PSW_EXPORT_CONSIGNEE_LIST> GetExportConsignee = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_FI_NUMBER == FiNumber && m.ECL_ISAUTH == true).ToList();
                                    if (GetExportConsignee.Count() > 0)
                                    {
                                        foreach (PSW_EXPORT_CONSIGNEE_LIST item in GetExportConsignee)
                                        {
                                            PSW_EXPORT_CONSIGNEE_LIST Update = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_ID == item.ECL_ID).FirstOrDefault();
                                            if (Update != null)
                                            {
                                                Update.ECL_IS_SHARED = true;
                                                context.Entry(Update).State = EntityState.Modified;
                                            }
                                        }
                                        context.SaveChanges();
                                    }
                                }
                            }
                            else
                                message += "EDI is turned off from system configuration";
                        }
                        else
                        {
                            message = "Error While Generating Tokens.";
                        }

                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EEFRequest", "ShareExportConsignee", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    PSW_EXPORT_CONSIGNEE_LIST exportconsignee = new PSW_EXPORT_CONSIGNEE_LIST();
                    return View("ExportConsignee", exportconsignee);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        #endregion
    }
}
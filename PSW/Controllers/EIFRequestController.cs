﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PSW.Models;
using CrystalDecisions.CrystalReports.Engine;

namespace PSW.Controllers
{
    public class EIFRequestController : Controller
    {
        PSWEntities context = new PSWEntities();

        public ActionResult Index(EIFFilter dbtable)
        {
            string DisplayStatus = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (dbtable.EIF_BASIC_INFO != null)
                        {
                            if (dbtable.EIF_BASIC_INFO.EIF_BI_APPROVAL_STATUS != null && dbtable.EIF_BASIC_INFO.EIF_BI_APPROVAL_STATUS != "0")
                            {
                                DisplayStatus = DAL.GetFilterationStatus("EIF").Where(m => m.Value == dbtable.EIF_BASIC_INFO.EIF_BI_APPROVAL_STATUS).FirstOrDefault().Text;
                                return View(dbtable);
                            }
                        }
                        else
                        {
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                            if (ex.InnerException.InnerException != null)
                            {
                                message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                            }
                        }
                    }
                    finally
                    {
                        List<SelectListItem> Status = DAL.GetFilterationStatus("EIF");
                        Status.Insert(0, (new SelectListItem { Text = "--Select Status--", Value = "0" }));
                        ViewBag.EIF_BI_APPROVAL_STATUS = Status;
                        ViewBag.DisplayStatus = DisplayStatus;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public ActionResult ManageEIF(string E = "", int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    EIF loadInfo = new EIF();
                    string message = "";
                    try
                    {
                        if (E != "")
                        {
                            ViewBag.InitiatedEIF = "Edit";
                            loadInfo.basic = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                            loadInfo.documents = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == E && m.EIF_DRI_AMENDMENT_NO == M).FirstOrDefault();
                            loadInfo.hscodes = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == M).ToList();
                            loadInfo.bank = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == E && m.EIF_BA_AMENDMENT_NO == M).FirstOrDefault();
                            loadInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();
                            loadInfo.CustomNew = false;
                            int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == loadInfo.basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_ID;
                            List<int?> assignPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == ClientID && m.APMC_STATUS == true).Select(m => m.APMC_PAY_MODE_ID).ToList();
                            List<SelectListItem> PayMode = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => assignPaymodes.Contains(m.APM_ID) && m.APM_TYPE_ID == 1 && m.APM_ISAUTH == true).Select(f => new
                            {
                                f.APM_ID,
                                APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                            }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                            PayMode.Insert(0, (new SelectListItem { Text = "--Select Mode of Import Payment--", Value = "0" }));
                            loadInfo.AuthPayModes = PayMode;
                            return View(loadInfo);
                        }
                        else
                        {
                            string REQ = "";
                            ViewBag.InitiatedEIF = "New";
                            try
                            {
                                using (PSWEntities cont = new PSWEntities())
                                {
                                    PSW_EIF_BASIC_INFO basic = new PSW_EIF_BASIC_INFO();
                                    basic.EIF_BI_ID = Convert.ToInt32(cont.PSW_EIF_BASIC_INFO.Max(m => (decimal?)m.EIF_BI_ID)) + 1;
                                    basic.EIF_BI_EIF_REQUEST_NO = "";
                                    basic.EIF_BI_INTERNAL_REF = "";
                                    REQ = basic.EIF_BI_EIF_REQUEST_NO;
                                    basic.EIF_BI_REQUEST_DATE = DateTime.Now.AddHours(5);
                                    basic.EIF_BI_ENTRY_DATETIME = DateTime.Now.AddHours(5);
                                    basic.EIF_BI_APPROVAL_STATUS = "Initiated";
                                    basic.EIF_BI_STATUS_DATE = DateTime.Now.AddHours(5);
                                    basic.EIF_BI_MAKER_ID = Session["USER_ID"].ToString();
                                    basic.EIF_BI_AMENDMENT_NO = 0;
                                    basic.EIF_BI_CERTIFIED = true;
                                    basic.EIF_BI_DOC_SUBMISSION = true;

                                    PSW_EIF_DOCUMENT_REQUEST_INFO docinfo = new PSW_EIF_DOCUMENT_REQUEST_INFO();
                                    docinfo.EIF_DRI_ID = Convert.ToInt32(cont.PSW_EIF_DOCUMENT_REQUEST_INFO.Max(m => (decimal?)m.EIF_DRI_ID)) + 1;
                                    docinfo.EIF_DRI_BI_EIF_REQUEST_NO = basic.EIF_BI_EIF_REQUEST_NO;
                                    docinfo.EIF_DRI_ENTRY_DATETIME = basic.EIF_BI_ENTRY_DATETIME;
                                    docinfo.EIF_DRI_MAKER_ID = basic.EIF_BI_MAKER_ID;
                                    docinfo.EIF_DRI_AMENDMENT_NO = 0;

                                    PSW_EIF_BANK_INFO bankInfo = new PSW_EIF_BANK_INFO();
                                    bankInfo.EIF_BA_ID = Convert.ToInt32(cont.PSW_EIF_BANK_INFO.Max(m => (decimal?)m.EIF_BA_ID)) + 1;
                                    bankInfo.EIF_BA_EIF_REQUEST_NO = basic.EIF_BI_EIF_REQUEST_NO;
                                    bankInfo.EIF_BA_ENTRY_DATETIME = basic.EIF_BI_ENTRY_DATETIME;
                                    bankInfo.EIF_BA_MAKER_ID = basic.EIF_BI_MAKER_ID;
                                    bankInfo.EIF_BA_AMENDMENT_NO = 0;
                                    bankInfo.EIF_BA_CITY = "Karachi";
                                    bankInfo.EIF_BA_BRANCH = "AWT Plaza";
                                    bankInfo.EIF_BA_BANK = "Citibank N.A. Pakistan";

                                    PSW_PAYMENT_MODES_PARAMETERS Param = new PSW_PAYMENT_MODES_PARAMETERS();
                                    Param.EIF_PMP_ID = Convert.ToInt32(cont.PSW_PAYMENT_MODES_PARAMETERS.Max(m => (decimal?)m.EIF_PMP_ID)) + 1;
                                    Param.EIF_PMP_EIF_REQUEST_NO = basic.EIF_BI_EIF_REQUEST_NO;
                                    Param.EIF_PMP_ENTRY_DATETIME = basic.EIF_BI_ENTRY_DATETIME;
                                    Param.EIF_PMP_MAKER_ID = basic.EIF_BI_MAKER_ID;
                                    Param.EIF_PMP_AMENDMENT_NO = 0;

                                    loadInfo.basic = basic;
                                    loadInfo.documents = docinfo;
                                    loadInfo.bank = bankInfo;
                                    loadInfo.PayParam = Param;
                                    loadInfo.CustomNew = true;
                                    List<SelectListItem> PayMode = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 1 && m.APM_ISAUTH == true).Select(f => new
                                    {
                                        f.APM_ID,
                                        APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                                    }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                                    PayMode.Insert(0, (new SelectListItem { Text = "--Select Mode of Import Payment--", Value = "0" }));
                                    loadInfo.AuthPayModes = PayMode;
                                    return View(loadInfo);
                                }
                            }
                            catch (Exception ex)
                            {
                                message = ex.Message;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("EIFRequest", "ManageEIF", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                    }
                    finally
                    {
                        int?[] ClientIds = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(x => x.APT_PROFILE_ID == 1).Select(x => x.APT_CLIENT_ID).ToArray();
                        List<SelectListItem> Customers = new SelectList(context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_STATUS == true && x.C_ISAUTH == true && x.C_ACCOUNT_STATUS_ID == 1 && ClientIds.Contains(x.C_ID)).Select(f => new
                        {
                            f.C_IBAN_NO,
                            C_NAME = f.C_NAME + " -- (" + f.C_IBAN_NO + ")"
                        }).ToList(), "C_IBAN_NO", "C_NAME", 0).ToList();
                        Customers.Insert(0, (new SelectListItem { Text = "--Select Business Name For Import --", Value = "0" }));
                        loadInfo.ListCustomers = Customers;
                        //List<SelectListItem> LisBanks = new SelectList(context.PSW_LIST_OF_BANKS.Where(x => x.LB_BANK_STATUS == true).ToList(), "LB_BANK_ID", "LB_BANK_NAME", 0).ToList();
                        //LisBanks.Insert(0, (new SelectListItem { Text = "--Select Bank --", Value = "0" }));
                        //loadInfo.ListBanks = LisBanks;
                        //List<SelectListItem> lisCitis = new SelectList(context.PSW_LIST_OF_CITIES.Where(x => x.LOCT_STATUS == true).ToList(), "LOCT_ID", "LOCT_NAME", 0).ToList();
                        //lisCitis.Insert(0, (new SelectListItem { Text = "--Select City --", Value = "0" }));
                        //loadInfo.ListCitis = lisCitis;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }

        [HttpPost]
        public ActionResult ManageEIF(EIF eif)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    EIF loadInfo = eif;
                    string message = "";
                    try
                    {
                        int rowcount = 0;
                        using (PSWEntities cont = new PSWEntities())
                        {
                            using (var transaction = cont.Database.BeginTransaction())
                            {
                                try
                                {
                                    if (eif.basic.EIF_BI_EIF_REQUEST_NO != null)
                                    {
                                        List<PSW_EIF_HS_CODE> checkHsCode = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == eif.basic.EIF_BI_EIF_REQUEST_NO).ToList();
                                        if (checkHsCode.Count() == 0)
                                        {
                                            message = "No Hs Code Found in the FI (Import)";
                                            return View(eif);
                                        }
                                    }
                                    else
                                    {
                                        message = "No Hs Code Found in the FI (Import)";
                                        return View(eif);
                                    }
                                    if (eif.basic.EIF_BI_BUSSINESS_NAME == "0")
                                    {
                                        message = "Select Bussiness Name";
                                        return View(eif);
                                    }
                                    PSW_EIF_BASIC_INFO basic = cont.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == eif.basic.EIF_BI_EIF_REQUEST_NO && m.EIF_BI_AMENDMENT_NO == eif.basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                                    int ClientId = 0;
                                    if (eif.basic.EIF_BI_BUSSINESS_NAME != "0")
                                    {
                                        ClientId = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eif.basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_ID;
                                    }
                                    else
                                    {
                                        message = "Invalid Client";
                                        return View(eif);
                                    }
                                    PSW_CLIENT_INFO client = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientId).FirstOrDefault();
                                    PSW_EIF_DOCUMENT_REQUEST_INFO docinfo = cont.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == eif.basic.EIF_BI_EIF_REQUEST_NO && m.EIF_DRI_AMENDMENT_NO == eif.basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                                    PSW_EIF_BANK_INFO bankInfo = cont.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == eif.basic.EIF_BI_EIF_REQUEST_NO && m.EIF_BA_AMENDMENT_NO == eif.basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                                    PSW_AUTH_PAY_MODES PayMode = null;
                                    PSW_PAYMENT_MODES_PARAMETERS PayParam = null;
                                    if (eif.basic.EIF_BI_MODE_OF_IMPORT_PAYMENT != 0)
                                    {
                                        PayMode = new PSW_AUTH_PAY_MODES();
                                        PayParam = new PSW_PAYMENT_MODES_PARAMETERS();
                                        PayMode = cont.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eif.basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                                        PayParam = cont.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == eif.basic.EIF_BI_EIF_REQUEST_NO && m.EIF_PMP_AMENDMENT_NO == eif.basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                                    }
                                    else
                                    {
                                        message = "Invalid Mode of Payment";
                                        return View(eif);
                                    }
                                    if (basic == null)
                                    {
                                        basic = new PSW_EIF_BASIC_INFO();
                                        basic.EIF_BI_ID = Convert.ToInt32(cont.PSW_EIF_BASIC_INFO.Max(m => (decimal?)m.EIF_BI_ID)) + 1;
                                        basic.EIF_BI_REQUEST_DATE = eif.basic.EIF_BI_REQUEST_DATE;
                                        basic.EIF_BI_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                        basic.EIF_BI_INTERNAL_REF = eif.basic.EIF_BI_INTERNAL_REF;
                                        eif.basic.EIF_BI_EIF_REQUEST_NO = basic.EIF_BI_EIF_REQUEST_NO;
                                        basic.EIF_BI_ENTRY_DATETIME = eif.basic.EIF_BI_STATUS_DATE;
                                        basic.EIF_BI_APPROVAL_STATUS = "Initiated";
                                        basic.EIF_BI_STATUS_DATE = eif.basic.EIF_BI_STATUS_DATE;
                                        basic.EIF_BI_MAKER_ID = Session["USER_ID"].ToString();
                                        if (basic.EIF_BI_MAKER_ID == "Admin123")
                                            basic.EIF_BI_CHECKER_ID = basic.EIF_BI_MAKER_ID;
                                        else
                                            basic.EIF_BI_CHECKER_ID = null;
                                        basic.EIF_BI_NTN = client.C_NTN_NO;
                                        basic.EIF_BI_BUSSINESS_NAME = eif.basic.EIF_BI_BUSSINESS_NAME;
                                        basic.EIF_BI_BUSSINESS_ADDRESS = client.C_ADDRESS;
                                        basic.EIF_BI_IS_REMITTANCE_FROM_PAKISTAN = eif.basic.EIF_BI_IS_REMITTANCE_FROM_PAKISTAN;
                                        basic.EIF_BI_STRN = eif.basic.EIF_BI_STRN;
                                        basic.EIF_BI_MODE_OF_IMPORT_PAYMENT = eif.basic.EIF_BI_MODE_OF_IMPORT_PAYMENT;
                                        basic.EIF_BI_AMENDMENT_NO = 0;
                                        basic.EIF_BI_DOC_SUBMISSION = eif.basic.EIF_BI_DOC_SUBMISSION;
                                        basic.EIF_BI_CERTIFIED = eif.basic.EIF_BI_CERTIFIED;
                                        if (eif.basic.EIF_BI_CERTIFIED == true)
                                            basic.EIF_BI_CERTIFICATION_DATE = DateTime.Now;
                                        cont.PSW_EIF_BASIC_INFO.Add(basic);
                                        rowcount += cont.SaveChanges();
                                    }
                                    else
                                    {
                                        DateTime EifEntryDate = DateTime.Now.AddHours(5);
                                        if (basic.EIF_BI_ENTRY_DATETIME != null)
                                            EifEntryDate = (DateTime)basic.EIF_BI_ENTRY_DATETIME;
                                        if (eif.CustomNew)
                                        {
                                            message = "EIF Request No Already Exist Now, Please Try to Initiate a new EIF";
                                            return View(eif);
                                        }
                                        basic = new PSW_EIF_BASIC_INFO();
                                        basic.EIF_BI_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                        basic.EIF_BI_ID = Convert.ToInt32(cont.PSW_EIF_BASIC_INFO.Max(m => (decimal?)m.EIF_BI_ID)) + 1;
                                        basic.EIF_BI_REQUEST_DATE = eif.basic.EIF_BI_REQUEST_DATE;
                                        basic.EIF_BI_INTERNAL_REF = eif.basic.EIF_BI_INTERNAL_REF;
                                        basic.EIF_BI_ENTRY_DATETIME = EifEntryDate;
                                        basic.EIF_BI_EDIT_DATETIME = DateTime.Now.AddHours(5);
                                        basic.EIF_BI_APPROVAL_STATUS = "Initiated";
                                        basic.EIF_BI_STATUS_DATE = DateTime.Now.AddHours(5);
                                        basic.EIF_BI_MAKER_ID = Session["USER_ID"].ToString();
                                        if (basic.EIF_BI_MAKER_ID == "Admin123")
                                            basic.EIF_BI_CHECKER_ID = basic.EIF_BI_MAKER_ID;
                                        else
                                            basic.EIF_BI_CHECKER_ID = null;
                                        basic.EIF_BI_NTN = client.C_NTN_NO;
                                        basic.EIF_BI_BUSSINESS_NAME = eif.basic.EIF_BI_BUSSINESS_NAME;
                                        basic.EIF_BI_BUSSINESS_ADDRESS = client.C_ADDRESS;
                                        basic.EIF_BI_IS_REMITTANCE_FROM_PAKISTAN = eif.basic.EIF_BI_IS_REMITTANCE_FROM_PAKISTAN;
                                        basic.EIF_BI_STRN = eif.basic.EIF_BI_STRN;
                                        basic.EIF_BI_MODE_OF_IMPORT_PAYMENT = eif.basic.EIF_BI_MODE_OF_IMPORT_PAYMENT;
                                        basic.EIF_BI_AMENDMENT_NO = Convert.ToInt32(cont.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == eif.basic.EIF_BI_EIF_REQUEST_NO).Max(m => (decimal?)m.EIF_BI_AMENDMENT_NO)) + 1;
                                        basic.EIF_BI_DOC_SUBMISSION = eif.basic.EIF_BI_DOC_SUBMISSION;
                                        basic.EIF_BI_CERTIFIED = eif.basic.EIF_BI_CERTIFIED;
                                        if (eif.basic.EIF_BI_CERTIFIED == true)
                                            basic.EIF_BI_CERTIFICATION_DATE = DateTime.Now;
                                        cont.PSW_EIF_BASIC_INFO.Add(basic);
                                        rowcount += cont.SaveChanges();
                                    }

                                    if (PayParam == null)
                                    {
                                        PayParam = new PSW_PAYMENT_MODES_PARAMETERS();
                                        PayParam.EIF_PMP_ID = Convert.ToInt32(cont.PSW_PAYMENT_MODES_PARAMETERS.Max(m => (decimal?)m.EIF_PMP_ID)) + 1;
                                        PayParam.EIF_PMP_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                        switch (PayMode.APM_CODE.ToString())
                                        {
                                            case "301": /// LC 
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = eif.PayParam.EIF_PMP_ADV_PAYPERCENT == null ? 0 : eif.PayParam.EIF_PMP_ADV_PAYPERCENT;
                                                PayParam.EIF_PMP_SIGHT_PERCENT = eif.PayParam.EIF_PMP_SIGHT_PERCENT == null ? 0 : eif.PayParam.EIF_PMP_SIGHT_PERCENT;
                                                PayParam.EIF_PMP_USANCE_PERCENT = eif.PayParam.EIF_PMP_USANCE_PERCENT == null ? 0 : eif.PayParam.EIF_PMP_USANCE_PERCENT;
                                                PayParam.EIF_PMP_DAYS = eif.PayParam.EIF_PMP_DAYS == null ? 0 : eif.PayParam.EIF_PMP_DAYS;
                                                PayParam.EIF_PMP_PERCENTAGE = eif.PayParam.EIF_PMP_PERCENTAGE == null ? 0 : eif.PayParam.EIF_PMP_PERCENTAGE;
                                                break;
                                            case "302": /// Open Account
                                                PayParam.EIF_PMP_GD_NO = eif.PayParam.EIF_PMP_GD_NO;
                                                PSW_EIF_GD_GENERAL_INFO GDInfo = cont.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_GD_NO == PayParam.EIF_PMP_GD_NO).OrderByDescending(m => m.EIGDGI_AMENDMENT_NO).FirstOrDefault();
                                                if (GDInfo != null)
                                                {
                                                    GDInfo.EIGDGI_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                                    cont.Entry(GDInfo).State = EntityState.Modified;
                                                    rowcount += cont.SaveChanges();
                                                    PSW_EIF_GD_IMPORT_EXPORT_INFO a = cont.PSW_EIF_GD_IMPORT_EXPORT_INFO.Where(m => m.EIGDIE_GD_NO == GDInfo.EIGDGI_GD_NO).OrderByDescending(m => m.EIGDIE_AMENDMENT_NO).FirstOrDefault();
                                                    if (a != null)
                                                    {
                                                        a.EIGDIE_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                                        cont.Entry(a).State = EntityState.Modified;
                                                        rowcount += cont.SaveChanges();
                                                    }
                                                    PSW_EIF_GD_INFORMATION b = cont.PSW_EIF_GD_INFORMATION.Where(m => m.EIGDI_GD_NO == GDInfo.EIGDGI_GD_NO).OrderByDescending(m => m.EIGDI_AMENDMENT_NO).FirstOrDefault();
                                                    if (b != null)
                                                    {
                                                        b.EIGDI_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                                        cont.Entry(b).State = EntityState.Modified;
                                                        rowcount += cont.SaveChanges();
                                                    }

                                                }
                                                else
                                                {
                                                    message = "Invalid GD Number";
                                                    break;
                                                }
                                                break;
                                            case "304": /// Contract Collection
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = eif.PayParam.EIF_PMP_ADV_PAYPERCENT == null ? 0 : eif.PayParam.EIF_PMP_ADV_PAYPERCENT;
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = eif.PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT == null ? 0 : eif.PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT;
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = eif.PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT == null ? 0 : eif.PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT;
                                                PayParam.EIF_PMP_DAYS = eif.PayParam.EIF_PMP_DAYS == null ? 0 : eif.PayParam.EIF_PMP_DAYS;
                                                PayParam.EIF_PMP_PERCENTAGE = eif.PayParam.EIF_PMP_PERCENTAGE == null ? 0 : eif.PayParam.EIF_PMP_PERCENTAGE;
                                                break;
                                            case "310":
                                                if (eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT != null && eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE != null)
                                                {
                                                    PayParam.EIF_PMP_CASH_MARGIN_PERCENT = eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT == null ? 0 : eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT;
                                                    PayParam.EIF_PMP_CASH_MARGIN_VALUE = eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE == null ? 0 : eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE;
                                                }
                                                else
                                                {
                                                    if (eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT == null && eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE != null)
                                                    {
                                                        message = "Cash Margin % is required";
                                                        return View(eif);
                                                    }
                                                    if (eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE == null && eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT != null)
                                                    {
                                                        message = "Cash Margin Value is required";
                                                        return View(eif);
                                                    }
                                                    else
                                                    {
                                                        message = "Cash Margin % and Cash Margin Value is required";
                                                        return View(eif);
                                                    }
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                        PayParam.EIF_PMP_COMMISION_CHARGES = eif.PayParam.EIF_PMP_COMMISION_CHARGES;
                                        PayParam.EIF_PMP_FED_CHARGES = eif.PayParam.EIF_PMP_FED_CHARGES;
                                        PayParam.EIF_PMP_ENTRY_DATETIME = basic.EIF_BI_ENTRY_DATETIME;
                                        PayParam.EIF_PMP_MAKER_ID = Session["USER_ID"].ToString();
                                        if (PayParam.EIF_PMP_MAKER_ID == "Admin123")
                                            PayParam.EIF_PMP_CHECKER_ID = PayParam.EIF_PMP_MAKER_ID;
                                        else
                                            PayParam.EIF_PMP_CHECKER_ID = null;
                                        PayParam.EIF_PMP_AMENDMENT_NO = 0;
                                        cont.PSW_PAYMENT_MODES_PARAMETERS.Add(PayParam);
                                        rowcount += cont.SaveChanges();
                                    }
                                    else
                                    {
                                        PayParam = new PSW_PAYMENT_MODES_PARAMETERS();
                                        PayParam.EIF_PMP_ID = Convert.ToInt32(cont.PSW_PAYMENT_MODES_PARAMETERS.Max(m => (decimal?)m.EIF_PMP_ID)) + 1;
                                        PayParam.EIF_PMP_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                        switch (PayMode.APM_CODE.ToString())
                                        {
                                            case "301": /// LC 
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = eif.PayParam.EIF_PMP_ADV_PAYPERCENT == null ? 0 : eif.PayParam.EIF_PMP_ADV_PAYPERCENT;
                                                PayParam.EIF_PMP_SIGHT_PERCENT = eif.PayParam.EIF_PMP_SIGHT_PERCENT == null ? 0 : eif.PayParam.EIF_PMP_SIGHT_PERCENT;
                                                PayParam.EIF_PMP_USANCE_PERCENT = eif.PayParam.EIF_PMP_USANCE_PERCENT == null ? 0 : eif.PayParam.EIF_PMP_USANCE_PERCENT;
                                                PayParam.EIF_PMP_DAYS = eif.PayParam.EIF_PMP_DAYS == null ? 0 : eif.PayParam.EIF_PMP_DAYS;
                                                PayParam.EIF_PMP_PERCENTAGE = eif.PayParam.EIF_PMP_PERCENTAGE == null ? 0 : eif.PayParam.EIF_PMP_PERCENTAGE;
                                                break;
                                            case "302": /// Open Account
                                                PayParam.EIF_PMP_GD_NO = eif.PayParam.EIF_PMP_GD_NO;
                                                PSW_EIF_GD_GENERAL_INFO GDInfo = cont.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_GD_NO == PayParam.EIF_PMP_GD_NO).OrderByDescending(m => m.EIGDGI_AMENDMENT_NO).FirstOrDefault();
                                                if (GDInfo != null)
                                                {
                                                    GDInfo.EIGDGI_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                                    cont.Entry(GDInfo).State = EntityState.Modified;
                                                    rowcount += cont.SaveChanges();
                                                    PSW_EIF_GD_IMPORT_EXPORT_INFO a = cont.PSW_EIF_GD_IMPORT_EXPORT_INFO.Where(m => m.EIGDIE_GD_NO == GDInfo.EIGDGI_GD_NO).OrderByDescending(m => m.EIGDIE_AMENDMENT_NO).FirstOrDefault();
                                                    if (a != null)
                                                    {
                                                        a.EIGDIE_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                                        cont.Entry(a).State = EntityState.Modified;
                                                        rowcount += cont.SaveChanges();
                                                    }
                                                    PSW_EIF_GD_INFORMATION b = cont.PSW_EIF_GD_INFORMATION.Where(m => m.EIGDI_GD_NO == GDInfo.EIGDGI_GD_NO).OrderByDescending(m => m.EIGDI_AMENDMENT_NO).FirstOrDefault();
                                                    if (b != null)
                                                    {
                                                        b.EIGDI_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                                        cont.Entry(b).State = EntityState.Modified;
                                                        rowcount += cont.SaveChanges();
                                                    }

                                                }
                                                else
                                                {
                                                    message = "Invalid GD Number";
                                                    break;
                                                }
                                                break;
                                            case "304": /// Contract Collection
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = eif.PayParam.EIF_PMP_ADV_PAYPERCENT == null ? 0 : eif.PayParam.EIF_PMP_ADV_PAYPERCENT;
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = eif.PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT == null ? 0 : eif.PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT;
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = eif.PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT == null ? 0 : eif.PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT;
                                                PayParam.EIF_PMP_DAYS = eif.PayParam.EIF_PMP_DAYS == null ? 0 : eif.PayParam.EIF_PMP_DAYS;
                                                PayParam.EIF_PMP_PERCENTAGE = eif.PayParam.EIF_PMP_PERCENTAGE == null ? 0 : eif.PayParam.EIF_PMP_PERCENTAGE;
                                                break;
                                            case "310":
                                                if (eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT != null && eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE != null)
                                                {
                                                    PayParam.EIF_PMP_CASH_MARGIN_PERCENT = eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT == null ? 0 : eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT;
                                                    PayParam.EIF_PMP_CASH_MARGIN_VALUE = eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE == null ? 0 : eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE;
                                                }
                                                else
                                                {
                                                    if (eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT == null && eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE != null)
                                                    {
                                                        message = "Cash Margin % is required";
                                                        return View(eif);
                                                    }
                                                    if (eif.PayParam.EIF_PMP_CASH_MARGIN_VALUE == null && eif.PayParam.EIF_PMP_CASH_MARGIN_PERCENT != null)
                                                    {
                                                        message = "Cash Margin Value is required";
                                                        return View(eif);
                                                    }
                                                    else
                                                    {
                                                        message = "Cash Margin % and Cash Margin Value is required";
                                                        return View(eif);
                                                    }
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                        PayParam.EIF_PMP_COMMISION_CHARGES = eif.PayParam.EIF_PMP_COMMISION_CHARGES;
                                        PayParam.EIF_PMP_FED_CHARGES = eif.PayParam.EIF_PMP_FED_CHARGES;
                                        PayParam.EIF_PMP_ENTRY_DATETIME = DateTime.Now;
                                        PayParam.EIF_PMP_MAKER_ID = Session["USER_ID"].ToString();
                                        if (PayParam.EIF_PMP_MAKER_ID == "Admin123")
                                            PayParam.EIF_PMP_CHECKER_ID = PayParam.EIF_PMP_MAKER_ID;
                                        else
                                            PayParam.EIF_PMP_CHECKER_ID = null;
                                        PayParam.EIF_PMP_AMENDMENT_NO = basic.EIF_BI_AMENDMENT_NO;
                                        cont.PSW_PAYMENT_MODES_PARAMETERS.Add(PayParam);
                                        rowcount += cont.SaveChanges();
                                    }

                                    if (docinfo == null)
                                    {
                                        docinfo = new PSW_EIF_DOCUMENT_REQUEST_INFO();
                                        docinfo.EIF_DRI_ID = Convert.ToInt32(cont.PSW_EIF_DOCUMENT_REQUEST_INFO.Max(m => (decimal?)m.EIF_DRI_ID)) + 1;
                                        docinfo.EIF_DRI_BI_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                        docinfo.EIF_DRI_BENEFICIARY_NAME = eif.documents.EIF_DRI_BENEFICIARY_NAME;
                                        docinfo.EIF_DRI_BENEFICIARY_IBAN = eif.documents.EIF_DRI_BENEFICIARY_IBAN;
                                        docinfo.EIF_DRI_BENEFICIARY_COUNTRY = eif.documents.EIF_DRI_BENEFICIARY_COUNTRY;
                                        docinfo.EIF_DRI_BENEFICIARY_ADDRESS = eif.documents.EIF_DRI_BENEFICIARY_ADDRESS;
                                        docinfo.EIF_DRI_EXPORTER_NAME = eif.documents.EIF_DRI_EXPORTER_NAME;
                                        docinfo.EIF_DRI_EXPORTER_ADDRESS = eif.documents.EIF_DRI_EXPORTER_ADDRESS;
                                        docinfo.EIF_DRI_EXPORTER_COUNTRY = eif.documents.EIF_DRI_EXPORTER_COUNTRY;
                                        docinfo.EIF_DRI_PORT_OF_SHIPMENT = eif.documents.EIF_DRI_PORT_OF_SHIPMENT;
                                        docinfo.EIF_DRI_DELIVERY_TERM = eif.documents.EIF_DRI_DELIVERY_TERM;
                                        docinfo.EIF_DRI_LC_CONT_NO = eif.documents.EIF_DRI_LC_CONT_NO;
                                        docinfo.EIF_DRI_TOTAL_INVOICE_VALUE = eif.documents.EIF_DRI_TOTAL_INVOICE_VALUE;
                                        docinfo.EIF_DRI_CCY = eif.documents.EIF_DRI_CCY;
                                        docinfo.EIF_DRI_EXCHANGE_RATE = eif.documents.EIF_DRI_EXCHANGE_RATE;
                                        docinfo.EIF_DRI_ENTRY_DATETIME = DateTime.Now;
                                        docinfo.EIF_DRI_MAKER_ID = Session["USER_ID"].ToString();
                                        if (docinfo.EIF_DRI_MAKER_ID == "Admin123")
                                            docinfo.EIF_DRI_CHECKER_ID = docinfo.EIF_DRI_MAKER_ID;
                                        else
                                            docinfo.EIF_DRI_CHECKER_ID = null;
                                        docinfo.EIF_DRI_AMENDMENT_NO = 0;
                                        cont.PSW_EIF_DOCUMENT_REQUEST_INFO.Add(docinfo);
                                        rowcount += cont.SaveChanges();
                                    }
                                    else
                                    {
                                        docinfo = new PSW_EIF_DOCUMENT_REQUEST_INFO();
                                        docinfo.EIF_DRI_ID = Convert.ToInt32(cont.PSW_EIF_DOCUMENT_REQUEST_INFO.Max(m => (decimal?)m.EIF_DRI_ID)) + 1;
                                        docinfo.EIF_DRI_BI_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                        docinfo.EIF_DRI_BENEFICIARY_NAME = eif.documents.EIF_DRI_BENEFICIARY_NAME;
                                        docinfo.EIF_DRI_BENEFICIARY_IBAN = eif.documents.EIF_DRI_BENEFICIARY_IBAN;
                                        docinfo.EIF_DRI_BENEFICIARY_COUNTRY = eif.documents.EIF_DRI_BENEFICIARY_COUNTRY;
                                        docinfo.EIF_DRI_BENEFICIARY_ADDRESS = eif.documents.EIF_DRI_BENEFICIARY_ADDRESS;
                                        docinfo.EIF_DRI_EXPORTER_NAME = eif.documents.EIF_DRI_EXPORTER_NAME;
                                        docinfo.EIF_DRI_EXPORTER_ADDRESS = eif.documents.EIF_DRI_EXPORTER_ADDRESS;
                                        docinfo.EIF_DRI_EXPORTER_COUNTRY = eif.documents.EIF_DRI_EXPORTER_COUNTRY;
                                        docinfo.EIF_DRI_PORT_OF_SHIPMENT = eif.documents.EIF_DRI_PORT_OF_SHIPMENT;
                                        docinfo.EIF_DRI_DELIVERY_TERM = eif.documents.EIF_DRI_DELIVERY_TERM;
                                        docinfo.EIF_DRI_LC_CONT_NO = eif.documents.EIF_DRI_LC_CONT_NO;
                                        docinfo.EIF_DRI_TOTAL_INVOICE_VALUE = eif.documents.EIF_DRI_TOTAL_INVOICE_VALUE;
                                        docinfo.EIF_DRI_CCY = eif.documents.EIF_DRI_CCY;
                                        docinfo.EIF_DRI_EXCHANGE_RATE = eif.documents.EIF_DRI_EXCHANGE_RATE;
                                        docinfo.EIF_DRI_ENTRY_DATETIME = DateTime.Now;
                                        docinfo.EIF_DRI_MAKER_ID = Session["USER_ID"].ToString();
                                        if (docinfo.EIF_DRI_MAKER_ID == "Admin123")
                                            docinfo.EIF_DRI_CHECKER_ID = docinfo.EIF_DRI_MAKER_ID;
                                        else
                                            docinfo.EIF_DRI_CHECKER_ID = null;
                                        docinfo.EIF_DRI_AMENDMENT_NO = basic.EIF_BI_AMENDMENT_NO;
                                        cont.PSW_EIF_DOCUMENT_REQUEST_INFO.Add(docinfo);
                                        rowcount += cont.SaveChanges();
                                    }

                                    if (bankInfo == null)
                                    {
                                        bankInfo = new PSW_EIF_BANK_INFO();
                                        bankInfo.EIF_BA_ID = Convert.ToInt32(cont.PSW_EIF_BANK_INFO.Max(m => (decimal?)m.EIF_BA_ID)) + 1;
                                        bankInfo.EIF_BA_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                        bankInfo.EIF_BA_EXPIRY_DATE = eif.bank.EIF_BA_EXPIRY_DATE;
                                        bankInfo.EIF_BA_TRANSPORT_DOC_DATE = eif.bank.EIF_BA_TRANSPORT_DOC_DATE;
                                        bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE = eif.bank.EIF_BA_INNTENDED_PAYMENT_DATE;
                                        bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT = eif.bank.EIF_BA_FINAL_DATE_OF_SHIPMENT;
                                        bankInfo.EIF_BA_IBAN = eif.bank.EIF_BA_IBAN;
                                        bankInfo.EIF_BA_REMARK = eif.bank.EIF_BA_REMARK;
                                        bankInfo.EIF_BA_SBP_APPROVAL_SUBJECT = eif.bank.EIF_BA_SBP_APPROVAL_SUBJECT;
                                        bankInfo.EIF_BA_SBP_APPROVAL_NO = eif.bank.EIF_BA_SBP_APPROVAL_NO;
                                        bankInfo.EIF_BA_SBP_APPROVAL_DATE = eif.bank.EIF_BA_SBP_APPROVAL_DATE;
                                        bankInfo.EIF_BA_BANK = eif.bank.EIF_BA_BANK;
                                        bankInfo.EIF_BA_CITY = eif.bank.EIF_BA_CITY;
                                        bankInfo.EIF_BA_BRANCH = eif.bank.EIF_BA_BRANCH;
                                        bankInfo.EIF_BA_SALES_TERM = eif.bank.EIF_BA_SALES_TERM;
                                        bankInfo.EIF_BA_APPROVAL_REMARKS = eif.bank.EIF_BA_APPROVAL_REMARKS;
                                        bankInfo.EIF_BA_ENTRY_DATETIME = DateTime.Now;
                                        bankInfo.EIF_BA_MAKER_ID = Session["USER_ID"].ToString();
                                        if (bankInfo.EIF_BA_MAKER_ID == "Admin123")
                                            bankInfo.EIF_BA_CHECKER_ID = bankInfo.EIF_BA_MAKER_ID;
                                        else
                                            bankInfo.EIF_BA_CHECKER_ID = null;
                                        bankInfo.EIF_BA_AMENDMENT_NO = 0;
                                        cont.PSW_EIF_BANK_INFO.Add(bankInfo);
                                        rowcount += cont.SaveChanges();
                                    }
                                    else
                                    {
                                        bankInfo = new PSW_EIF_BANK_INFO();
                                        bankInfo.EIF_BA_ID = Convert.ToInt32(cont.PSW_EIF_BANK_INFO.Max(m => (decimal?)m.EIF_BA_ID)) + 1;
                                        bankInfo.EIF_BA_EIF_REQUEST_NO = eif.basic.EIF_BI_EIF_REQUEST_NO;
                                        bankInfo.EIF_BA_EXPIRY_DATE = eif.bank.EIF_BA_EXPIRY_DATE;
                                        bankInfo.EIF_BA_TRANSPORT_DOC_DATE = eif.bank.EIF_BA_TRANSPORT_DOC_DATE;
                                        bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE = eif.bank.EIF_BA_INNTENDED_PAYMENT_DATE;
                                        bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT = eif.bank.EIF_BA_FINAL_DATE_OF_SHIPMENT;
                                        bankInfo.EIF_BA_IBAN = eif.bank.EIF_BA_IBAN;
                                        bankInfo.EIF_BA_REMARK = eif.bank.EIF_BA_REMARK;
                                        bankInfo.EIF_BA_SBP_APPROVAL_SUBJECT = eif.bank.EIF_BA_SBP_APPROVAL_SUBJECT;
                                        bankInfo.EIF_BA_SBP_APPROVAL_NO = eif.bank.EIF_BA_SBP_APPROVAL_NO;
                                        bankInfo.EIF_BA_SBP_APPROVAL_DATE = eif.bank.EIF_BA_SBP_APPROVAL_DATE;
                                        bankInfo.EIF_BA_BANK = eif.bank.EIF_BA_BANK;
                                        bankInfo.EIF_BA_CITY = eif.bank.EIF_BA_CITY;
                                        bankInfo.EIF_BA_BRANCH = eif.bank.EIF_BA_BRANCH;
                                        bankInfo.EIF_BA_SALES_TERM = eif.bank.EIF_BA_SALES_TERM;
                                        bankInfo.EIF_BA_APPROVAL_REMARKS = eif.bank.EIF_BA_APPROVAL_REMARKS;
                                        bankInfo.EIF_BA_EDIT_DATETIME = DateTime.Now;
                                        bankInfo.EIF_BA_MAKER_ID = Session["USER_ID"].ToString();
                                        if (bankInfo.EIF_BA_MAKER_ID == "Admin123")
                                            bankInfo.EIF_BA_CHECKER_ID = bankInfo.EIF_BA_MAKER_ID;
                                        else
                                            bankInfo.EIF_BA_CHECKER_ID = null;
                                        bankInfo.EIF_BA_AMENDMENT_NO = basic.EIF_BI_AMENDMENT_NO;
                                        cont.PSW_EIF_BANK_INFO.Add(bankInfo);
                                        rowcount += cont.SaveChanges();
                                    }

                                    transaction.Commit();

                                    message += "Transaction Saved Successfully";

                                }
                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    rowcount = 0;
                                    message += "Exception Occured " + ex.Message + Environment.NewLine;
                                    if (ex.InnerException != null)
                                    {
                                        message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                                        if (ex.InnerException.InnerException != null)
                                        {
                                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("EIFRequest", "ManageEIF", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> PayMode = new SelectList(context.PSW_AUTH_PAY_MODES.Where(m => m.APM_TYPE_ID == 1 && m.APM_ISAUTH == true).Select(f => new
                        {
                            f.APM_ID,
                            APM_STATUS = f.APM_STATUS + " -- " + f.APM_CODE
                        }).ToList(), "APM_ID", "APM_STATUS", 0).ToList();
                        PayMode.Insert(0, (new SelectListItem { Text = "--Select Mode of Import Payment--", Value = "0" }));
                        loadInfo.AuthPayModes = PayMode;
                        int?[] ClientIds = context.PSW_ASSIGN_PROFILE_TYPE_TO_CLIENT.Where(x => x.APT_PROFILE_ID == 1).Select(x => x.APT_CLIENT_ID).ToArray();
                        List<SelectListItem> Customers = new SelectList(context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_STATUS == true && x.C_ISAUTH == true && x.C_ACCOUNT_STATUS_ID == 1 && ClientIds.Contains(x.C_ID)).Select(f => new
                        {
                            f.C_IBAN_NO,
                            C_NAME = f.C_NAME + " -- (" + f.C_IBAN_NO + ")"
                        }).ToList(), "C_IBAN_NO", "C_NAME", 0).ToList();
                        Customers.Insert(0, (new SelectListItem { Text = "--Select Business Name For Import --", Value = "0" }));
                        loadInfo.ListCustomers = Customers;
                        //List<SelectListItem> LisBanks = new SelectList(context.PSW_LIST_OF_BANKS.Where(x => x.LB_BANK_STATUS == true).ToList(), "LB_BANK_ID", "LB_BANK_NAME", 0).ToList();
                        //LisBanks.Insert(0, (new SelectListItem { Text = "--Select Bank --", Value = "0" }));
                        //loadInfo.ListBanks = LisBanks;
                        //List<SelectListItem> lisCitis = new SelectList(context.PSW_LIST_OF_CITIES.Where(x => x.LOCT_STATUS == true).ToList(), "LOCT_ID", "LOCT_NAME", 0).ToList();
                        //lisCitis.Insert(0, (new SelectListItem { Text = "--Select City --", Value = "0" }));
                        //loadInfo.ListCitis = lisCitis;
                        ViewBag.message = message;
                    }
                    return View(loadInfo);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }

        [HttpPost]
        public JsonResult ListOfAuthPayModes(string clientId = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (clientId != "")
                    {
                        int cid = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == clientId).FirstOrDefault().C_ID;
                        int?[] AuthPayAids = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == cid && m.APMC_STATUS == true && m.APMC_ISAUTH == true).Select(m => m.APMC_PAY_MODE_ID).ToArray();
                        var coverletters = context.PSW_AUTH_PAY_MODES.Where(x => AuthPayAids.Contains(x.APM_ID) && x.APM_TYPE_ID == 1 && x.APM_ISAUTH == true).Select(x => new
                        {
                            Value = x.APM_ID,
                            Text = x.APM_STATUS + " -- " + x.APM_CODE
                        });
                        return Json(coverletters, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Client", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult GetBusinessDetails(string clientId = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (clientId != "")
                    {
                        int cid = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == clientId).FirstOrDefault().C_ID;
                        var coverletters = context.PSW_CLIENT_INFO.Where(x => x.C_ID == cid).Select(x => new
                        {
                            NTN = x.C_NTN_NO,
                            ADDRESS = x.C_ADDRESS,
                            STRN = x.C_STRN,
                            IBAN = x.C_IBAN_NO
                        });
                        return Json(coverletters, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Client", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult GetCharges(string BID, decimal IVal, string PM)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    int cid = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == BID).FirstOrDefault().C_ID;
                    PSW_CLIENT_INFO client = context.PSW_CLIENT_INFO.Where(m => m.C_ID == cid).FirstOrDefault();
                    decimal ChargesPercent = 0;
                    decimal FEDPercent = (decimal)(client.C_FED_RATE == null ? 0 : client.C_FED_RATE);
                    switch (PM)
                    {
                        case "301":
                            ChargesPercent = (decimal)(client.C_LC_COMMISIOIN == null ? 0 : client.C_LC_COMMISIOIN);
                            break;
                        case "309":
                            ChargesPercent = (decimal)(client.C_LC_COMMISIOIN == null ? 0 : client.C_LC_COMMISIOIN);
                            break;
                        case "303":
                            ChargesPercent = (decimal)(client.C_CONTRACT_COMMISION == null ? 0 : client.C_CONTRACT_COMMISION);
                            break;
                        case "304":
                            ChargesPercent = (decimal)(client.C_CONTRACT_COMMISION == null ? 0 : client.C_CONTRACT_COMMISION);
                            break;
                        default:
                            break;
                    }
                    decimal CommissionCharges = Math.Round((ChargesPercent / 100) * IVal, 2);
                    decimal FED = Math.Round((FEDPercent / 100) * CommissionCharges, 2);
                    var coverletters = new
                    {
                        CommCharges = CommissionCharges,
                        FEDCharges = FED
                    };
                    return Json(coverletters, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult GetEIFInformationForBDA(string E = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFBDA", Session["USER_ID"].ToString()))
                {
                    PSW_EIF_MASTER_DETAILS eif_info = context.PSW_EIF_MASTER_DETAILS.Where(x => x.EIF_BI_EIF_REQUEST_NO == E).FirstOrDefault();
                    decimal? PreviousBDAAmounts = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_EIF_REQUEST_NO == E && m.EIF_BDA_STATUS == "APPROVED").Sum(m => m.EIF_BDA_AMOUNT_FCY);
                    decimal? SampleAmountEIF = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E).Sum(m => m.EIF_HC_SAMPLE_VALUE);
                    decimal? CurrentBalance = (eif_info.EIF_DRI_TOTAL_INVOICE_VALUE) - (PreviousBDAAmounts == null ? 0 : PreviousBDAAmounts);
                    var coverletters = new
                    {
                        TOTALAMOUNT = eif_info.EIF_DRI_TOTAL_INVOICE_VALUE,
                        AUTCCY = eif_info.EIF_DRI_CCY,
                        EXCHAGERATE = eif_info.EIF_DRI_EXCHANGE_RATE.ToString(),
                        CurrentBalance = CurrentBalance,
                        SampleAmount = SampleAmountEIF == null ? 0 : SampleAmountEIF,
                        ClientName = eif_info.C_NAME 
                    };
                    return Json(coverletters, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public ActionResult GetEIFGDNumber(string REQ)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFBDA", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        List<PSW_EIF_GD_GENERAL_INFO_LIST> EifGDNo = new List<PSW_EIF_GD_GENERAL_INFO_LIST>();
                        EifGDNo = (context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(x => x.EIGDGI_EIF_REQUEST_NO == REQ)).ToList();
                        JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                        string result = javaScriptSerializer.Serialize(EifGDNo);
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return null;
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult AmountInWords(decimal Amount = 0)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFBDA", Session["USER_ID"].ToString()))
                {
                    string AmountInWords = DAL.ConvertToWords(Amount.ToString());
                    var coverletters = new
                    {
                        AmountInWordS = AmountInWords
                    };
                    return Json(coverletters, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        public PartialViewResult EIFRequests(string E,EIFFilter Entity)
        {
            List<PSW_EIF_MASTER_DETAILS> returnList = new List<PSW_EIF_MASTER_DETAILS>();

            string message = "";
            try
            {
                switch (E)
                {
                    case "1":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.EIF_BI_APPROVAL_STATUS == "Initiated").OrderByDescending(m => m.EIF_BI_ID).ToList();
                        else
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_APPROVAL_STATUS == "Initiated").OrderByDescending(m => m.EIF_BI_ID).Take(10).ToList();
                        break;
                    case "2":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.EIF_BI_APPROVAL_STATUS == "APPROVED").OrderByDescending(m => m.EIF_BI_ID).ToList();
                        else
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_APPROVAL_STATUS == "APPROVED").OrderByDescending(m => m.EIF_BI_ID).Take(10).ToList();
                        break;
                    case "3":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.EIF_BI_APPROVAL_STATUS == "APPROVED").OrderByDescending(m => m.EIF_BI_ID).ToList();
                        else
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_APPROVAL_STATUS == "APPROVED").OrderByDescending(m => m.EIF_BI_ID).Take(10).ToList();
                        break;
                    case "4":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.EIF_BI_APPROVAL_STATUS == "APPROVED").OrderByDescending(m => m.EIF_BI_ID).ToList();
                        else
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_APPROVAL_STATUS == "APPROVED").OrderByDescending(m => m.EIF_BI_ID).Take(10).ToList();
                        break;
                    case "5":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.EIF_BI_APPROVAL_STATUS == "APPROVED").OrderByDescending(m => m.EIF_BI_ID).ToList();
                        else
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_APPROVAL_STATUS == "APPROVED").OrderByDescending(m => m.EIF_BI_ID).Take(10).ToList();
                        break;
                    case "6":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate) && m.EIF_BI_APPROVAL_STATUS.Contains("BANK")).OrderByDescending(m => m.EIF_BI_ID).ToList();
                        else
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_APPROVAL_STATUS.Contains("BANK")).OrderByDescending(m => m.EIF_BI_ID).Take(10).ToList();
                        break;
                    case "7":
                        if (Entity.FromDate != null && Entity.ToDate != null)
                            returnList = context.PSW_EIF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Entity.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_REQUEST_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Entity.ToDate)).OrderByDescending(m => m.EIF_BI_ID).ToList();
                        else
                            returnList = context.PSW_EIF_MASTER_DETAILS.OrderByDescending(m => m.EIF_BI_ID).Take(10).ToList();
                        break;
                    default:
                        message = " Invalid Action Performed.";
                        break;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.SelectedOption = E;
                ViewBag.message = message;
            }
            return PartialView(returnList);
        }

        public ActionResult GetEIFViewMode(string E = "", string M = "", int AM = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        EIF eifInfo = new EIF();
                        if (E != "")
                        {
                            eifInfo.basic = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
                            if (eifInfo.basic == null)
                            {
                                message += "No EIF Basic Information Found In The System For the EIF # " + E + Environment.NewLine;
                            }
                            else
                            {
                                if (eifInfo.basic.EIF_BI_BUSSINESS_NAME != null)
                                {
                                    eifInfo.basic.EIF_BI_BUSSINESS_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eifInfo.basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_NAME;
                                    eifInfo.PayMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eifInfo.basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                                    eifInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == AM).FirstOrDefault();
                                }
                                else
                                {
                                    message += "Invalid Business Name" + Environment.NewLine;
                                }
                            }
                            eifInfo.documents = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == E && m.EIF_DRI_AMENDMENT_NO == AM).FirstOrDefault();
                            if (eifInfo.documents == null)
                            {
                                message += "No EIF Documents Request Information Found In The System For the EIF # " + E + Environment.NewLine;
                            }
                            else
                            {
                                if (eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY != null)
                                    eifInfo.BENECOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                                else
                                    message += "Invalid Bene Country" + Environment.NewLine;
                                if (eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY != null)
                                    eifInfo.EXPORTERCOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                                else
                                    message += "Invalid Exporter Country" + Environment.NewLine;
                                if (eifInfo.documents.EIF_DRI_DELIVERY_TERM != null)
                                    eifInfo.DeliveryTerm = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eifInfo.documents.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                                else
                                    message += "Invalid Delivery Term" + Environment.NewLine;
                                if (eifInfo.documents.EIF_DRI_CCY != null)
                                    eifInfo.Currency = context.PSW_CURRENCY.Where(m => m.CUR_ID == eifInfo.documents.EIF_DRI_CCY).FirstOrDefault();
                                else
                                    message += "Invalid Currency" + Environment.NewLine;
                                if (eifInfo.documents.EIF_DRI_PORT_OF_SHIPMENT != null)
                                    eifInfo.ShippingPort = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_CODE == eifInfo.documents.EIF_DRI_PORT_OF_SHIPMENT).FirstOrDefault();
                            }
                            eifInfo.hscodes = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == AM).ToList();
                            if (eifInfo.hscodes == null && eifInfo.hscodes.Count() <= 0)
                            {
                                message += "No Goods Declaration Found In The System For the EIF # " + E + Environment.NewLine;
                            }
                            eifInfo.bank = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == E && m.EIF_BA_AMENDMENT_NO == AM).FirstOrDefault();
                            if (eifInfo.bank == null)
                            {
                                message += "No EIF Bank Information Found In The System For the EIF # " + E + Environment.NewLine;
                            }
                            eifInfo.BDAList = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_EIF_REQUEST_NO == E).ToList();
                            if (eifInfo.BDAList == null && eifInfo.BDAList.Count() <= 0)
                            {
                                message += "No BDA Found In The System For the EIF # " + E + Environment.NewLine;
                            }
                            eifInfo.EIFGD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO == E).ToList();
                            if (eifInfo.EIFGD == null && eifInfo.EIFGD.Count() <= 0)
                            {
                                message += "No EIF GD Found In The System For the EIF # " + E + Environment.NewLine;
                            }
                            return View(eifInfo);
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("EIFRequest", "GetEIFViewMode", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                        ViewBag.SelectedOption = M;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public ActionResult AproveEIF(string E = "", int M = 0,int ASN = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != "")
                        {
                            EIF eifInfo = new EIF();
                            PSW_EIF_BASIC_INFO basicInfoUpdate = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                            if (basicInfoUpdate != null)
                            {
                                eifInfo.basic = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                                eifInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();

                                if (eifInfo.basic == null)
                                {
                                    message += "No EIF Basic Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eifInfo.basic.EIF_BI_BUSSINESS_NAME != null)
                                    {
                                        eifInfo.basic.EIF_BI_BUSSINESS_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eifInfo.basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_IBAN_NO;
                                        eifInfo.PayMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eifInfo.basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                                    }
                                    else
                                    {
                                        message += "Invalid Business Name";
                                    }
                                }
                                eifInfo.documents = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == E && m.EIF_DRI_AMENDMENT_NO == M).FirstOrDefault();
                                if (eifInfo.documents == null)
                                {
                                    message += "No EIF Documents Request Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY != null)
                                        eifInfo.BENECOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Bene Country";
                                    if (eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY != null)
                                        eifInfo.EXPORTERCOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Exporter Country";
                                    if (eifInfo.documents.EIF_DRI_DELIVERY_TERM != null)
                                        eifInfo.DeliveryTerm = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eifInfo.documents.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                                    else
                                        message += "Invalid Delivery Term";
                                }
                                eifInfo.hscodes = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == M).ToList();
                                if (eifInfo.hscodes == null && eifInfo.hscodes.Count() <= 0)
                                {
                                    message += "No Goods Declaration Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                eifInfo.bank = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == E && m.EIF_BA_AMENDMENT_NO == M).FirstOrDefault();
                                if (eifInfo.bank == null)
                                {
                                    message += "No EIF Bank Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                #region Get CashMargin HsCode
                                string GetAllEifHSCode = "";
                                bool CashMarginMailToSent = false;
                                List<PSW_EIF_HS_CODE> CheckHsCode = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == M).ToList();
                                if (CheckHsCode.Count > 0)
                                {
                                    foreach (PSW_EIF_HS_CODE HsCode in CheckHsCode)
                                    {
                                        PSW_CASH_MARGIN_HS_CODES CheckCashMargin = context.PSW_CASH_MARGIN_HS_CODES.Where(m => m.CM_HS4 == HsCode.EIF_HC_CODE).FirstOrDefault();
                                        if (CheckCashMargin != null)
                                        {
                                            CashMarginMailToSent = true;
                                            CheckCashMargin.CM_HS8 += GetAllEifHSCode + ",";
                                        }
                                    }
                                    if (CashMarginMailToSent == true)
                                    {
                                        message = EMAILING.EIF_HsCode(GetAllEifHSCode, E);
                                    }
                                }
                                #endregion
                                if (DAL.DO_EDI == "true")
                                {
                                    message += DAL.MethodId1520(E, basicInfoUpdate.EIF_BI_AMENDMENT_NO, ASN) + Environment.NewLine;
                                    //message += "  RAW REQUEST  DETAILS FROM OUR SIDE  -------------------- " + Environment.NewLine;
                                    //message += DAL.message;
                                    if (message.Contains("200"))
                                    {
                                        basicInfoUpdate.EIF_BI_APPROVAL_STATUS = "APPROVED";
                                        basicInfoUpdate.EIF_BI_STATUS_DATE = DateTime.Now;
                                        basicInfoUpdate.EIF_BI_CHECKER_ID = Session["USER_ID"].ToString();
                                        context.Entry(basicInfoUpdate).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message += "EIF Approved Successfully." + Environment.NewLine;
                                            message += EMAILING.EIF_Approve(E, basicInfoUpdate.EIF_BI_AMENDMENT_NO);
                                        }
                                        else
                                            message += "Unable to update transaction status." + Environment.NewLine;
                                    }
                                }
                                else
                                    message += "EDI is turned off from system configuration";
                                return View("GetEIFViewMode", eifInfo);
                            }
                            else
                            {
                                message = " Error While Aproving the EIF # " + E;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("EIFRequest", "ApproveEIF", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("GetEIFViewMode");
        }

        public ActionResult AproveBDA(int BA,int ASN = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFBDA", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string GDNO = "";
                    try
                    {
                        if (BA != 0)
                        {
                            PSW_EIF_BDA BDA = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_ID == BA).FirstOrDefault();
                            if (BDA != null)
                            {
                                if (DAL.DO_EDI == "true")
                                {
                                    message += DAL.MethodId1522(BA, ASN) + Environment.NewLine;
                                    //message += "  RAW REQUEST DETAILS  -------------------- " + Environment.NewLine;
                                    //message += DAL.message;
                                    if (message.Contains("200"))
                                    {
                                        GDNO = BDA.EIF_BDA_GD_NO;
                                        BDA.EIF_BDA_STATUS = "APPROVED";
                                        BDA.EIF_BDA_EDIT_DATETIME = DateTime.Now;
                                        BDA.EIF_BDA_CHECKER_ID = Session["USER_ID"].ToString();
                                        context.Entry(BDA).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message += "BDA Approved Successfully." + Environment.NewLine;
                                            message += EMAILING.EIF_BDA_Approve(BA);
                                        }
                                        else
                                            message += "Unable to update transaction status" + Environment.NewLine;
                                    }
                                }
                                else
                                    message += "EDI is turned off from system configuration";
                                return View("EIFBDA");
                            }
                            else
                            {
                                message = " Error While Aproving the BDA ";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("EIFRequest", "ApproveBDA", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                        return View("EIFBDA");
                    }
                    finally
                    {
                        List<SelectListItem> EifRequests = new SelectList(context.PSW_EIF_MASTER_DETAILS.ToList(), "EIF_BI_EIF_REQUEST_NO", "EIF_BI_EIF_REQUEST_NO", 0).ToList();
                        EifRequests.Insert(0, (new SelectListItem { Text = "--Select Request --", Value = "0" }));
                        ViewBag.EIF_BDA_EIF_REQUEST_NO = EifRequests;
                        List<SelectListItem> Currency = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        Currency.Insert(0, (new SelectListItem { Text = "--Select Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_CURRENCY_FCY = Currency;
                        List<SelectListItem> TotalBDA = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        TotalBDA.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY = TotalBDA;
                        List<SelectListItem> SampleAmount = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        SampleAmount.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY = SampleAmount;
                        List<SelectListItem> NetBDA = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        NetBDA.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY = NetBDA;
                        List<SelectListItem> PaymentCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_STATUS == true && m.LC_ISAUTH == true).ToList(), "LC_NAME", "LC_NAME", 0).ToList();
                        PaymentCountry.Insert(0, (new SelectListItem { Text = "--Select Payment Country--", Value = "0" }));
                        ViewBag.EIF_BDA_PAYMENT_COUNTRY = PaymentCountry;
                        if (GDNO != "")
                        {
                            List<SelectListItem> BDAGD = new SelectList(context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO == GDNO).ToList(), "EIGDGI_GD_NO", "EIGDGI_GD_NO", 0).ToList();
                            BDAGD.Insert(0, (new SelectListItem { Text = "--Select GD Number--", Value = "0" }));
                            ViewBag.EIF_BDA_GD_NO = BDAGD;
                        }
                        else
                        {
                            List<SelectListItem> BDAGD = new List<SelectListItem>();
                            BDAGD.Insert(0, (new SelectListItem { Text = "--No GD Number--", Value = "0" }));
                            ViewBag.EIF_BDA_GD_NO = BDAGD;
                        }
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("EIFBDA");
        }

        public ActionResult ReverseBDA(int BA)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFBDA", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string GDNO = "";
                    try
                    {
                        if (BA != 0)
                        {
                            PSW_EIF_BDA BDA = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_ID == BA).FirstOrDefault();
                            if (BDA != null)
                            {
                                if (DAL.DO_EDI == "true")
                                {
                                    message += DAL.MethodId1536(BA, true) + Environment.NewLine;
                                    //message += "  RAW REQUEST DETAILS  -------------------- " + Environment.NewLine;
                                    //message += DAL.message;
                                    if (message.Contains("212"))
                                    {
                                        GDNO = BDA.EIF_BDA_GD_NO;
                                        BDA.EIF_BDA_STATUS = "REVERSED";
                                        BDA.EIF_BDA_EDIT_DATETIME = DateTime.Now;
                                        context.Entry(BDA).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message += "BDA Reversed Successfully." + Environment.NewLine;
                                            message += EMAILING.EIF_BDA_Reverse(BA);
                                        }
                                        else
                                            message += "Problem While Updating The Status.";
                                    }
                                }
                                else
                                    message += "EDI is turned off from system configuration";
                                return View("EIFBDA");
                            }
                            else
                            {
                                message = " Error While Reversing the BDA ";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("EIFRequest", "ReverseBDA", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                        return View("EIFBDA");
                    }
                    finally
                    {
                        List<SelectListItem> EifRequests = new SelectList(context.PSW_EIF_MASTER_DETAILS.ToList(), "EIF_BI_EIF_REQUEST_NO", "EIF_BI_EIF_REQUEST_NO", 0).ToList();
                        EifRequests.Insert(0, (new SelectListItem { Text = "--Select Request --", Value = "0" }));
                        ViewBag.EIF_BDA_EIF_REQUEST_NO = EifRequests;
                        List<SelectListItem> Currency = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        Currency.Insert(0, (new SelectListItem { Text = "--Select Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_CURRENCY_FCY = Currency;
                        List<SelectListItem> TotalBDA = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        TotalBDA.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY = TotalBDA;
                        List<SelectListItem> SampleAmount = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        SampleAmount.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY = SampleAmount;
                        List<SelectListItem> NetBDA = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        NetBDA.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY = NetBDA;
                        List<SelectListItem> PaymentCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_STATUS == true && m.LC_ISAUTH == true).ToList(), "LC_NAME", "LC_NAME", 0).ToList();
                        PaymentCountry.Insert(0, (new SelectListItem { Text = "--Select Payment Country--", Value = "0" }));
                        ViewBag.EIF_BDA_PAYMENT_COUNTRY = PaymentCountry;
                        if (GDNO != "")
                        {
                            List<SelectListItem> BDAGD = new SelectList(context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO == GDNO).ToList(), "EIGDGI_GD_NO", "EIGDGI_GD_NO", 0).ToList();
                            BDAGD.Insert(0, (new SelectListItem { Text = "--Select GD Number--", Value = "0" }));
                            ViewBag.EIF_BDA_GD_NO = BDAGD;
                        }
                        else
                        {
                            List<SelectListItem> BDAGD = new List<SelectListItem>();
                            BDAGD.Insert(0, (new SelectListItem { Text = "--No GD Number--", Value = "0" }));
                            ViewBag.EIF_BDA_GD_NO = BDAGD;
                        }
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("EIFBDA");
        }

        public ActionResult CancelEIF(string E = "", int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != "")
                        {
                            EIF eifInfo = new EIF();
                            PSW_EIF_BASIC_INFO basicInfoUpdate = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                            if (basicInfoUpdate != null)
                            {
                                eifInfo.basic = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                                eifInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();

                                if (eifInfo.basic == null)
                                {
                                    message += "No EIF Basic Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eifInfo.basic.EIF_BI_BUSSINESS_NAME != null)
                                    {
                                        eifInfo.basic.EIF_BI_BUSSINESS_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eifInfo.basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_IBAN_NO;
                                        eifInfo.PayMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eifInfo.basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                                    }
                                    else
                                    {
                                        message += "Invalid Business Name";
                                    }
                                }
                                eifInfo.documents = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == E && m.EIF_DRI_AMENDMENT_NO == M).FirstOrDefault();
                                if (eifInfo.documents == null)
                                {
                                    message += "No EIF Documents Request Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY != null)
                                        eifInfo.BENECOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Bene Country";
                                    if (eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY != null)
                                        eifInfo.EXPORTERCOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Exporter Country";
                                    if (eifInfo.documents.EIF_DRI_DELIVERY_TERM != null)
                                        eifInfo.DeliveryTerm = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eifInfo.documents.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                                    else
                                        message += "Invalid Delivery Term";
                                }
                                eifInfo.hscodes = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == M).ToList();
                                if (eifInfo.hscodes == null && eifInfo.hscodes.Count() <= 0)
                                {
                                    message += "No Goods Declaration Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                eifInfo.bank = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == E && m.EIF_BA_AMENDMENT_NO == M).FirstOrDefault();
                                if (eifInfo.bank == null)
                                {
                                    message += "No EIF Bank Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    //if (eifInfo.bank.EIF_BA_BANK != null)
                                    //{
                                    //    int BankID = Convert.ToInt32(eifInfo.bank.EIF_BA_BANK);
                                    //    eifInfo.bank.EIF_BA_BANK = context.PSW_LIST_OF_BANKS.Where(m => m.LB_BANK_ID == BankID).FirstOrDefault().LB_BANK_NAME;
                                    //}
                                    //else
                                    //{
                                    //    message += "Invalid Bank";
                                    //}
                                    //if (eifInfo.bank.EIF_BA_CITY != null)
                                    //{
                                    //    int CityId = Convert.ToInt32(eifInfo.bank.EIF_BA_CITY);
                                    //    eifInfo.bank.EIF_BA_CITY = context.PSW_LIST_OF_CITIES.Where(m => m.LOCT_ID == CityId).FirstOrDefault().LOCT_NAME;
                                    //}
                                    //else
                                    //{
                                    //    message += "Invalid City";
                                    //}
                                }
                                if (DAL.DO_EDI == "true")
                                {
                                    message += DAL.MethodId1535(E, true) + Environment.NewLine;
                                    //message += "  RAW REQUEST  DETAILS FROM OUR SIDE  -------------------- " + Environment.NewLine;
                                    //message += DAL.message;
                                    if (message.Contains("211"))
                                    {
                                        basicInfoUpdate.EIF_BI_APPROVAL_STATUS = "CANCELLED";
                                        basicInfoUpdate.EIF_BI_STATUS_DATE = DateTime.Now;
                                        context.Entry(basicInfoUpdate).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message += "EIF CANCELLED Successfully." + Environment.NewLine;
                                            message += EMAILING.EIF_Cancel(E);
                                        }
                                        else
                                            message += "Unable to update transaction status." + Environment.NewLine;
                                    }
                                }
                                else
                                    message += "EDI is turned off from system configuration";
                                return View("GetEIFViewMode", eifInfo);
                            }
                            else
                            {
                                message = " Error While Canceling the EIF # " + E;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("EIFRequest", "CancelEIF", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("GetEIFViewMode");
        }
        public ActionResult SettleEIF(string E = "", int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != "")
                        {
                            EIF eifInfo = new EIF();
                            PSW_EIF_BASIC_INFO basicInfoUpdate = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                            if (basicInfoUpdate != null)
                            {
                                eifInfo.basic = basicInfoUpdate;
                                eifInfo.documents = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == E && m.EIF_DRI_AMENDMENT_NO == M).FirstOrDefault();
                                if (eifInfo.documents == null)
                                {
                                    message += "No EIF Documents Request Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY != null)
                                        eifInfo.BENECOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Bene Country";
                                    if (eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY != null)
                                        eifInfo.EXPORTERCOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Exporter Country";
                                    if (eifInfo.documents.EIF_DRI_DELIVERY_TERM != null)
                                        eifInfo.DeliveryTerm = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eifInfo.documents.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                                    else
                                        message += "Invalid Delivery Term";
                                }
                                //decimal? CurrentBDAAmounts = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_EIF_REQUEST_NO == E && m.EIF_BDA_STATUS == "APPROVED").Sum(m => m.EIF_BDA_AMOUNT_FCY);
                                //decimal? CurrentGDAmounts = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO == E).Sum(m => m.EIGDGI_ASSESSED_VALUE_USD);
                                //CurrentBDAAmounts = (CurrentBDAAmounts == null ? 0 : CurrentBDAAmounts);
                                //CurrentGDAmounts = (CurrentGDAmounts == null ? 0 : CurrentGDAmounts);
                                //decimal GD_n_BDA_Difference = (((decimal)(CurrentBDAAmounts == null ? 0 : CurrentBDAAmounts)) - (decimal)(CurrentGDAmounts == null ? 0 : CurrentGDAmounts));
                                //decimal FiAmount = (decimal)(eifInfo.documents.EIF_DRI_TOTAL_INVOICE_VALUE == null ? 0 : eifInfo.documents.EIF_DRI_TOTAL_INVOICE_VALUE);
                                //if (GD_n_BDA_Difference == 0 && CurrentBDAAmounts <= FiAmount)
                                //{
                                    eifInfo.basic = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                                    eifInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();

                                    if (eifInfo.basic == null)
                                    {
                                        message += "No EIF Basic Information Found In The System For the EIF # " + E + Environment.NewLine;
                                    }
                                    else
                                    {
                                        if (eifInfo.basic.EIF_BI_BUSSINESS_NAME != null)
                                        {
                                            eifInfo.basic.EIF_BI_BUSSINESS_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eifInfo.basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_IBAN_NO;
                                            eifInfo.PayMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eifInfo.basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                                        }
                                        else
                                        {
                                            message += "Invalid Business Name";
                                        }
                                    }
                                    eifInfo.hscodes = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == M).ToList();
                                    if (eifInfo.hscodes == null && eifInfo.hscodes.Count() <= 0)
                                    {
                                        message += "No Goods Declaration Found In The System For the EIF # " + E + Environment.NewLine;
                                    }
                                    eifInfo.bank = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == E && m.EIF_BA_AMENDMENT_NO == M).FirstOrDefault();
                                    if (eifInfo.bank == null)
                                    {
                                        message += "No EIF Bank Information Found In The System For the EIF # " + E + Environment.NewLine;
                                    }
                                    else
                                    {
                                        //if (eifInfo.bank.EIF_BA_BANK != null)
                                        //{
                                        //    int BankID = Convert.ToInt32(eifInfo.bank.EIF_BA_BANK);
                                        //    eifInfo.bank.EIF_BA_BANK = context.PSW_LIST_OF_BANKS.Where(m => m.LB_BANK_ID == BankID).FirstOrDefault().LB_BANK_NAME;
                                        //}
                                        //else
                                        //{
                                        //    message += "Invalid Bank";
                                        //}
                                        //if (eifInfo.bank.EIF_BA_CITY != null)
                                        //{
                                        //    int CityId = Convert.ToInt32(eifInfo.bank.EIF_BA_CITY);
                                        //    eifInfo.bank.EIF_BA_CITY = context.PSW_LIST_OF_CITIES.Where(m => m.LOCT_ID == CityId).FirstOrDefault().LOCT_NAME;
                                        //}
                                        //else
                                        //{
                                        //    message += "Invalid City";
                                        //}
                                    }
                                    if (DAL.DO_EDI == "true")
                                    {
                                        message += DAL.MethodId1537(E, true) + Environment.NewLine;
                                        //message += "  RAW REQUEST  DETAILS FROM OUR SIDE  -------------------- " + Environment.NewLine;
                                        //message += DAL.message;
                                        if (message.Contains("213"))
                                        {
                                            basicInfoUpdate.EIF_BI_APPROVAL_STATUS = "SETTLED";
                                            basicInfoUpdate.EIF_BI_STATUS_DATE = DateTime.Now;
                                            context.Entry(basicInfoUpdate).State = EntityState.Modified;
                                            int RowCount = context.SaveChanges();
                                            if (RowCount > 0)
                                            {
                                                message += "EIF SETTLED Successfully.";
                                            }
                                            else
                                            {
                                                message += "Problem While Updating The Status.";
                                            }
                                        }
                                    }
                                    else
                                        message += "EDI is turned off from system configuration";
                                    return View("GetEIFViewMode", eifInfo);
                                //}
                                //else
                                //{
                                //    message = "To Settle EIF ( Total Sum of BDA Amount -  Total Assessed value of GD ) Should be zero, And Total Sum of BDA Amount Should Be Less Than or Equal To The FI Amount. Current Sum of GD is :" + (CurrentGDAmounts == null ? 0 : CurrentGDAmounts) + " And Current Sum BDA is :" + (CurrentBDAAmounts == null ? 0 : CurrentBDAAmounts) + " And FI Amount is :" + FiAmount;
                                //    return View("GetEIFViewMode", eifInfo);
                                //}
                            }
                            else
                            {
                                message += " Error While Settle the EIF # " + E;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("EIFRequest", "SettleEIF", Session["USER_ID"].ToString(), ex);
                        message += ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("GetEIFViewMode");
        }

        public ActionResult RejectEIF(string E = "", int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != "")
                        {
                            EIF eifInfo = new EIF();
                            PSW_EIF_BASIC_INFO basicInfoUpdate = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                            if (basicInfoUpdate != null)
                            {
                                eifInfo.basic = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                                eifInfo.PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == E && m.EIF_PMP_AMENDMENT_NO == M).FirstOrDefault();

                                if (eifInfo.basic == null)
                                {
                                    message += "No EIF Basic Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eifInfo.basic.EIF_BI_BUSSINESS_NAME != null)
                                    {
                                        eifInfo.basic.EIF_BI_BUSSINESS_NAME = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == eifInfo.basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_IBAN_NO;
                                        eifInfo.PayMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eifInfo.basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                                    }
                                    else
                                    {
                                        message += "Invalid Business Name";
                                    }
                                }
                                eifInfo.documents = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == E && m.EIF_DRI_AMENDMENT_NO == M).FirstOrDefault();
                                if (eifInfo.documents == null)
                                {
                                    message += "No EIF Documents Request Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                else
                                {
                                    if (eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY != null)
                                        eifInfo.BENECOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Bene Country";
                                    if (eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY != null)
                                        eifInfo.EXPORTERCOUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == eifInfo.documents.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                                    else
                                        message += "Invalid Exporter Country";
                                    if (eifInfo.documents.EIF_DRI_DELIVERY_TERM != null)
                                        eifInfo.DeliveryTerm = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == eifInfo.documents.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                                    else
                                        message += "Invalid Delivery Term";
                                }
                                eifInfo.hscodes = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == M).ToList();
                                if (eifInfo.hscodes == null && eifInfo.hscodes.Count() <= 0)
                                {
                                    message += "No Goods Declaration Found In The System For the EIF # " + E + Environment.NewLine;
                                }
                                eifInfo.bank = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == E && m.EIF_BA_AMENDMENT_NO == M).FirstOrDefault();
                                if (eifInfo.bank == null)
                                {
                                    message += "No EIF Bank Information Found In The System For the EIF # " + E + Environment.NewLine;
                                }

                                basicInfoUpdate.EIF_BI_APPROVAL_STATUS = "REJECTED";
                                basicInfoUpdate.EIF_BI_STATUS_DATE = DateTime.Now;
                                context.Entry(basicInfoUpdate).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message += "EIF Rejected Successfully." + Environment.NewLine;
                                }
                                else
                                    message += "Unable to update transaction status." + Environment.NewLine;
                                return View("GetEIFViewMode", eifInfo);
                            }
                            else
                            {
                                message = " Error While Canceling the EIF # " + E;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.LogException("EIFRequest", "RejectEIF", Session["USER_ID"].ToString(), ex);
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("GetEIFViewMode");
        }

        [HttpPost]
        public JsonResult GenerateNewEIFReqNo()
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string eif_no = "";
                    string ir_no = "";
                    int MAXID = Convert.ToInt32(context.PSW_EIF_BASIC_INFO.Max(m => (decimal?)m.EIF_BI_ID)) + 1;
                    eif_no = "CBN-IMP-" + String.Format("{0:000000}", MAXID) + "-" + DateTime.Now.ToString("ddMMyyyy");
                    ir_no = "CITI-IMP-" + String.Format("{0:000000}", MAXID) + "-" + DateTime.Now.ToString("ddMMyyyy");
                    PSW_EIF_HS_CODE CheckEifNo = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == eif_no).FirstOrDefault();
                    if (CheckEifNo != null)
                    {
                        int Iteration = 1;
                        while (CheckEifNo != null)
                        {
                            int FinalMaxId = MAXID + Iteration;
                            eif_no = "CBN-IMP-" + String.Format("{0:000000}", FinalMaxId) + "-" + DateTime.Now.ToString("ddMMyyyy");
                            ir_no = "CITI-IMP-" + String.Format("{0:000000}", FinalMaxId) + "-" + DateTime.Now.ToString("ddMMyyyy");
                            CheckEifNo = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == eif_no).FirstOrDefault();
                            Iteration += 1;
                            if (CheckEifNo == null)
                            {
                                var coverletters = new
                                {
                                    EIF_NO = eif_no,
                                    IR_NO = ir_no
                                };
                                return Json(coverletters, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    var coverletter = new
                    {
                        EIF_NO = eif_no,
                        IR_NO = ir_no
                    };
                    return Json(coverletter, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public string GenerateNewBDANo()
        {
            string BDA_NO = "";
            int MAX_ID = Convert.ToInt32(context.PSW_EIF_BDA.Max(m => (decimal?)m.EIF_BDA_ID)) + 1;
            BDA_NO = "CBN-BDA-" + String.Format("{0:000000}", MAX_ID) + "-" + DateTime.Now.ToString("ddMMyyyy");
            return BDA_NO;
        }
        public string GenerateNewGDNO()
        {
            string GD_NO = "";
            int MAX_ID = Convert.ToInt32(context.PSW_EIF_GD_GENERAL_INFO.Max(m => (decimal?)m.EIGDGI_ID)) + 1;
            GD_NO = "CBN-GD-" + String.Format("{0:00000}", MAX_ID) + "-" + DateTime.Now.ToString("ddMMyyyy");
            return GD_NO;
        }

        #region EIF Not in Use
        public PartialViewResult EIFBasicInfo(int B = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (B != 0)
                        {
                            PSW_EIF_BASIC_INFO edit = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_ID == B).FirstOrDefault();
                            if (edit != null)
                            {
                                return PartialView(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + B;
                                return PartialView();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> PayMode = new SelectList(context.PSW_PAYMENT_TERM.ToList(), "PT_ID", "PT_NAME", 0).ToList();
                        PayMode.Insert(0, (new SelectListItem { Text = "--Select Mode of Import Payment--", Value = "0" }));
                        ViewBag.EIF_BI_MODE_OF_IMPORT_PAYMENT = PayMode;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                RedirectToAction("Index", "Authentication");
            }
            return PartialView();
        }
        [HttpPost]
        public PartialViewResult EIFBasicInfo(PSW_EIF_BASIC_INFO dbtable, string IsChecked)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EIF_BASIC_INFO UpdateEntity = new PSW_EIF_BASIC_INFO();

                        PSW_EIF_BASIC_INFO CheckIFExist = new PSW_EIF_BASIC_INFO();
                        if (dbtable.EIF_BI_ID == 0)
                        {
                            UpdateEntity.EIF_BI_ID = Convert.ToInt32(context.PSW_EIF_BASIC_INFO.Max(m => (decimal?)m.EIF_BI_ID)) + 1;
                            UpdateEntity.EIF_BI_ENTRY_DATETIME = DateTime.Now;
                            CheckIFExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_ID == dbtable.EIF_BI_ID).FirstOrDefault();
                            UpdateEntity.EIF_BI_EDIT_DATETIME = DateTime.Now;
                            CheckIFExist = UpdateEntity;
                        }
                        UpdateEntity.EIF_BI_EIF_REQUEST_NO = dbtable.EIF_BI_EIF_REQUEST_NO;
                        UpdateEntity.EIF_BI_REQUEST_DATE = dbtable.EIF_BI_REQUEST_DATE;
                        UpdateEntity.EIF_BI_NTN = dbtable.EIF_BI_NTN;
                        UpdateEntity.EIF_BI_BUSSINESS_NAME = dbtable.EIF_BI_BUSSINESS_NAME;
                        UpdateEntity.EIF_BI_BUSSINESS_ADDRESS = dbtable.EIF_BI_BUSSINESS_ADDRESS;
                        if (IsChecked == "on")
                            UpdateEntity.EIF_BI_IS_REMITTANCE_FROM_PAKISTAN = true;
                        else
                            UpdateEntity.EIF_BI_IS_REMITTANCE_FROM_PAKISTAN = false;
                        UpdateEntity.EIF_BI_APPROVAL_STATUS = dbtable.EIF_BI_APPROVAL_STATUS;
                        UpdateEntity.EIF_BI_STATUS_DATE = dbtable.EIF_BI_STATUS_DATE;
                        UpdateEntity.EIF_BI_STRN = dbtable.EIF_BI_STRN;
                        UpdateEntity.EIF_BI_MODE_OF_IMPORT_PAYMENT = dbtable.EIF_BI_MODE_OF_IMPORT_PAYMENT;
                        UpdateEntity.EIF_BI_MAKER_ID = "1";
                        UpdateEntity.EIF_BI_CHECKER_ID = "1";
                        if (CheckIFExist == null)
                            context.PSW_EIF_BASIC_INFO.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> PayMode = new SelectList(context.PSW_PAYMENT_TERM.ToList(), "PT_ID", "PT_NAME", 0).ToList();
                        PayMode.Insert(0, (new SelectListItem { Text = "--Select Mode of Import Payment--", Value = "0" }));
                        ViewBag.EIF_BI_MODE_OF_IMPORT_PAYMENT = PayMode;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                RedirectToAction("Index", "Authentication");
            }
            return PartialView();
        }
        public PartialViewResult EIFBasicInfoList()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    return PartialView(context.PSW_EIF_BASIC_INFO.ToList());
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult EIFDocumentRequestInfo(int E = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != 0)
                        {
                            PSW_EIF_DOCUMENT_REQUEST_INFO edit = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_ID == E).FirstOrDefault();
                            if (edit != null)
                            {
                                return PartialView(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + E;
                                return PartialView();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> BenefCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.ToList(), "LC_ID", "LC_NAME", 0).ToList();
                        BenefCountry.Insert(0, (new SelectListItem { Text = "--Select Beneficiary Country--", Value = "0" }));
                        ViewBag.EIF_DRI_BENEFICIARY_COUNTRY = BenefCountry;
                        List<SelectListItem> ExpCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.ToList(), "LC_ID", "LC_NAME", 0).ToList();
                        ExpCountry.Insert(0, (new SelectListItem { Text = "--Select Exporter Country--", Value = "0" }));
                        ViewBag.EIF_DRI_EXPORTER_COUNTRY = ExpCountry;
                        List<SelectListItem> DeliveryTerm = new SelectList(context.PSW_DELIVERY_TERMS.ToList(), "DT_ID", "DT_DESCRIPTION", 0).ToList();
                        DeliveryTerm.Insert(0, (new SelectListItem { Text = "--Select Delivery Term--", Value = "0" }));
                        ViewBag.EIF_DRI_DELIVERY_TERM = DeliveryTerm;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                RedirectToAction("Index", "Authentication");
            }
            return PartialView();
        }
        [HttpPost]
        public PartialViewResult EIFDocumentRequestInfo(PSW_EIF_DOCUMENT_REQUEST_INFO dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EIF_DOCUMENT_REQUEST_INFO UpdateEntity = new PSW_EIF_DOCUMENT_REQUEST_INFO();

                        PSW_EIF_DOCUMENT_REQUEST_INFO CheckIFExist = new PSW_EIF_DOCUMENT_REQUEST_INFO();
                        if (dbtable.EIF_DRI_ID == 0)
                        {
                            UpdateEntity.EIF_DRI_ID = Convert.ToInt32(context.PSW_EIF_DOCUMENT_REQUEST_INFO.Max(m => (decimal?)m.EIF_DRI_ID)) + 1;
                            UpdateEntity.EIF_DRI_ENTRY_DATETIME = DateTime.Now;
                            CheckIFExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_ID == dbtable.EIF_DRI_ID).FirstOrDefault();
                            UpdateEntity.EIF_DRI_EDIT_DATETIME = DateTime.Now;
                            CheckIFExist = UpdateEntity;
                        }
                        UpdateEntity.EIF_DRI_BI_EIF_REQUEST_NO = dbtable.EIF_DRI_BI_EIF_REQUEST_NO;
                        UpdateEntity.EIF_DRI_BENEFICIARY_NAME = dbtable.EIF_DRI_BENEFICIARY_NAME;
                        UpdateEntity.EIF_DRI_BENEFICIARY_COUNTRY = dbtable.EIF_DRI_BENEFICIARY_COUNTRY;
                        UpdateEntity.EIF_DRI_BENEFICIARY_ADDRESS = dbtable.EIF_DRI_BENEFICIARY_ADDRESS;
                        UpdateEntity.EIF_DRI_BENEFICIARY_IBAN = dbtable.EIF_DRI_BENEFICIARY_IBAN;
                        UpdateEntity.EIF_DRI_EXPORTER_NAME = dbtable.EIF_DRI_EXPORTER_NAME;
                        UpdateEntity.EIF_DRI_EXPORTER_ADDRESS = dbtable.EIF_DRI_EXPORTER_ADDRESS;
                        UpdateEntity.EIF_DRI_EXPORTER_COUNTRY = dbtable.EIF_DRI_EXPORTER_COUNTRY;
                        UpdateEntity.EIF_DRI_PORT_OF_SHIPMENT = dbtable.EIF_DRI_PORT_OF_SHIPMENT;
                        UpdateEntity.EIF_DRI_DELIVERY_TERM = dbtable.EIF_DRI_DELIVERY_TERM;
                        UpdateEntity.EIF_DRI_LC_CONT_NO = dbtable.EIF_DRI_LC_CONT_NO;
                        UpdateEntity.EIF_DRI_TOTAL_INVOICE_VALUE = dbtable.EIF_DRI_TOTAL_INVOICE_VALUE;
                        UpdateEntity.EIF_DRI_EXCHANGE_RATE = dbtable.EIF_DRI_EXCHANGE_RATE;
                        UpdateEntity.EIF_DRI_MAKER_ID = "1";
                        UpdateEntity.EIF_DRI_CHECKER_ID = "1";
                        if (CheckIFExist == null)
                            context.PSW_EIF_DOCUMENT_REQUEST_INFO.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> BenefCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.ToList(), "LC_ID", "LC_NAME", 0).ToList();
                        BenefCountry.Insert(0, (new SelectListItem { Text = "--Select Beneficiary Country--", Value = "0" }));
                        ViewBag.EIF_DRI_BENEFICIARY_COUNTRY = BenefCountry;
                        List<SelectListItem> ExpCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.ToList(), "LC_ID", "LC_NAME", 0).ToList();
                        ExpCountry.Insert(0, (new SelectListItem { Text = "--Select Exporter Country--", Value = "0" }));
                        ViewBag.EIF_DRI_EXPORTER_COUNTRY = ExpCountry;
                        List<SelectListItem> DeliveryTerm = new SelectList(context.PSW_DELIVERY_TERMS.ToList(), "DT_ID", "DT_DESCRIPTION", 0).ToList();
                        DeliveryTerm.Insert(0, (new SelectListItem { Text = "--Select Delivery Term--", Value = "0" }));
                        ViewBag.EIF_DRI_DELIVERY_TERM = DeliveryTerm;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                RedirectToAction("Index", "Authentication");
            }
            return PartialView();
        }
        public PartialViewResult EIFDocumentRequestInfoList()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    return PartialView(context.PSW_EIF_DOCUMENT_REQUEST_INFO.ToList());
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult EIFBankInfo(int E = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != 0)
                        {
                            PSW_EIF_BANK_INFO edit = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_ID == E).FirstOrDefault();
                            if (edit != null)
                            {
                                return PartialView(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + E;
                                return PartialView();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                RedirectToAction("Index", "Authentication");
            }
            return PartialView();
        }
        [HttpPost]
        public PartialViewResult EIFBankInfo(PSW_EIF_BANK_INFO dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EIF_BANK_INFO UpdateEntity = new PSW_EIF_BANK_INFO();

                        PSW_EIF_BANK_INFO CheckIFExist = new PSW_EIF_BANK_INFO();
                        if (dbtable.EIF_BA_ID == 0)
                        {
                            UpdateEntity.EIF_BA_ID = Convert.ToInt32(context.PSW_EIF_BANK_INFO.Max(m => (decimal?)m.EIF_BA_ID)) + 1;
                            UpdateEntity.EIF_BA_ENTRY_DATETIME = DateTime.Now;
                            CheckIFExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_ID == dbtable.EIF_BA_ID).FirstOrDefault();
                            UpdateEntity.EIF_BA_EDIT_DATETIME = DateTime.Now;
                            CheckIFExist = UpdateEntity;
                        }
                        UpdateEntity.EIF_BA_EXPIRY_DATE = dbtable.EIF_BA_EXPIRY_DATE;
                        UpdateEntity.EIF_BA_IBAN = dbtable.EIF_BA_IBAN;
                        UpdateEntity.EIF_BA_INNTENDED_PAYMENT_DATE = dbtable.EIF_BA_INNTENDED_PAYMENT_DATE;
                        UpdateEntity.EIF_BA_TRANSPORT_DOC_DATE = dbtable.EIF_BA_TRANSPORT_DOC_DATE;
                        UpdateEntity.EIF_BA_FINAL_DATE_OF_SHIPMENT = dbtable.EIF_BA_FINAL_DATE_OF_SHIPMENT;
                        UpdateEntity.EIF_BA_REMARK = dbtable.EIF_BA_REMARK;
                        UpdateEntity.EIF_BA_SBP_APPROVAL_SUBJECT = dbtable.EIF_BA_SBP_APPROVAL_SUBJECT;
                        UpdateEntity.EIF_BA_SBP_APPROVAL_NO = dbtable.EIF_BA_SBP_APPROVAL_NO;
                        UpdateEntity.EIF_BA_SBP_APPROVAL_DATE = dbtable.EIF_BA_SBP_APPROVAL_DATE;
                        UpdateEntity.EIF_BA_BANK = dbtable.EIF_BA_BANK;
                        UpdateEntity.EIF_BA_CITY = dbtable.EIF_BA_CITY;
                        UpdateEntity.EIF_BA_BRANCH = dbtable.EIF_BA_BRANCH;
                        UpdateEntity.EIF_BA_APPROVAL_REMARKS = dbtable.EIF_BA_APPROVAL_REMARKS;
                        UpdateEntity.EIF_BA_MAKER_ID = "1";
                        UpdateEntity.EIF_BA_CHECKER_ID = "1";
                        if (CheckIFExist == null)
                            context.PSW_EIF_BANK_INFO.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                RedirectToAction("Index", "Authentication");
            }
            return PartialView();
        }
        public PartialViewResult EIFBankInfoList()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    return PartialView(context.PSW_EIF_BANK_INFO.ToList());
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        #endregion

        [HttpPost]
        public JsonResult AddHsCodeToEIF(PSW_EIF_HS_CODE dbtable)
        {
            bool IsHsCode = false;
            bool CashMargin = false;
            string ImpoPol = "";
            string message = "";
            dynamic Result = new decimal();
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_LIST_OF_COMMODITIES CheckCommodity = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == dbtable.EIF_HC_CODE).FirstOrDefault();
                        if (CheckCommodity != null)
                        {
                            PSW_CASH_MARGIN_HS_CODES CheckCashMargin = context.PSW_CASH_MARGIN_HS_CODES.Where(m => m.CM_ID == CheckCommodity.LCOM_ID).FirstOrDefault();
                            if (CheckCashMargin != null)
                            {
                                CashMargin = true;
                            }
                            PSW_IMPORT_POLICY CheckImpPol = context.PSW_IMPORT_POLICY.Where(m => m.IP_PCT_CODE == dbtable.EIF_HC_CODE).FirstOrDefault();
                            if (CheckImpPol != null)
                            {
                                ImpoPol += "Import Policy Exist." + Environment.NewLine + "Commodity Description : " + CheckImpPol.IP_COMMODITY_DESCRIPTION + Environment.NewLine + "Heading Description : " + CheckImpPol.IP_HEADING_DESCRIPTION + "." + Environment.NewLine;
                            }
                        }

                        string HSCODE = DAL.RemoveNewLineAndTabs(dbtable.EIF_HC_CODE).Trim();
                        PSW_LIST_OF_COMMODITIES CheckHsCode = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == HSCODE && m.LCOM_STATUS == true).FirstOrDefault();
                        if (CheckHsCode != null)
                        {
                            int NextAmendmentNo = 0;
                            PSW_EIF_MASTER_DETAILS CheckAmend = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == dbtable.EIF_HC_EIF_REQUEST_NO && m.EIF_BI_AMENDMENT_NO == dbtable.EIF_HC_AMENDMENT_NO).FirstOrDefault();
                            if (CheckAmend != null)
                            {
                                NextAmendmentNo = CheckAmend.EIF_BI_AMENDMENT_NO + 1;
                            }
                            //PSW_EIF_HS_CODE CurrentEntity = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_CODE == dbtable.EIF_HC_CODE && m.EIF_HC_EIF_REQUEST_NO == dbtable.EIF_HC_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == NextAmendmentNo).FirstOrDefault();
                            //if (CurrentEntity == null)
                            //{
                            PSW_EIF_HS_CODE CurrentEntity = new PSW_EIF_HS_CODE();
                            CurrentEntity.EIF_HC_ID = Convert.ToInt32(context.PSW_EIF_HS_CODE.Max(m => (int?)m.EIF_HC_ID)) + 1;
                            CurrentEntity.EIF_HC_ENTRTY_DATETIME = DateTime.Now;
                            CurrentEntity.EIF_HC_CODE = dbtable.EIF_HC_CODE;
                            CurrentEntity.EIF_HC_DESCRIPTION = dbtable.EIF_HC_DESCRIPTION;
                            CurrentEntity.EIF_HC_QUANTITY = dbtable.EIF_HC_QUANTITY;
                            CurrentEntity.EIF_HC_UOM = dbtable.EIF_HC_UOM;
                            CurrentEntity.EIF_HC_ORGIN = dbtable.EIF_HC_ORGIN;
                            CurrentEntity.EIF_HC_MAKER_ID = Session["USER_ID"].ToString();
                            if (CurrentEntity.EIF_HC_MAKER_ID == "Admin123")
                                CurrentEntity.EIF_HC_CHECKER_ID = CurrentEntity.EIF_HC_MAKER_ID;
                            else
                                CurrentEntity.EIF_HC_CHECKER_ID = null;
                            CurrentEntity.EIF_HC_EIF_REQUEST_NO = dbtable.EIF_HC_EIF_REQUEST_NO;
                            CurrentEntity.EIF_HC_IS_SAMPLE = dbtable.EIF_HC_IS_SAMPLE;
                            CurrentEntity.EIF_HC_AMENDMENT_NO = NextAmendmentNo;
                            CurrentEntity.EIF_HC_SAMPLE_VALUE = dbtable.EIF_HC_SAMPLE_VALUE;
                            if (ImpoPol != "")
                                CurrentEntity.EIF_HC_POLICY = ImpoPol;
                            else
                                CurrentEntity.EIF_HC_POLICY = null;
                            CurrentEntity.EIF_HC_AMOUNT_AGAINST_HS_CODE = dbtable.EIF_HC_AMOUNT_AGAINST_HS_CODE;
                            context.PSW_EIF_HS_CODE.Add(CurrentEntity);
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                ImpoPol += "Added Sucessfully." + Environment.NewLine;
                                IsHsCode = true;
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                IsHsCode = false;
                            }
                            //}
                            //else
                            //{
                            //    message = "HS Code " + dbtable.EIF_HC_CODE + " Already Exist ";
                            //    IsHsCode = false;
                            //}
                        }
                        else
                        {
                            message = "No Hs Code Found In The System";
                            IsHsCode = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }

                    Result = new
                    {
                        HsCodeAddSuccess = IsHsCode,
                        CashMarginFall = CashMargin,
                        ImportPolicy = ImpoPol,
                        exp = message
                    };
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult GetEifHsCodeDescription(string HsCode = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (HsCode != "")
                    {
                        PSW_LIST_OF_COMMODITIES CheckHsCode = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == HsCode && m.LCOM_STATUS == true && m.LCOM_ISAUTH == true).FirstOrDefault();
                        if (CheckHsCode != null)
                        {
                            var HsCodeDescription = CheckHsCode.LCOM_NAME;
                            return Json(HsCodeDescription, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json("Invalid Hs Code", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Hs Code", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult CheckBannedHsCode(string HsCode = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (HsCode != "")
                    {
                        PSW_LIST_OF_COMMODITIES CheckHsCode = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == HsCode && m.LCOM_STATUS == true && m.LCOM_ISAUTH == true).FirstOrDefault();
                        if (CheckHsCode != null)
                        {
                            PSW_NEGATIVE_LIST_OF_COMMODITIES CheckBannedHsCode = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_LCOM_ID == CheckHsCode.LCOM_ID).FirstOrDefault();
                            if (CheckBannedHsCode != null)
                                return Json("Hs Code exist in the banned item.", JsonRequestBehavior.AllowGet);
                            else
                                return Json("", JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json("Invalid Hs Code", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Hs Code", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult VerifyPortOfShipment(string EnteredPort = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (EnteredPort != "")
                    {
                        PSW_PORT_OF_SHIPPING CheckPort = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_CODE == EnteredPort && m.POS_ISAUTH == true).FirstOrDefault();
                        if (CheckPort != null)
                        {
                            var coverletters = new
                            {
                                Code = CheckPort.POS_CODE,
                                Name = CheckPort.POS_NAME
                            };
                            return Json(coverletters, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json("Invalid Port Name", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Port Name", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult GetEIFValueFromGD(string GD)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (GD != "")
                    {
                        PSW_EIF_GD_GENERAL_INFO_LIST CheckGD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO == GD).FirstOrDefault();
                        if (CheckGD != null)
                        {
                            PSW_EIF_GD_GENERAL_INFO GDInfo = context.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_GD_NO == CheckGD.EIGDGI_GD_NO && m.EIGDGI_AMENDMENT_NO == CheckGD.EIGDGI_AMENDMENT_NO).FirstOrDefault();
                            PSW_EIF_GD_IMPORT_EXPORT_INFO GDEmportExport = context.PSW_EIF_GD_IMPORT_EXPORT_INFO.Where(m => m.EIGDIE_GD_NO == CheckGD.EIGDGI_GD_NO && m.EIGDIE_AMENDMENT_NO == CheckGD.EIGDGI_AMENDMENT_NO).FirstOrDefault();
                            PSW_DELIVERY_TERMS GetDT = context.PSW_DELIVERY_TERMS.Where(m => m.DT_CODE == GDInfo.EIGDGI_DELIVERY_TERM).FirstOrDefault();
                            var coverletters = new
                            {
                                Name = GDEmportExport.EIGDIE_CONSIGNOR_NAME,
                                Address = GDEmportExport.EIGDIE_CONSIGNOR_ADDRESS,
                                DelivertTerm = GetDT.DT_ID,
                                TotalValue = GDInfo.EIGDGI_TOTAL_DECLARED_VALUE,
                                ExchangeRate = GDInfo.EIGDGI_EXCHANGE_RATE
                            };
                            return Json(coverletters, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json("Invalid Port Name", JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Invalid Port Name", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public PartialViewResult EIFHSCodeList(string H = "", int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    if (H != "")
                    {
                        int NextAmendmentNo = 0;
                        PSW_EIF_MASTER_DETAILS CheckAmend = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == H && m.EIF_BI_AMENDMENT_NO == M).FirstOrDefault();
                        if (CheckAmend != null)
                        {
                            NextAmendmentNo = CheckAmend.EIF_BI_AMENDMENT_NO + 1;
                        }
                        List<PSW_EIF_HS_CODE> hscodes = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == H && m.EIF_HC_AMENDMENT_NO == NextAmendmentNo).ToList();
                        return PartialView(hscodes);
                    }
                    else
                        return PartialView();
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult RemoveHSCode(PSW_EIF_HS_CODE dbtable)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (dbtable.EIF_HC_EIF_REQUEST_NO != "" && dbtable.EIF_HC_CODE != "")
                        {
                            PSW_EIF_HS_CODE entry = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_ID == dbtable.EIF_HC_ID).FirstOrDefault();
                            if (entry != null)
                            {
                                context.Entry(entry).State = EntityState.Deleted;
                                int rowcount = context.SaveChanges();
                                if (rowcount > 0)
                                {
                                    message = "";
                                }
                                else
                                {
                                    message = "Error While Removing HS Code From The EIF";
                                }
                            }
                            else
                            {
                                message = "No Data Found To Remove";
                            }
                        }
                        else
                        {
                            message = "Invalid Data Passed.";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = "Exception Occurred : " + ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        [HttpPost]
        public JsonResult GetPreviousEIFHsCode(string E = "" , int AM = 0)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    try
                    {
                        if (E != "")
                        {
                            AM += 1;
                            int PAM = (AM - 1);
                            List<PSW_EIF_HS_CODE> CheckIfExist = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == AM).ToList();
                            if (CheckIfExist.Count > 0)
                            {
                                foreach (PSW_EIF_HS_CODE HSCodeToDelete in CheckIfExist)
                                {
                                    context.PSW_EIF_HS_CODE.Remove(HSCodeToDelete);
                                    RowCount += context.SaveChanges();
                                }
                            }


                            List<PSW_EIF_HS_CODE> gethscode = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == PAM).ToList();
                            if (gethscode.Count > 0)
                            {
                                RowCount = 0;
                                foreach (PSW_EIF_HS_CODE HSCode in gethscode)
                                {
                                    PSW_EIF_HS_CODE Entity = new PSW_EIF_HS_CODE();
                                    Entity.EIF_HC_ID = Convert.ToInt32(context.PSW_EIF_HS_CODE.Max(m => (decimal?)m.EIF_HC_ID)) + 1;
                                    Entity.EIF_HC_EIF_REQUEST_NO = HSCode.EIF_HC_EIF_REQUEST_NO;
                                    Entity.EIF_HC_CODE = HSCode.EIF_HC_CODE;
                                    Entity.EIF_HC_DESCRIPTION = HSCode.EIF_HC_DESCRIPTION;
                                    Entity.EIF_HC_QUANTITY = HSCode.EIF_HC_QUANTITY;
                                    Entity.EIF_HC_UOM = HSCode.EIF_HC_UOM;
                                    Entity.EIF_HC_ORGIN = HSCode.EIF_HC_ORGIN;
                                    Entity.EIF_HC_IS_SAMPLE = HSCode.EIF_HC_IS_SAMPLE;
                                    Entity.EIF_HC_SAMPLE_VALUE = HSCode.EIF_HC_SAMPLE_VALUE;
                                    Entity.EIF_HC_ENTRTY_DATETIME = DateTime.Now;
                                    Entity.EIF_HC_MAKER_ID = Session["USER_ID"].ToString();
                                    if (Entity.EIF_HC_MAKER_ID == "Admin123")
                                        Entity.EIF_HC_CHECKER_ID = Entity.EIF_HC_MAKER_ID;
                                    else
                                        Entity.EIF_HC_CHECKER_ID = null;
                                    Entity.EIF_HC_AMENDMENT_NO = (HSCode.EIF_HC_AMENDMENT_NO + 1);
                                    Entity.EIF_HC_POLICY = HSCode.EIF_HC_POLICY;
                                    Entity.EIF_HC_AMOUNT_AGAINST_HS_CODE = HSCode.EIF_HC_AMOUNT_AGAINST_HS_CODE;
                                    context.PSW_EIF_HS_CODE.Add(Entity);
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message = "";
                                }
                            }
                            else
                            {
                                message = "Unable to fetch previous HS Code on EIF Request No : " + E;
                            }
                        }
                        else
                        {
                            message = "No HS Code found";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult AddHsCodeFromGDToEIF(string E = "", int AM = 0, string GDNo = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    try
                    {
                        if (E != "")
                        {
                            int NextAmendmentNo = 0;
                            PSW_EIF_MASTER_DETAILS CheckAmend = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == E && m.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
                            if (CheckAmend != null)
                            {
                                NextAmendmentNo = CheckAmend.EIF_BI_AMENDMENT_NO + 1;
                            }
                            List<PSW_EIF_HS_CODE> CheckIfExist = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == E && m.EIF_HC_AMENDMENT_NO == NextAmendmentNo).ToList();
                            if (CheckIfExist.Count > 0)
                            {
                                foreach (PSW_EIF_HS_CODE HSCodeToDelete in CheckIfExist)
                                {
                                    context.PSW_EIF_HS_CODE.Remove(HSCodeToDelete);
                                    RowCount += context.SaveChanges();
                                }
                            }

                            PSW_EIF_GD_GENERAL_INFO_LIST GetGdData = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO == GDNo).FirstOrDefault();
                            if (GetGdData != null)
                            {
                                List<PSW_GD_IMORT_HS_CODE> gethscode = context.PSW_GD_IMORT_HS_CODE.Where(m => m.IHC_GD_NO == GetGdData.EIGDGI_GD_NO && m.IHC_AMENDMENT_NO == GetGdData.EIGDGI_AMENDMENT_NO).ToList();
                                if (gethscode.Count > 0)
                                {
                                    RowCount = 0;
                                    foreach (PSW_GD_IMORT_HS_CODE HSCode in gethscode)
                                    {
                                        string HC_Description = "";
                                        if (HSCode.IHC_HS_CODE != null && HSCode.IHC_HS_CODE != 0)
                                        {
                                            HC_Description = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == HSCode.IHC_HS_CODE.ToString()).Select(m => m.LCOM_NAME).FirstOrDefault();
                                        }
                                        PSW_EIF_HS_CODE Entity = new PSW_EIF_HS_CODE();
                                        Entity.EIF_HC_ID = Convert.ToInt32(context.PSW_EIF_HS_CODE.Max(m => (decimal?)m.EIF_HC_ID)) + 1;
                                        Entity.EIF_HC_EIF_REQUEST_NO = E;
                                        if (HSCode.IHC_HS_CODE != null && HSCode.IHC_HS_CODE != 0)
                                            Entity.EIF_HC_CODE = HSCode.IHC_HS_CODE.ToString();
                                        else
                                            Entity.EIF_HC_CODE = null;
                                        Entity.EIF_HC_DESCRIPTION = HC_Description;
                                        Entity.EIF_HC_QUANTITY = HSCode.IHC_QUANTITY;
                                        Entity.EIF_HC_UOM = HSCode.IHC_UOM;
                                        Entity.EIF_HC_ORGIN = null;
                                        Entity.EIF_HC_IS_SAMPLE = "N";
                                        Entity.EIF_HC_SAMPLE_VALUE = 0;
                                        Entity.EIF_HC_ENTRTY_DATETIME = DateTime.Now;
                                        Entity.EIF_HC_MAKER_ID = Session["USER_ID"].ToString();
                                        if (Entity.EIF_HC_MAKER_ID == "Admin123")
                                            Entity.EIF_HC_CHECKER_ID = Entity.EIF_HC_MAKER_ID;
                                        else
                                            Entity.EIF_HC_CHECKER_ID = null;
                                        Entity.EIF_HC_AMENDMENT_NO = NextAmendmentNo;
                                        Entity.EIF_HC_POLICY = null;
                                        Entity.EIF_HC_AMOUNT_AGAINST_HS_CODE = HSCode.IHC_UNIT_PRICE;
                                        context.PSW_EIF_HS_CODE.Add(Entity);
                                        RowCount += context.SaveChanges();
                                    }
                                    if (RowCount > 0)
                                    {
                                        message = "Successfully added Hs Code from GD#" + GDNo;
                                    }
                                }
                                else
                                {
                                    message = "Unable to fetch previous HS Code on EIF Request No : " + E;
                                }
                            }
                            else
                                message = "No Gd found in the system against GD No# " + GDNo;
                        }
                        else
                        {
                            message = "No HS Code found";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #region EIF BDA
        [HttpGet]
        public ActionResult EIFBDA(int B = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFBDA", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string FIno = "";
                    string GDNo = "";
                    try
                    {
                        if (B != 0)
                        {
                            PSW_EIF_BDA edit = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_ID == B).FirstOrDefault();
                            if (edit != null)
                            {
                                FIno = edit.EIF_BDA_EIF_REQUEST_NO;
                                GDNo = edit.EIF_BDA_GD_NO;
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record";
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EIFRequest", "EIFBDA", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> EifRequests = new SelectList(context.PSW_EIF_MASTER_DETAILS.ToList(), "EIF_BI_EIF_REQUEST_NO", "EIF_BI_EIF_REQUEST_NO", 0).ToList();
                        EifRequests.Insert(0, (new SelectListItem { Text = "--Select Request --", Value = "0" }));
                        ViewBag.EIF_BDA_EIF_REQUEST_NO = EifRequests;
                        List<SelectListItem> Currency = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        Currency.Insert(0, (new SelectListItem { Text = "--Select Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_CURRENCY_FCY = Currency;
                        List<SelectListItem> TotalBDA = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        TotalBDA.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY = TotalBDA;
                        List<SelectListItem> SampleAmount = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        SampleAmount.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY = SampleAmount;
                        List<SelectListItem> NetBDA = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        NetBDA.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY = NetBDA;
                        List<SelectListItem> PaymentCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_STATUS == true && m.LC_ISAUTH == true).ToList(), "LC_NAME", "LC_NAME", 0).ToList();
                        PaymentCountry.Insert(0, (new SelectListItem { Text = "--Select Payment Country--", Value = "0" }));
                        ViewBag.EIF_BDA_PAYMENT_COUNTRY = PaymentCountry;
                        List<SelectListItem> BDAGD;
                        if (FIno == "" && GDNo == "")
                        {
                            BDAGD = new List<SelectListItem>();
                        }
                        else
                        {
                            BDAGD = new SelectList(context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO == FIno).ToList(), "EIGDGI_GD_NO", "EIGDGI_GD_NO", GDNo).ToList();
                            BDAGD.Insert(0, (new SelectListItem { Text = "--Select GD Number--", Value = "0" }));
                        }
                        ViewBag.EIF_BDA_GD_NO = BDAGD;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        // Post:
        [ActionName("EIFBDA")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EIFBDA_post(PSW_EIF_BDA dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string GDNO = "";
                    try
                    {
                        if (dbtable.EIF_BDA_EIF_REQUEST_NO != "0")
                        {
                            if (dbtable.EIF_BDA_GD_NO == "0" || dbtable.EIF_BDA_GD_NO == null)
                            {
                                PSW_EIF_MASTER_DETAILS eif = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == dbtable.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                                if (eif != null)
                                {
                                    PSW_AUTH_PAY_MODES paymode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eif.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                                    if (paymode.APM_CODE == 302)
                                    {
                                        if (dbtable.EIF_BDA_GD_NO == null || dbtable.EIF_BDA_GD_NO == "0")
                                        {
                                            message = "GD Number is required";
                                            return View();
                                        }
                                    }
                                }
                            }
                            GDNO = dbtable.EIF_BDA_GD_NO;
                            PSW_EIF_BDA UpdateEntity = new PSW_EIF_BDA();
                            if (dbtable.EIF_BDA_ID == 0)
                            {
                                UpdateEntity.EIF_BDA_ID = Convert.ToInt32(context.PSW_EIF_BDA.Max(m => (decimal?)m.EIF_BDA_ID)) + 1;
                                UpdateEntity.EIF_BDA_ENTRY_DATETIME = DateTime.Now;
                                UpdateEntity.EIF_BDA_AMENDMENT_NO = 0;
                                UpdateEntity.EIF_BDA_NO = GenerateNewBDANo();
                            }
                            else
                            {
                                PSW_EIF_BDA AmendedRecord = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_ID == dbtable.EIF_BDA_ID).FirstOrDefault();
                                UpdateEntity = new PSW_EIF_BDA();
                                UpdateEntity.EIF_BDA_ID = Convert.ToInt32(context.PSW_EIF_BDA.Max(m => (decimal?)m.EIF_BDA_ID)) + 1;
                                UpdateEntity.EIF_BDA_AMENDMENT_NO = Convert.ToInt32(context.PSW_EIF_BDA.Where(m => m.EIF_BDA_NO == AmendedRecord.EIF_BDA_NO).Max(m => m.EIF_BDA_AMENDMENT_NO)) + 1;
                                UpdateEntity.EIF_BDA_NO = AmendedRecord.EIF_BDA_NO;
                                UpdateEntity.EIF_BDA_ENTRY_DATETIME = DateTime.Now;
                            }
                            UpdateEntity.EIF_BDA_EIF_REQUEST_NO = dbtable.EIF_BDA_EIF_REQUEST_NO;
                            UpdateEntity.EIF_BDA_DOCUMENT_REQUEST_NO = dbtable.EIF_BDA_DOCUMENT_REQUEST_NO;
                            UpdateEntity.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY = dbtable.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY;
                            UpdateEntity.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY = dbtable.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY;
                            UpdateEntity.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE = dbtable.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE == null ? 0 : dbtable.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE;
                            UpdateEntity.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY = dbtable.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY;
                            UpdateEntity.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY = dbtable.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY;
                            UpdateEntity.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY = dbtable.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY;
                            UpdateEntity.EIF_BDA_NET_BDA_AMOUNT_PKR = dbtable.EIF_BDA_NET_BDA_AMOUNT_PKR;
                            UpdateEntity.EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY = dbtable.EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY;
                            UpdateEntity.EIF_BDA_AMOUNT_IN_WORDS = dbtable.EIF_BDA_AMOUNT_IN_WORDS;
                            UpdateEntity.EIF_BDA_CURRENCY_FCY = dbtable.EIF_BDA_CURRENCY_FCY;
                            UpdateEntity.EIF_BDA_EXCHANGE_RATE_FCY = dbtable.EIF_BDA_EXCHANGE_RATE_FCY;
                            UpdateEntity.EIF_BDA_AMOUNT_FCY = dbtable.EIF_BDA_AMOUNT_FCY;
                            UpdateEntity.EIF_BDA_Balance_Amount_FCY = dbtable.EIF_BDA_Balance_Amount_FCY;
                            UpdateEntity.EIF_BDA_AMOUNT_PKR = dbtable.EIF_BDA_AMOUNT_PKR;
                            UpdateEntity.EIF_BDA_COMMMISSION_FCY = dbtable.EIF_BDA_COMMMISSION_FCY == null ? 0 : dbtable.EIF_BDA_COMMMISSION_FCY;
                            UpdateEntity.EIF_BDA_COMMISSION_PKR = dbtable.EIF_BDA_COMMISSION_PKR == null ? 0 : dbtable.EIF_BDA_COMMISSION_PKR;
                            UpdateEntity.EIF_BDA_FED_AMOUNT_FCY = dbtable.EIF_BDA_FED_AMOUNT_FCY == null ? 0 : dbtable.EIF_BDA_FED_AMOUNT_FCY;
                            UpdateEntity.EIF_BDA_FED_AMOUNT_PKR = dbtable.EIF_BDA_FED_AMOUNT_PKR == null ? 0 : dbtable.EIF_BDA_FED_AMOUNT_PKR;
                            UpdateEntity.EIF_BDA_SWIFT_CHARGES_PKR = dbtable.EIF_BDA_SWIFT_CHARGES_PKR == null ? 0 : dbtable.EIF_BDA_SWIFT_CHARGES_PKR;
                            UpdateEntity.EIF_BDA_OTHER_CHARGES_PKR = dbtable.EIF_BDA_OTHER_CHARGES_PKR == null ? 0 : dbtable.EIF_BDA_OTHER_CHARGES_PKR;
                            UpdateEntity.EIF_BDA_FLEX_REFERENCE = dbtable.EIF_BDA_FLEX_REFERENCE;
                            UpdateEntity.EIF_BDA_PAYMENT_COUNTRY = dbtable.EIF_BDA_PAYMENT_COUNTRY;
                            UpdateEntity.EIF_BDA_BILL_OF_LADING = dbtable.EIF_BDA_BILL_OF_LADING;
                            UpdateEntity.EIF_BDA_REMARKS = dbtable.EIF_BDA_REMARKS;
                            UpdateEntity.EIF_BDA_GD_NO = dbtable.EIF_BDA_GD_NO == "0" || dbtable.EIF_BDA_GD_NO == null ? null : dbtable.EIF_BDA_GD_NO;
                            UpdateEntity.EIF_BDA_STATUS = "initiated";
                            UpdateEntity.EIF_BDA_MAKER_ID = Session["USER_ID"].ToString();
                            if (UpdateEntity.EIF_BDA_MAKER_ID == "Admin123")
                                UpdateEntity.EIF_BDA_CHECKER_ID = UpdateEntity.EIF_BDA_MAKER_ID;
                            else
                                UpdateEntity.EIF_BDA_CHECKER_ID = null;
                            context.PSW_EIF_BDA.Add(UpdateEntity);
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                        else
                        {
                            message = "Please Select Request No.";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EIFRequest", "EIFBDA_post", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> EifRequests = new SelectList(context.PSW_EIF_MASTER_DETAILS.ToList(), "EIF_BI_EIF_REQUEST_NO", "EIF_BI_EIF_REQUEST_NO", 0).ToList();
                        EifRequests.Insert(0, (new SelectListItem { Text = "--Select Request --", Value = "0" }));
                        ViewBag.EIF_BDA_EIF_REQUEST_NO = EifRequests;
                        List<SelectListItem> Currency = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        Currency.Insert(0, (new SelectListItem { Text = "--Select Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_CURRENCY_FCY = Currency;
                        List<SelectListItem> TotalBDA = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        TotalBDA.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY = TotalBDA;
                        List<SelectListItem> SampleAmount = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        SampleAmount.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY = SampleAmount;
                        List<SelectListItem> NetBDA = new SelectList(context.PSW_CURRENCY.Where(m => m.CUR_STATUS == true).ToList(), "CUR_ID", "CUR_NAME", 0).ToList();
                        NetBDA.Insert(0, (new SelectListItem { Text = "--Currency--", Value = "0" }));
                        ViewBag.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY = NetBDA;
                        List<SelectListItem> PaymentCountry = new SelectList(context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_STATUS == true && m.LC_ISAUTH == true).ToList(), "LC_NAME", "LC_NAME", 0).ToList();
                        PaymentCountry.Insert(0, (new SelectListItem { Text = "--Select Payment Country--", Value = "0" }));
                        ViewBag.EIF_BDA_PAYMENT_COUNTRY = PaymentCountry;
                        if (GDNO != "")
                        {
                            List<SelectListItem> BDAGD = new SelectList(context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO == GDNO).ToList(), "EIGDGI_GD_NO", "EIGDGI_GD_NO", 0).ToList();
                            BDAGD.Insert(0, (new SelectListItem { Text = "--Select GD Number--", Value = "0" }));
                            ViewBag.EIF_BDA_GD_NO = BDAGD;
                        }
                        else
                        {
                            List<SelectListItem> BDAGD = new List<SelectListItem>();
                            BDAGD.Insert(0, (new SelectListItem { Text = "--No GD Number--", Value = "0" }));
                            ViewBag.EIF_BDA_GD_NO = BDAGD;
                        }
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult EIFBDAList(string E)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFBDA", Session["USER_ID"].ToString()))
                {
                    List<PSW_EIF_BDA_LIST> DataList = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_EIF_REQUEST_NO == E).OrderByDescending(m => m.EIF_BDA_ID).ToList();
                    return PartialView(DataList);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult GetGDAgainstBDA(string REQ)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFBDA", Session["USER_ID"].ToString()))
                {
                    List<PSW_EIF_GD_GENERAL_INFO_LIST> Entity = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO == REQ).ToList();
                    return PartialView(Entity);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult GetBDAViewMode(int BDA_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFBDA", Session["USER_ID"].ToString()))
                {
                    int CurrID = 0;
                    EIF entity = new EIF();
                    entity.BDA = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_ID == BDA_ID).FirstOrDefault();
                    if (entity.BDA.EIF_BDA_CURRENCY_FCY != null && entity.BDA.EIF_BDA_CURRENCY_FCY != "")
                        CurrID = Convert.ToInt32(entity.BDA.EIF_BDA_CURRENCY_FCY);
                    entity.Currency = context.PSW_CURRENCY.Where(m => m.CUR_ID == CurrID).FirstOrDefault();
                    return PartialView(entity);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public FileStreamResult EIFBDABulkPortfolio(HttpPostedFileBase postedFile)
        {
            string message = "";
            try
            {
                string filePath = string.Empty;
                if (postedFile != null)
                {
                    string path = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filePath = path + Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName);
                    if (extension == ".xlsx" || extension == ".xls")
                    {
                        postedFile.SaveAs(filePath);
                        var Scanner = new AntiVirus.Scanner();
                        var result = Scanner.ScanAndClean(filePath);
                        if (result != AntiVirus.ScanResult.VirusNotFound)
                        {
                            message += "malicious file detected Unable to Upload File To the Server.";
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                            return CreateLogFile(message);
                        }
                    }
                    else
                    {
                        message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                        return CreateLogFile(message);
                    }
                    DataTable dt = new DataTable();
                    try
                    {
                        dt = BulkOptions.ReadAsDataTable(filePath);
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                        return CreateLogFile(message);
                    }
                    if (dt.Rows.Count > 0)
                    {
                        int RowCount = 0;
                        foreach (DataRow row in dt.Rows)
                        {
                            PSW_EIF_BDA eifbda = new PSW_EIF_BDA();
                            if (row["EIf Request No"].ToString().Length > 2)
                            {
                                string EIFRequestNo = row["EIf Request No"].ToString();

                                bool EIFRequestNoExist = false;
                                PSW_EIF_BASIC_INFO checkEif = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIFRequestNo).FirstOrDefault();
                                if (checkEif != null)
                                {
                                    eifbda = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_EIF_REQUEST_NO == EIFRequestNo).FirstOrDefault();

                                    if (eifbda != null)
                                    {
                                        eifbda = new PSW_EIF_BDA();
                                        eifbda.EIF_BDA_ID = Convert.ToInt32(context.PSW_EIF_BDA.Max(m => (decimal?)m.EIF_BDA_ID)) + 1;
                                        eifbda.EIF_BDA_ENTRY_DATETIME = DateTime.Now;
                                        eifbda.EIF_BDA_AMENDMENT_NO = 0;
                                        EIFRequestNoExist = false;
                                    }
                                    //else
                                    //{
                                    //    PSW_EIF_BDA checkbda = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_EIF_REQUEST_NO == EIFRequestNo).FirstOrDefault();
                                    //    eifbda = new PSW_EIF_BDA();
                                    //    eifbda.EIF_BDA_ID = Convert.ToInt32(context.PSW_EIF_BDA.Max(m => (decimal?)m.EIF_BDA_ID)) + 1;
                                    //    eifbda.EIF_BDA_AMENDMENT_NO = Convert.ToInt32(context.PSW_EIF_BDA.Where(m => m.EIF_BDA_NO == checkbda.EIF_BDA_NO).Max(m => m.EIF_BDA_AMENDMENT_NO)) + 1;
                                    //    eifbda.EIF_BDA_ENTRY_DATETIME = DateTime.Now;
                                    //    EIFRequestNoExist = false;
                                    //}
                                    if (row["EIf Request No"] != null && row["EIf Request No"].ToString() != "")
                                    {
                                        string EIF_REQUEST_NO = row["EIf Request No"].ToString();
                                        eifbda.EIF_BDA_EIF_REQUEST_NO = EIF_REQUEST_NO;
                                        if (row["BDA Document Ref No"] != null && row["BDA Document Ref No"].ToString() != "")
                                        {
                                            string BDA_Document_RefNo = row["BDA Document Ref No"].ToString();
                                            eifbda.EIF_BDA_DOCUMENT_REQUEST_NO = BDA_Document_RefNo;
                                            if (row["Total BDA Amount (EIF FCY)"] != null && row["Total BDA Amount (EIF FCY)"].ToString() != "")
                                            {
                                                string TotalBDAAmount = row["Total BDA Amount (EIF FCY)"].ToString();
                                                eifbda.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY = Convert.ToDecimal(TotalBDAAmount);
                                                if (row["Total BDA (Currency)"] != null && row["Total BDA (Currency)"].ToString() != "")
                                                {
                                                    string TotalBDACurrency = row["Total BDA (Currency)"].ToString();
                                                    PSW_CURRENCY totalbdacurrency = new PSW_CURRENCY();
                                                    totalbdacurrency = context.PSW_CURRENCY.Where(m => m.CUR_NAME == TotalBDACurrency).FirstOrDefault();
                                                    if (totalbdacurrency != null)
                                                    {
                                                        eifbda.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY = totalbdacurrency.CUR_ID.ToString();
                                                        if (row["Sample Amount"] != null && row["Sample Amount"].ToString() != "")
                                                        {
                                                            string BDASampleAmount = row["Sample Amount"].ToString();
                                                            eifbda.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE = Convert.ToDecimal(BDASampleAmount);
                                                            if (row["Sample Amount (Currency)"] != null && row["Sample Amount (Currency)"].ToString() != "")
                                                            {
                                                                string BDASampleAmountCurrency = row["Sample Amount (Currency)"].ToString();
                                                                PSW_CURRENCY bdasampleamount = new PSW_CURRENCY();
                                                                bdasampleamount = context.PSW_CURRENCY.Where(m => m.CUR_NAME == BDASampleAmountCurrency).FirstOrDefault();
                                                                if (bdasampleamount != null)
                                                                {
                                                                    eifbda.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY = bdasampleamount.CUR_ID.ToString();
                                                                    if (row["Net BDA Amount (EIF FCY)"] != null && row["Net BDA Amount (EIF FCY)"].ToString() != "")
                                                                    {
                                                                        string NetBDAAmount = row["Net BDA Amount (EIF FCY)"].ToString();
                                                                        eifbda.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY = Convert.ToDecimal(NetBDAAmount);
                                                                        if (row["Net BDA Amount (Currency)"] != null && row["Net BDA Amount (Currency)"].ToString() != "")
                                                                        {
                                                                            string NetBDAAmountCurrency = row["Net BDA Amount (Currency)"].ToString();
                                                                            PSW_CURRENCY netbdaamount = new PSW_CURRENCY();
                                                                            netbdaamount = context.PSW_CURRENCY.Where(m => m.CUR_NAME == NetBDAAmountCurrency).FirstOrDefault();
                                                                            if (netbdaamount != null)
                                                                            {
                                                                                eifbda.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY = netbdaamount.CUR_ID.ToString();
                                                                                if (row["Exchange Rate (EIF FCY)"] != null && row["Exchange Rate (EIF FCY)"].ToString() != "")
                                                                                {
                                                                                    string ExchangeRate = row["Exchange Rate (EIF FCY)"].ToString();
                                                                                    eifbda.EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY = Convert.ToDecimal(ExchangeRate);
                                                                                    if (row["Net BDA Amount (EIF PKR)"] != null && row["Net BDA Amount (EIF PKR)"].ToString() != "")
                                                                                    {
                                                                                        string NetBDAAmountPKR = row["Net BDA Amount (EIF PKR)"].ToString();
                                                                                        eifbda.EIF_BDA_NET_BDA_AMOUNT_PKR = Convert.ToDecimal(NetBDAAmountPKR);
                                                                                        if (row["Amount In Words"] != null && row["Amount In Words"].ToString() != "")
                                                                                        {
                                                                                            string AmountInWords = row["Amount In Words"].ToString();
                                                                                            eifbda.EIF_BDA_AMOUNT_IN_WORDS = AmountInWords;
                                                                                            if (row["Currency (FCY)"] != null && row["Currency (FCY)"].ToString() != "")
                                                                                            {
                                                                                                string Currency = row["Currency (FCY)"].ToString();
                                                                                                PSW_CURRENCY CurrencyRoot = new PSW_CURRENCY();
                                                                                                CurrencyRoot = context.PSW_CURRENCY.Where(m => m.CUR_NAME == Currency).FirstOrDefault();
                                                                                                if (CurrencyRoot != null)
                                                                                                {
                                                                                                    eifbda.EIF_BDA_CURRENCY_FCY = CurrencyRoot.CUR_ID.ToString();
                                                                                                    if (row["BDA Amount (FCY)"] != null && row["BDA Amount (FCY)"].ToString() != "")
                                                                                                    {
                                                                                                        string bdaAmountFCY = row["BDA Amount (FCY)"].ToString();
                                                                                                        eifbda.EIF_BDA_AMOUNT_FCY = Convert.ToDecimal(bdaAmountFCY);
                                                                                                        if (row["Exchange Rate (FCY)"] != null && row["Exchange Rate (FCY)"].ToString() != "")
                                                                                                        {
                                                                                                            string ExchangeRateFCY = row["Exchange Rate (FCY)"].ToString();
                                                                                                            eifbda.EIF_BDA_EXCHANGE_RATE_FCY = Convert.ToDecimal(ExchangeRateFCY);
                                                                                                            if (row["BDA Amount (PKR)"] != null && row["BDA Amount (PKR)"].ToString() != "")
                                                                                                            {
                                                                                                                string BDAAmountPKR = row["BDA Amount (PKR)"].ToString();
                                                                                                                eifbda.EIF_BDA_AMOUNT_PKR = Convert.ToDecimal(BDAAmountPKR);
                                                                                                                if (row["BDA Balance Amount FCY"] != null && row["BDA Balance Amount FCY"].ToString() != "")
                                                                                                                {
                                                                                                                    string BDABalanceFCY = row["BDA Balance Amount FCY"].ToString();
                                                                                                                    eifbda.EIF_BDA_Balance_Amount_FCY = Convert.ToDecimal(BDABalanceFCY);
                                                                                                                    if (row["Commission (FCY)"] != null && row["Commission (FCY)"].ToString() != "")
                                                                                                                    {
                                                                                                                        string CommissionFCY = row["Commission (FCY)"].ToString();
                                                                                                                        eifbda.EIF_BDA_COMMMISSION_FCY = Convert.ToDecimal(CommissionFCY);
                                                                                                                        if (row["Commission Amount (PKR)"] != null && row["Commission Amount (PKR)"].ToString() != "")
                                                                                                                        {
                                                                                                                            string CommissionPKR = row["Commission Amount (PKR)"].ToString();
                                                                                                                            eifbda.EIF_BDA_COMMISSION_PKR = Convert.ToDecimal(CommissionPKR);
                                                                                                                            if (row["FED (FCY)"] != null && row["FED (FCY)"].ToString() != "")
                                                                                                                            {
                                                                                                                                string FEDfcy = row["FED (FCY)"].ToString();
                                                                                                                                eifbda.EIF_BDA_FED_AMOUNT_FCY = Convert.ToDecimal(FEDfcy);
                                                                                                                                if (row["FED Amount (PKR)"] != null && row["FED Amount (PKR)"].ToString() != "")
                                                                                                                                {
                                                                                                                                    string FEDfcyPKR = row["FED Amount (PKR)"].ToString();
                                                                                                                                    eifbda.EIF_BDA_FED_AMOUNT_PKR = Convert.ToDecimal(FEDfcyPKR);
                                                                                                                                    if (row["Swift Charges (PKR)"] != null && row["Swift Charges (PKR)"].ToString() != "")
                                                                                                                                    {
                                                                                                                                        string SwiftCharges = row["FED Amount (PKR)"].ToString();
                                                                                                                                        eifbda.EIF_BDA_SWIFT_CHARGES_PKR = Convert.ToDecimal(SwiftCharges);
                                                                                                                                        if (row["Other Charges (PKR)"] != null && row["Other Charges (PKR)"].ToString() != "")
                                                                                                                                        {
                                                                                                                                            string OtherChares = row["Other Charges (PKR)"].ToString();
                                                                                                                                            eifbda.EIF_BDA_OTHER_CHARGES_PKR = Convert.ToDecimal(OtherChares);
                                                                                                                                            if (row["Remarks"] != null && row["Remarks"].ToString() != "")
                                                                                                                                            {
                                                                                                                                                string remark = row["Remarks"].ToString();
                                                                                                                                                eifbda.EIF_BDA_REMARKS = remark;
                                                                                                                                                if (row["GD No"] != null && row["GD No"].ToString() != "")
                                                                                                                                                {
                                                                                                                                                    string GD_NO = row["GD No"].ToString();
                                                                                                                                                    eifbda.EIF_BDA_GD_NO = GD_NO;
                                                                                                                                                    if (!EIFRequestNoExist)
                                                                                                                                                    {
                                                                                                                                                        eifbda.EIF_BDA_NO = GenerateNewBDANo();
                                                                                                                                                        eifbda.EIF_BDA_STATUS = "initiated";
                                                                                                                                                        eifbda.EIF_BDA_MAKER_ID = Session["USER_ID"].ToString();
                                                                                                                                                        eifbda.EIF_BDA_CHECKER_ID = null;
                                                                                                                                                        context.PSW_EIF_BDA.Add(eifbda);
                                                                                                                                                    }
                                                                                                                                                    else
                                                                                                                                                    {
                                                                                                                                                        eifbda.EIF_BDA_STATUS = "initiated";
                                                                                                                                                        eifbda.EIF_BDA_MAKER_ID = Session["USER_ID"].ToString();
                                                                                                                                                        eifbda.EIF_BDA_CHECKER_ID = null;
                                                                                                                                                        context.Entry(eifbda).State = EntityState.Modified;
                                                                                                                                                    }
                                                                                                                                                    RowCount += context.SaveChanges();
                                                                                                                                                }
                                                                                                                                                else
                                                                                                                                                {
                                                                                                                                                    message += "Invalid GD No " + row["GD No"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            else
                                                                                                                                            {
                                                                                                                                                message += "Invalid Remarks " + row["Remarks"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                        else
                                                                                                                                        {
                                                                                                                                            message += "Invalid Other Charges (PKR) " + row["Other Charges (PKR)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    else
                                                                                                                                    {
                                                                                                                                        message += "Invalid Swift Charges (PKR) " + row["Swift Charges (PKR)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                else
                                                                                                                                {
                                                                                                                                    message += "Invalid FED Amount (PKR) " + row["FED Amount (PKR)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                                                }
                                                                                                                            }
                                                                                                                            else
                                                                                                                            {
                                                                                                                                message += "Invalid FED (FCY) " + row["FED (FCY)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                                            }
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            message += "Invalid Commission Amount (PKR) " + row["Commission Amount (PKR)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                                        }
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        message += "Invalid Commission (FCY) " + row["Commission (FCY)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                                    }
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    message += "Invalid BDA Balance Amount FCY " + row["BDA Balance Amount FCY"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                                }
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                message += "Invalid BDA Amount (PKR) " + row["BDA Amount (PKR)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                            }
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            message += "Invalid Exchange Rate (FCY) " + row["Exchange Rate (FCY)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        message += "Invalid BDA Amount (FCY) " + row["BDA Amount (FCY)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    message += "Given Currency (FCY) " + row["Currency (FCY)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                message += "Invalid Currency (FCY) " + row["Currency (FCY)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            message += "Invalid Amount In Words " + row["Amount In Words"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        message += "Invalid Net BDA Amount (EIF PKR) " + row["Net BDA Amount (EIF PKR)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    message += "Invalid Exchange Rate (EIF FCY) " + row["Exchange Rate (EIF FCY)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                message += "Given Net BDA Amount (Currency) " + row["Net BDA Amount (Currency)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "Invalid Net BDA Amount (Currency) " + row["Net BDA Amount (Currency)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid Net BDA Amount (EIF FCY) " + row["Net BDA Amount (EIF FCY)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Given Sample Amount (Currency) " + row["Sample Amount (Currency)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid Sample Amount(Currency) " + row["Sample Amount(Currency)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Sample Amount " + row["Sample Amount"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Given Total BDA (Currency) " + row["Total BDA (Currency)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + " does not exist in the system" + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid Total BDA (Currency) " + row["Total BDA (Currency)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Total BDA Amount (EIF FCY) " + row["Total BDA Amount (EIF FCY)"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid EIF Document Request Number  " + row["BDA Document Ref No"].ToString() + "  At EIF Request No # " + row["EIf Request No"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    else
                                    {
                                        message += "Invalid EIF Request Number ";
                                    }
                                }
                                else
                                {
                                    message += "EIF Request Not Found In The System With the number # " + EIFRequestNo + Environment.NewLine;
                                }
                            }
                            RowCount += context.SaveChanges();
                        }
                        if (RowCount > 0)
                        {
                            message += "Data Uploaded Successfully." + Environment.NewLine + RowCount + " Rows Affected";
                        }
                    }
                    else
                    {
                        message = "No Data Found In The File." + Environment.NewLine;
                    }
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("EIFRequest", "EIFBDABulkPortfolio", Session["USER_ID"].ToString(), ex);
                message += "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
            }
            return CreateLogFile(message);
        }
        public FileStreamResult CreateLogFile(string Message)
        {
            var byteArray = Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
        public ActionResult ExportEIFBDA(int AM = 0, string REQ_NO = "")
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    var EIFBDA = context.PSW_EIF_BDA_VIEW_FOR_DOWNLOAD.Where(m => m.EIF_BDA_NO == REQ_NO && m.EIF_BDA_AMENDMENT_NO == AM).FirstOrDefault();
                    CrystalDecisions.CrystalReports.Engine.ReportDocument rd = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                    rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EIF_BDA_FOR_DOWNLOAD.rpt"));
                    rd.DataSourceConnections.Clear();
                    rd.SetDataSource(new[] { EIFBDA });

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "PSW-EIF-BDA-REPORT(" + EIFBDA.EIF_BDA_NO + ").pdf");
                }
                else
                {
                    return RedirectToAction("Index", "Authentication");
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured :" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception : " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
                return CreateLogFile(message);
            }
        }
        #endregion
        // PSW GD Information Starts/////
        #region GD not in use
        public ActionResult EIFIGMInfo(int I = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (I != 0)
                        {
                            PSW_EIF_GD_INFORMATION edit = context.PSW_EIF_GD_INFORMATION.Where(m => m.EIGDI_ID == I).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + I;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EIFIGMInfo(PSW_EIF_GD_INFORMATION dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EIF_GD_INFORMATION UpdateEntity = new PSW_EIF_GD_INFORMATION();

                        PSW_EIF_GD_INFORMATION CheckIFExist = new PSW_EIF_GD_INFORMATION();
                        if (dbtable.EIGDI_ID == 0)
                        {
                            UpdateEntity.EIGDI_ID = Convert.ToInt32(context.PSW_EIF_GD_INFORMATION.Max(m => (decimal?)m.EIGDI_ID)) + 1;
                            UpdateEntity.EIGDI_ENTRY_DATETIME = DateTime.Now;
                            CheckIFExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_EIF_GD_INFORMATION.Where(m => m.EIGDI_ID == dbtable.EIGDI_ID).FirstOrDefault();
                            UpdateEntity.EIGDI_EDIT_DATETIME = DateTime.Now;
                            CheckIFExist = UpdateEntity;
                        }
                        //UpdateEntity.EIGDI_EIF_REQUEST_NO = GenerateNewEIFReqNo();
                        UpdateEntity.EIGDI_EGM_COLLECTORATE = dbtable.EIGDI_EGM_COLLECTORATE;
                        UpdateEntity.EIGDI_EGM_NUMBER = dbtable.EIGDI_EGM_NUMBER;
                        UpdateEntity.EIGDI_EGM_EXPORTER_NAME = dbtable.EIGDI_EGM_EXPORTER_NAME;
                        UpdateEntity.EIGDI_VESSEL_NAME = dbtable.EIGDI_VESSEL_NAME;
                        UpdateEntity.EIGDI_GROSS_WEIGHT = dbtable.EIGDI_GROSS_WEIGHT;
                        UpdateEntity.EIGDI_COSIGNMENT_TYPE = dbtable.EIGDI_COSIGNMENT_TYPE;
                        UpdateEntity.EIGDI_SECTION = dbtable.EIGDI_SECTION;
                        UpdateEntity.EIGDI_INDEX_NO = dbtable.EIGDI_INDEX_NO;
                        UpdateEntity.EIGDI_BL_NO = dbtable.EIGDI_BL_NO;
                        UpdateEntity.EIGDI_BL_DATE = dbtable.EIGDI_BL_DATE;
                        UpdateEntity.EIGDI_PORT_OF_SHIPMENT = dbtable.EIGDI_PORT_OF_SHIPMENT;
                        UpdateEntity.EIGDI_NET_WEIGHT = dbtable.EIGDI_NET_WEIGHT;
                        UpdateEntity.EIGDI_SHIPPING_LINE = dbtable.EIGDI_SHIPPING_LINE;
                        UpdateEntity.EIGDI_BERTHING_TERMINAL = dbtable.EIGDI_BERTHING_TERMINAL;
                        UpdateEntity.EIGDI_MAKER_ID = "1";
                        UpdateEntity.EIGDI_CHECKER_ID = "1";
                        if (CheckIFExist == null)
                            context.PSW_EIF_GD_INFORMATION.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult EIFIGMInfoList()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    return PartialView(context.PSW_EIF_GD_INFORMATION.ToList());
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public ActionResult EIFGDImportExportInfo(int G = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (G != 0)
                        {
                            PSW_EIF_GD_IMPORT_EXPORT_INFO edit = context.PSW_EIF_GD_IMPORT_EXPORT_INFO.Where(m => m.EIGDIE_ID == G).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + G;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EIFGDImportExportInfo(PSW_EIF_GD_IMPORT_EXPORT_INFO dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EIF_GD_IMPORT_EXPORT_INFO UpdateEntity = new PSW_EIF_GD_IMPORT_EXPORT_INFO();

                        PSW_EIF_GD_IMPORT_EXPORT_INFO CheckIFExist = new PSW_EIF_GD_IMPORT_EXPORT_INFO();
                        if (dbtable.EIGDIE_ID == 0)
                        {
                            UpdateEntity.EIGDIE_ID = Convert.ToInt32(context.PSW_EIF_GD_IMPORT_EXPORT_INFO.Max(m => (decimal?)m.EIGDIE_ID)) + 1;
                            UpdateEntity.EIGDIE_ENTRY_DATETIME = DateTime.Now;
                            CheckIFExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_EIF_GD_IMPORT_EXPORT_INFO.Where(m => m.EIGDIE_ID == dbtable.EIGDIE_ID).FirstOrDefault();
                            UpdateEntity.EIGDIE_EDIT_DATETIME = DateTime.Now;
                            CheckIFExist = UpdateEntity;
                        }
                        //UpdateEntity.EIGDIE_EIF_REQUEST_NO = GenerateNewEIFReqNo();
                        UpdateEntity.EIGDIE_NTN_FTN = dbtable.EIGDIE_NTN_FTN;
                        UpdateEntity.EIGDIE_STRN = dbtable.EIGDIE_STRN;
                        UpdateEntity.EIGDIE_CONSIGNEE_NAME = dbtable.EIGDIE_CONSIGNEE_NAME;
                        UpdateEntity.EIGDIE_CONSIGNEE_ADDRESS = dbtable.EIGDIE_CONSIGNEE_ADDRESS;
                        UpdateEntity.EIGDIE_CONSIGNOR_NAME = dbtable.EIGDIE_CONSIGNOR_NAME;
                        UpdateEntity.EIGDIE_CONSIGNOR_ADDRESS = dbtable.EIGDIE_CONSIGNOR_ADDRESS;
                        UpdateEntity.EIGDIE_MAKER_ID = "1";
                        UpdateEntity.EIGDIE_CHECKER_ID = "1";
                        if (CheckIFExist == null)
                            context.PSW_EIF_GD_IMPORT_EXPORT_INFO.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult EIFGDImportExportInfoList()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    return PartialView(context.PSW_EIF_GD_IMPORT_EXPORT_INFO.ToList());
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public ActionResult EIFGDGeneralInfo(int G = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (G != 0)
                        {
                            PSW_EIF_GD_GENERAL_INFO edit = context.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_ID == G).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + G;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EIFGDGeneralInfo(PSW_EIF_GD_GENERAL_INFO dbtable)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EIF_GD_GENERAL_INFO UpdateEntity = new PSW_EIF_GD_GENERAL_INFO();

                        PSW_EIF_GD_GENERAL_INFO CheckIFExist = new PSW_EIF_GD_GENERAL_INFO();
                        if (dbtable.EIGDGI_ID == 0)
                        {
                            UpdateEntity.EIGDGI_ID = Convert.ToInt32(context.PSW_EIF_GD_GENERAL_INFO.Max(m => (decimal?)m.EIGDGI_ID)) + 1;
                            UpdateEntity.EIGDGI_ENTRY_DATETIME = DateTime.Now;
                            CheckIFExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_ID == dbtable.EIGDGI_ID).FirstOrDefault();
                            UpdateEntity.EIGDGI_EDIT_DATETIME = DateTime.Now;
                            CheckIFExist = UpdateEntity;
                        }
                        //UpdateEntity.EIGDGI_EIF_REQUEST_NO = GenerateNewEIFReqNo();
                        UpdateEntity.EIGDGI_AGENT_NAME = dbtable.EIGDGI_AGENT_NAME;
                        UpdateEntity.EIGDGI_TRADE_TYPE = dbtable.EIGDGI_TRADE_TYPE;
                        UpdateEntity.EIGDGI_GD_NO = GenerateNewGDNO();
                        UpdateEntity.EIGDGI_COLLECTORATE = dbtable.EIGDGI_COLLECTORATE;
                        UpdateEntity.EIGDGI_DESTINATION_COUNTRY = dbtable.EIGDGI_DESTINATION_COUNTRY;
                        UpdateEntity.EIGDGI_PLACE_OF_DELIVERY = dbtable.EIGDGI_PLACE_OF_DELIVERY;
                        UpdateEntity.EIGDGI_GENERAL_DESCRIPTION = dbtable.EIGDGI_GENERAL_DESCRIPTION;
                        UpdateEntity.EIGDGI_SHED_TERMINAL_LOCATION = dbtable.EIGDGI_SHED_TERMINAL_LOCATION;
                        UpdateEntity.EIGDGI_AGENT_LICENSE_NO = dbtable.EIGDGI_AGENT_LICENSE_NO;
                        UpdateEntity.EIGDGI_DECLARATION_TYPE = dbtable.EIGDGI_DECLARATION_TYPE;
                        UpdateEntity.EIGDGI_CONSIGNMENT_CATE = dbtable.EIGDGI_CONSIGNMENT_CATE;
                        UpdateEntity.EIGDGI_GD_TYPE = dbtable.EIGDGI_GD_TYPE;
                        UpdateEntity.EIGDGI_PORT_OF_DISCHARGE = dbtable.EIGDGI_PORT_OF_DISCHARGE;
                        UpdateEntity.EIGDGI_1ST_EXAMINATION = dbtable.EIGDGI_1ST_EXAMINATION;
                        UpdateEntity.EIGDGI_MAKER_ID = "1";
                        UpdateEntity.EIGDGI_CHECKER_ID = "1";
                        if (CheckIFExist == null)
                            context.PSW_EIF_GD_GENERAL_INFO.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult EIFGDGeneralInfoList()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    return PartialView(context.PSW_EIF_GD_GENERAL_INFO.ToList());
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        #endregion

        #region EIF GD Main View
        public ActionResult GDs()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFGD", Session["USER_ID"].ToString()))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult EIFGD(EIFGDFilter edit, int ID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFGD", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_EIF_GD_GENERAL_INFO_LIST> List = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).ToList();
                            if (List.Count() == 0)
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (ID != 0)
                        {
                            edit.Entity = context.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_ID == ID).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + ID;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EIFRequest", "EIFGD", Session["USER_ID"].ToString(), ex);
                        //message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EIFGD(EIFGDFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFGD", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string PreviousStatus = "";
                    try
                    {
                        if (db_table.Entity.EIGDGI_GD_STATUS != null)
                        {
                            PSW_EIF_GD_GENERAL_INFO UpdateEntity = context.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_ID == db_table.Entity.EIGDGI_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                PSW_EIF_GD_GENERAL_INFO EntityToUpdate = new PSW_EIF_GD_GENERAL_INFO();
                                PreviousStatus = UpdateEntity.EIGDGI_GD_STATUS;
                                EntityToUpdate = UpdateEntity;
                                if (UpdateEntity != null && PreviousStatus != db_table.Entity.EIGDGI_GD_STATUS && db_table.Entity.EIGDGI_GD_STATUS != null)
                                {
                                    if (DAL.DO_EDI == "true")
                                    {
                                        message += DAL.MethodId1541(EntityToUpdate.EIGDGI_GD_NO,db_table.Entity.EIGDGI_GD_STATUS, true) + Environment.NewLine;
                                        //message += "  RAW REQUEST DETAILS  -------------------- " + Environment.NewLine;
                                        //message += DAL.message;
                                        if (message.Contains("200"))
                                        {
                                            EntityToUpdate = UpdateEntity;
                                            EntityToUpdate.EIGDGI_GD_STATUS = db_table.Entity.EIGDGI_GD_STATUS;
                                            EntityToUpdate.EIGDGI_EDIT_DATETIME = DateTime.Now;
                                            context.Entry(EntityToUpdate).State = EntityState.Modified;
                                            int RowCount = context.SaveChanges();
                                            if(RowCount > 0)
                                                message += "GD status updated successfully";
                                            else
                                                message += "Exception occur while updating GD status";
                                        }
                                    }
                                    else
                                        message += "EDI is turned off from system configuration";
                                }
                            }
                            else
                            {
                                message = "Exception Occur while fetching record";
                            }
                        }
                        else
                        {
                            message = "Select GD status to continue";
                            return View(db_table);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("EIFRequest", "EIFGD", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult EIFGDFilter(EIFGDFilter edit, int ID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFGD", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_EIF_GD_GENERAL_INFO_LIST> List = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).ToList();
                            if (List.Count() == 0)
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("EIFGD", edit);
        }
        [HttpPost]
        public ActionResult EIFGDFilter(EIFGDFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFGD", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_EIF_GD_GENERAL_INFO_LIST> List = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).ToList();
                            if (List.Count() == 0)
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("EIFGD", edit);
        }
        public PartialViewResult GetAllGDsList(EIFGDFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFGD", Session["USER_ID"].ToString()))
                {
                    List<PSW_EIF_GD_GENERAL_INFO_LIST> Entity = new List<PSW_EIF_GD_GENERAL_INFO_LIST>();
                    if (Filter.FromDate != null && Filter.ToDate != null)
                        Entity = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).ToList();
                    else
                        Entity = context.PSW_EIF_GD_GENERAL_INFO_LIST.Take(10).ToList();
                    return PartialView(Entity);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult GetGDView(int GD_ID)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "EIFGD", Session["USER_ID"].ToString()))
                {
                    PSW_EIF_GD_GENERAL_INFO MasterEntity = context.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_ID == GD_ID).FirstOrDefault();
                    GD_Import entity = new GD_Import();
                    entity.GeneralInfo = context.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_GD_NO == MasterEntity.EIGDGI_GD_NO && m.EIGDGI_AMENDMENT_NO == MasterEntity.EIGDGI_AMENDMENT_NO).FirstOrDefault();
                    entity.GD_Information = context.PSW_EIF_GD_INFORMATION.Where(m => m.EIGDI_GD_NO == entity.GeneralInfo.EIGDGI_GD_NO && m.EIGDI_AMENDMENT_NO == entity.GeneralInfo.EIGDGI_AMENDMENT_NO).FirstOrDefault();
                    entity.IE_Info = context.PSW_EIF_GD_IMPORT_EXPORT_INFO.Where(m => m.EIGDIE_GD_NO == entity.GeneralInfo.EIGDGI_GD_NO && m.EIGDIE_AMENDMENT_NO == entity.GeneralInfo.EIGDGI_AMENDMENT_NO).FirstOrDefault();
                    entity.Package_Info = context.PSW_GD_IMPORT_PACKAGE_INFORMATION.Where(m => m.IPI_GD_NO == entity.GeneralInfo.EIGDGI_GD_NO && m.IPI_AMENDMENT_NO == entity.GeneralInfo.EIGDGI_AMENDMENT_NO).ToList();
                    entity.Container_Info = context.PSW_GD_IMPORT_CONTAINER_INFORMATION.Where(m => m.CII_GD_NO == entity.GeneralInfo.EIGDGI_GD_NO && m.CII_AMENDMENT_NO == entity.GeneralInfo.EIGDGI_AMENDMENT_NO).ToList();
                    entity.Hs_Code = context.PSW_GD_IMORT_HS_CODE.Where(m => m.IHC_GD_NO == entity.GeneralInfo.EIGDGI_GD_NO && m.IHC_AMENDMENT_NO == entity.GeneralInfo.EIGDGI_AMENDMENT_NO).ToList();
                    return PartialView(entity);
                }
                else
                {
                    return null; /*RedirectToAction("UnAuthorizedUrl", "Error")*/;
                }
            }
            else
            {
                return null /*RedirectToAction("Index", "Authentication")*/;
            }
        }

        public ActionResult ExportEIFGD(int AM = 0, string GD_NO = "")
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    var EIFGDMASTER = context.PSW_EIF_GD_FOR_DOWNLOAD.Where(m => m.EIGDGI_GD_NO == GD_NO && m.EIGDGI_AMENDMENT_NO == AM).FirstOrDefault();
                    var EIFGDHSCode = context.PSW_GD_IMPORT_HS_CODE_VIEW.Where(m => m.IHC_GD_NO == GD_NO && m.IHC_AMENDMENT_NO == AM).ToList();
                    ReportDocument rd = new ReportDocument();
                    if (EIFGDMASTER != null)
                    {
                        DateTime InnDate = Convert.ToDateTime("1900-01-01");
                        if (EIFGDMASTER.EIGDGI_CLEARANCE_DATE != null && EIFGDMASTER.EIGDGI_CLEARANCE_DATE != InnDate)
                            rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EIF_GD_FOR_DOWNLOAD.rpt"));
                        else
                            rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EIF_GD_FOR_DOWNLOAD_WITHOUT_CLEAR_DATE.rpt"));
                    }
                    else
                        rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EIF_GD_FOR_DOWNLOAD.rpt"));
                    rd.DataSourceConnections.Clear();
                    rd.SetDataSource(new[] { EIFGDMASTER });
                    rd.Subreports[0].DataSourceConnections.Clear();
                    rd.Subreports[0].SetDataSource(EIFGDHSCode);

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "PSW-IMPORT-GD(" + GD_NO + ").pdf");
                }
                else
                {
                    return RedirectToAction("Index", "Authentication");
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured :" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception : " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
                return CreateLogFile(message);
            }
        }
        #endregion

        public PartialViewResult LoadUnpinnedGds()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("EIFRequest", "Index", Session["USER_ID"].ToString()))
                {
                    List<PSW_EIF_GD_GENERAL_INFO_LIST> Entity = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO == "" || m.EIGDGI_EIF_REQUEST_NO == null).ToList();
                    return PartialView(Entity);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
    }
}
﻿using PSW.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace PSW.Controllers
{
    public class GroupRoleController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: GroupRole
        public ActionResult Index(AssignFunctions_Custom entity, int R = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("GroupRole", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (R != 0)
                        {
                            entity = new AssignFunctions_Custom();
                            entity.Entity = context.PSW_ROLES.Where(m => m.R_ID == R).FirstOrDefault();
                            if (entity.Entity != null)
                            {
                                List<int?> PreviousFunctionIds = context.PSW_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == R && m.AF_STATUS == true).Select(m => m.AF_FUNCTION_ID).ToList();
                                List<SelectListItem> items = new SelectList(context.PSW_FUNCTIONS.OrderBy(m => m.F_NAME).ToList(), "F_ID", "F_NAME", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    int? CurrentFId = Convert.ToInt32(item.Value);
                                    if (PreviousFunctionIds.Contains(CurrentFId))
                                        item.Selected = true;
                                }
                                entity.FunctionsList = items;
                                return View(entity);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + R;
                                return View();
                            }
                        }
                        else
                        {
                            entity = new AssignFunctions_Custom();
                            entity.Entity = new PSW_ROLES();
                            entity.Entity.R_ID = 0;
                            List<SelectListItem> items = new SelectList(context.PSW_FUNCTIONS.OrderBy(m => m.F_NAME).ToList(), "F_ID", "F_NAME", 0).ToList();
                            entity.FunctionsList = items;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(AssignFunctions_Custom db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("GroupRole", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ROLES MasterEntityToUpdate = new PSW_ROLES();
                        MasterEntityToUpdate = context.PSW_ROLES.Where(m => m.R_NAME.ToLower() == db_table.Entity.R_NAME.ToLower() && m.R_ID != db_table.Entity.R_ID).FirstOrDefault();
                        if (MasterEntityToUpdate == null)
                        {
                            MasterEntityToUpdate = new PSW_ROLES();
                            if (db_table.Entity.R_ID != 0)
                            {
                                MasterEntityToUpdate = context.PSW_ROLES.Where(m => m.R_ID == db_table.Entity.R_ID).FirstOrDefault();
                                MasterEntityToUpdate.R_DATA_EDIT_DATETIME = DateTime.Now;
                                context.Entry(MasterEntityToUpdate).State = EntityState.Modified;
                            }
                            else
                            {
                                MasterEntityToUpdate.R_ID = Convert.ToInt32(context.PSW_ROLES.Max(m => (decimal?)m.R_ID)) + 1;
                                MasterEntityToUpdate.R_DATA_ENTRY_DATETIME = DateTime.Now;
                                context.PSW_ROLES.Add(MasterEntityToUpdate);
                            }
                            MasterEntityToUpdate.R_NAME = db_table.Entity.R_NAME;
                            MasterEntityToUpdate.R_DESCRIPTION = db_table.Entity.R_DESCRIPTION;
                            MasterEntityToUpdate.R_MAKER_ID = Session["USER_ID"].ToString();
                            if (MasterEntityToUpdate.R_MAKER_ID == "Admin123")
                            {
                                MasterEntityToUpdate.R_CHECKER_ID = MasterEntityToUpdate.R_MAKER_ID;
                                MasterEntityToUpdate.R_ISAUTH = true;
                            }
                            else
                            {
                                MasterEntityToUpdate.R_CHECKER_ID = null;
                                MasterEntityToUpdate.R_ISAUTH = false;
                            }
                            MasterEntityToUpdate.R_STATUS = db_table.Entity.R_STATUS;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                #region Get Old Function
                                List<PSW_ASSIGN_FUNCTIONS> OldFunctions = context.PSW_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == MasterEntityToUpdate.R_ID && m.AF_STATUS == true).ToList();

                                string TempNewFunction = "";
                                string TempRemoveFunction = "";
                                foreach (int functionid in db_table.SelectedFunctions)
                                {
                                    PSW_ASSIGN_FUNCTIONS Function = context.PSW_ASSIGN_FUNCTIONS.Where(m => m.AF_FUNCTION_ID == functionid && m.AF_ROLE_ID == MasterEntityToUpdate.R_ID && m.AF_STATUS == true).FirstOrDefault();
                                    if (Function == null)
                                    {
                                        PSW_FUNCTIONS FunctionTbl = context.PSW_FUNCTIONS.Where(m => m.F_ID == functionid).FirstOrDefault();
                                        TempNewFunction += FunctionTbl.F_NAME + ",";
                                    }
                                }

                                #endregion

                                List<PSW_ASSIGN_FUNCTIONS> DeActivatedFunctions = context.PSW_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == db_table.Entity.R_ID && !db_table.SelectedFunctions.Contains(m.AF_FUNCTION_ID)).ToList();
                                if (DeActivatedFunctions.Count > 0)
                                {
                                    foreach (PSW_ASSIGN_FUNCTIONS EachFunctionForUpdate in DeActivatedFunctions)
                                    {
                                        PSW_FUNCTIONS FunctionTbl = context.PSW_FUNCTIONS.Where(m => m.F_ID == EachFunctionForUpdate.AF_FUNCTION_ID).FirstOrDefault();
                                        TempRemoveFunction += FunctionTbl.F_NAME + ",";
                                        EachFunctionForUpdate.AF_STATUS = false;
                                        context.Entry(EachFunctionForUpdate).State = EntityState.Modified;
                                    }
                                }
                                int AssignFunId = 0;
                                foreach (int? SelectedFunctionId in db_table.SelectedFunctions)
                                {
                                    if (SelectedFunctionId != null)
                                    {
                                        PSW_ASSIGN_FUNCTIONS EachFunctionToUpdate = context.PSW_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == db_table.Entity.R_ID && m.AF_FUNCTION_ID == SelectedFunctionId).FirstOrDefault();
                                        if (EachFunctionToUpdate == null)
                                        {
                                            EachFunctionToUpdate = new PSW_ASSIGN_FUNCTIONS();
                                            if (AssignFunId == 0)
                                            {
                                                EachFunctionToUpdate.AF_ID = Convert.ToInt32(context.PSW_ASSIGN_FUNCTIONS.Max(m => (decimal?)m.AF_ID)) + 1;
                                                AssignFunId = EachFunctionToUpdate.AF_ID;
                                            }
                                            else
                                            {
                                                AssignFunId = (AssignFunId + 1);
                                                EachFunctionToUpdate.AF_ID = AssignFunId;
                                            }
                                            EachFunctionToUpdate.AF_FUNCTION_ID = SelectedFunctionId;
                                            EachFunctionToUpdate.AF_ROLE_ID = MasterEntityToUpdate.R_ID;
                                            EachFunctionToUpdate.AF_MAKER_ID = Session["USER_ID"].ToString();
                                            EachFunctionToUpdate.AF_STATUS = true;
                                            EachFunctionToUpdate.AF_DATA_ENTRY_DATETIME = DateTime.Now;
                                            context.PSW_ASSIGN_FUNCTIONS.Add(EachFunctionToUpdate);
                                        }
                                        else
                                        {
                                            EachFunctionToUpdate.AF_MAKER_ID = Session["USER_ID"].ToString();
                                            EachFunctionToUpdate.AF_STATUS = true;
                                            EachFunctionToUpdate.AF_DATA_ENTRY_DATETIME = DateTime.Now;
                                            context.Entry(EachFunctionToUpdate).State = EntityState.Modified;
                                        }
                                    }
                                }
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    if (Session["USER_ID"].ToString() != "Admin123")
                                    {
                                        #region Group Role Log
                                        RowCount = 0;
                                        PSW_ROLE_WITH_FUNCTIONS_LIST_VIEW AssignFunction = context.PSW_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.R_ID == MasterEntityToUpdate.R_ID).FirstOrDefault();
                                        PSW_GROUP_LOG GroupLog = new PSW_GROUP_LOG();
                                        GroupLog.GLOG_ID = Convert.ToInt32(context.PSW_GROUP_LOG.Max(m => (decimal?)m.GLOG_ID)) + 1;
                                        GroupLog.GLOG_ROLE_NAME = MasterEntityToUpdate.R_NAME;
                                        GroupLog.GLOG_DESCRIPTION = MasterEntityToUpdate.R_DESCRIPTION;
                                        GroupLog.GLOG_ASSIGNED_ROLES = AssignFunction.ASSIGNED_FUNCTION;
                                        GroupLog.GLOG_ADDED_BY = Session["USER_ID"].ToString();
                                        GroupLog.GLOG_ADDED_DATETIME = DateTime.Now;
                                        if (GroupLog.GLOG_ADDED_BY == "Admin123")
                                        {
                                            GroupLog.GLOG_ACTIVATED_BY = GroupLog.GLOG_ADDED_BY;
                                            GroupLog.GLOG_ACTIVATED_DATETIME = DateTime.Now;
                                        }
                                        else
                                        {
                                            GroupLog.GLOG_ACTIVATED_BY = null;
                                            GroupLog.GLOG_ACTIVATED_DATETIME = null;
                                        }
                                        if (TempNewFunction != "" && TempRemoveFunction != "")
                                            GroupLog.GLOG_CURRENT_STATUS = "New function " + TempNewFunction + " assigned & function name " + TempRemoveFunction + " is removed from the system : " + MasterEntityToUpdate.R_NAME;
                                        else if (TempNewFunction != "" && TempRemoveFunction == "")
                                            GroupLog.GLOG_CURRENT_STATUS = "New functions assigned to the Role : " + MasterEntityToUpdate.R_NAME + ", New Assigned functions " + TempNewFunction;
                                        else if (TempNewFunction == "" && TempRemoveFunction != "")
                                            GroupLog.GLOG_CURRENT_STATUS = "Following Functions Removed From Role " + MasterEntityToUpdate.R_NAME + ", Removed Functions " + TempRemoveFunction;
                                        else if (GroupLog.GLOG_ACTIVATED_BY == "Admin123")
                                            GroupLog.GLOG_CURRENT_STATUS = "New functions assigned to the Role : " + MasterEntityToUpdate.R_NAME + ", New Assigned functions " + TempNewFunction + ". Role " + MasterEntityToUpdate.R_NAME + " is activated";
                                        GroupLog.GLOG_ENTRY_DATETIME = DateTime.Now;
                                        context.PSW_GROUP_LOG.Add(GroupLog);
                                        RowCount = context.SaveChanges();
                                        #endregion
                                    }
                                    if (RowCount > 0)
                                    {
                                        message = "Data Inserted Successfully " + RowCount + " Affected";
                                    }
                                }
                            }
                            else
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                        else
                        {
                            message = "A Role with the given name " + db_table.Entity.R_NAME + " Already Exist";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {

                        ViewBag.message = message;
                    }
                    AssignFunctions_Custom entity = new AssignFunctions_Custom();
                    entity.Entity = new PSW_ROLES();
                    entity.Entity.R_ID = db_table.Entity.R_ID;
                    List<int?> PreviousFunctionIds = context.PSW_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == db_table.Entity.R_ID).Select(m => m.AF_FUNCTION_ID).ToList();
                    List<SelectListItem> items = new SelectList(context.PSW_FUNCTIONS.OrderBy(m => m.F_NAME).ToList(), "F_ID", "F_NAME", 0).ToList();
                    foreach (SelectListItem item in items)
                    {
                        int? CurrentFId = Convert.ToInt32(item.Value);
                        if (PreviousFunctionIds.Contains(CurrentFId))
                            item.Selected = true;
                    }
                    entity.FunctionsList = items;
                    return View(entity);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [ChildActionOnly]
        public PartialViewResult Grid_View(AssignFunctions_Custom Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ROLE_WITH_FUNCTIONS_LIST_VIEW> ViewData = new List<PSW_ROLE_WITH_FUNCTIONS_LIST_VIEW>();
                ViewData = context.PSW_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.R_MAKER_ID != SessionUser || m.R_ISAUTH == true).OrderByDescending(m => m.R_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeGroupRole(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("GroupRole", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ROLES UpdateEntity = context.PSW_ROLES.Where(m => m.R_ID == R_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.R_ISAUTH = true;
                            UpdateEntity.R_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.R_DATA_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                if (Session["USER_ID"].ToString() != "Admin123")
                                {
                                    RowCount = 0;
                                    #region Group Role Log
                                    PSW_ROLES MasterEntityToUpdate = context.PSW_ROLES.Where(m => m.R_ID == R_ID).FirstOrDefault();
                                    PSW_ROLE_WITH_FUNCTIONS_LIST_VIEW AssignFunction = context.PSW_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.R_ID == MasterEntityToUpdate.R_ID).FirstOrDefault();
                                    PSW_GROUP_LOG GroupLog = new PSW_GROUP_LOG();
                                    GroupLog.GLOG_ID = Convert.ToInt32(context.PSW_GROUP_LOG.Max(m => (decimal?)m.GLOG_ID)) + 1;
                                    GroupLog.GLOG_ROLE_NAME = MasterEntityToUpdate.R_NAME;
                                    GroupLog.GLOG_DESCRIPTION = MasterEntityToUpdate.R_DESCRIPTION;
                                    GroupLog.GLOG_ASSIGNED_ROLES = AssignFunction.ASSIGNED_FUNCTION;
                                    GroupLog.GLOG_ADDED_BY = UpdateEntity.R_MAKER_ID;
                                    GroupLog.GLOG_ADDED_DATETIME = UpdateEntity.R_DATA_ENTRY_DATETIME;
                                    GroupLog.GLOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                    GroupLog.GLOG_ACTIVATED_DATETIME = DateTime.Now;
                                    GroupLog.GLOG_ENTRY_DATETIME = DateTime.Now;
                                    GroupLog.GLOG_CURRENT_STATUS = " Activated the Role : " + MasterEntityToUpdate.R_NAME;
                                    context.PSW_GROUP_LOG.Add(GroupLog);
                                    RowCount = context.SaveChanges();
                                    #endregion
                                }
                                if (RowCount > 0)
                                {
                                    message = "Group Role : " + UpdateEntity.R_NAME + " is successfully Authorized";
                                }
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeGroupRole(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("GroupRole", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ROLES UpdateEntity = context.PSW_ROLES.Where(m => m.R_ID == R_ID).FirstOrDefault();
                        #region Save Temp Function Name Before Deleting
                        string GetAllFunction = "";
                        PSW_ROLE_WITH_FUNCTIONS_LIST_VIEW AllFunction = context.PSW_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.R_ID == R_ID).FirstOrDefault();
                        if (AllFunction != null)
                        {
                            GetAllFunction += AllFunction.ASSIGNED_FUNCTION;
                        }
                        #endregion
                        if (UpdateEntity != null)
                        {
                            context.PSW_ROLES.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                List<PSW_ASSIGN_FUNCTIONS> GetAllAssignFun = context.PSW_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == UpdateEntity.R_ID).ToList();
                                if (GetAllAssignFun.Count > 0)
                                {
                                    RowCount = 0;
                                    foreach (PSW_ASSIGN_FUNCTIONS FuntionToRemove in GetAllAssignFun)
                                    {
                                        context.PSW_ASSIGN_FUNCTIONS.Remove(FuntionToRemove);
                                        RowCount += context.SaveChanges();
                                    }
                                }
                                if (RowCount > 0)
                                {
                                    if (Session["USER_ID"].ToString() != "Admin123")
                                    {
                                        RowCount = 0;
                                        PSW_GROUP_LOG GroupLog = new PSW_GROUP_LOG();
                                        GroupLog.GLOG_ID = Convert.ToInt32(context.PSW_GROUP_LOG.Max(m => (decimal?)m.GLOG_ID)) + 1;
                                        GroupLog.GLOG_ROLE_NAME = UpdateEntity.R_NAME;
                                        GroupLog.GLOG_DESCRIPTION = UpdateEntity.R_DESCRIPTION;
                                        GroupLog.GLOG_ASSIGNED_ROLES = GetAllFunction;
                                        GroupLog.GLOG_ADDED_BY = UpdateEntity.R_MAKER_ID;
                                        GroupLog.GLOG_ADDED_DATETIME = UpdateEntity.R_DATA_ENTRY_DATETIME;
                                        GroupLog.GLOG_CURRENT_STATUS = "Following Role Rejected/Deleted : " + UpdateEntity.R_NAME;
                                        GroupLog.GLOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                        GroupLog.GLOG_ACTIVATED_DATETIME = DateTime.Now;
                                        GroupLog.GLOG_ENTRY_DATETIME = DateTime.Now;
                                        context.PSW_GROUP_LOG.Add(GroupLog);
                                        RowCount = context.SaveChanges();
                                    }
                                    if (RowCount > 0)
                                        message = "Group Role : " + UpdateEntity.R_NAME + " is Rejected";
                                }
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        #region Group Report
        public ActionResult GroupActivity()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("GroupRole", "GroupActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult GroupActivity(DateTime? FromDate, DateTime? EndDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("GroupRole", "GroupActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && EndDate != null)
                        {
                            var Data = context.GroupActivityReport(FromDate, EndDate).OrderByDescending(m => m.GLOG_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.GLOG_ACTIVATED_BY != "Admin123" && m.GLOG_ADDED_BY != "Admin123").ToList();
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + FromDate + " End date : " + EndDate;
                            }
                        }
                        else
                        {
                            if (FromDate == null && EndDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (EndDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        #endregion
    }
}
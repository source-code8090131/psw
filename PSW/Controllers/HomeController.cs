﻿using PSW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Controllers
{
    public class HomeController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: Home
        public ActionResult Index(PSW_FUNCTIONS db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Home", "Index", Session["USER_ID"].ToString()))
                {
                    string UserID = Session["USER_ID"].ToString();
                    PSW_LOGIN_MASTER MasterDetail = context.PSW_LOGIN_MASTER.Where(m => m.LOG_USER_ID == UserID).FirstOrDefault();
                    int?[] RoleIds = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == UserID && m.LR_STATUS == true && m.LR_AMENDMENT_NO == MasterDetail.LOG_AMENDMENT_NO).Select(m => m.LR_ROLE_ID).ToArray();
                    int?[] FunctionIds = context.PSW_ASSIGN_FUNCTIONS.Where(m => RoleIds.Contains(m.AF_ROLE_ID) && m.AF_STATUS == true).Select(m => m.AF_FUNCTION_ID).Distinct().ToArray();
                    List<PSW_FUNCTIONS> menu = context.PSW_FUNCTIONS.Where(m => FunctionIds.Contains(m.F_ID) && m.F_ACTION != "#" && m.F_ACTION != "#").Distinct().ToList();
                    return View(menu);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [ChildActionOnly]
        public PartialViewResult Menu()
        {
            if (Session["USER_ID"] != null)
            {
                string UserID = Session["USER_ID"].ToString();
                PSW_LOGIN_MASTER MasterDetail = context.PSW_LOGIN_MASTER.Where(m => m.LOG_USER_ID == UserID).FirstOrDefault();
                int?[] RoleIds = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == UserID && m.LR_STATUS == true && m.LR_AMENDMENT_NO == MasterDetail.LOG_AMENDMENT_NO).Select(m => m.LR_ROLE_ID).ToArray();
                int?[] FunctionIds = context.PSW_ASSIGN_FUNCTIONS.Where(m => RoleIds.Contains(m.AF_ROLE_ID) && m.AF_STATUS == true).Select(m => m.AF_FUNCTION_ID).Distinct().ToArray();
                List<PSW_FUNCTIONS> menu = context.PSW_FUNCTIONS.Where(m => FunctionIds.Contains(m.F_ID)).Distinct().ToList();
                return PartialView(menu);
            }
            else
            {
                return PartialView();
            }
        }

        [HttpPost]
        public JsonResult GetSessionTimeout()
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                int MinutesRemainingToTimeout = HttpContext.Session.Timeout;
                DateTime ExpiringTime = DateTime.Now.AddMinutes(MinutesRemainingToTimeout);
                double MinutesLeft = (ExpiringTime - DateTime.Now).TotalMinutes;

                var coverletters = new
                {
                    ReminingTime = MinutesLeft
                };
                return Json(coverletters, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Invalid Request");
            }
        }
    }
}
﻿using PSW.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PSW.Controllers
{
    public class ITRSController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: ITRS
        #region AC 700
        public ActionResult ItrsAC()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsAC", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public FileStreamResult BulkAC700(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsAC", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFileAC700(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFileAC700(message);
                            }

                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTableITRS(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFileAC700(message);
                            }

                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Select())
                                {
                                    PSW_ITRS_AC_700 Entity = new PSW_ITRS_AC_700();
                                    if (row["Account"].ToString().Length > 0)
                                    {
                                        if (row["Refference"] != null || row["Refference"].ToString() != "" && row["Date"] != null || row["Date"].ToString() != "")
                                        {
                                            string ReferenceNo = row["Refference"].ToString();
                                            ReferenceNo = ReferenceNo.Replace("OUR REF.  : ", "");

                                            int Date = Convert.ToInt32(row["Date"]);
                                            DateTime TransDate = DateTime.FromOADate(Date);
                                            decimal TempTransAmount = 0;
                                            string Amount = row["Amount"].ToString();
                                            bool ITRSTransactionExist = false;
                                            if (Amount != null && Amount != "")
                                            {
                                                if (Amount.Contains("-"))
                                                {
                                                    Decimal TransAmount = Convert.ToDecimal(Amount.Replace("-", ""));
                                                    Entity = context.PSW_ITRS_AC_700.Where(m => m.AC_REFERENCE_NO == ReferenceNo && m.AC_DEBIT_AMOUNT == TransAmount && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) == System.Data.Entity.DbFunctions.TruncateTime(TransDate)).FirstOrDefault();
                                                    if(Entity == null)
                                                    {
                                                        TempTransAmount = GetSubtractedTransactionAmount(TransAmount);
                                                        Entity = context.PSW_ITRS_AC_700.Where(m => m.AC_REFERENCE_NO == ReferenceNo && m.AC_DEBIT_AMOUNT == TempTransAmount && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) == System.Data.Entity.DbFunctions.TruncateTime(TransDate)).FirstOrDefault();
                                                    }
                                                }
                                                else
                                                {
                                                    Decimal TransAmount = Convert.ToDecimal(Amount);
                                                    Entity = context.PSW_ITRS_AC_700.Where(m => m.AC_REFERENCE_NO == ReferenceNo && m.AC_CREDIT_AMOUNT == TransAmount && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) == System.Data.Entity.DbFunctions.TruncateTime(TransDate)).FirstOrDefault();
                                                    if (Entity == null)
                                                    {
                                                        TempTransAmount = GetSubtractedTransactionAmount(TransAmount);
                                                        Entity = context.PSW_ITRS_AC_700.Where(m => m.AC_REFERENCE_NO == ReferenceNo && m.AC_CREDIT_AMOUNT == TempTransAmount && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) == System.Data.Entity.DbFunctions.TruncateTime(TransDate)).FirstOrDefault();
                                                    }
                                                }
                                            }
                                            if (Entity == null)
                                            {
                                                Entity = new PSW_ITRS_AC_700();
                                                Entity.AC_ID = Convert.ToInt32(context.PSW_ITRS_AC_700.Max(m => (decimal?)m.AC_ID)) + 1;
                                                Entity.AC_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Entity.AC_MAKER_ID == "Admin123")
                                                {
                                                    Entity.AC_CHECKER_ID = Entity.AC_MAKER_ID;
                                                    Entity.AC_IS_AUTH = true;
                                                }
                                                else
                                                {
                                                    Entity.AC_CHECKER_ID = null;
                                                    Entity.AC_IS_AUTH = false;
                                                }
                                                Entity.AC_ENTRY_DATETIME = DateTime.Now;
                                            }
                                            else
                                            {
                                                Entity.AC_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Entity.AC_MAKER_ID == "Admin123")
                                                {
                                                    Entity.AC_CHECKER_ID = Entity.AC_MAKER_ID;
                                                    Entity.AC_IS_AUTH = true;
                                                }
                                                else
                                                {
                                                    Entity.AC_CHECKER_ID = null;
                                                    Entity.AC_IS_AUTH = false;
                                                }
                                                Entity.AC_EDIT_DATETIME = DateTime.Now;
                                                ITRSTransactionExist = true;
                                            }
                                            if (row["Account"] != null || row["Account"].ToString() != "")
                                            {
                                                string AccountNo = row["Account"].ToString();
                                                if (row["Date"] != null || row["Date"].ToString() != "")
                                                {
                                                    if (row["Details"] != null || row["Details"].ToString() != "")
                                                    {
                                                        string TransDetail = row["Details"].ToString();
                                                        if (row["ValueDate"] != null || row["ValueDate"].ToString() != "")
                                                        {
                                                            int valuedate = Convert.ToInt32(row["ValueDate"]);
                                                            DateTime ValueDate = DateTime.FromOADate(valuedate);
                                                            if (row["Currency"] != null || row["Currency"].ToString() != "")
                                                            {
                                                                string Currency = row["Currency"].ToString();
                                                                if (row["Amount"] != null || row["Amount"].ToString() != "")
                                                                {
                                                                    Amount = row["Amount"].ToString();
                                                                    if (row["Description"] != null || row["Description"].ToString() != "")
                                                                    {
                                                                        string TransDescription = row["Description"].ToString();
                                                                        String RefNo = "";
                                                                        String ExchangeRateCCY = "";
                                                                        if (TransDescription.Contains("Exchange Rate/Ccy: "))
                                                                        {
                                                                            int ExchangeRateCCYFrom = TransDescription.IndexOf("Exchange Rate/Ccy: ") + "Exchange Rate/Ccy: ".Length;
                                                                            int ExchangeRateCCYTo = ExchangeRateCCYFrom + 8;
                                                                            ExchangeRateCCY = TransDescription.Substring(ExchangeRateCCYFrom, ExchangeRateCCYTo - ExchangeRateCCYFrom);
                                                                        }
                                                                        if (ReferenceNo.Contains("PK1BKOT") || ReferenceNo.Contains("PK1BKOZ") || ReferenceNo.Contains("PK1CDXB") || ReferenceNo.Contains("PK1INM1") || ReferenceNo.Contains("PK1PKSI") || ReferenceNo.Contains("PK1CWOT") || ReferenceNo.Contains("PK1CWOX") || ReferenceNo.Contains("PK1FTMN") || ReferenceNo.Contains("PK1PKSI") || ReferenceNo.Contains("PK1INKT") || ReferenceNo.Contains("PK1MMBO"))
                                                                        {
                                                                            Entity.AC_ACCOUNT_NO = AccountNo;
                                                                            Entity.AC_TRANSACTION_DATE = TransDate;
                                                                            Entity.AC_DESCRIPTION = TransDetail;
                                                                            Entity.AC_CURRENCY = Currency;
                                                                            Entity.AC_TRANS_DETAIL = TransDescription;
                                                                            if (TransDescription.Contains("Exchange Rate/Ccy: ") && ExchangeRateCCY != "" && ExchangeRateCCY != null)
                                                                                Entity.AC_EXCHANGE_RATE = Convert.ToDecimal(ExchangeRateCCY);
                                                                            if (Amount != null && Amount != "")
                                                                            {
                                                                                if (Amount.Contains("-"))
                                                                                    Entity.AC_DEBIT_AMOUNT = Convert.ToDecimal(Amount.Replace("-", ""));
                                                                                else
                                                                                    Entity.AC_CREDIT_AMOUNT = Convert.ToDecimal(Amount);
                                                                            }
                                                                            Entity.AC_REFERENCE_NO = ReferenceNo;
                                                                            if (ITRSTransactionExist == false)
                                                                            {
                                                                                context.PSW_ITRS_AC_700.Add(Entity);
                                                                            }
                                                                            else
                                                                            {
                                                                                context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                            }
                                                                        }
                                                                        else if (ReferenceNo.Contains("PK1FWCM") || ReferenceNo.Contains("PK1FWIB") || ReferenceNo.Contains("PK1SPCM") || ReferenceNo.Contains("PK1SPIB"))
                                                                        {
                                                                            //Get Exchange Rate
                                                                            int ExchangeRateFrom = TransDescription.IndexOf("Exchange Rate ") + "Exchange Rate ".Length;
                                                                            int ExchangeRateTo = TransDescription.LastIndexOf("Sold Currency");
                                                                            String ExchangeRate = TransDescription.Substring(ExchangeRateFrom, ExchangeRateTo - ExchangeRateFrom);
                                                                            if (ExchangeRate.Contains("_x000D_"))
                                                                                ExchangeRate = ExchangeRate.Replace("_x000D_", "");
                                                                            else if (ExchangeRate.Contains("x000D"))
                                                                                ExchangeRate = ExchangeRate.Replace("x000D", "");
                                                                            //Get Bought Currency
                                                                            int BoughtCurrencyFrom = TransDescription.IndexOf("Bought Currency ") + "Bought Currency ".Length;
                                                                            int BoughtCurrencyTo = TransDescription.LastIndexOf("Bought Amount");
                                                                            String BoughtCurrency = TransDescription.Substring(BoughtCurrencyFrom, BoughtCurrencyTo - BoughtCurrencyFrom);
                                                                            if (BoughtCurrency.Contains("_x000D_"))
                                                                                BoughtCurrency = BoughtCurrency.Replace("_x000D_", "");
                                                                            else if (BoughtCurrency.Contains("x000D"))
                                                                                BoughtCurrency = BoughtCurrency.Replace("x000D", "");
                                                                            //Get Sold Currency
                                                                            int SoldCurrencyFrom = TransDescription.IndexOf("Sold Currency ") + "Sold Currency ".Length;
                                                                            int SoldCurrencyTo = TransDescription.LastIndexOf("Sold Amount");
                                                                            String SoldCurrency = TransDescription.Substring(SoldCurrencyFrom, SoldCurrencyTo - SoldCurrencyFrom);
                                                                            if (SoldCurrency.Contains("_x000D_"))
                                                                                SoldCurrency = SoldCurrency.Replace("_x000D_", "");
                                                                            else if (SoldCurrency.Contains("x000D"))
                                                                                SoldCurrency = SoldCurrency.Replace("x000D", "");
                                                                            //Get CounterParty of Contract
                                                                            int CounterPartyFrom = TransDescription.IndexOf("Counterparty Of Contract ") + "Counterparty Of Contract ".Length;
                                                                            int CounterPartyTo = TransDescription.LastIndexOf("Broker Name");
                                                                            String CounterParty = TransDescription.Substring(CounterPartyFrom, CounterPartyTo - CounterPartyFrom);
                                                                            if (CounterParty.Contains("_x000D_"))
                                                                                CounterParty = CounterParty.Replace("_x000D_", "");
                                                                            else if (CounterParty.Contains("x000D"))
                                                                                CounterParty = CounterParty.Replace("x000D", "");
                                                                            //
                                                                            Entity.AC_ACCOUNT_NO = AccountNo;
                                                                            Entity.AC_TRANSACTION_DATE = TransDate;
                                                                            Entity.AC_DESCRIPTION = TransDetail;
                                                                            Entity.AC_CURRENCY = Currency;
                                                                            Entity.AC_TRANS_DETAIL = TransDescription;
                                                                            if (TransDescription.Contains("Exchange Rate/Ccy: ") && ExchangeRateCCY != "" && ExchangeRateCCY != null)
                                                                                Entity.AC_EXCHANGE_RATE = Convert.ToDecimal(ExchangeRateCCY);
                                                                            if (Amount != null && Amount != "")
                                                                            {
                                                                                if (Amount.Contains("-"))
                                                                                    Entity.AC_DEBIT_AMOUNT = Convert.ToDecimal(Amount.Replace("-", ""));
                                                                                else
                                                                                    Entity.AC_CREDIT_AMOUNT = Convert.ToDecimal(Amount);
                                                                            }
                                                                            Entity.AC_REFERENCE_NO = ReferenceNo;
                                                                            Entity.AC_EXCHANGE_RATE = Convert.ToDecimal(ExchangeRate);
                                                                            if (BoughtCurrency != null && BoughtCurrency != "")
                                                                                Entity.AC_BOUGHT_CURRENCY = BoughtCurrency.Replace("\r", "");
                                                                            else
                                                                                Entity.AC_BOUGHT_CURRENCY = BoughtCurrency;
                                                                            if (SoldCurrency != null && SoldCurrency != "")
                                                                                Entity.AC_SOLD_CURRENCY = SoldCurrency.Replace("\r", "");
                                                                            else
                                                                                Entity.AC_SOLD_CURRENCY = SoldCurrency;
                                                                            if (CounterParty != null && CounterParty != "")
                                                                                Entity.AC_CP_OF_CONTRACT = CounterParty.Replace("\r", "");
                                                                            else
                                                                                Entity.AC_CP_OF_CONTRACT = CounterParty;
                                                                            if (ITRSTransactionExist == false)
                                                                            {
                                                                                context.PSW_ITRS_AC_700.Add(Entity);
                                                                            }
                                                                            else
                                                                            {
                                                                                context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                            }
                                                                        }
                                                                        else if (ReferenceNo.Contains("PK1MANL") || ReferenceNo.Contains("PK1SWIF") || ReferenceNo.Contains("PK1CGWY"))
                                                                        {
                                                                            Entity.AC_ACCOUNT_NO = AccountNo;
                                                                            Entity.AC_TRANSACTION_DATE = TransDate;
                                                                            Entity.AC_DESCRIPTION = TransDetail;
                                                                            Entity.AC_CURRENCY = Currency;
                                                                            Entity.AC_TRANS_DETAIL = TransDescription;
                                                                            if (TransDescription.Contains("Exchange Rate/Ccy: ") && ExchangeRateCCY != "" && ExchangeRateCCY != null)
                                                                                Entity.AC_EXCHANGE_RATE = Convert.ToDecimal(ExchangeRateCCY);
                                                                            if (Amount != null && Amount != "")
                                                                            {
                                                                                if (Amount.Contains("-"))
                                                                                    Entity.AC_DEBIT_AMOUNT = Convert.ToDecimal(Amount.Replace("-", ""));
                                                                                else
                                                                                    Entity.AC_CREDIT_AMOUNT = Convert.ToDecimal(Amount);
                                                                            }
                                                                            Entity.AC_REFERENCE_NO = ReferenceNo;
                                                                            if (ITRSTransactionExist == false)
                                                                            {
                                                                                context.PSW_ITRS_AC_700.Add(Entity);
                                                                            }
                                                                            else
                                                                            {
                                                                                context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                            }
                                                                        }
                                                                        else if (ReferenceNo.Contains("PK1ZSEC"))
                                                                        {
                                                                            //Get Bene Name
                                                                            int BeneNameFrom = TransDescription.IndexOf("/CO/") + "/CO/".Length;
                                                                            int BeneNameTo = TransDescription.LastIndexOf("/SH");
                                                                            String BeneName = TransDescription.Substring(BeneNameFrom, BeneNameTo - BeneNameFrom);
                                                                            //
                                                                            Entity.AC_ACCOUNT_NO = AccountNo;
                                                                            Entity.AC_TRANSACTION_DATE = TransDate;
                                                                            Entity.AC_DESCRIPTION = TransDetail;
                                                                            Entity.AC_CURRENCY = Currency;
                                                                            Entity.AC_TRANS_DETAIL = TransDescription;
                                                                            if (TransDescription.Contains("Exchange Rate/Ccy: ") && ExchangeRateCCY != "" && ExchangeRateCCY != null)
                                                                                Entity.AC_EXCHANGE_RATE = Convert.ToDecimal(ExchangeRateCCY);
                                                                            if (Amount != null && Amount != "")
                                                                            {
                                                                                if (Amount.Contains("-"))
                                                                                    Entity.AC_DEBIT_AMOUNT = Convert.ToDecimal(Amount.Replace("-", ""));
                                                                                else
                                                                                    Entity.AC_CREDIT_AMOUNT = Convert.ToDecimal(Amount);
                                                                            }
                                                                            Entity.AC_REFERENCE_NO = ReferenceNo;
                                                                            if (BeneName != null && BeneName != "")
                                                                                Entity.AC_BENE_NAME = Regex.Replace(BeneName, @"\s+", " ");
                                                                            else
                                                                                Entity.AC_BENE_NAME = BeneName;
                                                                            if (ITRSTransactionExist == false)
                                                                            {
                                                                                context.PSW_ITRS_AC_700.Add(Entity);
                                                                            }
                                                                            else
                                                                            {
                                                                                context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                            }
                                                                        }
                                                                        else if (ReferenceNo.Contains("PK1ZNCS"))
                                                                        {
                                                                            //Get Bene Name
                                                                            int BeneNameFrom = TransDescription.IndexOf("/CO/") + "/CO/".Length;
                                                                            int BeneNameTo = TransDescription.LastIndexOf("/CC");
                                                                            String BeneName = TransDescription.Substring(BeneNameFrom, BeneNameTo - BeneNameFrom);
                                                                            //
                                                                            Entity.AC_ACCOUNT_NO = AccountNo;
                                                                            Entity.AC_TRANSACTION_DATE = TransDate;
                                                                            Entity.AC_DESCRIPTION = TransDetail;
                                                                            Entity.AC_CURRENCY = Currency;
                                                                            Entity.AC_TRANS_DETAIL = TransDescription;
                                                                            if (TransDescription.Contains("Exchange Rate/Ccy: ") && ExchangeRateCCY != "" && ExchangeRateCCY != null)
                                                                                Entity.AC_EXCHANGE_RATE = Convert.ToDecimal(ExchangeRateCCY);
                                                                            if (Amount != null && Amount != "")
                                                                            {
                                                                                if (Amount.Contains("-"))
                                                                                    Entity.AC_DEBIT_AMOUNT = Convert.ToDecimal(Amount.Replace("-", ""));
                                                                                else
                                                                                    Entity.AC_CREDIT_AMOUNT = Convert.ToDecimal(Amount);
                                                                            }
                                                                            Entity.AC_REFERENCE_NO = ReferenceNo;
                                                                            if (BeneName != null && BeneName != "")
                                                                                Entity.AC_BENE_NAME = Regex.Replace(BeneName, @"\s+", " ");
                                                                            else
                                                                                Entity.AC_BENE_NAME = BeneName;
                                                                            if (ITRSTransactionExist == false)
                                                                            {
                                                                                context.PSW_ITRS_AC_700.Add(Entity);
                                                                            }
                                                                            else
                                                                            {
                                                                                context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                            }
                                                                        }
                                                                        else if (ReferenceNo.Contains("PK1FWCM") || ReferenceNo.Contains("PK1SPCM"))
                                                                        {
                                                                            //Get Exchange Rate
                                                                            int ExchangeRateFrom = TransDescription.IndexOf("Exchange Rate ") + "Exchange Rate ".Length;
                                                                            int ExchangeRateTo = TransDescription.LastIndexOf("Sold Currency");
                                                                            String ExchangeRate = TransDescription.Substring(ExchangeRateFrom, ExchangeRateTo - ExchangeRateFrom);
                                                                            if (ExchangeRate.Contains("_x000D_"))
                                                                                ExchangeRate = ExchangeRate.Replace("_x000D_", "");
                                                                            else if (ExchangeRate.Contains("x000D"))
                                                                                ExchangeRate = ExchangeRate.Replace("x000D", "");
                                                                            //Get Bought Currency
                                                                            int BoughtCurrencyFrom = TransDescription.IndexOf("Bought Currency ") + "Bought Currency ".Length;
                                                                            int BoughtCurrencyTo = TransDescription.LastIndexOf("Bought Amount");
                                                                            String BoughtCurrency = TransDescription.Substring(BoughtCurrencyFrom, BoughtCurrencyTo - BoughtCurrencyFrom);
                                                                            if (BoughtCurrency.Contains("_x000D_"))
                                                                                BoughtCurrency = BoughtCurrency.Replace("_x000D_", "");
                                                                            else if (BoughtCurrency.Contains("x000D"))
                                                                                BoughtCurrency = BoughtCurrency.Replace("x000D", "");
                                                                            //Get Sold Currency
                                                                            int SoldCurrencyFrom = TransDescription.IndexOf("Sold Currency ") + "Sold Currency ".Length;
                                                                            int SoldCurrencyTo = TransDescription.LastIndexOf("Sold Amount");
                                                                            String SoldCurrency = TransDescription.Substring(SoldCurrencyFrom, SoldCurrencyTo - SoldCurrencyFrom);
                                                                            if (SoldCurrency.Contains("_x000D_"))
                                                                                SoldCurrency = SoldCurrency.Replace("_x000D_", "");
                                                                            else if (SoldCurrency.Contains("x000D"))
                                                                                SoldCurrency = SoldCurrency.Replace("x000D", "");
                                                                            //Get CounterParty of Contract
                                                                            int CounterPartyFrom = TransDescription.IndexOf("Counterparty Of Contract ") + "Counterparty Of Contract ".Length;
                                                                            int CounterPartyTo = TransDescription.LastIndexOf("Broker Name");
                                                                            String CounterParty = TransDescription.Substring(CounterPartyFrom, CounterPartyTo - CounterPartyFrom);
                                                                            if (CounterParty.Contains("_x000D_"))
                                                                                CounterParty = CounterParty.Replace("_x000D_", "");
                                                                            else if (CounterParty.Contains("x000D"))
                                                                                CounterParty = CounterParty.Replace("x000D", "");
                                                                            //
                                                                            Entity.AC_ACCOUNT_NO = AccountNo;
                                                                            Entity.AC_TRANSACTION_DATE = TransDate;
                                                                            Entity.AC_DESCRIPTION = TransDetail;
                                                                            Entity.AC_CURRENCY = Currency;
                                                                            Entity.AC_TRANS_DETAIL = TransDescription;
                                                                            if (TransDescription.Contains("Exchange Rate/Ccy: ") && ExchangeRateCCY != "" && ExchangeRateCCY != null)
                                                                                Entity.AC_EXCHANGE_RATE = Convert.ToDecimal(ExchangeRateCCY);
                                                                            if (Amount != null && Amount != "")
                                                                            {
                                                                                if (Amount.Contains("-"))
                                                                                    Entity.AC_DEBIT_AMOUNT = Convert.ToDecimal(Amount.Replace("-", ""));
                                                                                else
                                                                                    Entity.AC_CREDIT_AMOUNT = Convert.ToDecimal(Amount);
                                                                            }
                                                                            Entity.AC_REFERENCE_NO = ReferenceNo;
                                                                            Entity.AC_EXCHANGE_RATE = Convert.ToDecimal(ExchangeRate);
                                                                            if (BoughtCurrency != null && BoughtCurrency != "")
                                                                                Entity.AC_BOUGHT_CURRENCY = BoughtCurrency.Replace("\r", "");
                                                                            else
                                                                                Entity.AC_BOUGHT_CURRENCY = BoughtCurrency;
                                                                            if (SoldCurrency != null && SoldCurrency != "")
                                                                                Entity.AC_SOLD_CURRENCY = SoldCurrency.Replace("\r", "");
                                                                            else
                                                                                Entity.AC_SOLD_CURRENCY = SoldCurrency;
                                                                            if (CounterParty != null && CounterParty != "")
                                                                                Entity.AC_CP_OF_CONTRACT = CounterParty.Replace("\r", "");
                                                                            else
                                                                                Entity.AC_CP_OF_CONTRACT = CounterParty;
                                                                            if (ITRSTransactionExist == false)
                                                                            {
                                                                                context.PSW_ITRS_AC_700.Add(Entity);
                                                                            }
                                                                            else
                                                                            {
                                                                                context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                            }
                                                                        }
                                                                        else if (ReferenceNo.Contains("PK1"))
                                                                        {
                                                                            //Get Reference No
                                                                            int RefNoFrom = ReferenceNo.IndexOf("PK1") + "PK1".Length;
                                                                            int RefNoTo = 7;
                                                                            RefNo = ReferenceNo.Substring(RefNoFrom, RefNoTo - RefNoFrom);
                                                                            if (ReferenceNo.Contains("PK1") && Regex.IsMatch(RefNo, @"^\d{4}$"))
                                                                            {
                                                                                Entity.AC_ACCOUNT_NO = AccountNo;
                                                                                Entity.AC_TRANSACTION_DATE = TransDate;
                                                                                Entity.AC_DESCRIPTION = TransDetail;
                                                                                Entity.AC_CURRENCY = Currency;
                                                                                if (Amount != null && Amount != "")
                                                                                {
                                                                                    if (Amount.Contains("-"))
                                                                                        Entity.AC_DEBIT_AMOUNT = Convert.ToDecimal(Amount.Replace("-", ""));
                                                                                    else
                                                                                        Entity.AC_CREDIT_AMOUNT = Convert.ToDecimal(Amount);
                                                                                }
                                                                                Entity.AC_REFERENCE_NO = ReferenceNo;
                                                                                Entity.AC_TRANS_DETAIL = TransDescription;
                                                                                if (TransDescription.Contains("Exchange Rate/Ccy: ") && ExchangeRateCCY != "" && ExchangeRateCCY != null)
                                                                                    Entity.AC_EXCHANGE_RATE = Convert.ToDecimal(ExchangeRateCCY);
                                                                                if (ITRSTransactionExist == false)
                                                                                {
                                                                                    context.PSW_ITRS_AC_700.Add(Entity);
                                                                                }
                                                                                else
                                                                                {
                                                                                    context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid Transaction Detail : " + row["Description"].ToString() + "  At Reference No : " + row["Refference"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid Amount : " + row["Amount"].ToString() + "  At Reference No : " + row["Refference"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid Currency : " + row["Currency"].ToString() + "  At Reference No : " + row["Refference"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Value Date : " + row["ValueDate"].ToString() + "  At Reference No : " + row["Refference"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid Description : " + row["Details"].ToString() + "  At Reference No : " + row["Refference"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid Transaction Date : " + row["Date"].ToString() + "  At Reference No : " + row["Refference"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Account : " + row["Account"].ToString() + "  At Reference No : " + row["Refference"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            if(row["Refference"] == null || row["Refference"].ToString() == "")
                                                message += "Invalid Refference No : " + row["Refference"].ToString() + "  At Account : " + row["Account"].ToString() + Environment.NewLine;
                                            else if(row["Date"] != null || row["Date"].ToString() != "")
                                                message += "Invalid Transaction Date : " + row["Date"].ToString() + "  At Refference No : " + row["Refference"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                    dt.Rows.Remove(row);
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFileAC700(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        public ActionResult ItrsACFilter(ITRS_AC700_Filter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsAC", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_AC_700> List = context.PSW_ITRS_AC_700.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AC_IS_AUTH == true || m.AC_MAKER_ID != SessionUser).OrderByDescending(m => m.AC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ItrsAC", edit);
        }
        [HttpPost]
        public ActionResult ItrsACFilter(ITRS_AC700_Filter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsAC", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_AC_700> List = context.PSW_ITRS_AC_700.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AC_IS_AUTH == true || m.AC_MAKER_ID != SessionUser).OrderByDescending(m => m.AC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ItrsAC", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ItrsACView(ITRS_AC700_Filter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_AC_700> ViewData = new List<PSW_ITRS_AC_700>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_AC_700.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.AC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.AC_IS_AUTH == true || m.AC_MAKER_ID != SessionUser).OrderByDescending(m => m.AC_ID).ToList();
                    }
                }
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeItrsAC(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsAC", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_AC_700 UpdateEntity = context.PSW_ITRS_AC_700.Where(m => m.AC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.AC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.AC_EDIT_DATETIME = DateTime.Now;
                            UpdateEntity.AC_IS_AUTH = true;
                            context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Transaction on Ref No : " + UpdateEntity.AC_REFERENCE_NO + " is authorized sucessfully.";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult AuthorizeBullkItrsAC(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsAC", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    string SessionUser = Session["USER_ID"].ToString();
                    try
                    {
                        List<PSW_ITRS_AC_700> BulkData = new List<PSW_ITRS_AC_700>();
                        if (FromDate != null && ToDate != null)
                        {
                            BulkData = context.PSW_ITRS_AC_700.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).OrderByDescending(m => m.AC_ID).ToList();
                            if (BulkData.Count() > 0)
                            {
                                BulkData = BulkData.Where(m => m.AC_IS_AUTH == true || m.AC_MAKER_ID != SessionUser).OrderByDescending(m => m.AC_ID).ToList();
                                if (BulkData.Count() > 0)
                                {
                                    BulkData = BulkData.Where(m => m.AC_IS_AUTH == false).ToList();
                                    if (BulkData.Count() > 0)
                                    {
                                        foreach (var item in BulkData)
                                        {
                                            PSW_ITRS_AC_700 UpdateEntity = context.PSW_ITRS_AC_700.Where(m => m.AC_ID == item.AC_ID).FirstOrDefault();
                                            if (UpdateEntity != null)
                                            {
                                                UpdateEntity.AC_CHECKER_ID = Session["USER_ID"].ToString();
                                                UpdateEntity.AC_EDIT_DATETIME = DateTime.Now;
                                                UpdateEntity.AC_IS_AUTH = true;
                                                context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                                RowCount += context.SaveChanges();
                                            }
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        message = "Record authorized sucessfully.";
                                    }
                                }
                                else
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                            message = "Select From or To date to continue";
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public decimal GetSubtractedTransactionAmount(decimal Amount)
        {
            decimal TransAmount = 0;
            if (Amount != 0)
            {
                string tempamount = "0.01";
                decimal TempAmount = Convert.ToDecimal(tempamount);
                TransAmount = Amount - TempAmount;
            }
            return TransAmount;
        }
        #endregion

        #region FT-801
        public ActionResult ItrsFT()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsFT", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public FileStreamResult BulkFT801(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsFT", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFileFT801(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFileFT801(message);
                            }

                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFileFT801(message);
                            }

                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    List<PSW_ITRS_AC_700> Check_Entity = new List<PSW_ITRS_AC_700>();
                                    if (row["Contract Ref No"].ToString().Length > 0)
                                    {
                                        string ReferenceNo = row["Contract Ref No"].ToString();
                                        Check_Entity = context.PSW_ITRS_AC_700.Where(m => m.AC_REFERENCE_NO == ReferenceNo).ToList();

                                        if (Check_Entity.Count() > 0)
                                        {
                                            foreach (var Entity in Check_Entity)
                                            {
                                                if (Entity.AC_CREDIT_AMOUNT != null && Entity.AC_CREDIT_AMOUNT != 0 && Entity.AC_CURRENCY != "PKR")
                                                {
                                                    if (Entity.AC_REFERENCE_NO.Contains("BKOZ") || Entity.AC_REFERENCE_NO.Contains("BKOT") || Entity.AC_REFERENCE_NO.Contains("CDXB") || Entity.AC_REFERENCE_NO.Contains("CWOT") || Entity.AC_REFERENCE_NO.Contains("CWOX") || Entity.AC_REFERENCE_NO.Contains("FTMN") || Entity.AC_REFERENCE_NO.Contains("INM1") || Entity.AC_REFERENCE_NO.Contains("INKT") || Entity.AC_REFERENCE_NO.Contains("PKSI"))
                                                    {
                                                        //Dr Customer Name / Payment Details / Ultimate Beneficiary / Account with Institution / Sender To Receiver Information
                                                        if (row["Dr Customer Name"] != null || row["Dr Customer Name"].ToString() != "")
                                                        {
                                                            string CustomerName = row["Dr Customer Name"].ToString();
                                                            if (row["Payment Details"].ToString() != "" && row["Payment Details"] != null)
                                                            {
                                                                string PaymentDetail = row["Payment Details"].ToString();
                                                                if (row["By Order"].ToString() != "" && row["By Order"] != null)
                                                                {
                                                                    string ByOrder = row["By Order"].ToString();
                                                                    if (row["Ultimate Beneficiary"].ToString() != "" && row["Ultimate Beneficiary"] != null)
                                                                    {
                                                                        string UltimateBene = row["Ultimate Beneficiary"].ToString();
                                                                        if (row["Account with Institution"].ToString() != "" && row["Account with Institution"] != null)
                                                                        {
                                                                            string AccountwithInstitution = row["Account with Institution"].ToString();
                                                                            if (row["Sender To Receiver Information"].ToString() != "" && row["Sender To Receiver Information"] != null)
                                                                            {
                                                                                string SenderToReciver = row["Sender To Receiver Information"].ToString();
                                                                                if (row["Contact No."].ToString() != "" && row["Contact No."] != null)
                                                                                {
                                                                                    string ContractNo = row["Contact No."].ToString();
                                                                                    ContractNo = ContractNo.Substring(0, 4);
                                                                                    Entity.AC_PURPOSE_CODE = ContractNo;
                                                                                }
                                                                                else
                                                                                    Entity.AC_PURPOSE_CODE = null;
                                                                                if (row["Debit Account"].ToString() != "" && row["Debit Account"] != null)
                                                                                {
                                                                                    string FT_ACCOUNT = row["Debit Account"].ToString();
                                                                                    Entity.AC_FT_ACCOUNT_NO = FT_ACCOUNT;
                                                                                }
                                                                                else
                                                                                    Entity.AC_FT_ACCOUNT_NO = null;
                                                                                if (row["Ben A/C"].ToString() != "" && row["Ben A/C"] != null)
                                                                                {
                                                                                    string BENE_ACCOUNT_NO = row["Ben A/C"].ToString();
                                                                                    if (BENE_ACCOUNT_NO.Length > 20)
                                                                                    {
                                                                                        BENE_ACCOUNT_NO = BENE_ACCOUNT_NO.Substring(8, 4);
                                                                                        Entity.AC_BENE_ACCOUNT_NO = BENE_ACCOUNT_NO;
                                                                                    }
                                                                                    else
                                                                                        Entity.AC_BENE_ACCOUNT_NO = null;
                                                                                }
                                                                                else
                                                                                    Entity.AC_BENE_ACCOUNT_NO = null;
                                                                                Entity.AC_DR_CUSTOMER_NAME = CustomerName;
                                                                                Entity.AC_PAYMENT_DETAILS = PaymentDetail;
                                                                                Entity.AC_BY_ORDER = ByOrder;
                                                                                Entity.AC_ULTIMATE_BENEFICIARY = UltimateBene;
                                                                                Entity.AC_ACCOUNT_WITH_INSTITUTION = AccountwithInstitution;
                                                                                Entity.AC_SENDER_RECIVER_INFORMATION = SenderToReciver;
                                                                                Entity.AC_MAKER_ID = Session["USER_ID"].ToString();
                                                                                if (Entity.AC_MAKER_ID == "Admin123")
                                                                                {
                                                                                    Entity.AC_CHECKER_ID = Entity.AC_MAKER_ID;
                                                                                    Entity.AC_IS_AUTH = true;
                                                                                }
                                                                                else
                                                                                {
                                                                                    Entity.AC_CHECKER_ID = null;
                                                                                    Entity.AC_IS_AUTH = false;
                                                                                }
                                                                                context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                            }
                                                                            else
                                                                            {
                                                                                message += "Invalid Sender To Receiver Information : " + row["Sender To Receiver Information"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "Invalid Account with Institution : " + row["Account with Institution"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid Ultimate Beneficiary : " + row["Ultimate Beneficiary"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid By Order : " + row["By Order"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid Payment Details : " + row["Payment Details"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Dr Customer Name : " + row["Dr Customer Name"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                }
                                                else if (Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DEBIT_AMOUNT != 0 && Entity.AC_CURRENCY != "PKR")
                                                {
                                                    if (Entity.AC_REFERENCE_NO.Contains("FTMN") || Entity.AC_REFERENCE_NO.Contains("INM1"))
                                                    {
                                                        //Cr Customer Name / Payment Details / Ultimate Beneficiary / Account with Institution / Sender To Receiver Information
                                                        if (row["Cr Customer Name"] != null || row["Cr Customer Name"].ToString() != "")
                                                        {
                                                            string CustomerName = row["Cr Customer Name"].ToString();
                                                            if (row["Payment Details"].ToString() != "" && row["Payment Details"] != null)
                                                            {
                                                                string PaymentDetail = row["Payment Details"].ToString();
                                                                if (row["By Order"].ToString() != "" && row["By Order"] != null)
                                                                {
                                                                    string ByOrder = row["By Order"].ToString();
                                                                    if (row["Ultimate Beneficiary"].ToString() != "" && row["Ultimate Beneficiary"] != null)
                                                                    {
                                                                        string UltimateBene = row["Ultimate Beneficiary"].ToString();
                                                                        if (row["Account with Institution"].ToString() != "" && row["Account with Institution"] != null)
                                                                        {
                                                                            string AccountwithInstitution = row["Account with Institution"].ToString();
                                                                            if (row["Sender To Receiver Information"].ToString() != "" && row["Sender To Receiver Information"] != null)
                                                                            {
                                                                                string SenderToReciver = row["Sender To Receiver Information"].ToString();
                                                                                if (row["Contact No."].ToString() != "" && row["Contact No."] != null)
                                                                                {
                                                                                    string ContractNo = row["Contact No."].ToString();
                                                                                    ContractNo = ContractNo.Substring(0, 4);
                                                                                    Entity.AC_PURPOSE_CODE = ContractNo;
                                                                                }
                                                                                else
                                                                                    Entity.AC_PURPOSE_CODE = null;
                                                                                if (row["Credit Account"].ToString() != "" && row["Credit Account"] != null)
                                                                                {
                                                                                    string FT_ACCOUNT = row["Credit Account"].ToString();
                                                                                    Entity.AC_FT_ACCOUNT_NO = FT_ACCOUNT;
                                                                                }
                                                                                else
                                                                                    Entity.AC_FT_ACCOUNT_NO = null;
                                                                                if (row["Ben A/C"].ToString() != "" && row["Ben A/C"] != null)
                                                                                {
                                                                                    string BENE_ACCOUNT_NO = row["Ben A/C"].ToString();
                                                                                    if (BENE_ACCOUNT_NO.Length > 20)
                                                                                    {
                                                                                        BENE_ACCOUNT_NO = BENE_ACCOUNT_NO.Substring(8, 4);
                                                                                        Entity.AC_BENE_ACCOUNT_NO = BENE_ACCOUNT_NO;
                                                                                    }
                                                                                    else
                                                                                        Entity.AC_BENE_ACCOUNT_NO = null;
                                                                                }
                                                                                else
                                                                                    Entity.AC_BENE_ACCOUNT_NO = null;
                                                                                Entity.AC_DR_CUSTOMER_NAME = CustomerName;
                                                                                Entity.AC_PAYMENT_DETAILS = PaymentDetail;
                                                                                Entity.AC_BY_ORDER = ByOrder;
                                                                                Entity.AC_ULTIMATE_BENEFICIARY = UltimateBene;
                                                                                Entity.AC_ACCOUNT_WITH_INSTITUTION = AccountwithInstitution;
                                                                                Entity.AC_SENDER_RECIVER_INFORMATION = SenderToReciver;
                                                                                Entity.AC_MAKER_ID = Session["USER_ID"].ToString();
                                                                                if (Entity.AC_MAKER_ID == "Admin123")
                                                                                {
                                                                                    Entity.AC_CHECKER_ID = Entity.AC_MAKER_ID;
                                                                                    Entity.AC_IS_AUTH = true;
                                                                                }
                                                                                else
                                                                                {
                                                                                    Entity.AC_CHECKER_ID = null;
                                                                                    Entity.AC_IS_AUTH = false;
                                                                                }
                                                                                context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                            }
                                                                            else
                                                                            {
                                                                                message += "Invalid Sender To Receiver Information : " + row["Sender To Receiver Information"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "Invalid Account with Institution : " + row["Account with Institution"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid Ultimate Beneficiary : " + row["Ultimate Beneficiary"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid By Order : " + row["By Order"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid Payment Details : " + row["Payment Details"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Cr Customer Name : " + row["Cr Customer Name"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                }
                                                else if (Entity.AC_CREDIT_AMOUNT != null && Entity.AC_CREDIT_AMOUNT != 0 && Entity.AC_CURRENCY == "PKR")
                                                {
                                                    if (Entity.AC_REFERENCE_NO.Contains("FTMN") || Entity.AC_REFERENCE_NO.Contains("INM1"))
                                                    {
                                                        //Cr Customer Name / Payment Details / Ultimate Beneficiary / Account with Institution / Sender To Receiver Information
                                                        if (row["Cr Customer Name"] != null || row["Cr Customer Name"].ToString() != "")
                                                        {
                                                            string CustomerName = row["Cr Customer Name"].ToString();
                                                            if (row["Payment Details"].ToString() != "" && row["Payment Details"] != null)
                                                            {
                                                                string PaymentDetail = row["Payment Details"].ToString();
                                                                if (row["By Order"].ToString() != "" && row["By Order"] != null)
                                                                {
                                                                    string ByOrder = row["By Order"].ToString();
                                                                    if (row["Ultimate Beneficiary"].ToString() != "" && row["Ultimate Beneficiary"] != null)
                                                                    {
                                                                        string UltimateBene = row["Ultimate Beneficiary"].ToString();
                                                                        if (row["Account with Institution"].ToString() != "" && row["Account with Institution"] != null)
                                                                        {
                                                                            string AccountwithInstitution = row["Account with Institution"].ToString();
                                                                            if (row["Sender To Receiver Information"].ToString() != "" && row["Sender To Receiver Information"] != null)
                                                                            {
                                                                                string SenderToReciver = row["Sender To Receiver Information"].ToString();
                                                                                if (row["Contact No."].ToString() != "" && row["Contact No."] != null)
                                                                                {
                                                                                    string ContractNo = row["Contact No."].ToString();
                                                                                    ContractNo = ContractNo.Substring(0, 4);
                                                                                    Entity.AC_PURPOSE_CODE = ContractNo;
                                                                                }
                                                                                else
                                                                                    Entity.AC_PURPOSE_CODE = null;
                                                                                if (row["Debit Account"].ToString() != "" && row["Debit Account"] != null)
                                                                                {
                                                                                    string FT_ACCOUNT = row["Debit Account"].ToString();
                                                                                    Entity.AC_FT_ACCOUNT_NO = FT_ACCOUNT;
                                                                                }
                                                                                else
                                                                                    Entity.AC_FT_ACCOUNT_NO = null;
                                                                                if (row["Ben A/C"].ToString() != "" && row["Ben A/C"] != null)
                                                                                {
                                                                                    string BENE_ACCOUNT_NO = row["Ben A/C"].ToString();
                                                                                    if (BENE_ACCOUNT_NO.Length > 20)
                                                                                    {
                                                                                        BENE_ACCOUNT_NO = BENE_ACCOUNT_NO.Substring(8, 4);
                                                                                        Entity.AC_BENE_ACCOUNT_NO = BENE_ACCOUNT_NO;
                                                                                    }
                                                                                    else
                                                                                        Entity.AC_BENE_ACCOUNT_NO = null;
                                                                                }
                                                                                else
                                                                                    Entity.AC_BENE_ACCOUNT_NO = null;
                                                                                Entity.AC_DR_CUSTOMER_NAME = CustomerName;
                                                                                Entity.AC_PAYMENT_DETAILS = PaymentDetail;
                                                                                Entity.AC_BY_ORDER = ByOrder;
                                                                                Entity.AC_ULTIMATE_BENEFICIARY = UltimateBene;
                                                                                Entity.AC_ACCOUNT_WITH_INSTITUTION = AccountwithInstitution;
                                                                                Entity.AC_SENDER_RECIVER_INFORMATION = SenderToReciver;
                                                                                Entity.AC_MAKER_ID = Session["USER_ID"].ToString();
                                                                                if (Entity.AC_MAKER_ID == "Admin123")
                                                                                {
                                                                                    Entity.AC_CHECKER_ID = Entity.AC_MAKER_ID;
                                                                                    Entity.AC_IS_AUTH = true;
                                                                                }
                                                                                else
                                                                                {
                                                                                    Entity.AC_CHECKER_ID = null;
                                                                                    Entity.AC_IS_AUTH = false;
                                                                                }
                                                                                context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                            }
                                                                            else
                                                                            {
                                                                                message += "Invalid Sender To Receiver Information : " + row["Sender To Receiver Information"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "Invalid Account with Institution : " + row["Account with Institution"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid Ultimate Beneficiary : " + row["Ultimate Beneficiary"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid By Order : " + row["By Order"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid Payment Details : " + row["Payment Details"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Cr Customer Name : " + row["Cr Customer Name"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                }
                                                else if (Entity.AC_REFERENCE_NO.Contains("SWIF") && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DEBIT_AMOUNT != 0 && Entity.AC_CURRENCY == "PKR")
                                                {
                                                    //Cr Customer Name / Payment Details / Ultimate Beneficiary / Account with Institution / Sender To Receiver Information
                                                    if (row["Cr Customer Name"] != null || row["Cr Customer Name"].ToString() != "")
                                                    {
                                                        string CustomerName = row["Cr Customer Name"].ToString();
                                                        if (row["Payment Details"].ToString() != "" && row["Payment Details"] != null)
                                                        {
                                                            string PaymentDetail = row["Payment Details"].ToString();
                                                            if (row["By Order"].ToString() != "" && row["By Order"] != null)
                                                            {
                                                                string ByOrder = row["By Order"].ToString();
                                                                if (row["Ultimate Beneficiary"].ToString() != "" && row["Ultimate Beneficiary"] != null)
                                                                {
                                                                    string UltimateBene = row["Ultimate Beneficiary"].ToString();
                                                                    if (row["Account with Institution"].ToString() != "" && row["Account with Institution"] != null)
                                                                    {
                                                                        string AccountwithInstitution = row["Account with Institution"].ToString();
                                                                        if (row["Sender To Receiver Information"].ToString() != "" && row["Sender To Receiver Information"] != null)
                                                                        {
                                                                            string SenderToReciver = row["Sender To Receiver Information"].ToString();
                                                                            if (row["Contact No."].ToString() != "" && row["Contact No."] != null)
                                                                            {
                                                                                string ContractNo = row["Contact No."].ToString();
                                                                                ContractNo = ContractNo.Substring(0, 4);
                                                                                Entity.AC_PURPOSE_CODE = ContractNo;
                                                                            }
                                                                            else
                                                                                Entity.AC_PURPOSE_CODE = null;
                                                                            if (row["Credit Account"].ToString() != "" && row["Credit Account"] != null)
                                                                            {
                                                                                string FT_ACCOUNT = row["Credit Account"].ToString();
                                                                                Entity.AC_FT_ACCOUNT_NO = FT_ACCOUNT;
                                                                            }
                                                                            else
                                                                                Entity.AC_FT_ACCOUNT_NO = null;
                                                                            if (row["Ben A/C"].ToString() != "" && row["Ben A/C"] != null)
                                                                            {
                                                                                string BENE_ACCOUNT_NO = row["Ben A/C"].ToString();
                                                                                if (BENE_ACCOUNT_NO.Length > 20)
                                                                                {
                                                                                    BENE_ACCOUNT_NO = BENE_ACCOUNT_NO.Substring(8, 4);
                                                                                    Entity.AC_BENE_ACCOUNT_NO = BENE_ACCOUNT_NO;
                                                                                }
                                                                                else
                                                                                    Entity.AC_BENE_ACCOUNT_NO = null;
                                                                            }
                                                                            else
                                                                                Entity.AC_BENE_ACCOUNT_NO = null;
                                                                            Entity.AC_DR_CUSTOMER_NAME = CustomerName;
                                                                            Entity.AC_PAYMENT_DETAILS = PaymentDetail;
                                                                            Entity.AC_BY_ORDER = ByOrder;
                                                                            Entity.AC_ULTIMATE_BENEFICIARY = UltimateBene;
                                                                            Entity.AC_ACCOUNT_WITH_INSTITUTION = AccountwithInstitution;
                                                                            Entity.AC_SENDER_RECIVER_INFORMATION = SenderToReciver;
                                                                            Entity.AC_MAKER_ID = Session["USER_ID"].ToString();
                                                                            if (Entity.AC_MAKER_ID == "Admin123")
                                                                            {
                                                                                Entity.AC_CHECKER_ID = Entity.AC_MAKER_ID;
                                                                                Entity.AC_IS_AUTH = true;
                                                                            }
                                                                            else
                                                                            {
                                                                                Entity.AC_CHECKER_ID = null;
                                                                                Entity.AC_IS_AUTH = false;
                                                                            }
                                                                            context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "Invalid Sender To Receiver Information : " + row["Sender To Receiver Information"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid Account with Institution : " + row["Account with Institution"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid Ultimate Beneficiary : " + row["Ultimate Beneficiary"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid By Order : " + row["By Order"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Payment Details : " + row["Payment Details"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid Cr Customer Name : " + row["Cr Customer Name"].ToString() + "  At Reference No : " + row["Contract Ref No"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFileFT801(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        public ActionResult ItrsFTFilter(ITRS_AC700_Filter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsFT", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_AC_700> List = context.PSW_ITRS_AC_700.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AC_IS_AUTH == true || m.AC_MAKER_ID != SessionUser).OrderByDescending(m => m.AC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ItrsFT", edit);
        }
        [HttpPost]
        public ActionResult ItrsFTFilter(ITRS_AC700_Filter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsFT", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_AC_700> List = context.PSW_ITRS_AC_700.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AC_IS_AUTH == true || m.AC_MAKER_ID != SessionUser).OrderByDescending(m => m.AC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ItrsFT", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ItrsFTView(ITRS_AC700_Filter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_AC_700> ViewData = new List<PSW_ITRS_AC_700>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_AC_700.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.AC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.AC_IS_AUTH == true || m.AC_MAKER_ID != SessionUser).OrderByDescending(m => m.AC_ID).ToList();
                        if (ViewData.Count() > 0)
                        {
                            ViewData = ViewData.Where(m => m.AC_REFERENCE_NO.Contains("BKOZ") || m.AC_REFERENCE_NO.Contains("BKOT") || m.AC_REFERENCE_NO.Contains("CDXB") || m.AC_REFERENCE_NO.Contains("CWOT") || m.AC_REFERENCE_NO.Contains("CWOX") || m.AC_REFERENCE_NO.Contains("FTMN") || m.AC_REFERENCE_NO.Contains("INM1") || m.AC_REFERENCE_NO.Contains("INKT") || m.AC_REFERENCE_NO.Contains("PKSI")).ToList();
                            if (ViewData.Count() > 0)
                            {
                                return PartialView(ViewData);
                            }
                        }
                    }
                }
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeItrsFT(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsFT", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_AC_700 UpdateEntity = context.PSW_ITRS_AC_700.Where(m => m.AC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.AC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.AC_EDIT_DATETIME = DateTime.Now;
                            UpdateEntity.AC_IS_AUTH = true;
                            context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Transaction on Ref No : " + UpdateEntity.AC_REFERENCE_NO + " is authorized sucessfully.";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult AuthorizeBulkItrsFT(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRS", "ItrsFT", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    string SessionUser = Session["USER_ID"].ToString();
                    try
                    {
                        List<PSW_ITRS_AC_700> BulkData = new List<PSW_ITRS_AC_700>();
                        if (FromDate != null && ToDate != null)
                        {
                            BulkData = context.PSW_ITRS_AC_700.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).OrderByDescending(m => m.AC_ID).ToList();
                            if (BulkData.Count() > 0)
                            {
                                BulkData = BulkData.Where(m => m.AC_IS_AUTH == true || m.AC_MAKER_ID != SessionUser).OrderByDescending(m => m.AC_ID).ToList();
                                if (BulkData.Count() > 0)
                                {
                                    BulkData = BulkData.Where(m => m.AC_REFERENCE_NO.Contains("BKOZ") || m.AC_REFERENCE_NO.Contains("BKOT") || m.AC_REFERENCE_NO.Contains("CDXB") || m.AC_REFERENCE_NO.Contains("CWOT") || m.AC_REFERENCE_NO.Contains("CWOX") || m.AC_REFERENCE_NO.Contains("FTMN") || m.AC_REFERENCE_NO.Contains("INM1") || m.AC_REFERENCE_NO.Contains("INKT") || m.AC_REFERENCE_NO.Contains("PKSI")).ToList();
                                    if (BulkData.Count() > 0)
                                    {
                                        BulkData = BulkData.Where(m => m.AC_IS_AUTH == false).ToList();
                                        if (BulkData.Count() > 0)
                                        {
                                            foreach (var item in BulkData)
                                            {
                                                PSW_ITRS_AC_700 UpdateEntity = context.PSW_ITRS_AC_700.Where(m => m.AC_ID == item.AC_ID).FirstOrDefault();
                                                if (UpdateEntity != null)
                                                {
                                                    UpdateEntity.AC_CHECKER_ID = Session["USER_ID"].ToString();
                                                    UpdateEntity.AC_EDIT_DATETIME = DateTime.Now;
                                                    UpdateEntity.AC_IS_AUTH = true;
                                                    context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                                    RowCount += context.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        message = "Record authorized sucessfully.";
                                    }
                                }
                                else
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                            message = "Select From or To date to continue";
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Final Report
        public ActionResult FinalReport()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRS", "FinalReport", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult FinalReport(DateTime? FromDate, DateTime? ToDate)
        {
            string message = "";
            Session["GD_NO"] = "null";
            Session["RD_TYPE"] = "null";
            Session["SCHEDULE_CODE"] = "null";
            Session["SBP_APPROVAL_NO"] = "null";
            try
            {
                List<PSW_ITRS_AC_700> Data = context.PSW_ITRS_AC_700.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AC_TRANSACTION_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).ToList();
                if (Data.Count() > 0)
                {
                    var sb = new StringBuilder();
                    //Headers
                    sb.Append("Reporting date,Bank,Currency,Document/Schedule,Reference/Serial no.,AD,Transaction Date,Form No.,Unit,Quantity,Price,Amount,NTN/CNIC/Identity No,Exchange Rate (PKR per TC),Exchange Rate (PKR per USD),Country of Receipt/Payment/Immediate remitter,Country of Origin/Destination/Ultimate remitter,Incoterm,Statement,Beneficiary/Requesting Bank,Beneficiary/Requesting Branch,Scheme,Sales Terms,Date of Utilization,Amount Utilized in GD Currency,Reference of SBPs Approval,SBPs Approval Date,Department,Bill of Entry,Bill of Lading,HS /Purpose Code,Reference Document Type,Reference Document No.,Reference of SBP Approval Letter,Relevant Para of FEM if delegated to AD,Approval of Remittance,Beneficiary Name,Relationship with Beneficiary,Status of immediate remitter,Shipment Date,Negotiation Date,Invoice Currency,Freight Amount,Insurance,Company Name,LC Date,Loan ID,Lender Sector,Shareholding percentage,Project type,Funds realization,Opening Balance of S-1 (Dr.),Opening Balance of S-1 (Cr.),Closing Balance of S-1 (Dr.),Closing Balance of S-1 (Cr.),Opening Balance of S-4 (Dr.),Opening Balance of S-4 (Cr.),Closing Balance of S-4 (Dr.),Closing Balance of S-4 (Cr.),Opening Balance of S-6,Closing Balance of S-6" + Environment.NewLine);
                    foreach (var item in Data)
                    {
                        sb.Append("" + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd") + ",46," + GetCurrency(item.AC_CURRENCY) + "," + GetSchedule(item) + "," + item.AC_REFERENCE_NO + "," + GetAD(item.AC_DR_CUSTOMER_NAME) + "," + Convert.ToDateTime(item.AC_TRANSACTION_DATE, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "," + GetReferenceNo(item) + "," + GetHSCodeUnit(item.AC_REFERENCE_NO) + "," + GetHSCodeUnitQuantity(item.AC_REFERENCE_NO) + "," + GetHSCodeUnitPrice(item.AC_REFERENCE_NO) + "," + (item.AC_CREDIT_AMOUNT != null ? item.AC_CREDIT_AMOUNT : item.AC_DEBIT_AMOUNT) + "," + GetClientNTN(item) + "," + item.AC_EXCHANGE_RATE + ", " + GetDollarRate(item.AC_TRANSACTION_DATE) + " ," + GetPaymentCountry(item) + "," + GetOrigin(item.AC_REFERENCE_NO) + "," + GetIncoterm(item.AC_REFERENCE_NO) + "," + GetStatement(item) + "," + BeneRequestingBanks(item) + "," + GetBeneRequestingBranch(item.AC_BENE_ACCOUNT_NO) + "," + GetSchemeCode(item) + "," + GetSalesTerm(item.AC_REFERENCE_NO) + ",N.A,N.A,N.A,N.A," + GetClientDepartment(item.AC_DR_CUSTOMER_NAME) + "," + GetGDNo(item.AC_REFERENCE_NO) + "," + GetBLNO(item.AC_REFERENCE_NO) + "," + GetHsCode(item.AC_REFERENCE_NO, item.AC_PURPOSE_CODE, item) + " ," + ReferenceDocumentType(item) + "," + ReferenceDocumentNo(item) + "," + ReferenceofSBPApprovalLetter(item) + "," + RelevantParaOfFEM(item) + "," + ApprovalofRemittance() + ", " + BeneficiaryName(item) + " , " + RelationshipWithBeneficiary(item) + " , " + GetStatusOfImmediateRemitter() + " ," + ShipmentDate() + ",N.A," + InvoiceCurrency(item.AC_REFERENCE_NO) + "," + FreightAmount() + "," + Insurance() + ", " + CompanyName(item) + " , " + GetLCDate() + " , " + GetLoanID(item.AC_PAYMENT_DETAILS) + " , " + GetLenderSector(item) + " , " + GetShareHoldingPercentage(item.AC_PAYMENT_DETAILS) + " ,N.A, " + FundRealization(item.AC_REFERENCE_NO) + " ,,,,,,,,,," + Environment.NewLine);
                        message = sb.ToString();
                    }
                    string Todates = ToDate.ToString();
                    string[] date1 = Todates.Split('/');
                    string month = date1[0];
                    string year = date1[2];
                    string finaltodate = month + "/1/" + year;
                    DateTime FinalToDate = Convert.ToDateTime(finaltodate);

                    List<PSW_ITRS_DE003> GetDE003CDList = context.PSW_ITRS_DE003.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).ToList();
                    if (GetDE003CDList.Count() > 0)
                    {
                        foreach (var item in GetDE003CDList)
                        {
                            if (item.DE_TRANS_REF_NO.Contains("CDNC") || item.DE_TRANS_REF_NO.Contains("CDNP"))
                            {
                                sb.Append("" + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd") + ",46," + GetCurrency(item.DE_CURRENCY_CODE) + ",73," + item.DE_TRANS_REF_NO + "," + GetDe003AD(item.DE_DATA_CHECKER_ID) + "," + Convert.ToDateTime(item.DE_VALUE_DATE, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "," + GetDE003ReferenceNo(item.DE_TRANS_REF_NO) + ",,,," + item.DE_TXN_AMOUNT + "," + GetDe003NTN(item.DE_CUSTOMER_NAME) + ",,,2270,,,96,,,130,,,,,,,,,9534,,,,,,,,,,,,,,,,,,,,,,,,,,,,,," + Environment.NewLine);
                            }
                        }
                    }
                    List<PSW_ITRS_DE003> GetDE003CWList = context.PSW_ITRS_DE003.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).ToList();
                    if (GetDE003CWList.Count() > 0)
                    {
                        foreach (var item in GetDE003CWList)
                        {
                            if (item.DE_TRANS_REF_NO.Contains("CWCW") || item.DE_TRANS_REF_NO.Contains("CWCQ"))
                            {
                                sb.Append("" + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd") + ",46," + GetCurrency(item.DE_CURRENCY_CODE) + ",57," + item.DE_TRANS_REF_NO + "," + GetDe003AD(item.DE_DATA_CHECKER_ID) + "," + Convert.ToDateTime(item.DE_VALUE_DATE, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "," + GetDE003ReferenceNo(item.DE_TRANS_REF_NO) + ",,,," + item.DE_TXN_AMOUNT + "," + GetDe003NTN(item.DE_CUSTOMER_NAME) + ",,,2270,,,96,,,130,,,,,,"+ GetDe003Department(item.DE_CUSTOMER_NAME) + ",,,1524,,,,,,,,,,,,,,,,,,,,,,,,,,,,,," + Environment.NewLine);
                            }
                        }
                    }
                    List<PSW_ITRS_A404> GetA404List = context.PSW_ITRS_A404.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IA_UPLOAD_MONTH) >= System.Data.Entity.DbFunctions.TruncateTime(FinalToDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IA_UPLOAD_MONTH) <= System.Data.Entity.DbFunctions.TruncateTime(FinalToDate)).ToList();
                    if (GetA404List.Count() > 0)
                    {
                        foreach (var item in GetA404List)
                        {
                            sb.Append("" + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd") + ",46," + GetA404AndBalancesCurrency(item.IA_CURRENCY) + ",54," + item.IA_SERIAL_NO + "," + item.IA_AD + ",," + item.IA_EFORM_NO + ",,,,,,,,,,,,,,,," + GetA404UtilizedDate(item.IA_DATE_OF_UTILIZED) + "," + item.IA_AMOUNT_UTLIZED + "," + item.IA_RERERENCE_OF_SBP_APPROVAL + "," + GetA404SBPApprovalDate(item.IA_SBP_APPROVAL_DATE) +",,,," + item.IA_COMMODITY + " ,,,,,,,,," + GetA404UtilizedDate(item.IA_DATE_OF_UTILIZED) + ",,,,,,,,,,,,,,,,,,,,," + Environment.NewLine);
                        }
                    }
                    List<PSW_ITRS_BALANCES> GetBalancesList = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) >= System.Data.Entity.DbFunctions.TruncateTime(FinalToDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) <= System.Data.Entity.DbFunctions.TruncateTime(FinalToDate)).ToList();
                    if (GetBalancesList.Count() > 0)
                    {
                        foreach (var item in GetBalancesList)
                        {
                            sb.Append("" + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd") + ",46," + GetA404AndBalancesCurrency(item.IB_CURRENCY) + ",,," + item.IB_AD + ",,,,,,,,,,,,," + item.IB_STATEMENT + ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,," + item.IB_OPENING_BALANCE_OF_S1_DR + "," + item.IB_OPENING_BALANCE_OF_S1_CR + "," + item.IB_CLOSING_BALANCE_OF_S1_DR + "," + item.IB_CLOSING_BALANCE_OF_S1_CR + "," + item.IB_OPENING_BALANCE_OF_S4_DR + "," + item.IB_OPENING_BALANCE_OF_S4_CR + "," + item.IB_CLOSING_BALANCE_OF_S4_DR + "," + item.IB_CLOSING_BALANCE_OF_S4_CR + "," + item.IB_OPENING_BALANCE_OF_S6 + "," + item.IB_CLOSING_BALANCE_OF_S6 +"" + Environment.NewLine);
                        }
                    }
                    return File(Encoding.ASCII.GetBytes(sb.ToString()), "text/csv", "ITRS_" + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd")+".csv");
                }
                else
                {
                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                    var byteArray = System.Text.Encoding.ASCII.GetBytes(message);
                    var stream = new MemoryStream(byteArray);
                    string FileName = DateTime.Now.ToString();
                    return File(stream, "text/plain", FileName + ".txt");
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                var byteArray = System.Text.Encoding.ASCII.GetBytes(message);
                var stream = new MemoryStream(byteArray);
                string FileName = DateTime.Now.ToString();
                return File(stream, "text/plain", FileName + ".txt");
            }
        }

        private string GetA404UtilizedDate(DateTime? UtilizedDate)
        {
            string Date = "";
            if (UtilizedDate != null)
                Date = Convert.ToDateTime(UtilizedDate).ToString("yyyyMMdd");
            else
                Date = null;
            return Date;
        }
        private string GetA404AndBalancesCurrency(string Currency)
        {
            string CurrencyCode = "";
            int? currencycode = null;
            if (Currency != null && Currency != "")
                currencycode = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_CURRENCY_CODE == Currency && m.ICC_ISAUTH == true && m.ICC_STATUS == true).Select(m => m.ICC_CURRENCY_ID).FirstOrDefault();
            if (currencycode != null && currencycode != 0)
                CurrencyCode = currencycode.ToString();
            else
                CurrencyCode = Currency;
            return CurrencyCode;
        }

        private string GetDe003AD(string CheckerID)
        {
            string AD = "";
            if (CheckerID != null && CheckerID.Contains("SM53905"))
                AD = "70";
            else if (CheckerID != null && CheckerID.Contains("AK54084"))
                AD = "70";
            else if (CheckerID != null && CheckerID.Contains("NS65665"))
                AD = "70";
            else if (CheckerID != null && CheckerID.Contains("CS41830"))
                AD = "700";
            else if (CheckerID != null && CheckerID.Contains("DJ00315"))
                AD = "700";
            else if (CheckerID != null && CheckerID.Contains("KM49239"))
                AD = "339";
            else if (CheckerID != null && CheckerID.Contains("RH73632"))
                AD = "339";
            return AD;
        }
        public string GetDe003NTN(string ClientName)
        {
            string NTN = "";
            if (ClientName != null && ClientName != "")
            {
                PSW_ITRS_CLIENTS CheckClient = context.PSW_ITRS_CLIENTS.Where(m => m.IC_NAME.Contains(ClientName)).FirstOrDefault();
                if (CheckClient != null)
                {
                    NTN += "=";
                    NTN += '"' + CheckClient.IC_NTN + '"';
                }
            }
            return NTN;
        }
        public string GetDe003Department(string ClientName)
        {
            string Dept = "";
            if (ClientName != null && ClientName != "")
            {
                PSW_ITRS_CLIENTS CheckClient = context.PSW_ITRS_CLIENTS.Where(m => m.IC_NAME.Contains(ClientName)).FirstOrDefault();
                if (CheckClient != null)
                    Dept = CheckClient.IC_DEPT;
            }
            return Dept;
        }
        public string GetDE003ReferenceNo(string ReferenceNo)
        {
            string RefNo = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                var FinalString = ReferenceNo;
                RefNo = FinalString.Substring(FinalString.Length - 13).ToString();
            }
            return RefNo;
        }
        private string GetA404SBPApprovalDate(DateTime? SBPApprovalDate)
        {
            string Date = "";
            if (SBPApprovalDate != null)
                Date = Convert.ToDateTime(SBPApprovalDate).ToString("yyyyMMdd");
            else
                Date = null;
            return Date;
        }
        public int? GetCurrency(string Currency)
        {
            int? Currency_Code = 0;
            Currency_Code = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_CURRENCY_CODE == Currency).Select(m => m.ICC_CURRENCY_ID).FirstOrDefault();
            if (Currency_Code == null)
                Currency_Code = 0;
            return Currency_Code;
        }
        public string GetSchedule(PSW_ITRS_AC_700 Entity)
        {
            string Schedule_Code = "";
            Session["SCHEDULE_CODE"] = "null";
            if (Entity.AC_REFERENCE_NO.Contains("PK10506") && Entity.AC_CURRENCY == "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS CREDIT"))
                Schedule_Code = "67";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13020") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS CREDIT"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13020") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13025") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS CREDIT"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13021") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS CREDIT"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13021") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13031") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13031") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("FLEX balancing entry DR"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13032") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1400") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null)
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1400") && Entity.AC_CURRENCY == "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1400") && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1400") && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13023") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK13022") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK14002") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Fee billing"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK14002") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Fee billing"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK14000") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Securities Receive DR Equity"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK15785") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Book Transfer manual"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK17625") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT"))
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK15501") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS CREDIT"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1DIVD") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1ZNCS") && Entity.AC_CURRENCY == "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1ZSEC") && Entity.AC_CURRENCY == "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1ZSEC") && Entity.AC_CURRENCY == "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK10507") && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DEBIT_AMOUNT == 250)
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK10507") && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DEBIT_AMOUNT == 33)
                Schedule_Code = "73";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1BKOT") && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("TREASURY OPERATIONS"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1BKOT") && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("GLOBAL CUSTODY") && Entity.AC_TRANS_DETAIL.Contains("-PKR"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1BKOT") && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("GLOBAL CUSTODY") && Entity.AC_TRANS_DETAIL.Contains("USD-USD"))
                Schedule_Code = "77";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1BKOT") && Entity.AC_TRANS_DETAIL != null && !Entity.AC_TRANS_DETAIL.Contains("GLOBAL CUSTODY") && !Entity.AC_TRANS_DETAIL.Contains("TREASURY OPERATIONS"))
                Schedule_Code = "57";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1BKOZ") && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("CHARGES"))
                Schedule_Code = "59";//Fixed
            else if (Entity.AC_REFERENCE_NO.Contains("PK1BKOZ") && Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("CHARGES"))
                Schedule_Code = "59";//Fixed
            else if (Entity.AC_REFERENCE_NO.Contains("PK1BKOZ") && Entity.AC_TRANS_DETAIL != null && !Entity.AC_TRANS_DETAIL.Contains("CHARGES"))
                Schedule_Code = "55";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CWOX") && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("LOAN"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CWOX") && Entity.AC_TRANS_DETAIL != null && !Entity.AC_TRANS_DETAIL.Contains("LOAN"))
                Schedule_Code = "55";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") && Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("-OPN-"))
                Schedule_Code = "55";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") && Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("CN2"))
                Schedule_Code = "55";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("-PKR"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("USD-"))
                Schedule_Code = "57";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CWOT") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("USD-"))
                Schedule_Code = "57";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1INM1") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("USD-"))
                Schedule_Code = "57";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1PKSI") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("USD-"))
                Schedule_Code = "57";//Fixed
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SWIF") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("USD-"))
                Schedule_Code = "57";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SWIF") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("USD_"))
                Schedule_Code = "57";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SWIF") && Entity.AC_CURRENCY == "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CGWY") && Entity.AC_CURRENCY == "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1PKSI") && Entity.AC_CURRENCY == "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "67";//Fixed
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SWIF") && Entity.AC_CURRENCY == "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CGWY") && Entity.AC_CURRENCY == "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1PKSI") && Entity.AC_CURRENCY == "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";//Fixed
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SWIF") && Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1CWOT") && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("-PKR"))
                Schedule_Code = "59";//Working
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FTMN") && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("DOC-"))
                Schedule_Code = "51";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FTMN") && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("046-"))
                Schedule_Code = "52";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FTMN") && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("FUNDING RECEIVED"))
                Schedule_Code = "6";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FTMN") && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("DOC-"))
                Schedule_Code = "59";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FTMN"))
                Schedule_Code = "73";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1INKT") && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1INM1") && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1MMBO") && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "73";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1MMBO") && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "59";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1INKT") && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("-PKR"))
                Schedule_Code = "59";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1INM1") && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("-PKR"))
                Schedule_Code = "59";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1PKSI") && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("-PKR"))
                Schedule_Code = "59";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1PKSI") && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null)
                Schedule_Code = "59";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FWCM") && Entity.AC_CURRENCY == "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "79";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FWCM") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "77";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FWCM"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPCM") && Entity.AC_CURRENCY == "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "79";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPCM") && Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "77";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPCM"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "PKR")
                Schedule_Code = "67";
            //else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD")
            //    Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400114") && Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400118") && Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400127") && Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400138") && Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400137") && Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450198") && Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450458") && Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400123") && Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400114") && !Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400118") && !Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400127") && !Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400138") && !Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400137") && !Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450198") && !Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450458") && !Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400123") && !Entity.AC_TRANS_DETAIL.Contains("PKR"))
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400114"))
                Schedule_Code = "65";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400118"))
                Schedule_Code = "65";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400127"))
                Schedule_Code = "65";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400138"))
                Schedule_Code = "65";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400137"))
                Schedule_Code = "65";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450198"))
                Schedule_Code = "65";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450458"))
                Schedule_Code = "65";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400123"))
                Schedule_Code = "65";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400102"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401191"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401206"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401151"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400144"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401198"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400153"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450235"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400154"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450153"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401157"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400140"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400160"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450456"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450455"))
                Schedule_Code = "5";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400102"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401191"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401206"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401151"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400144"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401198"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400153"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450235"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400154"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450153"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0401157"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400140"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400160"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450456"))
                Schedule_Code = "12";
            else if (Entity.AC_CURRENCY == "USD" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450455"))
                Schedule_Code = "12";
            else if (Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0400157"))
                Schedule_Code = "6";
            else if (Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("0450454"))
                Schedule_Code = "6";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CURRENCY == "USD" && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "67";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FWIB") && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_DEBIT_AMOUNT != null)
                Schedule_Code = "53";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FWIB") && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "65";
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") && Entity.AC_CREDIT_AMOUNT != null)
                Schedule_Code = "65";
            else
                Schedule_Code = "";
            Session["SCHEDULE_CODE"] = Schedule_Code;
            return Schedule_Code;
        }
        public int? GetAD(string CustomerName)
        {
            int? AD_Code = 0;
            AD_Code = context.PSW_ITRS_CLIENTS.Where(m => m.IC_NAME == CustomerName).Select(m => m.IC_AD).FirstOrDefault();
            if (AD_Code == null)
                AD_Code = 70;
            return AD_Code;
        }
        public string GetReferenceNo(PSW_ITRS_AC_700 Entity)
        {
            string RefNo = "";
            if (Entity.AC_REFERENCE_NO != null && Entity.AC_REFERENCE_NO != "")
            {
                PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(Entity.AC_REFERENCE_NO)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                if (CheckBda != null)
                {
                    PSW_EIF_MASTER_DETAILS CheckFi = new PSW_EIF_MASTER_DETAILS();
                    CheckFi = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                    if (CheckFi != null)
                        RefNo = CheckFi.EIF_BI_EIF_REQUEST_NO;
                    else
                        RefNo = "";
                }
                else
                {
                    PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                    CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(Entity.AC_REFERENCE_NO)).FirstOrDefault();
                    if (CheckBca != null)
                    {
                        PSW_EEF_MASTER_DETAILS CheckFi = new PSW_EEF_MASTER_DETAILS();
                        CheckFi = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == CheckBca.BCAD_FORM_E_NO).FirstOrDefault();
                        if (CheckFi != null)
                        {
                            if (CheckFi.APM_STATUS == "Advance Payment")
                                RefNo = CheckFi.FDI_FORM_E_NO;
                            else
                            {
                                PSW_GD_ASSIGN_EFORMS CheckEEFGD = new PSW_GD_ASSIGN_EFORMS();
                                CheckEEFGD = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM.Contains(CheckBca.BCAD_FORM_E_NO)).FirstOrDefault();
                                if (CheckEEFGD != null)
                                    RefNo = CheckEEFGD.GDAEF_E_FORM;
                            }
                        }
                        else
                            RefNo = null;
                    }
                    else
                        RefNo = "";
                }
            }
            if (RefNo == null || RefNo == "")
            {
                if (Session["SCHEDULE_CODE"] != null && Session["SCHEDULE_CODE"] != null && Session["SCHEDULE_CODE"].ToString() == "59")
                {
                    if (Entity.AC_REFERENCE_NO != null && Entity.AC_REFERENCE_NO != "")
                    {
                        if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") || Entity.AC_REFERENCE_NO.Contains("PK1CWOT") || Entity.AC_REFERENCE_NO.Contains("PK1CWOX"))
                        {
                            if (Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("FORM-M"))
                            {
                                var FinalString = Entity.AC_PAYMENT_DETAILS.Split(new[] { "FORM-M" }, StringSplitOptions.None)[1];
                                FinalString = FinalString.Replace(" ", "");
                                RefNo = "FNC" + FinalString.Substring(0, 6);
                            }
                            else if (Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("Form-M"))
                            {
                                var FinalString = Entity.AC_PAYMENT_DETAILS.Split(new[] { "Form-M" }, StringSplitOptions.None)[1];
                                FinalString = FinalString.Replace(" ", "");
                                RefNo = "FNC" + FinalString.Substring(0, 6);
                            }
                            else if (Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("FORM M"))
                            {
                                var FinalString = Entity.AC_PAYMENT_DETAILS.Split(new[] { "FORM M" }, StringSplitOptions.None)[1];
                                FinalString = FinalString.Replace(" ", "");
                                RefNo = "FNC" + FinalString.Substring(0, 6);
                            }
                            else if (Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("Form M"))
                            {
                                var FinalString = Entity.AC_PAYMENT_DETAILS.Split(new[] { "Form M" }, StringSplitOptions.None)[1];
                                FinalString = FinalString.Replace(" ", "");
                                RefNo = "FNC" + FinalString.Substring(0, 6);
                            }
                            else if (Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("M FORM"))
                            {
                                var FinalString = Entity.AC_PAYMENT_DETAILS.Split(new[] { "M FORM" }, StringSplitOptions.None)[1];
                                FinalString = FinalString.Replace(" ", "");
                                RefNo = "FNC" + FinalString.Substring(0, 6);
                            }
                            else if (Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("M Form"))
                            {
                                var FinalString = Entity.AC_PAYMENT_DETAILS.Split(new[] { "M Form" }, StringSplitOptions.None)[1];
                                FinalString = FinalString.Replace(" ", "");
                                RefNo = "FNC" + FinalString.Substring(0, 6);
                            }
                        }
                    }
                }
                else
                {
                    if (Entity.AC_REFERENCE_NO != null && Entity.AC_REFERENCE_NO != "")
                    {
                        var FinalString = Entity.AC_REFERENCE_NO;
                        RefNo = FinalString.Substring(FinalString.Length - 13).ToString();
                    }
                }
            }
            if (RefNo == null || RefNo == "")
            {
                if (Entity.AC_REFERENCE_NO != null && Entity.AC_REFERENCE_NO != "")
                {
                    var FinalString = Entity.AC_REFERENCE_NO;
                    RefNo = FinalString.Substring(FinalString.Length - 13).ToString();
                }
            }
            Session["REFERENCE_NO"] = RefNo;
            return RefNo;
        }
        public string GetGDNo(string ReferenceNo)
        {
            string GDNo = "";
            Session["GD_NO"] = "null";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                if (CheckBda != null)
                {
                    if (CheckBda.EIF_BDA_GD_NO != null && CheckBda.EIF_BDA_GD_NO != "")
                    {
                        GDNo = CheckBda.EIF_BDA_GD_NO;
                        Session["GD_NO"] = GDNo;
                    }
                    else
                    {
                        PSW_EIF_GD_GENERAL_INFO_LIST CheckEIFGD = new PSW_EIF_GD_GENERAL_INFO_LIST();
                        CheckEIFGD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO.Contains(CheckBda.EIF_BDA_EIF_REQUEST_NO)).FirstOrDefault();
                        if (CheckEIFGD != null)
                            GDNo = CheckEIFGD.EIGDGI_GD_NO;
                        else
                            GDNo = "(Blank)";
                    }
                }
                else
                {
                    PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                    CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                    if (CheckBca != null)
                    {
                        if (CheckBca.BCAD_GD_NO != null && CheckBca.BCAD_GD_NO != "")
                        {
                            GDNo = CheckBca.BCAD_GD_NO;
                            Session["GD_NO"] = GDNo;
                        }
                        else
                        {
                            PSW_GD_ASSIGN_EFORMS CheckEEFGD = new PSW_GD_ASSIGN_EFORMS();
                            CheckEEFGD = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM.Contains(CheckBca.BCAD_FORM_E_NO)).FirstOrDefault();
                            if (CheckEEFGD != null)
                                GDNo = CheckEEFGD.GDAEF_E_FORM;
                            else
                                GDNo = "(Blank)";
                        }
                    }
                    else
                        GDNo = "(Blank)";
                }
            }
            return GDNo;
        }
        public string GetBLNO(string ReferenceNo)
        {
            string BLNO = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                if (CheckBda != null && CheckBda.EIF_BDA_BILL_OF_LADING != null)
                    BLNO = CheckBda.EIF_BDA_BILL_OF_LADING;
                else
                {
                    string GDNO = Session["GD_NO"].ToString();
                    if (GDNO != null && GDNO != "")
                    {
                        BLNO = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO == GDNO).Select(m => m.EIGDI_BL_NO).FirstOrDefault();
                        if (BLNO == null && BLNO == "")
                            BLNO = "N.A";
                    }
                    else
                        BLNO = "N.A";
                }
            }
            return BLNO;
        }
        public string GetHsCode(string ReferenceNo, string HSCODE, PSW_ITRS_AC_700 Entity)
        {
            string HsCode = "";
            string ScheduleCode = Session["SCHEDULE_CODE"].ToString();
            if (ReferenceNo != null && ReferenceNo != "")
            {
                if (ReferenceNo.Contains("BKOZ") || ReferenceNo.Contains("BKOT") || ReferenceNo.Contains("CDXB") || ReferenceNo.Contains("CWOT") || ReferenceNo.Contains("CWOX") || ReferenceNo.Contains("FTMN") || ReferenceNo.Contains("INM1") || ReferenceNo.Contains("INKT") || ReferenceNo.Contains("PKSI") || ReferenceNo.Contains("FWCM") || ReferenceNo.Contains("FWIB") || ReferenceNo.Contains("SPCM") || ReferenceNo.Contains("SPIB") || ReferenceNo.Contains("MANL") || ReferenceNo.Contains("SWIF") || ReferenceNo.Contains("CGWY"))
                {
                    PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                    CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                    if (CheckBda != null)
                    {
                        PSW_EIF_MASTER_DETAILS CheckFi = new PSW_EIF_MASTER_DETAILS();
                        CheckFi = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                        if (CheckFi != null)
                        {
                            PSW_EIF_HS_CODE GetEifHs = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == CheckFi.EIF_BI_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == CheckFi.EIF_BI_AMENDMENT_NO).OrderByDescending(m => m.EIF_HC_QUANTITY).FirstOrDefault();
                            if (GetEifHs != null)
                            {
                                //HsCode = GetEifHs.EIF_HC_CODE.ToString();
                                string FirstPart = ""; string SecondPart = "";
                                if (GetEifHs.EIF_HC_CODE != null)
                                {
                                    if (GetEifHs.EIF_HC_CODE.Contains("."))
                                    {
                                        FirstPart = GetEifHs.EIF_HC_CODE.Split('.')[0];
                                        SecondPart = GetEifHs.EIF_HC_CODE.Split('.')[1];
                                        FirstPart = FirstPart.ToString().PadLeft(4, '0');
                                        SecondPart = SecondPart.ToString().PadRight(4, '0');
                                        HsCode = FirstPart + SecondPart;
                                    }
                                    else
                                        HsCode = GetEifHs.EIF_HC_CODE.ToString().PadLeft(8, '0');
                                }
                            }
                        }
                        else
                            HsCode = HSCODE;
                    }
                    else
                    {
                        PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                        CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                        if (CheckBca != null)
                        {
                            PSW_EEF_MASTER_DETAILS CheckFi = new PSW_EEF_MASTER_DETAILS();
                            CheckFi = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == CheckBca.BCAD_FORM_E_NO).FirstOrDefault();
                            if (CheckFi != null)
                            {
                                PSW_EEF_HS_CODE GetEEfHs = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == CheckFi.FDI_FORM_E_NO && m.EEF_HC_AMENDMENT_NO == CheckFi.FDI_AMENDMENT_NO).OrderByDescending(m => m.EEF_HC_QUANTITY).FirstOrDefault();
                                if (GetEEfHs != null)
                                {
                                    //HsCode = GetEEfHs.EEF_HC_CODE.ToString();
                                    string FirstPart = ""; string SecondPart = "";
                                    if (GetEEfHs.EEF_HC_CODE != null)
                                    {
                                        if (GetEEfHs.EEF_HC_CODE.Contains("."))
                                        {
                                            FirstPart = GetEEfHs.EEF_HC_CODE.Split('.')[0];
                                            SecondPart = GetEEfHs.EEF_HC_CODE.Split('.')[1];
                                            FirstPart = FirstPart.ToString().PadLeft(4, '0');
                                            SecondPart = SecondPart.ToString().PadRight(4, '0');
                                            HsCode = FirstPart + SecondPart;
                                        }
                                        else
                                            HsCode = GetEEfHs.EEF_HC_CODE.ToString().PadLeft(8, '0');
                                    }
                                }
                            }
                            else
                                HsCode = HSCODE;
                        }
                        else
                            HsCode = HSCODE;
                    }
                }
                else
                    HsCode = HSCODE;
                if (HsCode == null || HsCode == "")
                {
                    if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT - MISCELLANEOUS DEBIT"))
                        HsCode = "9431";
                    else if(Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT -MISCELLANEOUS DEBIT"))
                        HsCode = "9431";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT - PLACEMENT"))
                        HsCode = "9539";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS DEBIT -PLACEMENT"))
                        HsCode = "9539";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1MMBO") && Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Money Market Deposit Start"))
                        HsCode = "9522";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1MMBO") && Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Money Market Deposit Maturity"))
                        HsCode = "1528";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1MMBO") && Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Money Market Deposit Interest"))
                        HsCode = "1436";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1BKOT") && Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("TREASURY OPERATIONS - KHI"))
                        HsCode = "1532";
                    else if (Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS CREDIT -"))
                        HsCode = "1171";
                    else if (Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("MISCELLANEOUS CREDIT - "))
                        HsCode = "1171";
                    else if (Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Fee billing"))
                        HsCode = "1171";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Fee billing"))
                        HsCode = "9171";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("-USD"))
                        HsCode = "9534";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("-GBP"))
                        HsCode = "9534";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("-JPY"))
                        HsCode = "9534";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("-EUR"))
                        HsCode = "9534";
                    else if (ScheduleCode != null && ScheduleCode == "67")
                        HsCode = "6010";
                    else if (ScheduleCode != null && ScheduleCode == "53")
                        HsCode = "6011";
                    else if (ScheduleCode != null && ScheduleCode == "5")
                        HsCode = "6012";
                    else if (ScheduleCode != null && ScheduleCode == "6")
                        HsCode = "6013";
                    else if (ScheduleCode != null && ScheduleCode == "22")
                        HsCode = "6014";
                    else if (ScheduleCode != null && ScheduleCode == "57")
                        HsCode = "1524";
                    else if (ScheduleCode != null && ScheduleCode == "77")
                        HsCode = "2010";
                    else if (ScheduleCode != null && ScheduleCode == "79")
                        HsCode = "7011";
                    else if (ScheduleCode != null && ScheduleCode == "65")
                        HsCode = "2011";
                    else if (ScheduleCode != null && ScheduleCode == "12")
                        HsCode = "2012";
                    else if (ScheduleCode != null && ScheduleCode == "23")
                        HsCode = "2015";
                    else if (Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_PAYMENT_DETAILS != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_PURPOSE> GetPurpose = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ISAUTH == true && m.IPC_STATUS == true).ToList();
                        if (GetPurpose != null && GetPurpose.Count() > 0)
                        {
                            foreach (var item in GetPurpose)
                            {
                                if (CheckStatus == false)
                                {
                                    if (item.IPC_PURPOSE_ID != null && item.IPC_PURPOSE_ID != 0)
                                    {
                                        if (Entity.AC_PAYMENT_DETAILS.Contains(item.IPC_PURPOSE_NAME))
                                        {
                                            string FirstChar = item.IPC_PURPOSE_ID.ToString();
                                            FirstChar = FirstChar.Substring(0, 1);
                                            if (FirstChar == "1")
                                            {
                                                HsCode = item.IPC_PURPOSE_ID.ToString();
                                                CheckStatus = true;
                                                break;
                                            }
                                            else
                                                CheckStatus = false;
                                        }
                                        else
                                            CheckStatus = false;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                            {
                                if (Entity.AC_PAYMENT_DETAILS.Contains(","))
                                {
                                    HsCode = Entity.AC_PAYMENT_DETAILS.Replace(",", "");
                                }
                                else
                                    HsCode = Entity.AC_PAYMENT_DETAILS;
                            }
                        }
                    }
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_PAYMENT_DETAILS != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_PURPOSE> GetPurpose = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ISAUTH == true && m.IPC_STATUS == true).ToList();
                        if (GetPurpose != null && GetPurpose.Count() > 0)
                        {
                            foreach (var item in GetPurpose)
                            {
                                if (CheckStatus == false)
                                {
                                    if (item.IPC_PURPOSE_ID != null && item.IPC_PURPOSE_ID != 0)
                                    {
                                        if (Entity.AC_PAYMENT_DETAILS.Contains(item.IPC_PURPOSE_NAME))
                                        {
                                            string FirstChar = item.IPC_PURPOSE_ID.ToString();
                                            FirstChar = FirstChar.Substring(0, 1);
                                            if (FirstChar == "9")
                                            {
                                                HsCode = item.IPC_PURPOSE_ID.ToString();
                                                CheckStatus = true;
                                                break;
                                            }
                                            else
                                                CheckStatus = false;
                                        }
                                        else
                                            CheckStatus = false;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                            {
                                if (Entity.AC_PAYMENT_DETAILS.Contains(","))
                                {
                                    HsCode = Entity.AC_PAYMENT_DETAILS.Replace(",", "");
                                }
                                else
                                    HsCode = Entity.AC_PAYMENT_DETAILS;
                            }
                        }
                    }
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("CGT PAYMENT"))
                        HsCode = "9451";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Inter BRK Comm WHT -"))
                        HsCode = "9451";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("CUSTODY FEES"))
                        HsCode = "9171";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("BILLING FEE"))
                        HsCode = "9171";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("TRANSFER STAMPS FOR SHARE"))
                        HsCode = "9171";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("FLEX balancing entry DR"))
                        HsCode = "9171";
                    else if (Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Securities Receive DR Equity -"))
                        HsCode = "9171";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1ZSEC") && Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("PAKISTAN TREASURY BILLS"))
                        HsCode = "9622";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1ZSEC") && Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("PAKISTAN INVESTMENT BOND"))
                        HsCode = "9612";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1ZSEC") && Entity.AC_DEBIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Securities Receive CR Equity"))
                        HsCode = "9608";
                    else if (Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("CGT REFUND"))
                        HsCode = "1174";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1ZSEC") && Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("PAKISTAN TREASURY BILLS"))
                        HsCode = "1612";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1ZSEC") && Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("PAKISTAN INVESTMENT BOND"))
                        HsCode = "1622";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1ZSEC") && Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Securities Deliver DR Equity"))
                        HsCode = "1608";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1ZNCS") && Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("Securities Deliver CR coupon"))
                        HsCode = "1622";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1ZNCS") && Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59")
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_PURPOSE> GetPurpose = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ISAUTH == true && m.IPC_STATUS == true).ToList();
                        if (GetPurpose != null && GetPurpose.Count() > 0)
                        {
                            foreach (var item in GetPurpose)
                            {
                                if (CheckStatus == false)
                                {
                                    if (item.IPC_PURPOSE_ID != null && item.IPC_PURPOSE_ID != 0)
                                    {
                                        if (Entity.AC_TRANS_DETAIL.Contains(item.IPC_PURPOSE_NAME))
                                        {
                                            HsCode = item.IPC_PURPOSE_ID.ToString();
                                            CheckStatus = true;
                                            break;
                                        }
                                        else
                                            CheckStatus = false;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                            {
                                if (Entity.AC_TRANS_DETAIL.Contains(","))
                                {
                                    HsCode = Entity.AC_TRANS_DETAIL.Replace(",", "");
                                }
                                else
                                    HsCode = Entity.AC_TRANS_DETAIL;
                            }
                        }
                    }
                    else if (Entity.AC_CREDIT_AMOUNT != null && ScheduleCode != null && ScheduleCode == "59" && Entity.AC_DESCRIPTION != null && Entity.AC_DESCRIPTION.Contains("FLEX balancing entry CR"))
                        HsCode = "1171";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1SWIF") && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DEBIT_AMOUNT == 250 && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_CURRENCY == "PKR")
                        HsCode = "9171";
                    else if (Entity.AC_REFERENCE_NO.Contains("PK1SWIF") && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_DEBIT_AMOUNT == 33 && ScheduleCode != null && ScheduleCode == "73" && Entity.AC_CURRENCY == "PKR")
                        HsCode = "9171";
                }
            }
            return HsCode;
        }
        public string GetHSCodeUnit(string ReferenceNo)
        {
            string UnitCode = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                if (ReferenceNo.Contains("BKOZ") || ReferenceNo.Contains("BKOT") || ReferenceNo.Contains("CDXB") || ReferenceNo.Contains("CWOT") || ReferenceNo.Contains("CWOX") || ReferenceNo.Contains("FTMN") || ReferenceNo.Contains("INM1") || ReferenceNo.Contains("INKT") || ReferenceNo.Contains("PKSI") || ReferenceNo.Contains("FWCM") || ReferenceNo.Contains("FWIB") || ReferenceNo.Contains("SPCM") || ReferenceNo.Contains("SPIB") || ReferenceNo.Contains("MANL") || ReferenceNo.Contains("SWIF") || ReferenceNo.Contains("CGWY"))
                {
                    PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                    CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                    if (CheckBda != null)
                    {
                        PSW_EIF_BASIC_INFO CheckFi = new PSW_EIF_BASIC_INFO();
                        CheckFi = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                        if (CheckFi != null)
                        {
                            PSW_EIF_HS_CODE GetEifHs = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == CheckFi.EIF_BI_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == CheckFi.EIF_BI_AMENDMENT_NO).OrderByDescending(m => m.EIF_HC_QUANTITY).FirstOrDefault();
                            if (GetEifHs != null)
                            {
                                PSW_ITRS_UNIT_CODE GetUnit = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_UNIT_NAME.Contains(GetEifHs.EIF_HC_UOM)).FirstOrDefault();
                                if (GetUnit != null)
                                    UnitCode = GetUnit.IUC_UNIT_CODE.ToString();
                                else
                                    UnitCode = "";
                            }
                        }
                        else
                            UnitCode = "";

                        #region Condition Not in Used
                        //if (CheckBda.EIF_BDA_GD_NO != null)
                        //{
                        //    PSW_EIF_GD_GENERAL_INFO_LIST CheckEIFGD = new PSW_EIF_GD_GENERAL_INFO_LIST();
                        //    CheckEIFGD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO.Contains(CheckBda.EIF_BDA_GD_NO)).FirstOrDefault();
                        //    if (CheckEIFGD != null)
                        //    {
                        //        PSW_GD_IMORT_HS_CODE GetHsCode = context.PSW_GD_IMORT_HS_CODE.Where(m => m.IHC_GD_NO == CheckEIFGD.EIGDGI_GD_NO && m.IHC_AMENDMENT_NO == CheckEIFGD.EIGDGI_AMENDMENT_NO).OrderByDescending(m => m.IHC_QUANTITY).FirstOrDefault();
                        //        if (GetHsCode != null)
                        //        {
                        //            PSW_ITRS_UNIT_CODE GetUnit = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_UNIT_NAME.Contains(GetHsCode.IHC_UOM)).FirstOrDefault();
                        //            if (GetUnit != null)
                        //                UnitCode = GetUnit.IUC_UNIT_CODE.ToString();
                        //            else
                        //                UnitCode = "";
                        //        }
                        //        else
                        //            UnitCode = "";
                        //    }
                        //    else
                        //        UnitCode = "";
                        //}
                        //else if (UnitCode == "" || UnitCode == null)
                        //{
                        //    PSW_EIF_GD_GENERAL_INFO_LIST CheckEIFGD = new PSW_EIF_GD_GENERAL_INFO_LIST();
                        //    CheckEIFGD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO.Contains(CheckBda.EIF_BDA_EIF_REQUEST_NO)).FirstOrDefault();
                        //    if (CheckEIFGD != null)
                        //    {
                        //        PSW_GD_IMORT_HS_CODE GetHsCode = context.PSW_GD_IMORT_HS_CODE.Where(m => m.IHC_GD_NO == CheckEIFGD.EIGDGI_GD_NO && m.IHC_AMENDMENT_NO == CheckEIFGD.EIGDGI_AMENDMENT_NO).OrderByDescending(m => m.IHC_QUANTITY).FirstOrDefault();
                        //        if (GetHsCode != null)
                        //        {
                        //            PSW_ITRS_UNIT_CODE GetUnit = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_UNIT_NAME.Contains(GetHsCode.IHC_UOM)).FirstOrDefault();
                        //            if (GetUnit != null)
                        //                UnitCode = GetUnit.IUC_UNIT_CODE.ToString();
                        //            else
                        //                UnitCode = "";
                        //        }
                        //        else
                        //            UnitCode = "";
                        //    }
                        //    else
                        //        UnitCode = "";
                        //}
                        //if (UnitCode == "" || UnitCode == null)
                        //{
                        //    PSW_EIF_MASTER_DETAILS CheckFi = new PSW_EIF_MASTER_DETAILS();
                        //    CheckFi = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                        //    if (CheckFi != null)
                        //    {
                        //        PSW_EIF_HS_CODE GetEifHs = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == CheckFi.EIF_BI_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == CheckFi.EIF_BI_AMENDMENT_NO).OrderByDescending(m => m.EIF_HC_QUANTITY).FirstOrDefault();
                        //        if (GetEifHs != null)
                        //        {
                        //            PSW_ITRS_UNIT_CODE GetUnit = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_UNIT_NAME.Contains(GetEifHs.EIF_HC_UOM)).FirstOrDefault();
                        //            if (GetUnit != null)
                        //                UnitCode = GetUnit.IUC_UNIT_CODE.ToString();
                        //            else
                        //                UnitCode = "";
                        //        }
                        //    }
                        //    else
                        //        UnitCode = "";
                        //}
                        #endregion
                    }
                    else
                    {
                        PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                        CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                        if (CheckBca != null)
                        {
                            PSW_FORM_E_DOCUMENT_INFO CheckFi = new PSW_FORM_E_DOCUMENT_INFO();
                            CheckFi = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == CheckBca.BCAD_FORM_E_NO).FirstOrDefault();
                            if (CheckFi != null)
                            {
                                PSW_EEF_HS_CODE GetEEfHs = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == CheckFi.FDI_FORM_E_NO && m.EEF_HC_AMENDMENT_NO == CheckFi.FDI_AMENDMENT_NO).OrderByDescending(m => m.EEF_HC_QUANTITY).FirstOrDefault();
                                if (GetEEfHs != null)
                                {
                                    PSW_ITRS_UNIT_CODE GetUnit = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_UNIT_NAME.Contains(GetEEfHs.EEF_HC_UOM)).FirstOrDefault();
                                    if (GetUnit != null)
                                        UnitCode = GetUnit.IUC_UNIT_CODE.ToString();
                                    else
                                        UnitCode = "";
                                }
                            }
                            else
                                UnitCode = "";
                            #region Condition not in use
                            //if (CheckBca.BCAD_GD_NO != null)
                            //{
                            //    PSW_EEF_GD_GENERAL_INFO_LIST CheckEEFGD = new PSW_EEF_GD_GENERAL_INFO_LIST();
                            //    CheckEEFGD = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_GD_NO.Contains(CheckBca.BCAD_GD_NO)).FirstOrDefault();
                            //    if (CheckEEFGD != null)
                            //    {
                            //        PSW_GD_EXPORT_HS_CODE GetGdHSCode = context.PSW_GD_EXPORT_HS_CODE.Where(m => m.EHC_GD_NO == CheckEEFGD.EFGDGI_GD_NO && m.EHC_AMENDMENT_NO == CheckEEFGD.EFGDGI_AMENDMENT_NO).OrderByDescending(m => m.EHC_QUANTITY).FirstOrDefault();
                            //        if (GetGdHSCode != null)
                            //        {
                            //            PSW_ITRS_UNIT_CODE GetUnit = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_UNIT_NAME.Contains(GetGdHSCode.EHC_UOM)).FirstOrDefault();
                            //            if (GetUnit != null)
                            //                UnitCode = GetUnit.IUC_UNIT_CODE.ToString();
                            //            else
                            //                UnitCode = "";
                            //        }
                            //        else
                            //            UnitCode = "";
                            //    }
                            //    else
                            //        UnitCode = "";
                            //}
                            //else if (UnitCode == null || UnitCode == "")
                            //{
                            //    PSW_GD_ASSIGN_EFORMS CheckEEFGD = new PSW_GD_ASSIGN_EFORMS();
                            //    CheckEEFGD = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM.Contains(CheckBca.BCAD_FORM_E_NO)).FirstOrDefault();
                            //    if (CheckEEFGD != null)
                            //    {
                            //        PSW_GD_EXPORT_HS_CODE GetGdHSCode = context.PSW_GD_EXPORT_HS_CODE.Where(m => m.EHC_GD_NO == CheckEEFGD.GDAEF_GD_NO && m.EHC_AMENDMENT_NO == CheckEEFGD.GDAEF_AMENDMENT_NO).OrderByDescending(m => m.EHC_QUANTITY).FirstOrDefault();
                            //        if (GetGdHSCode != null)
                            //        {
                            //            PSW_ITRS_UNIT_CODE GetUnit = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_UNIT_NAME.Contains(GetGdHSCode.EHC_UOM)).FirstOrDefault();
                            //            if (GetUnit != null)
                            //                UnitCode = GetUnit.IUC_UNIT_CODE.ToString();
                            //            else
                            //                UnitCode = "";
                            //        }
                            //        else
                            //            UnitCode = "";
                            //    }
                            //    else
                            //        UnitCode = "";
                            //}
                            //if (UnitCode == null || UnitCode == "")
                            //{

                            //}
                            #endregion
                        }
                        else
                            UnitCode = "";
                    }
                }
                else
                    UnitCode = "";
            }
            return UnitCode;
        }
        public string GetHSCodeUnitQuantity(string ReferenceNo)
        {
            string Quantity = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                if (ReferenceNo.Contains("BKOZ") || ReferenceNo.Contains("BKOT") || ReferenceNo.Contains("CDXB") || ReferenceNo.Contains("CWOT") || ReferenceNo.Contains("CWOX") || ReferenceNo.Contains("FTMN") || ReferenceNo.Contains("INM1") || ReferenceNo.Contains("INKT") || ReferenceNo.Contains("PKSI") || ReferenceNo.Contains("FWCM") || ReferenceNo.Contains("FWIB") || ReferenceNo.Contains("SPCM") || ReferenceNo.Contains("SPIB") || ReferenceNo.Contains("MANL") || ReferenceNo.Contains("SWIF") || ReferenceNo.Contains("CGWY"))
                {
                    PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                    CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                    if (CheckBda != null)
                    {
                        PSW_EIF_BASIC_INFO CheckFi = new PSW_EIF_BASIC_INFO();
                        CheckFi = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                        if (CheckFi != null)
                        {
                            PSW_EIF_HS_CODE GetEifHs = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == CheckFi.EIF_BI_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == CheckFi.EIF_BI_AMENDMENT_NO).OrderByDescending(m => m.EIF_HC_QUANTITY).FirstOrDefault();
                            if (GetEifHs != null)
                            {
                                Quantity = GetEifHs.EIF_HC_QUANTITY.ToString();
                            }
                        }
                        else
                            Quantity = "";
                        #region Condition not in use
                        //if (CheckBda.EIF_BDA_GD_NO != null)
                        //{
                        //    PSW_EIF_GD_GENERAL_INFO_LIST CheckEIFGD = new PSW_EIF_GD_GENERAL_INFO_LIST();
                        //    CheckEIFGD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO.Contains(CheckBda.EIF_BDA_GD_NO)).FirstOrDefault();
                        //    if (CheckEIFGD != null)
                        //    {
                        //        PSW_GD_IMORT_HS_CODE GetHsCode = context.PSW_GD_IMORT_HS_CODE.Where(m => m.IHC_GD_NO == CheckEIFGD.EIGDGI_GD_NO && m.IHC_AMENDMENT_NO == CheckEIFGD.EIGDGI_AMENDMENT_NO).OrderByDescending(m => m.IHC_QUANTITY).FirstOrDefault();
                        //        if (GetHsCode != null)
                        //            Quantity = GetHsCode.IHC_QUANTITY.ToString();
                        //        else
                        //            Quantity = "";
                        //    }
                        //    else
                        //        Quantity = "";
                        //}
                        //else if (Quantity == "" || Quantity == null)
                        //{
                        //    PSW_EIF_GD_GENERAL_INFO_LIST CheckEIFGD = new PSW_EIF_GD_GENERAL_INFO_LIST();
                        //    CheckEIFGD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO.Contains(CheckBda.EIF_BDA_EIF_REQUEST_NO)).FirstOrDefault();
                        //    if (CheckEIFGD != null)
                        //    {
                        //        PSW_GD_IMORT_HS_CODE GetHsCode = context.PSW_GD_IMORT_HS_CODE.Where(m => m.IHC_GD_NO == CheckEIFGD.EIGDGI_GD_NO && m.IHC_AMENDMENT_NO == CheckEIFGD.EIGDGI_AMENDMENT_NO).OrderByDescending(m => m.IHC_QUANTITY).FirstOrDefault();
                        //        if (GetHsCode != null)
                        //            Quantity = GetHsCode.IHC_QUANTITY.ToString();
                        //        else
                        //            Quantity = "";
                        //    }
                        //    else
                        //        Quantity = "";
                        //}
                        //if (Quantity == "" || Quantity == null)
                        //{
                            
                        //}
                        #endregion
                    }
                    else
                    {
                        PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                        CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                        if (CheckBca != null)
                        {
                            PSW_FORM_E_DOCUMENT_INFO CheckFi = new PSW_FORM_E_DOCUMENT_INFO();
                            CheckFi = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == CheckBca.BCAD_FORM_E_NO).FirstOrDefault();
                            if (CheckFi != null)
                            {
                                PSW_EEF_HS_CODE GetEEfHs = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == CheckFi.FDI_FORM_E_NO && m.EEF_HC_AMENDMENT_NO == CheckFi.FDI_AMENDMENT_NO).OrderByDescending(m => m.EEF_HC_QUANTITY).FirstOrDefault();
                                if (GetEEfHs != null)
                                {
                                    Quantity = GetEEfHs.EEF_HC_QUANTITY.ToString();
                                }
                            }
                            else
                                Quantity = "";
                            #region Condition not in use
                            //if (CheckBca.BCAD_GD_NO != null)
                            //{
                            //    PSW_EEF_GD_GENERAL_INFO_LIST CheckEEFGD = new PSW_EEF_GD_GENERAL_INFO_LIST();
                            //    CheckEEFGD = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_GD_NO.Contains(CheckBca.BCAD_GD_NO)).FirstOrDefault();
                            //    if (CheckEEFGD != null)
                            //    {
                            //        PSW_GD_EXPORT_HS_CODE GetGdHSCode = context.PSW_GD_EXPORT_HS_CODE.Where(m => m.EHC_GD_NO == CheckEEFGD.EFGDGI_GD_NO && m.EHC_AMENDMENT_NO == CheckEEFGD.EFGDGI_AMENDMENT_NO).OrderByDescending(m => m.EHC_QUANTITY).FirstOrDefault();
                            //        if (GetGdHSCode != null)
                            //            Quantity = GetGdHSCode.EHC_QUANTITY.ToString();
                            //        else
                            //            Quantity = "";
                            //    }
                            //    else
                            //        Quantity = "";
                            //}
                            //else if (Quantity == null || Quantity == "")
                            //{
                            //    PSW_GD_ASSIGN_EFORMS CheckEEFGD = new PSW_GD_ASSIGN_EFORMS();
                            //    CheckEEFGD = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM.Contains(CheckBca.BCAD_FORM_E_NO)).FirstOrDefault();
                            //    if (CheckEEFGD != null)
                            //    {
                            //        PSW_GD_EXPORT_HS_CODE GetGdHSCode = context.PSW_GD_EXPORT_HS_CODE.Where(m => m.EHC_GD_NO == CheckEEFGD.GDAEF_GD_NO && m.EHC_AMENDMENT_NO == CheckEEFGD.GDAEF_AMENDMENT_NO).OrderByDescending(m => m.EHC_QUANTITY).FirstOrDefault();
                            //        if (GetGdHSCode != null)
                            //            Quantity = GetGdHSCode.EHC_QUANTITY.ToString();
                            //        else
                            //            Quantity = "";
                            //    }
                            //    else
                            //        Quantity = "";
                            //}
                            //if (Quantity == null || Quantity == "")
                            //{

                            //}
                            #endregion
                        }
                        else
                            Quantity = "";
                    }
                }
                else
                    Quantity = "";
            }
            Session["QUANTITY"] = Quantity;
            return Quantity;
        }
        public string GetHSCodeUnitPrice(string ReferenceNo)
        {
            string Price = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                if (ReferenceNo.Contains("BKOZ") || ReferenceNo.Contains("BKOT") || ReferenceNo.Contains("CDXB") || ReferenceNo.Contains("CWOT") || ReferenceNo.Contains("CWOX") || ReferenceNo.Contains("FTMN") || ReferenceNo.Contains("INM1") || ReferenceNo.Contains("INKT") || ReferenceNo.Contains("PKSI") || ReferenceNo.Contains("FWCM") || ReferenceNo.Contains("FWIB") || ReferenceNo.Contains("SPCM") || ReferenceNo.Contains("SPIB") || ReferenceNo.Contains("MANL") || ReferenceNo.Contains("SWIF") || ReferenceNo.Contains("CGWY"))
                {
                    PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                    CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                    if (CheckBda != null)
                    {
                        PSW_EIF_BASIC_INFO CheckFi = new PSW_EIF_BASIC_INFO();
                        CheckFi = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                        if (CheckFi != null)
                        {
                            PSW_EIF_HS_CODE GetEifHs = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == CheckFi.EIF_BI_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == CheckFi.EIF_BI_AMENDMENT_NO).OrderByDescending(m => m.EIF_HC_QUANTITY).FirstOrDefault();
                            if (GetEifHs != null)
                            {
                                if (GetEifHs.EIF_HC_AMOUNT_AGAINST_HS_CODE != null)
                                    Price = GetEifHs.EIF_HC_AMOUNT_AGAINST_HS_CODE.ToString();
                                else
                                    Price = "";
                            }
                        }
                        else
                            Price = "";
                        #region Condition not in use
                        //if (CheckBda.EIF_BDA_GD_NO != null)
                        //{
                        //    PSW_EIF_GD_GENERAL_INFO_LIST CheckEIFGD = new PSW_EIF_GD_GENERAL_INFO_LIST();
                        //    CheckEIFGD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO.Contains(CheckBda.EIF_BDA_GD_NO)).FirstOrDefault();
                        //    if (CheckEIFGD != null)
                        //    {
                        //        PSW_GD_IMORT_HS_CODE GetHsCode = context.PSW_GD_IMORT_HS_CODE.Where(m => m.IHC_GD_NO == CheckEIFGD.EIGDGI_GD_NO && m.IHC_AMENDMENT_NO == CheckEIFGD.EIGDGI_AMENDMENT_NO).OrderByDescending(m => m.IHC_QUANTITY).FirstOrDefault();
                        //        if (GetHsCode != null)
                        //            Price = GetHsCode.IHC_UNIT_PRICE.ToString();
                        //        else
                        //            Price = "";
                        //    }
                        //    else
                        //        Price = "";
                        //}
                        //else if (Price == "" || Price == null)
                        //{
                        //    PSW_EIF_GD_GENERAL_INFO_LIST CheckEIFGD = new PSW_EIF_GD_GENERAL_INFO_LIST();
                        //    CheckEIFGD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO.Contains(CheckBda.EIF_BDA_EIF_REQUEST_NO)).FirstOrDefault();
                        //    if (CheckEIFGD != null)
                        //    {
                        //        PSW_GD_IMORT_HS_CODE GetHsCode = context.PSW_GD_IMORT_HS_CODE.Where(m => m.IHC_GD_NO == CheckEIFGD.EIGDGI_GD_NO && m.IHC_AMENDMENT_NO == CheckEIFGD.EIGDGI_AMENDMENT_NO).OrderByDescending(m => m.IHC_QUANTITY).FirstOrDefault();
                        //        if (GetHsCode != null)
                        //            Price = GetHsCode.IHC_UNIT_PRICE.ToString();
                        //        else
                        //            Price = "";
                        //    }
                        //    else
                        //        Price = "";
                        //}
                        //if (Price == "" || Price == null)
                        //{

                        //}
                        #endregion
                    }
                    else
                    {
                        PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                        CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                        if (CheckBca != null)
                        {
                            PSW_FORM_E_DOCUMENT_INFO CheckFi = new PSW_FORM_E_DOCUMENT_INFO();
                            CheckFi = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == CheckBca.BCAD_FORM_E_NO).FirstOrDefault();
                            if (CheckFi != null)
                            {
                                PSW_EEF_HS_CODE GetEEfHs = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == CheckFi.FDI_FORM_E_NO && m.EEF_HC_AMENDMENT_NO == CheckFi.FDI_AMENDMENT_NO).OrderByDescending(m => m.EEF_HC_QUANTITY).FirstOrDefault();
                                if (GetEEfHs != null)
                                {
                                    if (GetEEfHs.EEF_HC_AMOUNT_AGAINST_HS_CODE != null)
                                        Price = GetEEfHs.EEF_HC_AMOUNT_AGAINST_HS_CODE.ToString();
                                    else
                                        Price = "";
                                }
                            }
                            else
                                Price = "";
                            #region Condition not in use
                            //if (CheckBca.BCAD_GD_NO != null)
                            //{
                            //    PSW_EEF_GD_GENERAL_INFO_LIST CheckEEFGD = new PSW_EEF_GD_GENERAL_INFO_LIST();
                            //    CheckEEFGD = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_GD_NO.Contains(CheckBca.BCAD_GD_NO)).FirstOrDefault();
                            //    if (CheckEEFGD != null)
                            //    {
                            //        PSW_GD_EXPORT_HS_CODE GetGdHSCode = context.PSW_GD_EXPORT_HS_CODE.Where(m => m.EHC_GD_NO == CheckEEFGD.EFGDGI_GD_NO && m.EHC_AMENDMENT_NO == CheckEEFGD.EFGDGI_AMENDMENT_NO).OrderByDescending(m => m.EHC_QUANTITY).FirstOrDefault();
                            //        if (GetGdHSCode != null)
                            //            Price = GetGdHSCode.EHC_UNIT_PRICE.ToString();
                            //        else
                            //            Price = "";
                            //    }
                            //    else
                            //        Price = "";
                            //}
                            //else if (Price == null || Price == "")
                            //{
                            //    PSW_GD_ASSIGN_EFORMS CheckEEFGD = new PSW_GD_ASSIGN_EFORMS();
                            //    CheckEEFGD = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM.Contains(CheckBca.BCAD_FORM_E_NO)).FirstOrDefault();
                            //    if (CheckEEFGD != null)
                            //    {
                            //        PSW_GD_EXPORT_HS_CODE GetGdHSCode = context.PSW_GD_EXPORT_HS_CODE.Where(m => m.EHC_GD_NO == CheckEEFGD.GDAEF_GD_NO && m.EHC_AMENDMENT_NO == CheckEEFGD.GDAEF_AMENDMENT_NO).OrderByDescending(m => m.EHC_QUANTITY).FirstOrDefault();
                            //        if (GetGdHSCode != null)
                            //            Price = GetGdHSCode.EHC_UNIT_PRICE.ToString();
                            //        else
                            //            Price = "";
                            //    }
                            //    else
                            //        Price = "";
                            //}
                            //if (Price == null || Price == "")
                            //{

                            //}
                            #endregion
                        }
                        else
                            Price = "";
                    }
                }
                else
                    Price = "";
            }
            if (Price != "" && Price != null)
            {
                string Quantity = Session["QUANTITY"].ToString();
                if (Quantity != null && Quantity != "")
                {
                    decimal QuantityInt;
                    decimal PriceInt;
                    decimal result;
                    QuantityInt = Convert.ToDecimal(Quantity);
                    PriceInt = Convert.ToDecimal(Price);
                    result = QuantityInt / PriceInt;
                    Price = result.ToString();
                }
            }
            return Price;
        }
        public string GetOrigin(string ReferenceNo)
        {
            string Origin = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                if (ReferenceNo.Contains("BKOZ") || ReferenceNo.Contains("BKOT") || ReferenceNo.Contains("CDXB") || ReferenceNo.Contains("CWOT") || ReferenceNo.Contains("CWOX") || ReferenceNo.Contains("FTMN") || ReferenceNo.Contains("INM1") || ReferenceNo.Contains("INKT") || ReferenceNo.Contains("PKSI") || ReferenceNo.Contains("FWCM") || ReferenceNo.Contains("FWIB") || ReferenceNo.Contains("SPCM") || ReferenceNo.Contains("SPIB") || ReferenceNo.Contains("MANL") || ReferenceNo.Contains("SWIF") || ReferenceNo.Contains("CGWY"))
                {
                    PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                    CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                    if (CheckBda != null)
                    {
                        PSW_EIF_MASTER_DETAILS CheckFi = new PSW_EIF_MASTER_DETAILS();
                        CheckFi = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                        if (CheckFi != null)
                        {
                            PSW_EIF_HS_CODE GetEifHs = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == CheckFi.EIF_BI_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == CheckFi.EIF_BI_AMENDMENT_NO).OrderByDescending(m => m.EIF_HC_QUANTITY).FirstOrDefault();
                            if (GetEifHs != null)
                            {
                                PSW_LIST_OF_COUNTRIES GetFiCountry = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_CODE.Contains(GetEifHs.EIF_HC_ORGIN)).FirstOrDefault();
                                if (GetFiCountry != null)
                                {
                                    PSW_ITRS_COUNTRY getOriginCode = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_COUNTRY_NAME.Contains(GetFiCountry.LC_NAME)).FirstOrDefault();
                                    if (getOriginCode != null)
                                        Origin = getOriginCode.CUN_COUNTRY_ID.ToString();
                                }
                                else
                                    Origin = "N.A";
                            }
                            else
                                Origin = "N.A";
                        }
                        else
                            Origin = "N.A";
                    }
                    else
                    {
                        PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                        CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                        if (CheckBca != null)
                        {
                            PSW_EEF_MASTER_DETAILS CheckFi = new PSW_EEF_MASTER_DETAILS();
                            CheckFi = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == CheckBca.BCAD_FORM_E_NO).FirstOrDefault();
                            if (CheckFi != null)
                            {
                                if (CheckFi.FDRI_COUNTRY != 0)
                                {
                                    PSW_LIST_OF_COUNTRIES GetFiCountry = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == CheckFi.FDRI_COUNTRY).FirstOrDefault();
                                    if (GetFiCountry != null)
                                    {
                                        PSW_ITRS_COUNTRY getOriginCode = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_COUNTRY_NAME.Contains(GetFiCountry.LC_NAME)).FirstOrDefault();
                                        if (getOriginCode != null)
                                            Origin = getOriginCode.CUN_COUNTRY_ID.ToString();
                                    }
                                    else
                                        Origin = "N.A";
                                }
                                else
                                    Origin = "N.A";
                            }
                            else
                                Origin = "N.A";
                        }
                        else
                            Origin = "N.A";
                    }
                }
                else
                    Origin = "N.A";
            }
            return Origin;
        }
        public string GetSalesTerm(string ReferenceNo)
        {
            string SalesTerm = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                if (CheckBda != null)
                {
                    PSW_EIF_MASTER_DETAILS CheckFi = new PSW_EIF_MASTER_DETAILS();
                    CheckFi = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                    if (CheckFi != null)
                    {
                        PSW_PAYMENT_MODES_PARAMETERS GetFiDetail = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == CheckFi.EIF_BI_EIF_REQUEST_NO && m.EIF_PMP_AMENDMENT_NO == CheckFi.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                        if (GetFiDetail != null)
                        {
                            if (CheckFi.APM_STATUS.Contains("Letter of Credit"))
                            {
                                if (GetFiDetail.EIF_PMP_ADV_PAYPERCENT != 0 && GetFiDetail.EIF_PMP_ADV_PAYPERCENT != null)
                                    SalesTerm = "20";
                                else if (GetFiDetail.EIF_PMP_SIGHT_PERCENT != 0 && GetFiDetail.EIF_PMP_SIGHT_PERCENT != null)
                                    SalesTerm = "60";
                                else if (GetFiDetail.EIF_PMP_USANCE_PERCENT != 0 && GetFiDetail.EIF_PMP_USANCE_PERCENT != null)
                                    SalesTerm = "50";
                            }
                            else if (CheckFi.APM_STATUS.Contains("Advance Payment"))
                                SalesTerm = "10";
                            else if (CheckFi.APM_STATUS.Contains("Open Account"))
                                SalesTerm = "70";
                            else if (CheckFi.APM_STATUS.Contains("Contract/Collection"))
                            {
                                if (GetFiDetail.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT != 0 && GetFiDetail.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT != null)
                                    SalesTerm = "80";
                                else if (GetFiDetail.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT != 0 && GetFiDetail.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT != null)
                                    SalesTerm = "90";
                                else if (GetFiDetail.EIF_PMP_ADV_PAYPERCENT != 0 && GetFiDetail.EIF_PMP_ADV_PAYPERCENT != null)
                                    SalesTerm = "20";
                            }
                            else
                                SalesTerm = "N.A";
                        }
                        else
                            SalesTerm = "N.A";
                    }
                    else
                        SalesTerm = "N.A";
                }
                else
                {
                    PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                    CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                    if (CheckBca != null)
                    {
                        PSW_EEF_MASTER_DETAILS CheckFi = new PSW_EEF_MASTER_DETAILS();
                        CheckFi = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == CheckBca.BCAD_FORM_E_NO).FirstOrDefault();
                        if (CheckFi != null)
                        {
                            PSW_PAYMENT_MODES_PARAMETERS GetFiDetail = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == CheckFi.FDI_FORM_E_NO && m.EIF_PMP_AMENDMENT_NO == CheckFi.FDI_AMENDMENT_NO).FirstOrDefault();
                            if (GetFiDetail != null)
                            {
                                if (CheckFi.APM_STATUS.Contains("Letter of Credit"))
                                {
                                    if (GetFiDetail.EIF_PMP_ADV_PAYPERCENT != 0 && GetFiDetail.EIF_PMP_ADV_PAYPERCENT != null)
                                        SalesTerm = "20";
                                    else if (GetFiDetail.EIF_PMP_SIGHT_PERCENT != 0 && GetFiDetail.EIF_PMP_SIGHT_PERCENT != null)
                                        SalesTerm = "60";
                                    else if (GetFiDetail.EIF_PMP_USANCE_PERCENT != 0 && GetFiDetail.EIF_PMP_USANCE_PERCENT != null)
                                        SalesTerm = "50";
                                }
                                else if (CheckFi.APM_STATUS.Contains("Advance Payment"))
                                    SalesTerm = "10";
                                else if (CheckFi.APM_STATUS.Contains("Open Account"))
                                    SalesTerm = "70";
                                else if (CheckFi.APM_STATUS.Contains("Contract/Collection"))
                                {
                                    if (GetFiDetail.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT != 0 && GetFiDetail.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT != null)
                                        SalesTerm = "80";
                                    else if (GetFiDetail.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT != 0 && GetFiDetail.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT != null)
                                        SalesTerm = "90";
                                    else if (GetFiDetail.EIF_PMP_ADV_PAYPERCENT != 0 && GetFiDetail.EIF_PMP_ADV_PAYPERCENT != null)
                                        SalesTerm = "20";
                                }
                                else
                                    SalesTerm = "N.A";
                            }
                            else
                                SalesTerm = "N.A";
                        }
                        else
                            SalesTerm = "N.A";
                    }
                    else
                        SalesTerm = "N.A";
                }
            }
            Session["SALES_TERM"] = SalesTerm;
            return SalesTerm;
        }
        public string GetIncoterm(string ReferenceNo)
        {
            string Incoterm = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                if (CheckBda != null)
                {
                    PSW_EIF_MASTER_DETAILS CheckFi = new PSW_EIF_MASTER_DETAILS();
                    CheckFi = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                    if (CheckFi != null)
                    {
                        PSW_ITRS_INCOTERM GetIncoterm = context.PSW_ITRS_INCOTERM.Where(m => m.ICT_INCOTERM_NAME.Contains(CheckFi.DT_DESCRIPTION)).FirstOrDefault();
                        if (GetIncoterm != null)
                        {
                            if (GetIncoterm.ICT_INCOTERM_CODE != null)
                                Incoterm = GetIncoterm.ICT_INCOTERM_CODE.ToString();
                            else
                                Incoterm = "";
                        }
                        else
                            Incoterm = "";
                    }
                    else
                        Incoterm = "";
                }
                else
                {
                    PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                    CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                    if (CheckBca != null)
                    {
                        PSW_EEF_MASTER_DETAILS CheckFi = new PSW_EEF_MASTER_DETAILS();
                        CheckFi = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == CheckBca.BCAD_FORM_E_NO).FirstOrDefault();
                        if (CheckFi != null)
                        {
                            PSW_ITRS_INCOTERM GetIncoterm = context.PSW_ITRS_INCOTERM.Where(m => m.ICT_INCOTERM_NAME.Contains(CheckFi.DT_DESCRIPTION)).FirstOrDefault();
                            if (GetIncoterm != null)
                            {
                                if (GetIncoterm.ICT_INCOTERM_CODE != null)
                                    Incoterm = GetIncoterm.ICT_INCOTERM_CODE.ToString();
                                else
                                    Incoterm = "";
                            }
                            else
                                Incoterm = "";
                        }
                        else
                            Incoterm = "";
                    }
                    else
                        Incoterm = "";
                }
            }
            return Incoterm;
        }
        public string GetStatement(PSW_ITRS_AC_700 Entity)
        {
            string Statement = "";
            if (Entity.AC_REFERENCE_NO != null && Entity.AC_REFERENCE_NO != "")
            {
                if (Entity.AC_CURRENCY != "PKR" && Entity.AC_REFERENCE_NO.Contains("PK1CDNC") || Entity.AC_REFERENCE_NO.Contains("PK1CWCQ") || Entity.AC_REFERENCE_NO.Contains("PK1CWCW") || Entity.AC_REFERENCE_NO.Contains("PK1OSYS") || Entity.AC_REFERENCE_NO.Contains("PK1OFSY") || Entity.AC_REFERENCE_NO.Contains("PK1ZCSH"))
                    Statement = "96";
                else if (Entity.AC_CURRENCY != "PKR")
                    Statement = "91";
                else if (Entity.AC_CURRENCY == "PKR")
                    Statement = "94";
                else
                    Statement = null;
            }
            return Statement;
        }
        public string GetBeneRequestingBranch(string Branch)
        {
            string FinalResult = "";
            if (Branch != null && Branch != "")
            {
                FinalResult = "'" + Branch;
                FinalResult = FinalResult.Trim();
            }
            else
                FinalResult = "N.A";
            return FinalResult;
        }
        public string BeneRequestingBanks(PSW_ITRS_AC_700 Entity)
        {
            int? Name = null;
            string ReturnString = "";
            if (Entity.AC_CURRENCY == "PKR" && Entity.AC_DEBIT_AMOUNT != null && Entity.AC_REFERENCE_NO.Contains("PK1SWIF"))
            {
                if (Entity.AC_ACCOUNT_WITH_INSTITUTION != null && Entity.AC_ACCOUNT_WITH_INSTITUTION != "")
                {
                    string BankName = Entity.AC_ACCOUNT_WITH_INSTITUTION.Trim();
                    Name = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => m.BRB_BANK_NAME.Contains(BankName)).Select(m => m.BRB_BANK_CODE).FirstOrDefault();
                    if (Name != null && Name != 0)
                        ReturnString = Name.ToString();
                    else
                        ReturnString = "N.A";
                }
                else
                    ReturnString = "N.A";
            }
            else if (Entity.AC_REFERENCE_NO.Contains("PK1SPIB") || Entity.AC_REFERENCE_NO.Contains("PK1FWIB"))
            {
                if (Entity.AC_TRANS_DETAIL != null && Entity.AC_TRANS_DETAIL.Contains("Counterparty Of Contract "))
                {
                    string BankCode = "";
                    var FinalString = Entity.AC_TRANS_DETAIL.Split(new[] { "Counterparty Of Contract " }, StringSplitOptions.None)[1];
                    BankCode = FinalString.Substring(0, 7);
                    Name = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => m.BRB_BANK_NAME.Contains(BankCode)).Select(m => m.BRB_BANK_CODE).FirstOrDefault();
                    if (Name != null && Name != 0)
                        ReturnString = Name.ToString();
                    else
                        ReturnString = "N.A";
                }
                else
                    ReturnString = "N.A";
            }
            else
                ReturnString = "N.A";
            return ReturnString;
        }
        public int? GetSchemeCode(PSW_ITRS_AC_700 Entity)
        {
            string SchemeCode = "";
            int? FinalSchemeCode = null;
            if (Entity.AC_REFERENCE_NO != null && Entity.AC_REFERENCE_NO != "")
            {
                if (Entity.AC_FT_ACCOUNT_NO != null && Entity.AC_FT_ACCOUNT_NO != "")
                {
                    SchemeCode = Entity.AC_FT_ACCOUNT_NO;
                    FinalSchemeCode = context.PSW_ITRS_SCHEME.Where(m => m.ISC_SCHEME_NAME.Contains(SchemeCode) && m.ISC_ISAUTH == true && m.ISC_STATUS == true).Select(m => m.ISC_SCHEME_CODE).FirstOrDefault();
                }
                else if (Entity.AC_ACCOUNT_NO != null && Entity.AC_ACCOUNT_NO != "")
                {
                    SchemeCode = Entity.AC_ACCOUNT_NO;
                    FinalSchemeCode = context.PSW_ITRS_SCHEME.Where(m => m.ISC_SCHEME_NAME.Contains(SchemeCode) && m.ISC_ISAUTH == true && m.ISC_STATUS == true).Select(m => m.ISC_SCHEME_CODE).FirstOrDefault();
                }
                else
                    FinalSchemeCode = 998;
                if (FinalSchemeCode == null || FinalSchemeCode == 0)
                    FinalSchemeCode = 998;
            }
            return FinalSchemeCode;
        }
        public string GetPaymentCountry(PSW_ITRS_AC_700 Entity)
        {
            string PayCount = "";
            if (Entity.AC_REFERENCE_NO != null && Entity.AC_REFERENCE_NO != "")
            {
                if (Entity.AC_CURRENCY == "PKR" && Entity.AC_REFERENCE_NO.Contains("PK1SWIF") && Entity.AC_DEBIT_AMOUNT != null)
                {
                    if (Entity.AC_BY_ORDER != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_COUNTRY> GetCountry = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ISAUTH == true && m.CUN_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_BY_ORDER.Contains(item.CUN_COUNTRY_NAME))
                                    {
                                        PayCount = item.CUN_COUNTRY_ID.ToString();
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                            {
                                if (Entity.AC_BY_ORDER.Contains(","))
                                {
                                    PayCount = Entity.AC_BY_ORDER.Replace(",", "");
                                }
                                else
                                    PayCount = Entity.AC_BY_ORDER;
                            }
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else if (Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                {
                    if (Entity.AC_BY_ORDER != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_COUNTRY> GetCountry = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ISAUTH == true && m.CUN_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_BY_ORDER.Contains(item.CUN_COUNTRY_NAME))
                                    {
                                        PayCount = item.CUN_COUNTRY_ID.ToString();
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                            {
                                if (Entity.AC_BY_ORDER.Contains(","))
                                {
                                    PayCount = Entity.AC_BY_ORDER.Replace(",", "");
                                }
                                else
                                    PayCount = Entity.AC_BY_ORDER;
                            }
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else if (Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_REFERENCE_NO.Contains("PK1BKOZ"))
                {
                    if (Entity.AC_ACCOUNT_WITH_INSTITUTION != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_COUNTRY> GetCountry = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ISAUTH == true && m.CUN_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_ACCOUNT_WITH_INSTITUTION.Contains(item.CUN_COUNTRY_NAME))
                                    {
                                        PayCount = item.CUN_COUNTRY_ID.ToString();
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                            {
                                if (Entity.AC_ACCOUNT_WITH_INSTITUTION.Contains(","))
                                {
                                    PayCount = Entity.AC_ACCOUNT_WITH_INSTITUTION.Replace(",", "");
                                }
                                else
                                    PayCount = Entity.AC_ACCOUNT_WITH_INSTITUTION;
                            }
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else if (Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_REFERENCE_NO.Contains("PK1CDXB") || Entity.AC_REFERENCE_NO.Contains("PK1CWOX"))
                {
                    if (Entity.AC_ULTIMATE_BENEFICIARY != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_COUNTRY> GetCountry = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ISAUTH == true && m.CUN_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_ULTIMATE_BENEFICIARY.Contains(item.CUN_COUNTRY_NAME))
                                    {
                                        PayCount = item.CUN_COUNTRY_ID.ToString();
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                            {
                                if (Entity.AC_ULTIMATE_BENEFICIARY.Contains(","))
                                {
                                    PayCount = Entity.AC_ULTIMATE_BENEFICIARY.Replace(",", "");
                                }
                                else
                                    PayCount = Entity.AC_ULTIMATE_BENEFICIARY;
                            }
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else if (Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                {
                    if (Entity.AC_BY_ORDER != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_COUNTRY> GetCountry = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ISAUTH == true && m.CUN_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_BY_ORDER.Contains(item.CUN_COUNTRY_NAME))
                                    {
                                        PayCount = item.CUN_COUNTRY_ID.ToString();
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                            {
                                if (Entity.AC_BY_ORDER.Contains(","))
                                {
                                    PayCount = Entity.AC_BY_ORDER.Replace(",", "");
                                }
                                else
                                    PayCount = Entity.AC_BY_ORDER;
                            }
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else
                    PayCount = "N.A";
            }
            return PayCount;
        }
        public string GetClientNTN(PSW_ITRS_AC_700 Entity)
        {
            string NTN = "";
            PSW_ITRS_CLIENTS CheckClient = new PSW_ITRS_CLIENTS();
            if (Entity.AC_REFERENCE_NO.Contains("PK1ZSEC") || Entity.AC_REFERENCE_NO.Contains("PK1ZNCS"))
            {
                CheckClient = context.PSW_ITRS_CLIENTS.Where(m => m.IC_NAME == Entity.AC_BENE_NAME).FirstOrDefault();
                if (CheckClient != null)
                {
                    NTN += "=";
                    NTN += '"' + CheckClient.IC_NTN + '"';
                }
                else
                {
                    if (Entity.AC_BENE_NAME != null)
                    {
                        NTN = Entity.AC_BENE_NAME;
                        if (Entity.AC_BENE_NAME.Contains(","))
                        {
                            NTN = NTN.Replace(",", "");
                            NTN = NTN.Trim();
                        }
                        else
                            NTN = NTN.Trim();
                    }
                    else
                        NTN = "";
                }
            }
            else if (Entity.AC_REFERENCE_NO.Contains("PK1FTMN") || Entity.AC_REFERENCE_NO.Contains("PK1INM1"))
            {
                CheckClient = context.PSW_ITRS_CLIENTS.Where(m => m.IC_NAME == Entity.AC_DR_CUSTOMER_NAME).FirstOrDefault();
                if (CheckClient != null)
                {
                    NTN += "=";
                    NTN += '"' + CheckClient.IC_NTN + '"';
                }
                else
                {
                    if (Entity.AC_DR_CUSTOMER_NAME != null)
                    {
                        NTN = Entity.AC_DR_CUSTOMER_NAME;
                        if (Entity.AC_DR_CUSTOMER_NAME.Contains(","))
                        {
                            NTN = NTN.Replace(",", "");
                            NTN = NTN.Trim();
                        }
                        else
                            NTN = NTN.Trim();
                    }
                    else
                        NTN = "";
                }
            }
            else if (Entity.AC_REFERENCE_NO.Contains("PK1PKSI") || Entity.AC_REFERENCE_NO.Contains("PK1CWOX") || Entity.AC_REFERENCE_NO.Contains("PK1CDXB") || Entity.AC_REFERENCE_NO.Contains("PK1BKOZ") || Entity.AC_REFERENCE_NO.Contains("PK1SWIF") || Entity.AC_REFERENCE_NO.Contains("PK1INKT"))
            {
                CheckClient = context.PSW_ITRS_CLIENTS.Where(m => m.IC_NAME == Entity.AC_DR_CUSTOMER_NAME).FirstOrDefault();
                if (CheckClient != null)
                {
                    NTN += "=";
                    NTN += '"' + CheckClient.IC_NTN + '"';
                }
                else
                {
                    if (Entity.AC_DR_CUSTOMER_NAME != null)
                    {
                        NTN = Entity.AC_DR_CUSTOMER_NAME;
                        if (Entity.AC_DR_CUSTOMER_NAME.Contains(","))
                        {
                            NTN = NTN.Replace(",", "");
                            NTN = NTN.Trim();
                        }
                        else
                            NTN = NTN.Trim();
                    }
                    else
                        NTN = "";
                }
            }
            else
                NTN = "";
            return NTN;
        }
        public string GetClientDepartment(string CustomerName)
        {
            string Dept = "";
            if (CustomerName != null && CustomerName != "")
            {
                PSW_ITRS_CLIENTS CheckClient = new PSW_ITRS_CLIENTS();
                CheckClient = context.PSW_ITRS_CLIENTS.Where(m => m.IC_NAME.Contains(CustomerName)).FirstOrDefault();
                if (CheckClient != null)
                    Dept = CheckClient.IC_DEPT;
                else
                    Dept = "N.A";
            }
            else
                Dept = "N.A";
            return Dept;
        }
        public string ReferenceDocumentType(PSW_ITRS_AC_700 Entity)
        {
            string RDtype = "";
            Session["RD_TYPE"] = "null";
            if (Entity.AC_REFERENCE_NO.Contains("PK1FTMN") && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("DOC-"))
            {
                RDtype = "EFORM";
                Session["RD_TYPE"] = RDtype;
            }
            else
                RDtype = "N.A";
            return RDtype;
        }
        public string ReferenceDocumentNo(PSW_ITRS_AC_700 Entity)
        {
            string RDNo = "";
            if (Entity.AC_REFERENCE_NO.Contains("PK1FTMN") && Entity.AC_CREDIT_AMOUNT != null)
            {
                if (Session["RD_TYPE"].ToString() == "EFORM")
                    RDNo = Session["GD_NO"].ToString();
                else
                    RDNo = "N.A";
            }
            else
                RDNo = "N.A";
            return RDNo;
        }
        public string ReferenceofSBPApprovalLetter(PSW_ITRS_AC_700 Entity)
        {
            string RDNo = "";
            Session["SBP_APPROVAL_NO"] = "null";
            if (Session["SCHEDULE_CODE"] != null && Session["SCHEDULE_CODE"].ToString() != "" && Session["SCHEDULE_CODE"].ToString() == "59")
            {
                if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") || Entity.AC_REFERENCE_NO.Contains("PK1CWOT") || Entity.AC_REFERENCE_NO.Contains("PK1CWOX"))
                {
                    if (Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains("SBP"))
                    {
                        if (!Entity.AC_PAYMENT_DETAILS.Contains("SBP "))
                        {
                            Entity.AC_PAYMENT_DETAILS = Entity.AC_PAYMENT_DETAILS.Replace(" ", ",");
                            string[] arr = Entity.AC_PAYMENT_DETAILS.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            foreach (var item in arr)
                            {
                                if (item.Contains("SBP"))
                                {
                                    RDNo = item;
                                    Session["SBP_APPROVAL_NO"] = RDNo;
                                    return RDNo;
                                }
                                else
                                    RDNo = "N.A";
                            }
                        }
                        else
                            RDNo = "N.A";
                    }
                    else
                        RDNo = "N.A";
                }
                else
                    RDNo = "N.A";
            }
            else
                RDNo = "N.A";
            return RDNo;
        }
        public string RelevantParaOfFEM(PSW_ITRS_AC_700 Entity)
        {
            string RDNo = "";
            if (Session["SCHEDULE_CODE"] != null && Session["SCHEDULE_CODE"].ToString() != "" && Session["SCHEDULE_CODE"].ToString() == "59")
            {
                if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") || Entity.AC_REFERENCE_NO.Contains("PK1CWOT") || Entity.AC_REFERENCE_NO.Contains("PK1CWOX"))
                {
                    if (Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS.Contains(" CH "))
                    {
                        var FinalString = Entity.AC_PAYMENT_DETAILS.Split(new[] { " CH " }, StringSplitOptions.None)[1];
                        RDNo = "CH " + FinalString.Substring(0, 8);
                    }
                    else
                        RDNo = "N.A";
                }
                else
                    RDNo = "N.A";
            }
            else
                RDNo = "N.A";
            return RDNo;
        }
        public string BeneficiaryName(PSW_ITRS_AC_700 Entity)
        {
            string ReturnString = "";
            if (Session["SCHEDULE_CODE"] != null && Session["SCHEDULE_CODE"].ToString() != "" && Session["SCHEDULE_CODE"].ToString() == "59")
            {
                if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") || Entity.AC_REFERENCE_NO.Contains("PK1CWOT") || Entity.AC_REFERENCE_NO.Contains("PK1CWOX"))
                {
                    if (Entity.AC_ULTIMATE_BENEFICIARY != null)
                    {
                        if (Entity.AC_ULTIMATE_BENEFICIARY.Contains(","))
                        {
                            ReturnString = Entity.AC_ULTIMATE_BENEFICIARY;
                            ReturnString = ReturnString.Replace(",", "");
                        }
                        else
                            ReturnString = Entity.AC_ULTIMATE_BENEFICIARY;
                    }
                    else
                        ReturnString = "N.A";
                }
                else
                    ReturnString = "N.A";
            }
            else
                ReturnString = "N.A";
            return ReturnString;
        }
        public string RelationshipWithBeneficiary(PSW_ITRS_AC_700 Entity)
        {
            int? ReturnString = null;
            string Returnvalue = "";
            if (Session["SCHEDULE_CODE"] != null && Session["SCHEDULE_CODE"].ToString() != "" && Session["SCHEDULE_CODE"].ToString() == "59")
            {
                if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") || Entity.AC_REFERENCE_NO.Contains("PK1CWOT") || Entity.AC_REFERENCE_NO.Contains("PK1CWOX"))
                {
                    bool CheckStatus = false;
                    List<PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY> GetList = new List<PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY>();
                    GetList = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => m.RWB_ISAUTH == true && m.RWB_STATUS == true).ToList();
                    if (GetList != null && GetList.Count() > 0)
                    {
                        if (Entity.AC_PAYMENT_DETAILS != null && Entity.AC_PAYMENT_DETAILS != "")
                        {
                            foreach (var item in GetList)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_PAYMENT_DETAILS.Contains(item.RWB_BANK_NAME))
                                    {
                                        if (item.RWB_BANK_CODE != null)
                                        {
                                            Returnvalue = item.RWB_BANK_CODE.ToString();
                                            CheckStatus = true;
                                            break;
                                        }
                                        else
                                        {
                                            Returnvalue = "N.A";
                                            CheckStatus = true;
                                            break;
                                        }
                                    }
                                    else
                                        CheckStatus = false;
                                }
                                else
                                    Returnvalue = "N.A";
                            }
                        }
                        else
                            Returnvalue = "N.A";
                    }
                }
                else
                    Returnvalue = "N.A";
            }
            else
                Returnvalue = "N.A";
            return Returnvalue;
        }
        public string ApprovalofRemittance()
        {
            string AOR = "";
            string SBP_APPROVAL_NO = Session["SBP_APPROVAL_NO"].ToString();
            if (SBP_APPROVAL_NO != null && SBP_APPROVAL_NO != "null" && SBP_APPROVAL_NO != "")
            {
                if (SBP_APPROVAL_NO.Contains("EPD"))
                    AOR = "1 - Approved by EPD";
                else if (SBP_APPROVAL_NO.Contains("FEOD"))
                    AOR = "2 - Approved by FEOD";
            }
            else
                AOR = "3 - Delegated to AD";
            return AOR;
        }
        public string ShipmentDate()
        {
            DateTime? AOR = null;
            string FinalDate = null;
            if (Session["GD_NO"] != null && Session["GD_NO"].ToString() != "null" && Session["GD_NO"].ToString() != "")
            {
                string GD_NO = Session["GD_NO"].ToString();
                PSW_EIF_GD_GENERAL_INFO_LIST EIF_GD = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDI_GD_NO == GD_NO).FirstOrDefault();
                if (EIF_GD != null)
                {
                    AOR = context.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_GD_NO == EIF_GD.EIGDGI_GD_NO && m.EIGDGI_AMENDMENT_NO == EIF_GD.EIGDGI_AMENDMENT_NO).Select(m => m.EIGDGI_CLEARANCE_DATE).FirstOrDefault();
                    if (AOR != null)
                        FinalDate = AOR?.ToString("yyyyMMdd");
                    else
                        FinalDate = "N.A";
                    return FinalDate;
                }
                else
                {
                    PSW_EEF_GD_GENERAL_INFO_LIST EEF_GD = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_GD_NO == GD_NO).FirstOrDefault();
                    if (EIF_GD != null)
                    {
                        AOR = context.PSW_EEF_GD_GENERAL_INFO.Where(m => m.EFGDGI_GD_NO == EIF_GD.EIGDGI_GD_NO && m.EFGDGI_AMENDMENT_NO == EIF_GD.EIGDGI_AMENDMENT_NO).Select(m => m.EFGDGI_CLEARANCE_DATE).FirstOrDefault();
                        if (AOR != null)
                            FinalDate = AOR?.ToString("yyyyMMdd");
                        else
                            FinalDate = "N.A";
                        return FinalDate;
                    }
                    else
                        FinalDate = "N.A";
                }
            }
            return FinalDate;
        }
        public string InvoiceCurrency(string ReferenceNo)
        {
            string CurrencyCode = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                PSW_EIF_BDA_LIST CheckBda = new PSW_EIF_BDA_LIST();
                CheckBda = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_FLEX_REFERENCE.Contains(ReferenceNo)).OrderByDescending(m => m.EIF_BDA_AMOUNT_FCY).FirstOrDefault();
                if (CheckBda != null)
                {
                    PSW_EIF_MASTER_DETAILS CheckFi = new PSW_EIF_MASTER_DETAILS();
                    CheckFi = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckBda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                    if (CheckFi != null && CheckFi.EIF_DRI_CCY != null)
                    {
                        PSW_CURRENCY PSW_Currency = new PSW_CURRENCY();
                        PSW_Currency = context.PSW_CURRENCY.Where(m => m.CUR_ID == CheckFi.EIF_DRI_CCY).FirstOrDefault();
                        if (PSW_Currency != null && PSW_Currency.CUR_NAME != null)
                        {
                            PSW_ITRS_CURRENCY ITRS_Currency = new PSW_ITRS_CURRENCY();
                            ITRS_Currency = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_CURRENCY_CODE == PSW_Currency.CUR_NAME).FirstOrDefault();
                            if (ITRS_Currency != null && ITRS_Currency.ICC_CURRENCY_ID != null)
                            {
                                CurrencyCode = ITRS_Currency.ICC_CURRENCY_ID.ToString();
                            }
                            else
                                CurrencyCode = "N.A";
                        }
                        else
                            CurrencyCode = "N.A";
                    }
                    else
                        CurrencyCode = "N.A";
                }
                else
                {
                    PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                    CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                    if (CheckBca != null)
                    {
                        PSW_EEF_MASTER_DETAILS CheckFi = new PSW_EEF_MASTER_DETAILS();
                        CheckFi = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == CheckBca.BCAD_FORM_E_NO).FirstOrDefault();
                        if (CheckFi != null && CheckFi.FDRI_CURRENCY != 0)
                        {
                            PSW_CURRENCY PSW_Currency = new PSW_CURRENCY();
                            PSW_Currency = context.PSW_CURRENCY.Where(m => m.CUR_ID == CheckFi.FDRI_CURRENCY).FirstOrDefault();
                            if (PSW_Currency != null && PSW_Currency.CUR_NAME != null)
                            {
                                PSW_ITRS_CURRENCY ITRS_Currency = new PSW_ITRS_CURRENCY();
                                ITRS_Currency = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_CURRENCY_CODE == PSW_Currency.CUR_NAME).FirstOrDefault();
                                if (ITRS_Currency != null && ITRS_Currency.ICC_CURRENCY_ID != null)
                                {
                                    CurrencyCode = ITRS_Currency.ICC_CURRENCY_ID.ToString();
                                }
                                else
                                    CurrencyCode = "N.A";
                            }
                            else
                                CurrencyCode = "N.A";
                        }
                        else
                            CurrencyCode = "N.A";
                    }
                    else
                        CurrencyCode = "N.A";
                }
            }
            return CurrencyCode;
        }
        public string FreightAmount()
        {
            decimal? FREIGHT_AMOUNT = null;
            string ReturnString = null;
            if (Session["GD_NO"] != null && Session["GD_NO"].ToString() != "null" && Session["GD_NO"].ToString() != "")
            {
                string GD_NO = Session["GD_NO"].ToString();
                FREIGHT_AMOUNT = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDI_GD_NO == GD_NO).Select(m => m.EIGDGI_FRIEGHTUSD).FirstOrDefault();
                if (FREIGHT_AMOUNT != null)
                {
                    ReturnString = FREIGHT_AMOUNT.ToString();
                }
                else
                {
                    FREIGHT_AMOUNT = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_GD_NO == GD_NO).Select(m => m.EFGDGI_FREIGHT_USD).FirstOrDefault();
                    if (FREIGHT_AMOUNT != null)
                    {
                        ReturnString = FREIGHT_AMOUNT.ToString();
                    }
                    else
                        ReturnString = "N.A";
                }
            }
            else
                ReturnString = "N.A";
            return ReturnString;
        }
        public string Insurance()
        {
            decimal? INSURANCE_AMOUNT = null;
            string returnString = "";
            if (Session["GD_NO"] != null && Session["GD_NO"].ToString() != "null" && Session["GD_NO"].ToString() != "")
            {
                string GD_NO = Session["GD_NO"].ToString();
                INSURANCE_AMOUNT = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDI_GD_NO == GD_NO).Select(m => m.EIGDGI_INSURANCE_VALUE_USD).FirstOrDefault();
                if (INSURANCE_AMOUNT != null)
                    returnString = INSURANCE_AMOUNT.ToString();
                else
                {
                    INSURANCE_AMOUNT = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_GD_NO == GD_NO).Select(m => m.EFGDGI_INSURANCE_USD).FirstOrDefault();
                    if (INSURANCE_AMOUNT != null)
                        returnString = INSURANCE_AMOUNT.ToString();
                    else
                        returnString = "N.A";
                }
            }
            else
                returnString = "N.A";
            return returnString;
        }
        public string CompanyName(PSW_ITRS_AC_700 Entity)
        {
            string ReturnString = "";
            if (Session["SCHEDULE_CODE"] != null && Session["SCHEDULE_CODE"].ToString() != "" && Session["SCHEDULE_CODE"].ToString() == "59")
            {
                if (Entity.AC_REFERENCE_NO.Contains("PK1CDXB") || Entity.AC_REFERENCE_NO.Contains("PK1CWOT") || Entity.AC_REFERENCE_NO.Contains("PK1CWOX"))
                {
                    if (Entity.AC_ULTIMATE_BENEFICIARY != null)
                    {
                        if (Entity.AC_ULTIMATE_BENEFICIARY.Contains(","))
                        {
                            ReturnString = Entity.AC_ULTIMATE_BENEFICIARY;
                            ReturnString = ReturnString.Replace(",", "");
                        }
                        else
                            ReturnString = Entity.AC_ULTIMATE_BENEFICIARY;
                    }
                    else
                        ReturnString = "N.A";
                }
                else
                    ReturnString = "N.A";
            }
            else
                ReturnString = "N.A";
            return ReturnString;
        }
        public string GetShareHoldingPercentage(string PaymentDetail)
        {
            string ReturnString = "";
            if (PaymentDetail != null && PaymentDetail.Contains("SP%"))
            {
                var FinalString = PaymentDetail.Split(new[] { "SP%" }, StringSplitOptions.None)[1];
                ReturnString = FinalString.Substring(0, 3);
            }
            else
                ReturnString = "N.A";
            return ReturnString;
        }
        public string GetLoanID(string PaymentDetail)
        {
            string ReturnString = "";
            if (PaymentDetail != null && PaymentDetail.Contains("CITI-"))
            {
                var FinalString = PaymentDetail.Split(new[] { "CITI-" }, StringSplitOptions.None)[1];
                ReturnString = "CITI-" + FinalString.Substring(0, 25);
                //save string value is 30
            }
            else
                ReturnString = "N.A";
            return ReturnString;
        }
        public string GetLenderSector(PSW_ITRS_AC_700 Entity)
        {
            string PayCount = "";
            if (Entity.AC_REFERENCE_NO != null && Entity.AC_REFERENCE_NO != "")
            {
                if (Entity.AC_CURRENCY == "PKR" && Entity.AC_REFERENCE_NO.Contains("PK1SWIF") && Entity.AC_DEBIT_AMOUNT != null)
                {
                    if (Entity.AC_BY_ORDER != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_LENDER_SECTOR> GetCountry = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_IS_AUTH == true && m.LS_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_BY_ORDER.Contains(item.LS_NAME))
                                    {
                                        PayCount = item.LS_LENDER_SECTOR;
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                                PayCount = "N.A";
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else if (Entity.AC_CURRENCY != "PKR" && Entity.AC_DEBIT_AMOUNT != null)
                {
                    if (Entity.AC_BY_ORDER != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_LENDER_SECTOR> GetCountry = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_IS_AUTH == true && m.LS_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_BY_ORDER.Contains(item.LS_NAME))
                                    {
                                        PayCount = item.LS_LENDER_SECTOR;
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                                PayCount = "N.A";
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else if (Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_REFERENCE_NO.Contains("PK1BKOZ"))
                {
                    if (Entity.AC_ACCOUNT_WITH_INSTITUTION != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_LENDER_SECTOR> GetCountry = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_IS_AUTH == true && m.LS_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_ACCOUNT_WITH_INSTITUTION.Contains(item.LS_NAME))
                                    {
                                        PayCount = item.LS_LENDER_SECTOR;
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                                PayCount = "N.A";
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else if (Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null && Entity.AC_REFERENCE_NO.Contains("PK1CDXB") || Entity.AC_REFERENCE_NO.Contains("PK1CWOX"))
                {
                    if (Entity.AC_ULTIMATE_BENEFICIARY != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_LENDER_SECTOR> GetCountry = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_IS_AUTH == true && m.LS_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_ULTIMATE_BENEFICIARY.Contains(item.LS_NAME))
                                    {
                                        PayCount = item.LS_LENDER_SECTOR;
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                                PayCount = "N.A";
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else if (Entity.AC_CURRENCY != "PKR" && Entity.AC_CREDIT_AMOUNT != null)
                {
                    if (Entity.AC_BY_ORDER != null)
                    {
                        bool CheckStatus = false;
                        List<PSW_ITRS_LENDER_SECTOR> GetCountry = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_IS_AUTH == true && m.LS_STATUS == true).ToList();
                        if (GetCountry != null && GetCountry.Count() > 0)
                        {
                            foreach (var item in GetCountry)
                            {
                                if (CheckStatus == false)
                                {
                                    if (Entity.AC_BY_ORDER.Contains(item.LS_NAME))
                                    {
                                        PayCount = item.LS_LENDER_SECTOR;
                                        CheckStatus = true;
                                        break;
                                    }
                                    else
                                        CheckStatus = false;
                                }
                            }
                            if (CheckStatus == false)
                                PayCount = "N.A";
                        }
                    }
                    else
                        PayCount = "N.A";
                }
                else
                    PayCount = "N.A";
            }
            return PayCount;
        }
        public string GetLCDate()
        {
            string ReturnString = "";
            if (Session["SALES_TERM"] != null && Session["SALES_TERM"].ToString() != "")
            {
                if (Session["SALES_TERM"].ToString() == "50" || Session["SALES_TERM"].ToString() == "60")
                {
                    string RefNo = Session["REFERENCE_NO"].ToString();
                    if (RefNo != null && RefNo != "" && RefNo.Length > 20)
                    {
                        RefNo = RefNo.Substring(RefNo.Length - 8);
                        string Date = RefNo.Substring(0, 2);
                        string Month = RefNo.Substring(2, 2);
                        string Year = RefNo.Substring(4, 4);
                        ReturnString = Year + Month + Date;
                    }
                    else
                        ReturnString = "N.A";
                }
                else
                    ReturnString = "N.A";
            }
            else
                ReturnString = "N.A";
            return ReturnString;
        }
        public string GetStatusOfImmediateRemitter()
        {
            string ReturnString = "";
            if (Session["SCHEDULE_CODE"] != null && Session["SCHEDULE_CODE"].ToString() != "")
            {
                if (Session["SCHEDULE_CODE"].ToString() == "73")
                    ReturnString = "1";
                else
                    ReturnString = "N.A";
            }
            else
                ReturnString = "N.A";
            return ReturnString;
        }
        public string FundRealization(string ReferenceNo)
        {
            string FRCode = "";
            if (ReferenceNo != null && ReferenceNo != "")
            {
                PSW_EEF_BCA_LIST CheckBca = new PSW_EEF_BCA_LIST();
                CheckBca = context.PSW_EEF_BCA_LIST.Where(m => m.BCAD_FLEX_REFERENCE.Contains(ReferenceNo)).FirstOrDefault();
                if (CheckBca != null)
                    FRCode = "1";
                else
                    FRCode = "0";
            }
            return FRCode;
        }
        public decimal? GetDollarRate(DateTime? Todate)
        {
            decimal? DollarRate = null;
            if (Todate != null)
                DollarRate = context.PSW_ITRS_USD_RATE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUR_TODAY_DATE) == System.Data.Entity.DbFunctions.TruncateTime(Todate)).Select(m => m.IUR_RATE).FirstOrDefault();
            return DollarRate;
        }
        #endregion  

        public FileStreamResult CreateLogFileAC700(string Message)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = "AC700_" + DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
        public FileStreamResult CreateLogFileFT801(string Message)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = "FT801_" + DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
        public FileStreamResult CreateLogFile(string Message)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
    }
}
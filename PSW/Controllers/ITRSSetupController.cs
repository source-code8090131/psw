﻿using PSW.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PSW.Controllers
{
    public class ITRSSetupController : Controller
    {
        // GET: ITRSSetup
        PSWEntities context = new PSWEntities();
        #region Statement
        public ActionResult Statement(ITRS_STATEMENT_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Statement", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_STATEMENT> List = context.PSW_ITRS_STATEMENT.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.SC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID != SessionUser).OrderByDescending(m => m.SC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_STATEMENT.Where(m => m.SC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Statement(ITRS_STATEMENT_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Statement", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_STATEMENT UpdateEntity = new PSW_ITRS_STATEMENT();

                        PSW_ITRS_STATEMENT CheckIfExist = new PSW_ITRS_STATEMENT();
                        if (db_table.Entity.SC_ID == 0)
                        {
                            UpdateEntity.SC_ID = Convert.ToInt32(context.PSW_ITRS_STATEMENT.Max(m => (decimal?)m.SC_ID)) + 1;
                            UpdateEntity.SC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_STATEMENT.Where(m => m.SC_ID == db_table.Entity.SC_ID).FirstOrDefault();
                            UpdateEntity.SC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.SC_STATEMENT_CODE = db_table.Entity.SC_STATEMENT_CODE;
                        UpdateEntity.SC_CODE = db_table.Entity.SC_CODE;
                        UpdateEntity.SC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.SC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.SC_CHECKER_ID = UpdateEntity.SC_MAKER_ID;
                            UpdateEntity.SC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.SC_CHECKER_ID = null;
                            UpdateEntity.SC_ISAUTH = false;
                        }
                        UpdateEntity.SC_STATUS = db_table.Entity.SC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_STATEMENT.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult StatementFilter(ITRS_STATEMENT_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Statement", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_STATEMENT> List = context.PSW_ITRS_STATEMENT.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.SC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID!= SessionUser).OrderByDescending(m => m.SC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Statement", edit);
        }
        [HttpPost]
        public ActionResult StatementFilter(ITRS_STATEMENT_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Statement", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_STATEMENT> List = context.PSW_ITRS_STATEMENT.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.SC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID != SessionUser).OrderByDescending(m => m.SC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Statement", edit);
        }
        [ChildActionOnly]
        public PartialViewResult StatementView(ITRS_STATEMENT_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_STATEMENT> ViewData = new List<PSW_ITRS_STATEMENT>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_STATEMENT.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.SC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID != SessionUser).OrderByDescending(m => m.SC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_STATEMENT.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID != SessionUser).OrderByDescending(m => m.SC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeStatement(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Statement", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_STATEMENT UpdateEntity = context.PSW_ITRS_STATEMENT.Where(m => m.SC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.SC_ISAUTH = true;
                            UpdateEntity.SC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.SC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Statement : " + UpdateEntity.SC_STATEMENT_CODE + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectStatement(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Statement", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_STATEMENT UpdateEntity = context.PSW_ITRS_STATEMENT.Where(m => m.SC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_STATEMENT.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Statement : " + UpdateEntity.SC_STATEMENT_CODE + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkStatement(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Statement", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_STATEMENT Statement = new PSW_ITRS_STATEMENT();
                                    if (row["Code"].ToString().Length > 0)
                                    {
                                        string Code = row["Code"].ToString();
                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_STATEMENT.Where(m => m.SC_CODE == Code).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_STATEMENT();
                                            Statement.SC_ID = Convert.ToInt32(context.PSW_ITRS_STATEMENT.Max(m => (decimal?)m.SC_ID)) + 1;
                                            Statement.SC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.SC_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Statement Code"] != null || row["Statement Code"].ToString() != "")
                                        {
                                            string statementcode = row["Statement Code"].ToString();
                                            Statement.SC_STATEMENT_CODE = statementcode;
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                string code = row["Code"].ToString();
                                                Statement.SC_CODE = code;
                                                Statement.SC_MAKER_ID = Session["USER_ID"].ToString();
                                                if(Statement.SC_MAKER_ID == "Admin123")
                                                {
                                                    Statement.SC_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.SC_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.SC_CHECKER_ID = null;
                                                    Statement.SC_ISAUTH = false;
                                                }
                                                Statement.SC_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_STATEMENT.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Statement Code : " + row["Statement Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Code : " + row["Code"].ToString() + "  At Statement Code : " + row["Statement Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Client
        public ActionResult Client(ITRS_CLIENTS_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Client", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_CLIENTS> List = context.PSW_ITRS_CLIENTS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IC_ISAUTH == true || m.IC_MAKER_ID != SessionUser).OrderByDescending(m => m.IC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_CLIENTS.Where(m => m.IC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Client(ITRS_CLIENTS_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Client", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_CLIENTS UpdateEntity = new PSW_ITRS_CLIENTS();

                        PSW_ITRS_CLIENTS CheckIfExist = new PSW_ITRS_CLIENTS();
                        if (db_table.Entity.IC_ID == 0)
                        {
                            UpdateEntity.IC_ID = Convert.ToInt32(context.PSW_ITRS_CLIENTS.Max(m => (decimal?)m.IC_ID)) + 1;
                            UpdateEntity.IC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_CLIENTS.Where(m => m.IC_ID == db_table.Entity.IC_ID).FirstOrDefault();
                            UpdateEntity.IC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IC_NAME = db_table.Entity.IC_NAME;
                        UpdateEntity.IC_NTN = db_table.Entity.IC_NTN;
                        UpdateEntity.IC_DEPT = db_table.Entity.IC_DEPT;
                        UpdateEntity.IC_AD = db_table.Entity.IC_AD;
                        UpdateEntity.IC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IC_CHECKER_ID = UpdateEntity.IC_MAKER_ID;
                            UpdateEntity.IC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IC_CHECKER_ID = null;
                            UpdateEntity.IC_ISAUTH = false;
                        }
                        UpdateEntity.IC_STATUS = db_table.Entity.IC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_CLIENTS.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult ClientFilter(ITRS_CLIENTS_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Client", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_CLIENTS> List = context.PSW_ITRS_CLIENTS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IC_ISAUTH == true || m.IC_MAKER_ID != SessionUser).OrderByDescending(m => m.IC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Client", edit);
        }
        [HttpPost]
        public ActionResult ClientFilter(ITRS_CLIENTS_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Client", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_CLIENTS> List = context.PSW_ITRS_CLIENTS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IC_ISAUTH == true || m.IC_MAKER_ID != SessionUser).OrderByDescending(m => m.IC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Client", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ClientView(ITRS_CLIENTS_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_CLIENTS> ViewData = new List<PSW_ITRS_CLIENTS>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_CLIENTS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IC_ISAUTH == true || m.IC_MAKER_ID != SessionUser).OrderByDescending(m => m.IC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_CLIENTS.Where(m => m.IC_ISAUTH == true || m.IC_MAKER_ID != SessionUser).OrderByDescending(m => m.IC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeClient(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Client", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_CLIENTS UpdateEntity = context.PSW_ITRS_CLIENTS.Where(m => m.IC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IC_ISAUTH = true;
                            UpdateEntity.IC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Client : " + UpdateEntity.IC_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectClient(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Client", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_CLIENTS UpdateEntity = context.PSW_ITRS_CLIENTS.Where(m => m.IC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_CLIENTS.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Client : " + UpdateEntity.IC_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkClient(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Client", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_CLIENTS Statement = new PSW_ITRS_CLIENTS();
                                    if (row["CLIENT"].ToString().Length > 0)
                                    {
                                        string ClientName = row["CLIENT"].ToString();
                                        string ClientNTN = row["ntn"].ToString();
                                        string CLientDept = row["dept"].ToString();
                                        string AD = row["AD"].ToString();
                                        int clientAd = 0;
                                        if (AD != null && AD != "")
                                            clientAd = Convert.ToInt32(AD);
                                        else
                                            clientAd = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_CLIENTS.Where(m => m.IC_NAME == ClientName).FirstOrDefault();
                                        int PrimaryKey = 0;
                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_CLIENTS();
                                            Statement.IC_ID = Convert.ToInt32(context.PSW_ITRS_CLIENTS.Max(m => (decimal?)m.IC_ID)) + 1;
                                            Statement.IC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                            if (row["CLIENT"].ToString() != "" && row["CLIENT"] != null)
                                            {
                                                Statement.IC_NAME = ClientName;
                                                if (row["ntn"] != null && row["ntn"].ToString() != "")
                                                {
                                                    Statement.IC_NTN = ClientNTN;
                                                    if (row["dept"] != null || row["dept"].ToString() != "")
                                                    {
                                                        PSW_ITRS_DEPARTMENTS checkdept = new PSW_ITRS_DEPARTMENTS();
                                                        int ClientDepartmentId = 0;
                                                        if (CLientDept != null && CLientDept != "")
                                                        {
                                                            ClientDepartmentId = Convert.ToInt32(CLientDept);
                                                            checkdept = context.PSW_ITRS_DEPARTMENTS.Where(m => m.IDC_DEPARTMENT_ID == ClientDepartmentId).FirstOrDefault();
                                                        }
                                                        if (checkdept != null)
                                                            Statement.IC_DEPT = checkdept.IDC_DEPARTMENT_ID.ToString();
                                                        else
                                                            Statement.IC_DEPT = null;
                                                        if (row["AD"] != null && row["AD"].ToString() != "")
                                                        {
                                                            Statement.IC_AD = clientAd;
                                                            Statement.IC_MAKER_ID = Session["USER_ID"].ToString();
                                                            if (Statement.IC_MAKER_ID == "Admin123")
                                                            {
                                                                Statement.IC_CHECKER_ID = Session["USER_ID"].ToString();
                                                                Statement.IC_ISAUTH = true;
                                                            }
                                                            else
                                                            {
                                                                Statement.IC_CHECKER_ID = null;
                                                                Statement.IC_ISAUTH = false;
                                                            }
                                                            Statement.IC_STATUS = true;
                                                            if (CheckIfExist == false)
                                                            {
                                                                context.PSW_ITRS_CLIENTS.Add(Statement);
                                                            }
                                                            else
                                                            {
                                                                context.Entry(Statement).State = EntityState.Modified;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid AD : " + row["AD"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid Department : " + row["dept"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid NTN : " + row["ntn"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            //PrimaryKey = Statement.IC_ID;
                                            //if (Statement.IC_ID != PrimaryKey && Statement.IC_NAME != ClientName && Statement.IC_NTN != ClientNTN && Statement.IC_AD != Convert.ToInt32(AD))
                                            //{
                                                Statement.IC_EDIT_DATETIME = DateTime.Now;
                                                CheckIfExist = true;

                                                if (row["CLIENT"].ToString() != "" && row["CLIENT"] != null)
                                                {
                                                    Statement.IC_NAME = ClientName;
                                                    if (row["ntn"] != null || row["ntn"].ToString() != "")
                                                    {
                                                        Statement.IC_NTN = ClientNTN;
                                                        if (row["dept"] != null || row["dept"].ToString() != "")
                                                        {
                                                            PSW_ITRS_DEPARTMENTS checkdept = new PSW_ITRS_DEPARTMENTS();
                                                            int ClientDepartmentId = 0;
                                                            if (CLientDept != null && CLientDept != "")
                                                            {
                                                                ClientDepartmentId = Convert.ToInt32(CLientDept);
                                                                checkdept = context.PSW_ITRS_DEPARTMENTS.Where(m => m.IDC_DEPARTMENT_ID == ClientDepartmentId).FirstOrDefault();
                                                            }
                                                            if (checkdept != null)
                                                                Statement.IC_DEPT = checkdept.IDC_DEPARTMENT_ID.ToString();
                                                            else
                                                                Statement.IC_DEPT = null;
                                                            if (row["AD"] != null || row["AD"].ToString() != "")
                                                            {
                                                                Statement.IC_AD = clientAd;
                                                                Statement.IC_MAKER_ID = Session["USER_ID"].ToString();
                                                                if (Statement.IC_MAKER_ID == "Admin123")
                                                                {
                                                                    Statement.IC_CHECKER_ID = Session["USER_ID"].ToString();
                                                                    Statement.IC_ISAUTH = true;
                                                                }
                                                                else
                                                                {
                                                                    Statement.IC_CHECKER_ID = null;
                                                                    Statement.IC_ISAUTH = false;
                                                                }
                                                                Statement.IC_STATUS = true;
                                                                if (CheckIfExist == false)
                                                                {
                                                                    context.PSW_ITRS_CLIENTS.Add(Statement);
                                                                }
                                                                else
                                                                {
                                                                    context.Entry(Statement).State = EntityState.Modified;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid Department : " + row["dept"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Department : " + row["dept"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid NTN : " + row["ntn"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                }
                                            //}
                                            //else
                                            //    message += "Client Name : " + ClientName + ", NTN : " + ClientNTN + " and AD : " + AD + " is already exists." + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult AuthorizeBullkITRSClient(/*DateTime? FromDate, DateTime? ToDate*/)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Client", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    string SessionUser = Session["USER_ID"].ToString();
                    try
                    {
                        List<PSW_ITRS_CLIENTS> BulkData = new List<PSW_ITRS_CLIENTS>();
                        BulkData = context.PSW_ITRS_CLIENTS.Where(m => m.IC_ISAUTH == false && m.IC_MAKER_ID != SessionUser).OrderByDescending(m => m.IC_ID).ToList();
                        if (BulkData.Count() > 0)
                        {
                            foreach (var item in BulkData)
                            {
                                PSW_ITRS_CLIENTS UpdateEntity = context.PSW_ITRS_CLIENTS.Where(m => m.IC_ID == item.IC_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.IC_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.IC_EDIT_DATETIME = DateTime.Now;
                                    UpdateEntity.IC_ISAUTH = true;
                                    context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                                return Json("Record Authorized successfully");
                        }
                        else
                            message = "Unable to fetch data against the User Id : " + SessionUser;
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Currency
        public ActionResult Currency(ITRS_CURRENCY_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Currency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_CURRENCY> List = context.PSW_ITRS_CURRENCY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ICC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ICC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.ICC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.ICC_ISAUTH == true || m.ICC_MAKER_ID != SessionUser).OrderByDescending(m => m.ICC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Currency(ITRS_CURRENCY_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Currency", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_CURRENCY UpdateEntity = new PSW_ITRS_CURRENCY();

                        PSW_ITRS_CURRENCY CheckIfExist = new PSW_ITRS_CURRENCY();
                        if (db_table.Entity.ICC_ID == 0)
                        {
                            UpdateEntity.ICC_ID = Convert.ToInt32(context.PSW_ITRS_CURRENCY.Max(m => (decimal?)m.ICC_ID)) + 1;
                            UpdateEntity.ICC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_ID == db_table.Entity.ICC_ID).FirstOrDefault();
                            UpdateEntity.ICC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.ICC_CURRENCY_CODE = db_table.Entity.ICC_CURRENCY_CODE;
                        UpdateEntity.ICC_CURRENCY_ID = db_table.Entity.ICC_CURRENCY_ID;
                        UpdateEntity.ICC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.ICC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.ICC_CHECKER_ID = UpdateEntity.ICC_MAKER_ID;
                            UpdateEntity.ICC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.ICC_CHECKER_ID = null;
                            UpdateEntity.ICC_ISAUTH = false;
                        }
                        UpdateEntity.ICC_STATUS = db_table.Entity.ICC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_CURRENCY.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult CurrencyFilter(ITRS_CURRENCY_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Currency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_CURRENCY> List = context.PSW_ITRS_CURRENCY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ICC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ICC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.ICC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.ICC_ISAUTH == true || m.ICC_MAKER_ID != SessionUser).OrderByDescending(m => m.ICC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Currency", edit);
        }
        [HttpPost]
        public ActionResult CurrencyFilter(ITRS_CURRENCY_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Currency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_CURRENCY> List = context.PSW_ITRS_CURRENCY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ICC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ICC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.ICC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.ICC_ISAUTH == true || m.ICC_MAKER_ID != SessionUser).OrderByDescending(m => m.ICC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Currency", edit);
        }
        [ChildActionOnly]
        public PartialViewResult CurrencyView(ITRS_CURRENCY_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_CURRENCY> ViewData = new List<PSW_ITRS_CURRENCY>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_CURRENCY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ICC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ICC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.ICC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.ICC_ISAUTH == true || m.ICC_MAKER_ID != SessionUser).OrderByDescending(m => m.ICC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_ISAUTH == true || m.ICC_MAKER_ID != SessionUser).OrderByDescending(m => m.ICC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeCurrency(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Currency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_CURRENCY UpdateEntity = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.ICC_ISAUTH = true;
                            UpdateEntity.ICC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.ICC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Currency Code : " + UpdateEntity.ICC_CURRENCY_CODE + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectCurrency(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Currency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_CURRENCY UpdateEntity = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_CURRENCY.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Currency Code : " + UpdateEntity.ICC_CURRENCY_CODE + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkCurrency(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Currency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_CURRENCY Statement = new PSW_ITRS_CURRENCY();
                                    if (row["Code"].ToString().Length > 0)
                                    {
                                        string Code = row["Code"].ToString();
                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_CURRENCY.Where(m => m.ICC_CURRENCY_CODE == Code).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_CURRENCY();
                                            Statement.ICC_ID = Convert.ToInt32(context.PSW_ITRS_CURRENCY.Max(m => (decimal?)m.ICC_ID)) + 1;
                                            Statement.ICC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.ICC_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["CurrencyID"] != null || row["CurrencyID"].ToString() != "")
                                        {
                                            string statementcode = row["CurrencyID"].ToString();
                                            if (statementcode != null && statementcode != "")
                                                Statement.ICC_CURRENCY_ID = Convert.ToInt32(statementcode);
                                            else
                                                Statement.ICC_CURRENCY_ID = 0;
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                string code = row["Code"].ToString();
                                                Statement.ICC_CURRENCY_CODE = code;
                                                Statement.ICC_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.ICC_MAKER_ID == "Admin123")
                                                {
                                                    Statement.ICC_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.ICC_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.ICC_CHECKER_ID = null;
                                                    Statement.ICC_ISAUTH = false;
                                                }
                                                Statement.ICC_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_CURRENCY.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Currency Id : " + row["CurrencyID"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Currency Code : " + row["Code"].ToString() + "  At Currency Id : " + row["CurrencyID"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Schedule Code
        public ActionResult ScheduleCode(ITRS_SCHEDULE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ScheduleCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_SCHEDULE_CODE> List = context.PSW_ITRS_SCHEDULE_CODE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.SC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID != SessionUser).OrderByDescending(m => m.SC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_SCHEDULE_CODE.Where(m => m.SC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ScheduleCode(ITRS_SCHEDULE_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ScheduleCode", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_SCHEDULE_CODE UpdateEntity = new PSW_ITRS_SCHEDULE_CODE();

                        PSW_ITRS_SCHEDULE_CODE CheckIfExist = new PSW_ITRS_SCHEDULE_CODE();
                        if (db_table.Entity.SC_ID == 0)
                        {
                            UpdateEntity.SC_ID = Convert.ToInt32(context.PSW_ITRS_SCHEDULE_CODE.Max(m => (decimal?)m.SC_ID)) + 1;
                            UpdateEntity.SC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_SCHEDULE_CODE.Where(m => m.SC_ID == db_table.Entity.SC_ID).FirstOrDefault();
                            UpdateEntity.SC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.SC_SCHEDULE_NAME = db_table.Entity.SC_SCHEDULE_NAME;
                        UpdateEntity.SC_SCHEDULE_CODE = db_table.Entity.SC_SCHEDULE_CODE;
                        UpdateEntity.SC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.SC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.SC_CHECKER_ID = UpdateEntity.SC_MAKER_ID;
                            UpdateEntity.SC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.SC_CHECKER_ID = null;
                            UpdateEntity.SC_ISAUTH = false;
                        }
                        UpdateEntity.SC_STATUS = db_table.Entity.SC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_SCHEDULE_CODE.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult ScheduleCodeFilter(ITRS_SCHEDULE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ScheduleCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_SCHEDULE_CODE> List = context.PSW_ITRS_SCHEDULE_CODE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.SC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID != SessionUser).OrderByDescending(m => m.SC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ScheduleCode", edit);
        }
        [HttpPost]
        public ActionResult ScheduleCodeFilter(ITRS_SCHEDULE_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ScheduleCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_SCHEDULE_CODE> List = context.PSW_ITRS_SCHEDULE_CODE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.SC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID != SessionUser).OrderByDescending(m => m.SC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ScheduleCode", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ScheduleCodeView(ITRS_SCHEDULE_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_SCHEDULE_CODE> ViewData = new List<PSW_ITRS_SCHEDULE_CODE>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_SCHEDULE_CODE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.SC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.SC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID != SessionUser).OrderByDescending(m => m.SC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_SCHEDULE_CODE.Where(m => m.SC_ISAUTH == true || m.SC_MAKER_ID != SessionUser).OrderByDescending(m => m.SC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeScheduleCode(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ScheduleCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_SCHEDULE_CODE UpdateEntity = context.PSW_ITRS_SCHEDULE_CODE.Where(m => m.SC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.SC_ISAUTH = true;
                            UpdateEntity.SC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.SC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Schedule Name : " + UpdateEntity.SC_SCHEDULE_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectScheduleCode(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ScheduleCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_SCHEDULE_CODE UpdateEntity = context.PSW_ITRS_SCHEDULE_CODE.Where(m => m.SC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_SCHEDULE_CODE.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Schedule Name : " + UpdateEntity.SC_SCHEDULE_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkScheduleCode(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ScheduleCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_SCHEDULE_CODE Statement = new PSW_ITRS_SCHEDULE_CODE();
                                    if (row["Name of Schedule"].ToString().Length > 0)
                                    {
                                        string Schedulename = row["Name of Schedule"].ToString();
                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_SCHEDULE_CODE.Where(m => m.SC_SCHEDULE_NAME == Schedulename).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_SCHEDULE_CODE();
                                            Statement.SC_ID = Convert.ToInt32(context.PSW_ITRS_SCHEDULE_CODE.Max(m => (decimal?)m.SC_ID)) + 1;
                                            Statement.SC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.SC_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Name of Schedule"] != null || row["Name of Schedule"].ToString() != "")
                                        {
                                            string scheulecode = row["Code"].ToString();
                                            if (scheulecode != null && scheulecode != "")
                                                Statement.SC_SCHEDULE_CODE = Convert.ToInt32(scheulecode);
                                            else
                                                Statement.SC_SCHEDULE_CODE = 0;
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                Statement.SC_SCHEDULE_NAME = Schedulename;
                                                Statement.SC_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.SC_MAKER_ID == "Admin123")
                                                {
                                                    Statement.SC_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.SC_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.SC_CHECKER_ID = null;
                                                    Statement.SC_ISAUTH = false;
                                                }
                                                Statement.SC_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_SCHEDULE_CODE.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Schedule Name : " + row["Name of Schedule"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Schedule Code : " + row["Code"].ToString() + "  At Schedule Name : " + row["Name of Schedule"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Country
        public ActionResult Country(ITRS_COUNTRY_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Country", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_COUNTRY> List = context.PSW_ITRS_COUNTRY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CUN_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CUN_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.CUN_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.CUN_ISAUTH == true || m.CUN_MAKER_ID != SessionUser).OrderByDescending(m => m.CUN_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Country(ITRS_COUNTRY_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Country", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_COUNTRY UpdateEntity = new PSW_ITRS_COUNTRY();

                        PSW_ITRS_COUNTRY CheckIfExist = new PSW_ITRS_COUNTRY();
                        if (db_table.Entity.CUN_ID == 0)
                        {
                            UpdateEntity.CUN_ID = Convert.ToInt32(context.PSW_ITRS_COUNTRY.Max(m => (decimal?)m.CUN_ID)) + 1;
                            UpdateEntity.CUN_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ID == db_table.Entity.CUN_ID).FirstOrDefault();
                            UpdateEntity.CUN_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.CUN_COUNTRY_ID = db_table.Entity.CUN_COUNTRY_ID;
                        UpdateEntity.CUN_COUNTRY_NAME = db_table.Entity.CUN_COUNTRY_NAME;
                        UpdateEntity.CUN_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.CUN_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.CUN_CHECKER_ID = UpdateEntity.CUN_MAKER_ID;
                            UpdateEntity.CUN_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.CUN_CHECKER_ID = null;
                            UpdateEntity.CUN_ISAUTH = false;
                        }
                        UpdateEntity.CUN_STATUS = db_table.Entity.CUN_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_COUNTRY.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult CountryFilter(ITRS_COUNTRY_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Country", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_COUNTRY> List = context.PSW_ITRS_COUNTRY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CUN_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CUN_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.CUN_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.CUN_ISAUTH == true || m.CUN_MAKER_ID != SessionUser).OrderByDescending(m => m.CUN_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Country", edit);
        }
        [HttpPost]
        public ActionResult CountryFilter(ITRS_COUNTRY_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Country", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_COUNTRY> List = context.PSW_ITRS_COUNTRY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CUN_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CUN_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.CUN_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.CUN_ISAUTH == true || m.CUN_MAKER_ID != SessionUser).OrderByDescending(m => m.CUN_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Country", edit);
        }
        [ChildActionOnly]
        public PartialViewResult CountryView(ITRS_COUNTRY_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_COUNTRY> ViewData = new List<PSW_ITRS_COUNTRY>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_COUNTRY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CUN_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CUN_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.CUN_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.CUN_ISAUTH == true || m.CUN_MAKER_ID != SessionUser).OrderByDescending(m => m.CUN_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ISAUTH == true || m.CUN_MAKER_ID != SessionUser).OrderByDescending(m => m.CUN_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeCountry(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Country", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_COUNTRY UpdateEntity = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.CUN_ISAUTH = true;
                            UpdateEntity.CUN_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.CUN_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Country : " + UpdateEntity.CUN_COUNTRY_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectCountry(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Country", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_COUNTRY UpdateEntity = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_COUNTRY.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Country : " + UpdateEntity.CUN_COUNTRY_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkCountry(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Country", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_COUNTRY Statement = new PSW_ITRS_COUNTRY();
                                    if (row["Name"].ToString().Length > 0)
                                    {
                                        string countryname = row["Name"].ToString();
                                        string countryid = row["CountryID"].ToString();
                                        int CountId = 0;
                                        if (countryid != null && countryid != "")
                                            CountId = Convert.ToInt32(countryid);
                                        else
                                            CountId = 0;
                                        
                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_COUNTRY_NAME == countryname).FirstOrDefault();
                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_COUNTRY();
                                            Statement.CUN_ID = Convert.ToInt32(context.PSW_ITRS_COUNTRY.Max(m => (decimal?)m.CUN_ID)) + 1;
                                            Statement.CUN_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.CUN_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Name"] != null || row["Name"].ToString() != "")
                                        {
                                            if (row["CountryID"].ToString() != "" && row["CountryID"] != null)
                                            {
                                                Statement.CUN_COUNTRY_ID = CountId;
                                                Statement.CUN_COUNTRY_NAME = countryname;
                                                Statement.CUN_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.CUN_MAKER_ID == "Admin123")
                                                {
                                                    Statement.CUN_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.CUN_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.CUN_CHECKER_ID = null;
                                                    Statement.CUN_ISAUTH = false;
                                                }
                                                Statement.CUN_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_COUNTRY.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Country Id : " + row["CountryID"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Country Name : " + row["Name"].ToString() + "  At Country Id : " + row["CountryID"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        public JsonResult AuthorizeBullkITRSCountry(/*DateTime? FromDate, DateTime? ToDate*/)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Country", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    string SessionUser = Session["USER_ID"].ToString();
                    try
                    {
                        List<PSW_ITRS_COUNTRY> BulkData = new List<PSW_ITRS_COUNTRY>();
                        BulkData = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ISAUTH == false && m.CUN_MAKER_ID != SessionUser).OrderByDescending(m => m.CUN_ID).ToList();
                        if (BulkData.Count() > 0)
                        {
                            foreach (var item in BulkData)
                            {
                                PSW_ITRS_COUNTRY UpdateEntity = context.PSW_ITRS_COUNTRY.Where(m => m.CUN_ID == item.CUN_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.CUN_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.CUN_EDIT_DATETIME = DateTime.Now;
                                    UpdateEntity.CUN_ISAUTH = true;
                                    context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                                return Json("Record Authorized successfully");
                        }
                        else
                            message = "Unable to fetch data against the User Id : " + SessionUser;
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Approval Of Remittance
        public ActionResult ApprovalOfRemittance(ITRS_REMITTANCE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ApprovalOfRemittance", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_APPROVAL_OF_REMITTANCE> List = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AOR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AOR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AOR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AOR_ISAUTH == true || m.AOR_MAKER_ID != SessionUser).OrderByDescending(m => m.AOR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => m.AOR_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ApprovalOfRemittance(ITRS_REMITTANCE_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ApprovalOfRemittance", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_APPROVAL_OF_REMITTANCE UpdateEntity = new PSW_ITRS_APPROVAL_OF_REMITTANCE();

                        PSW_ITRS_APPROVAL_OF_REMITTANCE CheckIfExist = new PSW_ITRS_APPROVAL_OF_REMITTANCE();
                        if (db_table.Entity.AOR_ID == 0)
                        {
                            UpdateEntity.AOR_ID = Convert.ToInt32(context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Max(m => (decimal?)m.AOR_ID)) + 1;
                            UpdateEntity.AOR_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => m.AOR_ID == db_table.Entity.AOR_ID).FirstOrDefault();
                            UpdateEntity.AOR_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.AOR_REMITTANCE_NAME = db_table.Entity.AOR_REMITTANCE_NAME;
                        UpdateEntity.AOR_REMITTANCE_CODE = db_table.Entity.AOR_REMITTANCE_CODE;
                        UpdateEntity.AOR_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.AOR_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.AOR_CHECKER_ID = UpdateEntity.AOR_MAKER_ID;
                            UpdateEntity.AOR_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.AOR_CHECKER_ID = null;
                            UpdateEntity.AOR_ISAUTH = false;
                        }
                        UpdateEntity.AOR_STATUS = db_table.Entity.AOR_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult ApprovalOfRemittanceFilter(ITRS_REMITTANCE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ApprovalOfRemittance", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_APPROVAL_OF_REMITTANCE> List = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AOR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AOR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AOR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AOR_ISAUTH == true || m.AOR_MAKER_ID != SessionUser).OrderByDescending(m => m.AOR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ApprovalOfRemittance", edit);
        }
        [HttpPost]
        public ActionResult ApprovalOfRemittanceFilter(ITRS_REMITTANCE_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ApprovalOfRemittance", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_APPROVAL_OF_REMITTANCE> List = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AOR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AOR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AOR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AOR_ISAUTH == true || m.AOR_MAKER_ID != SessionUser).OrderByDescending(m => m.AOR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ApprovalOfRemittance", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ApprovalOfRemittanceView(ITRS_REMITTANCE_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_APPROVAL_OF_REMITTANCE> ViewData = new List<PSW_ITRS_APPROVAL_OF_REMITTANCE>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AOR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AOR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.AOR_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.AOR_ISAUTH == true || m.AOR_MAKER_ID != SessionUser).OrderByDescending(m => m.AOR_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => m.AOR_ISAUTH == true || m.AOR_MAKER_ID != SessionUser).OrderByDescending(m => m.AOR_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeApprovalOfRemittance(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ApprovalOfRemittance", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_APPROVAL_OF_REMITTANCE UpdateEntity = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => m.AOR_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.AOR_ISAUTH = true;
                            UpdateEntity.AOR_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.AOR_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Remittance : " + UpdateEntity.AOR_REMITTANCE_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectApprovalOfRemittance(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ApprovalOfRemittance", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_APPROVAL_OF_REMITTANCE UpdateEntity = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => m.AOR_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Remittance : " + UpdateEntity.AOR_REMITTANCE_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkApprovalOfRemittance(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ApprovalOfRemittance", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_APPROVAL_OF_REMITTANCE Statement = new PSW_ITRS_APPROVAL_OF_REMITTANCE();
                                    if (row["Approval of Remittance"].ToString().Length > 0)
                                    {
                                        string remittername = row["Approval of Remittance"].ToString();
                                        string remittercode = row["Code"].ToString();
                                        int CountId = 0;
                                        if (remittercode != null && remittercode != "")
                                            CountId = Convert.ToInt32(remittercode);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Where(m => m.AOR_REMITTANCE_NAME == remittername).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_APPROVAL_OF_REMITTANCE();
                                            Statement.AOR_ID = Convert.ToInt32(context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Max(m => (decimal?)m.AOR_ID)) + 1;
                                            Statement.AOR_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.AOR_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Approval of Remittance"] != null || row["Approval of Remittance"].ToString() != "")
                                        {
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                Statement.AOR_REMITTANCE_NAME = remittername;
                                                Statement.AOR_REMITTANCE_CODE = CountId;
                                                Statement.AOR_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.AOR_MAKER_ID == "Admin123")
                                                {
                                                    Statement.AOR_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.AOR_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.AOR_CHECKER_ID = null;
                                                    Statement.AOR_ISAUTH = false;
                                                }
                                                Statement.AOR_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_APPROVAL_OF_REMITTANCE.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Code : " + row["Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Remittance : " + row["Approval of Remittance"].ToString() + "  At Remittance Code : " + row["Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Relationship
        public ActionResult Relationship(ITRS_RELATIONSHIP_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Relationship", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_RELATION> List = context.PSW_ITRS_RELATION.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IRS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IRS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IRS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IRS_ISAUTH == true || m.IRS_MAKER_ID != SessionUser).OrderByDescending(m => m.IRS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_RELATION.Where(m => m.IRS_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Relationship(ITRS_RELATIONSHIP_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Relationship", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_RELATION UpdateEntity = new PSW_ITRS_RELATION();

                        PSW_ITRS_RELATION CheckIfExist = new PSW_ITRS_RELATION();
                        if (db_table.Entity.IRS_ID == 0)
                        {
                            UpdateEntity.IRS_ID = Convert.ToInt32(context.PSW_ITRS_RELATION.Max(m => (decimal?)m.IRS_ID)) + 1;
                            UpdateEntity.IRS_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_RELATION.Where(m => m.IRS_ID == db_table.Entity.IRS_ID).FirstOrDefault();
                            UpdateEntity.IRS_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IRS_RELATION_NAME = db_table.Entity.IRS_RELATION_NAME;
                        UpdateEntity.IRS_RELATION_CODE = db_table.Entity.IRS_RELATION_CODE;
                        UpdateEntity.IRS_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IRS_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IRS_CHECKER_ID = UpdateEntity.IRS_MAKER_ID;
                            UpdateEntity.IRS_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IRS_CHECKER_ID = null;
                            UpdateEntity.IRS_ISAUTH = false;
                        }
                        UpdateEntity.IRS_STATUS = db_table.Entity.IRS_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_RELATION.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult RelationshipFilter(ITRS_RELATIONSHIP_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Relationship", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_RELATION> List = context.PSW_ITRS_RELATION.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IRS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IRS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IRS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IRS_ISAUTH == true || m.IRS_MAKER_ID != SessionUser).OrderByDescending(m => m.IRS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Relationship", edit);
        }
        [HttpPost]
        public ActionResult RelationshipFilter(ITRS_RELATIONSHIP_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Relationship", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_RELATION> List = context.PSW_ITRS_RELATION.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IRS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IRS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IRS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IRS_ISAUTH == true || m.IRS_MAKER_ID != SessionUser).OrderByDescending(m => m.IRS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Relationship", edit);
        }
        [ChildActionOnly]
        public PartialViewResult RelationshipView(ITRS_RELATIONSHIP_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_RELATION> ViewData = new List<PSW_ITRS_RELATION>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_RELATION.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IRS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IRS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IRS_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IRS_ISAUTH == true || m.IRS_MAKER_ID != SessionUser).OrderByDescending(m => m.IRS_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_RELATION.Where(m => m.IRS_ISAUTH == true || m.IRS_MAKER_ID != SessionUser).OrderByDescending(m => m.IRS_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeRelationship(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Relationship", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_RELATION UpdateEntity = context.PSW_ITRS_RELATION.Where(m => m.IRS_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IRS_ISAUTH = true;
                            UpdateEntity.IRS_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IRS_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = UpdateEntity.IRS_RELATION_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectRelationship(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Relationship", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_RELATION UpdateEntity = context.PSW_ITRS_RELATION.Where(m => m.IRS_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_RELATION.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = UpdateEntity.IRS_RELATION_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkRelationship(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Relationship", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_RELATION Statement = new PSW_ITRS_RELATION();
                                    if (row["Relationship"].ToString().Length > 0)
                                    {
                                        string relationname = row["Relationship"].ToString();
                                        string code = row["Code"].ToString();
                                        int CountId = 0;
                                        if (code != null && code != "")
                                            CountId = Convert.ToInt32(code);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_RELATION.Where(m => m.IRS_RELATION_CODE == CountId).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_RELATION();
                                            Statement.IRS_ID = Convert.ToInt32(context.PSW_ITRS_RELATION.Max(m => (decimal?)m.IRS_ID)) + 1;
                                            Statement.IRS_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.IRS_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Relationship"] != null || row["Relationship"].ToString() != "")
                                        {
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                Statement.IRS_RELATION_NAME = relationname;
                                                Statement.IRS_RELATION_CODE = CountId;
                                                Statement.IRS_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.IRS_MAKER_ID == "Admin123")
                                                {
                                                    Statement.IRS_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.IRS_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.IRS_CHECKER_ID = null;
                                                    Statement.IRS_ISAUTH = false;
                                                }
                                                Statement.IRS_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_RELATION.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Code : " + row["Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Relation : " + row["Relationship"].ToString() + "  At Code : " + row["Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Scheme
        public ActionResult Scheme(ITRS_SCHEME_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Scheme", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_SCHEME> List = context.PSW_ITRS_SCHEME.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ISC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ISC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.ISC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.ISC_ISAUTH == true || m.ISC_MAKER_ID != SessionUser).OrderByDescending(m => m.ISC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_SCHEME.Where(m => m.ISC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Scheme(ITRS_SCHEME_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Scheme", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_SCHEME UpdateEntity = new PSW_ITRS_SCHEME();

                        PSW_ITRS_SCHEME CheckIfExist = new PSW_ITRS_SCHEME();
                        if (db_table.Entity.ISC_ID == 0)
                        {
                            UpdateEntity.ISC_ID = Convert.ToInt32(context.PSW_ITRS_SCHEME.Max(m => (decimal?)m.ISC_ID)) + 1;
                            UpdateEntity.ISC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_SCHEME.Where(m => m.ISC_ID == db_table.Entity.ISC_ID).FirstOrDefault();
                            UpdateEntity.ISC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.ISC_SCHEME_NAME = db_table.Entity.ISC_SCHEME_NAME;
                        UpdateEntity.ISC_SCHEME_CODE = db_table.Entity.ISC_SCHEME_CODE;
                        UpdateEntity.ISC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.ISC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.ISC_CHECKER_ID = UpdateEntity.ISC_MAKER_ID;
                            UpdateEntity.ISC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.ISC_CHECKER_ID = null;
                            UpdateEntity.ISC_ISAUTH = false;
                        }
                        UpdateEntity.ISC_STATUS = db_table.Entity.ISC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_SCHEME.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult SchemeFilter(ITRS_SCHEME_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Scheme", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_SCHEME> List = context.PSW_ITRS_SCHEME.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ISC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ISC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.ISC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.ISC_ISAUTH == true || m.ISC_MAKER_ID != SessionUser).OrderByDescending(m => m.ISC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Scheme", edit);
        }
        [HttpPost]
        public ActionResult SchemeFilter(ITRS_SCHEME_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Scheme", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_SCHEME> List = context.PSW_ITRS_SCHEME.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ISC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ISC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.ISC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.ISC_ISAUTH == true || m.ISC_MAKER_ID != SessionUser).OrderByDescending(m => m.ISC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Scheme", edit);
        }
        [ChildActionOnly]
        public PartialViewResult SchemeView(ITRS_SCHEME_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_SCHEME> ViewData = new List<PSW_ITRS_SCHEME>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_SCHEME.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ISC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ISC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.ISC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.ISC_ISAUTH == true || m.ISC_MAKER_ID != SessionUser).OrderByDescending(m => m.ISC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_SCHEME.Where(m => m.ISC_ISAUTH == true || m.ISC_MAKER_ID != SessionUser).OrderByDescending(m => m.ISC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeScheme(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Scheme", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_SCHEME UpdateEntity = context.PSW_ITRS_SCHEME.Where(m => m.ISC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.ISC_ISAUTH = true;
                            UpdateEntity.ISC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.ISC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Scheme : " + UpdateEntity.ISC_SCHEME_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectScheme(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Scheme", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_SCHEME UpdateEntity = context.PSW_ITRS_SCHEME.Where(m => m.ISC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_SCHEME.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Scheme : " + UpdateEntity.ISC_SCHEME_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkScheme(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Scheme", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_SCHEME Statement = new PSW_ITRS_SCHEME();
                                    if (row["Scheme"].ToString().Length > 0)
                                    {
                                        string schemename = row["Scheme"].ToString();
                                        string code = row["Code"].ToString();
                                        int CountId = 0;
                                        if (code != null && code != "")
                                            CountId = Convert.ToInt32(code);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_SCHEME.Where(m => m.ISC_SCHEME_NAME == schemename).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_SCHEME();
                                            Statement.ISC_ID = Convert.ToInt32(context.PSW_ITRS_SCHEME.Max(m => (decimal?)m.ISC_ID)) + 1;
                                            Statement.ISC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.ISC_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Scheme"] != null || row["Scheme"].ToString() != "")
                                        {
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                Statement.ISC_SCHEME_NAME = schemename;
                                                Statement.ISC_SCHEME_CODE = CountId;
                                                Statement.ISC_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.ISC_MAKER_ID == "Admin123")
                                                {
                                                    Statement.ISC_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.ISC_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.ISC_CHECKER_ID = null;
                                                    Statement.ISC_ISAUTH = false;
                                                }
                                                Statement.ISC_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_SCHEME.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Code : " + row["Code"].ToString() + " Provided at Scheme " + row["Scheme"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Scheme : " + row["Scheme"].ToString() + "  At Code : " + row["Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        public JsonResult AuthorizeBullkITRSScheme(/*DateTime? FromDate, DateTime? ToDate*/)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Scheme", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    string SessionUser = Session["USER_ID"].ToString();
                    try
                    {
                        List<PSW_ITRS_SCHEME> BulkData = new List<PSW_ITRS_SCHEME>();
                        BulkData = context.PSW_ITRS_SCHEME.Where(m => m.ISC_ISAUTH == false && m.ISC_MAKER_ID != SessionUser).OrderByDescending(m => m.ISC_ID).ToList();
                        if (BulkData.Count() > 0)
                        {
                            foreach (var item in BulkData)
                            {
                                PSW_ITRS_SCHEME UpdateEntity = context.PSW_ITRS_SCHEME.Where(m => m.ISC_ID == item.ISC_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.ISC_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.ISC_EDIT_DATETIME = DateTime.Now;
                                    UpdateEntity.ISC_ISAUTH = true;
                                    context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                                return Json("Record Authorized successfully");
                        }
                        else
                            message = "Unable to fetch data against the User Id : " + SessionUser;
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region SalesTerm
        public ActionResult SalesTerm(ITRS_SALES_TERM_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "SalesTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_SALES_TERM> List = context.PSW_ITRS_SALES_TERM.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IST_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IST_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IST_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IST_ISAUTH == true || m.IST_MAKER_ID != SessionUser).OrderByDescending(m => m.IST_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_SALES_TERM.Where(m => m.IST_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult SalesTerm(ITRS_SALES_TERM_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "SalesTerm", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_SALES_TERM UpdateEntity = new PSW_ITRS_SALES_TERM();

                        PSW_ITRS_SALES_TERM CheckIfExist = new PSW_ITRS_SALES_TERM();
                        if (db_table.Entity.IST_ID == 0)
                        {
                            UpdateEntity.IST_ID = Convert.ToInt32(context.PSW_ITRS_SALES_TERM.Max(m => (decimal?)m.IST_ID)) + 1;
                            UpdateEntity.IST_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_SALES_TERM.Where(m => m.IST_ID == db_table.Entity.IST_ID).FirstOrDefault();
                            UpdateEntity.IST_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IST_SALES_TERM_NAME = db_table.Entity.IST_SALES_TERM_NAME;
                        UpdateEntity.IST_SALES_TERM_CODE = db_table.Entity.IST_SALES_TERM_CODE;
                        UpdateEntity.IST_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IST_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IST_CHECKER_ID = UpdateEntity.IST_MAKER_ID;
                            UpdateEntity.IST_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IST_CHECKER_ID = null;
                            UpdateEntity.IST_ISAUTH = false;
                        }
                        UpdateEntity.IST_STATUS = db_table.Entity.IST_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_SALES_TERM.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult SalesTermFilter(ITRS_SALES_TERM_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "SalesTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_SALES_TERM> List = context.PSW_ITRS_SALES_TERM.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IST_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IST_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IST_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IST_ISAUTH == true || m.IST_MAKER_ID != SessionUser).OrderByDescending(m => m.IST_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("SalesTerm", edit);
        }
        [HttpPost]
        public ActionResult SalesTermFilter(ITRS_SALES_TERM_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "SalesTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_SALES_TERM> List = context.PSW_ITRS_SALES_TERM.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IST_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IST_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IST_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IST_ISAUTH == true || m.IST_MAKER_ID != SessionUser).OrderByDescending(m => m.IST_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("SalesTerm", edit);
        }
        [ChildActionOnly]
        public PartialViewResult SalesTermView(ITRS_SALES_TERM_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_SALES_TERM> ViewData = new List<PSW_ITRS_SALES_TERM>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_SALES_TERM.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IST_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IST_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IST_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IST_ISAUTH == true || m.IST_MAKER_ID != SessionUser).OrderByDescending(m => m.IST_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_SALES_TERM.Where(m => m.IST_ISAUTH == true || m.IST_MAKER_ID != SessionUser).OrderByDescending(m => m.IST_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeSalesTerm(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "SalesTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_SALES_TERM UpdateEntity = context.PSW_ITRS_SALES_TERM.Where(m => m.IST_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IST_ISAUTH = true;
                            UpdateEntity.IST_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IST_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Sales Term : " + UpdateEntity.IST_SALES_TERM_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectSalesTerm(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "SalesTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_SALES_TERM UpdateEntity = context.PSW_ITRS_SALES_TERM.Where(m => m.IST_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_SALES_TERM.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Sales Term : " + UpdateEntity.IST_SALES_TERM_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkSalesTerm(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "SalesTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_SALES_TERM Statement = new PSW_ITRS_SALES_TERM();
                                    if (row["Sales Term"].ToString().Length > 0)
                                    {
                                        string salesterm = row["Sales Term"].ToString();
                                        string code = row["Code"].ToString();
                                        int CountId = 0;
                                        if (code != null && code != "")
                                            CountId = Convert.ToInt32(code);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_SALES_TERM.Where(m => m.IST_SALES_TERM_CODE == CountId).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_SALES_TERM();
                                            Statement.IST_ID = Convert.ToInt32(context.PSW_ITRS_SALES_TERM.Max(m => (decimal?)m.IST_ID)) + 1;
                                            Statement.IST_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.IST_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Sales Term"] != null || row["Sales Term"].ToString() != "")
                                        {
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                Statement.IST_SALES_TERM_NAME = salesterm;
                                                Statement.IST_SALES_TERM_CODE = CountId;
                                                Statement.IST_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.IST_MAKER_ID == "Admin123")
                                                {
                                                    Statement.IST_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.IST_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.IST_CHECKER_ID = null;
                                                    Statement.IST_ISAUTH = false;
                                                }
                                                Statement.IST_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_SALES_TERM.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Code : " + row["Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Sales Term : " + row["Sales Term"].ToString() + "  At Code : " + row["Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Incoterm
        public ActionResult Incoterm(ITRS_INCOTERM_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Incoterm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_INCOTERM> List = context.PSW_ITRS_INCOTERM.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ICT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ICT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.ICT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.ICT_ISAUTH == true || m.ICT_MAKER_ID != SessionUser).OrderByDescending(m => m.ICT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_INCOTERM.Where(m => m.ICT_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Incoterm(ITRS_INCOTERM_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Incoterm", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_INCOTERM UpdateEntity = new PSW_ITRS_INCOTERM();

                        PSW_ITRS_INCOTERM CheckIfExist = new PSW_ITRS_INCOTERM();
                        if (db_table.Entity.ICT_ID == 0)
                        {
                            UpdateEntity.ICT_ID = Convert.ToInt32(context.PSW_ITRS_INCOTERM.Max(m => (decimal?)m.ICT_ID)) + 1;
                            UpdateEntity.ICT_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_INCOTERM.Where(m => m.ICT_ID == db_table.Entity.ICT_ID).FirstOrDefault();
                            UpdateEntity.ICT_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.ICT_INCOTERM_NAME = db_table.Entity.ICT_INCOTERM_NAME;
                        UpdateEntity.ICT_INCOTERM_CODE = db_table.Entity.ICT_INCOTERM_CODE;
                        UpdateEntity.ICT_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.ICT_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.ICT_CHECKER_ID = UpdateEntity.ICT_MAKER_ID;
                            UpdateEntity.ICT_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.ICT_CHECKER_ID = null;
                            UpdateEntity.ICT_ISAUTH = false;
                        }
                        UpdateEntity.ICT_STATUS = db_table.Entity.ICT_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_INCOTERM.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult IncotermFilter(ITRS_INCOTERM_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Incoterm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_INCOTERM> List = context.PSW_ITRS_INCOTERM.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ICT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ICT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.ICT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.ICT_ISAUTH == true || m.ICT_MAKER_ID != SessionUser).OrderByDescending(m => m.ICT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Incoterm", edit);
        }
        [HttpPost]
        public ActionResult IncotermFilter(ITRS_INCOTERM_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Incoterm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_INCOTERM> List = context.PSW_ITRS_INCOTERM.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ICT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ICT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.ICT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.ICT_ISAUTH == true || m.ICT_MAKER_ID != SessionUser).OrderByDescending(m => m.ICT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Incoterm", edit);
        }
        [ChildActionOnly]
        public PartialViewResult IncotermView(ITRS_INCOTERM_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_INCOTERM> ViewData = new List<PSW_ITRS_INCOTERM>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_INCOTERM.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.ICT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.ICT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.ICT_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.ICT_ISAUTH == true || m.ICT_MAKER_ID != SessionUser).OrderByDescending(m => m.ICT_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_INCOTERM.Where(m => m.ICT_ISAUTH == true || m.ICT_MAKER_ID != SessionUser).OrderByDescending(m => m.ICT_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeIncoterm(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Incoterm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_INCOTERM UpdateEntity = context.PSW_ITRS_INCOTERM.Where(m => m.ICT_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.ICT_ISAUTH = true;
                            UpdateEntity.ICT_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.ICT_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Incoterm : " + UpdateEntity.ICT_INCOTERM_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectIncoterm(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Incoterm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_INCOTERM UpdateEntity = context.PSW_ITRS_INCOTERM.Where(m => m.ICT_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_INCOTERM.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Incoterm : " + UpdateEntity.ICT_INCOTERM_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkIncoterm(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Incoterm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_INCOTERM Statement = new PSW_ITRS_INCOTERM();
                                    if (row["Name"].ToString().Length > 0)
                                    {
                                        string incoterm = row["Name"].ToString();
                                        string code = row["Code"].ToString();
                                        int CountId = 0;
                                        if (code != null && code != "")
                                            CountId = Convert.ToInt32(code);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_INCOTERM.Where(m => m.ICT_INCOTERM_CODE == CountId).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_INCOTERM();
                                            Statement.ICT_ID = Convert.ToInt32(context.PSW_ITRS_INCOTERM.Max(m => (decimal?)m.ICT_ID)) + 1;
                                            Statement.ICT_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.ICT_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Name"] != null || row["Name"].ToString() != "")
                                        {
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                Statement.ICT_INCOTERM_NAME = incoterm;
                                                Statement.ICT_INCOTERM_CODE = CountId;
                                                Statement.ICT_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.ICT_MAKER_ID == "Admin123")
                                                {
                                                    Statement.ICT_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.ICT_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.ICT_CHECKER_ID = null;
                                                    Statement.ICT_ISAUTH = false;
                                                }
                                                Statement.ICT_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_INCOTERM.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Code : " + row["Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Incoterm : " + row["Name"].ToString() + "  At Code : " + row["Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region UnitCode
        public ActionResult UnitCode(ITRS_UNIT_CODE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "UnitCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_UNIT_CODE> List = context.PSW_ITRS_UNIT_CODE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IUC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IUC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IUC_ISAUTH == true || m.IUC_MAKER_ID != SessionUser).OrderByDescending(m => m.IUC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UnitCode(ITRS_UNIT_CODE_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "UnitCode", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_UNIT_CODE UpdateEntity = new PSW_ITRS_UNIT_CODE();

                        PSW_ITRS_UNIT_CODE CheckIfExist = new PSW_ITRS_UNIT_CODE();
                        if (db_table.Entity.IUC_ID == 0)
                        {
                            UpdateEntity.IUC_ID = Convert.ToInt32(context.PSW_ITRS_UNIT_CODE.Max(m => (decimal?)m.IUC_ID)) + 1;
                            UpdateEntity.IUC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_ID == db_table.Entity.IUC_ID).FirstOrDefault();
                            UpdateEntity.IUC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IUC_UNIT_NAME = db_table.Entity.IUC_UNIT_NAME;
                        UpdateEntity.IUC_UNIT_CODE = db_table.Entity.IUC_UNIT_CODE;
                        UpdateEntity.IUC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IUC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IUC_CHECKER_ID = UpdateEntity.IUC_MAKER_ID;
                            UpdateEntity.IUC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IUC_CHECKER_ID = null;
                            UpdateEntity.IUC_ISAUTH = false;
                        }
                        UpdateEntity.IUC_STATUS = db_table.Entity.IUC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_UNIT_CODE.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult UnitCodeFilter(ITRS_UNIT_CODE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "UnitCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_UNIT_CODE> List = context.PSW_ITRS_UNIT_CODE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IUC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IUC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IUC_ISAUTH == true || m.IUC_MAKER_ID != SessionUser).OrderByDescending(m => m.IUC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("UnitCode", edit);
        }
        [HttpPost]
        public ActionResult UnitCodeFilter(ITRS_UNIT_CODE_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "UnitCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_UNIT_CODE> List = context.PSW_ITRS_UNIT_CODE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IUC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IUC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IUC_ISAUTH == true || m.IUC_MAKER_ID != SessionUser).OrderByDescending(m => m.IUC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("UnitCode", edit);
        }
        [ChildActionOnly]
        public PartialViewResult UnitCodeView(ITRS_UNIT_CODE_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_UNIT_CODE> ViewData = new List<PSW_ITRS_UNIT_CODE>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_UNIT_CODE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IUC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IUC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IUC_ISAUTH == true || m.IUC_MAKER_ID != SessionUser).OrderByDescending(m => m.IUC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_ISAUTH == true || m.IUC_MAKER_ID != SessionUser).OrderByDescending(m => m.IUC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeUnitCode(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "UnitCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_UNIT_CODE UpdateEntity = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IUC_ISAUTH = true;
                            UpdateEntity.IUC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IUC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Unit Code : " + UpdateEntity.IUC_UNIT_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectUnitCode(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "UnitCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_UNIT_CODE UpdateEntity = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_UNIT_CODE.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Unit Code : " + UpdateEntity.IUC_UNIT_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkUnitCode(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "UnitCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_UNIT_CODE Statement = new PSW_ITRS_UNIT_CODE();
                                    if (row["Name"].ToString().Length > 0)
                                    {
                                        string unitcode = row["Name"].ToString();
                                        string code = row["UnitID"].ToString();
                                        int CountId = 0;
                                        if (code != null && code != "")
                                            CountId = Convert.ToInt32(code);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_UNIT_CODE.Where(m => m.IUC_UNIT_NAME == unitcode).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_UNIT_CODE();
                                            Statement.IUC_ID = Convert.ToInt32(context.PSW_ITRS_UNIT_CODE.Max(m => (decimal?)m.IUC_ID)) + 1;
                                            Statement.IUC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.IUC_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Name"] != null || row["Name"].ToString() != "")
                                        {
                                            if (row["UnitID"].ToString() != "" && row["UnitID"] != null)
                                            {
                                                Statement.IUC_UNIT_NAME = unitcode;
                                                Statement.IUC_UNIT_CODE = CountId;
                                                Statement.IUC_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.IUC_MAKER_ID == "Admin123")
                                                {
                                                    Statement.IUC_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.IUC_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.IUC_CHECKER_ID = null;
                                                    Statement.IUC_ISAUTH = false;
                                                }
                                                Statement.IUC_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_UNIT_CODE.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Code : " + row["UnitID"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Unit Code : " + row["Name"].ToString() + "  At Code : " + row["UnitID"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Purpose
        public ActionResult Purpose(ITRS_PURPOSE_CODE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Purpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_PURPOSE> List = context.PSW_ITRS_PURPOSE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IPC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IPC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IPC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IPC_ISAUTH == true || m.IPC_MAKER_ID != SessionUser).OrderByDescending(m => m.IPC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Purpose(ITRS_PURPOSE_CODE_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Purpose", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_PURPOSE UpdateEntity = new PSW_ITRS_PURPOSE();

                        PSW_ITRS_PURPOSE CheckIfExist = new PSW_ITRS_PURPOSE();
                        if (db_table.Entity.IPC_ID == 0)
                        {
                            UpdateEntity.IPC_ID = Convert.ToInt32(context.PSW_ITRS_PURPOSE.Max(m => (decimal?)m.IPC_ID)) + 1;
                            UpdateEntity.IPC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ID == db_table.Entity.IPC_ID).FirstOrDefault();
                            UpdateEntity.IPC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IPC_PURPOSE_ID = db_table.Entity.IPC_PURPOSE_ID;
                        UpdateEntity.IPC_PURPOSE_NAME = db_table.Entity.IPC_PURPOSE_NAME;
                        UpdateEntity.IPC_PURPOSE_DESCRIPTION = db_table.Entity.IPC_PURPOSE_DESCRIPTION;
                        UpdateEntity.IPC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IPC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IPC_CHECKER_ID = UpdateEntity.IPC_MAKER_ID;
                            UpdateEntity.IPC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IPC_CHECKER_ID = null;
                            UpdateEntity.IPC_ISAUTH = false;
                        }
                        UpdateEntity.IPC_STATUS = db_table.Entity.IPC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_PURPOSE.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult PurposeFilter(ITRS_PURPOSE_CODE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Purpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_PURPOSE> List = context.PSW_ITRS_PURPOSE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IPC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IPC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IPC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IPC_ISAUTH == true || m.IPC_MAKER_ID != SessionUser).OrderByDescending(m => m.IPC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Purpose", edit);
        }
        [HttpPost]
        public ActionResult PurposeFilter(ITRS_PURPOSE_CODE_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Purpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_PURPOSE> List = context.PSW_ITRS_PURPOSE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IPC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IPC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IPC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IPC_ISAUTH == true || m.IPC_MAKER_ID != SessionUser).OrderByDescending(m => m.IPC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Purpose", edit);
        }
        [ChildActionOnly]
        public PartialViewResult PurposeView(ITRS_PURPOSE_CODE_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_PURPOSE> ViewData = new List<PSW_ITRS_PURPOSE>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_PURPOSE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IPC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IPC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IPC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IPC_ISAUTH == true || m.IPC_MAKER_ID != SessionUser).OrderByDescending(m => m.IPC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ISAUTH == true || m.IPC_MAKER_ID != SessionUser).OrderByDescending(m => m.IPC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizePurpose(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Purpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_PURPOSE UpdateEntity = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IPC_ISAUTH = true;
                            UpdateEntity.IPC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IPC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Purpose : " + UpdateEntity.IPC_PURPOSE_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectPurpose(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Purpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_PURPOSE UpdateEntity = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_PURPOSE.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Purpose : " + UpdateEntity.IPC_PURPOSE_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkPurpose(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Purpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_PURPOSE Statement = new PSW_ITRS_PURPOSE();
                                    if (row["Name"].ToString().Length > 0)
                                    {
                                        string PurposeId = row["PurposeID"].ToString();
                                        string PurposeName = row["Name"].ToString();
                                        //string PurposeDescription = row["Description"].ToString();
                                        int CountId = 0;
                                        if (PurposeId != null && PurposeId != "")
                                            CountId = Convert.ToInt32(PurposeId);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_PURPOSE_ID == CountId && m.IPC_PURPOSE_NAME == PurposeName).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_PURPOSE();
                                            Statement.IPC_ID = Convert.ToInt32(context.PSW_ITRS_PURPOSE.Max(m => (decimal?)m.IPC_ID)) + 1;
                                            Statement.IPC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.IPC_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["PurposeID"].ToString() != "" && row["PurposeID"] != null)
                                        {
                                            Statement.IPC_PURPOSE_ID = CountId;
                                            if (row["Name"] != null || row["Name"].ToString() != "")
                                            {
                                                if (PurposeName != null && PurposeName != "")
                                                    Statement.IPC_PURPOSE_NAME = PurposeName.Trim();
                                                else
                                                    Statement.IPC_PURPOSE_NAME = PurposeName;
                                                Statement.IPC_PURPOSE_DESCRIPTION = null;
                                                Statement.IPC_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.IPC_MAKER_ID == "Admin123")
                                                {
                                                    Statement.IPC_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.IPC_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.IPC_CHECKER_ID = null;
                                                    Statement.IPC_ISAUTH = false;
                                                }
                                                Statement.IPC_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_PURPOSE.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Purpose Name : " + row["Name"].ToString() + "  At Purpose Id : " + row["PurposeID"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Purpose Id : " + row["PurposeID"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        public JsonResult AuthorizeBullkITRSPurpose(/*DateTime? FromDate, DateTime? ToDate*/)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Purpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    string SessionUser = Session["USER_ID"].ToString();
                    try
                    {
                        List<PSW_ITRS_PURPOSE> BulkData = new List<PSW_ITRS_PURPOSE>();
                        BulkData = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ISAUTH == false && m.IPC_MAKER_ID != SessionUser).OrderByDescending(m => m.IPC_ID).ToList();
                        if (BulkData.Count() > 0)
                        {
                            foreach (var item in BulkData)
                            {
                                PSW_ITRS_PURPOSE UpdateEntity = context.PSW_ITRS_PURPOSE.Where(m => m.IPC_ID == item.IPC_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.IPC_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.IPC_EDIT_DATETIME = DateTime.Now;
                                    UpdateEntity.IPC_ISAUTH = true;
                                    context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                                return Json("Record Authorized successfully");
                        }
                        else
                            message = "Unable to fetch data against the User Id : " + SessionUser;
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Department
        public ActionResult Department(ITRS_DEPARTMENT_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Department", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_DEPARTMENTS> List = context.PSW_ITRS_DEPARTMENTS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IDC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IDC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IDC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IDC_ISAUTH == true || m.IDC_MAKER_ID != SessionUser).OrderByDescending(m => m.IDC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_DEPARTMENTS.Where(m => m.IDC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Department(ITRS_DEPARTMENT_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Department", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_DEPARTMENTS UpdateEntity = new PSW_ITRS_DEPARTMENTS();

                        PSW_ITRS_DEPARTMENTS CheckIfExist = new PSW_ITRS_DEPARTMENTS();
                        if (db_table.Entity.IDC_ID == 0)
                        {
                            UpdateEntity.IDC_ID = Convert.ToInt32(context.PSW_ITRS_DEPARTMENTS.Max(m => (decimal?)m.IDC_ID)) + 1;
                            UpdateEntity.IDC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_DEPARTMENTS.Where(m => m.IDC_ID == db_table.Entity.IDC_ID).FirstOrDefault();
                            UpdateEntity.IDC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IDC_DEPARTMENT_ID = db_table.Entity.IDC_DEPARTMENT_ID;
                        UpdateEntity.IDC_DEPARTMENT_NAME = db_table.Entity.IDC_DEPARTMENT_NAME;
                        UpdateEntity.IDC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IDC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IDC_CHECKER_ID = UpdateEntity.IDC_MAKER_ID;
                            UpdateEntity.IDC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IDC_CHECKER_ID = null;
                            UpdateEntity.IDC_ISAUTH = false;
                        }
                        UpdateEntity.IDC_STATUS = db_table.Entity.IDC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_DEPARTMENTS.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult DepartmentFilter(ITRS_DEPARTMENT_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Department", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_DEPARTMENTS> List = context.PSW_ITRS_DEPARTMENTS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IDC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IDC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IDC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IDC_ISAUTH == true || m.IDC_MAKER_ID != SessionUser).OrderByDescending(m => m.IDC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Department", edit);
        }
        [HttpPost]
        public ActionResult DepartmentFilter(ITRS_DEPARTMENT_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Department", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_DEPARTMENTS> List = context.PSW_ITRS_DEPARTMENTS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IDC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IDC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IDC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IDC_ISAUTH == true || m.IDC_MAKER_ID != SessionUser).OrderByDescending(m => m.IDC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Department", edit);
        }
        [ChildActionOnly]
        public PartialViewResult DepartmentView(ITRS_DEPARTMENT_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_DEPARTMENTS> ViewData = new List<PSW_ITRS_DEPARTMENTS>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_DEPARTMENTS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IDC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IDC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IDC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IDC_ISAUTH == true || m.IDC_MAKER_ID != SessionUser).OrderByDescending(m => m.IDC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_DEPARTMENTS.Where(m => m.IDC_ISAUTH == true || m.IDC_MAKER_ID != SessionUser).OrderByDescending(m => m.IDC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeDepartment(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Department", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_DEPARTMENTS UpdateEntity = context.PSW_ITRS_DEPARTMENTS.Where(m => m.IDC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IDC_ISAUTH = true;
                            UpdateEntity.IDC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IDC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Department : " + UpdateEntity.IDC_DEPARTMENT_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectDepartment(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Department", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_DEPARTMENTS UpdateEntity = context.PSW_ITRS_DEPARTMENTS.Where(m => m.IDC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_DEPARTMENTS.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Department : " + UpdateEntity.IDC_DEPARTMENT_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkDepartment(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Department", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_DEPARTMENTS Statement = new PSW_ITRS_DEPARTMENTS();
                                    if (row["Name"].ToString().Length > 0)
                                    {
                                        string DepartmentName = row["Name"].ToString();
                                        string DepartmentId = row["Department ID"].ToString();
                                        int CountId = 0;
                                        if (DepartmentId != null && DepartmentId != "")
                                            CountId = Convert.ToInt32(DepartmentId);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_DEPARTMENTS.Where(m => m.IDC_DEPARTMENT_ID == CountId).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_DEPARTMENTS();
                                            Statement.IDC_ID = Convert.ToInt32(context.PSW_ITRS_DEPARTMENTS.Max(m => (decimal?)m.IDC_ID)) + 1;
                                            Statement.IDC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.IDC_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Name"] != null || row["Name"].ToString() != "")
                                        {
                                            if (row["Department ID"].ToString() != "" && row["Department ID"] != null)
                                            {
                                                Statement.IDC_DEPARTMENT_NAME = DepartmentName;
                                                Statement.IDC_DEPARTMENT_ID = CountId;
                                                Statement.IDC_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.IDC_MAKER_ID == "Admin123")
                                                {
                                                    Statement.IDC_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.IDC_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.IDC_CHECKER_ID = null;
                                                    Statement.IDC_ISAUTH = false;
                                                }
                                                Statement.IDC_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_DEPARTMENTS.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Department Id : " + row["Department ID"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Department Name : " + row["Name"].ToString() + "  At Department Id : " + row["Department ID"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Immidiate Remitter
        public ActionResult ImmidiateRemitter(ITRS_IMMEDIATE_REMITTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ImmidiateRemitter", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_REMITTER> List = context.PSW_ITRS_REMITTER.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IR_ISAUTH == true || m.IR_MAKER_ID != SessionUser).OrderByDescending(m => m.IR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_REMITTER.Where(m => m.IR_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ImmidiateRemitter(ITRS_IMMEDIATE_REMITTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ImmidiateRemitter", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_REMITTER UpdateEntity = new PSW_ITRS_REMITTER();

                        PSW_ITRS_REMITTER CheckIfExist = new PSW_ITRS_REMITTER();
                        if (db_table.Entity.IR_ID == 0)
                        {
                            UpdateEntity.IR_ID = Convert.ToInt32(context.PSW_ITRS_REMITTER.Max(m => (decimal?)m.IR_ID)) + 1;
                            UpdateEntity.IR_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_REMITTER.Where(m => m.IR_ID == db_table.Entity.IR_ID).FirstOrDefault();
                            UpdateEntity.IR_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IR_REMITTER_NAME = db_table.Entity.IR_REMITTER_NAME;
                        UpdateEntity.IR_REMITTER_CODE = db_table.Entity.IR_REMITTER_CODE;
                        UpdateEntity.IR_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IR_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IR_CHECKER_ID = UpdateEntity.IR_MAKER_ID;
                            UpdateEntity.IR_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IR_CHECKER_ID = null;
                            UpdateEntity.IR_ISAUTH = false;
                        }
                        UpdateEntity.IR_STATUS = db_table.Entity.IR_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_REMITTER.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult ImmidiateRemitterFilter(ITRS_IMMEDIATE_REMITTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ImmidiateRemitter", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_REMITTER> List = context.PSW_ITRS_REMITTER.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IR_ISAUTH == true || m.IR_MAKER_ID != SessionUser).OrderByDescending(m => m.IR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ImmidiateRemitter", edit);
        }
        [HttpPost]
        public ActionResult ImmidiateRemitterFilter(ITRS_IMMEDIATE_REMITTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ImmidiateRemitter", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_REMITTER> List = context.PSW_ITRS_REMITTER.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IR_ISAUTH == true || m.IR_MAKER_ID != SessionUser).OrderByDescending(m => m.IR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ImmidiateRemitter", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ImmidiateRemitterView(ITRS_IMMEDIATE_REMITTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_REMITTER> ViewData = new List<PSW_ITRS_REMITTER>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_REMITTER.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IR_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IR_ISAUTH == true || m.IR_MAKER_ID != SessionUser).OrderByDescending(m => m.IR_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_REMITTER.Where(m => m.IR_ISAUTH == true || m.IR_MAKER_ID != SessionUser).OrderByDescending(m => m.IR_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeImmidiateRemitter(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ImmidiateRemitter", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_REMITTER UpdateEntity = context.PSW_ITRS_REMITTER.Where(m => m.IR_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IR_ISAUTH = true;
                            UpdateEntity.IR_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IR_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = UpdateEntity.IR_REMITTER_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectImmidiateRemitter(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ImmidiateRemitter", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_REMITTER UpdateEntity = context.PSW_ITRS_REMITTER.Where(m => m.IR_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_REMITTER.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = UpdateEntity.IR_REMITTER_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkImmidiateRemitter(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "ImmidiateRemitter", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_REMITTER Statement = new PSW_ITRS_REMITTER();
                                    if (row["Status of Immediate Remitter"].ToString().Length > 0)
                                    {
                                        string name = row["Status of Immediate Remitter"].ToString();
                                        string code = row["Code"].ToString();
                                        int CountId = 0;
                                        if (code != null && code != "")
                                            CountId = Convert.ToInt32(code);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_REMITTER.Where(m => m.IR_REMITTER_CODE == CountId).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_REMITTER();
                                            Statement.IR_ID = Convert.ToInt32(context.PSW_ITRS_REMITTER.Max(m => (decimal?)m.IR_ID)) + 1;
                                            Statement.IR_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.IR_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Status of Immediate Remitter"] != null || row["Status of Immediate Remitter"].ToString() != "")
                                        {
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                Statement.IR_REMITTER_NAME = name;
                                                Statement.IR_REMITTER_CODE = CountId;
                                                Statement.IR_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.IR_MAKER_ID == "Admin123")
                                                {
                                                    Statement.IR_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.IR_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.IR_CHECKER_ID = null;
                                                    Statement.IR_ISAUTH = false;
                                                }
                                                Statement.IR_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_REMITTER.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Remitter  Code: " + row["Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Remitter : " + row["Status of Immediate Remitter"].ToString() + "  At Remitter Code : " + row["Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Banks
        public ActionResult Banks(ITRS_BANK_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Banks", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_BANKS> List = context.PSW_ITRS_BANKS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.BC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.BC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.BC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.BC_ISAUTH == true || m.BC_MAKER_ID != SessionUser).OrderByDescending(m => m.BC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_BANKS.Where(m => m.BC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Banks(ITRS_BANK_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Banks", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_BANKS UpdateEntity = new PSW_ITRS_BANKS();

                        PSW_ITRS_BANKS CheckIfExist = new PSW_ITRS_BANKS();
                        if (db_table.Entity.BC_ID == 0)
                        {
                            UpdateEntity.BC_ID = Convert.ToInt32(context.PSW_ITRS_BANKS.Max(m => (decimal?)m.BC_ID)) + 1;
                            UpdateEntity.BC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_BANKS.Where(m => m.BC_ID == db_table.Entity.BC_ID).FirstOrDefault();
                            UpdateEntity.BC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.BC_BANK_ID = db_table.Entity.BC_BANK_ID;
                        UpdateEntity.BC_BANK_NAME = db_table.Entity.BC_BANK_NAME;
                        UpdateEntity.BC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.BC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.BC_CHECKER_ID = UpdateEntity.BC_MAKER_ID;
                            UpdateEntity.BC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.BC_CHECKER_ID = null;
                            UpdateEntity.BC_ISAUTH = false;
                        }
                        UpdateEntity.BC_STATUS = db_table.Entity.BC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_BANKS.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult BanksFilter(ITRS_BANK_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Banks", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_BANKS> List = context.PSW_ITRS_BANKS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.BC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.BC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.BC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.BC_ISAUTH == true || m.BC_MAKER_ID != SessionUser).OrderByDescending(m => m.BC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Banks", edit);
        }
        [HttpPost]
        public ActionResult BanksFilter(ITRS_BANK_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Banks", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_BANKS> List = context.PSW_ITRS_BANKS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.BC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.BC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.BC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.BC_ISAUTH == true || m.BC_MAKER_ID != SessionUser).OrderByDescending(m => m.BC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Banks", edit);
        }
        [ChildActionOnly]
        public PartialViewResult BanksView(ITRS_BANK_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_BANKS> ViewData = new List<PSW_ITRS_BANKS>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_BANKS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.BC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.BC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.BC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.BC_ISAUTH == true || m.BC_MAKER_ID != SessionUser).OrderByDescending(m => m.BC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_BANKS.Where(m => m.BC_ISAUTH == true || m.BC_MAKER_ID != SessionUser).OrderByDescending(m => m.BC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeBanks(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Banks", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_BANKS UpdateEntity = context.PSW_ITRS_BANKS.Where(m => m.BC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.BC_ISAUTH = true;
                            UpdateEntity.BC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.BC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Bank : " + UpdateEntity.BC_BANK_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectBanks(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Banks", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_BANKS UpdateEntity = context.PSW_ITRS_BANKS.Where(m => m.BC_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_BANKS.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Bank : " + UpdateEntity.BC_BANK_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkBanks(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Banks", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_BANKS Statement = new PSW_ITRS_BANKS();
                                    if (row["Name"].ToString().Length > 0)
                                    {
                                        string Bankname = row["Name"].ToString();
                                        string BankId = row["Organization ID"].ToString();
                                        int CountId = 0;
                                        if (BankId != null && BankId != "")
                                            CountId = Convert.ToInt32(BankId);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_BANKS.Where(m => m.BC_BANK_ID == CountId).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_BANKS();
                                            Statement.BC_ID = Convert.ToInt32(context.PSW_ITRS_BANKS.Max(m => (decimal?)m.BC_ID)) + 1;
                                            Statement.BC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.BC_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Name"] != null || row["Name"].ToString() != "")
                                        {
                                            if (row["Organization ID"].ToString() != "" && row["Organization ID"] != null)
                                            {
                                                Statement.BC_BANK_NAME = Bankname;
                                                Statement.BC_BANK_ID = CountId;
                                                Statement.BC_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.BC_MAKER_ID == "Admin123")
                                                {
                                                    Statement.BC_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.BC_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.BC_CHECKER_ID = null;
                                                    Statement.BC_ISAUTH = false;
                                                }
                                                Statement.BC_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_BANKS.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Bank Id : " + row["Organization ID"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Bank Name : " + row["Name"].ToString() + "  At Bank Id : " + row["Organization ID"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region USD Rate List
        public ActionResult USDRate(ITRS_USD_RATE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "USDRate", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_USD_RATE> List = context.PSW_ITRS_USD_RATE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUR_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IUR_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IUR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IUR_ISAUTH == true || m.IUR_MAKER_ID != SessionUser).OrderByDescending(m => m.IUR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_USD_RATE.Where(m => m.IUR_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult USDRate(ITRS_USD_RATE_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "USDRate", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_USD_RATE UpdateEntity = new PSW_ITRS_USD_RATE();

                        PSW_ITRS_USD_RATE CheckIfExist = new PSW_ITRS_USD_RATE();

                        if (db_table.Entity.IUR_ID == 0)
                        {
                            UpdateEntity.IUR_ID = Convert.ToInt32(context.PSW_ITRS_USD_RATE.Max(m => (decimal?)m.IUR_ID)) + 1;
                            UpdateEntity.IUR_DATA_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_USD_RATE.Where(m => m.IUR_ID == db_table.Entity.IUR_ID).FirstOrDefault();
                            UpdateEntity.IUR_DATA_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IUR_TODAY_DATE = db_table.Entity.IUR_TODAY_DATE;
                        UpdateEntity.IUR_RATE = db_table.Entity.IUR_RATE;
                        UpdateEntity.IUR_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IUR_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IUR_CHECKER_ID = UpdateEntity.IUR_MAKER_ID;
                            UpdateEntity.IUR_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IUR_CHECKER_ID = null;
                            UpdateEntity.IUR_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_ITRS_USD_RATE.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult USDRateFilter(ITRS_USD_RATE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "USDRate", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_USD_RATE> List = context.PSW_ITRS_USD_RATE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUR_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IUR_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IUR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IUR_ISAUTH == true || m.IUR_MAKER_ID != SessionUser).OrderByDescending(m => m.IUR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("USDRate", edit);
        }
        [HttpPost]
        public ActionResult USDRateFilter(ITRS_USD_RATE_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "USDRate", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_USD_RATE> List = context.PSW_ITRS_USD_RATE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUR_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IUR_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IUR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IUR_ISAUTH == true || m.IUR_MAKER_ID != SessionUser).OrderByDescending(m => m.IUR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("USDRate", edit);
        }
        [ChildActionOnly]
        public PartialViewResult USDRateView(ITRS_USD_RATE_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_USD_RATE> ViewData = new List<PSW_ITRS_USD_RATE>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_USD_RATE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUR_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IUR_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IUR_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IUR_ISAUTH == true || m.IUR_MAKER_ID != SessionUser).OrderByDescending(m => m.IUR_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_USD_RATE.Where(m => m.IUR_ISAUTH == true || m.IUR_MAKER_ID != SessionUser).OrderByDescending(m => m.IUR_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeUSDRate(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "USDRate", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_USD_RATE UpdateEntity = context.PSW_ITRS_USD_RATE.Where(m => m.IUR_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IUR_ISAUTH = true;
                            UpdateEntity.IUR_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IUR_DATA_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "USD Rate : " + UpdateEntity.IUR_RATE + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectUSDRate(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "USDRate", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_USD_RATE UpdateEntity = context.PSW_ITRS_USD_RATE.Where(m => m.IUR_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_USD_RATE.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "USD Rate : " + UpdateEntity.IUR_RATE + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkUSDRate(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "USDRate", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_USD_RATE Statement = new PSW_ITRS_USD_RATE();
                                    if (row["Date"].ToString().Length > 0)
                                    {
                                        int Date = Convert.ToInt32(row["Date"]);
                                        DateTime? TransDate = DateTime.FromOADate(Date);
                                        string TodatyRate = row["Rate"].ToString();
                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_USD_RATE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IUR_TODAY_DATE) == System.Data.Entity.DbFunctions.TruncateTime(TransDate)).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_USD_RATE();
                                            Statement.IUR_ID = Convert.ToInt32(context.PSW_ITRS_USD_RATE.Max(m => (decimal?)m.IUR_ID)) + 1;
                                            Statement.IUR_DATA_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.IUR_DATA_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Date"] != null || row["Date"].ToString() != "")
                                        {
                                            if (row["Rate"].ToString() != "" && row["Rate"] != null)
                                            {
                                                Statement.IUR_TODAY_DATE = TransDate;
                                                Statement.IUR_RATE = Convert.ToDecimal(TodatyRate);
                                                Statement.IUR_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.IUR_MAKER_ID == "Admin123")
                                                {
                                                    Statement.IUR_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.IUR_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.IUR_CHECKER_ID = null;
                                                    Statement.IUR_ISAUTH = false;
                                                }
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_USD_RATE.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid USD Today Rate : " + row["Rate"].ToString() + "  At USD Today Date : " + row["Date"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid USD Today Date : " + row["Date"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region A404
        public ActionResult A404(ITRS_A404_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "A404", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_A404> List = context.PSW_ITRS_A404.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IA_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IA_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IA_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IA_ISAUTH == true || m.IA_MAKER_ID != SessionUser).OrderByDescending(m => m.IA_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_A404.Where(m => m.IA_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult A404(ITRS_A404_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "A404", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_A404 UpdateEntity = new PSW_ITRS_A404();

                        PSW_ITRS_A404 CheckIfExist = new PSW_ITRS_A404();

                        if (db_table.Entity.IA_ID == 0)
                        {
                            UpdateEntity.IA_ID = Convert.ToInt32(context.PSW_ITRS_A404.Max(m => (decimal?)m.IA_ID)) + 1;
                            UpdateEntity.IA_DATA_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_A404.Where(m => m.IA_ID == db_table.Entity.IA_ID).FirstOrDefault();
                            UpdateEntity.IA_DATA_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IA_SERIAL_NO = db_table.Entity.IA_SERIAL_NO;
                        UpdateEntity.IA_AD = db_table.Entity.IA_AD;
                        UpdateEntity.IA_CURRENCY = db_table.Entity.IA_CURRENCY;
                        UpdateEntity.IA_DATE_OF_UTILIZED = db_table.Entity.IA_DATE_OF_UTILIZED;
                        UpdateEntity.IA_EFORM_NO = db_table.Entity.IA_EFORM_NO;
                        UpdateEntity.IA_APV_NO = db_table.Entity.IA_APV_NO;
                        UpdateEntity.IA_AMOUNT_UTLIZED = db_table.Entity.IA_AMOUNT_UTLIZED;
                        UpdateEntity.IA_RERERENCE_OF_SBP_APPROVAL = db_table.Entity.IA_RERERENCE_OF_SBP_APPROVAL;
                        UpdateEntity.IA_SBP_APPROVAL_DATE = db_table.Entity.IA_SBP_APPROVAL_DATE;
                        UpdateEntity.IA_COMMODITY = db_table.Entity.IA_COMMODITY;
                        UpdateEntity.IA_UPLOAD_MONTH = db_table.Entity.IA_UPLOAD_MONTH;
                        UpdateEntity.IA_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IA_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IA_CHECKER_ID = UpdateEntity.IA_MAKER_ID;
                            UpdateEntity.IA_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IA_CHECKER_ID = null;
                            UpdateEntity.IA_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_ITRS_A404.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult A404Filter(ITRS_A404_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "A404", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_A404> List = context.PSW_ITRS_A404.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IA_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IA_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IA_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IA_ISAUTH == true || m.IA_MAKER_ID != SessionUser).OrderByDescending(m => m.IA_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("A404", edit);
        }
        [HttpPost]
        public ActionResult A404Filter(ITRS_A404_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "A404", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_A404> List = context.PSW_ITRS_A404.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IA_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IA_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IA_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IA_ISAUTH == true || m.IA_MAKER_ID != SessionUser).OrderByDescending(m => m.IA_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("A404", edit);
        }
        [ChildActionOnly]
        public PartialViewResult A404View(ITRS_A404_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_A404> ViewData = new List<PSW_ITRS_A404>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_A404.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IA_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IA_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IA_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IA_ISAUTH == true || m.IA_MAKER_ID != SessionUser).OrderByDescending(m => m.IA_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_A404.Where(m => m.IA_ISAUTH == true || m.IA_MAKER_ID != SessionUser).OrderByDescending(m => m.IA_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeA404(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "A404", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_A404 UpdateEntity = context.PSW_ITRS_A404.Where(m => m.IA_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IA_ISAUTH = true;
                            UpdateEntity.IA_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IA_DATA_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = UpdateEntity.IA_EFORM_NO + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectA404(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "A404", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_A404 UpdateEntity = context.PSW_ITRS_A404.Where(m => m.IA_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_A404.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = UpdateEntity.IA_EFORM_NO + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkA404(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "A404", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_A404 Statement = new PSW_ITRS_A404();
                                    if (row["E Form No."].ToString().Length > 0)
                                    {
                                        DateTime UploadMonth = DateTime.Now;
                                        int uploadmonth = 0;
                                        string AD = row["AD"].ToString();
                                        string Currency = row["Currency"].ToString();
                                        string SerialNo = row["Serial No"].ToString();
                                        string EFormNo = row["E Form No."].ToString();
                                        if (row["Upload Month"].ToString() != "" && row["Upload Month"] != null)
                                        {
                                            uploadmonth = Convert.ToInt32(row["Upload Month"]);
                                            UploadMonth = DateTime.FromOADate(uploadmonth);
                                        }
                                        string Commodity = row["Commodity"].ToString();
                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_A404.Where(m => m.IA_AD == AD && m.IA_CURRENCY == Currency && m.IA_SERIAL_NO == SerialNo && m.IA_EFORM_NO == EFormNo && System.Data.Entity.DbFunctions.TruncateTime(m.IA_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(UploadMonth) && m.IA_COMMODITY == Commodity).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_A404();
                                            Statement.IA_ID = Convert.ToInt32(context.PSW_ITRS_A404.Max(m => (decimal?)m.IA_ID)) + 1;
                                            Statement.IA_DATA_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.IA_DATA_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Serial No"] != null || row["Serial No"].ToString() != "")
                                        {
                                            if (row["Date of Utilization"].ToString() != "" && row["Date of Utilization"] != null)
                                            {
                                                int dateofultilized = Convert.ToInt32(row["Date of Utilization"]);
                                                DateTime DateOfUitilized = DateTime.FromOADate(dateofultilized);
                                                if (row["E Form No."].ToString() != "" && row["E Form No."] != null)
                                                {
                                                    if (row["APV No."].ToString() != "" && row["APV No."] != null)
                                                    {
                                                        string APVNo = row["APV No."].ToString();
                                                        if (row["Amount Utilized in APV(A2/O2) Currency"].ToString() != "" && row["Amount Utilized in APV(A2/O2) Currency"] != null)
                                                        {
                                                            string AmountUitilized = row["Amount Utilized in APV(A2/O2) Currency"].ToString();
                                                            string SBPApprovalNo = row["Reference of SBP's Approval"].ToString();
                                                            DateTime SBPApprovalDate = DateTime.Now;
                                                            if(row["SBP's Approval Date"].ToString() != "NULL" && row["SBP's Approval Date"].ToString() != "" && row["SBP's Approval Date"] != null)
                                                            {
                                                                int sbpapprovaldate = Convert.ToInt32(row["SBP's Approval Date"]);
                                                                SBPApprovalDate = DateTime.FromOADate(dateofultilized);
                                                            }
                                                            if (row["Commodity"].ToString() != "" && row["Commodity"] != null)
                                                            {
                                                                if (row["Upload Month"].ToString() != "" && row["Upload Month"] != null)
                                                                {
                                                                    if (row["AD"].ToString() != "" && row["AD"] != null)
                                                                    {
                                                                        if (row["Currency"].ToString() != "" && row["Currency"] != null)
                                                                        {
                                                                            Statement.IA_SERIAL_NO = SerialNo;
                                                                            Statement.IA_AD = AD;
                                                                            Statement.IA_CURRENCY = Currency;
                                                                            Statement.IA_DATE_OF_UTILIZED = DateOfUitilized;
                                                                            Statement.IA_EFORM_NO = EFormNo;
                                                                            Statement.IA_APV_NO = APVNo;
                                                                            Statement.IA_AMOUNT_UTLIZED = Convert.ToDecimal(AmountUitilized);
                                                                            Statement.IA_RERERENCE_OF_SBP_APPROVAL = SBPApprovalNo;
                                                                            if (row["SBP's Approval Date"].ToString() != "" && row["SBP's Approval Date"] != null)
                                                                                Statement.IA_SBP_APPROVAL_DATE = SBPApprovalDate;
                                                                            Statement.IA_COMMODITY = Commodity;
                                                                            Statement.IA_UPLOAD_MONTH = UploadMonth;
                                                                            Statement.IA_DATE_OF_UTILIZED = DateOfUitilized;
                                                                            Statement.IA_MAKER_ID = Session["USER_ID"].ToString();
                                                                            if (Statement.IA_MAKER_ID == "Admin123")
                                                                            {
                                                                                Statement.IA_CHECKER_ID = Session["USER_ID"].ToString();
                                                                                Statement.IA_ISAUTH = true;
                                                                            }
                                                                            else
                                                                            {
                                                                                Statement.IA_CHECKER_ID = null;
                                                                                Statement.IA_ISAUTH = false;
                                                                            }
                                                                            if (CheckIfExist == false)
                                                                            {
                                                                                context.PSW_ITRS_A404.Add(Statement);
                                                                            }
                                                                            else
                                                                            {
                                                                                context.Entry(Statement).State = EntityState.Modified;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "Invalid Currency : " + row["Currency"].ToString() + " at Serial No : " + row["Serial No"].ToString() + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid AD : " + row["AD"].ToString() + " at Serial No : " + row["Serial No"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid Upload Month : " + row["Upload Month"].ToString() + " at Serial No : " + row["Serial No"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid Commodity : " + row["Commodity"].ToString() + " at Serial No : " + row["Serial No"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Amount Utilized in APV(A2/O2) Currency : " + row["Amount Utilized in APV(A2/O2) Currency"].ToString() + " at Serial No : " + row["Serial No"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid APV No : " + row["APV No."].ToString() + " at Serial No : " + row["Serial No"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid E Form No : " + row["E Form No."].ToString() + " at Serial No : " + row["Serial No"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Date of Utilization : " + row["Date of Utilization"].ToString() + " at Serial No : " + row["Serial No"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Serial No : " + row["Serial No"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Balances
        public ActionResult Balances(ITRS_BALANCES_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Balances", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_BALANCES> List = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IB_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IB_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IB_ISAUTH == true || m.IB_MAKER_ID != SessionUser).OrderByDescending(m => m.IB_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_BALANCES.Where(m => m.IB_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Balances(ITRS_BALANCES_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Balances", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_BALANCES UpdateEntity = new PSW_ITRS_BALANCES();

                        PSW_ITRS_BALANCES CheckIfExist = new PSW_ITRS_BALANCES();

                        if (db_table.Entity.IB_ID == 0)
                        {
                            UpdateEntity.IB_ID = Convert.ToInt32(context.PSW_ITRS_BALANCES.Max(m => (decimal?)m.IB_ID)) + 1;
                            UpdateEntity.IB_DATA_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_BALANCES.Where(m => m.IB_ID == db_table.Entity.IB_ID).FirstOrDefault();
                            UpdateEntity.IB_DATA_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IB_UPLOAD_MONTH = db_table.Entity.IB_UPLOAD_MONTH;
                        UpdateEntity.IB_CURRENCY = db_table.Entity.IB_CURRENCY;
                        UpdateEntity.IB_AD = db_table.Entity.IB_AD;
                        UpdateEntity.IB_STATEMENT = db_table.Entity.IB_STATEMENT;
                        UpdateEntity.IB_OPENING_BALANCE_OF_S1_DR = db_table.Entity.IB_OPENING_BALANCE_OF_S1_DR;
                        UpdateEntity.IB_OPENING_BALANCE_OF_S1_CR = db_table.Entity.IB_OPENING_BALANCE_OF_S1_CR;
                        UpdateEntity.IB_CLOSING_BALANCE_OF_S1_DR = db_table.Entity.IB_CLOSING_BALANCE_OF_S1_DR;
                        UpdateEntity.IB_CLOSING_BALANCE_OF_S1_CR = db_table.Entity.IB_CLOSING_BALANCE_OF_S1_CR;
                        UpdateEntity.IB_OPENING_BALANCE_OF_S4_DR = db_table.Entity.IB_OPENING_BALANCE_OF_S4_DR;
                        UpdateEntity.IB_OPENING_BALANCE_OF_S4_CR = db_table.Entity.IB_OPENING_BALANCE_OF_S4_CR;
                        UpdateEntity.IB_CLOSING_BALANCE_OF_S4_DR = db_table.Entity.IB_CLOSING_BALANCE_OF_S4_DR;
                        UpdateEntity.IB_CLOSING_BALANCE_OF_S4_CR = db_table.Entity.IB_CLOSING_BALANCE_OF_S4_CR;
                        UpdateEntity.IB_OPENING_BALANCE_OF_S6 = db_table.Entity.IB_OPENING_BALANCE_OF_S6;
                        UpdateEntity.IB_CLOSING_BALANCE_OF_S6 = db_table.Entity.IB_CLOSING_BALANCE_OF_S6;
                        UpdateEntity.IB_UPLOAD_MONTH = db_table.Entity.IB_UPLOAD_MONTH;
                        UpdateEntity.IB_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IB_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IB_CHECKER_ID = UpdateEntity.IB_MAKER_ID;
                            UpdateEntity.IB_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IB_CHECKER_ID = null;
                            UpdateEntity.IB_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_ITRS_BALANCES.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult BalancesFilter(ITRS_BALANCES_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Balances", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_BALANCES> List = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IB_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IB_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IB_ISAUTH == true || m.IB_MAKER_ID != SessionUser).OrderByDescending(m => m.IB_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Balances", edit);
        }
        [HttpPost]
        public ActionResult BalancesFilter(ITRS_BALANCES_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Balances", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_BALANCES> List = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IB_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IB_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IB_ISAUTH == true || m.IB_MAKER_ID != SessionUser).OrderByDescending(m => m.IB_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("Balances", edit);
        }
        [ChildActionOnly]
        public PartialViewResult BalancesView(ITRS_BALANCES_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_BALANCES> ViewData = new List<PSW_ITRS_BALANCES>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_DATA_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IB_DATA_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IB_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.IB_ISAUTH == true || m.IB_MAKER_ID != SessionUser).OrderByDescending(m => m.IB_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_BALANCES.Where(m => m.IB_ISAUTH == true || m.IB_MAKER_ID != SessionUser).OrderByDescending(m => m.IB_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeBalances(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Balances", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_BALANCES UpdateEntity = context.PSW_ITRS_BALANCES.Where(m => m.IB_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IB_ISAUTH = true;
                            UpdateEntity.IB_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IB_DATA_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = UpdateEntity.IB_STATEMENT + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectBalances(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Balances", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_BALANCES UpdateEntity = context.PSW_ITRS_BALANCES.Where(m => m.IB_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_BALANCES.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = UpdateEntity.IB_STATEMENT + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkBalances(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "Balances", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    int uploadmonth = 0;
                                    DateTime Month = DateTime.Now;
                                    PSW_ITRS_BALANCES Statement = new PSW_ITRS_BALANCES();
                                    if (row["Month"].ToString().Length > 0)
                                    {
                                        if (row["Month"] != null || row["Month"].ToString() != "")
                                        {
                                            uploadmonth = Convert.ToInt32(row["Month"]);
                                            Month = DateTime.FromOADate(uploadmonth);
                                        }
                                        decimal CheckDecimalValue = 0;
                                        string Currency = row["Currency"].ToString();
                                        string AD = row["AD"].ToString();
                                        string statement = row["Statement"].ToString();
                                        string OpeningBalanceofS1Dr = row["Dr S1 Opening Balance"].ToString();
                                        string OpeningBalanceofS1Cr = row["Cr S1 Opening Balance"].ToString();
                                        string ClosingBalanceofS1Dr = row["Dr S1 Closing Balance"].ToString();
                                        string ClosingBalanceofS1Cr = row["Cr S1 Closing Balance"].ToString();
                                        string OpeningBalanceofS4Dr = row["Dr S4 Opening Balance"].ToString();
                                        string OpeningBalanceofS4Cr = row["Cr S4 Opening Balance"].ToString();
                                        string ClosingBalanceofS4Dr = row["Dr S4 Closing Balance"].ToString();
                                        string ClosingBalanceofS4Cr = row["Cr S4 Closing Balance"].ToString();
                                        string OpeningBalanceofS6 = row["S6 Opening Balance"].ToString();
                                        string ClosingBalanceofS6 = row["S6 Closing Balance"].ToString();
                                        bool CheckIfExist = false;

                                        if (OpeningBalanceofS1Dr != "" && OpeningBalanceofS1Dr != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(OpeningBalanceofS1Dr);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_OPENING_BALANCE_OF_S1_DR == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else if (OpeningBalanceofS1Cr != "" && OpeningBalanceofS1Cr != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(OpeningBalanceofS1Cr);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_OPENING_BALANCE_OF_S1_CR == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else if (ClosingBalanceofS1Dr != "" && ClosingBalanceofS1Dr != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(ClosingBalanceofS1Dr);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_CLOSING_BALANCE_OF_S1_DR == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else if (ClosingBalanceofS1Cr != "" && ClosingBalanceofS1Cr != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(ClosingBalanceofS1Cr);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_CLOSING_BALANCE_OF_S1_CR == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else if (OpeningBalanceofS4Dr != "" && OpeningBalanceofS4Dr != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(OpeningBalanceofS4Dr);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_OPENING_BALANCE_OF_S4_DR == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else if (OpeningBalanceofS4Cr != "" && OpeningBalanceofS4Cr != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(OpeningBalanceofS4Cr);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_OPENING_BALANCE_OF_S4_CR == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else if (ClosingBalanceofS4Dr != "" && ClosingBalanceofS4Dr != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(ClosingBalanceofS4Dr);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_CLOSING_BALANCE_OF_S4_DR == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else if (ClosingBalanceofS4Cr != "" && ClosingBalanceofS4Cr != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(ClosingBalanceofS4Cr);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_CLOSING_BALANCE_OF_S4_CR == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else if (OpeningBalanceofS6 != "" && OpeningBalanceofS6 != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(OpeningBalanceofS6);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_OPENING_BALANCE_OF_S6 == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else if (ClosingBalanceofS6 != "" && ClosingBalanceofS6 != null)
                                        {
                                            CheckDecimalValue = Convert.ToDecimal(ClosingBalanceofS6);
                                            Statement = context.PSW_ITRS_BALANCES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IB_UPLOAD_MONTH) == System.Data.Entity.DbFunctions.TruncateTime(Month) && m.IB_CURRENCY == Currency && m.IB_AD == AD && m.IB_STATEMENT == statement && m.IB_CLOSING_BALANCE_OF_S6 == CheckDecimalValue).FirstOrDefault();
                                        }
                                        else
                                            Statement = null;
                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_BALANCES();
                                            Statement.IB_ID = Convert.ToInt32(context.PSW_ITRS_BALANCES.Max(m => (decimal?)m.IB_ID)) + 1;
                                            Statement.IB_DATA_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.IB_DATA_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Month"] != null || row["Month"].ToString() != "")
                                        {
                                            if (row["Currency"].ToString() != "" && row["Currency"] != null)
                                            {
                                                if (row["Statement"].ToString() != "" && row["Statement"] != null)
                                                {
                                                    if (row["AD"].ToString() != "" && row["AD"] != null)
                                                    {
                                                        Statement.IB_UPLOAD_MONTH = Month;
                                                        Statement.IB_CURRENCY = Currency;
                                                        Statement.IB_AD = AD;
                                                        Statement.IB_STATEMENT = statement;
                                                        if (OpeningBalanceofS1Dr != "" && OpeningBalanceofS1Dr != null)
                                                            Statement.IB_OPENING_BALANCE_OF_S1_DR = Convert.ToDecimal(OpeningBalanceofS1Dr);
                                                        if (OpeningBalanceofS1Cr != "" && OpeningBalanceofS1Cr != null)
                                                            Statement.IB_OPENING_BALANCE_OF_S1_CR = Convert.ToDecimal(OpeningBalanceofS1Cr);
                                                        if (ClosingBalanceofS1Dr != "" && ClosingBalanceofS1Dr != null)
                                                            Statement.IB_CLOSING_BALANCE_OF_S1_DR = Convert.ToDecimal(ClosingBalanceofS1Dr);
                                                        if (ClosingBalanceofS1Cr != "" && ClosingBalanceofS1Cr != null)
                                                            Statement.IB_CLOSING_BALANCE_OF_S1_CR = Convert.ToDecimal(ClosingBalanceofS1Cr);
                                                        if (OpeningBalanceofS4Dr != "" && OpeningBalanceofS4Dr != null)
                                                            Statement.IB_OPENING_BALANCE_OF_S4_DR = Convert.ToDecimal(OpeningBalanceofS4Dr);
                                                        if (OpeningBalanceofS4Cr != "" && OpeningBalanceofS4Cr != null)
                                                            Statement.IB_OPENING_BALANCE_OF_S4_CR = Convert.ToDecimal(OpeningBalanceofS4Cr);
                                                        if (ClosingBalanceofS4Dr != "" && ClosingBalanceofS4Dr != null)
                                                            Statement.IB_CLOSING_BALANCE_OF_S4_DR = Convert.ToDecimal(ClosingBalanceofS4Dr);
                                                        if (ClosingBalanceofS4Cr != "" && ClosingBalanceofS4Cr != null)
                                                            Statement.IB_CLOSING_BALANCE_OF_S4_CR = Convert.ToDecimal(ClosingBalanceofS4Cr);
                                                        if (OpeningBalanceofS6 != "" && OpeningBalanceofS6 != null)
                                                            Statement.IB_OPENING_BALANCE_OF_S6 = Convert.ToDecimal(OpeningBalanceofS6);
                                                        if (ClosingBalanceofS6 != "" && ClosingBalanceofS6 != null)
                                                            Statement.IB_CLOSING_BALANCE_OF_S6 = Convert.ToDecimal(ClosingBalanceofS6);
                                                        Statement.IB_MAKER_ID = Session["USER_ID"].ToString();
                                                        if (Statement.IB_MAKER_ID == "Admin123")
                                                        {
                                                            Statement.IB_CHECKER_ID = Session["USER_ID"].ToString();
                                                            Statement.IB_ISAUTH = true;
                                                        }
                                                        else
                                                        {
                                                            Statement.IB_CHECKER_ID = null;
                                                            Statement.IB_ISAUTH = false;
                                                        }
                                                        if (CheckIfExist == false)
                                                        {
                                                            context.PSW_ITRS_BALANCES.Add(Statement);
                                                        }
                                                        else
                                                        {
                                                            context.Entry(Statement).State = EntityState.Modified;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid AD : " + row["AD"].ToString() + " at Month : " + row["Month"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid Statement : " + row["Statement"].ToString() + " at Month : " + row["Month"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Currency : " + row["Currency"].ToString() + " at Month : " + row["Month"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Month : " + row["Month"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Beneficiary Requesting Bank
        public ActionResult BeneRequestingBank(PSW_ITRS_BENE_REQUESTING_BANK_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "BeneRequestingBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_BENEFICIARY_REQUESTING_BANK> List = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.BRB_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.BRB_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.BRB_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.BRB_ISAUTH == true || m.BRB_MAKER_ID != SessionUser).OrderByDescending(m => m.BRB_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => m.BRB_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult BeneRequestingBank(PSW_ITRS_BENE_REQUESTING_BANK_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "BeneRequestingBank", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_BENEFICIARY_REQUESTING_BANK UpdateEntity = new PSW_ITRS_BENEFICIARY_REQUESTING_BANK();

                        PSW_ITRS_BENEFICIARY_REQUESTING_BANK CheckIfExist = new PSW_ITRS_BENEFICIARY_REQUESTING_BANK();
                        if (db_table.Entity.BRB_ID == 0)
                        {
                            UpdateEntity.BRB_ID = Convert.ToInt32(context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Max(m => (decimal?)m.BRB_ID)) + 1;
                            UpdateEntity.BRB_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => m.BRB_ID == db_table.Entity.BRB_ID).FirstOrDefault();
                            UpdateEntity.BRB_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.BRB_BANK_CODE = db_table.Entity.BRB_BANK_CODE;
                        UpdateEntity.BRB_BANK_NAME = db_table.Entity.BRB_BANK_NAME;
                        UpdateEntity.BRB_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.BRB_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.BRB_CHECKER_ID = UpdateEntity.BRB_MAKER_ID;
                            UpdateEntity.BRB_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.BRB_CHECKER_ID = null;
                            UpdateEntity.BRB_ISAUTH = false;
                        }
                        UpdateEntity.BRB_STATUS = db_table.Entity.BRB_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult BeneRequestingBankFilter(PSW_ITRS_BENE_REQUESTING_BANK_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "BeneRequestingBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_BENEFICIARY_REQUESTING_BANK> List = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.BRB_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.BRB_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.BRB_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.BRB_ISAUTH == true || m.BRB_MAKER_ID != SessionUser).OrderByDescending(m => m.BRB_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("BeneRequestingBank", edit);
        }
        [HttpPost]
        public ActionResult BeneRequestingBankFilter(PSW_ITRS_BENE_REQUESTING_BANK_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "BeneRequestingBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_BENEFICIARY_REQUESTING_BANK> List = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.BRB_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.BRB_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.BRB_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.BRB_ISAUTH == true || m.BRB_MAKER_ID != SessionUser).OrderByDescending(m => m.BRB_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("BeneRequestingBank", edit);
        }
        [ChildActionOnly]
        public PartialViewResult BeneRequestingBankView(PSW_ITRS_BENE_REQUESTING_BANK_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_BENEFICIARY_REQUESTING_BANK> ViewData = new List<PSW_ITRS_BENEFICIARY_REQUESTING_BANK>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.BRB_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.BRB_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.BRB_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.BRB_ISAUTH == true || m.BRB_MAKER_ID != SessionUser).OrderByDescending(m => m.BRB_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => m.BRB_ISAUTH == true || m.BRB_MAKER_ID != SessionUser).OrderByDescending(m => m.BRB_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeBeneRequestingBank(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "BeneRequestingBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_BENEFICIARY_REQUESTING_BANK UpdateEntity = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => m.BRB_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.BRB_ISAUTH = true;
                            UpdateEntity.BRB_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.BRB_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Bank : " + UpdateEntity.BRB_BANK_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectBeneRequestingBank(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "BeneRequestingBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_BENEFICIARY_REQUESTING_BANK UpdateEntity = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => m.BRB_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Bank : " + UpdateEntity.BRB_BANK_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkBeneRequestingBank(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "BeneRequestingBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_BENEFICIARY_REQUESTING_BANK Statement = new PSW_ITRS_BENEFICIARY_REQUESTING_BANK();
                                    if (row["Name"].ToString().Length > 0)
                                    {
                                        string Bankname = row["Name"].ToString();
                                        string BankId = row["Bank Code"].ToString();
                                        int CountId = 0;
                                        if (BankId != null && BankId != "")
                                            CountId = Convert.ToInt32(BankId);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Where(m => m.BRB_BANK_NAME == Bankname).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_BENEFICIARY_REQUESTING_BANK();
                                            Statement.BRB_ID = Convert.ToInt32(context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Max(m => (decimal?)m.BRB_ID)) + 1;
                                            Statement.BRB_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.BRB_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Name"] != null || row["Name"].ToString() != "")
                                        {
                                            if (row["Bank Code"].ToString() != "" && row["Bank Code"] != null)
                                            {
                                                Statement.BRB_BANK_NAME = Bankname;
                                                Statement.BRB_BANK_CODE = CountId;
                                                Statement.BRB_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.BRB_MAKER_ID == "Admin123")
                                                {
                                                    Statement.BRB_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.BRB_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.BRB_CHECKER_ID = null;
                                                    Statement.BRB_ISAUTH = false;
                                                }
                                                Statement.BRB_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_BENEFICIARY_REQUESTING_BANK.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Bank Id : " + row["Bank Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Bank Name : " + row["Name"].ToString() + "  At Bank Id : " + row["Bank Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Relationship with Beneficiary
        public ActionResult RelationWithBene(PSW_ITRS_RELATIONSHIP_WITH_BENE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "RelationWithBene", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY> List = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.RWB_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.RWB_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.RWB_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.RWB_ISAUTH == true || m.RWB_MAKER_ID != SessionUser).OrderByDescending(m => m.RWB_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => m.RWB_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult RelationWithBene(PSW_ITRS_RELATIONSHIP_WITH_BENE_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "RelationWithBene", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY UpdateEntity = new PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY();

                        PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY CheckIfExist = new PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY();
                        if (db_table.Entity.RWB_ID == 0)
                        {
                            UpdateEntity.RWB_ID = Convert.ToInt32(context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Max(m => (decimal?)m.RWB_ID)) + 1;
                            UpdateEntity.RWB_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => m.RWB_ID == db_table.Entity.RWB_ID).FirstOrDefault();
                            UpdateEntity.RWB_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.RWB_BANK_CODE = db_table.Entity.RWB_BANK_CODE;
                        UpdateEntity.RWB_BANK_NAME = db_table.Entity.RWB_BANK_NAME;
                        UpdateEntity.RWB_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.RWB_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.RWB_CHECKER_ID = UpdateEntity.RWB_MAKER_ID;
                            UpdateEntity.RWB_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.RWB_CHECKER_ID = null;
                            UpdateEntity.RWB_ISAUTH = false;
                        }
                        UpdateEntity.RWB_STATUS = db_table.Entity.RWB_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult RelationWithBeneFilter(PSW_ITRS_RELATIONSHIP_WITH_BENE_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "RelationWithBene", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY> List = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.RWB_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.RWB_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.RWB_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.RWB_ISAUTH == true || m.RWB_MAKER_ID != SessionUser).OrderByDescending(m => m.RWB_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("RelationWithBene", edit);
        }
        [HttpPost]
        public ActionResult RelationWithBeneFilter(PSW_ITRS_RELATIONSHIP_WITH_BENE_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "RelationWithBene", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY> List = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.RWB_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.RWB_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.RWB_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.RWB_ISAUTH == true || m.RWB_MAKER_ID != SessionUser).OrderByDescending(m => m.RWB_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("RelationWithBene", edit);
        }
        [ChildActionOnly]
        public PartialViewResult RelationWithBeneView(PSW_ITRS_RELATIONSHIP_WITH_BENE_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY> ViewData = new List<PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.RWB_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.RWB_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.RWB_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.RWB_ISAUTH == true || m.RWB_MAKER_ID != SessionUser).OrderByDescending(m => m.RWB_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => m.RWB_ISAUTH == true || m.RWB_MAKER_ID != SessionUser).OrderByDescending(m => m.RWB_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeRelationWithBene(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "RelationWithBene", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY UpdateEntity = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => m.RWB_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.RWB_ISAUTH = true;
                            UpdateEntity.RWB_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.RWB_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Bank : " + UpdateEntity.RWB_BANK_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectRelationWithBene(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "RelationWithBene", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY UpdateEntity = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => m.RWB_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Bank : " + UpdateEntity.RWB_BANK_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkRelationWithBene(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "RelationWithBene", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY Statement = new PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY();
                                    if (row["Relationship with Beneficiary"].ToString().Length > 0)
                                    {
                                        string Bankname = row["Relationship with Beneficiary"].ToString();
                                        string BankId = row["Code"].ToString();
                                        int CountId = 0;
                                        if (BankId != null && BankId != "")
                                            CountId = Convert.ToInt32(BankId);
                                        else
                                            CountId = 0;

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => m.RWB_BANK_NAME == Bankname).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY();
                                            Statement.RWB_ID = Convert.ToInt32(context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Max(m => (decimal?)m.RWB_ID)) + 1;
                                            Statement.RWB_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.RWB_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Relationship with Beneficiary"] != null || row["Relationship with Beneficiary"].ToString() != "")
                                        {
                                            if (row["Code"].ToString() != "" && row["Code"] != null)
                                            {
                                                Statement.RWB_BANK_NAME = Bankname;
                                                Statement.RWB_BANK_CODE = CountId;
                                                Statement.RWB_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.RWB_MAKER_ID == "Admin123")
                                                {
                                                    Statement.RWB_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.RWB_ISAUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.RWB_CHECKER_ID = null;
                                                    Statement.RWB_ISAUTH = false;
                                                }
                                                Statement.RWB_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Bank Id : " + row["Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Bank Relationship with Beneficiary : " + row["Relationship with Beneficiary"].ToString() + "  At Bank Id : " + row["Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        public JsonResult AuthorizeBullkITRSRelationWithBene(/*DateTime? FromDate, DateTime? ToDate*/)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "RelationWithBene", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    string SessionUser = Session["USER_ID"].ToString();
                    try
                    {
                        List<PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY> BulkData = new List<PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY>();
                        BulkData = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => m.RWB_ISAUTH == false && m.RWB_MAKER_ID != SessionUser).OrderByDescending(m => m.RWB_ID).ToList();
                        if (BulkData.Count() > 0)
                        {
                            foreach (var item in BulkData)
                            {
                                PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY UpdateEntity = context.PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY.Where(m => m.RWB_ID == item.RWB_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.RWB_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.RWB_EDIT_DATETIME = DateTime.Now;
                                    UpdateEntity.RWB_ISAUTH = true;
                                    context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                                return Json("Record Authorized successfully");
                        }
                        else
                            message = "Unable to fetch data against the User Id : " + SessionUser;
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Lender Sector
        public ActionResult LenderSector(PSW_ITRS_LENDER_SECTOR_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "LenderSector", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_LENDER_SECTOR> List = context.PSW_ITRS_LENDER_SECTOR.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LS_IS_AUTH == true || m.LS_MAKER_ID != SessionUser).OrderByDescending(m => m.LS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult LenderSector(PSW_ITRS_LENDER_SECTOR_FILTER db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "LenderSector", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_ITRS_LENDER_SECTOR UpdateEntity = new PSW_ITRS_LENDER_SECTOR();

                        PSW_ITRS_LENDER_SECTOR CheckIfExist = new PSW_ITRS_LENDER_SECTOR();
                        if (db_table.Entity.LS_ID == 0)
                        {
                            UpdateEntity.LS_ID = Convert.ToInt32(context.PSW_ITRS_LENDER_SECTOR.Max(m => (decimal?)m.LS_ID)) + 1;
                            UpdateEntity.LS_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_ID == db_table.Entity.LS_ID).FirstOrDefault();
                            UpdateEntity.LS_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.LS_NAME = db_table.Entity.LS_NAME;
                        UpdateEntity.LS_LENDER_SECTOR = db_table.Entity.LS_LENDER_SECTOR;
                        UpdateEntity.LS_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.LS_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.LS_CHECKER_ID = UpdateEntity.LS_MAKER_ID;
                            UpdateEntity.LS_IS_AUTH = true;
                        }
                        else
                        {
                            UpdateEntity.LS_CHECKER_ID = null;
                            UpdateEntity.LS_IS_AUTH = false;
                        }
                        UpdateEntity.LS_STATUS = db_table.Entity.LS_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_ITRS_LENDER_SECTOR.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult LenderSectorFilter(PSW_ITRS_LENDER_SECTOR_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "LenderSector", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_LENDER_SECTOR> List = context.PSW_ITRS_LENDER_SECTOR.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LS_IS_AUTH == true || m.LS_MAKER_ID != SessionUser).OrderByDescending(m => m.LS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("LenderSector", edit);
        }
        [HttpPost]
        public ActionResult LenderSectorFilter(PSW_ITRS_LENDER_SECTOR_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "LenderSector", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_LENDER_SECTOR> List = context.PSW_ITRS_LENDER_SECTOR.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LS_IS_AUTH == true || m.LS_MAKER_ID != SessionUser).OrderByDescending(m => m.LS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                            else
                                message = "Select date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("LenderSector", edit);
        }
        [ChildActionOnly]
        public PartialViewResult LenderSectorView(PSW_ITRS_LENDER_SECTOR_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_LENDER_SECTOR> ViewData = new List<PSW_ITRS_LENDER_SECTOR>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_LENDER_SECTOR.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.LS_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.LS_IS_AUTH == true || m.LS_MAKER_ID != SessionUser).OrderByDescending(m => m.LS_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_IS_AUTH == true || m.LS_MAKER_ID != SessionUser).OrderByDescending(m => m.LS_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeLenderSector(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "LenderSector", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_LENDER_SECTOR UpdateEntity = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.LS_IS_AUTH = true;
                            UpdateEntity.LS_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.LS_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Lender Name : " + UpdateEntity.LS_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectLenderSector(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "LenderSector", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_LENDER_SECTOR UpdateEntity = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ITRS_LENDER_SECTOR.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Lender Name : " + UpdateEntity.LS_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkLenderSector(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "LenderSector", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_LENDER_SECTOR Statement = new PSW_ITRS_LENDER_SECTOR();
                                    if (row["Name"].ToString().Length > 0)
                                    {
                                        string LenderName = row["Name"].ToString();
                                        string LenderSector = row["Lender Sector"].ToString();

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_NAME == LenderName && m.LS_LENDER_SECTOR == LenderSector).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_LENDER_SECTOR();
                                            Statement.LS_ID = Convert.ToInt32(context.PSW_ITRS_LENDER_SECTOR.Max(m => (decimal?)m.LS_ID)) + 1;
                                            Statement.LS_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.LS_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Name"] != null || row["Name"].ToString() != "")
                                        {
                                            if (row["Lender Sector"].ToString() != "" && row["Lender Sector"] != null)
                                            {
                                                Statement.LS_NAME = LenderName;
                                                Statement.LS_LENDER_SECTOR = LenderSector;
                                                Statement.LS_MAKER_ID = Session["USER_ID"].ToString();
                                                if (Statement.LS_MAKER_ID == "Admin123")
                                                {
                                                    Statement.LS_CHECKER_ID = Session["USER_ID"].ToString();
                                                    Statement.LS_IS_AUTH = true;
                                                }
                                                else
                                                {
                                                    Statement.LS_CHECKER_ID = null;
                                                    Statement.LS_IS_AUTH = false;
                                                }
                                                Statement.LS_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_ITRS_LENDER_SECTOR.Add(Statement);
                                                }
                                                else
                                                {
                                                    context.Entry(Statement).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Lender Sector : " + row["Lender Sector"].ToString() + " at Name : " + row["Name"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Name : " + row["Name"].ToString() + "  At Lender Sector : " + row["Lender Sector"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        public JsonResult AuthorizeBullkITRSLenderSector(/*DateTime? FromDate, DateTime? ToDate*/)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "LenderSector", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    string SessionUser = Session["USER_ID"].ToString();
                    try
                    {
                        List<PSW_ITRS_LENDER_SECTOR> BulkData = new List<PSW_ITRS_LENDER_SECTOR>();
                        BulkData = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_IS_AUTH == false && m.LS_MAKER_ID != SessionUser).OrderByDescending(m => m.LS_ID).ToList();
                        if (BulkData.Count() > 0)
                        {
                            foreach (var item in BulkData)
                            {
                                PSW_ITRS_LENDER_SECTOR UpdateEntity = context.PSW_ITRS_LENDER_SECTOR.Where(m => m.LS_ID == item.LS_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.LS_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.LS_EDIT_DATETIME = DateTime.Now;
                                    UpdateEntity.LS_IS_AUTH = true;
                                    context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                                return Json("Record Authorized successfully");
                        }
                        else
                            message = "Unable to fetch data against the User Id : " + SessionUser;
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region DE003
        public ActionResult DE003Upload()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "DE003Upload", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public FileStreamResult BulkDE003Upload(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "DE003Upload", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTableDE003(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_ITRS_DE003 Statement = new PSW_ITRS_DE003();
                                    if (row["Transaction Ref No"].ToString().Length > 0)
                                    {
                                        string ReferenceNo = row["Transaction Ref No"].ToString();

                                        bool CheckIfExist = false;
                                        Statement = context.PSW_ITRS_DE003.Where(m => m.DE_TRANS_REF_NO == ReferenceNo).FirstOrDefault();

                                        if (Statement == null)
                                        {
                                            Statement = new PSW_ITRS_DE003();
                                            Statement.DE_ID = Convert.ToInt32(context.PSW_ITRS_DE003.Max(m => (decimal?)m.DE_ID)) + 1;
                                            Statement.DE_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Statement.DE_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Product Code"] != null || row["Product Code"].ToString() != "")
                                        {
                                            string ProductCode = row["Product Code"].ToString();
                                            if (row["Transaction Ref No"].ToString() != "" && row["Transaction Ref No"] != null)
                                            {
                                                if (row["Ccy Code"].ToString() != "" && row["Ccy Code"] != null)
                                                {
                                                    string CCY = row["Ccy Code"].ToString();
                                                    if (row["TXN Amount"].ToString() != "" && row["TXN Amount"] != null)
                                                    {
                                                        string TXNAmount = row["TXN Amount"].ToString();
                                                        if (row["Value Date"].ToString() != "" && row["Value Date"] != null)
                                                        {
                                                            int valuedate = Convert.ToInt32(row["Value Date"]);
                                                            DateTime ValueDate = DateTime.FromOADate(valuedate);
                                                            string MakerID = row["Maker Id"].ToString();
                                                            string CheckerID = row["Checker Id"].ToString();
                                                            string TotalTax = row["Total Tax"].ToString();
                                                            string TxnAccount = row["Txn Account"].ToString();
                                                            string CustomerName = row["Customer Name"].ToString();
                                                            string HomeBranch = row["Home Branch"].ToString();
                                                            string NTN = row["NTN#"].ToString();
                                                            Statement.DE_PRODUCT_CODE = ProductCode;
                                                            Statement.DE_TRANS_REF_NO = ReferenceNo;
                                                            Statement.DE_CURRENCY_CODE = CCY;
                                                            if (TXNAmount != null && TXNAmount != "")
                                                                Statement.DE_TXN_AMOUNT = Convert.ToDecimal(TXNAmount);
                                                            else
                                                                Statement.DE_TXN_AMOUNT = null;
                                                            Statement.DE_VALUE_DATE = ValueDate;
                                                            Statement.DE_DATA_MAKER_ID = MakerID;
                                                            Statement.DE_DATA_CHECKER_ID = CheckerID;
                                                            Statement.DE_TOTAL_TAX = TotalTax;
                                                            Statement.DE_TXT_ACCOUNT = TxnAccount;
                                                            Statement.DE_CUSTOMER_NAME = CustomerName;
                                                            Statement.DE_HOME_BRANCH = HomeBranch;
                                                            Statement.DE_NTN = NTN;
                                                            Statement.DE_MAKER_ID = Session["USER_ID"].ToString();
                                                            if (Statement.DE_MAKER_ID == "Admin123")
                                                            {
                                                                Statement.DE_CHECKER_ID = Session["USER_ID"].ToString();
                                                                Statement.DE_ISAUTH = true;
                                                            }
                                                            else
                                                            {
                                                                Statement.DE_CHECKER_ID = null;
                                                                Statement.DE_ISAUTH = false;
                                                            }
                                                            if (CheckIfExist == false)
                                                            {
                                                                context.PSW_ITRS_DE003.Add(Statement);
                                                            }
                                                            else
                                                            {
                                                                context.Entry(Statement).State = EntityState.Modified;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Value Date : " + row["Value Date"].ToString() + " at Transaction Ref No : " + row["Transaction Ref No"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid TXN Amount : " + row["TXN Amount"].ToString() + " at Transaction Ref No : " + row["Transaction Ref No"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid Ccy Code : " + row["Ccy Code"].ToString() + " at Transaction Ref No : " + row["Transaction Ref No"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Transaction Ref No : " + row["Transaction Ref No"].ToString() + " at Product Code : " + row["Product Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Product Code : " + row["Product Code"].ToString() + "  At Transaction Ref No : " + row["Transaction Ref No"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        public ActionResult DE003UploadFilter(PSW_ITRS_DE003_FILTER edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "DE003Upload", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_DE003> List = context.PSW_ITRS_DE003.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.DE_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.DE_ISAUTH == true || m.DE_MAKER_ID != SessionUser).OrderByDescending(m => m.DE_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("DE003Upload", edit);
        }
        [HttpPost]
        public ActionResult DE003UploadFilter(PSW_ITRS_DE003_FILTER edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "DE003Upload", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ITRS_DE003> List = context.PSW_ITRS_DE003.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.DE_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.DE_ISAUTH == true || m.DE_MAKER_ID != SessionUser).OrderByDescending(m => m.DE_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("DE003Upload", edit);
        }
        [ChildActionOnly]
        public PartialViewResult DE003UploadView(PSW_ITRS_DE003_FILTER Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ITRS_DE003> ViewData = new List<PSW_ITRS_DE003>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ITRS_DE003.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.DE_VALUE_DATE) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.DE_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.DE_ISAUTH == true || m.DE_MAKER_ID != SessionUser).OrderByDescending(m => m.DE_ID).ToList();
                        if (ViewData.Count() > 0)
                        {
                            return PartialView(ViewData);
                        }
                    }
                }
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeDE003Upload(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "DE003Upload", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ITRS_DE003 UpdateEntity = context.PSW_ITRS_DE003.Where(m => m.DE_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.DE_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.DE_EDIT_DATETIME = DateTime.Now;
                            UpdateEntity.DE_ISAUTH = true;
                            context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Transaction on Ref No : " + UpdateEntity.DE_TRANS_REF_NO + " is authorized sucessfully.";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult AuthorizeBulkDE003Upload(/*DateTime? FromDate, DateTime? ToDate*/)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ITRSSetup", "DE003Upload", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    string SessionUser = Session["USER_ID"].ToString();
                    try
                    {
                        List<PSW_ITRS_DE003> BulkData = new List<PSW_ITRS_DE003>();
                        BulkData = context.PSW_ITRS_DE003.Where(m => m.DE_ISAUTH == false && m.DE_MAKER_ID != SessionUser).OrderByDescending(m => m.DE_ID).ToList();
                        if (BulkData.Count() > 0)
                        {
                            foreach (var item in BulkData)
                            {
                                PSW_ITRS_DE003 UpdateEntity = context.PSW_ITRS_DE003.Where(m => m.DE_ID == item.DE_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.DE_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.DE_EDIT_DATETIME = DateTime.Now;
                                    UpdateEntity.DE_ISAUTH = true;
                                    context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                                return Json("Record Authorized successfully");
                        }
                        else
                            message = "Unable to fetch data against the User Id : " + SessionUser;
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion
        public FileStreamResult CreateLogFile(string Message)
        {
            var byteArray = Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
        public FileStreamResult CreateLogFileDE003(string Message)
        {
            var byteArray = Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = "DE003_" + DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
    }
}
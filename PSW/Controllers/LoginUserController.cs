﻿using PSW.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Controllers
{
    public class LoginUserController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: LoginUser
        public ActionResult AddUser(AssignRoles_Custom entity, int L = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("LoginUser", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (L != 0)
                        {
                            entity = new AssignRoles_Custom();
                            entity.Entity = context.PSW_LOGIN.Where(m => m.LOG_ID == L).FirstOrDefault();
                            if (entity != null)
                            {
                                List<int?> PreviousRoles = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == entity.Entity.LOG_USER_ID && m.LR_STATUS == true && m.LR_AMENDMENT_NO == entity.Entity.LOG_AMENDMENT_NO).Select(m => m.LR_ROLE_ID).ToList();
                                List<SelectListItem> items = new SelectList(context.PSW_ROLES.Where(m => m.R_NAME != "ADMIN" && m.R_ISAUTH == true && m.R_STATUS == "true").OrderBy(m => m.R_ID).ToList(), "R_ID", "R_NAME", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    int? CurrentRId = Convert.ToInt32(item.Value);
                                    if (PreviousRoles.Contains(CurrentRId))
                                        item.Selected = true;
                                }
                                entity.RolesList = items;
                                return View(entity);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + L;
                                return View();
                            }
                        }
                        else
                        {
                            entity = new AssignRoles_Custom();
                            entity.Entity = new PSW_LOGIN();
                            entity.Entity.LOG_ID = 0;
                            List<SelectListItem> items = new SelectList(context.PSW_ROLES.Where(m => m.R_NAME != "ADMIN" && m.R_ISAUTH == true && m.R_STATUS == "true").OrderBy(m => m.R_ID).ToList(), "R_ID", "R_NAME", 0).ToList();
                            entity.RolesList = items;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddUser(AssignRoles_Custom db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("LoginUser", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LOGIN UpdateEntity = new PSW_LOGIN();
                        PSW_LOGIN CheckIfExist = new PSW_LOGIN();

                        if (db_table.Entity.LOG_USER_STATUS == "Active" && db_table.SelectedRoles != null)
                        {
                            if (db_table.Entity.LOG_ID == 0)
                            {
                                CheckIfExist = context.PSW_LOGIN.Where(m => m.LOG_ID != db_table.Entity.LOG_ID && (m.LOG_USER_ID.ToLower() == db_table.Entity.LOG_USER_ID.ToLower())).FirstOrDefault();
                                if (CheckIfExist == null)
                                {
                                    UpdateEntity.LOG_ID = Convert.ToInt32(context.PSW_LOGIN.Max(m => (decimal?)m.LOG_ID)) + 1;
                                    UpdateEntity.LOG_DATA_ENTRY_DATETIME = DateTime.Now;
                                    UpdateEntity.LOG_USER_ID = db_table.Entity.LOG_USER_ID;
                                    UpdateEntity.LOG_USER_NAME = db_table.Entity.LOG_USER_NAME;
                                    UpdateEntity.LOG_EMAIL = db_table.Entity.LOG_EMAIL;
                                    UpdateEntity.LOG_USER_LAST_NAME = db_table.Entity.LOG_USER_LAST_NAME;
                                    UpdateEntity.LOG_USER_GE_ID = db_table.Entity.LOG_USER_GE_ID;
                                    UpdateEntity.LOG_RIST_ID = db_table.Entity.LOG_RIST_ID;
                                    UpdateEntity.LOG_USER_STATUS = db_table.Entity.LOG_USER_STATUS;
                                    UpdateEntity.LOG_MAKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.LOG_AMENDMENT_NO = 0;
                                    if (UpdateEntity.LOG_MAKER_ID == "Admin123")
                                    {
                                        UpdateEntity.LOG_CHECKER_ID = UpdateEntity.LOG_CHECKER_ID;
                                        UpdateEntity.LOG_ISAUTH = true;
                                    }
                                    else
                                    {
                                        UpdateEntity.LOG_CHECKER_ID = null;
                                        UpdateEntity.LOG_ISAUTH = false;
                                    }
                                    context.PSW_LOGIN.Add(UpdateEntity);
                                }
                                else
                                {
                                    message = "User ID " + CheckIfExist.LOG_USER_ID + " is already exist in the system";
                                }
                            }
                            else
                            {
                                //UpdateEntity = context.PSW_LOGIN.Where(m => m.LOG_ID == db_table.Entity.LOG_ID).FirstOrDefault();
                                UpdateEntity = new PSW_LOGIN();
                                UpdateEntity.LOG_ID = Convert.ToInt32(context.PSW_LOGIN.Max(m => (decimal?)m.LOG_ID)) + 1;
                                UpdateEntity.LOG_DATA_EDIT_DATETIME = DateTime.Now;
                                UpdateEntity.LOG_USER_ID = db_table.Entity.LOG_USER_ID;
                                UpdateEntity.LOG_USER_NAME = db_table.Entity.LOG_USER_NAME;
                                UpdateEntity.LOG_EMAIL = db_table.Entity.LOG_EMAIL;
                                UpdateEntity.LOG_USER_LAST_NAME = db_table.Entity.LOG_USER_LAST_NAME;
                                UpdateEntity.LOG_USER_GE_ID = db_table.Entity.LOG_USER_GE_ID;
                                UpdateEntity.LOG_RIST_ID = db_table.Entity.LOG_RIST_ID;
                                UpdateEntity.LOG_USER_STATUS = db_table.Entity.LOG_USER_STATUS;
                                UpdateEntity.LOG_MAKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.LOG_AMENDMENT_NO = Convert.ToInt32(context.PSW_LOGIN.Where(m => m.LOG_ID == db_table.Entity.LOG_ID).Max(m => (decimal?)m.LOG_AMENDMENT_NO)) + 1;
                                if (UpdateEntity.LOG_MAKER_ID == "Admin123")
                                {
                                    UpdateEntity.LOG_CHECKER_ID = UpdateEntity.LOG_CHECKER_ID;
                                    UpdateEntity.LOG_ISAUTH = true;
                                }
                                else
                                {
                                    UpdateEntity.LOG_CHECKER_ID = null;
                                    UpdateEntity.LOG_ISAUTH = false;
                                }
                                //context.Entry(UpdateEntity).State = EntityState.Modified;
                                context.PSW_LOGIN.Add(UpdateEntity);
                            }
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                #region Get Groups for Log Table
                                string TempNewUser = "";
                                string TempRemoveUser = "";
                                foreach (int RoleId in db_table.SelectedRoles)
                                {
                                    PSW_LOGIN_RIGHTS RoleTbl = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_ROLE_ID == RoleId && m.LR_USER_ID == UpdateEntity.LOG_USER_ID && m.LR_STATUS == true).FirstOrDefault();
                                    if (RoleTbl == null)
                                    {
                                        PSW_ROLES GetRoleName = context.PSW_ROLES.Where(m => m.R_ID == RoleId).FirstOrDefault();
                                        TempNewUser += GetRoleName.R_NAME + ",";
                                    }
                                }
                                #endregion

                                List<PSW_LOGIN_RIGHTS> DeActivatedRoles = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == db_table.Entity.LOG_USER_ID && !db_table.SelectedRoles.Contains(m.LR_ROLE_ID)).ToList();
                                if (DeActivatedRoles.Count > 0)
                                {
                                    foreach (PSW_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                    {
                                        PSW_ROLES RoleTbl = context.PSW_ROLES.Where(m => m.R_ID == EachRolesForUpdate.LR_ROLE_ID).FirstOrDefault();
                                        if (RoleTbl != null)
                                        {
                                            TempRemoveUser += RoleTbl.R_NAME + ",";
                                            //EachRolesForUpdate.LR_STATUS = false;
                                            //context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                        }
                                    }
                                }
                                int AssignRoleId = 0;
                                foreach (int? SelectedRolesId in db_table.SelectedRoles)
                                {
                                    if (SelectedRolesId != null)
                                    {
                                        PSW_LOGIN_RIGHTS EachRolesToUpdate = new PSW_LOGIN_RIGHTS();
                                        if (UpdateEntity.LOG_AMENDMENT_NO > 0)
                                            EachRolesToUpdate = null;
                                        else
                                            EachRolesToUpdate = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == db_table.Entity.LOG_USER_ID && m.LR_ROLE_ID == SelectedRolesId).FirstOrDefault();
                                        if (EachRolesToUpdate == null)
                                        {
                                            EachRolesToUpdate = new PSW_LOGIN_RIGHTS();
                                            if (AssignRoleId == 0)
                                            {
                                                EachRolesToUpdate.LR_ID = Convert.ToInt32(context.PSW_LOGIN_RIGHTS.Max(m => (decimal?)m.LR_ID)) + 1;
                                                AssignRoleId = EachRolesToUpdate.LR_ID;
                                                EachRolesToUpdate.LR_AMENDMENT_NO = UpdateEntity.LOG_AMENDMENT_NO;
                                            }
                                            else
                                            {
                                                AssignRoleId = (AssignRoleId + 1);
                                                EachRolesToUpdate.LR_ID = AssignRoleId;
                                                EachRolesToUpdate.LR_AMENDMENT_NO = UpdateEntity.LOG_AMENDMENT_NO;
                                            }
                                            EachRolesToUpdate.LR_ROLE_ID = SelectedRolesId;
                                            EachRolesToUpdate.LR_USER_ID = db_table.Entity.LOG_USER_ID;
                                            EachRolesToUpdate.LR_STATUS = true;
                                            EachRolesToUpdate.LR_MAKER_ID = Session["USER_ID"].ToString();
                                            EachRolesToUpdate.LR_CHECKER_ID = null;
                                            EachRolesToUpdate.LR_DATA_ENTRY_DATETIME = DateTime.Now;
                                            context.PSW_LOGIN_RIGHTS.Add(EachRolesToUpdate);
                                        }
                                        else
                                        {
                                            EachRolesToUpdate.LR_MAKER_ID = Session["USER_ID"].ToString();
                                            EachRolesToUpdate.LR_STATUS = true;
                                            EachRolesToUpdate.LR_DATA_EDIT_DATETIME = DateTime.Now;
                                            context.Entry(EachRolesToUpdate).State = EntityState.Modified;
                                        }
                                    }
                                }
                                RowsAffected = context.SaveChanges();
                                if (message == "")
                                {
                                    if (RowsAffected > 0)
                                    {
                                        if (Session["USER_ID"].ToString() != "Admin123")
                                        {
                                            RowsAffected = 0;
                                            PSW_USER_WITH_ROLE_LIST_VIEW AssignGroup = context.PSW_USER_WITH_ROLE_LIST_VIEW.Where(m => m.LOG_ID == UpdateEntity.LOG_ID).FirstOrDefault();
                                            PSW_USER_LOG UserLog = new PSW_USER_LOG();
                                            UserLog.ULOG_ID = Convert.ToInt32(context.PSW_USER_LOG.Max(m => (decimal?)m.ULOG_ID)) + 1;
                                            UserLog.ULOG_USER_ID = UpdateEntity.LOG_USER_ID;
                                            UserLog.ULOG_EMAIL = UpdateEntity.LOG_EMAIL;
                                            UserLog.ULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                            UserLog.ULOG_ADDED_BY = Session["USER_ID"].ToString();
                                            if (UserLog.ULOG_ADDED_BY == "Admin123")
                                            {
                                                UserLog.ULOG_ACTIVATED_BY = UserLog.ULOG_ADDED_BY;
                                                UserLog.ULOG_ACTIVATED_DATETIME = DateTime.Now;
                                            }
                                            else
                                            {
                                                UserLog.ULOG_ACTIVATED_BY = null;
                                                UserLog.ULOG_ACTIVATED_DATETIME = null;
                                            }
                                            UserLog.ULOG_ADDED_DATETIME = DateTime.Now;
                                            if (TempNewUser != "" && TempRemoveUser != "")
                                                UserLog.ULOG_STATUS = "New group " + TempNewUser + " assigned & group name " + TempRemoveUser + " is removed for the user : " + UpdateEntity.LOG_USER_ID;
                                            else if (TempNewUser != "" && TempRemoveUser == "")
                                                UserLog.ULOG_STATUS = "New group assigned to the user : " + UpdateEntity.LOG_USER_ID + ", New Assigned group " + TempNewUser;
                                            else if (TempNewUser == "" && TempRemoveUser != "")
                                                UserLog.ULOG_STATUS = "Following group removed for user : " + UpdateEntity.LOG_USER_ID + ", removed group " + TempRemoveUser;
                                            else if (UserLog.ULOG_ACTIVATED_BY == "Admin123")
                                                UserLog.ULOG_STATUS = "New group assigned to the user : " + UpdateEntity.LOG_USER_ID + ", New Assigned group " + TempNewUser + "User : " + UpdateEntity.LOG_USER_ID + " is activated";
                                            UserLog.ULOG_ENTRY_DATETIME = DateTime.Now;
                                            context.PSW_USER_LOG.Add(UserLog);
                                            RowsAffected = context.SaveChanges();
                                        }
                                        if (RowsAffected > 0)
                                            message = "Data Inserted Successfully 1 Affected";
                                    }
                                }
                            }
                            else
                            {
                                if (message == "")
                                {
                                    message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                }
                            }
                        }
                        else if (db_table.Entity.LOG_USER_STATUS == "Deactive" && db_table.SelectedRoles == null)
                        {
                            if (db_table.Entity.LOG_ID == 0)
                            {
                                CheckIfExist = context.PSW_LOGIN.Where(m => m.LOG_ID != db_table.Entity.LOG_ID && (m.LOG_USER_ID.ToLower() == db_table.Entity.LOG_USER_ID.ToLower())).FirstOrDefault();
                                if (CheckIfExist == null)
                                {
                                    UpdateEntity.LOG_ID = Convert.ToInt32(context.PSW_LOGIN.Max(m => (decimal?)m.LOG_ID)) + 1;
                                    UpdateEntity.LOG_DATA_ENTRY_DATETIME = DateTime.Now;
                                    UpdateEntity.LOG_USER_ID = db_table.Entity.LOG_USER_ID;
                                    UpdateEntity.LOG_USER_NAME = db_table.Entity.LOG_USER_NAME;
                                    UpdateEntity.LOG_EMAIL = db_table.Entity.LOG_EMAIL;
                                    UpdateEntity.LOG_USER_LAST_NAME = db_table.Entity.LOG_USER_LAST_NAME;
                                    UpdateEntity.LOG_USER_GE_ID = db_table.Entity.LOG_USER_GE_ID;
                                    UpdateEntity.LOG_RIST_ID = db_table.Entity.LOG_RIST_ID;
                                    UpdateEntity.LOG_USER_STATUS = db_table.Entity.LOG_USER_STATUS;
                                    UpdateEntity.LOG_MAKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.LOG_AMENDMENT_NO = 0;
                                    if (UpdateEntity.LOG_MAKER_ID == "Admin123")
                                    {
                                        UpdateEntity.LOG_CHECKER_ID = UpdateEntity.LOG_CHECKER_ID;
                                        UpdateEntity.LOG_ISAUTH = true;
                                    }
                                    else
                                    {
                                        UpdateEntity.LOG_CHECKER_ID = null;
                                        UpdateEntity.LOG_ISAUTH = false;
                                    }
                                    context.PSW_LOGIN.Add(UpdateEntity);
                                }
                                else
                                {
                                    message = "User ID " + CheckIfExist.LOG_USER_ID + " is already exist in the system";
                                }
                            }
                            else
                            {
                                //UpdateEntity = context.PSW_LOGIN.Where(m => m.LOG_ID == db_table.Entity.LOG_ID).FirstOrDefault();
                                UpdateEntity = new PSW_LOGIN();
                                UpdateEntity.LOG_ID = Convert.ToInt32(context.PSW_LOGIN.Max(m => (decimal?)m.LOG_ID)) + 1;
                                UpdateEntity.LOG_DATA_EDIT_DATETIME = DateTime.Now;
                                UpdateEntity.LOG_USER_ID = db_table.Entity.LOG_USER_ID;
                                UpdateEntity.LOG_USER_NAME = db_table.Entity.LOG_USER_NAME;
                                UpdateEntity.LOG_EMAIL = db_table.Entity.LOG_EMAIL;
                                UpdateEntity.LOG_USER_LAST_NAME = db_table.Entity.LOG_USER_LAST_NAME;
                                UpdateEntity.LOG_USER_GE_ID = db_table.Entity.LOG_USER_GE_ID;
                                UpdateEntity.LOG_RIST_ID = db_table.Entity.LOG_RIST_ID;
                                UpdateEntity.LOG_USER_STATUS = db_table.Entity.LOG_USER_STATUS;
                                UpdateEntity.LOG_MAKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.LOG_AMENDMENT_NO = Convert.ToInt32(context.PSW_LOGIN.Where(m => m.LOG_ID == db_table.Entity.LOG_ID).Max(m => (decimal?)m.LOG_AMENDMENT_NO)) + 1;
                                if (UpdateEntity.LOG_MAKER_ID == "Admin123")
                                {
                                    UpdateEntity.LOG_CHECKER_ID = UpdateEntity.LOG_CHECKER_ID;
                                    UpdateEntity.LOG_ISAUTH = true;
                                }
                                else
                                {
                                    UpdateEntity.LOG_CHECKER_ID = null;
                                    UpdateEntity.LOG_ISAUTH = false;
                                }
                                //context.Entry(UpdateEntity).State = EntityState.Modified;
                                context.PSW_LOGIN.Add(UpdateEntity);
                            }
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                if (db_table.SelectedRoles != null)
                                {
                                    List<PSW_LOGIN_RIGHTS> DeActivatedRoles = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == db_table.Entity.LOG_USER_ID && !db_table.SelectedRoles.Contains(m.LR_ROLE_ID)).ToList();
                                    if (DeActivatedRoles.Count > 0)
                                    {
                                        foreach (PSW_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                        {
                                            PSW_ROLES RoleTbl = context.PSW_ROLES.Where(m => m.R_ID == EachRolesForUpdate.LR_ROLE_ID).FirstOrDefault();
                                            if (RoleTbl != null)
                                            {
                                                EachRolesForUpdate.LR_STATUS = false;
                                                context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                            }
                                        }
                                    RowsAffected = context.SaveChanges();
                                    }
                                }
                                else
                                {
                                    List<PSW_LOGIN_RIGHTS> DeActivatedRoles = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == db_table.Entity.LOG_USER_ID).ToList();
                                    if (DeActivatedRoles.Count > 0)
                                    {
                                        foreach (PSW_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                        {
                                            PSW_ROLES RoleTbl = context.PSW_ROLES.Where(m => m.R_ID == EachRolesForUpdate.LR_ROLE_ID).FirstOrDefault();
                                            if (RoleTbl != null)
                                            {
                                                EachRolesForUpdate.LR_STATUS = false;
                                                context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                            }
                                        }
                                    RowsAffected = context.SaveChanges();
                                    }
                                }
                                if (message == "")
                                {
                                    if (RowsAffected > 0)
                                    {
                                        if (Session["USER_ID"].ToString() != "Admin123")
                                        {
                                            RowsAffected = 0;
                                            PSW_USER_WITH_ROLE_LIST_VIEW AssignGroup = context.PSW_USER_WITH_ROLE_LIST_VIEW.Where(m => m.LOG_ID == UpdateEntity.LOG_ID).FirstOrDefault();
                                            PSW_USER_LOG UserLog = new PSW_USER_LOG();
                                            UserLog.ULOG_ID = Convert.ToInt32(context.PSW_USER_LOG.Max(m => (decimal?)m.ULOG_ID)) + 1;
                                            UserLog.ULOG_USER_ID = UpdateEntity.LOG_USER_ID;
                                            UserLog.ULOG_EMAIL = UpdateEntity.LOG_EMAIL;
                                            UserLog.ULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                            UserLog.ULOG_ADDED_BY = Session["USER_ID"].ToString();
                                            if (UserLog.ULOG_ADDED_BY == "Admin123")
                                            {
                                                UserLog.ULOG_ACTIVATED_BY = UserLog.ULOG_ADDED_BY;
                                                UserLog.ULOG_ACTIVATED_DATETIME = DateTime.Now;
                                            }
                                            else
                                            {
                                                UserLog.ULOG_ACTIVATED_BY = null;
                                                UserLog.ULOG_ACTIVATED_DATETIME = null;
                                            }
                                            UserLog.ULOG_ADDED_DATETIME = DateTime.Now;
                                            UserLog.ULOG_STATUS = "Following user : " + UpdateEntity.LOG_USER_ID + " is de-active and all the roles are removed.";
                                            UserLog.ULOG_ENTRY_DATETIME = DateTime.Now;
                                            context.PSW_USER_LOG.Add(UserLog);
                                            RowsAffected = context.SaveChanges();
                                        }
                                        if (RowsAffected > 0)
                                            message = "Data Inserted Successfully 1 Affected";
                                    }
                                }
                            }
                            else
                            {
                                if (message == "")
                                {
                                    message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                }
                            }
                        }
                        else if (db_table.Entity.LOG_USER_STATUS == "Deactive" && db_table.SelectedRoles != null)
                        {
                            if (db_table.Entity.LOG_ID == 0)
                            {
                                CheckIfExist = context.PSW_LOGIN.Where(m => m.LOG_ID != db_table.Entity.LOG_ID && (m.LOG_USER_ID.ToLower() == db_table.Entity.LOG_USER_ID.ToLower())).FirstOrDefault();
                                if (CheckIfExist == null)
                                {
                                    UpdateEntity.LOG_ID = Convert.ToInt32(context.PSW_LOGIN.Max(m => (decimal?)m.LOG_ID)) + 1;
                                    UpdateEntity.LOG_DATA_ENTRY_DATETIME = DateTime.Now;
                                    UpdateEntity.LOG_USER_ID = db_table.Entity.LOG_USER_ID;
                                    UpdateEntity.LOG_USER_NAME = db_table.Entity.LOG_USER_NAME;
                                    UpdateEntity.LOG_EMAIL = db_table.Entity.LOG_EMAIL;
                                    UpdateEntity.LOG_USER_LAST_NAME = db_table.Entity.LOG_USER_LAST_NAME;
                                    UpdateEntity.LOG_USER_GE_ID = db_table.Entity.LOG_USER_GE_ID;
                                    UpdateEntity.LOG_RIST_ID = db_table.Entity.LOG_RIST_ID;
                                    UpdateEntity.LOG_USER_STATUS = db_table.Entity.LOG_USER_STATUS;
                                    UpdateEntity.LOG_MAKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.LOG_AMENDMENT_NO = 0;
                                    if (UpdateEntity.LOG_MAKER_ID == "Admin123")
                                    {
                                        UpdateEntity.LOG_CHECKER_ID = UpdateEntity.LOG_CHECKER_ID;
                                        UpdateEntity.LOG_ISAUTH = true;
                                    }
                                    else
                                    {
                                        UpdateEntity.LOG_CHECKER_ID = null;
                                        UpdateEntity.LOG_ISAUTH = false;
                                    }
                                    context.PSW_LOGIN.Add(UpdateEntity);
                                }
                                else
                                {
                                    message = "User ID " + CheckIfExist.LOG_USER_ID + " is already exist in the system";
                                }
                            }
                            else
                            {
                                //UpdateEntity = context.PSW_LOGIN.Where(m => m.LOG_ID == db_table.Entity.LOG_ID).FirstOrDefault();
                                UpdateEntity = new PSW_LOGIN();
                                UpdateEntity.LOG_ID = Convert.ToInt32(context.PSW_LOGIN.Max(m => (decimal?)m.LOG_ID)) + 1;
                                UpdateEntity.LOG_DATA_EDIT_DATETIME = DateTime.Now;
                                UpdateEntity.LOG_USER_ID = db_table.Entity.LOG_USER_ID;
                                UpdateEntity.LOG_USER_NAME = db_table.Entity.LOG_USER_NAME;
                                UpdateEntity.LOG_EMAIL = db_table.Entity.LOG_EMAIL;
                                UpdateEntity.LOG_USER_LAST_NAME = db_table.Entity.LOG_USER_LAST_NAME;
                                UpdateEntity.LOG_USER_GE_ID = db_table.Entity.LOG_USER_GE_ID;
                                UpdateEntity.LOG_RIST_ID = db_table.Entity.LOG_RIST_ID;
                                UpdateEntity.LOG_USER_STATUS = db_table.Entity.LOG_USER_STATUS;
                                UpdateEntity.LOG_MAKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.LOG_AMENDMENT_NO = Convert.ToInt32(context.PSW_LOGIN.Where(m => m.LOG_ID == db_table.Entity.LOG_ID).Max(m => (decimal?)m.LOG_AMENDMENT_NO)) + 1;
                                if (UpdateEntity.LOG_MAKER_ID == "Admin123")
                                {
                                    UpdateEntity.LOG_CHECKER_ID = UpdateEntity.LOG_CHECKER_ID;
                                    UpdateEntity.LOG_ISAUTH = true;
                                }
                                else
                                {
                                    UpdateEntity.LOG_CHECKER_ID = null;
                                    UpdateEntity.LOG_ISAUTH = false;
                                }
                                //context.Entry(UpdateEntity).State = EntityState.Modified;
                                context.PSW_LOGIN.Add(UpdateEntity);
                            }
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                #region Get Groups for Log Table
                                string TempNewUser = "";
                                string TempRemoveUser = "";
                                foreach (int RoleId in db_table.SelectedRoles)
                                {
                                    PSW_LOGIN_RIGHTS RoleTbl = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_ROLE_ID == RoleId && m.LR_USER_ID == UpdateEntity.LOG_USER_ID && m.LR_STATUS == true).FirstOrDefault();
                                    if (RoleTbl == null)
                                    {
                                        PSW_ROLES GetRoleName = context.PSW_ROLES.Where(m => m.R_ID == RoleId).FirstOrDefault();
                                        TempNewUser += GetRoleName.R_NAME + ",";
                                    }
                                }
                                #endregion

                                List<PSW_LOGIN_RIGHTS> DeActivatedRoles = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == db_table.Entity.LOG_USER_ID && !db_table.SelectedRoles.Contains(m.LR_ROLE_ID)).ToList();
                                if (DeActivatedRoles.Count > 0)
                                {
                                    foreach (PSW_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                    {
                                        PSW_ROLES RoleTbl = context.PSW_ROLES.Where(m => m.R_ID == EachRolesForUpdate.LR_ROLE_ID).FirstOrDefault();
                                        if (RoleTbl != null)
                                        {
                                            TempRemoveUser += RoleTbl.R_NAME + ",";
                                            //EachRolesForUpdate.LR_STATUS = false;
                                            //context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                        }
                                    }
                                }
                                int AssignRoleId = 0;
                                foreach (int? SelectedRolesId in db_table.SelectedRoles)
                                {
                                    if (SelectedRolesId != null)
                                    {
                                        PSW_LOGIN_RIGHTS EachRolesToUpdate = new PSW_LOGIN_RIGHTS();
                                        if (UpdateEntity.LOG_AMENDMENT_NO > 0)
                                            EachRolesToUpdate = null;
                                        else
                                            EachRolesToUpdate = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == db_table.Entity.LOG_USER_ID && m.LR_ROLE_ID == SelectedRolesId).FirstOrDefault();
                                        if (EachRolesToUpdate == null)
                                        {
                                            EachRolesToUpdate = new PSW_LOGIN_RIGHTS();
                                            if (AssignRoleId == 0)
                                            {
                                                EachRolesToUpdate.LR_ID = Convert.ToInt32(context.PSW_LOGIN_RIGHTS.Max(m => (decimal?)m.LR_ID)) + 1;
                                                AssignRoleId = EachRolesToUpdate.LR_ID;
                                                EachRolesToUpdate.LR_AMENDMENT_NO = UpdateEntity.LOG_AMENDMENT_NO;
                                            }
                                            else
                                            {
                                                AssignRoleId = (AssignRoleId + 1);
                                                EachRolesToUpdate.LR_ID = AssignRoleId;
                                                EachRolesToUpdate.LR_AMENDMENT_NO = UpdateEntity.LOG_AMENDMENT_NO;
                                            }
                                            EachRolesToUpdate.LR_ROLE_ID = SelectedRolesId;
                                            EachRolesToUpdate.LR_USER_ID = db_table.Entity.LOG_USER_ID;
                                            EachRolesToUpdate.LR_STATUS = true;
                                            EachRolesToUpdate.LR_MAKER_ID = Session["USER_ID"].ToString();
                                            EachRolesToUpdate.LR_CHECKER_ID = null;
                                            EachRolesToUpdate.LR_DATA_ENTRY_DATETIME = DateTime.Now;
                                            context.PSW_LOGIN_RIGHTS.Add(EachRolesToUpdate);
                                        }
                                        else
                                        {
                                            EachRolesToUpdate.LR_MAKER_ID = Session["USER_ID"].ToString();
                                            EachRolesToUpdate.LR_STATUS = true;
                                            EachRolesToUpdate.LR_DATA_EDIT_DATETIME = DateTime.Now;
                                            context.Entry(EachRolesToUpdate).State = EntityState.Modified;
                                        }
                                    }
                                }
                                RowsAffected = context.SaveChanges();
                                if (message == "")
                                {
                                    if (RowsAffected > 0)
                                    {
                                        if (Session["USER_ID"].ToString() != "Admin123")
                                        {
                                            RowsAffected = 0;
                                            PSW_USER_WITH_ROLE_LIST_VIEW AssignGroup = context.PSW_USER_WITH_ROLE_LIST_VIEW.Where(m => m.LOG_ID == UpdateEntity.LOG_ID).FirstOrDefault();
                                            PSW_USER_LOG UserLog = new PSW_USER_LOG();
                                            UserLog.ULOG_ID = Convert.ToInt32(context.PSW_USER_LOG.Max(m => (decimal?)m.ULOG_ID)) + 1;
                                            UserLog.ULOG_USER_ID = UpdateEntity.LOG_USER_ID;
                                            UserLog.ULOG_EMAIL = UpdateEntity.LOG_EMAIL;
                                            UserLog.ULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                            UserLog.ULOG_ADDED_BY = Session["USER_ID"].ToString();
                                            if (UserLog.ULOG_ADDED_BY == "Admin123")
                                            {
                                                UserLog.ULOG_ACTIVATED_BY = UserLog.ULOG_ADDED_BY;
                                                UserLog.ULOG_ACTIVATED_DATETIME = DateTime.Now;
                                            }
                                            else
                                            {
                                                UserLog.ULOG_ACTIVATED_BY = null;
                                                UserLog.ULOG_ACTIVATED_DATETIME = null;
                                            }
                                            UserLog.ULOG_ADDED_DATETIME = DateTime.Now;
                                            UserLog.ULOG_STATUS = "Following user : " + UpdateEntity.LOG_USER_ID + " is de-active and all the roles are removed.";
                                            UserLog.ULOG_ENTRY_DATETIME = DateTime.Now;
                                            context.PSW_USER_LOG.Add(UserLog);
                                            RowsAffected = context.SaveChanges();
                                        }
                                        if (RowsAffected > 0)
                                            message = "Data Inserted Successfully 1 Affected";
                                    }
                                }
                            }
                            else
                            {
                                if (message == "")
                                {
                                    message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                }
                            }
                        }
                        else
                        {
                            message = "Select atleast one role for the user : " + db_table.Entity.LOG_USER_ID;
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    AssignRoles_Custom entity = new AssignRoles_Custom();
                    entity.Entity = new PSW_LOGIN();
                    entity.Entity.LOG_ID = db_table.Entity.LOG_ID;
                    List<int?> PreviousRoles = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == db_table.Entity.LOG_USER_ID && m.LR_STATUS == true && m.LR_AMENDMENT_NO == entity.Entity.LOG_AMENDMENT_NO).Select(m => m.LR_ROLE_ID).ToList();
                    List<SelectListItem> items = new SelectList(context.PSW_ROLES.Where(m => m.R_NAME != "ADMIN" && m.R_ISAUTH == true && m.R_STATUS == "true").OrderBy(m => m.R_ID).ToList(), "R_ID", "R_NAME", 0).ToList();
                    foreach (SelectListItem item in items)
                    {
                        int? CurrentRId = Convert.ToInt32(item.Value);
                        if (PreviousRoles.Contains(CurrentRId))
                            item.Selected = true;
                    }
                    entity.RolesList = items;
                    return View(entity);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [ChildActionOnly]
        public PartialViewResult AddUserView()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_LOGIN_MASTER> ViewData = new List<PSW_LOGIN_MASTER>();
                ViewData = context.PSW_LOGIN_MASTER.Where(m => m.LOG_USER_ID != "Admin123" && m.LOG_USER_ID != SessionUser && (m.LOG_ISAUTH == true || m.LOG_MAKER_ID != SessionUser)).OrderByDescending(m => m.LOG_ID).ToList();
                if (ViewData.Count() > 0)
                {
                    ViewData = ViewData.GroupBy(m => m.LOG_USER_ID).Select(x => x.First()).ToList();
                }
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeAddUser(int LOG_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("LoginUser", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LOGIN UpdateEntity = context.PSW_LOGIN.Where(m => m.LOG_ID == LOG_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.LOG_ISAUTH = true;
                            UpdateEntity.LOG_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.LOG_DATA_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                if (Session["USER_ID"].ToString() != "Admin123")
                                {
                                    RowCount = 0;
                                    PSW_USER_WITH_ROLE_LIST_VIEW AssignGroup = context.PSW_USER_WITH_ROLE_LIST_VIEW.Where(m => m.LOG_ID == UpdateEntity.LOG_ID).FirstOrDefault();
                                    PSW_USER_LOG UserLog = new PSW_USER_LOG();
                                    UserLog.ULOG_ID = Convert.ToInt32(context.PSW_USER_LOG.Max(m => (decimal?)m.ULOG_ID)) + 1;
                                    UserLog.ULOG_USER_ID = UpdateEntity.LOG_USER_ID;
                                    UserLog.ULOG_EMAIL = UpdateEntity.LOG_EMAIL;
                                    UserLog.ULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                    UserLog.ULOG_ADDED_BY = UpdateEntity.LOG_MAKER_ID;
                                    UserLog.ULOG_ADDED_DATETIME = UpdateEntity.LOG_DATA_ENTRY_DATETIME;
                                    UserLog.ULOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                    UserLog.ULOG_ACTIVATED_DATETIME = DateTime.Now;
                                    UserLog.ULOG_STATUS = "Activated the user : " + UpdateEntity.LOG_USER_ID;
                                    UserLog.ULOG_ENTRY_DATETIME = DateTime.Now;
                                    context.PSW_USER_LOG.Add(UserLog);
                                    RowCount = context.SaveChanges();
                                }
                                if (RowCount > 0)
                                    message = "User ID : " + UpdateEntity.LOG_USER_ID + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOG_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeAddUser(int LOG_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("LoginUser", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LOGIN UpdateEntity = context.PSW_LOGIN.Where(m => m.LOG_ID == LOG_ID).FirstOrDefault();
                        #region Save Group Name Before Deleting
                        string GetGroupName = "";
                        PSW_LOGIN_MASTER GroupName = context.PSW_LOGIN_MASTER.Where(m => m.LOG_USER_ID == UpdateEntity.LOG_USER_ID).FirstOrDefault();
                        if (GroupName != null)
                        {
                            GetGroupName += GroupName.ASSIGNED_ROLES;
                        }
                        #endregion
                        if (UpdateEntity != null)
                        {
                            context.PSW_LOGIN.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                RowCount = 0;
                                List<PSW_LOGIN_RIGHTS> GetAssignGroup = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == UpdateEntity.LOG_USER_ID && m.LR_AMENDMENT_NO == UpdateEntity.LOG_AMENDMENT_NO).ToList();
                                if (GetAssignGroup.Count > 0)
                                {
                                    foreach (PSW_LOGIN_RIGHTS GroupToDelete in GetAssignGroup)
                                    {
                                        context.PSW_LOGIN_RIGHTS.Remove(GroupToDelete);
                                        RowCount += context.SaveChanges();
                                    }
                                }
                                if (Session["USER_ID"].ToString() != "Admin123")
                                {
                                    RowCount = 0;
                                    PSW_LOGIN_MASTER AssignGroup = context.PSW_LOGIN_MASTER.Where(m => m.LOG_ID == UpdateEntity.LOG_ID).FirstOrDefault();
                                    PSW_USER_LOG UserLog = new PSW_USER_LOG();
                                    UserLog.ULOG_ID = Convert.ToInt32(context.PSW_USER_LOG.Max(m => (decimal?)m.ULOG_ID)) + 1;
                                    UserLog.ULOG_USER_ID = UpdateEntity.LOG_USER_ID;
                                    UserLog.ULOG_EMAIL = UpdateEntity.LOG_EMAIL;
                                    UserLog.ULOG_ASSIGN_GROUP = GetGroupName;
                                    UserLog.ULOG_ADDED_BY = UpdateEntity.LOG_MAKER_ID;
                                    UserLog.ULOG_ADDED_DATETIME = UpdateEntity.LOG_DATA_EDIT_DATETIME;
                                    UserLog.ULOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                    UserLog.ULOG_ACTIVATED_DATETIME = DateTime.Now;
                                    UserLog.ULOG_STATUS = "Following user is Rejected/Deleted : " + UpdateEntity.LOG_USER_ID;
                                    UserLog.ULOG_ENTRY_DATETIME = DateTime.Now;
                                    context.PSW_USER_LOG.Add(UserLog);
                                    RowCount = context.SaveChanges();
                                }
                                if (RowCount > 0)
                                    message = "User Name : " + UpdateEntity.LOG_USER_NAME + " is Rejected";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOG_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        #region User Report
        public ActionResult UserActivity()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("LoginUser", "UserActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UserActivity(DateTime? FromDate, DateTime? EndDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("LoginUser", "UserActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && EndDate != null)
                        {
                            var Data = context.UserActivityReport(FromDate, EndDate).OrderByDescending(m => m.ULOG_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.ULOG_ADDED_BY != "Admin123" && m.ULOG_ACTIVATED_BY != "Admin123").ToList();
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + FromDate + " End date : " + EndDate;
                            }
                        }
                        else
                        {
                            if (FromDate == null && EndDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (EndDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        #endregion
    }
}
﻿//#define testing
#define production
using CrystalDecisions.CrystalReports.Engine;
using PSW.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Controllers
{
    public class MISReportsController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: MISReports

        public FileStreamResult CreateLogFile(string Message)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }

        #region EIF Reports
        public ActionResult EifDetail()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EifDetail", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EifDetail(DateTime? FromDate, DateTime? ToDate, string FIPaymentMode)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EifDetail", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                        #if testing
                        for (int i = 1; i <= 75; i++)
                        {
                            ReportDocument rd = new ReportDocument();
                            var EIFMASTER = context.PSW_SINGLE_EIF_VIEW.Where(m => m.EIF_BI_EIF_REQUEST_NO == "CBN-IMP-000090-24022022" && m.EIF_BI_AMENDMENT_NO == 0).FirstOrDefault();
                            var EIFDETAILS = context.PSW_EIF_HS_CODE_VIEW.Where(m => m.EIF_HC_EIF_REQUEST_NO == "CBN-IMP-000090-24022022" && m.EIF_HC_AMENDMENT_NO == 0).ToList();
                            rd.Load(Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "CRReports\\PSW_EIF_DETAIL_REPORT.rpt"));
                            rd.DataSourceConnections.Clear();
                            rd.SetDataSource(new[] { EIFMASTER });
                            rd.Subreports[0].DataSourceConnections.Clear();
                            rd.Subreports[0].SetDataSource(EIFDETAILS);
                            Stream ReportFileStream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                            string DateTimeNow = DateTime.Now.ToString("HHmmss");
                            string SavingPath = Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "TestReports\\CBN-IMP-000090-24022022-" + i + "-"+ DateTimeNow + ".pdf");
                            using (var fileStream = new FileStream(SavingPath, FileMode.Create, FileAccess.Write))
                            {
                                ReportFileStream.CopyTo(fileStream);
                            }
                            rd.Close();
                            rd.Dispose();
                            System.Threading.Thread.Sleep(500);
                        }
                        message = "10 files generated successfully";
                        #endif

                        #if production
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EIF_DETAIL_REPORT(FromDate, ToDate).OrderByDescending(m => m.EIF_BI_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                if(FIPaymentMode != "ALL")
                                    Data = Data.Where(m => m.APM_STATUS == FIPaymentMode).OrderByDescending(m => m.EIF_BI_ENTRY_DATETIME).ToList();
                                if (Data.Count() > 0)
                                    return View(Data);
                                else
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " and Payment Mode : " + FIPaymentMode;
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
#endif
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult LoadEIFHistory(string RequestNo = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("MISReports", "EifDetail", Session["USER_ID"].ToString()))
                {
                    List<PSW_EIF_BASIC_INFO> AllEIFList = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == RequestNo).ToList();
                    return PartialView(AllEIFList);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public ActionResult ExportEifReport(int AM = 0, string REQ_NO = "")
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    DateTime InvalidDate = Convert.ToDateTime("1900-01-01");
                    var EIFMASTER = context.PSW_SINGLE_EIF_VIEW.Where(m => m.EIF_BI_EIF_REQUEST_NO == REQ_NO && m.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
                    var EIFDETAILS = context.PSW_EIF_HS_CODE_VIEW_WITH_INVOICE_VALUE.Where(m => m.EIF_HC_EIF_REQUEST_NO == REQ_NO && m.EIF_HC_AMENDMENT_NO == AM).ToList();
                    ReportDocument rd = new ReportDocument();
                    rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EIF_DETAIL_REPORT.rpt"));
                    rd.DataSourceConnections.Clear();
                    rd.SetDataSource(new[] { EIFMASTER });
                    rd.Subreports[0].DataSourceConnections.Clear();
                    rd.Subreports[0].SetDataSource(EIFDETAILS);
                    if (EIFMASTER.EIF_BA_EXPIRY_DATE == InvalidDate)
                        rd.SetParameterValue("EIF_EXPIRY_DATE", "");
                    else
                    {
                        var shortDate = EIFMASTER.EIF_BA_EXPIRY_DATE.ToString("MMMM dd, yyyy");
                        rd.SetParameterValue("EIF_EXPIRY_DATE", shortDate);
                    }
                    if (EIFMASTER.EIF_BA_INNTENDED_PAYMENT_DATE == InvalidDate)
                        rd.SetParameterValue("EIF_INN_PAYMENT_DATE", "");
                    else
                    {
                        var shortDate = EIFMASTER.EIF_BA_INNTENDED_PAYMENT_DATE.ToString("MMMM dd, yyyy");
                        rd.SetParameterValue("EIF_INN_PAYMENT_DATE", shortDate);
                    }
                    if (EIFMASTER.EIF_BA_TRANSPORT_DOC_DATE == InvalidDate)
                        rd.SetParameterValue("EIF_TRANS_DOC_DATE", "");
                    else
                    {
                        var shortDate = EIFMASTER.EIF_BA_TRANSPORT_DOC_DATE.ToString("MMMM dd, yyyy");
                        rd.SetParameterValue("EIF_TRANS_DOC_DATE", shortDate);
                    }
                    if (EIFMASTER.EIF_BA_FINAL_DATE_OF_SHIPMENT == InvalidDate)
                        rd.SetParameterValue("EIF_FINAL_SHIP_DATE", "");
                    else
                    {
                        var shortDate = EIFMASTER.EIF_BA_FINAL_DATE_OF_SHIPMENT.ToString("MMMM dd, yyyy");
                        rd.SetParameterValue("EIF_FINAL_SHIP_DATE", shortDate);
                    }
                    if (EIFMASTER.EIF_BA_SBP_APPROVAL_DATE == InvalidDate)
                        rd.SetParameterValue("EIF_SBP_APPROVAL_DATE", "");
                    else
                    {
                        var shortDate = EIFMASTER.EIF_BA_SBP_APPROVAL_DATE.ToString("MMMM dd, yyyy");
                        rd.SetParameterValue("EIF_SBP_APPROVAL_DATE", shortDate);
                    }
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "PSW-EIF-REPORT(" + EIFMASTER.EIF_BI_EIF_REQUEST_NO + ").pdf");
                }
                else
                {
                    return RedirectToAction("Index", "Authentication");
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured :" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception : " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
                return CreateLogFile(message);
            }
        }
        public ActionResult EifBDADetail()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EifBDADetail", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EifBDADetail(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EifBDADetail", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EIF_BDA_REPORT(FromDate, ToDate).OrderByDescending(m => m.EIF_BDA_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult LoadEIFBDAHistory(string RequestNo = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("MISReports", "EifBDADetail", Session["USER_ID"].ToString()))
                {
                    List<PSW_EIF_BDA> AllEIFList = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_EIF_REQUEST_NO == RequestNo).ToList();
                    return PartialView(AllEIFList);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public ActionResult ExportEifBdaReport(int AM = 0, string REQ_NO = "", string BDA_NO = "")
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    if (DAL.CheckFunctionValidity("MISReports", "EifBDADetail", Session["USER_ID"].ToString()))
                    {
                        var EIFBDA = context.PSW_SINGLE_EIF_BDA_VIEW.Where(m => m.EIF_BDA_EIF_REQUEST_NO == REQ_NO && m.EIF_BDA_AMENDMENT_NO == AM && m.EIF_BDA_NO == BDA_NO).FirstOrDefault();
                        ReportDocument rd = new ReportDocument();
                        rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EIF_BDA_REPORT.rpt"));
                        rd.DataSourceConnections.Clear();
                        rd.SetDataSource(new[] { EIFBDA });

                        Response.Buffer = false;
                        Response.ClearContent();
                        Response.ClearHeaders();

                        Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        stream.Seek(0, SeekOrigin.Begin);
                        return File(stream, "application/pdf", "PSW-EIF-BDA-(" + EIFBDA.EIF_BDA_EIF_REQUEST_NO + ").pdf");
                    }
                    else
                    {
                        return RedirectToAction("UnAuthorizedUrl", "Error");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Authentication");
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured :" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception : " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
                return CreateLogFile(message);
            }
        }
        #endregion

        #region EEF Reports
        public ActionResult EefDetail()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EefDetail", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EefDetail(DateTime? FromDate, DateTime? ToDate, string FIPaymentMode)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EefDetail", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EEF_DETAIL_REPORT(FromDate, ToDate).OrderByDescending(m => m.FDI_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                if (FIPaymentMode != "ALL")
                                    Data = Data.Where(m => m.APM_STATUS == FIPaymentMode).OrderByDescending(m => m.FDI_ENTRY_DATETIME).ToList();
                                if (Data.Count() > 0)
                                    return View(Data);
                                else
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " Payment Mode : " + FIPaymentMode;
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }

        public PartialViewResult LoadEEFHistory(string RequestNo = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("MISReports", "EefDetail", Session["USER_ID"].ToString()))
                {
                    List<PSW_FORM_E_DOCUMENT_INFO> AllEEFList = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == RequestNo).ToList();
                    return PartialView(AllEEFList);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }

        public ActionResult ExportEefReport(int AM = 0, string REQ_NO = "")
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    //if (DAL.CheckFunctionValidity("MISReports", "EefDetail", Session["USER_ID"].ToString()))
                    //{
                        var EEFMASTER = context.PSW_SINGLE_EEF_VIEW.Where(m => m.FDI_FORM_E_NO == REQ_NO && m.FDI_AMENDMENT_NO == AM).FirstOrDefault();
                        var EEFDETAILS = context.PSW_EEF_HS_CODE_VIEW.Where(m => m.EEF_HC_EEF_REQUEST_NO == REQ_NO && m.EEF_HC_AMENDMENT_NO == AM).ToList();
                        ReportDocument rd = new ReportDocument();
                        rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_EEF_DETAIL_REPORT.rpt"));
                        rd.DataSourceConnections.Clear();
                        rd.SetDataSource(new[] { EEFMASTER });
                        rd.Subreports[0].DataSourceConnections.Clear();
                        rd.Subreports[0].SetDataSource(EEFDETAILS);

                        Response.Buffer = false;
                        Response.ClearContent();
                        Response.ClearHeaders();

                        Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        stream.Seek(0, SeekOrigin.Begin);
                        return File(stream, "application/pdf", "PSW-EEF-REPORT(" + EEFMASTER.FDI_FORM_E_NO + ").pdf");
                    //}
                    //else
                    //{
                    //    return RedirectToAction("UnAuthorizedUrl", "Error");
                    //}
                }
                else
                {
                    return RedirectToAction("Index", "Authentication");
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured :" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception : " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
                return CreateLogFile(message);
            }
        }

        public ActionResult EefBCADetail()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EefBCADetail", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EefBCADetail(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EefBCADetail", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EEF_BCA_REPORT(FromDate, ToDate).OrderByDescending(m => m.BCAD_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }

        #endregion

        #region Request n Response
        public ActionResult RequestNResponse()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "RequestNResponse", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult RequestNResponse(DateTime? FromDate, DateTime? EndDate, string ProcessCode = "0")
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "RequestNResponse", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && EndDate != null)
                        {
                            string fromdate = Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd");
                            string enddate = Convert.ToDateTime(EndDate, System.Globalization.CultureInfo.CurrentCulture).ToString("yyyyMMdd");
                            var Data = context.PSW_MESSAGES_REPORT(Convert.ToDecimal(fromdate), Convert.ToDecimal(enddate), ProcessCode).OrderByDescending(m => m.timestamp).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(EndDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && EndDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (EndDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public PartialViewResult LoadRequest(string MSG_ID = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("MISReports", "RequestNResponse", Session["USER_ID"].ToString()))
                {
                    PSW_MESSAGES Data = context.PSW_MESSAGES.Where(m => m.messageId == MSG_ID).FirstOrDefault();
                    return PartialView(Data);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult LoadResponse(string MSG_ID = "")
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("MISReports", "RequestNResponse", Session["USER_ID"].ToString()))
                {
                    PSW_MESSAGES Data = context.PSW_MESSAGES.Where(m => m.messageId == MSG_ID).FirstOrDefault();
                    return PartialView(Data);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        #endregion

        #region IMPORT PAYMENT REPORT
        public ActionResult ImportPayment()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ImportPayment", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ImportPayment(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ImportPayment", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_IMPORT_PAYMENT_REPORT(FromDate, ToDate).OrderByDescending(m => m.EIF_BDA_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region BDA Marking
        public ActionResult BDAMarking()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "BDAMarking", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult BDAMarking(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "BDAMarking", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EIF_BDA_MARKING(FromDate, ToDate).OrderByDescending(m => m.EIF_BDA_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region BCA Marking
        public ActionResult BCAMarking()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "BCAMarking", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult BCAMarking(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "BCAMarking", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EEF_BCA_MARKING(FromDate, ToDate).OrderByDescending(m => m.BCAD_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region Export Realized Bills
        public ActionResult ExportRealizedBills()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportRealizedBills", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ExportRealizedBills(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportRealizedBills", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EXPORT_REALIZED_BILLS(FromDate, ToDate).OrderByDescending(m => m.FDI_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
#endregion

        #region Export Advanced Payment Uitilization
        public ActionResult ExportAdvancePayment()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportAdvancePayment", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ExportAdvancePayment(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportAdvancePayment", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EXPORT_ADVANCE_PAYMENT_UITILIZATION(FromDate, ToDate).OrderByDescending(m => m.FDI_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        public ActionResult StatementEAP()
        {
            return View();
        }
        public ActionResult UtilizationAPV()
        {
            return View();
        }

        #region FORM E and Goods Declaration
        public ActionResult FormEGoodsDec()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportRealizedBills", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult FormEGoodsDec(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportRealizedBills", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_FORM_E_GOODS_DECLARATION(FromDate, ToDate).OrderByDescending(m => m.FDI_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region Advanced Payment Uitilization
        public ActionResult AdvancePaymentGDBasis()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "AdvancePaymentGDBasis", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AdvancePaymentGDBasis(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "AdvancePaymentGDBasis", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_ADVANCE_PAYMENT_WHERE_GD_FILED(FromDate, ToDate).OrderByDescending(m => m.EIF_BI_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region ADVANCE PAYMENT WITHOUT GD FILLED
        public ActionResult AdvancePaymentWithoutGDFilled()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "AdvancePaymentWithoutGDFilled", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AdvancePaymentWithoutGDFilled(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "AdvancePaymentWithoutGDFilled", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_ADVANCE_PAYMENT_WHERE_GD_NOT_FILED(FromDate, ToDate).OrderByDescending(m => m.EIF_BI_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region ADVANCE PAYMENT WITHOUT GD FILLED
        public ActionResult OpenAccountBDABasis()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "OpenAccountBDABasis", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult OpenAccountBDABasis(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "OpenAccountBDABasis", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_OPEN_ACCONT_WHERE_NO_BDA_FILED(FromDate, ToDate).OrderByDescending(m => m.EIF_BI_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region FORM-E REPORT-FULLY REALIZED/DISCOUNT SETTLED
        public ActionResult FormE_FRDS()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "FormE_FRDS", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult FormE_FRDS(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "FormE_FRDS", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_FORM_E_FULLY_REALIZED_DISCOUNT_SETTLED(FromDate, ToDate).OrderByDescending(m => m.FDI_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region SBP Reportings
        public ActionResult SBPReport()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "SBPReport", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult SBPReport(DateTime? FromDate, DateTime? ToDate, string FINO, string FIType, string FI_GD)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "SBPReport", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        SBPReportingFI_GD entity = new SBPReportingFI_GD();
                        if (FromDate != null && ToDate != null && FINO != "" && FIType != null && FI_GD != null)
                        {
                            if (FIType == "Import" && FI_GD == "FI")
                            {
                                entity.FI_IMPORT = context.PSW_EIF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate) && m.EIF_BI_EIF_REQUEST_NO == FINO).OrderByDescending(m => m.EIF_BI_ID).ToList();
                                if (entity.FI_IMPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " and Reference No : " + FINO;
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else if (FIType == "Import" && FI_GD == "GD")
                            {
                                entity.GD_IMPORT = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate) && m.EIGDGI_GD_NO == FINO).OrderByDescending(m => m.EIGDGI_ID).ToList();
                                if (entity.GD_IMPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " and Reference No : " + FINO;
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else if (FIType == "Export" && FI_GD == "FI")
                            {
                                entity.FI_EXPORT = context.PSW_EEF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate) && m.FDI_FORM_E_NO == FINO).OrderByDescending(m => m.FDI_ID).ToList();
                                if (entity.FI_EXPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " and Reference No : " + FINO;
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else if (FIType == "Export" && FI_GD == "GD")
                            {
                                entity.GD_EXPORT = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate) && m.EFGDGI_GD_NO == FINO).OrderByDescending(m => m.EFGDGI_ID).ToList();
                                if (entity.GD_EXPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " and Reference No : " + FINO;
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " and Reference No : " + FINO;
                            }
                        }
                        else if (FromDate != null && ToDate != null && FIType != null && FI_GD != null)
                        {
                            if (FIType == "Import" && FI_GD == "FI")
                            {
                                entity.FI_IMPORT = context.PSW_EIF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIF_BI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).OrderByDescending(m => m.EIF_BI_ID).ToList();
                                if (entity.FI_IMPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else if (FIType == "Import" && FI_GD == "GD")
                            {
                                entity.GD_IMPORT = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EIGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).OrderByDescending(m => m.EIGDGI_ID).ToList();
                                if (entity.GD_IMPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else if (FIType == "Export" && FI_GD == "FI")
                            {
                                entity.FI_EXPORT = context.PSW_EEF_MASTER_DETAILS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).OrderByDescending(m => m.FDI_ID).ToList();
                                if (entity.FI_EXPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else if (FIType == "Export" && FI_GD == "GD")
                            {
                                entity.GD_EXPORT = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EFGDGI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).OrderByDescending(m => m.EFGDGI_ID).ToList();
                                if (entity.GD_EXPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else if (FromDate == null && ToDate == null && FINO != "" && FIType != null && FI_GD != null)
                        {
                            if (FIType == "Import" && FI_GD == "FI")
                            {
                                entity.FI_IMPORT = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == FINO).OrderByDescending(m => m.EIF_BI_ID).ToList();
                                if (entity.FI_IMPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On Reference No : " + FINO;
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else if (FIType == "Import" && FI_GD == "GD")
                            {
                                entity.GD_IMPORT = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO == FINO).OrderByDescending(m => m.EIGDGI_ID).ToList();
                                if (entity.GD_IMPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On Reference No : " + FINO;
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else if (FIType == "Export" && FI_GD == "FI")
                            {
                                entity.FI_EXPORT = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == FINO).OrderByDescending(m => m.FDI_ID).ToList();
                                if (entity.FI_EXPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On Reference No : " + FINO;
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else if (FIType == "Export" && FI_GD == "GD")
                            {
                                entity.GD_EXPORT = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_GD_NO == FINO).OrderByDescending(m => m.EFGDGI_ID).ToList();
                                if (entity.GD_EXPORT.Count == 0)
                                {
                                    message = "No Data found in the system : On Reference No : " + FINO;
                                    return View(entity);
                                }
                                return View(entity);
                            }
                            else
                            {
                                message = "No Data found in the system : On Reference No : " + FINO;
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                        return View(entity);
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public ActionResult ExportSBPReports(string FI_GD_NO, string REQUEST_TYPE, string FI_GD)
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    if (REQUEST_TYPE == "Import" && FI_GD == "FI")
                    {
                        PSW_EIF_MASTER_DETAILS EIF_FI = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == FI_GD_NO).FirstOrDefault();
                        if (EIF_FI != null)
                        {
                            ReportDocument rd = new ReportDocument();
                            var EIFMASTER = context.PSW_SINGLE_EIF_VIEW.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_FI.EIF_BI_EIF_REQUEST_NO && m.EIF_BI_AMENDMENT_NO == EIF_FI.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                            if (EIFMASTER != null)
                            {
                                DateTime InnDate = Convert.ToDateTime("1900-01-01");
                                if (EIFMASTER.EIF_BA_INNTENDED_PAYMENT_DATE != null)
                                {
                                    if (EIFMASTER.EIF_BA_INNTENDED_PAYMENT_DATE == InnDate)
                                        rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_FI_IMPORT_WITHOUT_PAYMENT_DATE.rpt"));
                                    else
                                        rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_FI_IMPORT.rpt"));
                                }
                                else
                                    rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_FI_IMPORT.rpt"));
                            }
                            else
                                rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_FI_IMPORT.rpt"));
                            var EIFDETAILS = context.PSW_EIF_HS_CODE_VIEW.Where(m => m.EIF_HC_EIF_REQUEST_NO == EIF_FI.EIF_BI_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == EIF_FI.EIF_BI_AMENDMENT_NO).ToList();
                            rd.DataSourceConnections.Clear();
                            rd.SetDataSource(new[] { EIFMASTER });
                            rd.Subreports[0].DataSourceConnections.Clear();
                            rd.Subreports[0].SetDataSource(EIFDETAILS);

                            Response.Buffer = false;
                            Response.ClearContent();
                            Response.ClearHeaders();

                            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                            stream.Seek(0, SeekOrigin.Begin);
                            return File(stream, "application/pdf", "PSW-EIF(" + EIF_FI.EIF_BI_EIF_REQUEST_NO + ").pdf");
                        }
                    }
                    else if (REQUEST_TYPE == "Import" && FI_GD == "GD")
                    {
                        PSW_EIF_GD_GENERAL_INFO_LIST ImportGd = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_GD_NO == FI_GD_NO).FirstOrDefault();
                        if (ImportGd != null)
                        {
                            var EIFGDDETAILS = new List<PSW_GD_IMPORT_FI_VIEW>();
                            PSW_EIF_MASTER_DETAILS ImportFi = new PSW_EIF_MASTER_DETAILS();
                            if (ImportGd.EIGDGI_EIF_REQUEST_NO != null)
                                ImportFi = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == ImportGd.EIGDGI_EIF_REQUEST_NO).FirstOrDefault();

                            var EIFGDMASTER = context.PSW_GD_IMPORT_SINGLE_VIEW.Where(m => m.EIGDGI_GD_NO == ImportGd.EIGDGI_GD_NO && m.EIGDGI_AMENDMENT_NO == ImportGd.EIGDGI_AMENDMENT_NO).FirstOrDefault();
                            if (ImportFi != null)
                                EIFGDDETAILS = context.PSW_GD_IMPORT_FI_VIEW.Where(m => m.EIF_BI_EIF_REQUEST_NO == ImportFi.EIF_BI_EIF_REQUEST_NO && m.EIF_BI_AMENDMENT_NO == ImportFi.EIF_BI_AMENDMENT_NO).ToList();
                            else
                                EIFGDDETAILS = new List<PSW_GD_IMPORT_FI_VIEW>();
                            var EIFGDHSCode = context.PSW_GD_IMPORT_HS_CODE_VIEW.Where(m => m.IHC_GD_NO == ImportGd.EIGDGI_GD_NO && m.IHC_AMENDMENT_NO == ImportGd.EIGDGI_AMENDMENT_NO).ToList();
                            ReportDocument rd = new ReportDocument();
                            rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_GD_IMPORT.rpt"));
                            rd.DataSourceConnections.Clear();
                            rd.SetDataSource(new[] { EIFGDMASTER });
                            rd.Subreports[0].DataSourceConnections.Clear();
                            rd.Subreports[0].SetDataSource(EIFGDDETAILS);
                            rd.Subreports[1].DataSourceConnections.Clear();
                            rd.Subreports[1].SetDataSource(EIFGDHSCode);

                            Response.Buffer = false;
                            Response.ClearContent();
                            Response.ClearHeaders();

                            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                            stream.Seek(0, SeekOrigin.Begin);
                            return File(stream, "application/pdf", "PSW-IMPORT-GD(" + ImportGd.EIGDGI_GD_NO + ").pdf");
                        }
                    }
                    else if (REQUEST_TYPE == "Export" && FI_GD == "FI")
                    {
                        PSW_EEF_MASTER_DETAILS EEF_FI = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == FI_GD_NO).FirstOrDefault();
                        if (EEF_FI != null)
                        {
                            var EEFMASTER = context.PSW_SINGLE_EEF_VIEW.Where(m => m.FDI_FORM_E_NO == EEF_FI.FDI_FORM_E_NO && m.FDI_AMENDMENT_NO == EEF_FI.FDI_AMENDMENT_NO).FirstOrDefault();
                            var EEFDETAILS = context.PSW_EEF_HS_CODE_VIEW.Where(m => m.EEF_HC_EEF_REQUEST_NO == EEF_FI.FDI_FORM_E_NO && m.EEF_HC_AMENDMENT_NO == EEF_FI.FDI_AMENDMENT_NO).ToList();
                            ReportDocument rd = new ReportDocument();
                            rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_FI_EXPORT.rpt"));
                            rd.DataSourceConnections.Clear();
                            rd.SetDataSource(new[] { EEFMASTER });
                            rd.Subreports[0].DataSourceConnections.Clear();
                            rd.Subreports[0].SetDataSource(EEFDETAILS);

                            Response.Buffer = false;
                            Response.ClearContent();
                            Response.ClearHeaders();

                            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                            stream.Seek(0, SeekOrigin.Begin);
                            return File(stream, "application/pdf", "PSW-EEF(" + EEF_FI.FDI_FORM_E_NO + ").pdf");
                        }
                    }
                    else if (REQUEST_TYPE == "Export" && FI_GD == "GD")
                    {
                        PSW_EEF_GD_GENERAL_INFO_LIST ExportGd = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(m => m.EFGDGI_GD_NO == FI_GD_NO).FirstOrDefault();
                        if (ExportGd != null)
                        {
                            PSW_GD_ASSIGN_EFORMS GetEEFDetail = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_GD_NO == ExportGd.EFGDGI_GD_NO && m.GDAEF_AMENDMENT_NO == ExportGd.EFGDGI_AMENDMENT_NO).FirstOrDefault();
                            if (GetEEFDetail != null)
                            {
                                if (GetEEFDetail.GDAEF_E_FORM != null && GetEEFDetail.GDAEF_E_FORM.Contains(","))
                                {
                                    List<PSW_GD_EXPORT_FI_VIEW> ExportFisAgainstGd = new List<PSW_GD_EXPORT_FI_VIEW>();
                                    string[] FORME_MO = GetEEFDetail.GDAEF_E_FORM.Split(',');
                                    foreach (string MultiFormE in FORME_MO)
                                    {
                                        PSW_EEF_MASTER_DETAILS ExportFi = new PSW_EEF_MASTER_DETAILS();
                                        if (MultiFormE != null && MultiFormE != "")
                                            ExportFi = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == MultiFormE).FirstOrDefault();
                                        if (ExportFi != null)
                                        {
                                            PSW_GD_EXPORT_FI_VIEW GetSingleExportFI = context.PSW_GD_EXPORT_FI_VIEW.Where(m => m.FDI_FORM_E_NO == ExportFi.FDI_FORM_E_NO && m.FDI_AMENDMENT_NO == ExportFi.FDI_AMENDMENT_NO).FirstOrDefault();
                                            if (GetSingleExportFI != null)
                                                ExportFisAgainstGd.Add(GetSingleExportFI);
                                        }
                                    }

                                    var EEFGDMASTER = context.PSW_GD_EXPORT_SINGLE_VIEW.Where(m => m.EFGDGI_GD_NO == ExportGd.EFGDGI_GD_NO && m.EFGDGI_AMENDMENT_NO == ExportGd.EFGDGI_AMENDMENT_NO).FirstOrDefault();
                                    var EEFGDDETAILS = ExportFisAgainstGd;
                                    var EEFGDHSCode = context.PSW_GD_EXPORT_HS_CODE_VIEW.Where(m => m.EHC_GD_NO == ExportGd.EFGDGI_GD_NO && m.EHC_AMENDMENT_NO == ExportGd.EFGDGI_AMENDMENT_NO).ToList();
                                    ReportDocument rd = new ReportDocument();
                                    rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_GD_EXPORT.rpt"));
                                    rd.DataSourceConnections.Clear();
                                    rd.SetDataSource(new[] { EEFGDMASTER });
                                    rd.Subreports[0].DataSourceConnections.Clear();
                                    rd.Subreports[0].SetDataSource(EEFGDDETAILS);
                                    rd.Subreports[1].DataSourceConnections.Clear();
                                    rd.Subreports[1].SetDataSource(EEFGDHSCode);

                                    Response.Buffer = false;
                                    Response.ClearContent();
                                    Response.ClearHeaders();

                                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                                    stream.Seek(0, SeekOrigin.Begin);
                                    return File(stream, "application/pdf", "PSW-EXPORT-GD(" + ExportGd.EFGDGI_GD_NO + ").pdf");
                                }
                                else
                                {
                                    var EEFGDDETAILS = new List<PSW_GD_EXPORT_FI_VIEW>();
                                    PSW_EEF_MASTER_DETAILS ExportFi = new PSW_EEF_MASTER_DETAILS();
                                    if (GetEEFDetail.GDAEF_E_FORM != null && GetEEFDetail.GDAEF_E_FORM != "")
                                        ExportFi = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == GetEEFDetail.GDAEF_E_FORM).FirstOrDefault();

                                    var EEFGDMASTER = context.PSW_GD_EXPORT_SINGLE_VIEW.Where(m => m.EFGDGI_GD_NO == ExportGd.EFGDGI_GD_NO && m.EFGDGI_AMENDMENT_NO == ExportGd.EFGDGI_AMENDMENT_NO).FirstOrDefault();
                                    if (ExportFi != null)
                                        EEFGDDETAILS = context.PSW_GD_EXPORT_FI_VIEW.Where(m => m.FDI_FORM_E_NO == ExportFi.FDI_FORM_E_NO && m.FDI_AMENDMENT_NO == ExportFi.FDI_AMENDMENT_NO).ToList();
                                    else
                                        EEFGDDETAILS = new List<PSW_GD_EXPORT_FI_VIEW>();
                                    var EEFGDHSCode = context.PSW_GD_EXPORT_HS_CODE_VIEW.Where(m => m.EHC_GD_NO == ExportGd.EFGDGI_GD_NO && m.EHC_AMENDMENT_NO == ExportGd.EFGDGI_AMENDMENT_NO).ToList();
                                    ReportDocument rd = new ReportDocument();
                                    rd.Load(Path.Combine(Server.MapPath("~/CRReports/"), "PSW_GD_EXPORT.rpt"));
                                    rd.DataSourceConnections.Clear();
                                    rd.SetDataSource(new[] { EEFGDMASTER });
                                    rd.Subreports[0].DataSourceConnections.Clear();
                                    rd.Subreports[0].SetDataSource(EEFGDDETAILS);
                                    rd.Subreports[1].DataSourceConnections.Clear();
                                    rd.Subreports[1].SetDataSource(EEFGDHSCode);

                                    Response.Buffer = false;
                                    Response.ClearContent();
                                    Response.ClearHeaders();

                                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                                    stream.Seek(0, SeekOrigin.Begin);
                                    return File(stream, "application/pdf", "PSW-EXPORT-GD(" + ExportGd.EFGDGI_GD_NO + ").pdf");
                                }
                            }
                        }
                    }
                    return CreateLogFile("Problem while generating PDF for Reference No : " + FI_GD_NO + " Request Type : " + REQUEST_TYPE + " and FI/GD : " + FI_GD);
                }
                else
                {
                    return RedirectToAction("Index", "Authentication");
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured :" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception : " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
                return CreateLogFile(message);
            }
        }

        #endregion

        #region A404 Report (Monthly) - EEF / GD based data
        public ActionResult EEFA404()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EEFA404", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EEFA404(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EEFA404", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            List<PSW_EEF_A404_VIEW> Data = context.PSW_EEF_A404_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.FDRI_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(ToDate)).ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.FDI_STATUS == "APPROVED").ToList();
                                if (Data.Count() > 0)
                                    return View(Data);
                                else
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region EIF PAYMENT REPORT
        public ActionResult EIFPaymentReport()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EIFPaymentReport", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EIFPaymentReport(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EIFPaymentReport", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EIF_PAYMENT_REPORT(FromDate, ToDate).OrderByDescending(m => m.EIF_BDA_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region Import Goods Declaration
        public ActionResult ImportGoodsDeclaration()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ImportGoodsDeclaration", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ImportGoodsDeclaration(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ImportGoodsDeclaration", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EIF_GD_REPORT(FromDate, ToDate).OrderByDescending(m => m.EIGDGI_ID).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region Export Goods Declaration
        public ActionResult ExportGoodsDeclaration()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportGoodsDeclaration", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ExportGoodsDeclaration(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportGoodsDeclaration", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EEF_GD_REPORT(FromDate, ToDate).OrderByDescending(m => m.EFGDGI_ID).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region Import Financial Instrument
        public ActionResult ImportFinancialInstrument()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ImportFinancialInstrument", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ImportFinancialInstrument(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ImportFinancialInstrument", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EIF_REPORT(FromDate, ToDate).OrderByDescending(m => m.EIF_BA_ID).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region Export Financial Instrument
        public ActionResult ExportFinancialInstrument()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportFinancialInstrument", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ExportFinancialInstrument(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ExportFinancialInstrument", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EEF_REPORT(FromDate, ToDate).OrderByDescending(m => m.FDI_ID).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion

        #region EForm Short Format
        public ActionResult EFormSHortFormat()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EFormSHortFormat", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EFormSHortFormat(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "EFormSHortFormat", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PSW_EEF_GD_REPORT(FromDate, ToDate).OrderByDescending(m => m.EFGDGI_ID).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        #endregion
        public ActionResult FormE_SDFS()
        {
            return View();
        }

        public ActionResult FormE_BCAS()
        {
            return View();
        }

        public ActionResult FormE_DDES()
        {
            return View();
        }

        public ActionResult FormE_TransEFEs()
        {
            return View();
        }

    }
}
﻿using PSW.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PSW.Controllers
{
    public class SetupController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: Setup
        #region Manage Banks
        public ActionResult ManageBank(int B = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (B != 0)
                        {
                            PSW_LIST_OF_BANKS edit = context.PSW_LIST_OF_BANKS.Where(m => m.LB_BANK_ID == B).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + B;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ManageBank(PSW_LIST_OF_BANKS db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_BANKS UpdateEntity = new PSW_LIST_OF_BANKS();

                        PSW_LIST_OF_BANKS CheckIfExist = new PSW_LIST_OF_BANKS();
                        if (db_table.LB_BANK_ID == 0)
                        {
                            UpdateEntity.LB_BANK_ID = Convert.ToInt32(context.PSW_LIST_OF_BANKS.Max(m => (decimal?)m.LB_BANK_ID)) + 1;
                            UpdateEntity.LB_BANK_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_LIST_OF_BANKS.Where(m => m.LB_BANK_ID == db_table.LB_BANK_ID).FirstOrDefault();
                            UpdateEntity.LB_BANK_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.LB_BANK_NAME = db_table.LB_BANK_NAME;
                        UpdateEntity.LB_BANK_STATUS = db_table.LB_BANK_STATUS;
                        UpdateEntity.LB_BANK_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.LB_BANK_MAKER_ID == "Admin123")
                            UpdateEntity.LB_BANK_CHECKER_ID = UpdateEntity.LB_BANK_MAKER_ID;
                        else
                            UpdateEntity.LB_BANK_CHECKER_ID = null;
                        if (CheckIfExist == null)
                            context.PSW_LIST_OF_BANKS.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [ChildActionOnly]
        public PartialViewResult ManageBankView(decimal? Bank_ID)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_LIST_OF_BANKS> ViewData = context.PSW_LIST_OF_BANKS.Where(m => m.LB_BANK_MAKER_ID != SessionUser).OrderByDescending(m => m.LB_BANK_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeManageBank(int LB_BANK_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_BANKS UpdateEntity = context.PSW_LIST_OF_BANKS.Where(m => m.LB_BANK_ID == LB_BANK_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.LB_BANK_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.LB_BANK_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Bank : " + UpdateEntity.LB_BANK_NAME + " is sucessfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LB_BANK_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeManageBank(int LB_BANK_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageBank", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_BANKS UpdateEntity = context.PSW_LIST_OF_BANKS.Where(m => m.LB_BANK_ID == LB_BANK_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_LIST_OF_BANKS.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Bank : " + UpdateEntity.LB_BANK_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LB_BANK_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Manage Country
        public ActionResult ManageCountry(ManageCountryFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCountry", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_COUNTRIES> List = context.PSW_LIST_OF_COUNTRIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LC_ISAUTH == true || m.LC_MAKER_ID != SessionUser).OrderByDescending(m => m.LC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ManageCountry(ManageCountryFilter db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCountry", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_LIST_OF_COUNTRIES UpdateEntity = new PSW_LIST_OF_COUNTRIES();

                        PSW_LIST_OF_COUNTRIES CheckIfExist = new PSW_LIST_OF_COUNTRIES();
                        if (db_table.Entity.LC_ID == 0)
                        {
                            UpdateEntity.LC_ID = Convert.ToInt32(context.PSW_LIST_OF_COUNTRIES.Max(m => (decimal?)m.LC_ID)) + 1;
                            UpdateEntity.LC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == db_table.Entity.LC_ID).FirstOrDefault();
                            UpdateEntity.LC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.LC_NAME = db_table.Entity.LC_NAME;
                        UpdateEntity.LC_CODE = db_table.Entity.LC_CODE;
                        UpdateEntity.LC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.LC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.LC_CHECKER_ID = UpdateEntity.LC_MAKER_ID;
                            UpdateEntity.LC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.LC_CHECKER_ID = null;
                            UpdateEntity.LC_ISAUTH = false;
                        }
                        UpdateEntity.LC_STATUS = db_table.Entity.LC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_LIST_OF_COUNTRIES.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult ManageCountryFilter(ManageCountryFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCountry", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_COUNTRIES> List = context.PSW_LIST_OF_COUNTRIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LC_ISAUTH == true || m.LC_MAKER_ID != SessionUser).OrderByDescending(m => m.LC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageCountry", edit);
        }
        [HttpPost]
        public ActionResult ManageCountryFilter(ManageCountryFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCountry", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_COUNTRIES> List = context.PSW_LIST_OF_COUNTRIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LC_ISAUTH == true || m.LC_MAKER_ID != SessionUser).OrderByDescending(m => m.LC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageCountry", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ManageCountryView(ManageCountryFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_LIST_OF_COUNTRIES> ViewData = new List<PSW_LIST_OF_COUNTRIES>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_LIST_OF_COUNTRIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.LC_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.LC_ISAUTH == true || m.LC_MAKER_ID != SessionUser).OrderByDescending(m => m.LC_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ISAUTH == true || m.LC_MAKER_ID != SessionUser).OrderByDescending(m => m.LC_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeCountry(int LC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCountry", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_COUNTRIES UpdateEntity = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == LC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.LC_ISAUTH = true;
                            UpdateEntity.LC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.LC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Country : " + UpdateEntity.LC_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeCountry(int LC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCountry", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_COUNTRIES UpdateEntity = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == LC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_LIST_OF_COUNTRIES.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Country : " + UpdateEntity.LC_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkManageCountry(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCountry", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_LIST_OF_COUNTRIES ListOfCountry = new PSW_LIST_OF_COUNTRIES();
                                    if (row["Country Code"].ToString().Length > 0)
                                    {
                                        string CountryCode = row["Country Code"].ToString();
                                        bool CheckIfExist = false;
                                        ListOfCountry = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_CODE == CountryCode).FirstOrDefault();

                                        if (ListOfCountry == null)
                                        {
                                            ListOfCountry = new PSW_LIST_OF_COUNTRIES();
                                            ListOfCountry.LC_ID = Convert.ToInt32(context.PSW_LIST_OF_COUNTRIES.Max(m => (decimal?)m.LC_ID)) + 1;
                                            ListOfCountry.LC_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            ListOfCountry.LC_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Country Code"] != null || row["Country Code"].ToString() != "")
                                        {
                                            string countrycode = row["Country Code"].ToString();
                                            ListOfCountry.LC_CODE = countrycode;
                                            if (row["Name"].ToString() != "" && row["Name"] != null)
                                            {
                                                string name = row["Name"].ToString();
                                                ListOfCountry.LC_NAME = name;
                                                ListOfCountry.LC_MAKER_ID = Session["USER_ID"].ToString();
                                                ListOfCountry.LC_CHECKER_ID = Session["USER_ID"].ToString();
                                                ListOfCountry.LC_ISAUTH = true;
                                                ListOfCountry.LC_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_LIST_OF_COUNTRIES.Add(ListOfCountry);
                                                }
                                                else
                                                {
                                                    context.Entry(ListOfCountry).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Country Code : " + row["Country Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Country Name : " + row["Name"].ToString() + "  At Country Code : " + row["Country Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Payment Modes
        public ActionResult PaymentModes(ManagePayModeFilter edit, int P = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "PaymentModes", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_AUTH_PAY_MODE_VIEW> List = context.PSW_AUTH_PAY_MODE_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.APM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.APM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.APM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.APM_ISAUTH == true || m.APM_MAKER_ID != SessionUser).OrderByDescending(m => m.APM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (P != 0)
                        {
                            edit.Entity = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == P).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + P;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> PayMode = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        PayMode.Insert(0, (new SelectListItem { Text = "--Select Payment Term--", Value = "0" }));
                        ViewBag.APM_TYPE_ID = PayMode;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult PaymentModes(ManagePayModeFilter db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "PaymentModes", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PSW_AUTH_PAY_MODES UpdateEntity = new PSW_AUTH_PAY_MODES();

                        PSW_AUTH_PAY_MODES CheckIfExist = new PSW_AUTH_PAY_MODES();
                        if (db_table.Entity.APM_ID == 0)
                        {
                            UpdateEntity.APM_ID = Convert.ToInt32(context.PSW_AUTH_PAY_MODES.Max(m => (decimal?)m.APM_ID)) + 1;
                            UpdateEntity.APM_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == db_table.Entity.APM_ID).FirstOrDefault();
                            UpdateEntity.APM_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.APM_CODE = db_table.Entity.APM_CODE;
                        UpdateEntity.APM_STATUS = db_table.Entity.APM_STATUS;
                        UpdateEntity.APM_DESCRIPTION = db_table.Entity.APM_DESCRIPTION;
                        UpdateEntity.APM_TYPE_ID = db_table.Entity.APM_TYPE_ID;
                        UpdateEntity.APM_NO_OF_DAYS = db_table.Entity.APM_NO_OF_DAYS;
                        UpdateEntity.APM_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.APM_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.APM_CHECKER_ID = UpdateEntity.APM_MAKER_ID;
                            UpdateEntity.APM_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.APM_CHECKER_ID = null;
                            UpdateEntity.APM_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_AUTH_PAY_MODES.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> PayMode = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        PayMode.Insert(0, (new SelectListItem { Text = "--Select Payment Term--", Value = "0" }));
                        ViewBag.APM_TYPE_ID = PayMode;
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult PaymentModesFilter(ManagePayModeFilter edit, int P = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "PaymentModes", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_AUTH_PAY_MODE_VIEW> List = context.PSW_AUTH_PAY_MODE_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.APM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.APM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.APM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.APM_ISAUTH == true || m.APM_MAKER_ID != SessionUser).OrderByDescending(m => m.APM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> PayMode = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        PayMode.Insert(0, (new SelectListItem { Text = "--Select Payment Term--", Value = "0" }));
                        ViewBag.APM_TYPE_ID = PayMode;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("PaymentModes", edit);
        }
        [HttpPost]
        public ActionResult PaymentModesFilter(ManagePayModeFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "PaymentModes", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_AUTH_PAY_MODE_VIEW> List = context.PSW_AUTH_PAY_MODE_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.APM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.APM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.APM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.APM_ISAUTH == true || m.APM_MAKER_ID != SessionUser).OrderByDescending(m => m.APM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> PayMode = new SelectList(context.PSW_TYPE_OF_PROFILE.ToList(), "P_ID", "P_NAME", 0).ToList();
                        PayMode.Insert(0, (new SelectListItem { Text = "--Select Payment Term--", Value = "0" }));
                        ViewBag.APM_TYPE_ID = PayMode;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("PaymentModes", edit);
        }
        [ChildActionOnly]
        public PartialViewResult PaymentModesView(ManagePayModeFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_AUTH_PAY_MODE_VIEW> ViewData = new List<PSW_AUTH_PAY_MODE_VIEW>();
                if (Filter.FromDate != null  && Filter.ToDate != null )
                {
                    ViewData = context.PSW_AUTH_PAY_MODE_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.APM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.APM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.APM_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.APM_MAKER_ID != SessionUser || m.APM_ISAUTH == true).OrderByDescending(m => m.APM_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_AUTH_PAY_MODE_VIEW.Where(m => m.APM_MAKER_ID != SessionUser || m.APM_ISAUTH == true).OrderByDescending(m => m.APM_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizePayModes(int APM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "PaymentModes", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_AUTH_PAY_MODES UpdateEntity = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == APM_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.APM_ISAUTH = true;
                            UpdateEntity.APM_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.APM_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Payment Mode : " + UpdateEntity.APM_STATUS + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + APM_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizePayModes(int APM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "PaymentModes", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_AUTH_PAY_MODES UpdateEntity = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == APM_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_AUTH_PAY_MODES.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Payment Code : " + UpdateEntity.APM_CODE + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + APM_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Account Status
        public ActionResult AccountStatus(ManageAccountStatusFilter edit, int S = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountStatus", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ACCOUNT_STATUS> List = context.PSW_ACCOUNT_STATUS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AS_ISAUTH == true || m.AS_MAKER_ID != SessionUser).OrderByDescending(m => m.AS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (S != 0)
                        {
                            edit.Entity = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == S).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + S;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AccountStatus(ManageAccountStatusFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountStatus", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ACCOUNT_STATUS UpdateEntity = new PSW_ACCOUNT_STATUS();

                        PSW_ACCOUNT_STATUS CheckIfExist = new PSW_ACCOUNT_STATUS();
                        if (db_table.Entity.AS_ID == 0)
                        {
                            UpdateEntity.AS_ID = Convert.ToInt32(context.PSW_ACCOUNT_STATUS.Max(m => (decimal?)m.AS_ID)) + 1;
                            UpdateEntity.AS_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == db_table.Entity.AS_ID).FirstOrDefault();
                            UpdateEntity.AS_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.AS_CODE = db_table.Entity.AS_CODE;
                        UpdateEntity.AS_STATUS_NAME = db_table.Entity.AS_STATUS_NAME;
                        UpdateEntity.AS_DESCRIPTION = db_table.Entity.AS_DESCRIPTION;
                        UpdateEntity.AS_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.AS_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.AS_CHECKER_ID = UpdateEntity.AS_MAKER_ID;
                            UpdateEntity.AS_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.AS_CHECKER_ID = null;
                            UpdateEntity.AS_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_ACCOUNT_STATUS.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult AccountStatusFilter(ManageAccountStatusFilter edit, int S = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountStatus", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ACCOUNT_STATUS> List = context.PSW_ACCOUNT_STATUS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AS_ISAUTH == true || m.AS_MAKER_ID != SessionUser).OrderByDescending(m => m.AS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("AccountStatus", edit);
        }
        [HttpPost]
        public ActionResult AccountStatusFilter(ManageAccountStatusFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountStatus", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ACCOUNT_STATUS> List = context.PSW_ACCOUNT_STATUS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AS_ISAUTH == true || m.AS_MAKER_ID != SessionUser).OrderByDescending(m => m.AS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("AccountStatus", edit);
        }
        [ChildActionOnly]
        public PartialViewResult AccountStatusView(ManageAccountStatusFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ACCOUNT_STATUS> ViewData = new List<PSW_ACCOUNT_STATUS>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ACCOUNT_STATUS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.AS_ID).ToList();
                    if(ViewData.Count() > 0)
                        ViewData = ViewData.Where(m => m.AS_MAKER_ID != SessionUser || m.AS_ISAUTH == true).OrderByDescending(m => m.AS_ID).ToList();
                }
                else
                    ViewData = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_MAKER_ID != SessionUser || m.AS_ISAUTH == true).OrderByDescending(m => m.AS_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeAccountStatus(int AS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountStatus", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ACCOUNT_STATUS UpdateEntity = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == AS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.AS_ISAUTH = true;
                            UpdateEntity.AS_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.AS_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Account Status : " + UpdateEntity.AS_STATUS_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + AS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeAccountStatus(int AS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountStatus", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ACCOUNT_STATUS UpdateEntity = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == AS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ACCOUNT_STATUS.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Account Status : " + UpdateEntity.AS_STATUS_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + AS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Account Type
        public ActionResult AccountType(ManageAccountTypeFilter edit, int A = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountType", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ACCOUNT_TYPE> List = context.PSW_ACCOUNT_TYPE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AT_ISAUTH == true || m.AT_MAKER_ID != SessionUser).OrderByDescending(m => m.AT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (A != 0)
                        {
                            edit.Entity = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ID == A).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + A;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AccountType(ManageAccountTypeFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountType", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ACCOUNT_TYPE UpdateEntity = new PSW_ACCOUNT_TYPE();

                        PSW_ACCOUNT_TYPE CheckIfExist = new PSW_ACCOUNT_TYPE();
                        if (db_table.Entity.AT_ID == 0)
                        {
                            UpdateEntity.AT_ID = Convert.ToInt32(context.PSW_ACCOUNT_TYPE.Max(m => (decimal?)m.AT_ID)) + 1;
                            UpdateEntity.AT_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ID == db_table.Entity.AT_ID).FirstOrDefault();
                            UpdateEntity.AT_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.AT_CODE = db_table.Entity.AT_CODE;
                        UpdateEntity.AT_STATUS_NAME = db_table.Entity.AT_STATUS_NAME;
                        UpdateEntity.AT_DESCRIPTION = db_table.Entity.AT_DESCRIPTION;
                        UpdateEntity.AT_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.AT_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.AT_CHECKER_ID = UpdateEntity.AT_MAKER_ID;
                            UpdateEntity.AT_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.AT_CHECKER_ID = null;
                            UpdateEntity.AT_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_ACCOUNT_TYPE.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult AccountTypeFilter(ManageAccountTypeFilter edit, int A = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountType", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ACCOUNT_TYPE> List = context.PSW_ACCOUNT_TYPE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AT_ISAUTH == true || m.AT_MAKER_ID != SessionUser).OrderByDescending(m => m.AT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("AccountType", edit);
        }
        [HttpPost]
        public ActionResult AccountTypeFilter(ManageAccountTypeFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountType", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_ACCOUNT_TYPE> List = context.PSW_ACCOUNT_TYPE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.AT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.AT_ISAUTH == true || m.AT_MAKER_ID != SessionUser).OrderByDescending(m => m.AT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("AccountType", edit);
        }
        [ChildActionOnly]
        public PartialViewResult AccountTypeView(ManageAccountTypeFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_ACCOUNT_TYPE> ViewData = new List<PSW_ACCOUNT_TYPE>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_ACCOUNT_TYPE.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.AT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.AT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.AT_ID).ToList();
                    if(ViewData.Count() > 0)
                        ViewData = ViewData.Where(m => m.AT_MAKER_ID != SessionUser || m.AT_ISAUTH == true).OrderByDescending(m => m.AT_ID).ToList();
                }
                else
                    ViewData = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_MAKER_ID != SessionUser || m.AT_ISAUTH == true).OrderByDescending(m => m.AT_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeAccountType(int AT_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountType", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ACCOUNT_TYPE UpdateEntity = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ID == AT_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.AT_ISAUTH = true;
                            UpdateEntity.AT_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.AT_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Account Type : " + UpdateEntity.AT_STATUS_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + AT_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeAccountType(int AT_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "AccountType", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_ACCOUNT_TYPE UpdateEntity = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ID == AT_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_ACCOUNT_TYPE.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Account Type : " + UpdateEntity.AT_STATUS_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + AT_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Delivery Term
        public ActionResult DeliveryTerm(ManageDeliveryTermFilter edit, int D = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "DeliveryTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_DELIVERY_TERMS> List = context.PSW_DELIVERY_TERMS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.DT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.DT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.DT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.DT_ISAUTH == true || m.DT_MAKER_ID != SessionUser).OrderByDescending(m => m.DT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (D != 0)
                        {
                            edit.Entity = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == D).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + D;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult DeliveryTerm(ManageDeliveryTermFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "DeliveryTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_DELIVERY_TERMS UpdateEntity = new PSW_DELIVERY_TERMS();

                        PSW_DELIVERY_TERMS CheckIfExist = new PSW_DELIVERY_TERMS();
                        if (db_table.Entity.DT_ID == 0)
                        {
                            UpdateEntity.DT_ID = Convert.ToInt32(context.PSW_DELIVERY_TERMS.Max(m => (decimal?)m.DT_ID)) + 1;
                            UpdateEntity.DT_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == db_table.Entity.DT_ID).FirstOrDefault();
                            UpdateEntity.DT_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.DT_CODE = db_table.Entity.DT_CODE;
                        UpdateEntity.DT_DESCRIPTION = db_table.Entity.DT_DESCRIPTION;
                        UpdateEntity.DT_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.DT_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.DT_CHECKER_ID = UpdateEntity.DT_MAKER_ID;
                            UpdateEntity.DT_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.DT_CHECKER_ID = null;
                            UpdateEntity.DT_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_DELIVERY_TERMS.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult DeliveryTermFilter(ManageDeliveryTermFilter edit, int D = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "DeliveryTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_DELIVERY_TERMS> List = context.PSW_DELIVERY_TERMS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.DT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.DT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.DT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.DT_ISAUTH == true || m.DT_MAKER_ID != SessionUser).OrderByDescending(m => m.DT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("DeliveryTerm", edit);
        }
        [HttpPost]
        public ActionResult DeliveryTermFilter(ManageDeliveryTermFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "DeliveryTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_DELIVERY_TERMS> List = context.PSW_DELIVERY_TERMS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.DT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.DT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.DT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.DT_ISAUTH == true || m.DT_MAKER_ID != SessionUser).OrderByDescending(m => m.DT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("DeliveryTerm", edit);
        }
        [ChildActionOnly]
        public PartialViewResult DeliveryTermView(ManageDeliveryTermFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_DELIVERY_TERMS> ViewData = new List<PSW_DELIVERY_TERMS>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_DELIVERY_TERMS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.DT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.DT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.DT_ID).ToList();
                    if (ViewData.Count() > 0)
                    {
                        ViewData = ViewData.Where(m => m.DT_MAKER_ID != SessionUser || m.DT_ISAUTH == true).OrderByDescending(m => m.DT_ID).ToList();
                    }
                }
                else
                    ViewData = context.PSW_DELIVERY_TERMS.Where(m => m.DT_MAKER_ID != SessionUser || m.DT_ISAUTH == true).OrderByDescending(m => m.DT_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeDeliveryTerm(int DT_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "DeliveryTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_DELIVERY_TERMS UpdateEntity = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DT_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.DT_ISAUTH = true;
                            UpdateEntity.DT_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.DT_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Delivery Term : " + UpdateEntity.DT_DESCRIPTION + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + DT_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeDeliveryTerm(int DT_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "DeliveryTerm", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_DELIVERY_TERMS UpdateEntity = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DT_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_DELIVERY_TERMS.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Delivery Term : " + UpdateEntity.DT_DESCRIPTION + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + DT_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Manage Commodities 
        public ActionResult ManageCommodities(ManageCommoditiesFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCommodities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_COMMODITIES> List = context.PSW_LIST_OF_COMMODITIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LCOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LCOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LCOM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LCOM_ISAUTH == true || m.LCOM_MAKER_ID != SessionUser).OrderByDescending(m => m.LCOM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ManageCommodities(ManageCommoditiesFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCommodities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_COMMODITIES UpdateEntity = new PSW_LIST_OF_COMMODITIES();

                        PSW_LIST_OF_COMMODITIES CheckIfExist = new PSW_LIST_OF_COMMODITIES();
                        if (db_table.Entity.LCOM_ID == 0)
                        {
                            UpdateEntity.LCOM_ID = Convert.ToInt32(context.PSW_LIST_OF_COMMODITIES.Max(m => (decimal?)m.LCOM_ID)) + 1;
                            UpdateEntity.LCOM_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ID == db_table.Entity.LCOM_ID).FirstOrDefault();
                            UpdateEntity.LCOM_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        if (db_table.Entity.LCOM_CODE.Length <= 14)
                        {
                            UpdateEntity.LCOM_NAME = db_table.Entity.LCOM_NAME;
                            UpdateEntity.LCOM_CODE = DAL.RemoveNewLineAndTabs(db_table.Entity.LCOM_CODE).Trim();
                            UpdateEntity.LCOM_STATUS = db_table.Entity.LCOM_STATUS;
                            UpdateEntity.LCOM_MAKER_ID = Session["USER_ID"].ToString();
                            if (UpdateEntity.LCOM_MAKER_ID == "Admin123")
                            {
                                UpdateEntity.LCOM_CHECKER_ID = UpdateEntity.LCOM_MAKER_ID;
                                UpdateEntity.LCOM_ISAUTH = true;
                            }
                            else
                            {
                                UpdateEntity.LCOM_CHECKER_ID = null;
                                UpdateEntity.LCOM_ISAUTH = false;
                            }
                            if (CheckIfExist == null)
                                context.PSW_LIST_OF_COMMODITIES.Add(UpdateEntity);
                            else
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                        else
                        {
                            message = "Maximum Length for Hs CODE is (14), Current Hs Code Length is " + db_table.Entity.LCOM_CODE.Length;
                            return View(db_table);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public FileStreamResult BulkCommodityPortfolio(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCommodities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                 string.IsNullOrWhiteSpace(field as string)))
                                .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_LIST_OF_COMMODITIES commodity = new PSW_LIST_OF_COMMODITIES();

                                    if (row["COMMODITY"].ToString().Length > 2)
                                    {
                                        string ComCode = row["PCT Code"].ToString();

                                        bool CommodityExist = false;
                                        commodity = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == ComCode).FirstOrDefault();

                                        if (commodity == null)
                                        {
                                            commodity = new PSW_LIST_OF_COMMODITIES();
                                            commodity.LCOM_ID = Convert.ToInt32(context.PSW_LIST_OF_COMMODITIES.Max(m => (decimal?)m.LCOM_ID)) + 1;
                                            commodity.LCOM_ENTRY_DATETIME = DateTime.Now;
                                            CommodityExist = false;
                                        }
                                        else
                                        {
                                            commodity.LCOM_EDIT_DATETIME = DateTime.Now;
                                            CommodityExist = true;
                                        }
                                        if (row["COMMODITY"] != null && row["COMMODITY"].ToString() != "")
                                        {
                                            string Commodity_Name = row["COMMODITY"].ToString();
                                            commodity.LCOM_NAME = Commodity_Name;
                                            if (row["PCT CODE"] != null && row["PCT CODE"].ToString() != "")
                                            {
                                                string Commodity_Code = row["PCT CODE"].ToString();
                                                if (Commodity_Code.Length <= 14)
                                                {
                                                    commodity.LCOM_CODE = DAL.RemoveNewLineAndTabs(Commodity_Code).Trim();
                                                    commodity.LCOM_STATUS = false;
                                                    commodity.LCOM_MAKER_ID = Session["USER_ID"].ToString();
                                                    commodity.LCOM_CHECKER_ID = null;
                                                    commodity.LCOM_ISAUTH = false;
                                                    if (CommodityExist == false)
                                                    {
                                                        context.PSW_LIST_OF_COMMODITIES.Add(commodity);
                                                    }
                                                    else
                                                    {
                                                        context.Entry(commodity).State = EntityState.Modified;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Maximum Length for Hs Code Has been Exceeded, maximum Length is 14, current Length is " + Commodity_Code.Length + "  " + "  At Commodity Name : " + row["COMMODITY"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Commodity Code  " + row["PCT CODE"].ToString() + "  At Commodity Name : " + row["COMMODITY"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Commodity Name  " + row["COMMODITY"].ToString() + "  At Commodity Code : " + row["PCT CODE"].ToString() + Environment.NewLine;
                                        }
                                    }

                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        public FileStreamResult CreateLogFile(string Message)
        {
            var byteArray = Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
        public ActionResult ManageCommoditiesFilter(ManageCommoditiesFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCommodities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_COMMODITIES> List = context.PSW_LIST_OF_COMMODITIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LCOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LCOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LCOM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LCOM_ISAUTH == true || m.LCOM_MAKER_ID != SessionUser).OrderByDescending(m => m.LCOM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageCommodities", edit);
        }
        [HttpPost]
        public ActionResult ManageCommoditiesFilter(ManageCommoditiesFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCommodities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_COMMODITIES> List = context.PSW_LIST_OF_COMMODITIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LCOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LCOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LCOM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LCOM_ISAUTH == true || m.LCOM_MAKER_ID != SessionUser).OrderByDescending(m => m.LCOM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageCommodities", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ManageCommoditiesList(ManageCommoditiesFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_LIST_OF_COMMODITIES> ViewData = new List<PSW_LIST_OF_COMMODITIES>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_LIST_OF_COMMODITIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LCOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LCOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.LCOM_ID).ToList();
                    if(ViewData.Count() > 0)
                        ViewData = ViewData.Where(m => m.LCOM_MAKER_ID != SessionUser || m.LCOM_ISAUTH == true).OrderByDescending(m => m.LCOM_ID).ToList();
                }
                else
                    ViewData = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_MAKER_ID != SessionUser || m.LCOM_ISAUTH == true).OrderByDescending(m => m.LCOM_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeManageCommodities(int LCOM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCommodities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_COMMODITIES UpdateEntity = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ID == LCOM_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.LCOM_ISAUTH = true;
                            UpdateEntity.LCOM_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.LCOM_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Commodity Code : " + UpdateEntity.LCOM_CODE + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LCOM_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeManageCommodities(int LCOM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCommodities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_COMMODITIES UpdateEntity = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ID == LCOM_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_LIST_OF_COMMODITIES.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Commodity : " + UpdateEntity.LCOM_CODE + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LCOM_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Manage Suppliers
        public ActionResult ManageSuppliers(ManageSuppliersFilter edit, int S = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageSuppliers", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_SUPPLIERS> List = context.PSW_LIST_OF_SUPPLIERS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LOS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LOS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LOS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LOS_ISAUTH == true || m.LOS_MAKER_ID != SessionUser).OrderByDescending(m => m.LOS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (S != 0)
                        {
                            edit.Entity = context.PSW_LIST_OF_SUPPLIERS.Where(m => m.LOS_ID == S).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + S;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ManageSuppliers(ManageSuppliersFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageSuppliers", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_SUPPLIERS UpdateEntity = new PSW_LIST_OF_SUPPLIERS();

                        PSW_LIST_OF_SUPPLIERS CheckIfExist = new PSW_LIST_OF_SUPPLIERS();
                        if (db_table.Entity.LOS_ID == 0)
                        {
                            UpdateEntity.LOS_ID = Convert.ToInt32(context.PSW_LIST_OF_SUPPLIERS.Max(m => (decimal?)m.LOS_ID)) + 1;
                            UpdateEntity.LOS_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_LIST_OF_SUPPLIERS.Where(m => m.LOS_ID == db_table.Entity.LOS_ID).FirstOrDefault();
                            UpdateEntity.LOS_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.LOS_NAME = db_table.Entity.LOS_NAME;
                        UpdateEntity.LOS_CODE = db_table.Entity.LOS_CODE;
                        UpdateEntity.LOS_STATUS = db_table.Entity.LOS_STATUS;
                        UpdateEntity.LOS_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.LOS_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.LOS_CHECKER_ID = UpdateEntity.LOS_MAKER_ID;
                            UpdateEntity.LOS_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.LOS_CHECKER_ID = null;
                            UpdateEntity.LOS_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_LIST_OF_SUPPLIERS.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult ManageSuppliersFilter(ManageSuppliersFilter edit, int S = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageSuppliers", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_SUPPLIERS> List = context.PSW_LIST_OF_SUPPLIERS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LOS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LOS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LOS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LOS_ISAUTH == true || m.LOS_MAKER_ID != SessionUser).OrderByDescending(m => m.LOS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageSuppliers", edit);
        }
        [HttpPost]
        public ActionResult ManageSuppliersFilter(ManageSuppliersFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageSuppliers", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_SUPPLIERS> List = context.PSW_LIST_OF_SUPPLIERS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LOS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LOS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LOS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LOS_ISAUTH == true || m.LOS_MAKER_ID != SessionUser).OrderByDescending(m => m.LOS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageSuppliers", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ManageSuppliersList(ManageSuppliersFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_LIST_OF_SUPPLIERS> ViewData = new List<PSW_LIST_OF_SUPPLIERS>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_LIST_OF_SUPPLIERS.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LOS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LOS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.LOS_ID).ToList();
                    if(ViewData.Count() > 0)
                        ViewData = ViewData.Where(m => m.LOS_MAKER_ID != SessionUser || m.LOS_ISAUTH == true).OrderByDescending(m => m.LOS_ID).ToList();
                }
                else
                    ViewData = context.PSW_LIST_OF_SUPPLIERS.Where(m => m.LOS_MAKER_ID != SessionUser || m.LOS_ISAUTH == true).OrderByDescending(m => m.LOS_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeManageSuppliers(int LOS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageSuppliers", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_SUPPLIERS UpdateEntity = context.PSW_LIST_OF_SUPPLIERS.Where(m => m.LOS_ID == LOS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.LOS_ISAUTH = true;
                            UpdateEntity.LOS_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.LOS_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Supplier : " + UpdateEntity.LOS_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeManageSuppliers(int LOS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageSuppliers", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_SUPPLIERS UpdateEntity = context.PSW_LIST_OF_SUPPLIERS.Where(m => m.LOS_ID == LOS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_LIST_OF_SUPPLIERS.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Supplier : " + UpdateEntity.LOS_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Cash Margin
        public ActionResult CashMargin(ManageCashMarginFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "CashMargin", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CASH_MARGIN_HS_CODES_VIEW> List = context.PSW_CASH_MARGIN_HS_CODES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.CM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.CM_ISAUTH == true || m.CM_MAKER_ID != SessionUser).OrderByDescending(m => m.CM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_CASH_MARGIN_HS_CODES.Where(m => m.CM_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> Commodity = new SelectList(context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ISAUTH == true && m.LCOM_STATUS == true)
                        //    .Select(f => new
                        //    {
                        //        f.LCOM_ID,
                        //        LCOM_CODE = f.LCOM_CODE + " -- " + f.LCOM_NAME
                        //    }).ToList(), "LCOM_ID", "LCOM_CODE", 0).ToList();
                        //Commodity.Insert(0, (new SelectListItem { Text = "--Select CODE--", Value = "0" }));
                        //ViewBag.CM_PCT_CODE = Commodity;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult CashMargin(ManageCashMarginFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "CashMargin", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_CASH_MARGIN_HS_CODES UpdateEntity = new PSW_CASH_MARGIN_HS_CODES();

                        PSW_CASH_MARGIN_HS_CODES CheckIfExist = new PSW_CASH_MARGIN_HS_CODES();
                        if (db_table.Entity.CM_ID == 0)
                        {
                            UpdateEntity.CM_ID = Convert.ToInt32(context.PSW_CASH_MARGIN_HS_CODES.Max(m => (decimal?)m.CM_ID)) + 1;
                            UpdateEntity.CM_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_CASH_MARGIN_HS_CODES.Where(m => m.CM_ID == db_table.Entity.CM_ID).FirstOrDefault();
                            UpdateEntity.CM_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.CM_HS4 = db_table.Entity.CM_HS4;
                        UpdateEntity.CM_HS8 = db_table.Entity.CM_HS8;
                        UpdateEntity.CM_PCT_CODE = db_table.Entity.CM_PCT_CODE;
                        UpdateEntity.CM_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.CM_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.CM_CHECKER_ID = UpdateEntity.CM_MAKER_ID;
                            UpdateEntity.CM_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.CM_CHECKER_ID = null;
                            UpdateEntity.CM_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_CASH_MARGIN_HS_CODES.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> Commodity = new SelectList(context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ISAUTH == true && m.LCOM_STATUS == true).ToList(), "LCOM_ID", "LCOM_CODE", 0).ToList();
                        //Commodity.Insert(0, (new SelectListItem { Text = "--Select CODE--", Value = "0" }));
                        //ViewBag.CM_PCT_CODE = Commodity;
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public FileStreamResult BulkCashMarginPortfolio(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "CashMargin", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                 string.IsNullOrWhiteSpace(field as string)))
                                .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_CASH_MARGIN_HS_CODES CashMargin = new PSW_CASH_MARGIN_HS_CODES();

                                    if (row["COMMODITY"].ToString().Length > 2)
                                    {
                                        string PCTCode = row["PCT CODE"].ToString();

                                        bool CashMarginExist = false;
                                        CashMargin = context.PSW_CASH_MARGIN_HS_CODES.Where(m => m.CM_PCT_CODE == PCTCode).FirstOrDefault();
                                        if (CashMargin == null)
                                        {
                                            CashMargin = new PSW_CASH_MARGIN_HS_CODES();
                                            CashMargin.CM_ID = Convert.ToInt32(context.PSW_CASH_MARGIN_HS_CODES.Max(m => (decimal?)m.CM_ID)) + 1;
                                            CashMargin.CM_ENTRY_DATETIME = DateTime.Now;
                                            CashMarginExist = false;
                                        }
                                        else
                                        {
                                            CashMargin.CM_EDIT_DATETIME = DateTime.Now;
                                            CashMarginExist = true;
                                        }
                                        if (row["HS4"] != null && row["HS4"].ToString() != "" || row["HS4"].ToString() == "")
                                        {
                                            string CashMargin_HS4 = row["HS4"].ToString();
                                            CashMargin.CM_HS4 = CashMargin_HS4;
                                            if (row["HS8"] != null && row["HS8"].ToString() != "")
                                            {
                                                string CashMargin_HS8 = row["HS8"].ToString();
                                                CashMargin.CM_HS8 = CashMargin_HS8;
                                                if (row["PCT CODE"] != null && row["PCT CODE"].ToString() != "")
                                                {
                                                    string CashMargin_PCTCode = row["PCT CODE"].ToString();
                                                    PSW_LIST_OF_COMMODITIES Commodity = new PSW_LIST_OF_COMMODITIES();

                                                    bool CommodityExist = false;
                                                    Commodity = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == CashMargin_PCTCode).FirstOrDefault();
                                                    if (Commodity != null)
                                                    {
                                                        string PCTCODE = Commodity.LCOM_ID.ToString();
                                                        CashMargin.CM_PCT_CODE = PCTCODE;
                                                        CashMargin.CM_MAKER_ID = Session["USER_ID"].ToString();
                                                        CashMargin.CM_CHECKER_ID = null;
                                                        CashMargin.CM_ISAUTH = true;
                                                        if (CashMarginExist == false)
                                                        {
                                                            context.PSW_CASH_MARGIN_HS_CODES.Add(CashMargin);
                                                        }
                                                        else
                                                        {
                                                            context.Entry(CashMargin).State = EntityState.Modified;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "The Given PCT Code " + CashMargin_PCTCode + " Does Not Exist In System." + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid PCT Code " + row["PCT CODE"].ToString() + "  At Commodity : " + row["COMMODITY"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid HS8 " + row["HS8"].ToString() + "  At Commodity : " + row["COMMODITY"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid HS4 " + row["HS4"].ToString() + "  At Commodity : " + row["COMMODITY"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        public ActionResult CashMarginFilter(ManageCashMarginFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "CashMargin", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CASH_MARGIN_HS_CODES_VIEW> List = context.PSW_CASH_MARGIN_HS_CODES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.CM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.CM_ISAUTH == true || m.CM_MAKER_ID != SessionUser).OrderByDescending(m => m.CM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("CashMargin", edit);
        }
        [HttpPost]
        public ActionResult CashMarginFilter(ManageCashMarginFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "CashMargin", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CASH_MARGIN_HS_CODES_VIEW> List = context.PSW_CASH_MARGIN_HS_CODES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.CM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.CM_ISAUTH == true || m.CM_MAKER_ID != SessionUser).OrderByDescending(m => m.CM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("CashMargin", edit);
        }
        [ChildActionOnly]
        public PartialViewResult CashMarginList(ManageCashMarginFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_CASH_MARGIN_HS_CODES_VIEW> ViewData = new List<PSW_CASH_MARGIN_HS_CODES_VIEW>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_CASH_MARGIN_HS_CODES_VIEW.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.CM_ID).ToList();
                    if(ViewData.Count() > 0)
                        ViewData = ViewData.Where(m => m.CM_MAKER_ID != SessionUser || m.CM_ISAUTH == true).OrderByDescending(m => m.CM_ID).ToList();
                }
                else
                    ViewData = context.PSW_CASH_MARGIN_HS_CODES_VIEW.Where(m => m.CM_MAKER_ID != SessionUser || m.CM_ISAUTH == true).OrderByDescending(m => m.CM_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeCashMargin(int CM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "CashMargin", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_CASH_MARGIN_HS_CODES UpdateEntity = context.PSW_CASH_MARGIN_HS_CODES.Where(m => m.CM_ID == CM_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.CM_ISAUTH = true;
                            UpdateEntity.CM_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.CM_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Cash Margin : " + UpdateEntity.CM_HS4 + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + CM_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeCashMargin(int CM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "CashMargin", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_CASH_MARGIN_HS_CODES UpdateEntity = context.PSW_CASH_MARGIN_HS_CODES.Where(m => m.CM_ID == CM_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_CASH_MARGIN_HS_CODES.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Cash Margin : " + UpdateEntity.CM_HS4 + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + CM_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Import Policy
        public ActionResult ImportPolicy(ManageImportPolicyFilter edit, int I = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ImportPolicy", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_IMPORT_POLICY> List = context.PSW_IMPORT_POLICY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IP_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IP_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IP_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IP_ISAUTH == true || m.IP_MAKER_ID != SessionUser).OrderByDescending(m => m.IP_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (I != 0)
                        {
                            edit.Entity = context.PSW_IMPORT_POLICY.Where(m => m.IP_ID == I).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + I;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> Commodity = new SelectList(context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ISAUTH == true && m.LCOM_STATUS == true).Select(f => new
                        //{
                        //    f.LCOM_CODE,
                        //    LCOM_DCODE = f.LCOM_CODE + " -- " + f.LCOM_NAME
                        //}).ToList(), "LCOM_CODE", "LCOM_DCODE", 0).ToList();
                        //Commodity.Insert(0, (new SelectListItem { Text = "--Select CODE--", Value = "0" }));
                        //ViewBag.IP_PCT_CODE = Commodity;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ImportPolicy(ManageImportPolicyFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ImportPolicy", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_IMPORT_POLICY UpdateEntity = new PSW_IMPORT_POLICY();

                        PSW_IMPORT_POLICY CheckIfExist = new PSW_IMPORT_POLICY();
                        if (db_table.Entity.IP_ID == 0)
                        {
                            UpdateEntity.IP_ID = Convert.ToInt32(context.PSW_IMPORT_POLICY.Max(m => (decimal?)m.IP_ID)) + 1;
                            UpdateEntity.IP_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_IMPORT_POLICY.Where(m => m.IP_ID == db_table.Entity.IP_ID).FirstOrDefault();
                            UpdateEntity.IP_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.IP_PCT_CODE = db_table.Entity.IP_PCT_CODE;
                        UpdateEntity.IP_COMMODITY_DESCRIPTION = db_table.Entity.IP_COMMODITY_DESCRIPTION;
                        UpdateEntity.IP_CONDITIONS = db_table.Entity.IP_CONDITIONS;
                        UpdateEntity.IP_HEADING_DESCRIPTION = db_table.Entity.IP_HEADING_DESCRIPTION;
                        UpdateEntity.IP_STATUS = db_table.Entity.IP_STATUS;
                        UpdateEntity.IP_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.IP_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.IP_CHECKER_ID = UpdateEntity.IP_MAKER_ID;
                            UpdateEntity.IP_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.IP_CHECKER_ID = null;
                            UpdateEntity.IP_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_IMPORT_POLICY.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        //List<SelectListItem> Commodity = new SelectList(context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_ISAUTH == true && m.LCOM_STATUS == true).Select(f => new
                        //{
                        //    f.LCOM_CODE,
                        //    LCOM_DCODE = f.LCOM_CODE + " -- " + f.LCOM_NAME
                        //}).ToList(), "LCOM_CODE", "LCOM_DCODE", 0).ToList();
                        //Commodity.Insert(0, (new SelectListItem { Text = "--Select CODE--", Value = "0" }));
                        //ViewBag.IP_PCT_CODE = Commodity;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public FileStreamResult BulkImportPolicyPortfolio(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ImportPolicy", Session["USER_ID"].ToString()))
                {
                    int IgnoredRows = 0;
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                 string.IsNullOrWhiteSpace(field as string)))
                                .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_IMPORT_POLICY ImportPolicy = new PSW_IMPORT_POLICY();

                                    if (row["PCT Codes"].ToString().Length > 0)
                                    {
                                        string PCTCode = row["PCT Codes"].ToString();

                                        bool CashMarginExist = false;
                                        ImportPolicy = context.PSW_IMPORT_POLICY.Where(m => m.IP_PCT_CODE == PCTCode).FirstOrDefault();
                                        if (ImportPolicy == null)
                                        {
                                            ImportPolicy = new PSW_IMPORT_POLICY();
                                            ImportPolicy.IP_ID = Convert.ToInt32(context.PSW_IMPORT_POLICY.Max(m => (decimal?)m.IP_ID)) + 1;
                                            ImportPolicy.IP_ENTRY_DATETIME = DateTime.Now;
                                            CashMarginExist = false;
                                        }
                                        else
                                        {
                                            ImportPolicy.IP_EDIT_DATETIME = DateTime.Now;
                                            CashMarginExist = true;
                                        }
                                        if (row["PCT Codes"] != null && row["PCT Codes"].ToString() != "")
                                        {
                                            string PCT_Code = row["PCT Codes"].ToString();
                                            PSW_LIST_OF_COMMODITIES Commodity = new PSW_LIST_OF_COMMODITIES();

                                            bool CommodityExist = false;
                                            Commodity = context.PSW_LIST_OF_COMMODITIES.Where(m => m.LCOM_CODE == PCT_Code).FirstOrDefault();
                                            if (Commodity != null)
                                            {
                                                ImportPolicy.IP_PCT_CODE = PCT_Code;

                                                if (row["Commodity Description"] != null && row["Commodity Description"].ToString() != "")
                                                {
                                                    string Commodity_Description = row["Commodity Description"].ToString();
                                                    ImportPolicy.IP_COMMODITY_DESCRIPTION = Commodity_Description;
                                                    if (row["CONDITIONS"] != null && row["CONDITIONS"].ToString() != "" || row["CONDITIONS"].ToString() == "" || row["CONDITIONS"] == null)
                                                    {
                                                        string IP_Condition = row["CONDITIONS"].ToString();
                                                        ImportPolicy.IP_CONDITIONS = IP_Condition;
                                                        if (row["Heading Description"] != null && row["Heading Description"].ToString() != "" || row["Heading Description"].ToString() == "" || row["Heading Description"] == null)
                                                        {
                                                            string IP_Heading = row["Heading Description"].ToString();
                                                            ImportPolicy.IP_HEADING_DESCRIPTION = IP_Heading;

                                                            ImportPolicy.IP_STATUS = false;
                                                            ImportPolicy.IP_MAKER_ID = Session["USER_ID"].ToString();
                                                            ImportPolicy.IP_CHECKER_ID = null;
                                                            ImportPolicy.IP_ISAUTH = false;
                                                            if (CashMarginExist == false)
                                                            {
                                                                context.PSW_IMPORT_POLICY.Add(ImportPolicy);
                                                            }
                                                            else
                                                            {
                                                                context.Entry(ImportPolicy).State = EntityState.Modified;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Heading Description " + row["Heading Description"].ToString() + "  At PCT Code : " + row["PCT Codes"].ToString() + Environment.NewLine;
                                                            IgnoredRows += 1;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid Conditions " + row["CONDITIONS"].ToString() + "  At PCT Codes : " + row["PCT Codes"].ToString() + Environment.NewLine;
                                                        IgnoredRows += 1;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid Commodity Description " + row["Commodity Description"].ToString() + "  At PCT Codes : " + row["PCT Codes"].ToString() + Environment.NewLine;
                                                    IgnoredRows += 1;
                                                }
                                            }
                                            else
                                            {
                                                message += "The Given PCT Code " + PCT_Code + " Does Not Exist In System." + Environment.NewLine;
                                                IgnoredRows += 1;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid PCT Codes " + row["PCT Codes"].ToString() + "  At Commodity Description : " + row["Commodity Description"].ToString() + Environment.NewLine;
                                            IgnoredRows += 1;
                                        }
                                        RowCount += context.SaveChanges();
                                    }
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    if (IgnoredRows > 0)
                        message += Environment.NewLine + "( " + IgnoredRows + " ) Number of Rows Ignored Due to Errors Showing Above";
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        public ActionResult ImportPolicyFilter(ManageImportPolicyFilter edit, int I = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ImportPolicy", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_IMPORT_POLICY> List = context.PSW_IMPORT_POLICY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IP_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IP_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IP_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IP_ISAUTH == true || m.IP_MAKER_ID != SessionUser).OrderByDescending(m => m.IP_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ImportPolicy", edit);
        }
        [HttpPost]
        public ActionResult ImportPolicyFilter(ManageImportPolicyFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ImportPolicy", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_IMPORT_POLICY> List = context.PSW_IMPORT_POLICY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IP_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IP_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.IP_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.IP_ISAUTH == true || m.IP_MAKER_ID != SessionUser).OrderByDescending(m => m.IP_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ImportPolicy", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ImportPolicyList(ManageImportPolicyFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_IMPORT_POLICY> DataList = new List<PSW_IMPORT_POLICY>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    DataList = context.PSW_IMPORT_POLICY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.IP_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.IP_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.IP_ID).ToList();
                    if(DataList.Count() > 0)
                        DataList = DataList.Where(m => m.IP_MAKER_ID != SessionUser || m.IP_ISAUTH == true).OrderByDescending(m => m.IP_ID).ToList();
                }
               else
                    DataList = context.PSW_IMPORT_POLICY.Where(m => m.IP_MAKER_ID != SessionUser || m.IP_ISAUTH == true).OrderByDescending(m => m.IP_ID).Take(10).ToList();
                return PartialView(DataList);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeImportPolicy(int IP_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ImportPolicy", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_IMPORT_POLICY UpdateEntity = context.PSW_IMPORT_POLICY.Where(m => m.IP_ID == IP_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.IP_ISAUTH = true;
                            UpdateEntity.IP_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.IP_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Commodity : " + UpdateEntity.IP_PCT_CODE + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + IP_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeImportPolicy(int IP_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ImportPolicy", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_IMPORT_POLICY UpdateEntity = context.PSW_IMPORT_POLICY.Where(m => m.IP_ID == IP_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_IMPORT_POLICY.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Commodity : " + UpdateEntity.IP_PCT_CODE + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + IP_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Manage Cities
        public ActionResult ManageCities(ManageCitiesFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_CITIES> List = context.PSW_LIST_OF_CITIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LOCT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LOCT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LOCT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LOCT_ISAUTH == true || m.LOCT_MAKER_ID != SessionUser).OrderByDescending(m => m.LOCT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_LIST_OF_CITIES.Where(m => m.LOCT_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ManageCities(ManageCitiesFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_CITIES UpdateEntity = new PSW_LIST_OF_CITIES();

                        PSW_LIST_OF_CITIES CheckIfExist = new PSW_LIST_OF_CITIES();
                        if (db_table.Entity.LOCT_ID == 0)
                        {
                            UpdateEntity.LOCT_ID = Convert.ToInt32(context.PSW_LIST_OF_CITIES.Max(m => (decimal?)m.LOCT_ID)) + 1;
                            UpdateEntity.LOCT_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_LIST_OF_CITIES.Where(m => m.LOCT_ID == db_table.Entity.LOCT_ID).FirstOrDefault();
                            UpdateEntity.LOCT_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.LOCT_NAME = db_table.Entity.LOCT_NAME;
                        UpdateEntity.LOCT_CODE = db_table.Entity.LOCT_CODE;
                        UpdateEntity.LOCT_STATUS = db_table.Entity.LOCT_STATUS;
                        UpdateEntity.LOCT_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.LOCT_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.LOCT_CHECKER_ID = UpdateEntity.LOCT_MAKER_ID;
                            UpdateEntity.LOCT_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.LOCT_CHECKER_ID = null;
                            UpdateEntity.LOCT_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_LIST_OF_CITIES.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public ActionResult ManageCitiesFilter(ManageCitiesFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_CITIES> List = context.PSW_LIST_OF_CITIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LOCT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LOCT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LOCT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LOCT_ISAUTH == true || m.LOCT_MAKER_ID != SessionUser).OrderByDescending(m => m.LOCT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageCities", edit);
        }
        [HttpPost]
        public ActionResult ManageCitiesFilter(ManageCitiesFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_LIST_OF_CITIES> List = context.PSW_LIST_OF_CITIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LOCT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LOCT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.LOCT_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.LOCT_ISAUTH == true || m.LOCT_MAKER_ID != SessionUser).OrderByDescending(m => m.LOCT_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageCities", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ManageCitiesList(ManageCitiesFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_LIST_OF_CITIES> DataList = new List<PSW_LIST_OF_CITIES>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    DataList = context.PSW_LIST_OF_CITIES.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.LOCT_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.LOCT_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.LOCT_ID).ToList();
                    if(DataList.Count() > 0)
                        DataList = DataList.Where(m => m.LOCT_MAKER_ID != SessionUser || m.LOCT_ISAUTH == true).OrderByDescending(m => m.LOCT_ID).ToList();
                }
                else
                    DataList = context.PSW_LIST_OF_CITIES.Where(m => m.LOCT_MAKER_ID != SessionUser || m.LOCT_ISAUTH == true).OrderByDescending(m => m.LOCT_ID).Take(10).ToList();
                return PartialView(DataList);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeManageCities(int LOCT_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_CITIES UpdateEntity = context.PSW_LIST_OF_CITIES.Where(m => m.LOCT_ID == LOCT_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.LOCT_ISAUTH = true;
                            UpdateEntity.LOCT_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.LOCT_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "City : " + UpdateEntity.LOCT_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOCT_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeManageCities(int LOCT_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCities", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_LIST_OF_CITIES UpdateEntity = context.PSW_LIST_OF_CITIES.Where(m => m.LOCT_ID == LOCT_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_LIST_OF_CITIES.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "City : " + UpdateEntity.LOCT_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOCT_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Manage Currency
        public ActionResult ManageCurrency(ManageCurrencyFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCurrency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CURRENCY> List = context.PSW_CURRENCY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CUR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CUR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.CUR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.CUR_ISAUTH == true || m.CUR_MAKER_ID != SessionUser).OrderByDescending(m => m.CUR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (C != 0)
                        {
                            edit.Entity = context.PSW_CURRENCY.Where(m => m.CUR_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ManageCurrency(ManageCurrencyFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCurrency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_CURRENCY UpdateEntity = new PSW_CURRENCY();

                        PSW_CURRENCY CheckIfExist = new PSW_CURRENCY();
                        if (db_table.Entity.CUR_ID == 0)
                        {
                            UpdateEntity.CUR_ID = Convert.ToInt32(context.PSW_CURRENCY.Max(m => (decimal?)m.CUR_ID)) + 1;
                            UpdateEntity.CUR_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_CURRENCY.Where(m => m.CUR_ID == db_table.Entity.CUR_ID).FirstOrDefault();
                            UpdateEntity.CUR_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.CUR_NAME = db_table.Entity.CUR_NAME;
                        UpdateEntity.CUR_STATUS = db_table.Entity.CUR_STATUS;
                        UpdateEntity.CUR_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.CUR_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.CUR_CHECKER_ID = UpdateEntity.CUR_MAKER_ID;
                            UpdateEntity.CUR_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.CUR_CHECKER_ID = null;
                            UpdateEntity.CUR_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_CURRENCY.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public ActionResult ManageCurrencyFilter(ManageCurrencyFilter edit, int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCurrency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CURRENCY> List = context.PSW_CURRENCY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CUR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CUR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.CUR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.CUR_ISAUTH == true || m.CUR_MAKER_ID != SessionUser).OrderByDescending(m => m.CUR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageCurrency", edit);
        }
        [HttpPost]
        public ActionResult ManageCurrencyFilter(ManageCurrencyFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCurrency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_CURRENCY> List = context.PSW_CURRENCY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CUR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CUR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.CUR_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.CUR_ISAUTH == true || m.CUR_MAKER_ID != SessionUser).OrderByDescending(m => m.CUR_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("ManageCurrency", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ManageCurrencyList(ManageCurrencyFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_CURRENCY> DataList = new List<PSW_CURRENCY>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    DataList = context.PSW_CURRENCY.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.CUR_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.CUR_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.CUR_ID).ToList();
                    if(DataList.Count() > 0)
                        DataList = DataList.Where(m => m.CUR_MAKER_ID != SessionUser || m.CUR_ISAUTH == true).OrderByDescending(m => m.CUR_ID).ToList();
                }
                else
                    DataList = context.PSW_CURRENCY.Where(m => m.CUR_MAKER_ID != SessionUser || m.CUR_ISAUTH == true).OrderByDescending(m => m.CUR_ID).Take(10).ToList();
                return PartialView(DataList);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeManageCurrency(int CUR_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCurrency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_CURRENCY UpdateEntity = context.PSW_CURRENCY.Where(m => m.CUR_ID == CUR_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.CUR_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.CUR_ISAUTH = true;
                            UpdateEntity.CUR_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Currency : " + UpdateEntity.CUR_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + CUR_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeManageCurrency(int CUR_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCurrency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_CURRENCY UpdateEntity = context.PSW_CURRENCY.Where(m => m.CUR_ID == CUR_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_CURRENCY.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Currency Code : " + UpdateEntity.CUR_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + CUR_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkCurrency(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "ManageCurrency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_CURRENCY Currency = new PSW_CURRENCY();
                                    if (row["Code"].ToString().Length > 0)
                                    {
                                        string code = row["Code"].ToString();
                                        bool CheckIfExist = false;
                                        Currency = context.PSW_CURRENCY.Where(m => m.CUR_NAME == code).FirstOrDefault();

                                        if (Currency == null)
                                        {
                                            Currency = new PSW_CURRENCY();
                                            Currency.CUR_ID = Convert.ToInt32(context.PSW_CURRENCY.Max(m => (decimal?)m.CUR_ID)) + 1;
                                            Currency.CUR_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            Currency.CUR_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Code"] != null || row["Code"].ToString() != "")
                                        {
                                            string curr_code = row["Code"].ToString();
                                            Currency.CUR_NAME = curr_code;
                                            if (row["Name"] != null || row["Name"].ToString() != "")
                                            {
                                                string curr_name = row["Name"].ToString();
                                                Currency.CUR_DESCRIPTION = curr_name;
                                                Currency.CUR_MAKER_ID = Session["USER_ID"].ToString();
                                                Currency.CUR_CHECKER_ID = Session["USER_ID"].ToString();
                                                Currency.CUR_ISAUTH = true;
                                                Currency.CUR_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_CURRENCY.Add(Currency);
                                                }
                                                else
                                                {
                                                    context.Entry(Currency).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Currency Name : " + row["Name"].ToString() + " At Currency Code : " + row["Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Currency Code : " + row["Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Manage Port Of Shipping
        public ActionResult PortOfShipping(ManagePortOfShippingFilter edit, int P = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "PortOfShipping", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_PORT_OF_SHIPPING> List = context.PSW_PORT_OF_SHIPPING.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.POS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.POS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.POS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.POS_ISAUTH == true || m.POS_MAKER_ID != SessionUser).OrderByDescending(m => m.POS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (P != 0)
                        {
                            edit.Entity = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_ID == P).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + P;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult PortOfShipping(ManagePortOfShippingFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "PortOfShipping", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_PORT_OF_SHIPPING UpdateEntity = new PSW_PORT_OF_SHIPPING();

                        PSW_PORT_OF_SHIPPING CheckIfExist = new PSW_PORT_OF_SHIPPING();
                        if (db_table.Entity.POS_ID == 0)
                        {
                            UpdateEntity.POS_ID = Convert.ToInt32(context.PSW_PORT_OF_SHIPPING.Max(m => (decimal?)m.POS_ID)) + 1;
                            UpdateEntity.POS_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_ID == db_table.Entity.POS_ID).FirstOrDefault();
                            UpdateEntity.POS_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.POS_NAME = db_table.Entity.POS_NAME;
                        UpdateEntity.POS_CODE = db_table.Entity.POS_CODE;
                        UpdateEntity.POS_COUNTRY_CODE = db_table.Entity.POS_COUNTRY_CODE;
                        UpdateEntity.POS_PORT_TYPE_CODE = db_table.Entity.POS_PORT_TYPE_CODE;
                        UpdateEntity.POS_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.POS_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.POS_CHECKER_ID = UpdateEntity.POS_MAKER_ID;
                            UpdateEntity.POS_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.POS_CHECKER_ID = null;
                            UpdateEntity.POS_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_PORT_OF_SHIPPING.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult PortOfShippingFilter(ManagePortOfShippingFilter edit, int P = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "PortOfShipping", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_PORT_OF_SHIPPING> List = context.PSW_PORT_OF_SHIPPING.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.POS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.POS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.POS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.POS_ISAUTH == true || m.POS_MAKER_ID != SessionUser).OrderByDescending(m => m.POS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("PortOfShipping", edit);
        }
        [HttpPost]
        public ActionResult PortOfShippingFilter(ManagePortOfShippingFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "PortOfShipping", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_PORT_OF_SHIPPING> List = context.PSW_PORT_OF_SHIPPING.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.POS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.POS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.POS_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.POS_ISAUTH == true || m.POS_MAKER_ID != SessionUser).OrderByDescending(m => m.POS_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("PortOfShipping", edit);
        }
        [ChildActionOnly]
        public PartialViewResult PortOfShippingList(ManagePortOfShippingFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_PORT_OF_SHIPPING> ViewData = new List<PSW_PORT_OF_SHIPPING>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_PORT_OF_SHIPPING.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.POS_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.POS_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.POS_ID).ToList();
                    if (ViewData.Count() > 0)
                        ViewData = ViewData.Where(m => m.POS_MAKER_ID != SessionUser || m.POS_ISAUTH == true).OrderByDescending(m => m.POS_ID).ToList();
                }
                else
                    ViewData = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_MAKER_ID != SessionUser || m.POS_ISAUTH == true).OrderByDescending(m => m.POS_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizePortOfShipping(int POS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "PortOfShipping", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_PORT_OF_SHIPPING UpdateEntity = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_ID == POS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.POS_ISAUTH = true;
                            UpdateEntity.POS_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.POS_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Shipping Port : " + UpdateEntity.POS_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + POS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizePortOfShipping(int POS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "PortOfShipping", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_PORT_OF_SHIPPING UpdateEntity = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_ID == POS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_PORT_OF_SHIPPING.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Shipping Port : " + UpdateEntity.POS_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + POS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkPortOfShipping(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "PortOfShipping", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_PORT_OF_SHIPPING shippingport = new PSW_PORT_OF_SHIPPING();
                                    if (row["Code"].ToString().Length > 2)
                                    {
                                        string CountryName = row["Name"].ToString();
                                        bool CheckIfExist = false;
                                        shippingport = context.PSW_PORT_OF_SHIPPING.Where(m => m.POS_NAME == CountryName).FirstOrDefault();

                                        if (shippingport == null)
                                        {
                                            shippingport = new PSW_PORT_OF_SHIPPING();
                                            shippingport.POS_ID = Convert.ToInt32(context.PSW_PORT_OF_SHIPPING.Max(m => (decimal?)m.POS_ID)) + 1;
                                            shippingport.POS_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            shippingport.POS_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["CountryCode"] != null || row["CountryCode"].ToString() != "" || row["CountryCode"].ToString() == "")
                                        {
                                            string CountryCode = row["CountryCode"].ToString();
                                            shippingport.POS_COUNTRY_CODE = CountryCode;
                                            if (row["Name"].ToString() != "" && row["Name"] != null)
                                            {
                                                string Name = row["Name"].ToString();
                                                shippingport.POS_NAME = Name;
                                                if (row["Code"].ToString() != "" && row["Code"] != null)
                                                {
                                                    string Code = row["Code"].ToString();
                                                    shippingport.POS_CODE = Code;
                                                    if (row["PortTypeCode"].ToString() != "" && row["PortTypeCode"] != null)
                                                    {
                                                        string PortTypeCode = row["PortTypeCode"].ToString();
                                                        shippingport.POS_PORT_TYPE_CODE = PortTypeCode;

                                                        shippingport.POS_MAKER_ID = Session["USER_ID"].ToString();
                                                        shippingport.POS_CHECKER_ID = Session["USER_ID"].ToString(); ;
                                                        shippingport.POS_ISAUTH = true;
                                                        if (CheckIfExist == false)
                                                        {
                                                            context.PSW_PORT_OF_SHIPPING.Add(shippingport);
                                                        }
                                                        else
                                                        {
                                                            context.Entry(shippingport).State = EntityState.Modified;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid Port Type Code " + row["PortTypeCode"].ToString() + "  At Country Name : " + row["Name"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid Code " + row["Code"].ToString() + "  At Country Name : " + row["Name"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Country Name " + row["Name"].ToString() + "  At Country Code : " + row["CountryCode"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Country Code " + row["CountryCode"].ToString() + "  At Country Name : " + row["Name"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Manage Email Setup
        public ActionResult EmailSetup(ManageEmailSetupFilter edit, int E = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_EMAIL_CONFIGRATION> List = context.PSW_EMAIL_CONFIGRATION.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.EC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.EC_IS_AUTH == true || m.EC_MAKER_ID != SessionUser).OrderByDescending(m => m.EC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (E != 0)
                        {
                            edit.Entity = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_ID == E).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + E;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EmailSetup(ManageEmailSetupFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EMAIL_CONFIGRATION UpdateEntity = new PSW_EMAIL_CONFIGRATION();

                        PSW_EMAIL_CONFIGRATION CheckIfExist = new PSW_EMAIL_CONFIGRATION();
                        if (db_table.Entity.EC_ID == 0)
                        {
                            UpdateEntity.EC_ID = Convert.ToInt32(context.PSW_EMAIL_CONFIGRATION.Max(m => (decimal?)m.EC_ID)) + 1;
                            UpdateEntity.EC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_ID == db_table.Entity.EC_ID).FirstOrDefault();
                            UpdateEntity.EC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.EC_SEREVER_HOST = db_table.Entity.EC_SEREVER_HOST;
                        UpdateEntity.EC_CREDENTIAL_ID = db_table.Entity.EC_CREDENTIAL_ID;
                        UpdateEntity.EC_EMAIL_PORT = db_table.Entity.EC_EMAIL_PORT;
                        UpdateEntity.EC_CC_EMAIL = db_table.Entity.EC_CC_EMAIL;
                        UpdateEntity.EC_STATUS = false;
                        UpdateEntity.EC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.EC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.EC_CHECKER_ID = UpdateEntity.EC_MAKER_ID;
                            UpdateEntity.EC_IS_AUTH = true;
                        }
                        else
                        {
                            UpdateEntity.EC_CHECKER_ID = null;
                            UpdateEntity.EC_IS_AUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_EMAIL_CONFIGRATION.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        public ActionResult EmailSetupFilter(ManageEmailSetupFilter edit, int E = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_EMAIL_CONFIGRATION> List = context.PSW_EMAIL_CONFIGRATION.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.EC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.EC_IS_AUTH == true || m.EC_MAKER_ID != SessionUser).OrderByDescending(m => m.EC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("EmailSetup", edit);
        }
        [HttpPost]
        public ActionResult EmailSetupFilter(ManageEmailSetupFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_EMAIL_CONFIGRATION> List = context.PSW_EMAIL_CONFIGRATION.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.EC_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.EC_IS_AUTH == true || m.EC_MAKER_ID != SessionUser).OrderByDescending(m => m.EC_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("EmailSetup", edit);
        }
        [ChildActionOnly]
        public PartialViewResult ManageEmailSetupList(ManageEmailSetupFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_EMAIL_CONFIGRATION> Data = new List<PSW_EMAIL_CONFIGRATION>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    Data = context.PSW_EMAIL_CONFIGRATION.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.EC_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.EC_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.EC_ID).ToList();
                    if (Data.Count() > 0)
                        Data = Data.Where(m => m.EC_MAKER_ID != SessionUser || m.EC_IS_AUTH == true).OrderByDescending(m => m.EC_ID).ToList();
                }
                else
                    Data = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_MAKER_ID != SessionUser || m.EC_IS_AUTH == true).OrderByDescending(m => m.EC_ID).Take(10).ToList();
                return PartialView(Data);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeEmailSetup(int EC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EMAIL_CONFIGRATION UpdateEntity = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.EC_IS_AUTH = true;
                            UpdateEntity.EC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.EC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Setup Email : " + UpdateEntity.EC_CREDENTIAL_ID + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + EC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectEmailSetup(int EC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_EMAIL_CONFIGRATION UpdateEntity = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_EMAIL_CONFIGRATION.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Setup Email : " + UpdateEntity.EC_CREDENTIAL_ID + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + EC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult UpdateEmailSetupStatus(int EC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        int RowCount = 0;
                        PSW_EMAIL_CONFIGRATION EntityToUpdate = new PSW_EMAIL_CONFIGRATION();
                        List<PSW_EMAIL_CONFIGRATION> DeactiveStatus = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_ID != EC_ID).ToList();
                        if (DeactiveStatus.Count > 0)
                        {
                            foreach (PSW_EMAIL_CONFIGRATION item in DeactiveStatus)
                            {
                                PSW_EMAIL_CONFIGRATION GetListOfEmails = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_ID == item.EC_ID).FirstOrDefault();
                                if (GetListOfEmails != null)
                                {
                                    EntityToUpdate = GetListOfEmails;
                                    EntityToUpdate.EC_STATUS = false;
                                    context.Entry(EntityToUpdate).State = EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                            {
                                RowCount = 0;
                                PSW_EMAIL_CONFIGRATION CheckEntity = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                                if (CheckEntity != null)
                                {
                                    EntityToUpdate = new PSW_EMAIL_CONFIGRATION();
                                    EntityToUpdate = CheckEntity;
                                    EntityToUpdate.EC_STATUS = true;
                                    context.Entry(EntityToUpdate).State = EntityState.Modified;
                                    RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                        message = "Current email server is changed to : " + CheckEntity.EC_CREDENTIAL_ID;
                                }

                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + EC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Unit Of Measurement(UOM)
        public ActionResult UOM(ManageUOMFilter edit, int U = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "UOM", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_UNIT_OF_MEASUREMENT> List = context.PSW_UNIT_OF_MEASUREMENT.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.UOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.UOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.UOM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.UOM_ISAUTH == true || m.UOM_MAKER_ID != SessionUser).OrderByDescending(m => m.UOM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (U != 0)
                        {
                            edit.Entity = context.PSW_UNIT_OF_MEASUREMENT.Where(m => m.UOM_ID == U).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + U;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UOM(ManageUOMFilter db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "UOM", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_UNIT_OF_MEASUREMENT UpdateEntity = new PSW_UNIT_OF_MEASUREMENT();

                        PSW_UNIT_OF_MEASUREMENT CheckIfExist = new PSW_UNIT_OF_MEASUREMENT();
                        if (db_table.Entity.UOM_ID == 0)
                        {
                            UpdateEntity.UOM_ID = Convert.ToInt32(context.PSW_UNIT_OF_MEASUREMENT.Max(m => (decimal?)m.UOM_ID)) + 1;
                            UpdateEntity.UOM_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_UNIT_OF_MEASUREMENT.Where(m => m.UOM_ID == db_table.Entity.UOM_ID).FirstOrDefault();
                            UpdateEntity.UOM_EDIT_DATETIEM = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.UOM_CODE = db_table.Entity.UOM_CODE;
                        UpdateEntity.UOM_DESCRIPTION = db_table.Entity.UOM_DESCRIPTION;
                        UpdateEntity.UOM_STATUS = db_table.Entity.UOM_STATUS;
                        UpdateEntity.UOM_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.UOM_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.UOM_CHECKER_ID = UpdateEntity.UOM_MAKER_ID;
                            UpdateEntity.UOM_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.UOM_CHECKER_ID = null;
                            UpdateEntity.UOM_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PSW_UNIT_OF_MEASUREMENT.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        public ActionResult UOMFilter(ManageUOMFilter edit, int U = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "UOM", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_UNIT_OF_MEASUREMENT> List = context.PSW_UNIT_OF_MEASUREMENT.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.UOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.UOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.UOM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.UOM_ISAUTH == true || m.UOM_MAKER_ID != SessionUser).OrderByDescending(m => m.UOM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                        if (U != 0)
                        {
                            edit.Entity = context.PSW_UNIT_OF_MEASUREMENT.Where(m => m.UOM_ID == U).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + U;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("UOM", edit);
        }
        [HttpPost]
        public ActionResult UOMFilter(ManageUOMFilter edit)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "UOM", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (edit.FromDate != null && edit.ToDate != null)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            List<PSW_UNIT_OF_MEASUREMENT> List = context.PSW_UNIT_OF_MEASUREMENT.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.UOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(edit.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.UOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(edit.ToDate)).OrderByDescending(m => m.UOM_ID).ToList();
                            if (List.Count() > 0)
                            {
                                List = List.Where(m => m.UOM_ISAUTH == true || m.UOM_MAKER_ID != SessionUser).OrderByDescending(m => m.UOM_ID).ToList();
                                if (List.Count() == 0)
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                            }
                            else
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(edit.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy") + " End date : " + Convert.ToDateTime(edit.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            if (edit.FromDate != null || edit.ToDate != null)
                            {
                                if (edit.FromDate == null)
                                    message = "Select From date to continue";
                                if (edit.ToDate == null)
                                    message = "Select To date to continue";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View("UOM", edit);
        }
        [ChildActionOnly]
        public PartialViewResult UOMList(ManageUOMFilter Filter)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_UNIT_OF_MEASUREMENT> ViewData = new List<PSW_UNIT_OF_MEASUREMENT>();
                if (Filter.FromDate != null && Filter.ToDate != null)
                {
                    ViewData = context.PSW_UNIT_OF_MEASUREMENT.Where(m => System.Data.Entity.DbFunctions.TruncateTime(m.UOM_ENTRY_DATETIME) >= System.Data.Entity.DbFunctions.TruncateTime(Filter.FromDate) && System.Data.Entity.DbFunctions.TruncateTime(m.UOM_ENTRY_DATETIME) <= System.Data.Entity.DbFunctions.TruncateTime(Filter.ToDate)).OrderByDescending(m => m.UOM_ID).ToList();
                    if (ViewData.Count() > 0)
                        ViewData = ViewData.Where(m => m.UOM_MAKER_ID != SessionUser || m.UOM_ISAUTH == true).OrderByDescending(m => m.UOM_ID).ToList();
                }
                else
                    ViewData = context.PSW_UNIT_OF_MEASUREMENT.Where(m => m.UOM_MAKER_ID != SessionUser || m.UOM_ISAUTH == true).OrderByDescending(m => m.UOM_ID).Take(10).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeUOM(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "UOM", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_UNIT_OF_MEASUREMENT UpdateEntity = context.PSW_UNIT_OF_MEASUREMENT.Where(m => m.UOM_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.UOM_ISAUTH = true;
                            UpdateEntity.UOM_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.UOM_EDIT_DATETIEM = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "UOM : " + UpdateEntity.UOM_CODE + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectUOM(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "UOM", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_UNIT_OF_MEASUREMENT UpdateEntity = context.PSW_UNIT_OF_MEASUREMENT.Where(m => m.UOM_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_UNIT_OF_MEASUREMENT.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "UOM : " + UpdateEntity.UOM_CODE + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public FileStreamResult BulkUOM(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "UOM", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PSW_UNIT_OF_MEASUREMENT UOM = new PSW_UNIT_OF_MEASUREMENT();
                                    if (row["UOM Code"].ToString().Length > 0)
                                    {
                                        string UOMCode = row["UOM Code"].ToString();
                                        bool CheckIfExist = false;
                                        UOM = context.PSW_UNIT_OF_MEASUREMENT.Where(m => m.UOM_CODE == UOMCode).FirstOrDefault();

                                        if (UOM == null)
                                        {
                                            UOM = new PSW_UNIT_OF_MEASUREMENT();
                                            UOM.UOM_ID = Convert.ToInt32(context.PSW_UNIT_OF_MEASUREMENT.Max(m => (decimal?)m.UOM_ID)) + 1;
                                            UOM.UOM_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            UOM.UOM_EDIT_DATETIEM = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["UOM Code"] != null || row["UOM Code"].ToString() != "")
                                        {
                                            string uom_code = row["UOM Code"].ToString();
                                            UOM.UOM_CODE = uom_code;
                                            if (row["UOM Description"].ToString() != "" && row["UOM Description"] != null)
                                            {
                                                string uom_desc = row["UOM Description"].ToString();
                                                UOM.UOM_DESCRIPTION = uom_desc;
                                                UOM.UOM_MAKER_ID = Session["USER_ID"].ToString();
                                                UOM.UOM_CHECKER_ID = Session["USER_ID"].ToString();
                                                UOM.UOM_ISAUTH = true;
                                                UOM.UOM_STATUS = true;
                                                if (CheckIfExist == false)
                                                {
                                                    context.PSW_UNIT_OF_MEASUREMENT.Add(UOM);
                                                }
                                                else
                                                {
                                                    context.Entry(UOM).State = EntityState.Modified;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid UOM Code : " + row["UOM Code"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid UOM Description : " + row["UOM Description"].ToString() + "  At Code : " + row["UOM Code"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

    }
}
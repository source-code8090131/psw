﻿using PSW.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Controllers
{
    public class SystemSetupController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: SystemSetup
        #region System Setup
        public ActionResult Index(int S = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (S != 0)
                        {
                            PSW_SYSTEM_SETUP edit = context.PSW_SYSTEM_SETUP.Where(m => m.SS_ID == S).FirstOrDefault();
                            if (edit != null)
                            {
                                //edit.SS_HASH = DAL.Decrypt(edit.SS_HASH);
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + S;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "SystemSetup", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(PSW_SYSTEM_SETUP Entity)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    try
                    {
                        if (Entity != null)
                        {
                            PSW_SYSTEM_SETUP Update = context.PSW_SYSTEM_SETUP.Where(m => m.SS_ID == Entity.SS_ID).FirstOrDefault();
                            if (Update != null)
                            {
                                Update.SS_HASH = DAL.Encrypt(Entity.SS_HASH);
                                Update.SS_IS_AUTH = true;
                                Update.SS_ENTRY_DATETIME = DateTime.Now;
                                Update.SS_MAKER_ID = Session["USER_ID"].ToString();
                                context.Entry(Update).State = EntityState.Modified;
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    ModelState.Clear();
                                    message = "The system setup record has been successfully updated.";
                                }
                                return View();
                            }
                            else
                            {
                                message = "Select record to continue.";
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "SystemSetup", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [ChildActionOnly]
        public ActionResult IndexList()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetup", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<PSW_SYSTEM_SETUP> Data = context.PSW_SYSTEM_SETUP.ToList();
                    return PartialView(Data);
                }
                else
                    return PartialView(null);
            }
        }
        #endregion
    }
}
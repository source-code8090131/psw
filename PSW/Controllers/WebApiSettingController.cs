﻿using PSW.Models;   
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PSW.Controllers
{
    public class WebApiSettingController : Controller
    {
        PSWEntities context = new PSWEntities();
        // GET: WebApiSetting
        #region Process Code
        public ActionResult ProcessCode(int P = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "ProcessCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (P != 0)
                        {
                            PSW_PROCESS_CODE edit = context.PSW_PROCESS_CODE.Where(m => m.PC_ID == P).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + P;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult ProcessCode(PSW_PROCESS_CODE db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "ProcessCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_PROCESS_CODE UpdateEntity = new PSW_PROCESS_CODE();

                        PSW_PROCESS_CODE CheckIfExist = new PSW_PROCESS_CODE();
                        if (db_table.PC_ID == 0)
                        {
                            UpdateEntity.PC_ID = Convert.ToInt32(context.PSW_PROCESS_CODE.Max(m => (decimal?)m.PC_ID)) + 1;
                            UpdateEntity.PC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_PROCESS_CODE.Where(m => m.PC_ID == db_table.PC_ID).FirstOrDefault();
                            UpdateEntity.PC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.PC_NAME = db_table.PC_NAME;
                        UpdateEntity.PC_DETAIL = db_table.PC_DETAIL;
                        UpdateEntity.PC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.PC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.PC_CHECKER_ID = UpdateEntity.PC_MAKER_ID;
                            UpdateEntity.PC_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.PC_CHECKER_ID = null;
                            UpdateEntity.PC_ISAUTH = false;
                        }
                        UpdateEntity.PC_STATUS = db_table.PC_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_PROCESS_CODE.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [ChildActionOnly]
        public PartialViewResult ProcessCodeVIew()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_PROCESS_CODE> ViewData = context.PSW_PROCESS_CODE.Where(m => m.PC_ISAUTH == true || m.PC_MAKER_ID != SessionUser).OrderByDescending(m => m.PC_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeProcessCode(int PC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "ProcessCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_PROCESS_CODE UpdateEntity = context.PSW_PROCESS_CODE.Where(m => m.PC_ID == PC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.PC_ISAUTH = true;
                            UpdateEntity.PC_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.PC_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Process Code : " + UpdateEntity.PC_NAME + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + PC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectAuthorizeProcessCode(int PC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "ProcessCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_PROCESS_CODE UpdateEntity = context.PSW_PROCESS_CODE.Where(m => m.PC_ID == PC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_PROCESS_CODE.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Process Code : " + UpdateEntity.PC_NAME + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Response Code
        public ActionResult ResponseCode(int R = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "ResponseCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (R != 0)
                        {
                            PSW_RESPONSE_CODE edit = context.PSW_RESPONSE_CODE.Where(m => m.RES_CODE_ID == R).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + R;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult ResponseCode(PSW_RESPONSE_CODE db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "ResponseCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_RESPONSE_CODE UpdateEntity = new PSW_RESPONSE_CODE();

                        PSW_RESPONSE_CODE CheckIfExist = new PSW_RESPONSE_CODE();
                        if (db_table.RES_CODE_ID == 0)
                        {
                            UpdateEntity.RES_CODE_ID = Convert.ToInt32(context.PSW_RESPONSE_CODE.Max(m => (decimal?)m.RES_CODE_ID)) + 1;
                            UpdateEntity.RES_CODE_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_RESPONSE_CODE.Where(m => m.RES_CODE_ID == db_table.RES_CODE_ID).FirstOrDefault();
                            UpdateEntity.RES_CODE_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.RES_CODE = db_table.RES_CODE;
                        UpdateEntity.RES_CODE_STATUS_NAME = db_table.RES_CODE_STATUS_NAME;
                        UpdateEntity.RES_CODE_DESCRIPTION = db_table.RES_CODE_DESCRIPTION;
                        UpdateEntity.RES_CODE_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.RES_CODE_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.RES_CODE_CHECKER_ID = UpdateEntity.RES_CODE_MAKER_ID;
                            UpdateEntity.RES_CODE_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.RES_CODE_CHECKER_ID = null;
                            UpdateEntity.RES_CODE_ISAUTH = false;
                        }
                        UpdateEntity.RES_CODE_STATUS = db_table.RES_CODE_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_RESPONSE_CODE.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [ChildActionOnly]
        public PartialViewResult ResponseCodeView()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_RESPONSE_CODE> ViewData = context.PSW_RESPONSE_CODE.Where(m => m.RES_CODE_ISAUTH == true || m.RES_CODE_MAKER_ID != SessionUser).OrderByDescending(m => m.RES_CODE_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeResponseCode(int RES_CODE_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "ResponseCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_RESPONSE_CODE UpdateEntity = context.PSW_RESPONSE_CODE.Where(m => m.RES_CODE_ID == RES_CODE_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.RES_CODE_ISAUTH = true;
                            UpdateEntity.RES_CODE_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.RES_CODE_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Response Code : " + UpdateEntity.RES_CODE + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + RES_CODE_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectAuthorizeResponseCode(int RES_CODE_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "ResponseCode", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_RESPONSE_CODE UpdateEntity = context.PSW_RESPONSE_CODE.Where(m => m.RES_CODE_ID == RES_CODE_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_RESPONSE_CODE.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Response Code : " + UpdateEntity.RES_CODE + " is rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + RES_CODE_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Methor ID
        public ActionResult MethodID(int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "MethodID", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (M != 0)
                        {
                            PSW_PSW_METHOD_ID edit = context.PSW_PSW_METHOD_ID.Where(m => m.M_ID == M).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + M;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult MethodID(PSW_PSW_METHOD_ID db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "MethodID", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_PSW_METHOD_ID UpdateEntity = new PSW_PSW_METHOD_ID();

                        PSW_PSW_METHOD_ID CheckIfExist = new PSW_PSW_METHOD_ID();
                        if (db_table.M_ID == 0)
                        {
                            UpdateEntity.M_ID = Convert.ToInt32(context.PSW_PSW_METHOD_ID.Max(m => (decimal?)m.M_ID)) + 1;
                            UpdateEntity.M_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PSW_PSW_METHOD_ID.Where(m => m.M_ID == db_table.M_ID).FirstOrDefault();
                            UpdateEntity.M_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.M_CODE = db_table.M_CODE;
                        UpdateEntity.M_DESCRIPTION = db_table.M_DESCRIPTION;
                        UpdateEntity.M_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.M_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.M_CHECKER_ID = UpdateEntity.M_MAKER_ID;
                            UpdateEntity.M_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.M_CHECKER_ID = null;
                            UpdateEntity.M_ISAUTH = false;
                        }
                        UpdateEntity.M_STATUS = db_table.M_STATUS;
                        if (CheckIfExist == null)
                            context.PSW_PSW_METHOD_ID.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [ChildActionOnly]
        public PartialViewResult MethodIDView()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PSW_PSW_METHOD_ID> ViewData = context.PSW_PSW_METHOD_ID.Where(m => m.M_MAKER_ID != SessionUser || m.M_ISAUTH == true).OrderByDescending(m => m.M_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeMethodID(int M_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "MethodID", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_PSW_METHOD_ID UpdateEntity = context.PSW_PSW_METHOD_ID.Where(m => m.M_ID == M_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.M_ISAUTH = true;
                            UpdateEntity.M_CHECKER_ID = Session["USER_ID"].ToString();
                            UpdateEntity.M_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Method ID : " + UpdateEntity.M_CODE + " is successfully Authorized";
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + M_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectAuthorizeMethodID(int M_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "MethodID", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_PSW_METHOD_ID UpdateEntity = context.PSW_PSW_METHOD_ID.Where(m => m.M_ID == M_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            context.PSW_PSW_METHOD_ID.Remove(UpdateEntity);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Method Code : " + UpdateEntity.M_CODE + " rejected successfully";
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + M_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        public PartialViewResult MessageList()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "MessageList", Session["USER_ID"].ToString()))
                {
                    return PartialView(context.PSW_MESSAGES.ToList());
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView(null);
            }
        }
        public ActionResult TestingEDI(int M = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "TestingEDI", Session["USER_ID"].ToString()))
                {
                    ViewBag.message = "";
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }
        [HttpPost]
        public ActionResult TestApiToken()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "TestingEDI", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString();
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (var client = new HttpClient(clientHandler, false))
                            {
                                System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                                System.Net.ServicePointManager.Expect100Continue = false;
                                System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)768 | (System.Net.SecurityProtocolType)3072;
                                client.DefaultRequestHeaders.ExpectContinue = false;
                                client.BaseAddress = new Uri(BASE_URL + "/api/dealers/citi/ediSend/token/");
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                                var request = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress);
                                TokenPSWKeys tokent = new TokenPSWKeys();
                                tokent.client_id = System.Configuration.ConfigurationManager.AppSettings["CID"].ToString();
                                tokent.client_secret = System.Configuration.ConfigurationManager.AppSettings["CSECRET"].ToString();
                                tokent.grant_type = "client_credentials";
                                var keyValues = new List<KeyValuePair<string, string>>();
                                keyValues.Add(new KeyValuePair<string, string>("client_id", tokent.client_id));
                                keyValues.Add(new KeyValuePair<string, string>("client_secret", tokent.client_secret));
                                keyValues.Add(new KeyValuePair<string, string>("grant_type", tokent.grant_type));
                                request.Content = new FormUrlEncodedContent(keyValues);
                                var respone = client.SendAsync(request).Result;
                                var contents = respone.Content.ReadAsStringAsync().Result;
                                message += "Client Request Details : <br>" + Environment.NewLine;
                                string Content = "";
                                foreach (var item in keyValues)
                                {
                                    Content += " " + item.Key + " = " + item.Value + " ";
                                }
                                message += " URL : ( " + request.RequestUri + " ), REQUEST METHOD : ( " + request.Method + " ) Content Type : ( " + request.Content.GetType() + " , Content : ( " + Content + " ) <br> <br>";
                                message += "Response Recieved Below " + Environment.NewLine;
                                message += "Response : " + contents + Environment.NewLine;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View("TestingEDI");
        }

        [HttpPost]
        public ActionResult TestApiTokenWithCertificate()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "TestingEDI", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString();
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            using (var client = new HttpClient(clientHandler, false))
                            {
                                client.BaseAddress = new Uri(BASE_URL + "/api/dealers/citi/ediSend/token/");
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                                var request = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress);
                                TokenPSWKeys tokent = new TokenPSWKeys();
                                tokent.client_id = System.Configuration.ConfigurationManager.AppSettings["CID"].ToString();
                                tokent.client_secret = System.Configuration.ConfigurationManager.AppSettings["CSECRET"].ToString();
                                tokent.grant_type = "client_credentials";
                                var keyValues = new List<KeyValuePair<string, string>>();
                                keyValues.Add(new KeyValuePair<string, string>("client_id", tokent.client_id));
                                keyValues.Add(new KeyValuePair<string, string>("client_secret", tokent.client_secret));
                                keyValues.Add(new KeyValuePair<string, string>("grant_type", tokent.grant_type));
                                request.Content = new FormUrlEncodedContent(keyValues);
                                var respone = client.SendAsync(request).Result;
                                var contents = respone.Content.ReadAsStringAsync().Result;
                                message += "Client Request Details : <br>" + Environment.NewLine;
                                string Content = "";
                                foreach (var item in keyValues)
                                {
                                    Content += "" + item.Key + " = " + item.Value;
                                }
                                message += " URL : ( " + request.RequestUri + " ), REQUEST METHOD : ( " + request.Method + " ) Content Type : ( " + request.Content.GetType() + " , Content : ( " + Content + " ) <br> <br>";
                                message += "Response Recieved Below " + Environment.NewLine;
                                message += "Response : " + contents + Environment.NewLine;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View("TestingEDI");
        }

        [HttpPost]
        public ActionResult ValidateApiToken()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "TestingEDI", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PSW_TOKENS token = DAL.CheckTokenAuthentication();
                        if (token != null && token.PT_TOKEN != null)
                        {
                            message = "Token Aquiring was successfull" + Environment.NewLine;
                            message += "Acquired Token : Start :::" + token.PT_TOKEN + "::: END" + Environment.NewLine;
                            message += "Expiry Time : " + token.PT_EXPIRY_DATETIME;
                        }
                        else
                        {
                            message = DAL.message;
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View("TestingEDI");
        }

        [HttpPost]
        public ActionResult TestApiMethods(string cid, string token, string Uri, string testingcase)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "TestingEDI", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (cid != "")
                        {
                            if (token != "")
                            {
                                int ClientID = Convert.ToInt16(cid);
                                DAL.message = "";
                                string ClientName = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault().C_NAME;
                                if (testingcase == "BOTH")
                                {
                                    message = SendRequestMethodId1512BOTH(ClientID, token, Uri);
                                }
                                else if (testingcase == "RH")
                                {
                                    message = SendRequestMethodId1512RH(ClientID, token, Uri);
                                }
                                else if (testingcase == "CH")
                                {
                                    message = SendRequestMethodId1512CH(ClientID, token, Uri);
                                }
                                else if (testingcase == "OLD")
                                {
                                    message = SendRequestMethodId1512(ClientID, token);
                                }
                            }
                            else
                            {
                                message = "Please Insert a Valid Token.";
                            }
                        }
                        else
                        {
                            message = "Invalid Client Id Provided.";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View("TestingEDI");
        }

        [HttpPost]
        public ActionResult GetSignature(string SignatureData)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "TestingEDI", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (SignatureData != null && SignatureData != "")
                        {
                            dynamic RequestData = System.Web.Helpers.Json.Decode(SignatureData);

                            string Serialized = System.Text.Json.JsonSerializer.Serialize(RequestData);
                            string CreatedSignature = DAL.GetSignature(Serialized);
                            message = " --------------  Created Signature :" + CreatedSignature + " -------------- " + Environment.NewLine;
                            message += "  --------------  Source Data Serialized :" + Serialized + " -------------- " + Environment.NewLine;
                        }
                        else
                            message = "Please Provide Signature Data.";
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View("TestingEDI");
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }

        [HttpPost]
        public ActionResult DataTesting(string DataTestQuery)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("WebApiSetting", "TestingEDI", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (DataTestQuery != null && DataTestQuery != "")
                        {
                            System.Data.SqlClient.SqlConnection con = (System.Data.SqlClient.SqlConnection)context.Database.Connection;
                            System.Data.SqlClient.SqlDataAdapter adap = new System.Data.SqlClient.SqlDataAdapter(DataTestQuery,con);
                            System.Data.DataTable dt = new System.Data.DataTable();
                            adap.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                string html = "<table id='example1' class='table table-bordered text-center table-hover'>";
                                //add header row
                                html += "<thead>";
                                html += "<tr>";
                                for (int i = 0; i < dt.Columns.Count; i++)
                                    html += "<th>" + dt.Columns[i].ColumnName + "</th>";
                                html += "</tr>";
                                html += "</thead>";
                                //add rows
                                html += "<tbody>";
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    html += "<tr>";
                                    for (int j = 0; j < dt.Columns.Count; j++)
                                        html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
                                    html += "</tr>";
                                }
                                html += "</tbody>";
                                html += "</table>";
                                message = html;
                            }
                            else
                                message = "No Records Found";
                        }
                        else
                            message = "Please Provide Data.";
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View("TestingEDI");
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Authentication");
            }
        }

        public string SendRequestMethodId1512BOTH(int Cid, string Token,string Uri)
        {
            string message = "";
            try
            {
                 message += "Sending Message Request. 1512" + " @ DATETIME : " + DateTime.Now + Environment.NewLine;
                PSWEntities context = new PSWEntities();
                PSW_CLIENT_INFO clientinfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == Cid).FirstOrDefault();
                PSW_ACCOUNT_STATUS AccountStatusInfo = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == clientinfo.C_ACCOUNT_STATUS_ID).FirstOrDefault();
                List<int?> assignPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == clientinfo.C_ID && m.APMC_STATUS == true).Select(m => m.APMC_PAY_MODE_ID).ToList();
                List<int?> AuthMentModesImport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 1).Select(u => u.APM_CODE).ToList();
                List<int?> AuthMentModesExport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 2).Select(u => u.APM_CODE).ToList();
                PSW_ACCOUNT_TYPE AccountTypeInfo = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ID == clientinfo.C_ACCOUNT_TYPE_ID).FirstOrDefault();
                string response = "";
                /// If PROD 
                string BASE_URL = Uri; //System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/a/d/i/edi";
                                       //// if DEV
                                       //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                                       /////
                                       ///
                message += "Requesting  @ URL  " + BASE_URL + Environment.NewLine;
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                    using (HttpClient client = new HttpClient(clientHandler))
                    {
                        message += "Adding Request Header : Bearer " + Token.Substring(0, 15) + "......" + Environment.NewLine;
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                        using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            message += "Adding Request Header : Content Type : application/json " + Environment.NewLine;
                            req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                            ShareUpdatedAuthPayModes RequestData = new ShareUpdatedAuthPayModes();
                            RequestData.messageId = Guid.NewGuid().ToString();
                            RequestData.timestamp = Repository.ConvertToTimestamp(DateTime.Now).ToString();
                            RequestData.senderId = "CBN";
                            RequestData.receiverId = "PSW";
                            RequestData.methodId = "1512";
                            RequestData.data = new ShareUpdatedAuthPayModes.DetailData
                            {
                                iban = clientinfo.C_IBAN_NO,
                                authorizedPaymentModesForImport =
                                AuthMentModesImport.ConvertAll<string>(x => x.ToString()),
                                authorizedPaymentModesForExport =
                                AuthMentModesExport.ConvertAll<string>(x => x.ToString()),
                            };
                            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));

                            message += "Created Signature " + RequestData.signature + Environment.NewLine;
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req.Content = content;
                            req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            message += "Request Data To Post " + dataAsJson.Replace(@"\", "") + Environment.NewLine;
                            message += Environment.NewLine + " Complete Request : " + req.ToString();
                            HttpResponseMessage responsse = client.SendAsync(req).Result;
                            string Content = responsse.Content.ReadAsStringAsync().Result;
                            message += "Response Recieved : { Status : " + responsse.StatusCode + " Content : " + Content + "  Reason Pharase " + responsse.ReasonPhrase + "  }    response Raw : { " + responsse.ToString() + " }. ";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                message += " Exception Occurred :" + ex.Message;
            }
            return message;
        }

        public string SendRequestMethodId1512RH(int Cid, string Token, string Uri)
        {
            string message = "";
            try
            {
                message += "Sending Message Request. 1512" + " @ DATETIME : " + DateTime.Now + Environment.NewLine;
                PSWEntities context = new PSWEntities();
                PSW_CLIENT_INFO clientinfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == Cid).FirstOrDefault();
                PSW_ACCOUNT_STATUS AccountStatusInfo = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == clientinfo.C_ACCOUNT_STATUS_ID).FirstOrDefault();
                List<int?> assignPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == clientinfo.C_ID && m.APMC_STATUS == true).Select(m => m.APMC_PAY_MODE_ID).ToList();
                List<int?> AuthMentModesImport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 1).Select(u => u.APM_CODE).ToList();
                List<int?> AuthMentModesExport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 2).Select(u => u.APM_CODE).ToList();
                PSW_ACCOUNT_TYPE AccountTypeInfo = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ID == clientinfo.C_ACCOUNT_TYPE_ID).FirstOrDefault();
                string response = "";
                /// If PROD 
                string BASE_URL = Uri; //System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/a/d/i/edi";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                ///
                message += "Requesting  @ URL  " + BASE_URL + Environment.NewLine;
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    // Remove on PROD
                    clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                    using (HttpClient client = new HttpClient(clientHandler))
                    {
                        using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            message += "Adding Request Header : Content Type : application/json " + Environment.NewLine;
                            message += "Adding Request Authorization Header In HttpRequestMessage.Headers.Authorization AS Bearer " + Token.Substring(0, 15) + "......" + Environment.NewLine;
                            req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                            ShareUpdatedAuthPayModes RequestData = new ShareUpdatedAuthPayModes();
                            RequestData.messageId = Guid.NewGuid().ToString();
                            RequestData.timestamp = Repository.ConvertToTimestamp(DateTime.Now).ToString();
                            RequestData.senderId = "CBN";
                            RequestData.receiverId = "PSW";
                            RequestData.methodId = "1512";
                            RequestData.data = new ShareUpdatedAuthPayModes.DetailData
                            {
                                iban = clientinfo.C_IBAN_NO,
                                authorizedPaymentModesForImport =
                                AuthMentModesImport.ConvertAll<string>(x => x.ToString()),
                                authorizedPaymentModesForExport =
                                AuthMentModesExport.ConvertAll<string>(x => x.ToString()),
                            };
                            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));

                            message += "Created Signature " + RequestData.signature + Environment.NewLine;
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req.Content = content;
                            req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            message += "Request Data To Post " + dataAsJson.Replace(@"\", "") + Environment.NewLine;
                            message += Environment.NewLine + " Complete Request : " + req.ToString();
                            HttpResponseMessage responsse = client.SendAsync(req).Result;
                            message += "Response Recieved : { Status : " + responsse.StatusCode + " Content : " + responsse.Content.ToString() + "  Reason Pharase " + responsse.ReasonPhrase + "  }    response Raw : { " + responsse.ToString() + " }. ";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                message += " Exception Occurred :" + ex.Message;
            }
            return message;
        }



        public string SendRequestMethodId1512CH(int Cid, string Token, string Uri)
        {
            string message = "";
            try
            {
                message += "Sending Message Request. 1512" + " @ DATETIME : " + DateTime.Now + Environment.NewLine;
                PSWEntities context = new PSWEntities();
                PSW_CLIENT_INFO clientinfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == Cid).FirstOrDefault();
                PSW_ACCOUNT_STATUS AccountStatusInfo = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == clientinfo.C_ACCOUNT_STATUS_ID).FirstOrDefault();
                List<int?> assignPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == clientinfo.C_ID && m.APMC_STATUS == true).Select(m => m.APMC_PAY_MODE_ID).ToList();
                List<int?> AuthMentModesImport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 1).Select(u => u.APM_CODE).ToList();
                List<int?> AuthMentModesExport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 2).Select(u => u.APM_CODE).ToList();
                PSW_ACCOUNT_TYPE AccountTypeInfo = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ID == clientinfo.C_ACCOUNT_TYPE_ID).FirstOrDefault();
                string response = "";
                /// If PROD 
                string BASE_URL = Uri; //System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/a/d/i/edi";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                ///
                message += "Requesting  @ URL  " + BASE_URL + Environment.NewLine;
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                    using (HttpClient client = new HttpClient(clientHandler))
                    {
                        message += "Adding Request Authorization Header In HttpClient.DefaultRequestHeaders.Authorization AS Bearer " + Token.Substring(0, 15) + "......" + Environment.NewLine;
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                        using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            message += "Adding Request Header : Content Type : application/json " + Environment.NewLine;
                            req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            ShareUpdatedAuthPayModes RequestData = new ShareUpdatedAuthPayModes();
                            RequestData.messageId = Guid.NewGuid().ToString();
                            RequestData.timestamp = Repository.ConvertToTimestamp(DateTime.Now).ToString();
                            RequestData.senderId = "CBN";
                            RequestData.receiverId = "PSW";
                            RequestData.methodId = "1512";
                            RequestData.data = new ShareUpdatedAuthPayModes.DetailData
                            {
                                iban = clientinfo.C_IBAN_NO,
                                authorizedPaymentModesForImport =
                                AuthMentModesImport.ConvertAll<string>(x => x.ToString()),
                                authorizedPaymentModesForExport =
                                AuthMentModesExport.ConvertAll<string>(x => x.ToString()),
                            };
                            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                            message += "Created Signature " + RequestData.signature + Environment.NewLine;
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req.Content = content;
                            req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            message += "Request Data To Post " + dataAsJson.Replace(@"\", "") + Environment.NewLine;
                            message += Environment.NewLine + " Complete Request : " + req.ToString();
                            HttpResponseMessage responsse = client.SendAsync(req).Result;
                            message += "Response Recieved : { Status : " + responsse.StatusCode + " Content : " + responsse.Content.ToString() + "  Reason Pharase " + responsse.ReasonPhrase + "  }    response Raw : { " + responsse.ToString() + " }. ";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                message += " Exception Occurred :" + ex.Message;
            }
            return message;
        }


        public string SendRequestMethodId1512(int Cid, string Token)
        {
            string message = "";
            try
            {
                message += "Sending Message Request. 1512" + " @ DATETIME : " + DateTime.Now + Environment.NewLine;
                PSWEntities context = new PSWEntities();
                PSW_CLIENT_INFO clientinfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == Cid).FirstOrDefault();
                PSW_ACCOUNT_STATUS AccountStatusInfo = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == clientinfo.C_ACCOUNT_STATUS_ID).FirstOrDefault();
                List<int?> assignPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == clientinfo.C_ID && m.APMC_STATUS == true).Select(m => m.APMC_PAY_MODE_ID).ToList();
                List<int?> AuthMentModesImport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 1).Select(u => u.APM_CODE).ToList();
                List<int?> AuthMentModesExport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 2).Select(u => u.APM_CODE).ToList();
                PSW_ACCOUNT_TYPE AccountTypeInfo = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ID == clientinfo.C_ACCOUNT_TYPE_ID).FirstOrDefault();
                string response = "";
                ///// If PROD 
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/a/d/i/edi";
                //// if DEV
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                ///
                ///
                message += "Requesting  @ URL  " + BASE_URL + Environment.NewLine;
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                    using (HttpClient client = new HttpClient(clientHandler))
                    {
                        message += "Adding Request Header : Bearer " + Token.Substring(0, 15) + "......" + Environment.NewLine;
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                        using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            message += "Adding Request Header : Content Type : application/json " + Environment.NewLine;
                            req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                            ShareUpdatedAuthPayModes RequestData = new ShareUpdatedAuthPayModes();
                            RequestData.messageId = Guid.NewGuid().ToString();
                            RequestData.timestamp = Repository.ConvertToTimestamp(DateTime.Now).ToString();
                            RequestData.senderId = "CBN";
                            RequestData.receiverId = "PSW";
                            RequestData.methodId = "1512";
                            RequestData.data = new ShareUpdatedAuthPayModes.DetailData
                            {
                                iban = clientinfo.C_IBAN_NO,
                                authorizedPaymentModesForImport =
                                AuthMentModesImport.ConvertAll<string>(x => x.ToString()),
                                authorizedPaymentModesForExport =
                                AuthMentModesExport.ConvertAll<string>(x => x.ToString()),
                            };
                            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));

                            message += "Created Signature " + RequestData.signature + Environment.NewLine;
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req.Content = content;
                            req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            message += "Request Data To Post " + dataAsJson.Replace(@"\", "") + Environment.NewLine;
                            message += Environment.NewLine + " Complete Request : " + req.ToString();
                            HttpResponseMessage responsse = client.SendAsync(req).Result;
                            string Content = responsse.Content.ReadAsStringAsync().Result;
                            message += "Response Recieved : { Status : " + responsse.StatusCode + " Content : " + Content + "  Reason Pharase " + responsse.ReasonPhrase + "  }    response Raw : { " + responsse.ToString() + " }. ";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message += " Exception Occurred :" + ex.Message;
            }
            return message;
        }
        
    }
}
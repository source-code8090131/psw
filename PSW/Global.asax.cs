﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PSW.Models;

namespace PSW
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();

            string[] headers = { "Server", "X-AspNet-Version", "X-AspNetMvc-Version", "X-Powered-By" };

            if (!Response.HeadersWritten)
            {
                Response.AddOnSendingHeaders((c) =>
                {
                    if (c != null && c.Response != null && c.Response.Headers != null)
                    {
                        foreach (string header in headers)
                        {
                            if (c.Response.Headers[header] != null)
                            {
                                c.Response.Headers.Remove(header);
                            }
                        }
                    }
                });
            }
        }
        protected void Session_Start(Object sender, EventArgs e)
        {
            if (Request.IsSecureConnection == true)
                Response.Cookies["ASP.NET_SessionId"].Secure = true;
        }
        public void CheckPendingMails()
        {
            PSWEntities context = new PSWEntities();
            List<PSW_EIF_BASIC_INFO> Eifs = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_APPROVAL_STATUS == "APPROVED").ToList();
            foreach (PSW_EIF_BASIC_INFO eif in Eifs)
            {
                DateTime Case15Days = Convert.ToDateTime(eif.EIF_BI_ENTRY_DATETIME).AddDays(15);
                DateTime Case20Days = Convert.ToDateTime(eif.EIF_BI_ENTRY_DATETIME).AddDays(20);
                if (Case15Days.Date == DateTime.Now.Date || Case20Days.Date == DateTime.Now.Date)
                {

                }
            }
        }
    }
}

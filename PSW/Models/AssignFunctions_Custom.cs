﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Models
{
    public class AssignFunctions_Custom
    {
        public PSW_ROLES Entity { get; set; }
        public int?[] SelectedFunctions { get; set; }
        public IEnumerable<SelectListItem> FunctionsList { get; set; }
    }
}
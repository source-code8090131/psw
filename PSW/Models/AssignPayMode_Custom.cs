﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Models
{
    public class AssignPayMode_Custom
    {
        public PSW_ASSIGN_PAY_MODE_TO_CLIENT Entity { get; set; }
        public int?[] SelectedPayModeImport { get; set; }
        public IEnumerable<SelectListItem> PayModeListImport { get; set; }
        public int?[] SelectedPayModeExport { get; set; }
        public IEnumerable<SelectListItem> PayModeListExport { get; set; }
    }
}
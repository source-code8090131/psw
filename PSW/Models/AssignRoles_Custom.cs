﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Models
{
    public class AssignRoles_Custom
    {
        public PSW_LOGIN Entity { get; set; }
        public int?[] SelectedRoles { get; set; }
        public IEnumerable<SelectListItem> RolesList { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Models
{
    public class BDA
    {
        public PSW_EIF_BDA bda { get; set; }

        public IEnumerable<SelectListItem> EIF_BDA_EIF_REQUEST_NOList { get; set; }
        public IEnumerable<SelectListItem> EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCYList { get; set; }
        public IEnumerable<SelectListItem> EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCYList { get; set; }
        public IEnumerable<SelectListItem> EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCYList { get; set; }
        public IEnumerable<SelectListItem> EIF_BDA_CURRENCY_FCYList { get; set; }


    }
}
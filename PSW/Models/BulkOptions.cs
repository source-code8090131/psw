﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace PSW.Models
{
    public class BulkOptions
    {
        public static DataTable ReadAsDataTable(string fileName)
        {
            DataTable dataTable = new DataTable();
            try
            {
                //Open the Excel file in Read Mode using OpenXml.
                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fileName, false))
                {
                    //Read the first Sheet from Excel file.
                    Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();

                    //Get the Worksheet instance.
                    Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;

                    //Fetch all the rows present in the Worksheet.
                    IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();

                    //Loop through the Worksheet rows.
                    foreach (Row row in rows)
                    {
                        //Use the first row to add columns to DataTable.
                        if (row.RowIndex.Value == 1)
                        {
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                dataTable.Columns.Add(GetValue(doc, cell));
                            }
                        }
                        else
                        {
                            //Add rows to DataTable.
                            dataTable.Rows.Add();
                            int i = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                dataTable.Rows[dataTable.Rows.Count - 1][i] = GetValue(doc, cell);
                                i++;
                            }
                            string firstCellValue = dataTable.Rows[dataTable.Rows.Count - 1][0].ToString();
                            if (firstCellValue == "")
                            {
                                break;
                            }
                        }
                    }
                     return dataTable;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static DataTable ReadAsDataTableDE003(string fileName)
        {
            DataTable dataTable = new DataTable();
            try
            {
                //Open the Excel file in Read Mode using OpenXml.
                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fileName, false))
                {
                    //Read the first Sheet from Excel file.
                    Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();

                    //Get the Worksheet instance.
                    Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;

                    //Fetch all the rows present in the Worksheet.
                    IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();

                    //Loop through the Worksheet rows.
                    foreach (Row row in rows)
                    {
                        //Use the first row to add columns to DataTable.

                        if (dataTable.Columns.Count == 0)
                        {

                            if (row.RowIndex.Value == 3)
                            {
                                foreach (Cell cell in row.Descendants<Cell>())
                                {
                                    dataTable.Columns.Add(GetValue(doc, cell));
                                }
                            }
                        }
                        else
                        {
                            //Add rows to DataTable.
                            dataTable.Rows.Add();
                            int i = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                dataTable.Rows[dataTable.Rows.Count - 1][i] = GetValue(doc, cell);
                                i++;
                            }
                            string firstCellValue = dataTable.Rows[dataTable.Rows.Count - 1][0].ToString();
                            if (firstCellValue == "")
                            {
                                break;
                            }
                        }
                    }
                    return dataTable;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static string GetValue(SpreadsheetDocument doc, Cell cell)
        {
            string value = "";
            if (cell.CellValue != null)
            {
                value = cell.CellValue.InnerText;
                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
                }
            }
            return value;
        }

        public static DataTable ReadAsDataTableITRS(string fileName)
        {
            DataTable dataTable = new DataTable();
            try
            {
                //Open the Excel file in Read Mode using OpenXml.
                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fileName, false))
                {
                    //Read the first Sheet from Excel file.
                    Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();

                    //Get the Worksheet instance.
                    Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;

                    //Fetch all the rows present in the Worksheet.
                    IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();

                    //Loop through the Worksheet rows.
                    foreach (Row row in rows)
                    {
                        //Use the first row to add columns to DataTable.
                        if (row.RowIndex.Value == 1)
                        {
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                dataTable.Columns.Add(GetValue(doc, cell));
                            }
                        }
                        else
                        {
                            //Add rows to DataTable.
                            dataTable.Rows.Add();
                            int i = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                dataTable.Rows[dataTable.Rows.Count - 1][i] = GetValue(doc, cell);
                                i++;
                            }
                        }
                    }
                    if (dataTable.Rows.Count > 1)
                    {
                        return TuneITRSData(dataTable);
                    }
                    else
                        return dataTable;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private static DataTable TuneITRSData(DataTable dt)
        {
            DataTable ReturnDataTable = new DataTable();
            try
            {
                for (int i = 0; i < 17; i++)
                {
                    dt.Rows.RemoveAt(0);
                }
                ReturnDataTable.Columns.Add("Account");
                ReturnDataTable.Columns.Add("Date");
                ReturnDataTable.Columns.Add("Details");
                ReturnDataTable.Columns.Add("Refference");
                ReturnDataTable.Columns.Add("Description");
                ReturnDataTable.Columns.Add("ValueDate");
                ReturnDataTable.Columns.Add("Currency");
                ReturnDataTable.Columns.Add("Amount");

                while (dt.Rows.Count > 0)
                {
                    if (dt.Rows.Count > 3)
                    {
                        if (dt.Rows[0]["Column5"].ToString() == "CLOSING BALANCE")
                        {
                            for (int i = 0; i < 20; i++)
                            {
                                dt.Rows.RemoveAt(0);
                            }
                        }
                        else
                        {
                            if (dt.Rows[0]["Column5"].ToString().Contains("BALANCE BROUGHT"))
                            {
                                for (int i = 0; i < 1; i++)
                                {
                                    dt.Rows.RemoveAt(0);
                                }
                            }
                            ReturnDataTable.Rows.Add();
                            for (int i = 0; i < 3; i++)
                            {
                                InsertDataToOneRow(dt, i, ref ReturnDataTable);
                                if (dt.Rows.Count > 0)
                                    dt.Rows.RemoveAt(0);
                            }
                        }
                    }
                    else
                        return ReturnDataTable;
                }
                return ReturnDataTable;
            }
            catch (Exception exp)
            {

                throw exp;
            }

        }


        private static void InsertDataToOneRow(DataTable fromdt, int rownum, ref DataTable todt)
        {
            try
            {

                if (rownum == 0)
                {
                    todt.Rows[todt.Rows.Count - 1]["Account"] = fromdt.Rows[0]["Column3"].ToString();
                    todt.Rows[todt.Rows.Count - 1]["Date"] = fromdt.Rows[0]["Column4"].ToString();
                    todt.Rows[todt.Rows.Count - 1]["Details"] = fromdt.Rows[0]["Column5"].ToString();
                    todt.Rows[todt.Rows.Count - 1]["ValueDate"] = fromdt.Rows[0]["Column6"].ToString();
                    todt.Rows[todt.Rows.Count - 1]["Currency"] = fromdt.Rows[0]["Column7"].ToString();
                    if (fromdt.Rows[0]["Column8"].ToString() != "" && fromdt.Rows[0]["Column8"].ToString() != null)
                        todt.Rows[todt.Rows.Count - 1]["Amount"] = fromdt.Rows[0]["Column8"].ToString();
                    else
                        todt.Rows[todt.Rows.Count - 1]["Amount"] = fromdt.Rows[0]["Column9"].ToString();

                }
                else if (rownum == 1)
                {
                    todt.Rows[todt.Rows.Count - 1]["Refference"] = fromdt.Rows[0]["Column5"].ToString();
                }
                else if (rownum == 2)
                {
                    todt.Rows[todt.Rows.Count - 1]["Description"] = fromdt.Rows[0]["Column5"].ToString();
                }

            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class CustomLogin
    {
        public int LR_ROLE_ID { get; set; }
        public string LOG_USER_ID { get; set; }
        public int LOG_ID { get; set; }
        public string LOG_USER_NAME { get; set; }
        public string LOG_EMAIL { get; set; }
        public string LOG_PASSWORD { get; set; }
        public string LOG_USER_STATUS { get; set; }
        public int LR_ID { get; set; }
        public int R_ID { get; set; }
        public string R_NAME { get; set; }
        public string LOG_USER_LAST_NAME { get; set; }
        public string LOG_RIST_ID { get; set; }
        public string LOG_USER_GE_ID { get; set; }
        public string DOMAIN_NAME { get; set; }
    }
}
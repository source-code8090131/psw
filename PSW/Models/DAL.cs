﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PSW.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Data;
using System.Data.Entity;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.IO;
using System.Net;
using System.Text.Json;
using Newtonsoft.Json.Linq;

namespace PSW.Models
{
    public class DAL
    {
        PSW_LOGIN LoggedinUser;

        public static string DO_EDI = System.Configuration.ConfigurationManager.AppSettings["EDI"].ToString();

        public static string message = "";
        public static dynamic LogCurrentRequest;
        public static HttpResponseMessage CurrentResponse;
        public PSW_LOGIN GetLoggedInUser()
        {
            PSWEntities context = new PSWEntities();
            try
            {
                LoggedinUser = null;
                string USER_ID = (HttpContext.Current.Session["USER_ID"] == null || HttpContext.Current.Session["USER_ID"].ToString() == "") ? "" : HttpContext.Current.Session["USER_ID"].ToString();
                if (USER_ID != "")
                    LoggedinUser = context.PSW_LOGIN.Where(m => m.LOG_USER_ID == USER_ID).FirstOrDefault();
                return LoggedinUser;
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "GetLoggedInUser", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                throw;
            }
        }
        public static bool CheckFunctionValidity(string CName, string AName, string userID)
        {
            try
            {
                PSWEntities context = new PSWEntities();
                bool success = false;
                int FunctionId = context.PSW_FUNCTIONS.Where(m => m.F_CONTROLLER == CName && m.F_ACTION == AName).Select(m => m.F_ID).FirstOrDefault();
                List<PSW_LOGIN_RIGHTS> RoleId = context.PSW_LOGIN_RIGHTS.Where(m => m.LR_USER_ID == userID && m.LR_STATUS == true).ToList();
                foreach (PSW_LOGIN_RIGHTS CheckRoles in RoleId)
                {
                    PSW_ASSIGN_FUNCTIONS CheckFunction = context.PSW_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == CheckRoles.LR_ROLE_ID && m.AF_FUNCTION_ID == FunctionId && m.AF_STATUS == true).FirstOrDefault();
                    if (CheckFunction == null)
                    {
                        success = false;
                    }
                    else
                    {
                        success = true;
                        return success;
                    }
                }
                return success;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Encryption Dycrytion
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        #endregion
        public static List<SelectListItem> GetFilterationStatus(string TYPE)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            SelectListItem S1 = new SelectListItem();
            S1.Text = TYPE + " Request(s) -- ALL INITIATED "+ TYPE +"(s) ";
            S1.Value = "1";
            list.Add(S1);

            SelectListItem S2 = new SelectListItem();
            S2.Text = "Cancel "+ TYPE +" -- TO CANCEL THE APPROVED "+ TYPE;
            S2.Value = "2";
            list.Add(S2);

            SelectListItem S3 = new SelectListItem();
            S3.Text = "View "+ TYPE +" -- TO VIEW THE APPROVED "+ TYPE;
            S3.Value = "3";
            list.Add(S3);

            SelectListItem S4 = new SelectListItem();
            S4.Text = "Edit "+ TYPE +" -- TO EDIT THE APPROVED "+ TYPE +" CONTENTS";
            S4.Value = "4";
            list.Add(S4);

            SelectListItem S5 = new SelectListItem();
            S5.Text = "Settle "+ TYPE +" -- TO SETTLE THE PAID PAYMENT "+ TYPE;
            S5.Value = "5";
            list.Add(S5);

            SelectListItem S6 = new SelectListItem();
            S6.Text = "Transfer "+ TYPE +" -- TO TRANSFER THE APPROVED "+ TYPE +" TO ANOTHER BANK";
            S6.Value = "6";
            list.Add(S6);

            SelectListItem S7 = new SelectListItem();
            S7.Text = TYPE + " Request(s) -- ALL " + TYPE + "(s) ";
            S7.Value = "7";
            list.Add(S7);

            return list;
        }

        #region AmountInWords

        public static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }

        public static String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }

        public static String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX    
                bool isDone = false;//test if already translated    
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))    
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric    
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping    
                    String place = "";//digit grouping name:hundres,thousand,etc...    
                    switch (numDigits)
                    {
                        case 1://ones' range    

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range    
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range    
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range    
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range    
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range    
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...    
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)    
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros    
                        //if (beginsZero) word = " and " + word.Trim();    
                    }
                    //ignore digit grouping names    
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }
        public static String ConvertToWords(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "Only";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents    
                        endStr = "Paisa " + endStr;//Cents    
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }

        public static String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }

        #endregion

        #region Repository
        public static string RemoveNewLineAndTabs(string Input)
        {
            if (Input != null)
                return System.Text.RegularExpressions.Regex.Replace(Input, @"\t|\n|\r", "");
            else
               return "";
        }
        public static string GetSignature(string data)
        {
            var options = new JsonSerializerOptions()
            {
                WriteIndented = false
            };
            options.Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping;
            string JsonStr = System.Text.Json.JsonSerializer.Serialize(data, options);
            var doc = System.Text.Json.JsonSerializer.Deserialize<object>(JsonStr);
            string JsonStr1 = System.Text.Json.JsonSerializer.Serialize(doc);
            JsonDocument JsonDoc1 = JsonDocument.Parse(doc.ToString());
            JsonElement RootDoc = JsonDoc1.RootElement;
            JsonElement DataDoc = RootDoc.GetProperty("data");
            string signature = Sign(DataDoc);
            return signature;
        }
        public static string Sign(JsonElement data)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
            {
                return System.Convert.ToBase64String(algorithm.ComputeHash(System.Text.UTF8Encoding.UTF8.GetBytes(data.ToString())));
            }
        }
        public static string ConvertResponseToStringTesting(HttpResponseMessage request)
        {
            string UserMessage = "";
            try
            {
                if (request.ReasonPhrase.Contains("Excepotion : "))
                    return request.ReasonPhrase;
                else
                    return request.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {   
                UserMessage += "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    UserMessage += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
                return UserMessage;
            }    
        }

        public static HttpClient CreateRequestToSend()
        {
            PSW_TOKENS token = CheckTokenAuthentication();
            if (token != null && token.PT_TOKEN != null)
            {
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    HttpClient client = new HttpClient(clientHandler);
                    ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                    ServicePointManager.Expect100Continue = false;
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                    client.DefaultRequestHeaders.ExpectContinue = false;
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN.ToString());
                    return client;
                }
            }
            else
                return new HttpClient();
        }
        #endregion

        #region Token
        public static PSW_TOKENS CheckTokenAuthentication()
        {
            try
            {
                PSWEntities context = new PSWEntities();
                List<PSW_TOKENS> AllExpired = context.PSW_TOKENS.Where(m => m.PT_EXPIRY_DATETIME < DateTime.Now && m.PT_STATUS == true).ToList();
                if (AllExpired.Count > 0)
                {
                    foreach (var Each_Token in AllExpired)
                    {
                        Each_Token.PT_STATUS = false;
                        context.Entry(Each_Token).State = EntityState.Modified;
                    }
                    context.SaveChanges();
                }
                DateTime check10minutes = DateTime.Now.AddMinutes(5);
                PSW_TOKENS ValidToken = context.PSW_TOKENS.Where(m => m.PT_EXPIRY_DATETIME >= check10minutes && m.PT_STATUS == true).FirstOrDefault();
                if (ValidToken != null)
                {
                    return ValidToken;
                }
                else
                {
                    PSW_TOKENS newToken = SendTokenRequest();
                    if (newToken != null && newToken.PT_TOKEN != null)
                    {
                        PSW_TOKENS token = new PSW_TOKENS();
                        token.PT_ID = Convert.ToInt32(context.PSW_TOKENS.Max(m => (decimal?)m.PT_ID)) + 1;
                        token.PT_TOKEN = newToken.PT_TOKEN;
                        token.PT_GENERATED_DATETIME = DateTime.Now;
                        token.PT_EXPIRY_DATETIME = newToken.PT_EXPIRY_DATETIME;
                        token.PT_STATUS = true;
                        context.PSW_TOKENS.Add(token);
                        context.SaveChanges();
                        return token;
                    }
                    else
                    {
                        return new PSW_TOKENS();
                    }
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "CheckTokenAuthentication", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                throw ex;
            }
        }
        public static PSW_TOKENS SendTokenRequest()
        {
            message = "";
            try
            {
                message += "Sending Token Request." + DateTime.Now + Environment.NewLine;
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString();
                message += "URL " + BASE_URL + "/api/dealers/citi/ediSend/token/" + Environment.NewLine;
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                    HttpClient client = new HttpClient(clientHandler);
                    ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                    ServicePointManager.Expect100Continue = false;
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                    client.DefaultRequestHeaders.ExpectContinue = false;
                    client.BaseAddress = new Uri(BASE_URL + "/api/dealers/citi/ediSend/token/");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    var request = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress);
                    message += "Request Header Added  = application/x-www-form-urlencoded  " + Environment.NewLine; 
                    TokenPSWKeys token = new TokenPSWKeys();
                    token.client_id = System.Configuration.ConfigurationManager.AppSettings["CID"].ToString();
                    token.client_secret = System.Configuration.ConfigurationManager.AppSettings["CSECRET"].ToString();
                    token.grant_type = "client_credentials";
                    var keyValues = new List<KeyValuePair<string, string>>();
                    keyValues.Add(new KeyValuePair<string, string>("client_id", token.client_id));
                    keyValues.Add(new KeyValuePair<string, string>("client_secret", token.client_secret));
                    keyValues.Add(new KeyValuePair<string, string>("grant_type", token.grant_type));

                    request.Content = new FormUrlEncodedContent(keyValues);
                    message += "Request Sent Waiting For Response " + Environment.NewLine;
                    var respone = client.SendAsync(request).Result;
                    var contents = respone.Content.ReadAsStringAsync().Result;
                    message += "Response Recieved Below "+ Environment.NewLine;
                    message += "Response : " + contents + Environment.NewLine;
                    dynamic RequestData = System.Text.Json.JsonSerializer.Deserialize<TokenPSWResponse>(contents);
                    var accesstoken = RequestData.access_token;
                    if (accesstoken != null)
                    {
                        PSW_TOKENS GeneratedToken = new PSW_TOKENS();
                        GeneratedToken.PT_TOKEN = accesstoken;
                        int NumberOfSeconds = Convert.ToInt32(RequestData.expires_in);
                        GeneratedToken.PT_EXPIRY_DATETIME = DateTime.Now.AddSeconds(NumberOfSeconds);
                        return GeneratedToken;
                    }

                }

                return new PSW_TOKENS();
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendTokenRequest", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                message += "Exception Occurred While Sending Token Request : " + ex.Message + DateTime.Now + Environment.NewLine;
                throw ex;
            }
        }
        #endregion

        #region MethodIds
        public static ShareUpdatedAuthPayModes M1512(int CID)
        {
            PSWEntities context = new PSWEntities();
            PSW_CLIENT_INFO clientinfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == CID).FirstOrDefault();
            if (clientinfo != null)
            {
                PSW_ACCOUNT_STATUS AccountStatusInfo = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == clientinfo.C_ACCOUNT_STATUS_ID).FirstOrDefault();
                List<int?> assignPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == clientinfo.C_ID && m.APMC_STATUS == true).Select(m => m.APMC_PAY_MODE_ID).ToList();
                List<int?> AuthMentModesImport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 1 && x.APM_CODE != 310 && x.APM_CODE != 309).Select(u => u.APM_CODE).ToList();
                List<int?> AuthMentModesExport = context.PSW_AUTH_PAY_MODES.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 2).Select(u => u.APM_CODE).ToList();
                ShareUpdatedAuthPayModes RequestData = new ShareUpdatedAuthPayModes();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                RequestData.methodId = "1512";
                RequestData.data = new ShareUpdatedAuthPayModes.DetailData
                {
                    iban = clientinfo.C_IBAN_NO,
                    authorizedPaymentModesForImport =
                    AuthMentModesImport.ConvertAll<string>(x => x.ToString()),
                    authorizedPaymentModesForExport =
                    AuthMentModesExport.ConvertAll<string>(x => x.ToString()),
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                try
                {
                    List<PSW_ASSIGN_PAY_MODE_TO_CLIENT> AssignedPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_IS_SHARED == false).ToList();
                    foreach (PSW_ASSIGN_PAY_MODE_TO_CLIENT PayModes in AssignedPaymodes)
                    {
                        PayModes.APMC_IS_SHARED = true;
                        context.Entry(PayModes).State = EntityState.Modified;
                    }
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    DAL.LogException("DAL", "M1512", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                    message += "Exception Occurred While Updating Entries Shared " + ex.Message;
                }
                return RequestData;
            }
            else
            {
                return new ShareUpdatedAuthPayModes();
            }
        }
        public static ShareUpdatedTraderActiveInActive M1516(int CID)
        {
            PSWEntities context = new PSWEntities();
            PSW_CLIENT_INFO clientinfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == CID).FirstOrDefault();
            if (clientinfo != null)
            {
                string AccountStatus = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == clientinfo.C_ACCOUNT_STATUS_ID).FirstOrDefault().AS_CODE.Trim();
                ShareUpdatedTraderActiveInActive RequestData = new ShareUpdatedTraderActiveInActive();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                RequestData.methodId = "1516";
                RequestData.data = new ShareUpdatedTraderActiveInActive.DetailData
                {
                    iban = clientinfo.C_IBAN_NO,
                    accountStatus = AccountStatus
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
            {
                return new ShareUpdatedTraderActiveInActive();
            }
        }
        public static ShareNegativeListOfCountriesByAD M1554_or_M1557()
        {
            PSWEntities context = new PSWEntities();
            List<PSW_NEGATIVE_LIST_OF_COUNTRIES> CheckNegativeListOfCountiresImport = context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Where(m => m.NLOC_PROFILE_TYPE == 1 && m.NLOC_STATUS == true).ToList();
            List<PSW_NEGATIVE_LIST_OF_COUNTRIES> CheckNegativeListOfCountiresExport = context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Where(m => m.NLOC_PROFILE_TYPE == 2 && m.NLOC_STATUS == true).ToList();
            string MethodId = "";
            if (CheckNegativeListOfCountiresImport.Any(m => m.NLOC_IS_SHARED == true) || CheckNegativeListOfCountiresExport.Any(m => m.NLOC_IS_SHARED == true))
            {
                MethodId = "1557";
            }
            else
            {
                MethodId = "1554";
            }
            List<int?> NegativeListOfCountiresImport = CheckNegativeListOfCountiresImport.Select(m => m.NLOC_COUNTRY_ID).ToList();
            List<int?> NegativeListOfCountiresExport = CheckNegativeListOfCountiresExport.Select(m => m.NLOC_COUNTRY_ID).ToList();
            List<string> NegetiveListImport = context.PSW_LIST_OF_COUNTRIES.Where(x => NegativeListOfCountiresImport.Contains(x.LC_ID)).Select(u => u.LC_CODE).ToList();
            List<string> NegetiveListExport = context.PSW_LIST_OF_COUNTRIES.Where(x => NegativeListOfCountiresExport.Contains(x.LC_ID)).Select(u => u.LC_CODE).ToList();
            ShareNegativeListOfCountriesByAD RequestData = new ShareNegativeListOfCountriesByAD();
            RequestData.messageId = Guid.NewGuid().ToString();
            RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            RequestData.senderId = "CBN";
            RequestData.receiverId = "PSW";
            RequestData.methodId = MethodId;
            RequestData.data = new ShareNegativeListOfCountriesByAD.CountryDetailData
            {
                bankCode = "CBN",
                restrictedCountriesForImport =
                NegetiveListImport.ConvertAll<string>(x => x.ToString()),
                restrictedCountriesForExport =
                NegetiveListExport.ConvertAll<string>(x => x.ToString()),
            };
            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
            try
            {
                List<PSW_NEGATIVE_LIST_OF_COUNTRIES> ALLCont = context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Where(m => m.NLOC_IS_SHARED == false).ToList();
                foreach (PSW_NEGATIVE_LIST_OF_COUNTRIES Cont in ALLCont)
                {
                    Cont.NLOC_IS_SHARED = true;
                    context.Entry(Cont).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "M1554_or_M1557", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                message += "Exception Occurred While Updating Entries Shared " + ex.Message; 
            }
            return RequestData;
        }
        public static ShareNegativeListOfCommodityByAD M1555_or_M1558()
        {
            PSWEntities context = new PSWEntities();
            List<PSW_NEGATIVE_LIST_OF_COMMODITIES> CheckNegativeListOfCommoditiesImport = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_PROFILE_TYPE == 1 && m.NLCOM_STATUS == true).ToList();
            List<PSW_NEGATIVE_LIST_OF_COMMODITIES> CheckNegativeListOfCommoditiesExport = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_PROFILE_TYPE == 2 && m.NLCOM_STATUS == true).ToList();
            string MethodId = "";
            if (CheckNegativeListOfCommoditiesImport.Any(m => m.NLCOM_IS_SHARED == true) || CheckNegativeListOfCommoditiesExport.Any(m => m.NLCOM_IS_SHARED == true))
            {
                MethodId = "1558";
            }
            else
            {
                MethodId = "1555";
            }
            List<int?> NegativeListOfCommoditiesImport = CheckNegativeListOfCommoditiesImport.Select(m => m.NLCOM_LCOM_ID).ToList();
            List<int?> NegativeListOfCommoditiesExport = CheckNegativeListOfCommoditiesExport.Select(m => m.NLCOM_LCOM_ID).ToList();
            List<string> NegetiveListImport = context.PSW_LIST_OF_COMMODITIES.Where(x => NegativeListOfCommoditiesImport.Contains(x.LCOM_ID)).Select(u => u.LCOM_CODE).ToList();
            List<string> NegetiveListExport = context.PSW_LIST_OF_COMMODITIES.Where(x => NegativeListOfCommoditiesExport.Contains(x.LCOM_ID)).Select(u => u.LCOM_CODE).ToList();
            ShareNegativeListOfCommodityByAD RequestData = new ShareNegativeListOfCommodityByAD();
            RequestData.messageId = Guid.NewGuid().ToString();
            RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            RequestData.senderId = "CBN";
            RequestData.receiverId = "PSW";
            RequestData.methodId = MethodId;
            RequestData.data = new ShareNegativeListOfCommodityByAD.ShareNegativeDetailData
            {
                bankCode = "CBN",
                restrictedCommoditiesForImport =
                NegetiveListImport.ConvertAll<string>(x => x.ToString()),
                restrictedCommoditiesForExport =
                NegetiveListExport.ConvertAll<string>(x => x.ToString()),
            };
            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
            try
            {
                List<PSW_NEGATIVE_LIST_OF_COMMODITIES> ALLCommodities = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_IS_SHARED == false).ToList();
                foreach (PSW_NEGATIVE_LIST_OF_COMMODITIES Commo in ALLCommodities)
                {
                    Commo.NLCOM_IS_SHARED = true;
                    context.Entry(Commo).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "N1555_or_M1558", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                message += "Exception Occurred While Updating Entries Shared " + ex.Message;
            }
            return RequestData;

        }
        public static ShareNegativeListOfSupplierByAD  M1556_or_1559()
        {
            PSWEntities context = new PSWEntities();
            List<PSW_NEGATIVE_LIST_OF_SUPPLIERS> CheckNegativeListOfSuppliersImport = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_PROFILE_TYPE == 1 && m.NLOS_STATUS == true).ToList();
            List<PSW_NEGATIVE_LIST_OF_SUPPLIERS> CheckNegativeListOfSuppliersExport = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_PROFILE_TYPE == 2 && m.NLOS_STATUS == true).ToList();
            string MethodId = "";
            if (CheckNegativeListOfSuppliersImport.Any(m => m.NLOS_IS_SHARED == true) || CheckNegativeListOfSuppliersExport.Any(m => m.NLOS_IS_SHARED == true))
            {
                MethodId = "1559";
            }
            else
            {
                MethodId = "1556";
            }
            List<int?> NegativeListOfSuppliersImport = CheckNegativeListOfSuppliersImport.Select(m => m.NLOS_LOS_ID).ToList();
            List<int?> NegativeListOfSuppliersExport = CheckNegativeListOfSuppliersExport.Select(m => m.NLOS_LOS_ID).ToList();
            List<string> NegetiveListImport = context.PSW_LIST_OF_SUPPLIERS.Where(x => NegativeListOfSuppliersImport.Contains(x.LOS_ID)).Select(u => u.LOS_NAME).ToList();
            List<string> NegetiveListExport = context.PSW_LIST_OF_SUPPLIERS.Where(x => NegativeListOfSuppliersExport.Contains(x.LOS_ID)).Select(u => u.LOS_NAME).ToList();
            ShareNegativeListOfSupplierByAD RequestData = new ShareNegativeListOfSupplierByAD();
            RequestData.messageId = Guid.NewGuid().ToString();
            RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            RequestData.senderId = "CBN";
            RequestData.receiverId = "PSW";
            RequestData.methodId = MethodId;
            RequestData.data = new ShareNegativeListOfSupplierByAD.SupplierDetailData
            {
                bankCode = "CBN",
                restrictedSuppliersForImport =
                NegetiveListImport.ConvertAll<string>(x => x.ToString()),
                restrictedSuppliersForExport =
                NegetiveListExport.ConvertAll<string>(x => x.ToString()),
            };
            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
            try
            {
                List<PSW_NEGATIVE_LIST_OF_SUPPLIERS> AllSupp = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_IS_SHARED == false).ToList();
                foreach (PSW_NEGATIVE_LIST_OF_SUPPLIERS Sup in AllSupp)
                {
                    Sup.NLOS_IS_SHARED = true;
                    context.Entry(Sup).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "M1556_or_1559", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                message += "Exception Occurred While Updating Entries Shared " + ex.Message;
            }
            return RequestData;

        }
        public static ShareExportConsigneeList M1550_or_M1551(string FiNumber)
        {
            PSWEntities context = new PSWEntities();
            PSW_EEF_MASTER_DETAILS FIData = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == FiNumber).FirstOrDefault();
            List<PSW_EXPORT_CONSIGNEE_LIST> CheckAmend = context.PSW_EXPORT_CONSIGNEE_LIST.Where(m => m.ECL_FI_NUMBER == FiNumber && m.ECL_ISAUTH == true).ToList();
            List<string> ConsigneeNames = new List<string>();
            
            string MethodId = "";
            if (CheckAmend.Any(m => m.ECL_IS_SHARED == true))
            {
                MethodId = "1551";
            }
            else
            {
                MethodId = "1550";
            }
            foreach (PSW_EXPORT_CONSIGNEE_LIST item in CheckAmend)
            {
                ConsigneeNames.Add(item.ECL_CONSIGNEE_NAME);
            }
            ShareExportConsigneeList RequestData = new ShareExportConsigneeList();
            RequestData.messageId = Guid.NewGuid().ToString();
            RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            RequestData.senderId = "CBN";
            RequestData.receiverId = "PSW";
            RequestData.methodId = MethodId;
            RequestData.data = new ShareExportConsigneeList.ShareConsigneeName
            {
                ntn = FIData.FDI_NTN,
                iban = FIData.FDRI_CONSIGNEE_IBAN,
                finInsUniqueNumber = FIData.FDI_FORM_E_NO,
                consignee = ConsigneeNames,
            };
            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
            return RequestData;

        }

        #region M1520
        public static ShareFinancialNGDInfoLcData M1520_301(string EIF_REQ, int AM,int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_EIF_BASIC_INFO eif_basic = context.PSW_EIF_BASIC_INFO.Where(x => x.EIF_BI_EIF_REQUEST_NO == EIF_REQ && x.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eif_basic != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eif_basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eif_basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                PSW_PAYMENT_MODES_PARAMETERS PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == EIF_REQ && m.EIF_PMP_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_EIF_DOCUMENT_REQUEST_INFO DocInfo = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == EIF_REQ && m.EIF_DRI_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocInfo.EIF_DRI_CCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES BENE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES Exporter_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocInfo.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                PSW_EIF_BANK_INFO bankInfo = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == EIF_REQ && m.EIF_BA_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                List<ShareFinancialNGDInfoLcData.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoLcData.DetailItemInformation>();
                List<PSW_EIF_HS_CODE> EIF_HS_CODES = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == EIF_REQ && m.EIF_HC_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).ToList();
                foreach (PSW_EIF_HS_CODE item in EIF_HS_CODES)
                {
                    if (item.EIF_HC_IS_SAMPLE == "Y")
                    {
                        hscodes.Add(new ShareFinancialNGDInfoLcData.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "Y",
                            sampleValue = Convert.ToDecimal(item.EIF_HC_SAMPLE_VALUE == null ? 0 : item.EIF_HC_SAMPLE_VALUE),
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                    else
                    {
                        hscodes.Add(new ShareFinancialNGDInfoLcData.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "N",
                            sampleValue = item.EIF_HC_SAMPLE_VALUE,
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                }
                ShareFinancialNGDInfoLcData RequestData = new ShareFinancialNGDInfoLcData();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if(eif_basic.EIF_BI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1520";
                else
                    RequestData.methodId = "1549";
                RequestData.data = new ShareFinancialNGDInfoLcData.DetailData
                {
                    importerNtn = eif_basic.EIF_BI_NTN,
                    importerName = client.C_NAME,
                    importerIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eif_basic.EIF_BI_EIF_REQUEST_NO,
                    lcData = new ShareFinancialNGDInfoLcData.DetailLCData
                    {
                        advPayPercentage = (int)PayParam.EIF_PMP_ADV_PAYPERCENT,
                        sightPercentage = (int)PayParam.EIF_PMP_SIGHT_PERCENT,
                        usancePercentage = (int)PayParam.EIF_PMP_USANCE_PERCENT,
                        days = (int)PayParam.EIF_PMP_DAYS,
                        totalPercentage = (int)PayParam.EIF_PMP_PERCENTAGE
                    },
                    paymentInformation = new ShareFinancialNGDInfoLcData.DetailPaymentInformation
                    {
                        beneficiaryName = DocInfo.EIF_DRI_BENEFICIARY_NAME,
                        beneficiaryAddress = DocInfo.EIF_DRI_BENEFICIARY_ADDRESS,
                        beneficiaryCountry = BENE_COUNTRY.LC_CODE.Trim(),
                        beneficiaryIban = DocInfo.EIF_DRI_BENEFICIARY_IBAN == null ? null : DocInfo.EIF_DRI_BENEFICIARY_IBAN,
                        exporterName = DocInfo.EIF_DRI_EXPORTER_NAME,
                        exporterAddress = DocInfo.EIF_DRI_EXPORTER_ADDRESS,
                        exporterCountry = Exporter_COUNTRY.LC_CODE.Trim(),
                        portOfShipment = DocInfo.EIF_DRI_PORT_OF_SHIPMENT == null ? null : DocInfo.EIF_DRI_PORT_OF_SHIPMENT,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentValue = (decimal)DocInfo.EIF_DRI_TOTAL_INVOICE_VALUE,
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        exchangeRate = (decimal)DocInfo.EIF_DRI_EXCHANGE_RATE,
                        lcContractNo = DocInfo.EIF_DRI_LC_CONT_NO
                    },
                    itemInformation = hscodes,
                    financialTranInformation = new ShareFinancialNGDInfoLcData.DetailFinancialTranInformation
                    {
                        intendedPayDate = bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE).ToString("yyyyMMdd"),
                        transportDocDate = bankInfo.EIF_BA_TRANSPORT_DOC_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_TRANSPORT_DOC_DATE).ToString("yyyyMMdd"),
                        finalDateOfShipment = bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT).ToString("yyyyMMdd"),
                        expiryDate = Convert.ToDateTime(bankInfo.EIF_BA_EXPIRY_DATE).ToString("yyyyMMdd")
                    },
                    remarks = bankInfo.EIF_BA_REMARK
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoLcData();
        }
        public static ShareFinancialNGDInfoOpenAccount M1520_302(string EIF_REQ, int AM, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_EIF_BASIC_INFO eif_basic = context.PSW_EIF_BASIC_INFO.Where(x => x.EIF_BI_EIF_REQUEST_NO == EIF_REQ && x.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eif_basic != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eif_basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eif_basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                PSW_PAYMENT_MODES_PARAMETERS PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == EIF_REQ && m.EIF_PMP_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_EIF_DOCUMENT_REQUEST_INFO DocInfo = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == EIF_REQ && m.EIF_DRI_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocInfo.EIF_DRI_CCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES BENE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES Exporter_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocInfo.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                PSW_EIF_BANK_INFO bankInfo = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == EIF_REQ && m.EIF_BA_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                List<ShareFinancialNGDInfoOpenAccount.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoOpenAccount.DetailItemInformation>();
                List<PSW_EIF_HS_CODE> EIF_HS_CODES = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == EIF_REQ && m.EIF_HC_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).ToList();
                foreach (PSW_EIF_HS_CODE item in EIF_HS_CODES)
                {
                    if (item.EIF_HC_IS_SAMPLE == "Y")
                    {
                        hscodes.Add(new ShareFinancialNGDInfoOpenAccount.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "Y",
                            sampleValue = Convert.ToDecimal(item.EIF_HC_SAMPLE_VALUE == null ? 0 : item.EIF_HC_SAMPLE_VALUE),
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                    else
                    {
                        hscodes.Add(new ShareFinancialNGDInfoOpenAccount.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "N",
                            sampleValue = item.EIF_HC_SAMPLE_VALUE,
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                }
                ShareFinancialNGDInfoOpenAccount RequestData = new ShareFinancialNGDInfoOpenAccount();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if (eif_basic.EIF_BI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1520";
                else
                    RequestData.methodId = "1549";
                RequestData.data = new ShareFinancialNGDInfoOpenAccount.DetailData
                {
                    importerNtn = eif_basic.EIF_BI_NTN,
                    importerName = client.C_NAME,
                    importerIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eif_basic.EIF_BI_EIF_REQUEST_NO,
                    openAccountData = new ShareFinancialNGDInfoOpenAccount.DetailOpenAccountData
                    {
                        gdNumber = PayParam.EIF_PMP_GD_NO.Trim()
                    },
                    paymentInformation = new ShareFinancialNGDInfoOpenAccount.DetailPaymentInformation
                    {
                        beneficiaryName = DocInfo.EIF_DRI_BENEFICIARY_NAME,
                        beneficiaryAddress = DocInfo.EIF_DRI_BENEFICIARY_ADDRESS,
                        beneficiaryCountry = BENE_COUNTRY.LC_CODE.Trim(),
                        beneficiaryIban = DocInfo.EIF_DRI_BENEFICIARY_IBAN == null ? null : DocInfo.EIF_DRI_BENEFICIARY_IBAN,
                        exporterName = DocInfo.EIF_DRI_EXPORTER_NAME,
                        exporterAddress = DocInfo.EIF_DRI_EXPORTER_ADDRESS,
                        exporterCountry = Exporter_COUNTRY.LC_CODE.Trim(),
                        portOfShipment = DocInfo.EIF_DRI_PORT_OF_SHIPMENT == null ? null : DocInfo.EIF_DRI_PORT_OF_SHIPMENT,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentValue = (decimal)DocInfo.EIF_DRI_TOTAL_INVOICE_VALUE,
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        exchangeRate = (decimal)DocInfo.EIF_DRI_EXCHANGE_RATE,
                        lcContractNo = DocInfo.EIF_DRI_LC_CONT_NO
                    },
                    itemInformation = hscodes,
                    financialTranInformation = new ShareFinancialNGDInfoOpenAccount.DetailFinancialTranInformation
                    {
                        intendedPayDate = bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE).ToString("yyyyMMdd"),
                        transportDocDate = bankInfo.EIF_BA_TRANSPORT_DOC_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_TRANSPORT_DOC_DATE).ToString("yyyyMMdd"),
                        finalDateOfShipment = bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT).ToString("yyyyMMdd"),
                        expiryDate = Convert.ToDateTime(bankInfo.EIF_BA_EXPIRY_DATE).ToString("yyyyMMdd")
                    },
                    remarks = bankInfo.EIF_BA_REMARK
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoOpenAccount();
        }
        public static ShareFinancialNGDInfoAdnavcePayment M1520_303(string EIF_REQ, int AM, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_EIF_BASIC_INFO eif_basic = context.PSW_EIF_BASIC_INFO.Where(x => x.EIF_BI_EIF_REQUEST_NO == EIF_REQ && x.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eif_basic != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eif_basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eif_basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                PSW_PAYMENT_MODES_PARAMETERS PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == EIF_REQ && m.EIF_PMP_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_EIF_DOCUMENT_REQUEST_INFO DocInfo = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == EIF_REQ && m.EIF_DRI_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocInfo.EIF_DRI_CCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES BENE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES Exporter_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocInfo.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                PSW_EIF_BANK_INFO bankInfo = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == EIF_REQ && m.EIF_BA_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                List<ShareFinancialNGDInfoAdnavcePayment.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoAdnavcePayment.DetailItemInformation>();
                List<PSW_EIF_HS_CODE> EIF_HS_CODES = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == EIF_REQ && m.EIF_HC_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).ToList();
                foreach (PSW_EIF_HS_CODE item in EIF_HS_CODES)
                {
                    if (item.EIF_HC_IS_SAMPLE == "Y")
                    {
                        hscodes.Add(new ShareFinancialNGDInfoAdnavcePayment.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "Y",
                            sampleValue = Convert.ToDecimal(item.EIF_HC_SAMPLE_VALUE == null ? 0 : item.EIF_HC_SAMPLE_VALUE),
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                    else
                    {
                        hscodes.Add(new ShareFinancialNGDInfoAdnavcePayment.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "N",
                            sampleValue = item.EIF_HC_SAMPLE_VALUE,
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                }
                ShareFinancialNGDInfoAdnavcePayment RequestData = new ShareFinancialNGDInfoAdnavcePayment();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if (eif_basic.EIF_BI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1520";
                else
                    RequestData.methodId = "1549";
                RequestData.data = new ShareFinancialNGDInfoAdnavcePayment.DetailData
                {
                    importerNtn = eif_basic.EIF_BI_NTN,
                    importerName = client.C_NAME,
                    importerIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eif_basic.EIF_BI_EIF_REQUEST_NO,
                    paymentInformation = new ShareFinancialNGDInfoAdnavcePayment.DetailPaymentInformation
                    {
                        beneficiaryName = DocInfo.EIF_DRI_BENEFICIARY_NAME,
                        beneficiaryAddress = DocInfo.EIF_DRI_BENEFICIARY_ADDRESS,
                        beneficiaryCountry = BENE_COUNTRY.LC_CODE.Trim(),
                        beneficiaryIban = DocInfo.EIF_DRI_BENEFICIARY_IBAN == null ? null : DocInfo.EIF_DRI_BENEFICIARY_IBAN,
                        exporterName = DocInfo.EIF_DRI_EXPORTER_NAME,
                        exporterAddress = DocInfo.EIF_DRI_EXPORTER_ADDRESS,
                        exporterCountry = Exporter_COUNTRY.LC_CODE.Trim(),
                        portOfShipment = DocInfo.EIF_DRI_PORT_OF_SHIPMENT == null ? null : DocInfo.EIF_DRI_PORT_OF_SHIPMENT,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentValue = (decimal)DocInfo.EIF_DRI_TOTAL_INVOICE_VALUE,
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        exchangeRate = (decimal)DocInfo.EIF_DRI_EXCHANGE_RATE,
                        lcContractNo = DocInfo.EIF_DRI_LC_CONT_NO
                    },
                    itemInformation = hscodes,
                    financialTranInformation = new ShareFinancialNGDInfoAdnavcePayment.DetailFinancialTranInformation
                    {
                        intendedPayDate = bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE).ToString("yyyyMMdd"),
                        transportDocDate = bankInfo.EIF_BA_TRANSPORT_DOC_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_TRANSPORT_DOC_DATE).ToString("yyyyMMdd"),
                        finalDateOfShipment = bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT).ToString("yyyyMMdd"),
                        expiryDate = Convert.ToDateTime(bankInfo.EIF_BA_EXPIRY_DATE).ToString("yyyyMMdd")
                    },
                    remarks = bankInfo.EIF_BA_REMARK
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoAdnavcePayment();
        }
        public static ShareFinancialNGDInfoContColl M1520_304(string EIF_REQ, int AM, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_EIF_BASIC_INFO eif_basic = context.PSW_EIF_BASIC_INFO.Where(x => x.EIF_BI_EIF_REQUEST_NO == EIF_REQ && x.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eif_basic != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eif_basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eif_basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                PSW_PAYMENT_MODES_PARAMETERS PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == EIF_REQ && m.EIF_PMP_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_EIF_DOCUMENT_REQUEST_INFO DocInfo = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == EIF_REQ && m.EIF_DRI_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocInfo.EIF_DRI_CCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES BENE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES Exporter_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocInfo.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                PSW_EIF_BANK_INFO bankInfo = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == EIF_REQ && m.EIF_BA_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                List<ShareFinancialNGDInfoContColl.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoContColl.DetailItemInformation>();
                List<PSW_EIF_HS_CODE> EIF_HS_CODES = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == EIF_REQ && m.EIF_HC_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).ToList();
                foreach (PSW_EIF_HS_CODE item in EIF_HS_CODES)
                {
                    if (item.EIF_HC_IS_SAMPLE == "Y")
                    {
                        hscodes.Add(new ShareFinancialNGDInfoContColl.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "Y",
                            sampleValue = Convert.ToDecimal(item.EIF_HC_SAMPLE_VALUE == null ? 0 : item.EIF_HC_SAMPLE_VALUE),
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                    else
                    {
                        hscodes.Add(new ShareFinancialNGDInfoContColl.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "N",
                            sampleValue = item.EIF_HC_SAMPLE_VALUE,
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                }
                ShareFinancialNGDInfoContColl RequestData = new ShareFinancialNGDInfoContColl();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if (eif_basic.EIF_BI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1520";
                else
                    RequestData.methodId = "1549";
                RequestData.data = new ShareFinancialNGDInfoContColl.DetailData
                {
                    importerNtn = eif_basic.EIF_BI_NTN,
                    importerName = client.C_NAME,
                    importerIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eif_basic.EIF_BI_EIF_REQUEST_NO,
                    contractCollectionData = new ShareFinancialNGDInfoContColl.DetailContractCollectionData
                    {
                        advPayPercentage = (int)PayParam.EIF_PMP_ADV_PAYPERCENT,
                        docAgainstPayPercentage = (int)PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT,
                        docAgainstAcceptancePercentage = (int)PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT,
                        days = (int)PayParam.EIF_PMP_DAYS,
                        totalPercentage = (int)PayParam.EIF_PMP_PERCENTAGE
                    },
                    paymentInformation = new ShareFinancialNGDInfoContColl.DetailPaymentInformation
                    {
                        beneficiaryName = DocInfo.EIF_DRI_BENEFICIARY_NAME,
                        beneficiaryAddress = DocInfo.EIF_DRI_BENEFICIARY_ADDRESS,
                        beneficiaryCountry = BENE_COUNTRY.LC_CODE.Trim(),
                        beneficiaryIban = DocInfo.EIF_DRI_BENEFICIARY_IBAN == null ? null : DocInfo.EIF_DRI_BENEFICIARY_IBAN,
                        exporterName = DocInfo.EIF_DRI_EXPORTER_NAME,
                        exporterAddress = DocInfo.EIF_DRI_EXPORTER_ADDRESS,
                        exporterCountry = Exporter_COUNTRY.LC_CODE.Trim(),
                        portOfShipment = DocInfo.EIF_DRI_PORT_OF_SHIPMENT == null ? null : DocInfo.EIF_DRI_PORT_OF_SHIPMENT,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentValue = (decimal)DocInfo.EIF_DRI_TOTAL_INVOICE_VALUE,
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        exchangeRate = (decimal)DocInfo.EIF_DRI_EXCHANGE_RATE,
                        lcContractNo = DocInfo.EIF_DRI_LC_CONT_NO
                    },
                    itemInformation = hscodes,
                    financialTranInformation = new ShareFinancialNGDInfoContColl.DetailFinancialTranInformation
                    {
                        intendedPayDate = bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE).ToString("yyyyMMdd"),
                        transportDocDate = bankInfo.EIF_BA_TRANSPORT_DOC_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_TRANSPORT_DOC_DATE).ToString("yyyyMMdd"),
                        finalDateOfShipment = bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT).ToString("yyyyMMdd"),
                        expiryDate = Convert.ToDateTime(bankInfo.EIF_BA_EXPIRY_DATE).ToString("yyyyMMdd")
                    },
                    remarks = bankInfo.EIF_BA_REMARK
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoContColl();
        }
        public static ShareFinancialNGDInfoRemittanceNotInvoved M1520_309(string EIF_REQ, int AM, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_EIF_BASIC_INFO eif_basic = context.PSW_EIF_BASIC_INFO.Where(x => x.EIF_BI_EIF_REQUEST_NO == EIF_REQ && x.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eif_basic != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eif_basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eif_basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                PSW_PAYMENT_MODES_PARAMETERS PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == EIF_REQ && m.EIF_PMP_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_EIF_DOCUMENT_REQUEST_INFO DocInfo = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == EIF_REQ && m.EIF_DRI_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocInfo.EIF_DRI_CCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES BENE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES Exporter_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocInfo.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                PSW_EIF_BANK_INFO bankInfo = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == EIF_REQ && m.EIF_BA_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                List<ShareFinancialNGDInfoRemittanceNotInvoved.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoRemittanceNotInvoved.DetailItemInformation>();
                List<PSW_EIF_HS_CODE> EIF_HS_CODES = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == EIF_REQ && m.EIF_HC_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).ToList();
                foreach (PSW_EIF_HS_CODE item in EIF_HS_CODES)
                {
                    if (item.EIF_HC_IS_SAMPLE == "Y")
                    {
                        hscodes.Add(new ShareFinancialNGDInfoRemittanceNotInvoved.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "Y",
                            sampleValue = Convert.ToDecimal(item.EIF_HC_SAMPLE_VALUE == null ? 0 : item.EIF_HC_SAMPLE_VALUE),
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                    else
                    {
                        hscodes.Add(new ShareFinancialNGDInfoRemittanceNotInvoved.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "N",
                            sampleValue = item.EIF_HC_SAMPLE_VALUE,
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                }
                ShareFinancialNGDInfoRemittanceNotInvoved RequestData = new ShareFinancialNGDInfoRemittanceNotInvoved();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if (eif_basic.EIF_BI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1520";
                else
                    RequestData.methodId = "1549";
                RequestData.data = new ShareFinancialNGDInfoRemittanceNotInvoved.DetailData
                {
                    importerNtn = eif_basic.EIF_BI_NTN,
                    importerName = client.C_NAME,
                    importerIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eif_basic.EIF_BI_EIF_REQUEST_NO,
                    paymentInformation = new ShareFinancialNGDInfoRemittanceNotInvoved.DetailPaymentInformation
                    {
                        beneficiaryName = DocInfo.EIF_DRI_BENEFICIARY_NAME,
                        beneficiaryAddress = DocInfo.EIF_DRI_BENEFICIARY_ADDRESS,
                        beneficiaryCountry = BENE_COUNTRY.LC_CODE.Trim(),
                        beneficiaryIban = DocInfo.EIF_DRI_BENEFICIARY_IBAN == null ? null : DocInfo.EIF_DRI_BENEFICIARY_IBAN,
                        exporterName = DocInfo.EIF_DRI_EXPORTER_NAME,
                        exporterAddress = DocInfo.EIF_DRI_EXPORTER_ADDRESS,
                        exporterCountry = Exporter_COUNTRY.LC_CODE.Trim(),
                        portOfShipment = DocInfo.EIF_DRI_PORT_OF_SHIPMENT == null ? null : DocInfo.EIF_DRI_PORT_OF_SHIPMENT,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentValue = (decimal)DocInfo.EIF_DRI_TOTAL_INVOICE_VALUE,
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        exchangeRate = (decimal)DocInfo.EIF_DRI_EXCHANGE_RATE,
                        lcContractNo = DocInfo.EIF_DRI_LC_CONT_NO
                    },
                    itemInformation = hscodes,
                    financialTranInformation = new ShareFinancialNGDInfoRemittanceNotInvoved.DetailFinancialTranInformation
                    {
                        intendedPayDate = bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE).ToString("yyyyMMdd"),
                        transportDocDate = bankInfo.EIF_BA_TRANSPORT_DOC_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_TRANSPORT_DOC_DATE).ToString("yyyyMMdd"),
                        finalDateOfShipment = bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT).ToString("yyyyMMdd"),
                        expiryDate = Convert.ToDateTime(bankInfo.EIF_BA_EXPIRY_DATE).ToString("yyyyMMdd")
                    },
                    remarks = bankInfo.EIF_BA_REMARK
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoRemittanceNotInvoved();
        }
        public static ShareFinancialNGDInfoCashMargin M1520_310(string EIF_REQ, int AM, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_EIF_BASIC_INFO eif_basic = context.PSW_EIF_BASIC_INFO.Where(x => x.EIF_BI_EIF_REQUEST_NO == EIF_REQ && x.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eif_basic != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eif_basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eif_basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                PSW_PAYMENT_MODES_PARAMETERS PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == EIF_REQ && m.EIF_PMP_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_EIF_DOCUMENT_REQUEST_INFO DocInfo = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == EIF_REQ && m.EIF_DRI_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocInfo.EIF_DRI_CCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES BENE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_BENEFICIARY_COUNTRY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES Exporter_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocInfo.EIF_DRI_EXPORTER_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocInfo.EIF_DRI_DELIVERY_TERM).FirstOrDefault();
                PSW_EIF_BANK_INFO bankInfo = context.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == EIF_REQ && m.EIF_BA_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                List<ShareFinancialNGDInfoCashMargin.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoCashMargin.DetailItemInformation>();
                List<PSW_EIF_HS_CODE> EIF_HS_CODES = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == EIF_REQ && m.EIF_HC_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).ToList();
                foreach (PSW_EIF_HS_CODE item in EIF_HS_CODES)
                {
                    if (item.EIF_HC_IS_SAMPLE == "Y")
                    {
                        hscodes.Add(new ShareFinancialNGDInfoCashMargin.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "Y",
                            sampleValue = Convert.ToDecimal(item.EIF_HC_SAMPLE_VALUE == null ? 0 : item.EIF_HC_SAMPLE_VALUE),
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                    else
                    {
                        hscodes.Add(new ShareFinancialNGDInfoCashMargin.DetailItemInformation
                        {
                            hsCode = item.EIF_HC_CODE,
                            goodsDescription = item.EIF_HC_DESCRIPTION,
                            quantity = Convert.ToDecimal(item.EIF_HC_QUANTITY),
                            uom = item.EIF_HC_UOM,
                            countryOfOrigin = item.EIF_HC_ORGIN,
                            sample = "N",
                            sampleValue = item.EIF_HC_SAMPLE_VALUE,
                            itemInvoiceValue = item.EIF_HC_AMOUNT_AGAINST_HS_CODE != null ? Convert.ToDecimal(item.EIF_HC_AMOUNT_AGAINST_HS_CODE) : 0
                        });
                    }
                }
                ShareFinancialNGDInfoCashMargin RequestData = new ShareFinancialNGDInfoCashMargin();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if (eif_basic.EIF_BI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1520";
                else
                    RequestData.methodId = "1549";
                RequestData.data = new ShareFinancialNGDInfoCashMargin.DetailData
                {
                    importerNtn = eif_basic.EIF_BI_NTN,
                    importerName = client.C_NAME,
                    importerIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eif_basic.EIF_BI_EIF_REQUEST_NO,
                    cashMargin = new ShareFinancialNGDInfoCashMargin.DetailCashMargin
                    {
                        cashmarginPercentage = (int)PayParam.EIF_PMP_CASH_MARGIN_PERCENT,
                        cashmarginValue = (int)PayParam.EIF_PMP_CASH_MARGIN_VALUE
                    },
                    paymentInformation = new ShareFinancialNGDInfoCashMargin.DetailPaymentInformation
                    {
                        beneficiaryName = DocInfo.EIF_DRI_BENEFICIARY_NAME,
                        beneficiaryAddress = DocInfo.EIF_DRI_BENEFICIARY_ADDRESS,
                        beneficiaryCountry = BENE_COUNTRY.LC_CODE.Trim(),
                        beneficiaryIban = DocInfo.EIF_DRI_BENEFICIARY_IBAN == null ? null : DocInfo.EIF_DRI_BENEFICIARY_IBAN,
                        exporterName = DocInfo.EIF_DRI_EXPORTER_NAME,
                        exporterAddress = DocInfo.EIF_DRI_EXPORTER_ADDRESS,
                        exporterCountry = Exporter_COUNTRY.LC_CODE.Trim(),
                        portOfShipment = DocInfo.EIF_DRI_PORT_OF_SHIPMENT == null ? null : DocInfo.EIF_DRI_PORT_OF_SHIPMENT,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentValue = (decimal)DocInfo.EIF_DRI_TOTAL_INVOICE_VALUE,
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        exchangeRate = (decimal)DocInfo.EIF_DRI_EXCHANGE_RATE,
                        lcContractNo = DocInfo.EIF_DRI_LC_CONT_NO
                    },
                    itemInformation = hscodes,
                    financialTranInformation = new ShareFinancialNGDInfoCashMargin.DetailFinancialTranInformation
                    {
                        intendedPayDate = bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE).ToString("yyyyMMdd"),
                        transportDocDate = bankInfo.EIF_BA_TRANSPORT_DOC_DATE == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_TRANSPORT_DOC_DATE).ToString("yyyyMMdd"),
                        finalDateOfShipment = bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT == null ? null : Convert.ToDateTime(bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT).ToString("yyyyMMdd"),
                        expiryDate = Convert.ToDateTime(bankInfo.EIF_BA_EXPIRY_DATE).ToString("yyyyMMdd")
                    },
                    remarks = bankInfo.EIF_BA_REMARK
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoCashMargin();
        }
        #endregion

        public static ShareBDAInfoByAD M1522(int B, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_EIF_BDA bda = context.PSW_EIF_BDA.Where(x => x.EIF_BDA_ID == B).FirstOrDefault();
            if (bda != null)
            {
                PSW_EIF_MASTER_DETAILS basic = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == bda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                int BDAAmountCurrencyId = 0;
                int SampleAmountCurrencyId = 0;
                int NetBDAAmountCurrencyId = 0;
                int FCYCurrencyId = 0;

                bool BDAAmountIsValidCCY = int.TryParse(bda.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY, out BDAAmountCurrencyId);
                bool SampleAmountIsValidCCY = int.TryParse(bda.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY, out SampleAmountCurrencyId);
                bool NetBDAAmountIsValidCCY = int.TryParse(bda.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY, out NetBDAAmountCurrencyId);
                bool FCYIsValidCCY = int.TryParse(bda.EIF_BDA_CURRENCY_FCY, out FCYCurrencyId);

                if (BDAAmountIsValidCCY == true && SampleAmountIsValidCCY == true && NetBDAAmountIsValidCCY == true && FCYIsValidCCY == true)
                {
                    PSW_CURRENCY CCYBDAAMOUNT = context.PSW_CURRENCY.Where(m => m.CUR_ID == BDAAmountCurrencyId).FirstOrDefault();
                    PSW_CURRENCY CCYSampleAmount = context.PSW_CURRENCY.Where(m => m.CUR_ID == SampleAmountCurrencyId).FirstOrDefault();
                    PSW_CURRENCY CCYNETAMOUNT = context.PSW_CURRENCY.Where(m => m.CUR_ID == NetBDAAmountCurrencyId).FirstOrDefault();
                    PSW_CURRENCY CCYFCY = context.PSW_CURRENCY.Where(m => m.CUR_ID == FCYCurrencyId).FirstOrDefault();


                    if (basic != null)
                    {
                        PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == basic.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                        PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                        List<PSW_EIF_GD_GENERAL_INFO_LIST> GD_General_Info = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(m => m.EIGDGI_EIF_REQUEST_NO == bda.EIF_BDA_EIF_REQUEST_NO).ToList();
                        ShareBDAInfoByAD RequestData = new ShareBDAInfoByAD();
                        RequestData.messageId = Guid.NewGuid().ToString();
                        RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                        RequestData.senderId = "CBN";
                        RequestData.receiverId = "PSW";
                        //if (bda.EIF_BDA_AMENDMENT_NO == 0 || ASN == 1)
                        RequestData.methodId = "1522";
                        //else
                        //    RequestData.methodId = "1546";
                        RequestData.data = new ShareBDAInfoByAD.BDADetailData
                        {
                            bdaUniqueIdNumber = bda.EIF_BDA_NO,
                            gdNumber = bda.EIF_BDA_GD_NO == null ? null : bda.EIF_BDA_GD_NO,
                            iban = client.C_IBAN_NO,
                            importerNtn = basic.EIF_BI_NTN,
                            importerName = client.C_NAME,
                            bdaDate = Convert.ToDateTime(bda.EIF_BDA_ENTRY_DATETIME).ToString("yyyyMMdd"),
                            modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                            finInsUniqueNumber = basic.EIF_BI_EIF_REQUEST_NO,
                            bdaInformation = new ShareBDAInfoByAD.BDADetailData.DetailBDAInformation
                            {
                                totalBdaAmountFcy = bda.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY,
                                totalBdaAmountCurrency = CCYBDAAMOUNT.CUR_NAME.Trim(),
                                sampleAmountExclude = bda.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE,
                                sampleAmountCurrency = bda.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY == "0" ? null : CCYSampleAmount.CUR_NAME.Trim(),
                                netBdaAmountFcy = bda.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY,
                                netBdaAmountCurrency = CCYNETAMOUNT.CUR_NAME.Trim(),
                                exchangeRateFiFcy = bda.EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY,
                                netBdaAmountPkr = bda.EIF_BDA_NET_BDA_AMOUNT_PKR,
                                amountInWords = bda.EIF_BDA_AMOUNT_IN_WORDS,
                                currencyFcy = CCYFCY.CUR_NAME.Trim(),
                                exchangeRateFcy = bda.EIF_BDA_EXCHANGE_RATE_FCY,
                                bdaAmountFcy = bda.EIF_BDA_AMOUNT_FCY,
                                bdaAmountPkr = bda.EIF_BDA_AMOUNT_PKR,
                                bdaDocumentRefNumber = bda.EIF_BDA_DOCUMENT_REQUEST_NO,
                                commisionAmountFcy = bda.EIF_BDA_COMMMISSION_FCY,
                                commisionAmountPkr = bda.EIF_BDA_COMMISSION_PKR,
                                fedFcy = bda.EIF_BDA_FED_AMOUNT_FCY,
                                fedAmountPkr = bda.EIF_BDA_FED_AMOUNT_PKR,
                                swiftChargesPkr = bda.EIF_BDA_SWIFT_CHARGES_PKR,
                                otherChargesPkr = bda.EIF_BDA_OTHER_CHARGES_PKR,
                                remarks = bda.EIF_BDA_REMARKS,
                                balanceBdaAmountFcy = bda.EIF_BDA_Balance_Amount_FCY
                            }
                        };
                        RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                        return RequestData;
                    }
                    else
                        return new ShareBDAInfoByAD();
                }
                else
                    return new ShareBDAInfoByAD();
            }
            else
                return new ShareBDAInfoByAD();
        }
        public static ShareBCAInfoByAD M1526(int B, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_BCA_INFO bca = context.PSW_BCA_INFO.Where(x => x.BCAD_ID == B).FirstOrDefault();
            if (bca != null)
            {
                PSW_EEF_MASTER_DETAILS basic = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == bca.BCAD_FORM_E_NO).FirstOrDefault();
                PSW_CURRENCY CCY = context.PSW_CURRENCY.Where(m => m.CUR_ID == bca.BCAD_CURRENCY).FirstOrDefault();
                if (basic != null)
                {
                    string BCAEnventName = context.PSW_BCA_EVENT.Where(m => m.BCAE_ID == bca.BCAD_EVENTID).FirstOrDefault().BCAE_NAME.ToString().Trim();
                    PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == basic.FDI_MODE_OF_EXPORT_PAYMENT).FirstOrDefault();
                    PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == basic.FDI_TRADER_NAME).FirstOrDefault();
                    PSW_EIF_DOCUMENT_REQUEST_INFO DocInfo = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == bca.BCAD_FORM_E_NO).FirstOrDefault();
                    List<string> GD_Nos = context.PSW_GD_ASSIGN_EFORMS.Where(m => m.GDAEF_E_FORM == bca.BCAD_FORM_E_NO).Select(m => m.GDAEF_GD_NO).ToList();
                    ShareBCAInfoByAD RequestData = new ShareBCAInfoByAD();
                    RequestData.messageId = Guid.NewGuid().ToString();
                    RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    RequestData.senderId = "CBN";
                    RequestData.receiverId = "PSW";
                    //if (bca.BCAD_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1526";
                    //else
                    //    RequestData.methodId = "1547";
                    RequestData.data = new ShareBCAInfoByAD.BCADetailData
                    {
                        bcaUniqueIdNumber = bca.BCAD_BCA_NO,
                        iban = client.C_IBAN_NO,
                        gdNumber = bca.BCAD_GD_NO == null ? null : bca.BCAD_GD_NO,
                        exporterNtn = client.C_NTN_NO,
                        exporterName = client.C_NAME,
                        modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                        finInsUniqueNumber = basic.FDI_FORM_E_NO,
                        bcaInformation = new ShareBCAInfoByAD.BCADetailData.DetailBCAInformation
                        {
                            bcaEventName = BCAEnventName,
                            eventDate = Convert.ToDateTime(bca.BCAD_EVENT_DATE).ToString("yyyyMMdd"),
                            runningSerialNumber = bca.BCAD_RUNNING_SERIAL_DETAIL == null ? 0 : (int)bca.BCAD_RUNNING_SERIAL_DETAIL,
                            swiftReference = bca.BCAD_SWIFT_REFERENCES,
                            billNumber = bca.BCAD_BILL_NO == null ? null : bca.BCAD_BILL_NO,
                            billDated = Convert.ToDateTime(bca.BCAD_BILL_DATE).ToString("yyyyMMdd"),
                            billAmount = (decimal)bca.BCAD_BILL_AMOUNT,
                            invoiceNumber = bca.BCAD_INVOICE_NO,
                            invoiceDate = Convert.ToDateTime(bca.BCAD_INVOICE_DATE).ToString("yyyyMMdd"),
                            invoiceAmount = (decimal)bca.BCAD_INVOICE_AMOUNT
                        },
                        deductions = new ShareBCAInfoByAD.BCADetailData.DetailDeductions
                        {
                            foreignBankChargesFcy = (decimal)bca.BCAD_FORIEGN_BANK_CHARGES_FCY,
                            agentCommissionFcy = (decimal)bca.BCAD_AGENT_BROKE_COMMISSION_FCY,
                            withholdingTaxPkr = (decimal)bca.BCAD_WITH_HOLDING_TAX_PKR,
                            edsPkr = (decimal)bca.BCAD_EDS_PKR,
                            edsUniqueReferenceNumber = bca.BCAD_EDS_UNIQUE_BANK_REF_NO
                        },
                        netAmountRealized = new ShareBCAInfoByAD.BCADetailData.DetailNetAmountRealized
                        {
                            isFinInsCurrencyDiff = bca.BCAD_CURR_OF_REALIZATION == true ? "Y" : "N",
                            currency = CCY.CUR_NAME == null ? null : CCY.CUR_NAME.Trim(),
                            fcyExchangeRate = (decimal)bca.BCAD_FCY_EXCHANGE_RATE,
                            bcaPkr = (decimal)bca.BCAD_BCA_PKR,
                            bcaFc = (decimal)bca.BCAD_BCA_FC,
                            dateOfRealized = Convert.ToDateTime(bca.BCAD_DATE_OF_REALIZED).ToString("yyyyMMdd"),
                            adjustFromSpecialFcyAcc = Convert.ToDecimal(bca.BCAD_AFJUSTMENT_FROM_SPECIAL_FCY_AMOUNT),
                            isRemAmtSettledWithDiscount = bca.BCAD_FULL_AMOUNT_IS_NOT_REALIZED == true ? "Y" : "N",
                            allowedDiscount = (decimal)bca.BCAD_ALLOWED_DISCOUNT,
                            allowedDiscountPercentage = (int)bca.BCAD_ALLOWED_DISCOUNT_PERCENT,
                            totalBcaAmount = (decimal)bca.BCAD_INVOICE_AMOUNT,
                            amountRealized = (decimal)bca.BCAD_FORM_AMOUNT_REALIZED,
                            balance = (decimal)bca.BCAD_BALANCE
                        },
                        remarks = bca.BCAD_REMARKS
                    };
                    RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                    return RequestData;
                }
                else
                    return new ShareBCAInfoByAD();
            }
            else
                return new ShareBCAInfoByAD();
        }

        #region M1524
        public static ShareFinancialNGDInfoExportOpenAccount M1524_305(string EEF_REQ, int AM, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_FORM_E_DOCUMENT_INFO eef_doc_Info = context.PSW_FORM_E_DOCUMENT_INFO.Where(x => x.FDI_FORM_E_NO == EEF_REQ && x.FDI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eef_doc_Info != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eef_doc_Info.FDI_TRADER_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eef_doc_Info.FDI_MODE_OF_EXPORT_PAYMENT).FirstOrDefault();
                PSW_FORM_E_DOCUMENT_REQUEST_INFO DocRequestInfo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == EEF_REQ && m.FDRI_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocRequestInfo.FDRI_CURRENCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES CONSIGNEE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocRequestInfo.FDRI_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocRequestInfo.FDRI_DELIVERY_TERMS).FirstOrDefault();
                PSW_PAYMENT_MODES_PARAMETERS PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == eef_doc_Info.FDI_FORM_E_NO).FirstOrDefault();
                List<PSW_EEF_HS_CODE> EEF_HS_CODES = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == EEF_REQ && m.EEF_HC_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).ToList();
                List<ShareFinancialNGDInfoExportOpenAccount.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoExportOpenAccount.DetailItemInformation>();
                foreach (PSW_EEF_HS_CODE item in EEF_HS_CODES)
                {
                    hscodes.Add(new ShareFinancialNGDInfoExportOpenAccount.DetailItemInformation
                    {
                        hsCode = item.EEF_HC_CODE,
                        goodsDescription = item.EEF_HC_DESCRIPTION,
                        quantity = Convert.ToDecimal(item.EEF_HC_QUANTITY),
                        uom = item.EEF_HC_UOM,
                        itemInvoiceValue = ((decimal)(item.EEF_HC_INVOICE_VALUE)),
                        countryOfOrigin = item.EEF_HC_ORGIN
                    });
                }
                ShareFinancialNGDInfoExportOpenAccount RequestData = new ShareFinancialNGDInfoExportOpenAccount();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if (DocRequestInfo.FDRI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1524";
                else
                    RequestData.methodId = "1548";
                RequestData.data = new ShareFinancialNGDInfoExportOpenAccount.FinDetailData
                {
                    exporterNtn = client.C_NTN_NO,
                    exporterName = client.C_NAME,
                    exporterIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eef_doc_Info.FDI_FORM_E_NO,
                    openAccountData = new ShareFinancialNGDInfoExportOpenAccount.DetailOpenAccountData
                    {
                        gdNumber = PayParam.EIF_PMP_GD_NO.Trim()
                    },
                    paymentInformation = new ShareFinancialNGDInfoExportOpenAccount.DetailPaymentInformation()
                    {
                        consigneeName = DocRequestInfo.FDRI_CONSIG_NAME,
                        consigneeAddress = DocRequestInfo.FDRI_CONSID_ADDRESS,
                        consigneeCountry = CONSIGNEE_COUNTRY.LC_CODE.Trim(),
                        consigneeIban = DocRequestInfo.FDRI_CONSIGNEE_IBAN == null ? null : DocRequestInfo.FDRI_CONSIGNEE_IBAN,
                        portOfDischarge = DocRequestInfo.FDRI_PORT_OF_DISCHARGE,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        financialInstrumentValue = (decimal)DocRequestInfo.FDRI_TOTAL_VALUE_OF_SHIPMENT,
                        expiryDate = Convert.ToDateTime(DocRequestInfo.FDRI_FINAN_EXPIRY).ToString("yyyyMMdd")
                    },
                    itemInformation = hscodes
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoExportOpenAccount();
        }
        public static ShareFinancialNGDInfoExportAdvPayment M1524_306(string EEF_REQ, int AM,int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_FORM_E_DOCUMENT_INFO eef_doc_Info = context.PSW_FORM_E_DOCUMENT_INFO.Where(x => x.FDI_FORM_E_NO == EEF_REQ && x.FDI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eef_doc_Info != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eef_doc_Info.FDI_TRADER_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eef_doc_Info.FDI_MODE_OF_EXPORT_PAYMENT).FirstOrDefault();
                PSW_FORM_E_DOCUMENT_REQUEST_INFO DocRequestInfo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == EEF_REQ && m.FDRI_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocRequestInfo.FDRI_CURRENCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES CONSIGNEE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocRequestInfo.FDRI_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocRequestInfo.FDRI_DELIVERY_TERMS).FirstOrDefault();
                List<PSW_EEF_HS_CODE> EEF_HS_CODES = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == EEF_REQ && m.EEF_HC_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).ToList();
                List<ShareFinancialNGDInfoExportAdvPayment.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoExportAdvPayment.DetailItemInformation>();
                foreach (PSW_EEF_HS_CODE item in EEF_HS_CODES)
                {
                    hscodes.Add(new ShareFinancialNGDInfoExportAdvPayment.DetailItemInformation
                    {
                        hsCode = item.EEF_HC_CODE,
                        goodsDescription = item.EEF_HC_DESCRIPTION,
                        quantity = Convert.ToDecimal(item.EEF_HC_QUANTITY),
                        uom = item.EEF_HC_UOM,
                        itemInvoiceValue = ((decimal)(item.EEF_HC_INVOICE_VALUE)),
                        countryOfOrigin = item.EEF_HC_ORGIN
                    });
                }
                ShareFinancialNGDInfoExportAdvPayment RequestData = new ShareFinancialNGDInfoExportAdvPayment();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if (DocRequestInfo.FDRI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1524";
                else
                    RequestData.methodId = "1548";
                RequestData.data = new ShareFinancialNGDInfoExportAdvPayment.FinDetailData
                {
                    exporterNtn = client.C_NTN_NO,
                    exporterName = client.C_NAME,
                    exporterIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eef_doc_Info.FDI_FORM_E_NO,
                    paymentInformation = new ShareFinancialNGDInfoExportAdvPayment.DetailPaymentInformation()
                    {
                        consigneeName = DocRequestInfo.FDRI_CONSIG_NAME,
                        consigneeAddress = DocRequestInfo.FDRI_CONSID_ADDRESS,
                        consigneeCountry = CONSIGNEE_COUNTRY.LC_CODE.Trim(),
                        consigneeIban = DocRequestInfo.FDRI_CONSIGNEE_IBAN == null ? null : DocRequestInfo.FDRI_CONSIGNEE_IBAN,
                        portOfDischarge = DocRequestInfo.FDRI_PORT_OF_DISCHARGE,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        financialInstrumentValue = (decimal)DocRequestInfo.FDRI_TOTAL_VALUE_OF_SHIPMENT,
                        expiryDate = Convert.ToDateTime(DocRequestInfo.FDRI_FINAN_EXPIRY).ToString("yyyyMMdd")
                    },
                    itemInformation = hscodes
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoExportAdvPayment();
        }
        public static ShareFinancialNGDInfoExportLC M1524_307(string EEF_REQ, int AM, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_FORM_E_DOCUMENT_INFO eef_doc_Info = context.PSW_FORM_E_DOCUMENT_INFO.Where(x => x.FDI_FORM_E_NO == EEF_REQ && x.FDI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eef_doc_Info != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eef_doc_Info.FDI_TRADER_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eef_doc_Info.FDI_MODE_OF_EXPORT_PAYMENT).FirstOrDefault();
                PSW_PAYMENT_MODES_PARAMETERS PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == EEF_REQ && m.EIF_PMP_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).FirstOrDefault();
                PSW_FORM_E_DOCUMENT_REQUEST_INFO DocRequestInfo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == EEF_REQ && m.FDRI_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocRequestInfo.FDRI_CURRENCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES CONSIGNEE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocRequestInfo.FDRI_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocRequestInfo.FDRI_DELIVERY_TERMS).FirstOrDefault();
                List<PSW_EEF_HS_CODE> EEF_HS_CODES = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == EEF_REQ && m.EEF_HC_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).ToList();
                List<ShareFinancialNGDInfoExportLC.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoExportLC.DetailItemInformation>();
                foreach (PSW_EEF_HS_CODE item in EEF_HS_CODES)
                {
                    hscodes.Add(new ShareFinancialNGDInfoExportLC.DetailItemInformation
                    {
                        hsCode = item.EEF_HC_CODE,
                        goodsDescription = item.EEF_HC_DESCRIPTION,
                        quantity = Convert.ToDecimal(item.EEF_HC_QUANTITY),
                        uom = item.EEF_HC_UOM,
                        itemInvoiceValue = ((decimal)(item.EEF_HC_INVOICE_VALUE)),
                        countryOfOrigin = item.EEF_HC_ORGIN
                    });
                }
                ShareFinancialNGDInfoExportLC RequestData = new ShareFinancialNGDInfoExportLC();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if (DocRequestInfo.FDRI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1524";
                else
                    RequestData.methodId = "1548";
                RequestData.data = new ShareFinancialNGDInfoExportLC.FinDetailData
                {
                    exporterNtn = client.C_NTN_NO,
                    exporterName = client.C_NAME,
                    exporterIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eef_doc_Info.FDI_FORM_E_NO,
                    lcData = new ShareFinancialNGDInfoExportLC.DetailLcData
                    {
                        advPayPercentage = (int)PayParam.EIF_PMP_ADV_PAYPERCENT,
                        sightPercentage = (int)PayParam.EIF_PMP_SIGHT_PERCENT,
                        usancePercentage = (int)PayParam.EIF_PMP_USANCE_PERCENT,
                        days = (int)PayParam.EIF_PMP_DAYS,
                        totalPercentage = (int)PayParam.EIF_PMP_PERCENTAGE
                    },
                    paymentInformation = new ShareFinancialNGDInfoExportLC.DetailPaymentInformation()
                    {
                        consigneeName = DocRequestInfo.FDRI_CONSIG_NAME,
                        consigneeAddress = DocRequestInfo.FDRI_CONSID_ADDRESS,
                        consigneeCountry = CONSIGNEE_COUNTRY.LC_CODE.Trim(),
                        consigneeIban = DocRequestInfo.FDRI_CONSIGNEE_IBAN == null ? null : DocRequestInfo.FDRI_CONSIGNEE_IBAN,
                        portOfDischarge = DocRequestInfo.FDRI_PORT_OF_DISCHARGE,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        financialInstrumentValue = (decimal)DocRequestInfo.FDRI_TOTAL_VALUE_OF_SHIPMENT,
                        expiryDate = Convert.ToDateTime(DocRequestInfo.FDRI_FINAN_EXPIRY).ToString("yyyyMMdd")
                    },
                    itemInformation = hscodes
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoExportLC();
        }
        public static ShareFinancialNGDInfoExportContColl M1524_308(string EEF_REQ, int AM, int ASN)
        {
            PSWEntities context = new PSWEntities();
            PSW_FORM_E_DOCUMENT_INFO eef_doc_Info = context.PSW_FORM_E_DOCUMENT_INFO.Where(x => x.FDI_FORM_E_NO == EEF_REQ && x.FDI_AMENDMENT_NO == AM).FirstOrDefault();
            if (eef_doc_Info != null)
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eef_doc_Info.FDI_TRADER_NAME).FirstOrDefault();
                PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eef_doc_Info.FDI_MODE_OF_EXPORT_PAYMENT).FirstOrDefault();
                PSW_PAYMENT_MODES_PARAMETERS PayParam = context.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == EEF_REQ && m.EIF_PMP_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).FirstOrDefault();
                PSW_FORM_E_DOCUMENT_REQUEST_INFO DocRequestInfo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == EEF_REQ && m.FDRI_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).FirstOrDefault();
                PSW_CURRENCY ccy = context.PSW_CURRENCY.Where(m => m.CUR_ID == DocRequestInfo.FDRI_CURRENCY).FirstOrDefault();
                PSW_LIST_OF_COUNTRIES CONSIGNEE_COUNTRY = context.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_ID == DocRequestInfo.FDRI_COUNTRY).FirstOrDefault();
                PSW_DELIVERY_TERMS DelTerms = context.PSW_DELIVERY_TERMS.Where(m => m.DT_ID == DocRequestInfo.FDRI_DELIVERY_TERMS).FirstOrDefault();
                List<PSW_EEF_HS_CODE> EEF_HS_CODES = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == EEF_REQ && m.EEF_HC_AMENDMENT_NO == eef_doc_Info.FDI_AMENDMENT_NO).ToList();
                List<ShareFinancialNGDInfoExportContColl.DetailItemInformation> hscodes = new List<ShareFinancialNGDInfoExportContColl.DetailItemInformation>();
                foreach (PSW_EEF_HS_CODE item in EEF_HS_CODES)
                {
                    hscodes.Add(new ShareFinancialNGDInfoExportContColl.DetailItemInformation
                    {
                        hsCode = item.EEF_HC_CODE,
                        goodsDescription = item.EEF_HC_DESCRIPTION,
                        quantity = Convert.ToDecimal(item.EEF_HC_QUANTITY),
                        uom = item.EEF_HC_UOM,
                        itemInvoiceValue = ((decimal)(item.EEF_HC_INVOICE_VALUE)),
                        countryOfOrigin = item.EEF_HC_ORGIN
                    });
                }
                ShareFinancialNGDInfoExportContColl RequestData = new ShareFinancialNGDInfoExportContColl();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                if (DocRequestInfo.FDRI_AMENDMENT_NO == 0 || ASN == 1)
                    RequestData.methodId = "1524";
                else
                    RequestData.methodId = "1548";
                RequestData.data = new ShareFinancialNGDInfoExportContColl.FinDetailData
                {
                    exporterNtn = client.C_NTN_NO,
                    exporterName = client.C_NAME,
                    exporterIban = client.C_IBAN_NO,
                    modeOfPayment = paymentMode.APM_CODE.ToString().Trim(),
                    finInsUniqueNumber = eef_doc_Info.FDI_FORM_E_NO,
                    contractCollectionData = new ShareFinancialNGDInfoExportContColl.DetailContractCollectionData
                    {
                        advPayPercentage = (int)PayParam.EIF_PMP_ADV_PAYPERCENT,
                        docAgainstPayPercentage = (int)PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT,
                        docAgainstAcceptancePercentage = (int)PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT,
                        days = (int)PayParam.EIF_PMP_DAYS,
                        totalPercentage = (int)PayParam.EIF_PMP_PERCENTAGE
                    },
                    paymentInformation = new ShareFinancialNGDInfoExportContColl.DetailPaymentInformation()
                    {
                        consigneeName = DocRequestInfo.FDRI_CONSIG_NAME,
                        consigneeAddress = DocRequestInfo.FDRI_CONSID_ADDRESS,
                        consigneeCountry = CONSIGNEE_COUNTRY.LC_CODE.Trim(),
                        consigneeIban = DocRequestInfo.FDRI_CONSIGNEE_IBAN == null ? null : DocRequestInfo.FDRI_CONSIGNEE_IBAN,
                        portOfDischarge = DocRequestInfo.FDRI_PORT_OF_DISCHARGE,
                        deliveryTerms = DelTerms.DT_CODE.Trim(),
                        financialInstrumentCurrency = ccy.CUR_NAME.Trim(),
                        financialInstrumentValue = (decimal)DocRequestInfo.FDRI_TOTAL_VALUE_OF_SHIPMENT,
                        expiryDate = Convert.ToDateTime(DocRequestInfo.FDRI_FINAN_EXPIRY).ToString("yyyyMMdd")
                    },
                    itemInformation = hscodes
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareFinancialNGDInfoExportContColl();
        }

        #endregion
        public static CancelOfFinancialTransByAD M1535(string UniqueNumber,bool Import)
        {
            PSWEntities context = new PSWEntities();
            if (Import)
            {
                PSW_EIF_MASTER_DETAILS eif_basic = context.PSW_EIF_MASTER_DETAILS.Where(x => x.EIF_BI_EIF_REQUEST_NO == UniqueNumber).FirstOrDefault();
                if (eif_basic != null)
                {
                    PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eif_basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                    CancelOfFinancialTransByAD RequestData = new CancelOfFinancialTransByAD();
                    RequestData.messageId = Guid.NewGuid().ToString();
                    RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    RequestData.senderId = "CBN";
                    RequestData.receiverId = "PSW";
                    RequestData.methodId = "1535";
                    RequestData.data = new CancelOfFinancialTransByAD.CancelDetailData
                    {
                        tradeTranType = "01",
                        traderNTN = client.C_NTN_NO,
                        traderName = client.C_NAME,
                        iban = client.C_IBAN_NO,
                        finInsUniqueNumber = eif_basic.EIF_BI_EIF_REQUEST_NO
                    };
                    RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                    return RequestData;
                }
                else
                    return new CancelOfFinancialTransByAD();
            }
            else
            {
                PSW_EEF_MASTER_DETAILS eef_basic = context.PSW_EEF_MASTER_DETAILS.Where(x => x.FDI_FORM_E_NO == UniqueNumber).FirstOrDefault();
                if (eef_basic != null)
                {
                    PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eef_basic.FDI_TRADER_NAME).FirstOrDefault();
                    CancelOfFinancialTransByAD RequestData = new CancelOfFinancialTransByAD();
                    RequestData.messageId = Guid.NewGuid().ToString();
                    RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    RequestData.senderId = "CBN";
                    RequestData.receiverId = "PSW";
                    RequestData.methodId = "1535";
                    RequestData.data = new CancelOfFinancialTransByAD.CancelDetailData
                    {
                        tradeTranType = "02",
                        traderNTN = client.C_NTN_NO,
                        traderName = client.C_NAME,
                        iban = client.C_IBAN_NO,
                        finInsUniqueNumber = eef_basic.FDI_FORM_E_NO
                    };
                    RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                    return RequestData;
                }
                else
                    return new CancelOfFinancialTransByAD();
            }
        }

        public static ReversalOfBdaBcaByAD M1536(int B,bool IsImport)
        {
            if (B != 0)
            {
                PSWEntities context = new PSWEntities();
                if (IsImport)
                {
                    PSW_EIF_BDA bda = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_ID == B).FirstOrDefault();
                    if (bda != null)
                    {
                        PSW_EIF_MASTER_DETAILS basic = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == bda.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                        if (basic != null)
                        {
                            PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                            ReversalOfBdaBcaByAD RequestData = new ReversalOfBdaBcaByAD();
                            RequestData.messageId = Guid.NewGuid().ToString();
                            RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                            RequestData.senderId = "CBN";
                            RequestData.receiverId = "PSW";
                            RequestData.methodId = "1536";
                            RequestData.data = new ReversalOfBdaBcaByAD.ReverseDetailData
                            {
                                tradeTranType = "01",
                                traderNTN = client.C_NTN_NO,
                                traderName = client.C_NAME,
                                iban = client.C_IBAN_NO,
                                finInsUniqueNumber = bda.EIF_BDA_EIF_REQUEST_NO,
                                bcaBdaUniqueIdNumber = bda.EIF_BDA_NO
                            };
                            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                            return RequestData;
                        }
                        else
                            return new ReversalOfBdaBcaByAD();
                    }
                    else
                        return new ReversalOfBdaBcaByAD();
                }
                else
                {
                    PSW_BCA_INFO bca = context.PSW_BCA_INFO.Where(m => m.BCAD_ID == B).FirstOrDefault();
                    if (bca != null)
                    {
                        PSW_EEF_MASTER_DETAILS basic = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == bca.BCAD_FORM_E_NO).FirstOrDefault();
                        if (basic != null)
                        {
                            PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == basic.FDI_TRADER_NAME).FirstOrDefault();
                            ReversalOfBdaBcaByAD RequestData = new ReversalOfBdaBcaByAD();
                            RequestData.messageId = Guid.NewGuid().ToString();
                            RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                            RequestData.senderId = "CBN";
                            RequestData.receiverId = "PSW";
                            RequestData.methodId = "1536";
                            RequestData.data = new ReversalOfBdaBcaByAD.ReverseDetailData
                            {
                                tradeTranType = "02",
                                traderNTN = client.C_NTN_NO,
                                traderName = client.C_NAME,
                                iban = client.C_IBAN_NO,
                                finInsUniqueNumber = bca.BCAD_FORM_E_NO,
                                bcaBdaUniqueIdNumber = bca.BCAD_BCA_NO
                            };
                            RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                            return RequestData;
                        }
                        else
                            return new ReversalOfBdaBcaByAD();
                    }
                    else
                        return new ReversalOfBdaBcaByAD();
                }
            }
            else
                return new ReversalOfBdaBcaByAD();
        }

        public static SettlementOfFinInstrumentByAD M1537(string UniqueNumber, bool Import)
        {
            PSWEntities context = new PSWEntities();
            if (Import)
            {
                PSW_EIF_MASTER_DETAILS eif_basic = context.PSW_EIF_MASTER_DETAILS.Where(x => x.EIF_BI_EIF_REQUEST_NO == UniqueNumber).FirstOrDefault();
                PSW_EIF_DOCUMENT_REQUEST_INFO DocInfo = context.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == UniqueNumber && m.EIF_DRI_AMENDMENT_NO == eif_basic.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                if (eif_basic != null && DocInfo != null)
                {
                    PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eif_basic.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
                    List<string> BDAUniqueNumbers = context.PSW_EIF_BDA_LIST.Where(x => x.EIF_BDA_EIF_REQUEST_NO == eif_basic.EIF_BI_EIF_REQUEST_NO && x.EIF_BDA_STATUS == "APPROVED").Select(u => u.EIF_BDA_NO).ToList();
                    decimal? ToTalBDASharedvalue = context.PSW_EIF_BDA_LIST.Where(m => m.EIF_BDA_EIF_REQUEST_NO == eif_basic.EIF_BI_EIF_REQUEST_NO && m.EIF_BDA_STATUS == "APPROVED").Sum(m => m.EIF_BDA_AMOUNT_FCY);
                    ToTalBDASharedvalue = ToTalBDASharedvalue == null ? 0 : ToTalBDASharedvalue;
                    decimal BalanceRemaining = (((decimal)DocInfo.EIF_DRI_TOTAL_INVOICE_VALUE) - (decimal)ToTalBDASharedvalue);
                    SettlementOfFinInstrumentByAD RequestData = new SettlementOfFinInstrumentByAD();
                    RequestData.messageId = Guid.NewGuid().ToString();
                    RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    RequestData.senderId = "CBN";
                    RequestData.receiverId = "PSW";
                    RequestData.methodId = "1537";
                    RequestData.data = new SettlementOfFinInstrumentByAD.SettleDetailData
                    {
                        tradeTranType = "01",
                        traderNTN = client.C_NTN_NO,
                        traderName = client.C_NAME,
                        finInsUniqueNumber = eif_basic.EIF_BI_EIF_REQUEST_NO,
                        bcaBdaUniqueIdNumber = BDAUniqueNumbers,
                        finInsValue = (decimal)DocInfo.EIF_DRI_TOTAL_INVOICE_VALUE,
                        totalValueOfSharedBCABDA = (decimal)ToTalBDASharedvalue,
                        balance = BalanceRemaining
                    };
                    RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                    return RequestData;
                }
                else
                    return new SettlementOfFinInstrumentByAD();
            }
            else
            {
                PSW_EEF_MASTER_DETAILS eef_basic = context.PSW_EEF_MASTER_DETAILS.Where(x => x.FDI_FORM_E_NO == UniqueNumber).FirstOrDefault();
                PSW_FORM_E_DOCUMENT_REQUEST_INFO DocRequestInfo = context.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == UniqueNumber && m.FDRI_AMENDMENT_NO == eef_basic.FDRI_AMENDMENT_NO).FirstOrDefault();
                if (eef_basic != null && DocRequestInfo != null)
                {
                    PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == eef_basic.FDI_TRADER_NAME).FirstOrDefault();
                    List<string> BCAUniqueNumbers = context.PSW_EEF_BCA_LIST.Where(x => x.BCAD_FORM_E_NO == eef_basic.FDI_FORM_E_NO && x.BCAD_STATUS == "APPROVED").Select(u => u.BCAD_BCA_NO).ToList();
                    decimal? ToTalBCASharedvalue = context.PSW_EEF_BCA_LIST.Where(x => x.BCAD_FORM_E_NO == eef_basic.FDI_FORM_E_NO && x.BCAD_STATUS == "APPROVED").Sum(m => m.BCAD_INVOICE_AMOUNT);
                    ToTalBCASharedvalue = ToTalBCASharedvalue == null ? 0 : ToTalBCASharedvalue;
                    decimal BalanceRemaining = (((decimal)DocRequestInfo.FDRI_TOTAL_VALUE_OF_SHIPMENT) - (decimal)ToTalBCASharedvalue);
                    SettlementOfFinInstrumentByAD RequestData = new SettlementOfFinInstrumentByAD();
                    RequestData.messageId = Guid.NewGuid().ToString();
                    RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    RequestData.senderId = "CBN";
                    RequestData.receiverId = "PSW";
                    RequestData.methodId = "1537";
                    RequestData.data = new SettlementOfFinInstrumentByAD.SettleDetailData
                    {
                        tradeTranType = "02",
                        traderNTN = client.C_NTN_NO,
                        traderName = client.C_NAME,
                        finInsUniqueNumber = eef_basic.FDI_FORM_E_NO,
                        bcaBdaUniqueIdNumber = BCAUniqueNumbers,
                        finInsValue = (decimal)DocRequestInfo.FDRI_TOTAL_VALUE_OF_SHIPMENT,
                        totalValueOfSharedBCABDA = (decimal)ToTalBCASharedvalue,
                        balance = BalanceRemaining
                    };
                    RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                    return RequestData;
                }
                else
                    return new SettlementOfFinInstrumentByAD();
            }
        }

        public static ShareGDClearanceMessageByAD M1541(string UniqueNumber,string GdStatus, bool Import)
        {
            PSWEntities context = new PSWEntities();
            if (Import)
            {
                PSW_EIF_GD_GENERAL_INFO_LIST gdGeneral = context.PSW_EIF_GD_GENERAL_INFO_LIST.Where(x => x.EIGDGI_GD_NO == UniqueNumber).FirstOrDefault();
                if (gdGeneral != null)
                {
                    int ClientId = 0;
                    PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == gdGeneral.EIGDGI_IMPORTER_IBAN).FirstOrDefault();
                    if (client != null)
                    {
                        ShareGDClearanceMessageByAD RequestData = new ShareGDClearanceMessageByAD();
                        RequestData.messageId = Guid.NewGuid().ToString();
                        RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                        RequestData.senderId = "CBN";
                        RequestData.receiverId = "PSW";
                        RequestData.methodId = "1541";
                        RequestData.data = new ShareGDClearanceMessageByAD.ClearanceDetailData
                        {
                            tradeTranType = "01",
                            traderNTN = client.C_NTN_NO,
                            traderName = client.C_NAME,
                            iban = client.C_IBAN_NO,
                            gdNumber = gdGeneral.EIGDGI_GD_NO,
                            clearanceStatus = GdStatus
                        };
                        RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                        return RequestData;
                    }
                    else
                        return new ShareGDClearanceMessageByAD();
                }
                else
                    return new ShareGDClearanceMessageByAD();
            }
            else
            {
                PSW_EEF_GD_GENERAL_INFO_LIST gdGeneral = context.PSW_EEF_GD_GENERAL_INFO_LIST.Where(x => x.EFGDGI_GD_NO == UniqueNumber).FirstOrDefault();
                if (gdGeneral != null)
                {
                    int ClientId = 0;
                    PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(x => x.C_IBAN_NO == gdGeneral.EFGDGI_EXPORTER_IBAN).FirstOrDefault();
                    ShareGDClearanceMessageByAD RequestData = new ShareGDClearanceMessageByAD();
                    RequestData.messageId = Guid.NewGuid().ToString();
                    RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    RequestData.senderId = "CBN";
                    RequestData.receiverId = "PSW";
                    RequestData.methodId = "1541";
                    RequestData.data = new ShareGDClearanceMessageByAD.ClearanceDetailData
                    {
                        tradeTranType = "02",
                        traderNTN = client.C_NTN_NO,
                        traderName = client.C_NAME,
                        iban = client.C_IBAN_NO,
                        gdNumber = gdGeneral.EFGDGI_GD_NO,
                        clearanceStatus = GdStatus
                    };
                    RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                    return RequestData;
                }
                else
                    return new ShareGDClearanceMessageByAD();
            }
        }

        public static ShareChangeOfBankRequestApprovalRejectionByADReponse M1538_or_M1539(string UniqueNumber, string COBStatus)
        {
            PSWEntities context = new PSWEntities();
            PSW_CHANGE_OF_BANK_REQUEST COBReq = context.PSW_CHANGE_OF_BANK_REQUEST.Where(x => x.COB_UNIQUE_NO == UniqueNumber).FirstOrDefault();
            PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == COBReq.COB_IBAN).FirstOrDefault();
            if (COBReq != null && client != null)
            {
                ShareChangeOfBankRequestApprovalRejectionByADReponse RequestData = new ShareChangeOfBankRequestApprovalRejectionByADReponse();
                RequestData.messageId = Guid.NewGuid().ToString();
                RequestData.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                RequestData.senderId = "CBN";
                RequestData.receiverId = "PSW";
                RequestData.methodId = COBReq.METHODID.ToString();
                RequestData.data = new ShareChangeOfBankRequestApprovalRejectionByADReponse.DetailData
                {
                    cobUniqueIdNumber = COBReq.COB_UNIQUE_NO,
                    tradeTranType = COBReq.COB_TRANS_TYPE.Trim(),
                    iban = client.C_IBAN_NO,
                    traderNTN = client.C_NTN_NO,
                    traderName = client.C_NAME,
                    finInsUniqueNumber = COBReq.COB_REQUEST_NO,
                    cobStatus = COBStatus
                };
                RequestData.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(RequestData));
                return RequestData;
            }
            else
                return new ShareChangeOfBankRequestApprovalRejectionByADReponse();
            
        }

        #endregion

        #region AddMessageToDB
        public static PSW_MESSAGES ConvertMessageToEntity(dynamic Data, string Response)
        {
            try
            {
                PSWEntities context = new PSWEntities();
                string TSP = "";
                PSW_MESSAGES NewMessage = new PSW_MESSAGES();
                if (Data == null)
                {
                    TSP = DateTime.Now.ToString("yyyyMMddHHmmss");
                }

                NewMessage.messageId = Guid.NewGuid().ToString();
                NewMessage.timestamp = Data == null ? Convert.ToInt64(TSP) : Convert.ToInt64(Data.timestamp);
                NewMessage.senderId = Data == null ? "DBK" : Data.senderId;
                NewMessage.receiverId = Data == null ? "PSW" : Data.receiverId;

                NewMessage.processingCode = Data == null ? null : Convert.ToInt32(Data.methodId);
                NewMessage.data = message;
                NewMessage.signature = Data == null ? null : Data.signature;
                NewMessage.response = Response;
                if (NewMessage.senderId == "CBN")
                {
                    bool validjson = false;
                    try
                    {
                        JObject.Parse(Response);
                        validjson = true;
                    }
                    catch (Exception)
                    {
                        validjson = false;
                    }
                    if (validjson)
                    {
                        dynamic ResponseData = JObject.Parse(Response);
                        NewMessage.code = ResponseData.message.code;
                        NewMessage.message = ResponseData.message.description;
                    }
                }
                context.PSW_MESSAGES.Add(NewMessage);
                context.SaveChanges();
                return NewMessage;
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "ConvertMessageToEntity", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                throw ex;
            }
        }
        #endregion

        #region MethodId1512
        public static HttpResponseMessage SendRequestMethodId1512(int Cid)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    ShareUpdatedAuthPayModes RequestData = M1512(Cid);
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1512", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }
        public static string MethodId1512(int Cid)
        {
            HttpResponseMessage response = SendRequestMethodId1512(Cid);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }

        }
        #endregion

        #region MethodId1554_or_1557
        public static HttpResponseMessage SendRequestMethodId1554_or_1557()
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    ShareNegativeListOfCountriesByAD RequestData = M1554_or_M1557();
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1554_or_1557", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }
        public static string MethodId1554_or_1557()
        {
            HttpResponseMessage response = SendRequestMethodId1554_or_1557();
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1555_or_M1558
        public static HttpResponseMessage SendRequestMethodId1555_or_M1558()
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    ShareNegativeListOfCommodityByAD RequestData = M1555_or_M1558();
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1555_or_1558", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }

        public static string MethodId1555_or_M1558()
        {
            HttpResponseMessage response = SendRequestMethodId1555_or_M1558();
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1556_or_1559
        public static HttpResponseMessage SendRequestMethodId1556_or_1559()
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    ShareNegativeListOfSupplierByAD RequestData = M1556_or_1559();
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1556_or_1559", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }

        public static string MethodId1556_or_1559()
        {
            HttpResponseMessage response = SendRequestMethodId1556_or_1559();
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1516
        public static HttpResponseMessage SendRequestMethodId1516(int Cid)
        {
            message = "";
            try
            {
                /// If PROD 
                    string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    ShareUpdatedTraderActiveInActive RequestData = M1516(Cid);
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1516", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }

        public static string MethodId1516(int Cid)
        {
            HttpResponseMessage response = SendRequestMethodId1516(Cid);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1520
        public static HttpResponseMessage SendRequestMethodId1520(string FinInsNo,int AM,int ASN)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSWEntities context = new PSWEntities();
                PSW_EIF_BASIC_INFO eif = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == FinInsNo && m.EIF_BI_AMENDMENT_NO == AM).FirstOrDefault();
                if (eif != null)
                {
                    PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eif.EIF_BI_MODE_OF_IMPORT_PAYMENT).FirstOrDefault();
                    if (paymentMode != null)
                    {
                        PSW_TOKENS token = CheckTokenAuthentication();
                        if (token != null && token.PT_TOKEN != null)
                        {
                            switch (paymentMode.APM_CODE.ToString().Trim())
                            {
                                case "301":
                                    ShareFinancialNGDInfoLcData RequestData301 = M1520_301(eif.EIF_BI_EIF_REQUEST_NO, AM,ASN);
                                    LogCurrentRequest = RequestData301;
                                    if (RequestData301 != null)
                                    {
                                        using (HttpClientHandler clientHandler301 = new HttpClientHandler())
                                        {
                                            clientHandler301.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client301 = new HttpClient(clientHandler301))
                                            {
                                                client301.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req301 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req301.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req301.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData301,
                                                    new JsonSerializerOptions() { WriteIndented = false});
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req301.Content = content;
                                                    req301.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client301.SendAsync(req301).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EIF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                case "302":
                                    ShareFinancialNGDInfoOpenAccount RequestData302 = M1520_302(eif.EIF_BI_EIF_REQUEST_NO,AM,ASN);
                                    LogCurrentRequest = RequestData302;
                                    if (RequestData302 != null)
                                    {
                                        using (HttpClientHandler clientHandler302 = new HttpClientHandler())
                                        {
                                            clientHandler302.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client302 = new HttpClient(clientHandler302))
                                            {
                                                client302.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req302 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req302.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req302.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData302,
                                                    new JsonSerializerOptions() { WriteIndented = false});
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req302.Content = content;
                                                    req302.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client302.SendAsync(req302).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EIF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                case "303":
                                    ShareFinancialNGDInfoAdnavcePayment RequestData303 = M1520_303(eif.EIF_BI_EIF_REQUEST_NO,AM,ASN);
                                    LogCurrentRequest = RequestData303;
                                    if (RequestData303 != null)
                                    {
                                        using (HttpClientHandler clientHandler303 = new HttpClientHandler())
                                        {
                                            clientHandler303.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client303 = new HttpClient(clientHandler303))
                                            {
                                                client303.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req303 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req303.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req303.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData303,
                                                    new JsonSerializerOptions() { WriteIndented = false});
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req303.Content = content;
                                                    req303.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client303.SendAsync(req303).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EIF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                case "304":
                                    ShareFinancialNGDInfoContColl RequestData304 = M1520_304(eif.EIF_BI_EIF_REQUEST_NO,AM,ASN);
                                    LogCurrentRequest = RequestData304;
                                    if (RequestData304 != null)
                                    {
                                        using (HttpClientHandler clientHandler304 = new HttpClientHandler())
                                        {
                                            clientHandler304.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client304 = new HttpClient(clientHandler304))
                                            {
                                                client304.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req304 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req304.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req304.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData304,
                                                    new JsonSerializerOptions() { WriteIndented = false});
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req304.Content = content;
                                                    req304.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client304.SendAsync(req304).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EIF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                case "309":
                                    ShareFinancialNGDInfoRemittanceNotInvoved RequestData309 = M1520_309(eif.EIF_BI_EIF_REQUEST_NO,AM,ASN);
                                    LogCurrentRequest = RequestData309;
                                    if (RequestData309 != null)
                                    {
                                        using (HttpClientHandler clientHandler309 = new HttpClientHandler())
                                        {
                                            clientHandler309.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client309 = new HttpClient(clientHandler309))
                                            {
                                                client309.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req309 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req309.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req309.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData309,
                                                    new JsonSerializerOptions() { WriteIndented = false });
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req309.Content = content;
                                                    req309.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client309.SendAsync(req309).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EIF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                case "310":
                                    ShareFinancialNGDInfoCashMargin RequestData310 = M1520_310(eif.EIF_BI_EIF_REQUEST_NO,AM,ASN);
                                    LogCurrentRequest = RequestData310;
                                    if (RequestData310 != null)
                                    {
                                        using (HttpClientHandler clientHandler310 = new HttpClientHandler())
                                        {
                                            clientHandler310.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client310 = new HttpClient(clientHandler310))
                                            {
                                                client310.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req310.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData310,
                                                    new JsonSerializerOptions() { WriteIndented = false });
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req310.Content = content;
                                                    req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client310.SendAsync(req310).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EIF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                default:
                                    {
                                        string message = "Exception : Invalid Payment Mode in The Given Request." + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                            }
                        }
                        else
                        {
                            string message = "Exception : Problem while Fetching Token .";
                            return CreateCustomResponse("500", message);
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while Fetching Code Of Selected Payment Mode. " + FinInsNo;
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Data For EIF Req No : " + FinInsNo;
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1520", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }

        public static string MethodId1520(string FinInsNo, int AM,int ASN)
        {
            HttpResponseMessage response = SendRequestMethodId1520(FinInsNo, AM,ASN);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
            //return "Response Recieved : { Status : " + response.StatusCode + " Content : " + Content + "  Reason Pharase " + response.ReasonPhrase + "  }    response Raw : { " + response.ToString() + " }. ";
        }
        #endregion

        #region MethodId1522
        public static HttpResponseMessage SendRequestMethodId1522(int B, int ASN)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    PSWEntities context = new PSWEntities();
                    PSW_EIF_BDA bda = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_ID == B).FirstOrDefault();
                    if (bda != null)
                    {
                        ShareBDAInfoByAD RequestData304 = M1522(bda.EIF_BDA_ID, ASN);
                        LogCurrentRequest = RequestData304;
                        if (RequestData304 != null)
                        {
                            using (HttpClientHandler clientHandler = new HttpClientHandler())
                            {
                                clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                                using (HttpClient client = new HttpClient(clientHandler))
                                {
                                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                    {
                                        req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                        req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData304);
                                        message += dataAsJson + Environment.NewLine;
                                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                        req.Content = content;
                                        req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                        HttpResponseMessage responsse = client.SendAsync(req).Result;
                                        return responsse;
                                    }
                                }
                            }
                        }
                        else
                        {
                            string message = "Exception : Problem while creating data for this client";
                            return CreateCustomResponse("500", message);
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while Fetching Data For EIF Req No : " + B;
                        return CreateCustomResponse("500", message);
                    }

                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethod1552", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }

        public static string MethodId1522(int B,int ASN)
        {
            HttpResponseMessage response = SendRequestMethodId1522(B,ASN);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1524
        public static HttpResponseMessage SendRequestMethodId1524(string FinInsNo, int AM, int ASN)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSWEntities context = new PSWEntities();
                PSW_FORM_E_DOCUMENT_INFO eef = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == FinInsNo && m.FDI_AMENDMENT_NO == AM).FirstOrDefault();
                if (eef != null)
                {
                    PSW_TOKENS token = CheckTokenAuthentication();
                    if (token != null && token.PT_TOKEN != null)
                    {
                        PSW_AUTH_PAY_MODES paymentMode = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_ID == eef.FDI_MODE_OF_EXPORT_PAYMENT).FirstOrDefault();
                        if (paymentMode != null)
                        {

                            switch (paymentMode.APM_CODE.ToString().Trim())
                            {
                                case "305":
                                    ShareFinancialNGDInfoExportOpenAccount RequestData305 = M1524_305(eef.FDI_FORM_E_NO, AM,ASN);
                                    LogCurrentRequest = RequestData305;
                                    if (RequestData305 != null)
                                    {
                                        using (HttpClientHandler clientHandler305 = new HttpClientHandler())
                                        {
                                            clientHandler305.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client305 = new HttpClient(clientHandler305))
                                            {
                                                client305.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req305 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req305.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req305.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData305);
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req305.Content = content;
                                                    req305.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client305.SendAsync(req305).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EIF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                case "306":
                                    ShareFinancialNGDInfoExportAdvPayment RequestData306 = M1524_306(eef.FDI_FORM_E_NO, AM,ASN);
                                    LogCurrentRequest = RequestData306;
                                    if (RequestData306 != null)
                                    {
                                        using (HttpClientHandler clientHandler306 = new HttpClientHandler())
                                        {
                                            clientHandler306.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client306 = new HttpClient(clientHandler306))
                                            {
                                                client306.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req306 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req306.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req306.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData306);
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req306.Content = content;
                                                    req306.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client306.SendAsync(req306).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EIF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                case "307":
                                    ShareFinancialNGDInfoExportLC RequestData307 = M1524_307(eef.FDI_FORM_E_NO,AM,ASN);
                                    LogCurrentRequest = RequestData307;
                                    if (RequestData307 != null)
                                    {
                                        using (HttpClientHandler clientHandler307 = new HttpClientHandler())
                                        {
                                            clientHandler307.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client307 = new HttpClient(clientHandler307))
                                            {
                                                client307.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req307 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req307.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req307.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData307);
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req307.Content = content;
                                                    req307.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client307.SendAsync(req307).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EEF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                case "308":
                                    ShareFinancialNGDInfoExportContColl RequestData308 = M1524_308(eef.FDI_FORM_E_NO,AM,ASN);
                                    LogCurrentRequest = RequestData308;
                                    if (RequestData308 != null)
                                    {
                                        using (HttpClientHandler clientHandler308 = new HttpClientHandler())
                                        {
                                            clientHandler308.ServerCertificateCustomValidationCallback = delegate { return true; };
                                            using (HttpClient client308 = new HttpClient(clientHandler308))
                                            {
                                                client308.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                using (HttpRequestMessage req308 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                                {
                                                    req308.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                                    req308.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData308);
                                                    message += dataAsJson + Environment.NewLine;
                                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                                    req308.Content = content;
                                                    req308.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                                    HttpResponseMessage responsse = client308.SendAsync(req308).Result;
                                                    return responsse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string message = "Exception : Problem while creating data for the EEF Request # " + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                                default:
                                    {
                                        string message = "Exception : Invalid Payment Mode in The Given Request." + FinInsNo;
                                        return CreateCustomResponse("500", message);
                                    }
                            }
                        }
                        else
                        {
                            string message = "Exception : Problem while Fetching Code Of Selected Payment Mode. " + FinInsNo;
                            return CreateCustomResponse("500", message);
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while Fetching Token .";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Data For EEF Req No : " + FinInsNo;
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1524", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }
        public static string MethodId1524(string FinInsNo, int AM,int ASN)
        {
            HttpResponseMessage response = SendRequestMethodId1524(FinInsNo, AM,ASN);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }

        #endregion

        #region MethodId1535
        public static HttpResponseMessage SendRequestMethodId1535(string UniqueNo,bool IsImport)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    CancelOfFinancialTransByAD RequestData = M1535(UniqueNo,IsImport);
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1535", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }
        public static string MethodId1535(string UniqueNo, bool IsImport)
        {
            HttpResponseMessage response = SendRequestMethodId1535(UniqueNo,IsImport);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1536
        public static HttpResponseMessage SendRequestMethodId1536(int B, bool IsImport)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    ReversalOfBdaBcaByAD RequestData = M1536(B, IsImport);
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1536", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }
        public static string MethodId1536(int B, bool IsImport)
        {
            HttpResponseMessage response = SendRequestMethodId1536(B, IsImport);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1537
        public static HttpResponseMessage SendRequestMethodId1537(string UniqueNo, bool IsImport)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    SettlementOfFinInstrumentByAD RequestData = M1537(UniqueNo, IsImport);
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1537", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }
        public static string MethodId1537(string UniqueNo, bool IsImport)
        {
            HttpResponseMessage response = SendRequestMethodId1537(UniqueNo, IsImport);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1541
        public static HttpResponseMessage SendRequestMethodId1541(string UniqueNo, string GdStatus, bool IsImport)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    ShareGDClearanceMessageByAD RequestData = M1541(UniqueNo, GdStatus, IsImport);
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1541", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }
        public static string MethodId1541(string UniqueNo, string GdStatus ,bool IsImport)
        {
            try
            {
                HttpResponseMessage response = SendRequestMethodId1541(UniqueNo, GdStatus, IsImport);
                CurrentResponse = response;
                int StatusCode = (int)response.StatusCode;
                string StatusDesc = response.StatusCode.ToString();
                if (StatusCode == 200)
                {
                    string ResponseData = response.Content.ReadAsStringAsync().Result;
                    PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                    StatusCode = Convert.ToInt32(DbMessage.code);
                    StatusDesc = DbMessage.message;
                    return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
                }
                else
                {
                    PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                    return "Code : " + StatusCode + " | Description : " + StatusDesc;
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "MethodId1541", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occured while creating message" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return message;
            }
        }
        #endregion

        #region MethodId1526
        public static HttpResponseMessage SendRequestMethodId1526(int B,int ASN)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    PSWEntities context = new PSWEntities();
                    PSW_BCA_INFO bca = context.PSW_BCA_INFO.Where(m => m.BCAD_ID == B).FirstOrDefault();
                    if (bca != null)
                    {
                        ShareBCAInfoByAD RequestData304 = M1526(bca.BCAD_ID,ASN);
                        LogCurrentRequest = RequestData304;
                        if (RequestData304 != null)
                        {
                            using (HttpClientHandler clientHandler = new HttpClientHandler())
                            {
                                clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                                using (HttpClient client = new HttpClient(clientHandler))
                                {
                                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                    {
                                        req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                        req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData304);
                                        message += dataAsJson + Environment.NewLine;
                                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                        req.Content = content;
                                        req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                        HttpResponseMessage responsse = client.SendAsync(req).Result;
                                        return responsse;
                                    }
                                }
                            }
                        }
                        else
                        {
                            string message = "Exception : Problem while creating data for this client";
                            return CreateCustomResponse("500", message);
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while Fetching Data For EIF Req No : " + B;
                        return CreateCustomResponse("500", message);
                    }

                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1526", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }

        public static string MethodId1526(int B, int ASN)
        {
            HttpResponseMessage response = SendRequestMethodId1526(B,ASN);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1538
        public static HttpResponseMessage SendRequestMethodId1538(string COBUniqueNum, string COBStatus)
        {
            message = "";
            try
            {
                PSWEntities contt = new PSWEntities();
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    PSW_CHANGE_OF_BANK_REQUEST COBReq = contt.PSW_CHANGE_OF_BANK_REQUEST.Where(x => x.COB_UNIQUE_NO == COBUniqueNum).FirstOrDefault();
                    PSW_CLIENT_MASTER_DETAIL clientInfo = contt.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == COBReq.COB_IBAN).FirstOrDefault();
                    if (clientInfo != null)
                    {
                        if (COBReq.COB_TRANS_TYPE == "01")
                        {
                            PSW_EIF_BASIC_INFO eifbasic = contt.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == COBReq.COB_REQUEST_NO).FirstOrDefault();
                            if (eifbasic != null)
                            {
                                if (COBStatus == "209")
                                {
                                    eifbasic.EIF_BI_APPROVAL_STATUS = "BANK CHANGED.";
                                    contt.Entry(eifbasic).State = EntityState.Modified;
                                    int rowcount = contt.SaveChanges();
                                    if (rowcount == 0)
                                    {
                                        string message = "Error While Updating the FI Status.";
                                        return CreateCustomResponse("500", message);
                                    }
                                }
                            }
                            else
                            {
                                string message = "No FI Exist in the system with unique no # " + COBReq.COB_REQUEST_NO + ".";
                                return CreateCustomResponse("500", message);
                            }
                        }
                        else if (COBReq.COB_TRANS_TYPE == "02")
                        {
                            PSW_FORM_E_DOCUMENT_INFO eefdoc = contt.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == COBReq.COB_REQUEST_NO).FirstOrDefault();
                            if (eefdoc != null)
                            {
                                if (COBStatus == "209")
                                {
                                    eefdoc.FDI_STATUS = "BANK CHANGED.";
                                    eefdoc.FDI_STATUS_DATE = DateTime.Now;
                                    contt.Entry(eefdoc).State = EntityState.Modified;
                                    int rowcount = contt.SaveChanges();
                                    if (rowcount == 0)
                                    {
                                        string message = "Error While Updating the FI Status.";
                                        return CreateCustomResponse("500", message);
                                    }
                                }
                            }
                            else
                            {
                                string message = "No FI Exist in the system with unique no # " + COBReq.COB_REQUEST_NO + ".";
                                return CreateCustomResponse("500", message);
                            }
                        }
                        ShareChangeOfBankRequestApprovalRejectionByADReponse RequestData = M1538_or_M1539(COBUniqueNum, COBStatus);
                        LogCurrentRequest = RequestData;
                        if (RequestData != null)
                        {
                            using (HttpClientHandler clientHandler = new HttpClientHandler())
                            {
                                clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                                using (HttpClient client = new HttpClient(clientHandler))
                                {
                                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                    {
                                        req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                        req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                        message += dataAsJson + Environment.NewLine;
                                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                        req.Content = content;
                                        req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                        HttpResponseMessage responsse = client.SendAsync(req).Result;
                                        return responsse;
                                    }
                                }
                            }
                        }
                        else
                        {
                            string message = "Exception : Problem while creating data for this client";
                            return CreateCustomResponse("500", message);
                        }
                    }
                    else
                    {
                        string message = "No Client Profile Exist on the given iban # " + COBReq.COB_IBAN;
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1538", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }
        public static HttpResponseMessage CreateCustomResponse(string code, string desc)
        {
            CustomResponseFromPSW data = new CustomResponseFromPSW();
            data.message = new CustomResponseFromPSW.DetailMessage
            {
                code = code,
                description = desc
            };
            string DataAsJson =  Newtonsoft.Json.JsonConvert.SerializeObject(data);
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK, Content = new StringContent(DataAsJson) };
            //return Request.CreateResponse(HttpStatusCode.OK, data);
        }
        public static string MethodId1538(string UniqueNo, string COBStatus)
        {
            HttpResponseMessage response = SendRequestMethodId1538(UniqueNo, COBStatus);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        #region MethodId1550_or_1551
        public static HttpResponseMessage SendRequestMethodId1550_or_M1551(string FiNumber)
        {
            message = "";
            try
            {
                /// If PROD 
                string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/ediSend/";
                //// if DEV
                //string BASE_URL = System.Configuration.ConfigurationManager.AppSettings["PSWEndPointDomain"].ToString() + "/api/dealers/citi/edi/";
                /////
                PSW_TOKENS token = CheckTokenAuthentication();
                if (token != null && token.PT_TOKEN != null)
                {
                    ShareExportConsigneeList RequestData = M1550_or_M1551(FiNumber);
                    LogCurrentRequest = RequestData;
                    if (RequestData != null)
                    {
                        using (HttpClientHandler clientHandler = new HttpClientHandler())
                        {
                            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
                            using (HttpClient client = new HttpClient(clientHandler))
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                                {
                                    req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.PT_TOKEN);
                                    string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                    message += dataAsJson + Environment.NewLine;
                                    var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                    req.Content = content;
                                    req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                    HttpResponseMessage responsse = client.SendAsync(req).Result;
                                    return responsse;
                                }
                            }
                        }
                    }
                    else
                    {
                        string message = "Exception : Problem while creating data for this client";
                        return CreateCustomResponse("500", message);
                    }
                }
                else
                {
                    string message = "Exception : Problem while Fetching Token .";
                    return CreateCustomResponse("500", message);
                }
            }
            catch (Exception ex)
            {
                DAL.LogException("DAL", "SendRequestMethodId1550_or_M1551", System.Web.HttpContext.Current.Session["USER_ID"].ToString(), ex);
                string message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
                return CreateCustomResponse("500", message);
            }
        }

        public static string MethodId1550_or_1551(string FiNumber)
        {
            HttpResponseMessage response = SendRequestMethodId1550_or_M1551(FiNumber);
            CurrentResponse = response;
            int StatusCode = (int)response.StatusCode;
            string StatusDesc = response.StatusCode.ToString();
            if (StatusCode == 200)
            {
                string ResponseData = response.Content.ReadAsStringAsync().Result;
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, ResponseData);
                StatusCode = Convert.ToInt32(DbMessage.code);
                StatusDesc = DbMessage.message;
                return "Code : " + StatusCode + " | Description : " + StatusDesc + Environment.NewLine;
            }
            else
            {
                PSW_MESSAGES DbMessage = ConvertMessageToEntity(LogCurrentRequest, StatusDesc);
                return "Code : " + StatusCode + " | Description : " + StatusDesc;
            }
        }
        #endregion

        public static string LogException(string controllername, string functionName, string userId, Exception excep)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "PSW_Logs\\";
                string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
                bool bool_pswlogs = false; bool bool_icorelogs_rps_logs = false;
                string complete_path = path + filename;
                if (System.IO.Directory.Exists(path))
                {
                    bool_pswlogs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(path);
                    bool_pswlogs = true;
                }

                if (bool_pswlogs)
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(complete_path, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                    {
                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fs))
                        {
                            writer.WriteLine("---------------------------------------------------------------");
                            writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                            writer.WriteLine("Class/Form/Report Name: " + controllername);
                            writer.WriteLine("Function Name: " + functionName);
                            writer.WriteLine("User ID: " + userId);
                            if (excep.InnerException != null)
                            {
                                writer.WriteLine("Inner Exception: " + excep.InnerException);
                                if (excep.InnerException.Message != null)
                                    writer.WriteLine("      Inner Exception Message : " + excep.InnerException.Message);

                                if (excep.InnerException.InnerException != null)
                                {
                                    writer.WriteLine("Inner Inner Exception: " + excep.InnerException.InnerException);
                                    if (excep.InnerException.InnerException.Message != null)
                                        writer.WriteLine("      Inner Inner Exception Message : " + excep.InnerException.InnerException.Message);
                                }
                            }
                            writer.WriteLine("Exception Description:");
                            writer.WriteLine(excep.Message);
                            writer.WriteLine("Exception Stack Trace:");
                            writer.WriteLine(excep.StackTrace);
                            writer.WriteLine("--------------------------------------------------------------- ");
                            writer.Close();
                        }
                    }
                }


                return "Some error occured, please contact software administrator.";

            }
            catch (Exception ex)
            {
                string message = "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
                return message;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Models
{
    public class EEF
    {
        public PSW_FORM_E_DOCUMENT_INFO DocInfo { get; set; }
        public PSW_FORM_E_DOCUMENT_REQUEST_INFO DocRequestNo { get; set; }
        public PSW_BCA_INFO BCA { get; set; }
        public PSW_LIST_OF_COUNTRIES DOC_REQ_COUNTRY { get; set; }
        public PSW_LIST_OF_COUNTRIES searchOriginEEF { get; set; }
        public PSW_UNIT_OF_MEASUREMENT searchUOMEEF { get; set; }
        public IEnumerable<SelectListItem> AuthPayModes { get; set; }
        public IEnumerable<SelectListItem> ListCustomers { get; set; }
        public PSW_AUTH_PAY_MODES PayMode { get; set; }
        public PSW_PAYMENT_MODES_PARAMETERS PayParam { get; set; }
        public List<PSW_EEF_HS_CODE> hscodes { get; set; }
        public List<PSW_EEF_GD_GENERAL_INFO_LIST> EEFGD { get; set; }
        public List<PSW_EEF_BCA_LIST> BCAList { get; set; }
        public PSW_BCA_EVENT Event { get; set; }
        public PSW_CURRENCY CCY { get; set; }
        public PSW_DELIVERY_TERMS DELIVERY_TERM { get; set; }
        public PSW_PORT_OF_SHIPPING ShippingPort { get; set; }
        public bool CustomNew { get; set; }
        public bool SendAsAmendment { get; set; }
    }
}
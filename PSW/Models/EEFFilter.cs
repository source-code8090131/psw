﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class EEFFilter
    {
        public PSW_FORM_E_DOCUMENT_INFO EEF { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }   
    }
}
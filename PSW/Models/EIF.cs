﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSW.Models
{
    public class EIF
    {
        public PSW_EIF_BASIC_INFO basic { get; set; }
        public IEnumerable<SelectListItem> AuthPayModes { get; set; }
        public IEnumerable<SelectListItem> ListCustomers { get; set; }
        public PSW_EIF_DOCUMENT_REQUEST_INFO documents { get; set; }
        public PSW_EIF_BANK_INFO bank { get; set; }
        public IEnumerable<SelectListItem> ListBanks { get; set; }
        public IEnumerable<SelectListItem> ListCitis { get; set; }
        public IEnumerable<PSW_EIF_HS_CODE> hscodes { get; set; }
        public IEnumerable<PSW_EIF_BDA_LIST> BDAList { get; set; }
        public IEnumerable<PSW_EIF_GD_GENERAL_INFO_LIST> EIFGD { get; set; }
        public PSW_EIF_BDA BDA { get; set; }
        public PSW_AUTH_PAY_MODES PayMode { get; set; }
        public PSW_PAYMENT_MODES_PARAMETERS PayParam { get; set; }
        public PSW_LIST_OF_COUNTRIES BENECOUNTRY { get; set; }
        public PSW_LIST_OF_COUNTRIES EXPORTERCOUNTRY { get; set; }
        public PSW_LIST_OF_COUNTRIES searchOrigin { get; set; }
        public PSW_UNIT_OF_MEASUREMENT searchUOM { get; set; }
        public PSW_DELIVERY_TERMS DeliveryTerm { get; set; }
        public PSW_CURRENCY Currency { get; set; }
        public PSW_PORT_OF_SHIPPING ShippingPort { get; set; }
        public bool CustomNew { get; set; }
        public bool SendAsAmendment { get; set; }
    }
}
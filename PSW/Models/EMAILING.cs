﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Hosting;

namespace PSW.Models
{
    public class EMAILING
    {
        #region if Production 
        public static bool Application_Mode = true;
        #endregion

        //#region if Developer 
        //public static bool Application_Mode = false;
        //#endregion

        #region TRACER_EMAILS
        public static string UpdateEIFTracerEmails(string EIFNO)
        {
            int RowCount = 0;
            PSWEntities context = new PSWEntities();
            PSW_EIF_MASTER_DETAILS EIF = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIFNO).FirstOrDefault();
            PSW_CLIENT_MASTER_DETAIL Client = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == EIF.EIF_BI_BUSSINESS_NAME).FirstOrDefault();
            
            // 10 Days
            PSW_TRACER_EMAILS email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EIFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EIF.EIF_BI_ENTRY_DATETIME.AddDays(10);
            email.TE_SUBJECT = "EIF EXPIRY NOTIFICATION";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIFNO + " </span> is being expired, kindly submit the required documents for approval.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();

            // 15 Days
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EIFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EIF.EIF_BI_ENTRY_DATETIME.AddDays(15);
            email.TE_SUBJECT = "EIF EXPIRY NOTIFICATION- FINAL NOTICE";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIFNO + " </span> is expired, kindly submit the required documents for approval.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();

            int AdvancePaymentId = context.PSW_AUTH_PAY_MODES.Where(m => m.APM_CODE == 303).FirstOrDefault().APM_ID;
            if (EIF.EIF_BI_MODE_OF_IMPORT_PAYMENT == AdvancePaymentId)
            {
                // Advace Payment 

                // 60 Days
                email = new PSW_TRACER_EMAILS();
                email.TE_REF_DOC_ID = EIFNO;
                email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
                email.TE_ACTION_DATETIME = EIF.EIF_BI_ENTRY_DATETIME.AddDays(60);
                email.TE_SUBJECT = "SHIPPING DOCUMENTS REQUIRED AGAINST IMPORT ADVANCE PAYMENT";
                email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the shipping documents are due for submission against import advance payment EIF number <span style='font-family:Arial;font-size:13px'>" + EIFNO + ". </span> As per State Bank of Pakistan “SBP” regulation within four months of payment date undertaken at the time of payment.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                email.TE_SENT = false;
                context.PSW_TRACER_EMAILS.Add(email);
                RowCount += context.SaveChanges();

                // 70 Days
                email = new PSW_TRACER_EMAILS();
                email.TE_REF_DOC_ID = EIFNO;
                email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
                email.TE_ACTION_DATETIME = EIF.EIF_BI_ENTRY_DATETIME.AddDays(70);
                email.TE_SUBJECT = "SHIPPING DOCUMENTS REQUIRED AGAINST IMPORT ADVANCE PAYMENT- FIRST REMINDER";
                email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the shipping documents are due for submission against import advance payment EIF number <span style='font-family:Arial;font-size:13px'>" + EIFNO + ". </span> As per State Bank of Pakistan “SBP” regulation within four months of payment date undertaken at the time of payment.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                email.TE_SENT = false;
                context.PSW_TRACER_EMAILS.Add(email);
                RowCount += context.SaveChanges();

                // 80 Days
                email = new PSW_TRACER_EMAILS();
                email.TE_REF_DOC_ID = EIFNO;
                email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
                email.TE_ACTION_DATETIME = EIF.EIF_BI_ENTRY_DATETIME.AddDays(80);
                email.TE_SUBJECT = "SHIPPING DOCUMENTS REQUIRED AGAINST IMPORT ADVANCE PAYMENT- SECOND REMINDER";
                email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the shipping documents are due for submission against import advance payment EIF number <span style='font-family:Arial;font-size:13px'>" + EIFNO + ". </span> As per State Bank of Pakistan “SBP” regulation within four months of payment date undertaken at the time of payment.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                email.TE_SENT = false;
                context.PSW_TRACER_EMAILS.Add(email);
                RowCount += context.SaveChanges();

                // 90 Days
                email = new PSW_TRACER_EMAILS();
                email.TE_REF_DOC_ID = EIFNO;
                email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
                email.TE_ACTION_DATETIME = EIF.EIF_BI_ENTRY_DATETIME.AddDays(90);
                email.TE_SUBJECT = "SHIPPING DOCUMENTS REQUIRED AGAINST IMPORT ADVANCE PAYMENT- THIRD REMINDER";
                email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the shipping documents are due for submission against import advance payment EIF number <span style='font-family:Arial;font-size:13px'>" + EIFNO + ". </span> As per State Bank of Pakistan “SBP” regulation within four months of payment date undertaken at the time of payment.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                email.TE_SENT = false;
                context.PSW_TRACER_EMAILS.Add(email);
                RowCount += context.SaveChanges();

                // 120 Days
                email = new PSW_TRACER_EMAILS();
                email.TE_REF_DOC_ID = EIFNO;
                email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
                email.TE_ACTION_DATETIME = EIF.EIF_BI_ENTRY_DATETIME.AddDays(120);
                email.TE_SUBJECT = "SHIPPING DOCUMENTS REQUIRED AGAINST IMPORT ADVANCE PAYMENT- FINAL NOTICE";
                email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the shipping documents are due for submission against import advance payment EIF number <span style='font-family:Arial;font-size:13px'>" + EIFNO + ". </span> As per State Bank of Pakistan “SBP” regulation within four months of payment date undertaken at the time of payment.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                email.TE_SENT = false;
                context.PSW_TRACER_EMAILS.Add(email);
                RowCount += context.SaveChanges();
            }

            return RowCount + " Tracer Emails Set For The Document # " + EIFNO;
        }

        public static string UpdateEEFTracerEmails(string EEFNO)
        {
            int RowCount = 0;
            PSWEntities context = new PSWEntities();
            PSW_EEF_MASTER_DETAILS EEF = context.PSW_EEF_MASTER_DETAILS.Where(m => m.FDI_FORM_E_NO == EEFNO).FirstOrDefault();
            PSW_CLIENT_MASTER_DETAIL Client = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == EEF.FDI_TRADER_NAME).FirstOrDefault();

            // 10 Days
            PSW_TRACER_EMAILS email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(10);
            email.TE_SUBJECT = "E FORM "+ EEFNO +" STATUS";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the E Form <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> is being expired.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();

            // 15 Days
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(15);
            email.TE_SUBJECT = "E FORM " + EEFNO + " STATUS";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the E Form <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Has been expired.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();


            // 20 Days
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(20);
            email.TE_SUBJECT = "E FORM " + EEFNO + " STATUS";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the E Form <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Will be expired after 45 days from E form certification date.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();


            // 30 Days
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(30);
            email.TE_SUBJECT = "E FORM " + EEFNO + " STATUS- FIRST REMINDER";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the E Form <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Will be expired after 45 days from E form certification date.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();

            // 35 Days
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(35);
            email.TE_SUBJECT = "E FORM " + EEFNO + " STATUS- SECOND REMINDER";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the E Form <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Will be expired after 45 days from E form certification date.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();


            // 40 Days
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(40);
            email.TE_SUBJECT = "E FORM " + EEFNO + " STATUS- THIRD REMINDER";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the E Form <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Will be expired after 45 days from E form certification date.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();

            // 45 Days
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(45);
            email.TE_SUBJECT = "E FORM " + EEFNO + " STATUS- FINAL REMINDER";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the E Form <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Has been expired.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();


            // 7 Days After Shipping Date
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(7);
            email.TE_SUBJECT = "EXPORT DOCUMENTS SUBMISSION AWAITED- FIRST REMINDER";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note in reference to E form number <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Shipping documents submission awaited.</p><p style='font-family:Candara;font-size:15px'>As per State Bank of Pakistan “SBP” regulation these should be submitted to Authorized Dealer within 14 days of shipment date.Kindly submit at the earliest to update record.<br /> </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();


            // 10 Days After Shipping Date
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(10);
            email.TE_SUBJECT = "EXPORT DOCUMENTS SUBMISSION AWAITED- SECOND REMINDER";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note in reference to E form number <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Shipping documents submission awaited.</p><p style='font-family:Candara;font-size:15px'>As per State Bank of Pakistan “SBP” regulation these should be submitted to Authorized Dealer within 14 days of shipment date.Kindly submit at the earliest to update record.<br /> </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();

            // 12 Days After Shipping Date
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(12);
            email.TE_SUBJECT = "EXPORT DOCUMENTS SUBMISSION AWAITED- THIRD REMINDER";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note in reference to E form number <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Shipping documents submission awaited.</p><p style='font-family:Candara;font-size:15px'>As per State Bank of Pakistan “SBP” regulation these should be submitted to Authorized Dealer within 14 days of shipment date.Kindly submit at the earliest to update record.<br /> </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();

            // 14 Days After Shipping Date
            email = new PSW_TRACER_EMAILS();
            email.TE_REF_DOC_ID = EEFNO;
            email.TE_TO_EMAIL = Client.C_EMAIL_ADDRESS;
            email.TE_ACTION_DATETIME = EEF.FDI_ENTRY_DATETIME.AddDays(14);
            email.TE_SUBJECT = "EXPORT DOCUMENTS SUBMISSION AWAITED- FINAL REMINDER";
            email.TE_BODY = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + Client.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note in reference to E form number <span style='font-family:Arial;font-size:13px'>" + EEFNO + " </span> Shipping documents submission awaited.</p><p style='font-family:Candara;font-size:15px'>As per State Bank of Pakistan “SBP” regulation these should be submitted to Authorized Dealer within 14 days of shipment date.Kindly submit at the earliest to update record.<br /> </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
            email.TE_SENT = false;
            context.PSW_TRACER_EMAILS.Add(email);
            RowCount += context.SaveChanges();







            return RowCount + " Tracer Emails Set For The Document # " + EEFNO;
        }

        #endregion



        ////Emailing Starts//

        #region Import Emails
        public static string EIF_Approve(string EIF_REQ_NO,int AMEND)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO && m.EIF_BI_AMENDMENT_NO == AMEND).FirstOrDefault();
                    if (CheckEIF != null)
                    {
                        int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEIF.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_ID;
                        PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                        {
                            ReportDocument rd = new ReportDocument();
                            List<PSW_EIF_HS_CODE> CheckHsCode = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == CheckEIF.EIF_BI_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == CheckEIF.EIF_BI_AMENDMENT_NO).ToList();
                            if (CheckHsCode.Count() > 0)
                            {
                                DateTime InvalidDate = Convert.ToDateTime("1900-01-01");
                                var EIFMASTER = context.PSW_SINGLE_EIF_VIEW.Where(m => m.EIF_BI_EIF_REQUEST_NO == CheckEIF.EIF_BI_EIF_REQUEST_NO && m.EIF_BI_AMENDMENT_NO == CheckEIF.EIF_BI_AMENDMENT_NO).FirstOrDefault();
                                var EIFDETAILS = context.PSW_EIF_HS_CODE_VIEW_WITH_INVOICE_VALUE.Where(m => m.EIF_HC_EIF_REQUEST_NO == CheckEIF.EIF_BI_EIF_REQUEST_NO && m.EIF_HC_AMENDMENT_NO == CheckEIF.EIF_BI_AMENDMENT_NO).ToList();
                                rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReports\\PSW_EIF_DETAIL_REPORT.rpt"));
                                rd.DataSourceConnections.Clear();
                                rd.SetDataSource(new[] { EIFMASTER });
                                rd.Subreports[0].DataSourceConnections.Clear();
                                rd.Subreports[0].SetDataSource(EIFDETAILS);
                                if (EIFMASTER.EIF_BA_EXPIRY_DATE == InvalidDate)
                                    rd.SetParameterValue("EIF_EXPIRY_DATE", "");
                                else
                                {
                                    var shortDate = EIFMASTER.EIF_BA_EXPIRY_DATE.ToString("MMMM dd, yyyy");
                                    rd.SetParameterValue("EIF_EXPIRY_DATE", shortDate);
                                }
                                if (EIFMASTER.EIF_BA_INNTENDED_PAYMENT_DATE == InvalidDate)
                                    rd.SetParameterValue("EIF_INN_PAYMENT_DATE", "");
                                else
                                {
                                    var shortDate = EIFMASTER.EIF_BA_INNTENDED_PAYMENT_DATE.ToString("MMMM dd, yyyy");
                                    rd.SetParameterValue("EIF_INN_PAYMENT_DATE", shortDate);
                                }
                                if (EIFMASTER.EIF_BA_TRANSPORT_DOC_DATE == InvalidDate)
                                    rd.SetParameterValue("EIF_TRANS_DOC_DATE", "");
                                else
                                {
                                    var shortDate = EIFMASTER.EIF_BA_TRANSPORT_DOC_DATE.ToString("MMMM dd, yyyy");
                                    rd.SetParameterValue("EIF_TRANS_DOC_DATE", shortDate);
                                }
                                if (EIFMASTER.EIF_BA_FINAL_DATE_OF_SHIPMENT == InvalidDate)
                                    rd.SetParameterValue("EIF_FINAL_SHIP_DATE", "");
                                else
                                {
                                    var shortDate = EIFMASTER.EIF_BA_FINAL_DATE_OF_SHIPMENT.ToString("MMMM dd, yyyy");
                                    rd.SetParameterValue("EIF_FINAL_SHIP_DATE", shortDate);
                                }
                                if (EIFMASTER.EIF_BA_SBP_APPROVAL_DATE == InvalidDate)
                                    rd.SetParameterValue("EIF_SBP_APPROVAL_DATE", "");
                                else
                                {
                                    var shortDate = EIFMASTER.EIF_BA_SBP_APPROVAL_DATE.ToString("MMMM dd, yyyy");
                                    rd.SetParameterValue("EIF_SBP_APPROVAL_DATE", shortDate);
                                }
                            }
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length -1);
                                        if(specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEIF.EIF_BI_AMENDMENT_NO == 0)
                                        mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    else
                                        mail.Subject = "(Amend) EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    if (CheckHsCode.Count() > 0)
                                        mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), "PSW-EIF-REPORT(" + CheckEIF.EIF_BI_EIF_REQUEST_NO + ").pdf"));
                                    mail.IsBodyHtml = true;
                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    rd.Close();
                                    rd.Dispose();
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Test@@123";
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEIF.EIF_BI_AMENDMENT_NO == 0)
                                        mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    else
                                        mail.Subject = "(Amend) EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    if (CheckHsCode.Count() > 0)
                                        mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), "PSW-EIF-REPORT(" + CheckEIF.EIF_BI_EIF_REQUEST_NO + ").pdf"));
                                    mail.IsBodyHtml = true;
                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    rd.Close();
                                    rd.Dispose();
                                }
                            }

                            rd.Close();
                            rd.Dispose();
                        }
                        else
                        {
                            if (GetClientInfo == null)
                                message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                            if (GetClientInfo.C_TO_CLIENT_EMAIL == null)
                                message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)      
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EIF_Edit(string EIF_REQ_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                    if (CheckEIF != null)
                    {
                        int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEIF.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_ID;
                        PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                        {
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEIF.EIF_BI_AMENDMENT_NO == 0)
                                        mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    else
                                        mail.Subject = "(Amend) EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been amended at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Testuser0592";
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEIF.EIF_BI_AMENDMENT_NO == 0)
                                        mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    else
                                        mail.Subject = "(Amend) EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been amended at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            if (GetClientInfo == null)
                                message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                            if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EIF_Cancel(string EIF_REQ_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                    if (CheckEIF != null)
                    {
                        int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEIF.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_ID;
                        PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                        {
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEIF.EIF_BI_AMENDMENT_NO == 0)
                                        mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    else
                                        mail.Subject = "(Amend) EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been cancelled.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Testuser0592";
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEIF.EIF_BI_AMENDMENT_NO == 0)
                                        mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    else
                                        mail.Subject = "(Amend) EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been cancelled.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            if (GetClientInfo == null)
                                message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                            if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EIF_HsCode(string EIFHsCode, string EIF_REQ_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_HS_CODE GetHsCode = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                    if (GetHsCode != null)
                    {
                        PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                        if (CheckEIF != null)
                        {
                            int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEIF.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_ID;
                            PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                            if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                            {
                                if (Application_Mode == true)
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (CheckEIF.EIF_BI_AMENDMENT_NO == 0)
                                            mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) EIF " + EIF_REQ_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note you have selected the HS Code <span style='font-family:Arial;font-size:13px'>" + EIFHsCode + "</span>  Fall under SBP cash margin held requirement and will be processed accordingly.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                                else
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        //////#if dev
                                        NetworkCredentials.Password = "Testuser0592";
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (CheckEIF.EIF_BI_AMENDMENT_NO == 0)
                                            mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) EIF " + EIF_REQ_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note you have selected the HS Code <span style='font-family:Arial;font-size:13px'>" + EIFHsCode + "</span>  Fall under SBP cash margin held requirement and will be processed accordingly.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            //if dev
                                            smtp.EnableSsl = true;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                            }
                            else
                            {
                                if (GetClientInfo == null)
                                    message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                                if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                    message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                            }
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Hs Code For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EIF_GD(string EIF_REQ_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                    if (CheckEIF != null)
                    {
                        int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEIF.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_ID;
                        PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                        {
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    mail.Subject = "GD UPDATE";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>This is to update that you have submitted Goods Declaration in Pakistan Single window.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Testuser0592";
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    mail.Subject = "GD UPDATE";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>This is to update that you have submitted Goods Declaration in Pakistan Single window.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            if (GetClientInfo == null)
                                message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                            if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EIF_BDA_Approve(int BDA_ID)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BDA BDA = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_ID == BDA_ID).FirstOrDefault();
                    if (BDA != null)
                    {
                        PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == BDA.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                        if (CheckEIF != null)
                        {
                            var EIFBDA = context.PSW_SINGLE_EIF_BDA_VIEW.Where(m => m.EIF_BDA_EIF_REQUEST_NO == BDA.EIF_BDA_EIF_REQUEST_NO && m.EIF_BDA_AMENDMENT_NO == BDA.EIF_BDA_AMENDMENT_NO && m.EIF_BDA_NO == BDA.EIF_BDA_NO).FirstOrDefault();
                            ReportDocument rd = new ReportDocument();
                            rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReports\\PSW_EIF_BDA_REPORT.rpt"));
                            rd.DataSourceConnections.Clear();
                            rd.SetDataSource(new[] { EIFBDA });

                            int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEIF.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_ID;
                            PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                            if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                            {
                                if (Application_Mode == true)
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (BDA.EIF_BDA_AMENDMENT_NO == 0)
                                            mail.Subject = "BDA : " + BDA.EIF_BDA_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) BDA : " + BDA.EIF_BDA_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested BDA <span style='font-family:Arial;font-size:13px'>" + BDA.EIF_BDA_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), "PSW-BDA-REPORT(" + BDA.EIF_BDA_EIF_REQUEST_NO + ").pdf"));
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                        rd.Close();
                                        rd.Dispose();
                                    }
                                }
                                else
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        //////#if dev
                                        NetworkCredentials.Password = "Test@@123";
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (BDA.EIF_BDA_AMENDMENT_NO == 0)
                                            mail.Subject = "BDA : " + BDA.EIF_BDA_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) BDA : " + BDA.EIF_BDA_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested BDA <span style='font-family:Arial;font-size:13px'>" + BDA.EIF_BDA_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), "PSW-BDA-REPORT(" + BDA.EIF_BDA_EIF_REQUEST_NO + ").pdf"));
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            //if dev
                                            smtp.EnableSsl = true;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                        rd.Close();
                                        rd.Dispose();
                                    }
                                }
                            }
                            else
                            {
                                if (GetClientInfo == null)
                                    message += "Exception : Problem while Fetching Client Detail For BDA No : " + BDA.EIF_BDA_NO + Environment.NewLine;
                                if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                    message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                            }

                            rd.Close();
                            rd.Dispose();
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Data For BDA No : " + BDA.EIF_BDA_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For BDA No : " + BDA_ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EIF_BDA_Reverse(int BDA_ID)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BDA BDA = context.PSW_EIF_BDA.Where(m => m.EIF_BDA_ID == BDA_ID).FirstOrDefault();
                    if (BDA != null)
                    {
                        PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == BDA.EIF_BDA_EIF_REQUEST_NO).FirstOrDefault();
                        if (CheckEIF != null)
                        {
                            int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEIF.EIF_BI_BUSSINESS_NAME).FirstOrDefault().C_ID;
                            PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                            if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                            {
                                if (Application_Mode == true)
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (BDA.EIF_BDA_AMENDMENT_NO == 0)
                                            mail.Subject = "BDA : " + BDA.EIF_BDA_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) BDA : " + BDA.EIF_BDA_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested BDA <span style='font-family:Arial;font-size:13px'>" + BDA.EIF_BDA_NO + "</span>  has been reverse.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                                else
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        //////#if dev
                                        NetworkCredentials.Password = "Testuser0592";
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (BDA.EIF_BDA_AMENDMENT_NO == 0)
                                            mail.Subject = "BDA : " + BDA.EIF_BDA_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) BDA : " + BDA.EIF_BDA_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested BDA <span style='font-family:Arial;font-size:13px'>" + BDA.EIF_BDA_NO + "</span>  has been reverse.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            //if dev
                                            smtp.EnableSsl = true;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                            }
                            else
                            {
                                if (GetClientInfo == null)
                                    message += "Exception : Problem while Fetching Client Detail For BDA No : " + BDA.EIF_BDA_NO + Environment.NewLine;
                                if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                    message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                            }
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Data For BDA No : " + BDA.EIF_BDA_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For BDA No : " + BDA_ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        #endregion

        #region Export Emails
        public static string EEF_GD(string EXPORTER_IBAN)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_CLIENT_MASTER_DETAIL GetClientInfo = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == EXPORTER_IBAN).FirstOrDefault();
                    if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                    {
                        if (Application_Mode == true)
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                string HostName = EmailConfig.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                mail.From = new MailAddress(FromEmail);
                                if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                {
                                    string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                    string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                    if (specialChar.Contains(LastChar))
                                    {
                                        string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        string[] TO_Email = toemail.Split(',');
                                        foreach (string MultiToEmail in TO_Email)
                                        {
                                            mail.To.Add(MultiToEmail);
                                        }
                                    }
                                    else
                                    {
                                        string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                        foreach (string MultiToEmail in TO_Email)
                                        {
                                            mail.To.Add(MultiToEmail);
                                        }
                                    }
                                }
                                else
                                    mail.To.Add(new MailAddress(ToEmail));
                                string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                foreach (string MultiEmail in CC_Email)
                                {
                                    mail.CC.Add(MultiEmail);
                                }
                                mail.Subject = "GD UPDATE";
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>This is to update that you have submitted Goods Declaration in Pakistan Single window.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.Send(mail);
                                }
                                message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                            }
                        }
                        else
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                string HostName = EmailConfig.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                //////#if dev
                                NetworkCredentials.Password = "Testuser0592";
                                mail.From = new MailAddress(FromEmail);
                                if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                {
                                    string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                    string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                    if (specialChar.Contains(LastChar))
                                    {
                                        string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        string[] TO_Email = toemail.Split(',');
                                        foreach (string MultiToEmail in TO_Email)
                                        {
                                            mail.To.Add(MultiToEmail);
                                        }
                                    }
                                    else
                                    {
                                        string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                        foreach (string MultiToEmail in TO_Email)
                                        {
                                            mail.To.Add(MultiToEmail);
                                        }
                                    }
                                }
                                else
                                    mail.To.Add(new MailAddress(ToEmail));
                                string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                foreach (string MultiEmail in CC_Email)
                                {
                                    mail.CC.Add(MultiEmail);
                                }
                                mail.Subject = "GD UPDATE";
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>This is to update that you have submitted Goods Declaration in Pakistan Single window.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    //if dev
                                    smtp.EnableSsl = true;
                                    smtp.Send(mail);
                                }
                                message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        if (GetClientInfo == null)
                            message += "Exception : Problem while Fetching Client Detail on the given IBAN No : " + EXPORTER_IBAN + Environment.NewLine;
                        if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                            message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EEF_Approve(string E_FORM_NO)
        {
            string message = "";
            ReportDocument rd = new ReportDocument();
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_FORM_E_DOCUMENT_INFO CheckEEF = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E_FORM_NO).FirstOrDefault();
                    if (CheckEEF != null)
                    {
                        int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEEF.FDI_TRADER_NAME).FirstOrDefault().C_ID;
                        PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                        {
                            List<PSW_EEF_HS_CODE> CheckHsCode = context.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == CheckEEF.FDI_FORM_E_NO && m.EEF_HC_AMENDMENT_NO == CheckEEF.FDI_AMENDMENT_NO).ToList();
                            if (CheckHsCode.Count() > 0)
                            {
                                var EEFMASTER = context.PSW_SINGLE_EEF_VIEW.Where(m => m.FDI_FORM_E_NO == CheckEEF.FDI_FORM_E_NO && m.FDI_AMENDMENT_NO == CheckEEF.FDI_AMENDMENT_NO).FirstOrDefault();
                                var EEFDETAILS = context.PSW_EEF_HS_CODE_VIEW.Where(m => m.EEF_HC_EEF_REQUEST_NO == CheckEEF.FDI_FORM_E_NO && m.EEF_HC_AMENDMENT_NO == CheckEEF.FDI_AMENDMENT_NO).ToList();
                                
                                rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReports\\PSW_EEF_DETAIL_REPORT.rpt"));
                                rd.DataSourceConnections.Clear();
                                rd.SetDataSource(new[] { EEFMASTER });
                                rd.Subreports[0].DataSourceConnections.Clear();
                                rd.Subreports[0].SetDataSource(EEFDETAILS);
                            }
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEEF.FDI_AMENDMENT_NO == 0)
                                        mail.Subject = "E FORM : " + E_FORM_NO + " STATUS";
                                    else
                                        mail.Subject = "(Amend) E FORM : " + E_FORM_NO + " STATUS";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested Electronic Form E <span style='font-family:Arial;font-size:13px'>" + E_FORM_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    if(CheckHsCode.Count() > 0)
                                        mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), "PSW-EEF-REPORT(" + CheckEEF.FDI_FORM_E_NO + ").pdf"));
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    rd.Close();
                                    rd.Dispose();
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Test@@123";
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEEF.FDI_AMENDMENT_NO == 0)
                                        mail.Subject = "E FORM : " + E_FORM_NO + " STATUS";
                                    else
                                        mail.Subject = "(Amend) E FORM : " + E_FORM_NO + " STATUS";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested Electronic Form E <span style='font-family:Arial;font-size:13px'>" + E_FORM_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    if (CheckHsCode.Count() > 0)
                                        mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), "PSW-EEF-REPORT(" + CheckEEF.FDI_FORM_E_NO + ").pdf"));
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    rd.Close();
                                    rd.Dispose();
                                }
                            }

                            rd.Close();
                            rd.Dispose();
                        }
                        else
                        {
                            if (GetClientInfo == null)
                                message += "Exception : Problem while Fetching Client Detail For E FORM No : " + E_FORM_NO + Environment.NewLine;
                            if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data on E FORM No : " + E_FORM_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EEF_Edit(string E_FORM_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_FORM_E_DOCUMENT_INFO CheckEEF = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E_FORM_NO).FirstOrDefault();
                    if (CheckEEF != null)
                    {
                        int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEEF.FDI_TRADER_NAME).FirstOrDefault().C_ID;
                        PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                        {
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEEF.FDI_AMENDMENT_NO == 0)
                                        mail.Subject = "E FORM : " + E_FORM_NO + " STATUS";
                                    else
                                        mail.Subject = "(Amend) E FORM : " + E_FORM_NO + " STATUS";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested Electronic Form E <span style='font-family:Arial;font-size:13px'>" + E_FORM_NO + "</span>  has been amended at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Testuser0592";
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEEF.FDI_AMENDMENT_NO == 0)
                                        mail.Subject = "E FORM : " + E_FORM_NO + " STATUS";
                                    else
                                        mail.Subject = "(Amend) E FORM : " + E_FORM_NO + " STATUS";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested Electronic Form E <span style='font-family:Arial;font-size:13px'>" + E_FORM_NO + "</span>  has been amended at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            if (GetClientInfo == null)
                                message += "Exception : Problem while Fetching Client Detail For E FORM No : " + E_FORM_NO + Environment.NewLine;
                            if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data on E FORM No : " + E_FORM_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EEF_Cancel(string E_FORM_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_FORM_E_DOCUMENT_INFO CheckEEF = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == E_FORM_NO).FirstOrDefault();
                    if (CheckEEF != null)
                    {
                        int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEEF.FDI_TRADER_NAME).FirstOrDefault().C_ID;
                        PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                        {
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEEF.FDI_AMENDMENT_NO == 0)
                                        mail.Subject = "E FORM : " + E_FORM_NO + " STATUS";
                                    else
                                        mail.Subject = "(Amend) E FORM : " + E_FORM_NO + " STATUS";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested Electronic Form E <span style='font-family:Arial;font-size:13px'>" + E_FORM_NO + "</span>  has been Cancelled.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Testuser0592";
                                    mail.From = new MailAddress(FromEmail);
                                    if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                    {
                                        string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                        string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                        if (specialChar.Contains(LastChar))
                                        {
                                            string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            string[] TO_Email = toemail.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                        else
                                        {
                                            string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                            foreach (string MultiToEmail in TO_Email)
                                            {
                                                mail.To.Add(MultiToEmail);
                                            }
                                        }
                                    }
                                    else
                                        mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                    if (CheckEEF.FDI_AMENDMENT_NO == 0)
                                        mail.Subject = "E FORM : " + E_FORM_NO + " STATUS";
                                    else
                                        mail.Subject = "(Amend) E FORM : " + E_FORM_NO + " STATUS";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested Electronic Form E <span style='font-family:Arial;font-size:13px'>" + E_FORM_NO + "</span>  has been Cancelled.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            if (GetClientInfo == null)
                                message += "Exception : Problem while Fetching Client Detail For E FORM No : " + E_FORM_NO + Environment.NewLine;
                            if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data on E FORM No : " + E_FORM_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EEF_BCA_Approve(int BCA_ID)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_BCA_INFO BCA = context.PSW_BCA_INFO.Where(m => m.BCAD_ID == BCA_ID).FirstOrDefault();
                    if (BCA != null)
                    {
                        PSW_FORM_E_DOCUMENT_INFO CheckEEF = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == BCA.BCAD_FORM_E_NO).FirstOrDefault();
                        if (CheckEEF != null)
                        {
                            int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEEF.FDI_TRADER_NAME).FirstOrDefault().C_ID;
                            PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                            if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                            {
                                if (Application_Mode == true)
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (BCA.BCAD_AMENDMENT_NO == 0)
                                            mail.Subject = "BCA : " + BCA.BCAD_BCA_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) BCA : " + BCA.BCAD_BCA_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested BCA <span style='font-family:Arial;font-size:13px'>" + BCA.BCAD_BCA_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                                else
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        //////#if dev
                                        NetworkCredentials.Password = "Testuser0592";
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (BCA.BCAD_AMENDMENT_NO == 0)
                                            mail.Subject = "BCA : " + BCA.BCAD_BCA_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) BCA : " + BCA.BCAD_BCA_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested BCA <span style='font-family:Arial;font-size:13px'>" + BCA.BCAD_BCA_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            //if dev
                                            smtp.EnableSsl = true;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                            }
                            else
                            {
                                if (GetClientInfo == null)
                                    message += "Exception : Problem while Fetching Client Detail For BCA No : " + BCA.BCAD_BCA_NO + Environment.NewLine;
                                if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                    message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                            }
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Data For BDA No : " + BCA.BCAD_BCA_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For BDA No : " + BCA_ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }

        public static string EEF_BCA_Reverse(int BCA_ID)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_BCA_INFO BCA = context.PSW_BCA_INFO.Where(m => m.BCAD_ID == BCA_ID).FirstOrDefault();
                    if (BCA != null)
                    {
                        PSW_FORM_E_DOCUMENT_INFO CheckEEF = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == BCA.BCAD_FORM_E_NO).FirstOrDefault();
                        if (CheckEEF != null)
                        {
                            int ClientID = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == CheckEEF.FDI_TRADER_NAME).FirstOrDefault().C_ID;
                            PSW_CLIENT_INFO GetClientInfo = context.PSW_CLIENT_INFO.Where(m => m.C_ID == ClientID).FirstOrDefault();
                            if (GetClientInfo != null && GetClientInfo.C_TO_CLIENT_EMAIL != null)
                            {
                                if (Application_Mode == true)
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (BCA.BCAD_AMENDMENT_NO == 0)
                                            mail.Subject = "BCA : " + BCA.BCAD_BCA_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) BCA : " + BCA.BCAD_BCA_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested BCA <span style='font-family:Arial;font-size:13px'>" + BCA.BCAD_BCA_NO + "</span>  has been reverse.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                                else
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_TO_CLIENT_EMAIL;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        //////#if dev
                                        NetworkCredentials.Password = "Testuser0592";
                                        mail.From = new MailAddress(FromEmail);
                                        if (GetClientInfo.C_TO_CLIENT_EMAIL.Contains(","))
                                        {
                                            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                                            string LastChar = GetClientInfo.C_TO_CLIENT_EMAIL.Substring(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                            if (specialChar.Contains(LastChar))
                                            {
                                                string toemail = GetClientInfo.C_TO_CLIENT_EMAIL.Remove(GetClientInfo.C_TO_CLIENT_EMAIL.Length - 1);
                                                string[] TO_Email = toemail.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                            else
                                            {
                                                string[] TO_Email = GetClientInfo.C_TO_CLIENT_EMAIL.Split(',');
                                                foreach (string MultiToEmail in TO_Email)
                                                {
                                                    mail.To.Add(MultiToEmail);
                                                }
                                            }
                                        }
                                        else
                                            mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.CC.Add(MultiEmail);
                                        }
                                        if (BCA.BCAD_AMENDMENT_NO == 0)
                                            mail.Subject = "BCA : " + BCA.BCAD_BCA_NO + " Status";
                                        else
                                            mail.Subject = "(Amend) BCA : " + BCA.BCAD_BCA_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested BCA <span style='font-family:Arial;font-size:13px'>" + BCA.BCAD_BCA_NO + "</span>  has been reverse.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            //if dev
                                            smtp.EnableSsl = true;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                            }
                            else
                            {
                                if (GetClientInfo == null)
                                    message += "Exception : Problem while Fetching Client Detail For BCA No : " + BCA.BCAD_BCA_NO + Environment.NewLine;
                                if (GetClientInfo.C_TO_CLIENT_EMAIL != null)
                                    message += "No email address found against Client : " + GetClientInfo.C_NAME + " in the system " + Environment.NewLine;
                            }
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Data For BDA No : " + BCA.BCAD_BCA_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For BDA No : " + BCA_ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured in Sending Email : " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception - " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception - " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
            }
            return (message);
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class GD_Import
    {
        public PSW_EIF_GD_GENERAL_INFO GeneralInfo { get; set; }
        public PSW_EIF_GD_IMPORT_EXPORT_INFO IE_Info { get; set; }
        public PSW_EIF_GD_INFORMATION GD_Information { get; set; }
        public IEnumerable<PSW_GD_IMORT_HS_CODE> Hs_Code { get; set; }
        public IEnumerable<PSW_GD_IMPORT_PACKAGE_INFORMATION> Package_Info { get; set; }
        public IEnumerable<PSW_GD_IMPORT_CONTAINER_INFORMATION> Container_Info { get; set; }


    }
}
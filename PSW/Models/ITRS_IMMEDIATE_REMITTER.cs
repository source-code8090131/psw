﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ITRS_IMMEDIATE_REMITTER
    {
        public PSW_ITRS_REMITTER Entity { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
﻿using System;
using System.DirectoryServices;
using System.Collections.Specialized;
using System.Collections;

namespace PSW.Models
{
    public class LdapAuthentication
    {

        private String _path;
        private String _filterAttribute;

        public LdapAuthentication(String path)
        {
            _path = path;
        }

        public bool IsAuthenticated(String domain, String username, String pwd)
        {
            String domainAndUsername = domain + @"\" + username;
            //String domainAndUsername = username + "@" + domain;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);

            try
            {	//Bind to the native AdsObject to force authentication.			
                Object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (null == result)
                {
                    return false;
                }

                //Update the new path to the user in the directory.
                _path = result.Path;
                _filterAttribute = (String)result.Properties["cn"][0];
            }
            catch (Exception ex)
            {
                throw new Exception("Error authenticating user. " + ex.Message);
            }

            return true;
        }
        public SortedList GetADGroups(string loginName, string domain, string pwd)
        {
            if (string.IsNullOrEmpty(loginName))
                throw new ArgumentException("The loginName should not be empty");

            SortedList ADGroups = new SortedList();

            int backSlash = loginName.IndexOf("\\");
            string userName = backSlash > 0 ? loginName.Substring(backSlash + 1) : loginName;



            String domainAndUsername = domain + @"\" + loginName;
            //String domainAndUsername = username + "@" + domain;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);

            Object obj = entry.NativeObject;

            DirectorySearcher search = new DirectorySearcher(entry);
            search.Filter = "(SAMAccountName=" + loginName + ")";
            search.PropertiesToLoad.Add("cn");
            SearchResult result = search.FindOne();




            //DirectoryEntry directoryEntry = new DirectoryEntry();
            //DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry, "(sAMAccountName=" + userName + ")");

            //SearchResult searchResult = directorySearcher.FindOne();
            if (null != result)
            {
                DirectoryEntry userADEntry = new DirectoryEntry(result.Path);

                // Invoke Groups method.
                object userADGroups = userADEntry.Invoke("Groups");
                foreach (object obj2 in (IEnumerable)userADGroups)
                {
                    // Create object for each group.
                    DirectoryEntry groupDirectoryEntry = new DirectoryEntry(obj2);
                    string groupName = groupDirectoryEntry.Name.Replace("cn=", string.Empty);
                    groupName = groupName.Replace("CN=", string.Empty);
                    if (!ADGroups.ContainsKey(groupName))
                        ADGroups.Add(groupName, groupName);
                }
            }

            return ADGroups;
        }
    }
}
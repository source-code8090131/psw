﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ManageAccountTypeFilter
    {
        public PSW_ACCOUNT_TYPE Entity { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
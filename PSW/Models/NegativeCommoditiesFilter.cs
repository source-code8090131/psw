﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class NegativeCommoditiesFilter
    {
        public PSW_NEGATIVE_LIST_OF_COMMODITIES Entity { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class NegativeCountriesFilter
    {
        public PSW_NEGATIVE_LIST_OF_COUNTRIES Entity { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSW.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class PSW_EEF_GD_GENERAL_INFO
    {
        public int EFGDGI_ID { get; set; }
        public string EFGDGI_AGENT_NAME { get; set; }
        public string EFGDGI_TRADE_TYPE { get; set; }
        public string EFGDGI_GD_NO { get; set; }
        public string EFGDGI_GD_STATUS { get; set; }
        public string EFGDGI_COLLECTORATE { get; set; }
        public string EFGDGI_DESTINATION_COUNTRY { get; set; }
        public string EFGDGI_PLACE_OF_DELIVERY { get; set; }
        public string EFGDGI_GENERAL_DESCRIPTION { get; set; }
        public string EFGDGI_SHED_TERMINAL_LOCATION { get; set; }
        public string EFGDGI_AGENT_LICENSE_NO { get; set; }
        public string EFGDGI_DECLARATION_TYPE { get; set; }
        public string EFGDGI_CONSIGNMENT_CATE { get; set; }
        public string EFGDGI_GD_TYPE { get; set; }
        public string EFGDGI_PORT_OF_DISCHARGE { get; set; }
        public string EFGDGI_1ST_EXAMINATION { get; set; }
        public string EFGDGI_NEG_COUNTRY { get; set; }
        public string EFGDGI_NEG_SUPPLIER { get; set; }
        public string EFGDGI_NEG_COMMS { get; set; }
        public Nullable<System.DateTime> EFGDGI_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> EIGDGI_EDIT_DATETIME { get; set; }
        public string EFGDGI_MAKER_ID { get; set; }
        public string EFGDGI_CHECKER_ID { get; set; }
        public string EFGDGI_VIR_AIR_NUMBER { get; set; }
        public string EFGDGI_EXPORTER_IBAN { get; set; }
        public string EFGDGI_MODE_OF_PAYMENT { get; set; }
        public string EFGDGI_CURRENCY { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> EFGDGI_TOTAL_DECLARED_VALUE { get; set; }
        public string EFGDGI_INVOICE_NUMBER { get; set; }
        public Nullable<System.DateTime> EFGDGI_INVOICE_DATE { get; set; }
        public string EFGDGI_DELIVERY_TERM { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> EFGDGI_FOBVALUE_USD { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> EFGDGI_FREIGHT_USD { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> EFGDGI_CFR_VALUE_USD { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> EFGDGI_INSURANCE_USD { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> EFGDGI_LANDING_CHARGES_USD { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> EFGDGI_ASSESSED_VALUE_USD { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> EFGDGI_OTHER_CHARGES { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> EFGDGI_EXCHANGE_RATE { get; set; }
        public string EFGDGI_CONSIGNMENT_TYPE { get; set; }
        public string EFGDGI_SHIPPING_LINE { get; set; }
        public int EFGDGI_AMENDMENT_NO { get; set; }
        public Nullable<System.DateTime> EFGDGI_CLEARANCE_DATE { get; set; }
    }
}

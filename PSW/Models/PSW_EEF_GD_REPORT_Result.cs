//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSW.Models
{
    using System;
    
    public partial class PSW_EEF_GD_REPORT_Result
    {
        public string HS_CODE { get; set; }
        public string HS_CODE_DESCRIPTION { get; set; }
        public string HS_CODE_QUANTITY { get; set; }
        public string HS_CODE_UNIT_PRICE { get; set; }
        public string HS_CODE_UOM { get; set; }
        public System.DateTime EFGDGI_ENTRY_DATETIME { get; set; }
        public int EFGDGI_ID { get; set; }
        public string EFGDGI_GD_NO { get; set; }
        public decimal EFGDGI_FOBVALUE_USD { get; set; }
        public decimal EFGDGI_FREIGHT_USD { get; set; }
        public decimal EFGDGI_INSURANCE_USD { get; set; }
        public decimal EFGDGI_CFR_VALUE_USD { get; set; }
        public decimal EFGDGI_TOTAL_DECLARED_VALUE { get; set; }
        public string EFGDGI_COLLECTORATE { get; set; }
        public string EFGDGI_DECLARATION_TYPE { get; set; }
        public string EFGDI_BL_NO { get; set; }
        public string EFGDI_PORT_OF_SHIPMENT { get; set; }
        public string EFGDGI_PORT_OF_DISCHARGE { get; set; }
        public System.DateTime EFGDGI_INVOICE_DATE { get; set; }
        public string EFGDIE_CONSIGNOR_NAME { get; set; }
        public string EFGDIE_NTN_FTN { get; set; }
        public string EFGDIE_CONSIGNOR_ADDRESS { get; set; }
        public string EFGDIE_CONSIGNEE_NAME { get; set; }
        public string EFGDIE_CONSIGNEE_ADDRESS { get; set; }
        public string EFGDGI_FORM_E_NO { get; set; }
    }
}

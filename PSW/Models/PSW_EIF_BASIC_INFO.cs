//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSW.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class PSW_EIF_BASIC_INFO
    {
        public int EIF_BI_ID { get; set; }
        public string EIF_BI_EIF_REQUEST_NO { get; set; }
        public Nullable<System.DateTime> EIF_BI_REQUEST_DATE { get; set; }
        [Required(ErrorMessage = "NTN Number is required !!")]
        [StringLength(50, ErrorMessage = "Maximum Length is 50 !!")]
        public string EIF_BI_NTN { get; set; }
        [Required(ErrorMessage = "Select Bussiness Name !!")]
        [StringLength(50, ErrorMessage = "Maximum Length is 50 !!")]
        public string EIF_BI_BUSSINESS_NAME { get; set; }
        [StringLength(200, ErrorMessage = "Maximum Length is 200 !!")]
        public string EIF_BI_BUSSINESS_ADDRESS { get; set; }
        public bool EIF_BI_IS_REMITTANCE_FROM_PAKISTAN { get; set; }
        public string EIF_BI_APPROVAL_STATUS { get; set; }
        public Nullable<System.DateTime> EIF_BI_STATUS_DATE { get; set; }
        [StringLength(50, ErrorMessage = "Maximum Length is 50 !!")]
        public string EIF_BI_STRN { get; set; }
        [Range(1, 100, ErrorMessage = "Select Import Payment Mode")]
        public Nullable<int> EIF_BI_MODE_OF_IMPORT_PAYMENT { get; set; }
        public Nullable<System.DateTime> EIF_BI_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> EIF_BI_EDIT_DATETIME { get; set; }
        public string EIF_BI_MAKER_ID { get; set; }
        public string EIF_BI_CHECKER_ID { get; set; }
        public string EIF_BI_INTERNAL_REF { get; set; }
        public int EIF_BI_AMENDMENT_NO { get; set; }
        public Nullable<System.DateTime> EIF_BI_CERTIFICATION_DATE { get; set; }
        public bool EIF_BI_CERTIFIED { get; set; }
        public bool EIF_BI_DOC_SUBMISSION { get; set; }
    }
}

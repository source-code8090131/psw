//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSW.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class PSW_FORM_E_DOCUMENT_REQUEST_INFO
    {
        public int FDRI_ID { get; set; }
        public string FDRI_FORM_E_NO { get; set; }
        [Required(ErrorMessage = "Consignee Name is required !!")]
        [StringLength(50, ErrorMessage = "Maximum Length is 50 !!")]
        public string FDRI_CONSIG_NAME { get; set; }
        [Required(ErrorMessage = "Consignee Address is required !!")]
        public string FDRI_CONSID_ADDRESS { get; set; }
        [StringLength(50, ErrorMessage = "Maximum Length is 50 !!")]
        public string FDRI_CONSIGNEE_IBAN { get; set; }
        [Required(ErrorMessage = "Select Country")]
        [Range(1, 1000, ErrorMessage = "Select Country")]
        public Nullable<int> FDRI_COUNTRY { get; set; }
        [Required(ErrorMessage = "Port of Discharge is required !!")]
        [StringLength(50, ErrorMessage = "Maximum Length is 50 !!")]
        public string FDRI_PORT_OF_DISCHARGE { get; set; }
        [Required(ErrorMessage = "Select Curreny")]
        [Range(1, 1000, ErrorMessage = "Select Curreny")]
        public Nullable<int> FDRI_CURRENCY { get; set; }
        [Required(ErrorMessage = "Select Delivery Term")]
        [Range(1, 1000, ErrorMessage = "Select Delivery Term")]
        public Nullable<int> FDRI_DELIVERY_TERMS { get; set; }
        [Required(ErrorMessage = "Total Value of Shipment is required !!")]
        [DisplayFormat(DataFormatString = "{0:0.00000}")]
        public Nullable<decimal> FDRI_TOTAL_VALUE_OF_SHIPMENT { get; set; }
        [Required(ErrorMessage = "Expiry Date is required !!")]
        public Nullable<System.DateTime> FDRI_FINAN_EXPIRY { get; set; }
        public Nullable<System.DateTime> FDRI_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> FDRI_EDIT_DATETIME { get; set; }
        public string FDRI_MAKER_ID { get; set; }
        public string FDRI_CHECKER_ID { get; set; }
        public int FDRI_AMENDMENT_NO { get; set; }
    }
}

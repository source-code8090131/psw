//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSW.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSW_ITRS_BALANCES
    {
        public int IB_ID { get; set; }
        public Nullable<System.DateTime> IB_UPLOAD_MONTH { get; set; }
        public string IB_CURRENCY { get; set; }
        public string IB_STATEMENT { get; set; }
        public Nullable<decimal> IB_OPENING_BALANCE_OF_S1_DR { get; set; }
        public Nullable<decimal> IB_OPENING_BALANCE_OF_S1_CR { get; set; }
        public Nullable<decimal> IB_CLOSING_BALANCE_OF_S1_DR { get; set; }
        public Nullable<decimal> IB_CLOSING_BALANCE_OF_S1_CR { get; set; }
        public Nullable<decimal> IB_OPENING_BALANCE_OF_S4_DR { get; set; }
        public Nullable<decimal> IB_OPENING_BALANCE_OF_S4_CR { get; set; }
        public Nullable<decimal> IB_CLOSING_BALANCE_OF_S4_DR { get; set; }
        public Nullable<decimal> IB_CLOSING_BALANCE_OF_S4_CR { get; set; }
        public Nullable<decimal> IB_OPENING_BALANCE_OF_S6 { get; set; }
        public Nullable<decimal> IB_CLOSING_BALANCE_OF_S6 { get; set; }
        public string IB_MAKER_ID { get; set; }
        public string IB_CHECKER_ID { get; set; }
        public Nullable<System.DateTime> IB_DATA_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> IB_DATA_EDIT_DATETIME { get; set; }
        public Nullable<bool> IB_ISAUTH { get; set; }
        public string IB_AD { get; set; }
    }
}

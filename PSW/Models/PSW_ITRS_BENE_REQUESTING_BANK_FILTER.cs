﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class PSW_ITRS_BENE_REQUESTING_BANK_FILTER
    {
        public PSW_ITRS_BENEFICIARY_REQUESTING_BANK Entity { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class PSW_ITRS_LENDER_SECTOR_FILTER
    {
        public PSW_ITRS_LENDER_SECTOR Entity { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
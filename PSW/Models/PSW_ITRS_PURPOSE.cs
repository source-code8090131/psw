//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSW.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSW_ITRS_PURPOSE
    {
        public int IPC_ID { get; set; }
        public Nullable<int> IPC_PURPOSE_ID { get; set; }
        public string IPC_PURPOSE_NAME { get; set; }
        public string IPC_PURPOSE_DESCRIPTION { get; set; }
        public Nullable<bool> IPC_STATUS { get; set; }
        public string IPC_MAKER_ID { get; set; }
        public string IPC_CHECKER_ID { get; set; }
        public Nullable<System.DateTime> IPC_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> IPC_EDIT_DATETIME { get; set; }
        public Nullable<bool> IPC_ISAUTH { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class PSW_ITRS_RELATIONSHIP_WITH_BENE_FILTER
    {
        public PSW_ITRS_RELATIONSHIP_WITH_BENEFICIARY Entity { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
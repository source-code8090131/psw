//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSW.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class PSW_PORT_OF_SHIPPING
    {
        public int POS_ID { get; set; }
        [Required(ErrorMessage = "Select Country")]
        public string POS_COUNTRY_CODE { get; set; }
        [Required(ErrorMessage = "Port of Shipping Name is required !!")]
        public string POS_NAME { get; set; }
        [Required(ErrorMessage = "Port of Shipping Code is required !!")]
        public string POS_CODE { get; set; }
        [Required(ErrorMessage = "Port of Shipping Port Type Code is required !!")]
        public string POS_PORT_TYPE_CODE { get; set; }
        public string POS_MAKER_ID { get; set; }
        public string POS_CHECKER_ID { get; set; }
        public Nullable<System.DateTime> POS_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> POS_EDIT_DATETIME { get; set; }
        public Nullable<bool> POS_ISAUTH { get; set; }
    }
}

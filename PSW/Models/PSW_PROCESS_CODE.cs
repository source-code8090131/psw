//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSW.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSW_PROCESS_CODE
    {
        public int PC_ID { get; set; }
        public string PC_NAME { get; set; }
        public string PC_DETAIL { get; set; }
        public Nullable<System.DateTime> PC_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> PC_EDIT_DATETIME { get; set; }
        public string PC_MAKER_ID { get; set; }
        public string PC_CHECKER_ID { get; set; }
        public Nullable<bool> PC_STATUS { get; set; }
        public Nullable<bool> PC_ISAUTH { get; set; }
    }
}

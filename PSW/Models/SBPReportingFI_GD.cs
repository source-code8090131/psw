﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class SBPReportingFI_GD
    {
        public List<PSW_EIF_MASTER_DETAILS> FI_IMPORT { get; set; }
        public List<PSW_EIF_GD_GENERAL_INFO_LIST> GD_IMPORT { get; set; }
        public List<PSW_EEF_MASTER_DETAILS> FI_EXPORT { get; set; }
        public List<PSW_EEF_GD_GENERAL_INFO_LIST> GD_EXPORT { get; set; }
    }
}
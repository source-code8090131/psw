﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class SettlementOfFinInstrumentByAD
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public SettleDetailData data { get; set; }
        public string signature { get; set; }

        public class SettleDetailData
        {
            public string tradeTranType { get; set; }
            public string traderNTN { get; set; }
            public string traderName { get; set; }
            public string finInsUniqueNumber { get; set; }
            public List<string> bcaBdaUniqueIdNumber { get; set; }
            public decimal finInsValue { get; set; }
            public decimal totalValueOfSharedBCABDA { get; set; }
            public decimal balance { get; set; }
        }
    }
}
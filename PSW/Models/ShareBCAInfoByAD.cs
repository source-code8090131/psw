﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareBCAInfoByAD
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public BCADetailData data { get; set; }
        public string signature { get; set; }

        public class BCADetailData
        {
            public string bcaUniqueIdNumber { get; set; }
            public string iban { get; set; }
            public string gdNumber { get; set; }
            public string exporterNtn { get; set; }
            public string exporterName { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public DetailBCAInformation bcaInformation { get; set; }
            public DetailDeductions deductions { get; set; }
            public DetailNetAmountRealized netAmountRealized { get; set; }
            public string remarks { get; set; }


            public class DetailBCAInformation
            {
                public string bcaEventName { get; set; }
                public string eventDate { get; set; }
                public int runningSerialNumber { get; set; }
                public string swiftReference { get; set; }
                public string billNumber { get; set; }
                public string billDated { get; set; }
                public decimal billAmount { get; set; }
                public string invoiceNumber { get; set; }
                public string invoiceDate { get; set; }
                public decimal invoiceAmount { get; set; }
            }
            public class DetailDeductions
            {
                public decimal foreignBankChargesFcy { get; set; }
                public decimal agentCommissionFcy { get; set; }
                public decimal withholdingTaxPkr { get; set; }
                public decimal edsPkr { get; set; }
                public string edsUniqueReferenceNumber { get; set; }
            }
            public class DetailNetAmountRealized
            {
                public string isFinInsCurrencyDiff { get; set; }
                public string currency { get; set; }
                public decimal fcyExchangeRate { get; set; }
                public decimal bcaPkr { get; set; }
                public decimal bcaFc { get; set; }
                public string dateOfRealized { get; set; }
                public decimal adjustFromSpecialFcyAcc { get; set; }
                public string isRemAmtSettledWithDiscount { get; set; }
                public decimal allowedDiscount { get; set; }
                public int allowedDiscountPercentage { get; set; }
                public decimal totalBcaAmount { get; set; }
                public decimal amountRealized { get; set; }
                public decimal balance { get; set; }
            }
        }
    }
}
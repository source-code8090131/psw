﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareBDAInfoByAD
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public BDADetailData data { get; set; }
        public string signature { get; set; }

        public class BDADetailData
        {
            public string bdaUniqueIdNumber { get; set; }
            public string gdNumber { get; set; }
            public string iban { get; set; }
            public string importerNtn { get; set; }
            public string importerName { get; set; }
            public string bdaDate { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public DetailBDAInformation bdaInformation { get; set; }

            public class DetailBDAInformation
            {
                public decimal? totalBdaAmountFcy { get; set; }
                public string totalBdaAmountCurrency { get; set; }
                public decimal? sampleAmountExclude { get; set; }
                public string sampleAmountCurrency { get; set; }
                public decimal? netBdaAmountFcy { get; set; }
                public string netBdaAmountCurrency { get; set; }

                public decimal? exchangeRateFiFcy { get; set; }
                public decimal? netBdaAmountPkr { get; set; }
                public string amountInWords { get; set; }
                public string currencyFcy { get; set; }
                public decimal? exchangeRateFcy { get; set; }
                public decimal? bdaAmountFcy { get; set; }
                public decimal? bdaAmountPkr { get; set; }
                public string bdaDocumentRefNumber { get; set; }
                public decimal? commisionAmountFcy { get; set; }
                public decimal? commisionAmountPkr { get; set; }
                public decimal? fedFcy { get; set; }
                public decimal? fedAmountPkr { get; set; }
                public decimal? swiftChargesPkr { get; set; }
                public decimal? otherChargesPkr { get; set; }
                public string remarks { get; set; }
                public decimal? balanceBdaAmountFcy { get; set; }
            }
        }
    }
   
}
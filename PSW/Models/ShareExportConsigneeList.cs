﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareExportConsigneeList
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public ShareConsigneeName data { get; set; }
        public string signature { get; set; }

        public class ShareConsigneeName
        {
            public string ntn { get; set; }
            public string iban { get; set; }
            public string finInsUniqueNumber { get; set; }
            public List<string> consignee { get; set; }
        }
    }
}
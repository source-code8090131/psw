﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareFinancialNGDInfoCashMargin
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public DetailData data { get; set; }
        public string signature { get; set; }
        public class DetailData
        {
            public string importerNtn { get; set; }
            public string importerName { get; set; }
            public string importerIban { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public DetailCashMargin cashMargin { get; set; }
            public DetailPaymentInformation paymentInformation { get; set; }
            public List<DetailItemInformation> itemInformation { get; set; }
            public DetailFinancialTranInformation financialTranInformation { get; set; }
            public string remarks { get; set; }
        }
        public class DetailCashMargin
        {
            public int cashmarginPercentage { get; set; }
            public int cashmarginValue { get; set; }
        }

        public class DetailOpenAccountData
        {
            public string gdNumber { get; set; }
        }

        public class DetailPaymentInformation
        {
            public string beneficiaryName { get; set; }
            public string beneficiaryAddress { get; set; }
            public string beneficiaryCountry { get; set; }
            public string beneficiaryIban { get; set; }
            public string exporterName { get; set; }
            public string exporterAddress { get; set; }
            public string exporterCountry { get; set; }
            public string portOfShipment { get; set; }
            public string deliveryTerms { get; set; }
            public decimal financialInstrumentValue { get; set; }
            public string financialInstrumentCurrency { get; set; }
            public decimal exchangeRate { get; set; }
            public string lcContractNo { get; set; }
        }
        public class DetailItemInformation
        {
            public string hsCode { get; set; }
            public string goodsDescription { get; set; }
            public decimal quantity { get; set; }
            public string uom { get; set; }
            public string countryOfOrigin { get; set; }
            public string sample { get; set; }
            public decimal? sampleValue { get; set; }
            public decimal itemInvoiceValue { get; set; }
        }
        public class DetailFinancialTranInformation
        {
            public string intendedPayDate { get; set; }
            public string transportDocDate { get; set; }
            public string finalDateOfShipment { get; set; }
            public string expiryDate { get; set; }
        }
    }
}
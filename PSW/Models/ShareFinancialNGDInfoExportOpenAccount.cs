﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareFinancialNGDInfoExportOpenAccount
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public FinDetailData data { get; set; }
        public string signature { get; set; }

        public class FinDetailData
        {
            public string exporterNtn { get; set; }
            public string exporterName { get; set; }
            public string exporterIban { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public DetailOpenAccountData openAccountData { get; set; }
            public DetailPaymentInformation paymentInformation { get; set; }
            public List<DetailItemInformation> itemInformation { get; set; }

        }
        public class DetailOpenAccountData
        {
            public string gdNumber { get; set; }
        }
        public class DetailPaymentInformation
        {
            public string consigneeName { get; set; }
            public string consigneeAddress { get; set; }
            public string consigneeCountry { get; set; }
            public string consigneeIban { get; set; }
            public string portOfDischarge { get; set; }
            public string deliveryTerms { get; set; }
            public string financialInstrumentCurrency { get; set; }
            public decimal financialInstrumentValue { get; set; }
            public string expiryDate { get; set; }
        }
        public class DetailItemInformation
        {
            public string hsCode { get; set; }
            public string goodsDescription { get; set; }
            public decimal quantity { get; set; }
            public string uom { get; set; }
            public decimal itemInvoiceValue { get; set; }
            public string countryOfOrigin { get; set; }
        }
    }
}
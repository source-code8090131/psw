﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareGDClearanceMessageByAD
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public ClearanceDetailData data { get; set; }
        public string signature { get; set; }

        public class ClearanceDetailData
        {
            public string tradeTranType { get; set; }
            public string traderNTN { get; set; }
            public string traderName { get; set; }
            public string iban { get; set; }
            public string gdNumber { get; set; }
            public string clearanceStatus { get; set; }
        }
    }
}
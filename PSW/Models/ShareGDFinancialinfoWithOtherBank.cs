﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareGDFinancialinfoWithOtherBank
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string processingCode { get; set; }
        public DetailData data { get; set; }
        public DetailFinancialInstrumentInfo financialInstrumentInfo { get; set; }
        public List<DetailGDInfo> gdInfo { get; set; }
        public List<DetailBankAdviceInfo> bankAdviceInfo { get; set; }
        public string signature { get; set; }
        public class DetailData
        {
            public string cobUniqueIdNumber { get; set; }
            public string tradeTranType { get; set; }
            public FinInstrumentInfo financialInstrumentInfo { get; set; }
            public List<GDInfo> gdInfo { get; set; }
            public BankAdviceInfo bankAdviceInfo { get; set; }
        }
        public class DetailFinancialInstrumentInfo
        {
            public string exporterNtn { get; set; }
            public string exporterName { get; set; }
            public string exporterIban { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public ContractCollectionData contractCollectionData { get; set; }
            public LCsData lcData { get; set; }
            public PaymentInformation paymentInformation { get; set; }
            public List<ItemInformations> itemInformation { get; set; }
        }
        public class DetailGDInfo
        {
            public string gdNumber { get; set; }
            public string gdStatus { get; set; }
            public string consignmentCategory { get; set; }
            public string gdType { get; set; }
            public string collectorate { get; set; }
            public string blAwbNumber { get; set; }
            public string blAwbDate { get; set; }
            public string virAirNumber { get; set; }
            public ConsigConsigneeInfo consignorConsigneeInfo { get; set; }
            public FinancialInformation financialInformation { get; set; }
            public GeneralInformation generalInformation { get; set; }
            public List<ItemInfomations> itemInformation { get; set; }
        }
        public class DetailBankAdviceInfo
        {
            public string bcaUniqueIdNumber { get; set; }
            public string iban { get; set; }
            public List<string> gdNumber { get; set; }
            public string exporterNtn { get; set; }
            public string exporterName { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public BCAInformation bcaInformation { get; set; }
            public Deductions deductions { get; set; }
            public NetAmountRealized netAmountRealized { get; set; }
            public string remarks { get; set; }
        }
        public class FinInstrumentInfo
        {
            public string importerNtn { get; set; }
            public string importerName { get; set; }
            public string importerIban { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public ContractCollData contractCollectionData { get; set; }
            public LCData lcData { get; set; }
            public CashMargins cashMargin { get; set; }
            public PayInformation paymentInformation { get; set; }
            public List<ItemInfo> itemInformation { get; set; }
            public FinTranInfo financialTranInformation { get; set; }
            public string remarks { get; set; }
        }
        public class GDInfo
        {
            public string gdNumber { get; set; }
            public string gdStatus { get; set; }
            public string consignmentCategory { get; set; }
            public string gdType { get; set; }
            public string collectorate { get; set; }
            public string blAwbNumber { get; set; }
            public string blAwbDate { get; set; }
            public string virAirNumber { get; set; }
            public ConsignorConsigneeInfo consignorConsigneeInfo { get; set; }
            public FinancialInfo financialInformation { get; set; }
            public GeneralInfo generalInformation { get; set; }
            public List<ItemInformation> itemInformation { get; set; }
        }
        public class BankAdviceInfo
        {
            public string bdaUniqueIdNumber { get; set; }
            public List<string> gdNumber { get; set; }
            public string iban { get; set; }
            public string importerNtn { get; set; }
            public string importerName { get; set; }
            public string bdaDate { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public BDAInformation bdaInformation { get; set; }
        }
        public class ContractCollData
        {
            public string advPayPercentage { get; set; }
            public string docAgainstPayPercentage { get; set; }
            public string docAgainstAcceptancePercentage { get; set; }
            public string days { get; set; }
            public string totalPercentage { get; set; }
        }
        public class LCData
        {
            public string advPayPercentage { get; set; }
            public string sightPercentage { get; set; }
            public string usancePercentage { get; set; }
            public string days { get; set; }
            public string totalPercentage { get; set; }
        }
        public class CashMargins
        {
            public string cashmarginPercentage { get; set; }
            public string cashmarginValue { get; set; }
        }
        public class PayInformation
        {
            public string beneficiaryName { get; set; }
            public string beneficiaryAddress { get; set; }
            public string beneficiaryCountry { get; set; }
            public string beneficiaryIban { get; set; }
            public string exporterName { get; set; }
            public string exporterAddress { get; set; }
            public string exporterCountry { get; set; }
            public string portOfShipment { get; set; }
            public string deliveryTerms { get; set; }
            public string financialInstrumentValue { get; set; }
            public string financialInstrumentCurrency { get; set; }
            public string exchangeRate { get; set; }
            public string lcContractNo { get; set; }
        }
        public class ItemInfo
        {
            public string hsCode { get; set; }
            public string goodsDescription { get; set; }
            public string quantity { get; set; }
            public string uom { get; set; }
            public string countryOfOrigin { get; set; }
            public string sample { get; set; }
            public string sampleValue { get; set; }
        }
        public class FinTranInfo
        {
            public string intendedPayDate { get; set; }
            public string transportDocDate { get; set; }
            public string finalDateOfShipment { get; set; }
            public string expiryDate { get; set; }
        }
        public class ConsignorConsigneeInfo
        {
            public string ntnFtn { get; set; }
            public string strn { get; set; }
            public string consigneeName { get; set; }
            public string consigneeAddress { get; set; }
            public string consignorName { get; set; }
            public string consignorAddress { get; set; }
        }
        public class FinancialInfo
        {
            public string exporterIban { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public string currency { get; set; }
            public string totalDeclaredValue { get; set; }
            public string invoiceNumber { get; set; }
            public string invoiceDate { get; set; }
            public string deliveryTerm { get; set; }
            public string fobValueUsd { get; set; }
            public string freightUsd { get; set; }
            public string cfrValueUsd { get; set; }
            public string insuranceUsd { get; set; }
            public string landingChargesUsd { get; set; }
            public string assessedValueUsd { get; set; }
            public string OtherCharges { get; set; }
            public string exchangeRate { get; set; }
        }
        public class GeneralInfo
        {
            public List<PackagesInfo> packagesInformation { get; set; }
            public List<ContainerVehicleInfo> containerVehicleInformation { get; set; }
            public string netWeight { get; set; }
            public string grossWeight { get; set; }
            public string consignmentType { get; set; }
            public string portOfShipment { get; set; }
            public string placeOfDelivery { get; set; }
            public string portOfDischarge { get; set; }
            public string terminalLocation { get; set; }
            public string shippingLine { get; set; }
        }
        public class ItemInformation
        {
            public string hsCode { get; set; }
            public string quantity { get; set; }
            public string unitPrice { get; set; }
            public string totalValue { get; set; }
            public string exportValue { get; set; }
            public string uom { get; set; }
        }
        public class PackagesInfo
        {
            public string numberOfPackages { get; set; }
            public string packageType { get; set; }
        }
        public class ContainerVehicleInfo
        {
            public string containerOrTruckNumber { get; set; }
            public string sealNumber { get; set; }
            public string containerType { get; set; }
        }
        public class BDAInformation
        {
            public string totalBdaAmountFcy { get; set; }
            public string totalBdaAmountCurrency { get; set; }
            public string sampleAmountExclude { get; set; }
            public string sampleAmountCurrency { get; set; }
            public string netBdaAmountFcy { get; set; }
            public string netBdaAmountCurrency { get; set; }
            public string exchangeRateFcy { get; set; }
            public string netBdaAmountPkr { get; set; }
            public string amountInWords { get; set; }
            public string currencyFcy { get; set; }
            public string bdaAmountFcy { get; set; }
            public string bdaAmountPkr { get; set; }
            public string bdaDocumentRefNumber { get; set; }
            public string commisionAmountFcy { get; set; }
            public string commisionAmountPkr { get; set; }
            public string fedFcy { get; set; }
            public string fedAmountPkr { get; set; }
            public string swiftChargesPkr { get; set; }
            public string otherChargesPkr { get; set; }
            public string remarks { get; set; }
            public string balanceBdaAmountFcy { get; set; }
        }
        public class ContractCollectionData
        {
            public string advPayPercentage { get; set; }
            public string docAgainstPayPercentage { get; set; }
            public string docAgainstAcceptancePercentage { get; set; }
            public string days { get; set; }
            public string totalPercentage { get; set; }
        }
        public class LCsData
        {
            public string advPayPercentage { get; set; }
            public string sightPercentage { get; set; }
            public string usancePercentage { get; set; }
            public string days { get; set; }
            public string totalPercentage { get; set; }
        }
        public class PaymentInformation
        {
            public string consigneeName { get; set; }
            public string consigneeAddress { get; set; }
            public string consigneeCountry { get; set; }
            public string consigneeIban { get; set; }
            public string portOfDischarge { get; set; }
            public string deliveryTerms { get; set; }
            public string financialInstrumentCurrency { get; set; }
            public string financialInstrumentValue { get; set; }
            public string expiryDate { get; set; }
        }
        public class ItemInformations
        {
            public string hsCode { get; set; }
            public string goodsDescription { get; set; }
            public string quantity { get; set; }
            public string uom { get; set; }
            public string countryOfOrigin { get; set; }
            public string itemInvoiceValue { get; set; }
        }
        public class ConsigConsigneeInfo
        {
            public string ntnFtn { get; set; }
            public string strn { get; set; }
            public string consigneeName { get; set; }
            public string consigneeAddress { get; set; }
            public string consignorName { get; set; }
            public string consignorAddress { get; set; }
        }
        public class FinancialInformation
        {
            public string exporterIban { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public string currency { get; set; }
            public string totalDeclaredValue { get; set; }
            public string invoiceNumber { get; set; }
            public string invoiceDate { get; set; }
            public string deliveryTerm { get; set; }
            public string fobValueUsd { get; set; }
            public string freightUsd { get; set; }
            public string cfrValueUsd { get; set; }
            public string insuranceUsd { get; set; }
            public string landingChargesUsd { get; set; }
            public string assessedValueUsd { get; set; }
            public string OtherCharges { get; set; }
            public string exchangeRate { get; set; }
        }
        public class GeneralInformation
        {
            public List<PackagesInformation> packagesInformation { get; set; }
            public List<ContainerVehicleInformation> containerVehicleInformation { get; set; }
            public string netWeight { get; set; }
            public string grossWeight { get; set; }
            public string consignmentType { get; set; }
            public string portOfShipment { get; set; }
            public string placeOfDelivery { get; set; }
            public string portOfDischarge { get; set; }
            public string terminalLocation { get; set; }
            public string shippingLine { get; set; }
        }
        public class PackagesInformation
        {
            public string numberOfPackages { get; set; }
            public string packageType { get; set; }
        }
        public class ContainerVehicleInformation
        {
            public string containerOrTruckNumber { get; set; }
            public string sealNumber { get; set; }
            public string containerType { get; set; }
        }
        public class ItemInfomations
        {
            public string hsCode { get; set; }
            public string quantity { get; set; }
            public string unitPrice { get; set; }
            public string totalValue { get; set; }
            public string exportValue { get; set; }
            public string uom { get; set; }
        }
        public class BCAInformation
        {
            public string bcaEventName { get; set; }
            public string eventDate { get; set; }
            public string runningSerialNumber { get; set; }
            public string swiftReference { get; set; }
            public string billNumber { get; set; }
            public string billDated { get; set; }
            public string billAmount { get; set; }
            public string invoiceNumber { get; set; }
            public string invoiceDate { get; set; }
            public string invoiceAmount { get; set; }
            public string MyProperty { get; set; }
        }
        public class Deductions
        {
            public string foreignBankChargesFcy { get; set; }
            public string agentCommissionFcy { get; set; }
            public string withholdingTaxPkr { get; set; }
            public string edsPkr { get; set; }
        }
        public class NetAmountRealized
        {
            public string bcaFc { get; set; }
            public string fcyExchangeRate { get; set; }
            public string bcaPkr { get; set; }
            public string dateOfRealized { get; set; }
            public string adjustFromSpecialFcyAcc { get; set; }
            public string isFinInsCurrencyDiff { get; set; }
            public string currency { get; set; }
            public string isRemAmtSettledWithDiscount { get; set; }
            public string totalBcaAmount { get; set; }
            public string amountRealized { get; set; }
            public string balance { get; set; }
            public string allowedDiscount { get; set; }
            public string allowedDiscountPercentage { get; set; }
        }
    }
}
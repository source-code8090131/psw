﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareNegativeListOfCommodityByAD
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public ShareNegativeDetailData data { get; set; }
        public string signature { get; set; }

        public class ShareNegativeDetailData
        {
            public string bankCode { get; set; }
            public List<string> restrictedCommoditiesForImport { get; set; }
            public List<string> restrictedCommoditiesForExport { get; set; }
        }
    }
}
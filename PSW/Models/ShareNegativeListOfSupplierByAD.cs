﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareNegativeListOfSupplierByAD
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public SupplierDetailData data { get; set; }
        public string signature { get; set; }

        public class SupplierDetailData
        {
            public string bankCode { get; set; }
            public List<string> restrictedSuppliersForImport { get; set; }
            public List<string> restrictedSuppliersForExport { get; set; }
        }


    }
    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class ShareUpdatedAuthPayModes
    {

        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public DetailData data { get; set; }
        public string signature { get; set; }



        public class DetailData
        {
            public string iban { get; set; }
            public List<string> authorizedPaymentModesForImport { get; set; }
            public List<string> authorizedPaymentModesForExport { get; set; }

        }

    }
}
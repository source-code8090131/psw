﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class TokenPSWKeys
    {
        public string client_id { get; set;}

        public string client_secret { get; set; }

        public string grant_type { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSW.Models
{
    public class WebApiMessageFilter
    {
        public IEnumerable<PSW_MESSAGES_VIEW> Entity { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
﻿// #region CUSTOM ALIDATIONS

// Decimal Input Only
function DecimalsOnly(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
// Decimal Input Only


$('#EEFHsCodes').on('update', function () {
    GetSUMHsCodesEEF();
});


function GetSUMHsCodesEEF() {
    var REQ = $("#DocInfo_FDI_FORM_E_NO").val();
    if (REQ != 0)
    {
        $.ajax({
            type: "POST",
            url: "/EEFRequest/GetHsCodeSUM?E=" + REQ,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                $("#DocRequestNo_FDRI_TOTAL_VALUE_OF_SHIPMENT").val(data.InVoiceAmount.toFixed(2));
            }
        });
    }
}





// #endregion



$(function () {

    $(document).ready(function () {
        if ($('#EEFHsCodes').length)
        {
            GetSUMHsCodesEEF();
        }
        if ($('#basic_EIF_BI_BUSSINESS_NAME').length) {
            GetSelectedParametersImport();
        }
        if ($('#DocInfo_FDI_TRADER_NAME').length) {
            GetSelectedParametersExport();
        }

        if ($('#EIF_BDA_EIF_REQUEST_NO').length) {
            GetEIFInformationForBDA();
        }

        if ($('#BCAD_FORM_E_NO').length) {
            GetEEFInformationForBCA();
        }

        if ($('#searchIsSample').length) {
            $('#searchIsSample').prop("checked", false);  
            IsSampleImport(this);
        }

        if ($('#DocRequestNo_FDRI_TOTAL_VALUE_OF_SHIPMENT').length) {
            EEFCalculation();
        }

    });

    $("#BCAD_FORM_E_NO").change(function () {
        GetEEFInformationForBCA();
    });

    $("#BCAD_ALLOWED_DISCOUNT").change(function () {
        GetDiscountPercentEEF();
    });

    $("#BCAD_ALLOWED_DISCOUNT_PERCENT").change(function () {
        GetDiscountValueEEF();
    });

    function GetDiscountPercentEEF()
    {
        var ToTalValue = $("#BCAD_FORM_AMOUNT_REALIZED").val();
        var AmountInserted = $("#BCAD_ALLOWED_DISCOUNT").val();
        var DiscountPercent = parseFloat(parseFloat(parseFloat(AmountInserted) / parseFloat(ToTalValue)) * 100);
        $("#BCAD_ALLOWED_DISCOUNT_PERCENT").val(DiscountPercent.toFixed(2));
    }


    function GetDiscountValueEEF()
    {
        var ToTalValue = $("#BCAD_FORM_AMOUNT_REALIZED").val();
        var PercentInserted = $("#BCAD_ALLOWED_DISCOUNT_PERCENT").val();
        var DicountAmount = parseFloat(parseFloat(parseFloat(PercentInserted) / parseFloat(100)) * parseFloat(ToTalValue)).toFixed(2);
        $("#BCAD_ALLOWED_DISCOUNT").val(DicountAmount);
    }



    $("#BCAD_FCY_EXCHANGE_RATE").change(function () {
        BCACalculation();
    });

    $("#BCAD_BCA_FC").change(function () {
        BCACalculation();
    });

    $("#EIF_BDA_EIF_REQUEST_NO").change(function () {
            GetEIFInformationForBDA();
    });

    $("#EIF_BDA_SAMPLE_AMOUNT_EXCLUDE").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_AMOUNT_FCY").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_EXCHANGE_RATE_FCY").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_COMMMISSION_FCY").change(function () {
        BDAOnPageCalculations();
    });

    $("#EIF_BDA_FED_AMOUNT_FCY").change(function () {
        BDAOnPageCalculations();
    });





    $("#basic_EIF_BI_BUSSINESS_NAME").change(function () {
        var EIF_BusinessName = $("#basic_EIF_BI_BUSSINESS_NAME option:selected").val();
        GetAuthorizePayModesIMPORT(EIF_BusinessName);
        GetBusinessDetailsIMPORT(EIF_BusinessName);
    });

    $("#DocInfo_FDI_TRADER_NAME").change(function () {
        var EIF_BusinessName = $("#DocInfo_FDI_TRADER_NAME option:selected").val();
        GetAuthorizePayModesEXPORT(EIF_BusinessName);
        GetBusinessDetailsEXPORT(EIF_BusinessName);
    });

    $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT").change(function () {
        GetSelectedParametersImport();
    });

  
    $('#searchIsSample').change(function () {
        IsSampleImport(this);
    });

    $('#searchIsSampleEEF').change(function () {
        IsSampleExport(this);
    });

    function IsSampleImport(elem)
    {
        if (elem.checked) {
            $(this).prop("checked", true);
            $("#searchSampleValue").attr('readonly', false);
            $("#searchSampleValue").val('0.00');
        }
        else {
            $(elem).prop("checked", false);
            $("#searchSampleValue").attr('readonly', true);
            $("#searchSampleValue").val('0.00');
        }
        $('#searchIsSample').val(elem.checked);
    }

    function GetSelectedParametersImport()
    {
        if ($("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT option:selected").val() != 0)
        {
            var ModeOfImportPayment = $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT option:selected").text();
            var code = ModeOfImportPayment.substr(ModeOfImportPayment.length - 3);
            switch (code) {
                case "301":
                    $("#payparamifLC301").show();
                    $("#payparamifCont304").hide();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").show();
                    $("#payparamcommisionCharges").show();
                    break;
                case "302":
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").hide();
                    $("#payparamifcashmargin").show();
                    $("#payparamDays").hide();
                    break;
                case "303":
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").show();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").show();
                    $("#payparamcommisionCharges").show();
                    break;
                case "304":
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").show();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").show();
                    $("#payparamcommisionCharges").show();
                    break;
                case "309":
                    $("#payparamifLC301").show();
                    $("#payparamifCont304").hide();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").show();
                    $("#payparamcommisionCharges").show();
                    break;
                default:
                    alert("Invalid Payment Mode Selected");
                    $("#payparamifLC301").hide();
                    $("#payparamifCont304").hide();
                    $("#payparamifcashmargin").hide();
                    $("#payparamDays").hide();
                    $("#payparamcommisionCharges").show();
                    break;
            }
        }
    }


    $("#documents_EIF_DRI_TOTAL_INVOICE_VALUE").change(function () {
        var BusId = $("#basic_EIF_BI_BUSSINESS_NAME option:selected").val();
        var InvVal = $("#documents_EIF_DRI_TOTAL_INVOICE_VALUE").val();
        var ModeOfImportPayment = $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT option:selected").text();
        var Paycode = ModeOfImportPayment.substr(ModeOfImportPayment.length - 3);
        if (BusId != 0 && InvVal > 0 && Paycode != "") {
            PerformCalculationImport(BusId, InvVal, Paycode);
        }
    });

    function PerformCalculationImport(BusId, InvVal, Paycode) {
            $.ajax({
                type: "POST",
                url: "/EIFRequest/GetCharges",
                data: { BID: BusId, IVal: InvVal, PM: Paycode },
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $("#PayParam_EIF_PMP_COMMISION_CHARGES").val(data.CommCharges);
                    $("#PayParam_EIF_PMP_FED_CHARGES").val(data.FEDCharges);   
                }
            }); 
    }



    $("#DocRequestNo_FDRI_TOTAL_VALUE_OF_SHIPMENT").change(function () {
        EEFCalculation();
    });

    function EEFCalculation()
    {

        var BusId = $("#DocInfo_FDI_TRADER_NAME option:selected").val();
        var InvVal = $("#DocRequestNo_FDRI_TOTAL_VALUE_OF_SHIPMENT").val();
        var ModeOfExportPayment = $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT option:selected").text();
        var RequestNo = $("#DocInfo_FDI_FORM_E_NO").val();
        var Paycode = ModeOfExportPayment.substr(ModeOfExportPayment.length - 3);
        alert(BusId + "---" + InvVal + "----" + Paycode);
        if (BusId != 0 && InvVal > 0 && Paycode != "") {
            PerformCalculationExport(BusId, InvVal, Paycode, RequestNo);
        }
    }

    function PerformCalculationExport(BusId, InvVal, Paycode,Requ) {
        $.ajax({
            type: "POST",
            url: "/EEFRequest/GetCharges",
            data: { BID: BusId, IVal: InvVal, PM: Paycode, Req: Requ },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {

                $("#PayParam_EIF_PMP_COMMISION_CHARGES").val(data.CommCharges);
                $("#PayParam_EIF_PMP_FED_CHARGES").val(data.FEDCharges);
                $("#DocRequestNo_FDRI_TOTAL_VALUE_OF_SHIPMENT").val(data.InVoiceAmount);
            }
        });
    }


    $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT").change(function () {
        GetSelectedParametersExport();
    });

    function GetSelectedParametersExport()
    {
        if ($("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT option:selected").val() != 0)
        {
            var ModeOfExportPayment = $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT option:selected").text();
            var code = ModeOfExportPayment.substr(ModeOfExportPayment.length - 3);
            switch (code) {
                case "307":
                    $("#payparamifLC307").show();
                    $("#payparamifCont308").hide();
                    $("#payparamDays").show();
                    $("#payparamcommisionChargesExport").show();
                    break;
                case "308":
                    $("#payparamifLC307").hide();
                    $("#payparamifCont308").show();
                    $("#payparamcommisionChargesExport").show();
                    $("#payparamDays").show();
                    break;
                default:
                    $("#payparamifLC307").hide();
                    $("#payparamifCont308").hide();
                    $("#payparamDays").hide();
                    $("#payparamcommisionChargesExport").show();
                    break;
            }
        }
    }

    function GetEIFInformationForBDA()
    {
        var REQ = $("#EIF_BDA_EIF_REQUEST_NO option:selected").val();
        if (REQ != 0)
        {
            $.ajax({
                type: "POST",
                url: "/EIFRequest/GetEIFInformationForBDA?E=" + REQ,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $("#EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY").val(data.TOTALAMOUNT);
                    $("#EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY").val(data.AUTCCY).change();
                    $("#EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY").val(data.AUTCCY).change();
                    $("#EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY").val(data.AUTCCY).change();
                    $("#EIF_BDA_CURRENCY_FCY").val(data.AUTCCY).change();
                    $("#EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY").val(data.EXCHAGERATE);
                    $("#EIF_BDA_EXCHANGE_RATE_FCY").val(data.EXCHAGERATE);
                    $("#EIF_BDA_SAMPLE_AMOUNT_EXCLUDE").val(data.SampleAmount.toFixed(2));
                    $("#EIF_BDA_Balance_Amount_FCY").val(data.CurrentBalance.toFixed(2));
                    BDAOnPageCalculations();
                }
            });
            $('#LoadBDAsInfoByEIF').load('/EIFRequest/EIFBDAList?E=' + REQ);
        }
    }


    function GetEEFInformationForBCA() {
        var REQ = $("#BCAD_FORM_E_NO option:selected").val();
        if (REQ != 0) {
            $.ajax({
                type: "POST",
                url: "/EEFRequest/GetEEFInformationForBCA?E=" + REQ,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $("#BCAD_CURRENCY").val(data.AUTCCY).change();
                    BCACalculation();
                }
            });
            $('#LoadBCAsInfoByEEF').load('/EEFRequest/BCAList?E=' + REQ);
        }
    }

    function BCACalculation()
    {
        var ExchangeRate = $("#BCAD_FCY_EXCHANGE_RATE").val();
        var BCAFCYAmount = $("#BCAD_BCA_FC").val();
        var BCAPKRAmount = parseFloat(parseFloat(ExchangeRate) * parseFloat(BCAFCYAmount)).toFixed(2);
        $("#BCAD_BCA_PKR").val(BCAPKRAmount);
    }


    function BDAOnPageCalculations()
    {
        var totalBDA  = $("#EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY").val();
        var SampleBDA = $("#EIF_BDA_SAMPLE_AMOUNT_EXCLUDE").val();
        var ExchageRate = $("#EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY").val();
        var previousBalance = $("#EIF_BDA_Balance_Amount_FCY").val();
        var NetBDAAmount = (totalBDA - (parseFloat(SampleBDA)));
        var NetBDAAmountPKR = (NetBDAAmount * ExchageRate);
        var BalanceAmount = (parseFloat(NetBDAAmount) - parseFloat(previousBalance));
        $("#EIF_BDA_Balance_Amount_FCY").val(BalanceAmount.toFixed(2));
        $("#EIF_BDA_NET_BDA_AMOUNT_EIF_FCY").val(NetBDAAmount.toFixed(2));
        $("#EIF_BDA_NET_BDA_AMOUNT_PKR").val(NetBDAAmountPKR.toFixed(2));
        $.ajax({
            type: "POST",
            url: "/EIFRequest/AmountInWords?Amount=" + NetBDAAmountPKR,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                $("#EIF_BDA_AMOUNT_IN_WORDS").val(data.AmountInWordS);
            }
        });

        var BDAAmountFCY = $("#EIF_BDA_AMOUNT_FCY").val();

        var CommisionAmountFCY = $("#EIF_BDA_COMMMISSION_FCY").val();


        if (CommisionAmountFCY != 0 && CommisionAmountFCY != "")
        {
            var ExchageRateFCY = $("#EIF_BDA_EXCHANGE_RATE_FCY").val();
            var CommisionAmountPKR = (CommisionAmountFCY * ExchageRateFCY);
            $("#EIF_BDA_COMMISSION_PKR").val(CommisionAmountPKR.toFixed(2));
        }
        else
        {
            $("#EIF_BDA_COMMISSION_PKR").val(0);
        }

        var FEDAmountFCY = $("#EIF_BDA_FED_AMOUNT_FCY").val();

        if (FEDAmountFCY != 0 && FEDAmountFCY != "") {
            var ExchageRateFCY = $("#EIF_BDA_EXCHANGE_RATE_FCY").val();
            var FEDAmountPKR = (FEDAmountFCY * ExchageRateFCY);
            $("#EIF_BDA_FED_AMOUNT_PKR").val(FEDAmountPKR.toFixed(2));
        }
        else {
            $("#EIF_BDA_FED_AMOUNT_PKR").val(0);
        }


        if (BDAAmountFCY != 0 && BDAAmountFCY != "") {
            var ExchageRateFCY = $("#EIF_BDA_EXCHANGE_RATE_FCY").val();
            BDAAmountPKR = (BDAAmountFCY * ExchageRateFCY);
            $("#EIF_BDA_AMOUNT_PKR").val(BDAAmountPKR.toFixed(2));
        }
        else
        {
            $("#EIF_BDA_AMOUNT_PKR").val(0);
        }




    }



    function GetAuthorizePayModesIMPORT(BusId)
    {
        if (BusId != 0) {
            $.ajax({
                type: "POST",
                url: "/EIFRequest/ListOfAuthPayModes?clientId=" + BusId,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    var rowcount = 0;
                    $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT").empty();
                    $.each(data, function (idx, elem) {
                        rowcount += 1;
                        if (rowcount == 1)
                        {
                            $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT").append("<option value='0'> --Select Mode of Import Payment-- </option>");
                        }
                        {
                            $("#basic_EIF_BI_MODE_OF_IMPORT_PAYMENT").append("<option value=" + elem.Value + ">" + elem.Text + "</option>");
                        }
                    });
                }
            });
        }
    }
    function GetBusinessDetailsIMPORT(BusId)
    {
        if (BusId != 0)
        {
            $.ajax({
                type: "POST",
                url: "/EIFRequest/GetBusinessDetails?clientId=" + BusId,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $.each(data, function (idx, elem) {
                            $("#basic_EIF_BI_NTN").val(elem.NTN);
                            $("#basic_EIF_BI_BUSSINESS_ADDRESS").val(elem.ADDRESS);
                            $("#basic_EIF_BI_STRN").val(elem.STRN);
                    });
                }
            });
        }
    }

    function GetAuthorizePayModesEXPORT(BusId) {
        if (BusId != 0) {
            $.ajax({
                type: "POST",
                url: "/EEFRequest/ListOfAuthPayModes?clientId=" + BusId,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    var rowcount = 0;
                    $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT").empty();
                    $.each(data, function (idx, elem) {
                        rowcount += 1;
                        if (rowcount == 1) {
                            $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT").append("<option value='0'> --Select Mode of Export Payment-- </option>");
                        }
                        {
                            $("#DocInfo_FDI_MODE_OF_EXPORT_PAYMENT").append("<option value=" + elem.Value + ">" + elem.Text + "</option>");
                        }
                    });
                }
            });
        }
    }
    function GetBusinessDetailsEXPORT(BusId) {
        if (BusId != 0) {
            $.ajax({
                type: "POST",
                url: "/EEFRequest/GetBusinessDetails?clientId=" + BusId,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $.each(data, function (idx, elem) {
                        $("#DocInfo_FDI_NTN").val(elem.NTN);
                        $("#DocInfo_FDI_TRADER_ADDRESS").val(elem.ADDRESS);
                    });
                }
            });
        }
    }


 
    function AddHsCodeToEIF()
    {

        var InputHSCode = $("#searchHsCode").val();
        var EIF_REQ = $("#basic_EIF_BI_EIF_REQUEST_NO").val();
        var InputQTY = $("#searchQty").val();
        var InputUOM = $("#searchUOM").val();
        var InputOrigin = $("#searchOrigin").val();
        var IsSample = $('#searchIsSample').val();
        var Samplevalue = $("#searchSampleValue").val();
        var hscode = {};
        hscode.EIF_HC_ID = 0;
        hscode.EIF_HC_EIF_REQUEST_NO = EIF_REQ;
        hscode.EIF_HC_CODE = InputHSCode;
        hscode.EIF_HC_QUANTITY = InputQTY;
        hscode.EIF_HC_UOM = InputUOM;
        hscode.EIF_HC_ORGIN = InputOrigin;
        if (IsSample == "true")
        {
            hscode.EIF_HC_IS_SAMPLE = "Y";
            hscode.EIF_HC_SAMPLE_VALUE = Samplevalue;
        }
        else
        {
            hscode.EIF_HC_IS_SAMPLE = "N";
            hscode.EIF_HC_SAMPLE_VALUE = 0;
        }
        $.ajax({
            type: "POST",
            url: "/EIFRequest/AddHsCodeToEIF",
            data: JSON.stringify(hscode),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (r) {
                if (r != "") {
                    alert(r);
                }
                else
                {
                    alert("Added Successfully.");
                    $('#HsGoodsDetailsAJ').load('/EIFRequest/EIFHSCodeList?H=' + EIF_REQ);
                }
            }
        });
    }

    function AddHsCodeToEEF() {

        var InputHSCode = $("#searchHsCodeEEF").val();
        var EIF_REQ = $("#DocInfo_FDI_FORM_E_NO").val();
        var InputQTY = $("#searchQtyEEF").val();
        var InputUOM = $("#searchUOMEEF").val();
        var InputOrigin = $("#searchOriginEEF").val();
        var InvoiceValue = $("#searchInvoiceValueEEF").val();
        var hscode = {};
        hscode.EEF_HC_ID = 0;
        hscode.EEF_HC_EEF_REQUEST_NO = EIF_REQ;
        hscode.EEF_HC_CODE = InputHSCode;
        hscode.EEF_HC_QUANTITY = InputQTY;
        hscode.EEF_HC_UOM = InputUOM;
        hscode.EEF_HC_ORGIN = InputOrigin;
        hscode.EEF_HC_INVOICE_VALUE = InvoiceValue;
        $.ajax({
            type: "POST",
            url: "/EEFRequest/AddHsCodeToEEF",
            data: JSON.stringify(hscode),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (r) {
                if (r != "") {
                    alert(r);
                }
                else {
                    alert("Added Successfully.");
                    $('#HsGoodsDetailsEEF').load('/EEFRequest/EEFHSCodeList?H=' + EIF_REQ);
                    GetSUMHsCodesEEF();
                }
            }
        });
    }

    $("body").on("click", "#AddHsCode", function () {
        AddHsCodeToEIF();
    });

    $("body").on("click", "#AddHsCodeEEF", function () {
        AddHsCodeToEEF();
    });


});


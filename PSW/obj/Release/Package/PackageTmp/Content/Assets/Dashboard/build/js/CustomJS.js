﻿//#region Custom Template Js 
$(window).on('load', function () {
    $('#loading').hide();
});
$(function () {
    $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#EIFHsCodes").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#EEFHsCodes").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#LoadUnpinnedGds").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#Reporting").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#Reporting_wrapper .col-md-6:eq(0)');

    ////Initialize Select2 Elements
    //$('.select2').select2()
    ////Initialize Select2 Elements
    //$('.select2bs4').select2({
    //    theme: 'bootstrap4'
    //})
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
//#endregion

//#region Client's Functionality(Parent)

//#region Client Authorize/Reject
$("[name='AuthorizeClient']").click(function (button) {
    var C_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ClientInfo/AuthorizeClient?C_ID=' + C_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeClient']").click(function (button) {
    var C_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ClientInfo/RejectAuthorizeClient?C_ID=' + C_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Assign Profile Type to Client Authorize/Reject
$("[name='AuthorizeAssignProfileType']").click(function (button) {
    var APT_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ClientInfo/AuthorizeAssignProfileType?APT_ID=' + APT_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeAssignProfileType']").click(function (button) {
    var APT_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ClientInfo/RejectAuthorizeAssignProfileType?APT_ID=' + APT_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Assign Payment Mode to Client Authorize/Reject
$("[name='AuthorizeAssignPayMode']").click(function (button) {
    var APMC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ClientInfo/AuthorizeAssignPayMode?APMC_ID=' + APMC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeAssignPayMode']").click(function (button) {
    var APMC_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ClientInfo/RejectAuthorizeAssignPayMode?APMC_ID=' + APMC_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Client's Negative List of Countries Authorize/Reject
$("[name='AuthorizeNegativeListOfCountries']").click(function (button) {
    var NLOC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ClientInfo/AuthorizeNegativeListOfCountries?NLOC_ID=' + NLOC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeNegativeListOfCountries']").click(function (button) {
    var NLOC_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ClientInfo/RejectAuthorizeNegativeListOfCountries?NLOC_ID=' + NLOC_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Client's Negative List of Commodity Authorize/Reject
$("[name='AuthorizeNegativeListOfCommodities']").click(function (button) {
    var NLCOM_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ClientInfo/AuthorizeNegativeListOfCommodities?NLCOM_ID=' + NLCOM_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeNegativeListOfCommodities']").click(function (button) {
    var NLCOM_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ClientInfo/RejectAuthorizeNegativeListOfCommodities?NLCOM_ID=' + NLCOM_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Client's Negative List of Suppliers Authorize/Reject
$("[name='AuthorizeNegativeListOfSuppliers']").click(function (button) {
    var NLOS_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ClientInfo/AuthorizeNegativeListOfSuppliers?NLOS_ID=' + NLOS_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeNegativeListOfSuppliers']").click(function (button) {
    var NLOS_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ClientInfo/RejectAuthorizeNegativeListOfSuppliers?NLOS_ID=' + NLOS_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#endregion

$("[id='SetEEFGDNumber']").click(function (button) {
    var GDNumber = $(this).val();
    if (GDNumber != "" || GDNumber != null) {
        $('#PayParam_EIF_PMP_GD_NO').val(GDNumber);
        $('#modal-GDs').modal('hide');
    }
    else {
        $('#PayParam_EIF_PMP_GD_NO').val('');
        $('#modal-GDs').modal('hide');
    }
});

//#region Get GD in Pop Modal 
$("[id='SetGDNumber']").click(function (button) {
    var GDNumber = $(this).val();
    if (GDNumber != "" || GDNumber != null) {
        $('#PayParam_EIF_PMP_GD_NO').val(GDNumber);
        PopulateDataInEIFFormFromGD(GDNumber);
        $('#modal-GDs').modal('hide');
    }
    else {
        $('#PayParam_EIF_PMP_GD_NO').val('');
        $('#modal-GDs').modal('hide');
    }
});
function PopulateDataInEIFFormFromGD(GDNo) {
    if (GDNo != "" && GDNo != null) {
        $.ajax({
            type: "POST",
            url: "/EIFRequest/GetEIFValueFromGD?GD=" + GDNo,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                if (data != "") {
                    $('#documents_EIF_DRI_BENEFICIARY_NAME').val(data.Name);
                    $('#documents_EIF_DRI_BENEFICIARY_ADDRESS').val(data.Address);
                    $('#documents_EIF_DRI_EXPORTER_NAME').val(data.Name);
                    $('#documents_EIF_DRI_EXPORTER_ADDRESS').val(data.Address);
                    $('#documents_EIF_DRI_DELIVERY_TERM').val(data.DelivertTerm);
                    $('#documents_EIF_DRI_TOTAL_INVOICE_VALUE').val(data.TotalValue.toFixed(5));
                    $('#documents_EIF_DRI_EXCHANGE_RATE').val(data.ExchangeRate.toFixed(5));
                }
                var EIF_REQ = $("#basic_EIF_BI_EIF_REQUEST_NO").val();
                if (EIF_REQ == "") {
                    $.ajax({
                        type: "POST",
                        url: "/EIFRequest/GenerateNewEIFReqNo",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $('#loading').show();
                        },
                        complete: function () {
                            $('#loading').hide();
                        },
                        success: function (Data) {
                            $('#basic_EIF_BI_EIF_REQUEST_NO').val(Data.EIF_NO);
                            $('#basic_EIF_BI_INTERNAL_REF').val(Data.IR_NO);
                            AddHsCodeFromGDToEIF(GDNo, Data.EIF_NO);
                        }
                    });
                }
                else {
                    AddHsCodeFromGDToEIF(GDNo,"");
                }
            }
        });
    }
}

function AddHsCodeFromGDToEIF(GDNO, EIF_NO) {
    if (EIF_NO == "" || EIF_NO == null) {
        EIF_NO = $('#basic_EIF_BI_EIF_REQUEST_NO').val();
    }
    var CurrentURL = $(location).attr('href'); 
    var GetAmendmentNo = 0;
    if (CurrentURL.indexOf("&M=") != -1) {
        GetAmendmentNo = CurrentURL.substr(CurrentURL.indexOf('&M=') + 3)
    }
    $.ajax({
        type: "POST",
        url: "/EIFRequest/AddHsCodeFromGDToEIF?E=" + EIF_NO + "&AM=" + GetAmendmentNo + "&GDNo=" + GDNO,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (r) {
            if (r != "") {
                alert(r);
                $('#HsGoodsDetailsAJ').load('/EIFRequest/EIFHSCodeList?H=' + EIF_NO + '&M=' + GetAmendmentNo);
            }
        }
    });
}
//#endregion

//#region Remove EIF HS Code
$('body').on('click', "[name='RemoveHsCodeEIF']", function () {
    var EIF_REQ = $("#basic_EIF_BI_EIF_REQUEST_NO").val();
    var EIF_HC_AMENDMENT_NO = $("#basic_EIF_BI_AMENDMENT_NO").val();
    //alert(EIF_HC_AMENDMENT_NO);
    var row = $(this).closest("TR");
    //alert($("TD", row).eq(1).html().trim());
    var hscode = {};
    hscode.EIF_HC_EIF_REQUEST_NO = EIF_REQ;
    hscode.EIF_HC_CODE = $("TD", row).eq(2).html().trim();
    hscode.EIF_HC_AMENDMENT_NO = EIF_HC_AMENDMENT_NO;
    hscode.EIF_HC_ID = $("TD", row).eq(0).html().trim();
    if (hscode.EIF_HC_CODE != "") {
        $.ajax({
            type: "POST",
            url: "/EIFRequest/RemoveHSCode",
            data: JSON.stringify(hscode),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (r) {
                if (r != "") {
                    alert(r);
                }
                else {
                    alert("Removed Successfully.");
                    $('#HsGoodsDetailsAJ').load('/EIFRequest/EIFHSCodeList?H=' + EIF_REQ + '&M=' + EIF_HC_AMENDMENT_NO);
                }
            }
        });
    }
});
//#endregion

//#region Remove EEF HS Code
$('body').on('click', "[name='RemoveHsCodeEEF']", function () {
    var EEF_REQ = $("#DocInfo_FDI_FORM_E_NO").val();
    var HC_AMENDMENT_NO = $("#DocInfo_FDI_AMENDMENT_NO").val();
    var row = $(this).closest("TR");

    //alert($("TD", row).eq(1).html().trim());
    var hscode = {};
    hscode.EEF_HC_EEF_REQUEST_NO = EEF_REQ;
    hscode.EEF_HC_CODE = $("TD", row).eq(2).html().trim();
    hscode.EEF_HC_ID = $("TD", row).eq(0).html().trim();
    hscode.EEF_HC_AMENDMENT_NO = HC_AMENDMENT_NO;
    if (hscode.EEF_HC_CODE != "") {
        $.ajax({
            type: "POST",
            url: "/EEFRequest/RemoveHSCode",
            data: JSON.stringify(hscode),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (r) {
                if (r != "") {
                    alert(r);
                }
                else {
                    alert("Removed Successfully.");
                    $('#HsGoodsDetailsEEF').load('/EEFRequest/EEFHSCodeList?H=' + EEF_REQ + '&M=' + HC_AMENDMENT_NO);
                    GetSUMHsCodesEEF();
                }
            }
        });
    }
});
//#endregion

//#region Group Role Authorize/Reject
$("[name='AuthorizeGroupRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/GroupRole/AuthorizeGroupRole?R_ID=' + R_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeGroupRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/GroupRole/RejectAuthorizeGroupRole?R_ID=' + R_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Login User Authorize/Reject
$("[name='AuthorizeAddUser']").click(function (button) {
    var LOG_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/LoginUser/AuthorizeAddUser?LOG_ID=' + LOG_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeAddUser']").click(function (button) {
    var LOG_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/LoginUser/RejectAuthorizeAddUser?LOG_ID=' + LOG_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region System Setup Functionality(Parent)

//#region Country Authorize/Reject
$("[name='AuthorizeCountry']").click(function (button) {
    var LC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeCountry?LC_ID=' + LC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeCountry']").click(function (button) {
    var LC_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeCountry?LC_ID=' + LC_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Payment Modes Authorize/Reject
$("[name='AuthorizePayModes']").click(function (button) {
    var APM_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizePayModes?APM_ID=' + APM_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizePayModes']").click(function (button) {
    var APM_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizePayModes?APM_ID=' + APM_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Account Status Authorize/Reject
$("[name='AuthorizeAccountStatus']").click(function (button) {
    var AS_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeAccountStatus?AS_ID=' + AS_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeAccountStatus']").click(function (button) {
    var AS_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeAccountStatus?AS_ID=' + AS_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Account Type Authorize/Reject
$("[name='AuthorizeAccountType']").click(function (button) {
    var AT_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeAccountType?AT_ID=' + AT_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeAccountType']").click(function (button) {
    var AT_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeAccountType?AT_ID=' + AT_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Delivery Term Authorize/Reject
$("[name='AuthorizeDeliveryTerm']").click(function (button) {
    var DT_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeDeliveryTerm?DT_ID=' + DT_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeDeliveryTerm']").click(function (button) {
    var DT_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeDeliveryTerm?DT_ID=' + DT_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Commodity Authorize/Reject
$("[name='AuthorizeManageCommodities']").click(function (button) {
    var LCOM_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeManageCommodities?LCOM_ID=' + LCOM_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeManageCommodities']").click(function (button) {
    var LCOM_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeManageCommodities?LCOM_ID=' + LCOM_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Supplier Authorize/Reject
$("[name='AuthorizeManageSuppliers']").click(function (button) {
    var LOS_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeManageSuppliers?LOS_ID=' + LOS_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeManageSuppliers']").click(function (button) {
    var LOS_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeManageSuppliers?LOS_ID=' + LOS_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Cash Margin Authorize/Reject
$("[name='AuthorizeCashMargin']").click(function (button) {
    var CM_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeCashMargin?CM_ID=' + CM_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeCashMargin']").click(function (button) {
    var CM_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeCashMargin?CM_ID=' + CM_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Import Policy Authorize/Reject
$("[name='AuthorizeImportPolicy']").click(function (button) {
    var IP_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeImportPolicy?IP_ID=' + IP_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
                //$('#ImportPolicyList').load('/Setup/AuthorizeImportPolicy?IP_ID=' + IP_ID);
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeImportPolicy']").click(function (button) {
    var IP_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeImportPolicy?IP_ID=' + IP_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Cities Authorize/Reject
$("[name='AuthorizeManageCities']").click(function (button) {
    var LOCT_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeManageCities?LOCT_ID=' + LOCT_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeManageCities']").click(function (button) {
    var LOCT_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeManageCities?LOCT_ID=' + LOCT_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Banks Authorize/Reject
$("[name='AuthorizeManageBank']").click(function (button) {
    var LB_BANK_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeManageBank?LB_BANK_ID=' + LB_BANK_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeManageBank']").click(function (button) {
    var LB_BANK_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeManageBank?LB_BANK_ID=' + LB_BANK_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Currency Authorize/Reject
$("[name='AuthorizeManageCurrency']").click(function (button) {
    var CUR_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeManageCurrency?CUR_ID=' + CUR_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeManageCurrency']").click(function (button) {
    var CUR_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizeManageCurrency?CUR_ID=' + CUR_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Port of Shipping Authorize/Reject
$("[name='AuthorizePortOfShipping']").click(function (button) {
    var POS_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizePortOfShipping?POS_ID=' + POS_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizePortOfShipping']").click(function (button) {
    var POS_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectAuthorizePortOfShipping?POS_ID=' + POS_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    } 
});
//#endregion

//#region Email Configration Authorize/Reject/Set As Current
$("[name='AuthorizeEmailSetup']").click(function (button) {
    var EC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeEmailSetup?EC_ID=' + EC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectEmailSetup']").click(function (button) {
    var EC_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectEmailSetup?EC_ID=' + EC_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
$("[name='UpdateEmailSetupStatus']").click(function (button) {
    var EC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/UpdateEmailSetupStatus?EC_ID=' + EC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
//#endregion

//#region UOM Authorize/Reject
$("[name='AuthorizeUOM']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeUOM?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectUOM']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectUOM?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#endregion

//#region WebApi Setting Functionality(Parent)

//#region Process Code Authorize/Reject
$("[name='AuthorizeProcessCode']").click(function (button) {
    var PC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/WebApiSetting/AuthorizeProcessCode?PC_ID=' + PC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeProcessCode']").click(function (button) {
    var PC_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/WebApiSetting/RejectAuthorizeProcessCode?PC_ID=' + PC_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Response Code Authorize/Reject
$("[name='AuthorizeResponseCode']").click(function (button) {
    var RES_CODE_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/WebApiSetting/AuthorizeResponseCode?RES_CODE_ID=' + RES_CODE_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeResponseCode']").click(function (button) {
    var RES_CODE_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/WebApiSetting/RejectAuthorizeResponseCode?RES_CODE_ID=' + RES_CODE_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Method ID's Authorize/Reject
$("[name='AuthorizeMethodID']").click(function (button) {
    var M_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/WebApiSetting/AuthorizeMethodID?M_ID=' + M_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectAuthorizeMethodID']").click(function (button) {
    var M_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/WebApiSetting/RejectAuthorizeMethodID?M_ID=' + M_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#endregion

//#region PSW Messages
$("[name='MsgRequest']").click(function (button) {
    var MSG_ID = $(this).attr("value");
    $("#ViewSelectedRequest").load("/MISReports/LoadRequest?MSG_ID=" + MSG_ID);
});
$("[name='MsgResponse']").click(function (button) {
    var MSG_ID = $(this).attr("value");
    $("#ViewSelectedRequest").load("/MISReports/LoadResponse?MSG_ID=" + MSG_ID);
});
//#endregion

//#region Export Consignee Authorize/Reject
$(document).on("click", '.AuthorizeExportConsignee', function (event) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/EEFRequest/AuthorizeExportConsignee?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$(document).on("click", '.RejectExportConsignee', function (event) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/EEFRequest/RejectExportConsignee?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

$("#ECL_FI_NUMBER").change(function () {
    var FiNumber = $("#ECL_FI_NUMBER").val();
    $("#ExportConsigneeListData").load("/EEFRequest/ExportConsigneeList?FiNumber=" + FiNumber);
});
if ($('#ECL_FI_NUMBER').length) {
    var FiNumber = $("#ECL_FI_NUMBER").val();
    $("#ExportConsigneeListData").load("/EEFRequest/ExportConsigneeList?FiNumber=" + FiNumber);
}
//#endregion

//#region ITRS Statement Authorize/Reject
$("[name='AuthorizeStatement']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeStatement?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectStatement']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectStatement?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Currency Authorize/Reject
$("[name='AuthorizeCurrency']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeCurrency?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectCurrency']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectCurrency?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Schedule Code Authorize/Reject
$("[name='AuthorizeScheduleCode']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeScheduleCode?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectScheduleCode']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectScheduleCode?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Country Authorize/Reject
$("[name='AuthorizeITRSCountry']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeCountry?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectITRSCountry']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectCountry?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Approval Of Remittance Authorize/Reject
$("[name='AuthorizeApprovalOfRemittance']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeApprovalOfRemittance?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectApprovalOfRemittance']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectApprovalOfRemittance?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Relationship Authorize/Reject
$("[name='AuthorizeRelationship']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeRelationship?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectRelationship']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectRelationship?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Scheme Authorize/Reject
$("[name='AuthorizeScheme']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeScheme?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectScheme']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectScheme?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Sales Term Authorize/Reject
$("[name='AuthorizeSalesTerm']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeSalesTerm?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectSalesTerm']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectSalesTerm?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Incoterm Authorize/Reject
$("[name='AuthorizeIncoterm']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeIncoterm?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectIncoterm']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectIncoterm?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Unit Code Authorize/Reject
$("[name='AuthorizeUnitCode']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeUnitCode?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectUnitCode']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectUnitCode?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Purpose Authorize/Reject
$("[name='AuthorizePurpose']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizePurpose?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectPurpose']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectPurpose?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Department Authorize/Reject
$("[name='AuthorizeDepartment']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeDepartment?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectDepartment']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectDepartment?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Immidiate Remitter Authorize/Reject
$("[name='AuthorizeImmidiateRemitter']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeImmidiateRemitter?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectImmidiateRemitter']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectImmidiateRemitter?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Banks Authorize/Reject
$("[name='AuthorizeBanks']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeBanks?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectBanks']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectBanks?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Client Authorize/Reject
$("[name='AuthorizeITRSClient']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeClient?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectITRSClient']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectClient?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Ac700 Authorize/Authorize All
$("[name='AuthorizeSingleAC700']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRS/AuthorizeItrsAC?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

if ($("[name='AuthorizeSingleAC700']").length) {
    $("#AlthorizedAllAc700Block").show();
}
else {
    $("#AlthorizedAllAc700Block").hide();
}
$("[name='AuthorizeBullkItrsAC']").click(function (button) {
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();
    if (FromDate != "" && ToDate != "") {
        $.ajax
            ({
                url: '/ITRS/AuthorizeBullkItrsAC',
                type: 'POST',
                data: JSON.stringify({
                    FromDate: FromDate,
                    ToDate: ToDate
                }),
                contentType: "application/json; charset=utf-8",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
    else
        alert("Select from or todate date to continue")
});
//#endregion

//#region ITRS Authoriz All Client
if ($("[name='AuthorizeITRSClient']").length) {
    $("#AlthorizedAllITRSClient").show();
}
else {
    $("#AlthorizedAllITRSClient").hide();
}
$("[name='AuthorizeBullkITRSClient']").click(function (button) {
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeBullkITRSClient',
            type: 'POST',
            //data: JSON.stringify({
            //    FromDate: FromDate,
            //    ToDate: ToDate
            //}),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
//#endregion

//#region ITRS Authoriz All Country
if ($("[name='AuthorizeITRSCountry']").length) {
    $("#AlthorizedAllITRSCountry").show();
}
else {
    $("#AlthorizedAllITRSCountry").hide();
}
$("[name='AuthorizeBullkITRSCountry']").click(function (button) {
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeBullkITRSCountry',
            type: 'POST',
            //data: JSON.stringify({
            //    FromDate: FromDate,
            //    ToDate: ToDate
            //}),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
//#endregion
//#region ITRS Authoriz All Scheme
if ($("[name='AuthorizeScheme']").length) {
    $("#AlthorizedAllITRSScheme").show();
}
else {
    $("#AlthorizedAllITRSScheme").hide();
}
$("[name='AuthorizeBullkITRSScheme']").click(function (button) {
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeBullkITRSScheme',
            type: 'POST',
            //data: JSON.stringify({
            //    FromDate: FromDate,
            //    ToDate: ToDate
            //}),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
//#endregion

//#region ITRS Authoriz All Relationship With Beneficiary
if ($("[name='AuthorizeRelationWithBene']").length) {
    $("#AlthorizedAllITRSRelationWithBene").show();
}
else {
    $("#AlthorizedAllITRSRelationWithBene").hide();
}
$("[name='AuthorizeBullkITRSRelationWithBene']").click(function (button) {
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeBullkITRSRelationWithBene',
            type: 'POST',
            //data: JSON.stringify({
            //    FromDate: FromDate,
            //    ToDate: ToDate
            //}),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
//#endregion

//#region ITRS Ac700 Authorize/Authorize All
$("[name='AuthorizeSingleFT801']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRS/AuthorizeItrsAC?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

if ($("[name='AuthorizeSingleFT801']").length) {
    $("#AlthorizedAllFt801lock").show();
}
else {
    $("#AlthorizedAllFt801lock").hide();
}
$("[name='AuthorizeBullkItrsFT']").click(function (button) {
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();
    if (FromDate != "" && ToDate != "") {
        $.ajax
            ({
                url: '/ITRS/AuthorizeBulkItrsFT',
                type: 'POST',
                data: JSON.stringify({
                    FromDate: FromDate,
                    ToDate: ToDate
                }),
                contentType: "application/json; charset=utf-8",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
    else
        alert("Select from or todate date to continue")
});

//#endregion

//#region ITRS USD Rate Authorize/Reject
$("[name='AuthorizeUSDRate']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeUSDRate?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectUSDRate']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectUSDRate?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS A404 Authorize/Reject
$("[name='AuthorizeA404']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeA404?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectA404']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectA404?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Balances Authorize/Reject
$("[name='AuthorizeBalances']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeBalances?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectBalances']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectBalances?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Beneficiary Requesting Bank Authorize/Reject
$("[name='AuthorizeBeneRequestingBank']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeBeneRequestingBank?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectBeneRequestingBank']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectBeneRequestingBank?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion
//#region ITRS Relationship With Beneficiary Authorize/Reject
$("[name='AuthorizeRelationWithBene']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeRelationWithBene?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectRelationWithBene']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectRelationWithBene?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#endregion

//#region ITRS Authoriz All Purpose
if ($("[name='AuthorizePurpose']").length) {
    $("#AlthorizedAllITRSPurpose").show();
}
else {
    $("#AlthorizedAllITRSPurpose").hide();
}
$("[name='AuthorizeBullkITRSPurpose']").click(function (button) {
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeBullkITRSPurpose',
            type: 'POST',
            //data: JSON.stringify({
            //    FromDate: FromDate,
            //    ToDate: ToDate
            //}),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
//#endregion
//#region ITRS Lender Sector Authorize/Reject
$("[name='AuthorizeLenderSector']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeLenderSector?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='RejectLenderSector']").click(function (button) {
    var ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ITRSSetup/RejectLenderSector?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});

//#region ITRS Authoriz Lender Sector
if ($("[name='AuthorizeLenderSector']").length) {
    $("#AlthorizedAllITRSLenderSector").show();
}
else {
    $("#AlthorizedAllITRSLenderSector").hide();
}
$("[name='AuthorizeBullkITRSLenderSector']").click(function (button) {
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeBullkITRSLenderSector',
            type: 'POST',
            //data: JSON.stringify({
            //    FromDate: FromDate,
            //    ToDate: ToDate
            //}),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
//#endregion

//#endregion
//#region ITRS DE003 Authorize/Authorize All
$("[name='AuthorizeSingleDE003']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ITRSSetup/AuthorizeDE003Upload?ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

if ($("[name='AuthorizeSingleDE003']").length) {
    $("#AlthorizedAllDE003Block").show();
}
else {
    $("#AlthorizedAllDE003Block").hide();
}
$("[name='AuthorizeBullkDE003']").click(function (button) {
    var FromDate = $("#FromDate").val();
    var ToDate = $("#ToDate").val();
    if (FromDate != "" && ToDate != "") {
        $.ajax
            ({
                url: '/ITRSSetup/AuthorizeBulkDE003Upload',
                type: 'POST',
                //data: JSON.stringify({
                //    FromDate: FromDate,
                //    ToDate: ToDate
                //}),
                contentType: "application/json; charset=utf-8",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
    else
        alert("Select from or todate date to continue")
});
//#endregion

//#region Browser Back Button
$("[name='BrowserBack']").click(function (button) {
    history.back();
});
//#endregion



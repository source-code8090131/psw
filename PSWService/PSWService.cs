﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace PSWService
{
    public partial class PSWService : ServiceBase
    {
        
        System.Timers.Timer timer = new System.Timers.Timer();
        Int32 ThreadSleepTime = Int32.Parse(ConfigurationManager.AppSettings["ServiceThreadSleepTime"]);
        System.Threading.Thread serviceThread;
        public PSWService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            serviceThread = new System.Threading.Thread(new System.Threading.ThreadStart(OnstartThread));
            // Start the thread
            serviceThread.Start();

        }

        public void OnstartThread()
        {
            DAL dal = new DAL();
            if (dal.ValidateSqlConnection())
            {
                WriteToFile("Attemp To Connect With Database Was Successful." + DateTime.Now, true, EventLogEntryType.Information);
                try
                {
                    WriteToFile(this.ServiceName + " is started at " + DateTime.Now, true, EventLogEntryType.Information);
                    timer.Elapsed += new System.Timers.ElapsedEventHandler(OnElapsedTime);
                    timer.Interval = ThreadSleepTime; //number in milisecinds 
                    timer.Enabled = true;

                }
                catch (Exception ex)
                {
                    WriteToFile(ex.Message, true, EventLogEntryType.Error);
                }
            }
            else
            {
                WriteToFile("Attemp To Connect With Database Was Failed." + DateTime.Now, true, EventLogEntryType.Information);
            }
            
        }

        private void OnElapsedTime(object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                WriteToFile(this.ServiceName + " is recall at " + DateTime.Now, false, EventLogEntryType.Information);
                StartProcess();
            }
            catch (Exception ex)
            {
                WriteToFile(ex.Message, true, EventLogEntryType.Error);
            }
        }

        protected override void OnStop()
        {
            try
            {
                serviceThread.Abort();
                WriteToFile(this.ServiceName + " is stoped at " + DateTime.Now, true, EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                WriteToFile(ex.Message, false, EventLogEntryType.Error);
            }
        }
        public void StartProcess()
        {
            string message = "";
            try
            {
                string Insertedtime = ConfigurationManager.AppSettings["ERSTIME"].ToString();
                DateTime dt;
                if (!DateTime.TryParseExact(Insertedtime, "HH:mm", System.Globalization.CultureInfo.InvariantCulture,
                                                              System.Globalization.DateTimeStyles.None, out dt))
                {
                    System.Diagnostics.EventLog.WriteEntry("PSW", "Error While Parsing DateTime For EERS", System.Diagnostics.EventLogEntryType.Error);
                }
                TimeSpan RunningTime = dt.TimeOfDay;
                TimeSpan CurrentTime = DateTime.Now.TimeOfDay;
                string Path = ConfigurationManager.AppSettings["ERSPATH"].ToString() + "/";
                if (Directory.Exists(Path))
                {                 
                    if (RunningTime <= CurrentTime)
                    {
                        WriteERSFeed(Path);
                    }
                }
                else
                    WriteToFile("Path does not exist : " + Path, false, EventLogEntryType.Error);
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
            }
            finally
            {
                if (message != "")
                {
                    WriteToFile(message, false, EventLogEntryType.Error);
                }
            }
        }

        public void WriteERSFeed(string P)
        {
            string message = "";
            try
            {
                string complete_path = P;
                string filename = "EERS174944.txt";
                string path = complete_path + filename;
                if (!System.IO.Directory.Exists(complete_path))
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                }
                DateTime creation = File.GetLastWriteTime(path);
                if (creation.Date < DateTime.Now.Date)
                {
                    WriteEERSFile(path);
                }
                else
                {
                    string FileContent = File.ReadAllText(path);
                    if (FileContent == "")
                    {
                        WriteEERSFile(path);
                    }
                }
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on WriteERSFeed" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
            }
            finally
            {
                if (message != "")
                {
                    WriteToFile(message, false, EventLogEntryType.Error);
                }
            }
        }


        public void WriteEERSFile(string path)
        {
            File.WriteAllText(path, String.Empty);
            using (System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write))
            {
                DAL dal = new DAL();
                int TLRRowCount = 0;
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                DataTable Users = dal.GetAllActiveUsers();
                if (Users.Rows.Count > 0)
                {
                    writer.WriteLine("HDR " + " \t " + DateTime.Now.ToString("yyyy/MM/dd"));
                    TLRRowCount = Users.Rows.Count;
                    Users = Users.AsEnumerable()
                    .GroupBy(r => new { Col1 = Convert.ToString(r["LOG_USER_ID"]).ToLower() })
                    .Select(g => g.OrderByDescending(r => r["LOG_ID"]).First())
                    .CopyToDataTable();
                    foreach (DataRow user in Users.Rows)
                    {
                        string USERID = user["LOG_USER_ID"].ToString();
                        string ANO = user["LOG_AMENDMENT_NO"].ToString();
                        List<int?> AssignRolesIds = dal.GetAssignedRoleIds(USERID, ANO);
                        if (AssignRolesIds.Count > 0)
                        {
                            foreach (int RoleId in AssignRolesIds)
                            {
                                DataTable RoleInfo = dal.GetRoleInfoByRoleId(RoleId);
                                if (RoleInfo.Rows.Count > 0)
                                {
                                    if (RoleInfo != null)
                                    {
                                        if (RoleInfo.Rows[0]["R_NAME"].ToString().Contains("ISA"))
                                        {
                                            writer.WriteLine("174944" + " \t " + user["LOG_USER_LAST_NAME"].ToString() + " \t " + user["LOG_USER_NAME"].ToString() + " \t " + user["LOG_USER_ID"].ToString() + " \t " + user["LOG_RIST_ID"].ToString() + " \t " + user["LOG_USER_GE_ID"].ToString() + " \t " + RoleInfo.Rows[0]["R_NAME"].ToString() + " \t " + RoleInfo.Rows[0]["R_DESCRIPTION"].ToString() + " \t " + user["LOG_USER_ID"].ToString() + " \t  \t  \t  \t  \t  \t   \t " + "ISA");
                                        }
                                        else
                                        {
                                            writer.WriteLine("174944" + " \t " + user["LOG_USER_LAST_NAME"].ToString() + " \t " + user["LOG_USER_NAME"].ToString() + " \t " + user["LOG_USER_ID"].ToString() + " \t " + user["LOG_RIST_ID"].ToString() + " \t " + user["LOG_USER_GE_ID"].ToString() + " \t " + RoleInfo.Rows[0]["R_NAME"].ToString() + " \t " + RoleInfo.Rows[0]["R_DESCRIPTION"].ToString() + " \t " + user["LOG_USER_ID"].ToString());
                                        }
                                        TLRRowCount += 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            WriteToFile("No Assign Roles Found In The System For UserId :  " + USERID, false, EventLogEntryType.Error);
                        }
                    }
                    writer.WriteLine("TRL " + " \t " + TLRRowCount);
                }
                else
                {
                    WriteToFile("No Active Users Found in the System For EERS Feed. ", false, EventLogEntryType.Error);
                }
                writer.Close();
                fs.Close();
            }
        }
        public void WriteToFile(string Message, bool WriteTolEvent, EventLogEntryType type)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\" + this.ServiceName + "Log_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            if (WriteTolEvent)
                EventLog.WriteEntry(Message, type);

        }
    }
}

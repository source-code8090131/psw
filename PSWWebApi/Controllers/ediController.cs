﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PSWWebApi.Models;
using System.Security.Cryptography;
using System.Globalization;
using System.Text.Json;
using System.Web;

namespace PSWWebApi.Controllers
{
    public class ediController : ApiController
    {

        PSWEntities context = new PSWEntities();
        public static string LogCurrentRequest;

        // GET api/<controller>

        [HttpGet]
        [Route("api/dealers/citi/edi/")]
        public IEnumerable<string> Get()
        {
            return new string[] { "You Are Now Authorized To The Api", "Detailed Messages will be visible soon. " + DateTime.Now};
        }

        [Authorize]
        [HttpPost]
        [Route("api/dealers/citi/edi/")]

        /// Else If Dev
        //public HttpResponseMessage Post(HttpRequestMessage request)
        //{
        //    try
        //    {
        //        var data = request.Content.ReadAsStringAsync().Result;
        //        dynamic RequestData = JObject.Parse(data);
        //        string JSONSerialized = data.ToString();
        //        return Request.CreateResponse(HttpStatusCode.OK, JSONSerialized);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException != null)
        //            if (ex.InnerException.InnerException != null)
        //                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.InnerException.InnerException.Message));
        //        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message));
        //    }
        //}

        //////// if Prod 
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            try
            {
                var data = request.Content.ReadAsStringAsync().Result;
                dynamic RequestData = System.Web.Helpers.Json.Decode(data);
                string MessageId = RequestData.messageId;
                string Signature = RequestData.signature;
                string reciever = RequestData.receiverId;
                LogCurrentRequest = data;
                if (reciever == "CBN")
                {
                    if (MessageId.Length >= 25)
                    {
                        string ProcCode = RequestData.processingCode;
                        switch (ProcCode)
                        {
                            case "301":
                                //string Serialized301 = JsonConvert.SerializeObject(RequestData);
                                string CreatedSignature301 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "301");
                                if (Signature == CreatedSignature301)
                                {
                                    string IBAN301 = RequestData.data.iban;
                                    string Email = RequestData.data.email;
                                    string Mobile = RequestData.data.mobileNumber;
                                    string NTN = RequestData.data.ntn;
                                    return VerifyClient(IBAN301, Email, Mobile, NTN);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            case "302":
                                string CreatedSignature302 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "302");
                                if (Signature == CreatedSignature302)
                                {
                                    string IBAN302 = RequestData.data.iban;
                                    return ShareDetailsAndAuthPayModes(IBAN302);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            case "303":
                                string CreatedSignature303 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "303");
                                if (Signature == CreatedSignature303)
                                {
                                    string IBAN303 = RequestData.data.iban;
                                    return ShareNegativeListOfCountries(IBAN303);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            case "304":
                                string CreatedSignature304 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "304");
                                if (Signature == CreatedSignature304)
                                {
                                    string IBAN304 = RequestData.data.iban;
                                    return ShareNegativeListOfCommodities(IBAN304);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            case "305":
                                string CreatedSignature305 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "305");
                                if (Signature == CreatedSignature305)
                                {
                                    string IBAN305 = RequestData.data.iban;
                                    return ShareNegativeListOfSuppliers(IBAN305);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            case "101":
                                string CreatedSignature101 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "101");
                                if (Signature == CreatedSignature101)
                                {
                                    return ShareGDAndFinancialTransactionWithAD(RequestData);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            case "102":
                                string CreatedSignature102 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "102");
                                if (Signature == CreatedSignature102)
                                {
                                    return ShareGDAndFinancialTransactionWithADExport(RequestData);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            case "306":
                                string CreatedSignature306 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "306");
                                if (Signature == CreatedSignature306)
                                {
                                    return ShareCOBWithAD(RequestData);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            case "307":
                                string CreatedSignature307 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "307");
                                if (Signature == CreatedSignature307)
                                {
                                    return ShareGDNFIWithOtherAD(RequestData);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            case "308":
                                string CreatedSignature308 = DAL.GetSignature(data);
                                LogRequestRecieve(data, "308");
                                if (Signature == CreatedSignature308)
                                {
                                    return ShareCOBAprovalRejection(RequestData);
                                }
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("701", "Signature Mismatch"));
                            default:
                                return Request.CreateResponse(HttpStatusCode.OK, "Invalid Processing Code. Please Try A Valid Processing Code Within The Request.");
                        }
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("208", "Sorry The MessageId is InValid"));
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, CreateResposeWithCode("208", "Invalid Reciever Id, To Message CITI Please use CBN as receiverId"));

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    if (ex.InnerException.InnerException != null)
                        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.InnerException.InnerException.Message + " (Server)"));
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message + " (Server)"));
            }
        }
        public string GetSignature(string data)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
            {
                return Convert.ToBase64String(algorithm.ComputeHash(System.Text.UTF8Encoding.UTF8.GetBytes(data.ToString())));
            }
        }

        public void LogRequestRecieve(string RequestData, string proc_code)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "\\RecieverLogs\\" + "Proc_Code_" + proc_code + "_DATED_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            System.IO.File.WriteAllText(path, RequestData.ToString());
        }
        #region AddMessageToDB
        public PSW_MESSAGES ConvertMessageToEntity(dynamic Data)
        {
            try
            {
                string Response = "";
                string Request = "";
                if (Data != null)
                    Response = System.Text.Json.JsonSerializer.Serialize(Data);
                if (LogCurrentRequest != null)
                    Request = LogCurrentRequest;
                PSW_MESSAGES CheckExist = new PSW_MESSAGES();
                PSW_MESSAGES NewMessage = new PSW_MESSAGES();
                System.Text.Json.Nodes.JsonNode GetRequestParam = System.Text.Json.JsonSerializer.Deserialize<System.Text.Json.Nodes.JsonNode>(Request);
                string SmId = (string)GetRequestParam["messageId"];
                CheckExist = context.PSW_MESSAGES.Find(SmId);
                if (CheckExist != null)
                {
                    NewMessage = CheckExist;
                }
                NewMessage.messageId = (string)GetRequestParam["messageId"];
                decimal timestampR = 0;
                string timestampS = (string)GetRequestParam["timestamp"];
                if (decimal.TryParse(timestampS, out timestampR))
                {
                    NewMessage.timestamp = timestampR;
                } 
                NewMessage.senderId = (string)GetRequestParam["senderId"];
                NewMessage.receiverId = (string)GetRequestParam["receiverId"];
                decimal processingCodeR = 0;
                string processingCodeS = (string)GetRequestParam["processingCode"];
                if (decimal.TryParse(processingCodeS, out processingCodeR))
                {
                    NewMessage.processingCode = processingCodeR;
                }
                NewMessage.data = Request;
                NewMessage.signature = (string)GetRequestParam["signature"];
                NewMessage.response = Response;
                NewMessage.code = Data.message.code;
                NewMessage.message = Data.message.description;
                if (CheckExist == null)
                    context.PSW_MESSAGES.Add(NewMessage);
                else
                    context.Entry(NewMessage).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return NewMessage;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region ProcCode 301
        HttpResponseMessage VerifyClient(string iban, string email, string mobilenumber, string ntn)
        {
            try
            {
                PSW_CLIENT_MASTER_DETAIL info = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == iban && m.C_EMAIL_ADDRESS == email && m.C_MOBILE_NO == mobilenumber && m.C_NTN_NO == ntn).FirstOrDefault();
                if (info != null)
                {
                    PSW_CLIENT_INFO UpdateEntity = context.PSW_CLIENT_INFO.Where(m => m.C_ID == info.C_ID).FirstOrDefault();
                    if (UpdateEntity != null)
                    {
                        ClientVerificationFROMPSW data = new ClientVerificationFROMPSW();
                        data.messageId = Guid.NewGuid().ToString();
                        data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                        data.senderId = "CBN";
                        data.receiverId = "PSW";
                        data.processingCode = "301";
                        data.data = new ClientVerificationFROMPSW.DetailData { };
                        data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                        data.message = new ClientVerificationFROMPSW.MessageData
                        {
                            code = "207",
                            description = "IBAN verification successful."
                        };
                        ConvertMessageToEntity(data);
                        UpdateEntity.C_VERIFIRD_BY_PSW = "Verified";
                        context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                        int RowCount = context.SaveChanges();
                        if (RowCount > 0)
                            return Request.CreateResponse(HttpStatusCode.OK, data);
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208"));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208"));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208"));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("208", ex.Message));
            }
        }
        #endregion


        ResonseMessageFromSBP.MessageData CreateResposeWithCode(string Code, string descr = "")
        {
            ResonseMessageFromSBP.MessageData Message = new ResonseMessageFromSBP.MessageData();
            PSWEntities context = new PSWEntities();
            if (descr == "")
            {
                PSW_RESPONSE_CODE response = context.PSW_RESPONSE_CODE.Where(m => m.RES_CODE == Code).FirstOrDefault();
                if (response != null)
                {
                    Message.code = response.RES_CODE;
                    Message.description = response.RES_CODE_DESCRIPTION;
                    return Message;
                }
                Message.code = Code;
                Message.description = "Description";
            }
            else
            {
                Message.code = Code;
                Message.description = descr;
            }
            return Message;
        }

        #region ProcCode 302
        HttpResponseMessage ShareDetailsAndAuthPayModes(string iban)
        {
            try
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == iban).FirstOrDefault();
                if (client != null)
                {
                    List<int?> assignPaymodes = context.PSW_ASSIGN_PAY_MODE_TO_CLIENT.Where(m => m.APMC_CLIENT_ID == client.C_ID && m.APMC_STATUS == true).Select(m => m.APMC_PAY_MODE_ID).ToList();
                    if (assignPaymodes.Count > 0)
                    {
                        List<int?> PayCodesForImport = context.PSW_AUTH_PAY_MODE_VIEW.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 1 && x.APM_CODE != 310 && x.APM_CODE != 309).Select(u => u.APM_CODE).ToList();
                        List<int?> PayCodesForExport = context.PSW_AUTH_PAY_MODE_VIEW.Where(x => assignPaymodes.Contains(x.APM_ID) && x.APM_TYPE_ID == 2).Select(u => u.APM_CODE).ToList();
                        if (PayCodesForExport.Count > 0 || PayCodesForImport.Count > 0)
                        {
                            string AccountStatus = context.PSW_ACCOUNT_STATUS.Where(m => m.AS_ID == client.C_ACCOUNT_STATUS_ID).Select(m => m.AS_CODE).FirstOrDefault();
                            string AccountType = context.PSW_ACCOUNT_TYPE.Where(m => m.AT_ID == client.C_ACCOUNT_TYPE_ID).Select(m => m.AT_CODE).FirstOrDefault();
                            ShareDetailsAndAuthPayModes data = new ShareDetailsAndAuthPayModes();
                            data.messageId = Guid.NewGuid().ToString();
                            data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                            data.senderId = "CBN";
                            data.receiverId = "PSW";
                            data.processingCode = "302";
                            data.data = new ShareDetailsAndAuthPayModes.DetailData
                            {
                                iban = client.C_IBAN_NO,
                                accountTitle = client.C_NAME,
                                accountNumber = client.C_BANK_ACCOUNT_NO,
                                accountStatus = AccountStatus.Trim(),
                                ntn = client.C_NTN_NO,
                                cnic = client.C_NTN_NO,
                                authorizedPaymentModesForImport =
                                PayCodesForImport.ConvertAll<string>(x => x.ToString()),
                                authorizedPaymentModesForExport =
                                PayCodesForExport.ConvertAll<string>(x => x.ToString())
                            };
                            data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                            data.message = new ShareDetailsAndAuthPayModes.MessageData
                            {
                                code = "200",
                                description = "Account details and authorized payment modes shared."
                            };
                            ConvertMessageToEntity(data);
                            return Request.CreateResponse(HttpStatusCode.OK, data);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Authorized Paymodes Found For the Requested Client"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Authorized Paymodes Found For the Requested Client"));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Sharing Data Failed, Invalid IBAN provided"));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region ProcCode 303
        HttpResponseMessage ShareNegativeListOfCountries(string iban)
        {
            try
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == iban).FirstOrDefault();
                if (client != null)
                {
                    List<int?> NegetiveCountriesImport = context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Where(m => m.NLOC_STATUS == true && m.NLOC_PROFILE_TYPE == 1).Select(m => m.NLOC_COUNTRY_ID).ToList();
                    List<int?> NegetiveCountriesExport = context.PSW_NEGATIVE_LIST_OF_COUNTRIES.Where(m => m.NLOC_STATUS == true && m.NLOC_PROFILE_TYPE == 2).Select(m => m.NLOC_COUNTRY_ID).ToList();

                    if (NegetiveCountriesImport.Count > 0 || NegetiveCountriesExport.Count > 0)
                    {
                        List<string> NegetiveCountriesForImport = context.PSW_LIST_OF_COUNTRIES.Where(x => NegetiveCountriesImport.Contains(x.LC_ID)).Select(u => u.LC_CODE).ToList();
                        List<string> NegetiveCountriesForExport = context.PSW_LIST_OF_COUNTRIES.Where(x => NegetiveCountriesExport.Contains(x.LC_ID)).Select(u => u.LC_CODE).ToList();
                        if (NegetiveCountriesForImport.Count > 0 && NegetiveCountriesForExport.Count > 0)
                        {
                            SHARE_NEGETIVE_LIST_OF_COUNTRIES data = new SHARE_NEGETIVE_LIST_OF_COUNTRIES();
                            data.messageId = Guid.NewGuid().ToString();
                            data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                            data.senderId = "CBN";
                            data.receiverId = "PSW";
                            data.processingCode = "303";
                            data.data = new SHARE_NEGETIVE_LIST_OF_COUNTRIES.DetailData
                            {
                                iban = client.C_IBAN_NO,
                                restrictedCountriesForImport = NegetiveCountriesForImport,
                                restrictedCountriesForExport = NegetiveCountriesForExport
                            };
                            data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                            data.message = new SHARE_NEGETIVE_LIST_OF_COUNTRIES.MessageData
                            {
                                code = "200",
                                description = "Negative list of countries shared."
                            };
                            ConvertMessageToEntity(data);
                            return Request.CreateResponse(HttpStatusCode.OK, data);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Negetive Countries Found."));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Negetive Countries Found."));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Sharing Data Failed, Invalid IBAN provided"));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message));
            }
        }
        #endregion

        #region ProcCode 304
        HttpResponseMessage ShareNegativeListOfCommodities(string iban)
        {
            try
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == iban).FirstOrDefault();
                if (client != null)
                {
                    List<int?> NegetiveCommoditiesImport = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_STATUS == true && m.NLCOM_PROFILE_TYPE == 1).Select(m => m.NLCOM_LCOM_ID).ToList();
                    List<int?> NegetiveCommoditiesExport = context.PSW_NEGATIVE_LIST_OF_COMMODITIES.Where(m => m.NLCOM_STATUS == true && m.NLCOM_PROFILE_TYPE == 2).Select(m => m.NLCOM_LCOM_ID).ToList();

                    if (NegetiveCommoditiesImport.Count > 0 || NegetiveCommoditiesExport.Count > 0)
                    {
                        List<string> NegetiveCommoditiesForImport = context.PSW_LIST_OF_COMMODITIES.Where(x => NegetiveCommoditiesImport.Contains(x.LCOM_ID)).Select(u => u.LCOM_CODE).ToList();
                        List<string> NegetiveCommoditiesForExport = context.PSW_LIST_OF_COMMODITIES.Where(x => NegetiveCommoditiesExport.Contains(x.LCOM_ID)).Select(u => u.LCOM_CODE).ToList();
                        if (NegetiveCommoditiesForImport.Count > 0 && NegetiveCommoditiesForExport.Count > 0)
                        {
                            SHARE_NEGATIVE_LIST_OF_COMMODITIES data = new SHARE_NEGATIVE_LIST_OF_COMMODITIES();
                            data.messageId = Guid.NewGuid().ToString();
                            data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                            data.senderId = "CBN";
                            data.receiverId = "PSW";
                            data.processingCode = "304";
                            data.data = new SHARE_NEGATIVE_LIST_OF_COMMODITIES.DetailData
                            {
                                iban = client.C_IBAN_NO,
                                restrictedCommoditiesForImport = NegetiveCommoditiesForImport,
                                restrictedCommoditiesForExport = NegetiveCommoditiesForExport
                            };
                            data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                            data.message = new SHARE_NEGATIVE_LIST_OF_COMMODITIES.MessageData
                            {
                                code = "200",
                                description = "Negative list of commodities shared."
                            };
                            ConvertMessageToEntity(data);
                            return Request.CreateResponse(HttpStatusCode.OK, data);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Negetive Commodities Found."));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Negetive Commodities Found."));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Sharing Data Failed, Invalid IBAN provided"));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message));
            }
        }
        #endregion

        #region ProcCode 305
        HttpResponseMessage ShareNegativeListOfSuppliers(string iban)
        {
            try
            {
                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == iban).FirstOrDefault();
                if (client != null)
                {
                    List<int?> NegetiveSuppliersImport = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_STATUS == true && m.NLOS_PROFILE_TYPE == 1).Select(m => m.NLOS_LOS_ID).ToList();
                    List<int?> NegetiveSuppliersExport = context.PSW_NEGATIVE_LIST_OF_SUPPLIERS.Where(m => m.NLOS_STATUS == true && m.NLOS_PROFILE_TYPE == 2).Select(m => m.NLOS_LOS_ID).ToList();

                    if (NegetiveSuppliersImport.Count > 0 || NegetiveSuppliersExport.Count > 0)
                    {
                        List<string> NegetiveSuppliersForImport = context.PSW_LIST_OF_SUPPLIERS.Where(x => NegetiveSuppliersImport.Contains(x.LOS_ID)).Select(u => u.LOS_NAME).ToList();
                        List<string> NegetiveSuppliersForExport = context.PSW_LIST_OF_SUPPLIERS.Where(x => NegetiveSuppliersExport.Contains(x.LOS_ID)).Select(u => u.LOS_NAME).ToList();
                        if (NegetiveSuppliersForImport.Count > 0 || NegetiveSuppliersForExport.Count > 0)
                        {
                            SHARE_NEGATIVE_LIST_OF_SUPPLIERS data = new SHARE_NEGATIVE_LIST_OF_SUPPLIERS();
                            data.messageId = Guid.NewGuid().ToString();
                            data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                            data.senderId = "CBN";
                            data.receiverId = "PSW";
                            data.processingCode = "305";
                            data.data = new SHARE_NEGATIVE_LIST_OF_SUPPLIERS.DetailData
                            {
                                iban = client.C_IBAN_NO,
                                restrictedSuppliersForImport = NegetiveSuppliersForImport,
                                restrictedSuppliersForExport = NegetiveSuppliersForExport
                            };
                            data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                            data.message = new SHARE_NEGATIVE_LIST_OF_SUPPLIERS.MessageData
                            {
                                code = "200",
                                description = "Negative list of suppliers shared."
                            };
                            ConvertMessageToEntity(data);
                            return Request.CreateResponse(HttpStatusCode.OK, data);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Negetive Suppliers Found."));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Negetive Suppliers Found."));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Sharing Data Failed, Invalid IBAN provided"));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message));
            }
        }
        #endregion

        #region ProcCode 101
        HttpResponseMessage ShareGDAndFinancialTransactionWithAD(dynamic RequestData)
        {
            try
            {
                bool CheckEIF = false;

                string EIF_REQ_NO = "";
                string GD_NO = "";
                string collectorate = "";
                string portOfDelivery = "";
                string terminalLocation = "";
                string consignmentCategory = "";
                string gdType = "";
                string gdStatus = "";
                string portOfDischarge = "";
                string ntnFtn = "";
                string strn = "";
                string consigneeName = "";
                string consigneeAddress = "";
                string consignorName = "";
                string consignorAddress = "";
                string grossWeight = "";
                string blAwbNumber = "";
                string blAwbDate = "";
                string clearanceDate = "";
                string portOfShipment = "";
                string netWeight = "";
                string modeOfPayment = "";
                string importerIban = "";
                string virAirNumber = "";
                string invoiceNumber = "";
                string invoiceDate = "";
                string deliveryTerm = "";

                EIF_REQ_NO = RequestData.data.financialInfo.finInsUniqueNumber;
                modeOfPayment = RequestData.data.financialInfo.modeOfPayment;
                importerIban = RequestData.data.financialInfo.importerIban;
                virAirNumber = RequestData.data.virAirNumber;
                invoiceNumber = RequestData.data.financialInfo.invoiceNumber;
                if (RequestData.data.financialInfo.invoiceDate != null)
                {
                    invoiceDate = DateTime.ParseExact(Convert.ToString(RequestData.data.financialInfo.invoiceDate),
                                  "yyyyMMdd",
                                   CultureInfo.InvariantCulture).ToString();
                }
                else
                    invoiceDate = null;
                var totalDeclaredValue = RequestData.data.financialInfo.totalDeclaredValue;
                deliveryTerm = RequestData.data.financialInfo.deliveryTerm;
                var fobValueUsd = RequestData.data.financialInfo.fobValueUsd;
                var freightUsd = RequestData.data.financialInfo.freightUsd;
                var cfrValueUsd = RequestData.data.financialInfo.cfrValueUsd;
                var insuranceUsd = RequestData.data.financialInfo.insuranceUsd;
                var landingChargesUsd = RequestData.data.financialInfo.landingChargesUsd;
                var assessedValueUsd = RequestData.data.financialInfo.assessedValueUsd;
                var otherCharges = RequestData.data.financialInfo.otherCharges;
                var exchangeRate = RequestData.data.financialInfo.exchangeRate;


                GD_NO = RequestData.data.gdNumber;
                collectorate = RequestData.data.collectorate;
                portOfDelivery = RequestData.data.generalInformation.portOfDelivery;
                terminalLocation = RequestData.data.generalInformation.terminalLocation;
                consignmentCategory = RequestData.data.consignmentCategory;
                gdType = RequestData.data.gdType;
                gdStatus = RequestData.data.gdStatus;
                portOfDischarge = RequestData.data.generalInformation.portOfDischarge;
                ntnFtn = RequestData.data.consignorConsigneeInfo.ntnFtn;
                strn = RequestData.data.consignorConsigneeInfo.strn;
                consigneeName = RequestData.data.consignorConsigneeInfo.consigneeName;
                consigneeAddress = RequestData.data.consignorConsigneeInfo.consigneeAddress;
                consignorName = RequestData.data.consignorConsigneeInfo.consignorName;
                consignorAddress = RequestData.data.consignorConsigneeInfo.consignorAddress;
                grossWeight = RequestData.data.generalInformation.grossWeight;
                blAwbNumber = RequestData.data.blAwbNumber;
                if (RequestData.data.blAwbDate != null)
                {
                    blAwbDate = DateTime.ParseExact(Convert.ToString(RequestData.data.blAwbDate),
                                  "yyyyMMdd",
                                   CultureInfo.InvariantCulture).ToString();
                }
                else
                    blAwbDate = null;
                if (RequestData.data.clearanceDate != null)
                {
                    clearanceDate = DateTime.ParseExact(Convert.ToString(RequestData.data.clearanceDate),
                                  "yyyyMMdd",
                                   CultureInfo.InvariantCulture).ToString();
                }
                else
                    clearanceDate = null;
                virAirNumber = RequestData.data.virAirNumber;

                portOfShipment = RequestData.data.generalInformation.portOfShipment;
                netWeight = RequestData.data.generalInformation.netWeight;
                if (modeOfPayment == "302")
                {
                    CheckEIF = true;
                }
                PSW_EIF_MASTER_DETAILS EIF = context.PSW_EIF_MASTER_DETAILS.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                if (EIF != null || CheckEIF == true)
                {
                    PSW_EIF_GD_GENERAL_INFO GeneralInfo = new PSW_EIF_GD_GENERAL_INFO();
                    GeneralInfo.EIGDGI_ID = Convert.ToInt32(context.PSW_EIF_GD_GENERAL_INFO.Max(m => (decimal?)m.EIGDGI_ID)) + 1;
                    GeneralInfo.EIGDGI_EIF_REQUEST_NO = EIF_REQ_NO;
                    GeneralInfo.EIGDGI_AGENT_NAME = "";
                    GeneralInfo.EIGDGI_TRADE_TYPE = "Import";
                    GeneralInfo.EIGDGI_GD_NO = GD_NO;
                    GeneralInfo.EIGDGI_COLLECTORATE = collectorate;
                    GeneralInfo.EIGDGI_DESTINATION_COUNTRY = "";
                    GeneralInfo.EIGDGI_PLACE_OF_DELIVERY = portOfDelivery;
                    GeneralInfo.EIGDGI_GENERAL_DESCRIPTION = "";
                    GeneralInfo.EIGDGI_SHED_TERMINAL_LOCATION = terminalLocation;
                    GeneralInfo.EIGDGI_AGENT_LICENSE_NO = "";
                    GeneralInfo.EIGDGI_DECLARATION_TYPE = "";
                    GeneralInfo.EIGDGI_CONSIGNMENT_CATE = consignmentCategory;
                    GeneralInfo.EIGDGI_GD_TYPE = gdType;
                    GeneralInfo.EIGDGI_GD_STATUS = gdStatus.Trim();
                    GeneralInfo.EIGDGI_PORT_OF_DISCHARGE = portOfDischarge;
                    GeneralInfo.EIGDGI_1ST_EXAMINATION = "";
                    GeneralInfo.EIGDGI_MODE_OF_PAYMENT = modeOfPayment.Trim();
                    GeneralInfo.EIGDGI_IMPORTER_IBAN = importerIban.Trim();
                    GeneralInfo.EIGDGI_VIR_AIR_NUM = virAirNumber.Trim();
                    GeneralInfo.EIGDGI_INVOICE_NUM = (invoiceNumber == null) ? invoiceNumber : invoiceNumber.Trim();
                    if (invoiceDate != null)
                        GeneralInfo.EIGDGI_INVOICE_DATE = Convert.ToDateTime(invoiceDate);
                    GeneralInfo.EIGDGI_TOTAL_DECLARED_VALUE = Convert.ToDecimal(totalDeclaredValue);
                    GeneralInfo.EIGDGI_DELIVERY_TERM = deliveryTerm.Trim();
                    GeneralInfo.EIGDGI_FOBVALUEUSD = Convert.ToDecimal(fobValueUsd);
                    GeneralInfo.EIGDGI_FRIEGHTUSD = Convert.ToDecimal(freightUsd);
                    GeneralInfo.EIGDGI_CRFVALUEUSD = Convert.ToDecimal(cfrValueUsd);
                    GeneralInfo.EIGDGI_INSURANCE_VALUE_USD = Convert.ToDecimal(insuranceUsd);
                    GeneralInfo.EIGDGI_LANDING_CHARGES = Convert.ToDecimal(landingChargesUsd);
                    GeneralInfo.EIGDGI_ASSESSED_VALUE_USD = Convert.ToDecimal(assessedValueUsd);
                    GeneralInfo.EIGDGI_OTHER_CHARGES = Convert.ToDecimal(otherCharges);
                    GeneralInfo.EIGDGI_EXCHANGE_RATE = Convert.ToDecimal(exchangeRate);
                    GeneralInfo.EIGDGI_ENTRY_DATETIME = DateTime.Now;
                    GeneralInfo.EIGDGI_MAKER_ID = "PSW";
                    if (clearanceDate != null && clearanceDate != "")
                        GeneralInfo.EIGDGI_CLEARANCE_DATE = Convert.ToDateTime(clearanceDate);
                    else
                        GeneralInfo.EIGDGI_CLEARANCE_DATE = null;
                    GeneralInfo.EIGDGI_AMENDMENT_NO = Convert.ToInt32(context.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_GD_NO == GeneralInfo.EIGDGI_GD_NO).Max(m => (decimal?)m.EIGDGI_AMENDMENT_NO)) + 1;
                    if (RequestData.data.negativeList != null)
                    {
                        GeneralInfo.EIGDGI_NEG_COUNTRY = RequestData.data.negativeList.country;
                        GeneralInfo.EIGDGI_NEG_SUPPLIER = RequestData.data.negativeList.supplier;
                        Array NEGCOMS = RequestData.data.negativeList.commodities;
                        int ComCount = 0;
                        string NEGCOMString = "";
                        if (NEGCOMS != null)
                        {
                            foreach (string Coms in NEGCOMS)
                            {
                                if (ComCount == 0)
                                    NEGCOMString += Coms;
                                else
                                    NEGCOMString += "," + Coms;
                                ComCount += 1;
                            }
                        }
                        GeneralInfo.EIGDGI_NEG_COMMS = NEGCOMString;
                    }
                    context.PSW_EIF_GD_GENERAL_INFO.Add(GeneralInfo);
                    int RowCount = context.SaveChanges();
                    if (RowCount > 0)
                    {
                        PSW_EIF_GD_IMPORT_EXPORT_INFO IMPORTEXPORTInfo = new PSW_EIF_GD_IMPORT_EXPORT_INFO();
                        IMPORTEXPORTInfo.EIGDIE_ID = Convert.ToInt32(context.PSW_EIF_GD_IMPORT_EXPORT_INFO.Max(m => (decimal?)m.EIGDIE_ID)) + 1;
                        IMPORTEXPORTInfo.EIGDIE_EIF_REQUEST_NO = EIF_REQ_NO;
                        IMPORTEXPORTInfo.EIGDIE_NTN_FTN = ntnFtn;
                        IMPORTEXPORTInfo.EIGDIE_STRN = strn;
                        IMPORTEXPORTInfo.EIGDIE_GD_NO = GD_NO;
                        IMPORTEXPORTInfo.EIGDIE_CONSIGNEE_NAME = consigneeName;
                        IMPORTEXPORTInfo.EIGDIE_CONSIGNEE_ADDRESS = consigneeAddress;
                        IMPORTEXPORTInfo.EIGDIE_CONSIGNOR_NAME = consignorName;
                        IMPORTEXPORTInfo.EIGDIE_CONSIGNOR_ADDRESS = consignorAddress;
                        IMPORTEXPORTInfo.EIGDIE_ENTRY_DATETIME = DateTime.Now;
                        IMPORTEXPORTInfo.EIGDIE_MAKER_ID = "PSW";
                        IMPORTEXPORTInfo.EIGDIE_AMENDMENT_NO = GeneralInfo.EIGDGI_AMENDMENT_NO;
                        context.PSW_EIF_GD_IMPORT_EXPORT_INFO.Add(IMPORTEXPORTInfo);
                        RowCount = context.SaveChanges();
                        if (RowCount > 0)
                        {
                            PSW_EIF_GD_INFORMATION GDInfo = new PSW_EIF_GD_INFORMATION();
                            GDInfo.EIGDI_ID = Convert.ToInt32(context.PSW_EIF_GD_INFORMATION.Max(m => (decimal?)m.EIGDI_ID)) + 1;
                            GDInfo.EIGDI_EIF_REQUEST_NO = EIF_REQ_NO;
                            GDInfo.EIGDI_EGM_COLLECTORATE = collectorate;
                            GDInfo.EIGDI_GD_NO = GD_NO;
                            GDInfo.EIGDI_EGM_NUMBER = "-";
                            GDInfo.EIGDI_EGM_EXPORTER_NAME = "-";
                            GDInfo.EIGDI_VESSEL_NAME = "";
                            GDInfo.EIGDI_GROSS_WEIGHT = grossWeight;
                            GDInfo.EIGDI_COSIGNMENT_TYPE = "";
                            GDInfo.EIGDI_SECTION = "";
                            GDInfo.EIGDI_INDEX_NO = "";
                            GDInfo.EIGDI_BL_NO = blAwbNumber;
                            GDInfo.EIGDI_BL_DATE = Convert.ToDateTime(blAwbDate);
                            GDInfo.EIGDI_PORT_OF_SHIPMENT = portOfShipment;
                            GDInfo.EIGDI_NET_WEIGHT = netWeight;
                            GDInfo.EIGDI_SHIPPING_LINE = "";
                            GDInfo.EIGDI_BERTHING_TERMINAL = "";
                            GDInfo.EIGDI_ENTRY_DATETIME = DateTime.Now;
                            GDInfo.EIGDI_MAKER_ID = "PSW";
                            GDInfo.EIGDI_AMENDMENT_NO = GeneralInfo.EIGDGI_AMENDMENT_NO;
                            context.PSW_EIF_GD_INFORMATION.Add(GDInfo);
                            RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                int CurrentRow = 0;
                                if (RequestData.data.generalInformation.packagesInformation != null)
                                {
                                    if (RequestData.data.generalInformation.packagesInformation.Count > 0)
                                    {
                                        foreach (dynamic i in RequestData.data.generalInformation.packagesInformation)
                                        {
                                            PSW_GD_IMPORT_PACKAGE_INFORMATION Packages = new PSW_GD_IMPORT_PACKAGE_INFORMATION();
                                            int MAXID = Convert.ToInt32(context.PSW_GD_IMPORT_PACKAGE_INFORMATION.Max(m => (decimal?)m.IPI_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                Packages.IPI_ID = MAXID;
                                            }
                                            else
                                            {
                                                Packages.IPI_ID = (MAXID + CurrentRow);
                                            }
                                            var IPI_NO_OF_PACKAGE = i.numberOfPackages;
                                            Packages.IPI_NO_OF_PACKAGE = IPI_NO_OF_PACKAGE;
                                            var IPI_PACKAGE_TYPE = i.packageType;
                                            Packages.IPI_PACKAGE_TYPE = IPI_PACKAGE_TYPE;
                                            Packages.IPI_GD_NO = GD_NO;
                                            Packages.IPI_ENTRY_DATETIME = DateTime.Now;
                                            Packages.IPI_MAKER_ID = "PSW";
                                            Packages.IPI_AMENDMENT_NO = GeneralInfo.EIGDGI_AMENDMENT_NO;
                                            context.PSW_GD_IMPORT_PACKAGE_INFORMATION.Add(Packages);
                                            CurrentRow += 1;
                                        }
                                        RowCount = context.SaveChanges();
                                    }
                                }
                                if (RowCount > 0)
                                {
                                    CurrentRow = 0;
                                    if (RequestData.data.generalInformation.containerVehicleInformation != null)
                                    {
                                        if (RequestData.data.generalInformation.containerVehicleInformation.Count > 0)
                                        {
                                            foreach (dynamic i in RequestData.data.generalInformation.containerVehicleInformation)
                                            {
                                                PSW_GD_IMPORT_CONTAINER_INFORMATION Containers = new PSW_GD_IMPORT_CONTAINER_INFORMATION();
                                                int MAXID = Convert.ToInt32(context.PSW_GD_IMPORT_CONTAINER_INFORMATION.Max(m => (decimal?)m.CII_ID)) + 1;
                                                if (CurrentRow == 0)
                                                {
                                                    Containers.CII_ID = MAXID;
                                                }
                                                else
                                                {
                                                    Containers.CII_ID = (MAXID + CurrentRow);
                                                }
                                                var CII_CONTAINER_OR_TRUCK_NO = i.containerOrTruckNumber;
                                                Containers.CII_CONTAINER_OR_TRUCK_NO = CII_CONTAINER_OR_TRUCK_NO;
                                                var CII_SERIAL_NO = i.sealNumber;
                                                Containers.CII_SERIAL_NO = CII_SERIAL_NO;
                                                var CII_CONTAINER_TYPE = i.containerType;
                                                Containers.CII_CONTAINER_TYPE = CII_CONTAINER_TYPE;
                                                Containers.CII_GD_NO = GD_NO;
                                                Containers.CII_ENTRY_DATETIME = DateTime.Now;
                                                Containers.CII_MAKER_ID = "PSW";
                                                Containers.CII_AMENDMENT_NO = GeneralInfo.EIGDGI_AMENDMENT_NO;
                                                context.PSW_GD_IMPORT_CONTAINER_INFORMATION.Add(Containers);
                                                CurrentRow += 1;
                                            }
                                            RowCount = context.SaveChanges();
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        CurrentRow = 0;
                                        if (RequestData.data.itemInformation != null)
                                        {
                                            foreach (dynamic i in RequestData.data.itemInformation)
                                            {
                                                PSW_GD_IMORT_HS_CODE Items = new PSW_GD_IMORT_HS_CODE();
                                                int MAXID = Convert.ToInt32(context.PSW_GD_IMORT_HS_CODE.Max(m => (decimal?)m.IHC_ID)) + 1;
                                                if (CurrentRow == 0)
                                                {
                                                    Items.IHC_ID = MAXID;
                                                }
                                                else
                                                {
                                                    Items.IHC_ID = (MAXID + CurrentRow);
                                                }
                                                var IHC_HS_CODE = i.hsCode;
                                                Items.IHC_HS_CODE = Convert.ToDecimal(IHC_HS_CODE);
                                                var IHC_QUANTITY = i.quantity;
                                                Items.IHC_QUANTITY = Convert.ToDecimal(IHC_QUANTITY);
                                                var IHC_UNIT_PRICE = i.unitPrice;
                                                Items.IHC_UNIT_PRICE = Convert.ToDecimal(IHC_UNIT_PRICE);
                                                var IHC_TOTAL_VALUE = i.totalValue;
                                                Items.IHC_TOTAL_VALUE = Convert.ToDecimal(IHC_TOTAL_VALUE);
                                                var IHC_IMPORTER_VALUE = i.importValue;
                                                Items.IHC_IMPORTER_VALUE = Convert.ToDecimal(IHC_IMPORTER_VALUE);
                                                var IHC_UOM = i.uom;
                                                Items.IHC_UOM = IHC_UOM;
                                                Items.IHC_GD_NO = GD_NO;
                                                Items.IHC_ENTRY_DATETIME = DateTime.Now;
                                                Items.IHC_MAKER_ID = "PSW";
                                                Items.IHC_AMENDMENT_NO = GeneralInfo.EIGDGI_AMENDMENT_NO;
                                                context.PSW_GD_IMORT_HS_CODE.Add(Items);
                                                CurrentRow += 1;
                                            }
                                            RowCount = context.SaveChanges();
                                        }
                                        if (RowCount > 0)
                                        {
                                            string success = EMAILING.EIF_GD(EIF_REQ_NO);
                                            SHARE_GDNFinancialInfoWithADResponse data = new SHARE_GDNFinancialInfoWithADResponse();
                                            data.messageId = Guid.NewGuid().ToString();
                                            data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                                            data.senderId = "CBN";
                                            data.receiverId = "PSW";
                                            data.processingCode = "101";
                                            data.data = new SHARE_GDNFinancialInfoWithADResponse.DetailData { };
                                            data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                                            data.message = new SHARE_GDNFinancialInfoWithADResponse.MessageData
                                            {
                                                code = "200",
                                                description = "Updated GD and financial information."
                                            };
                                            ConvertMessageToEntity(data);
                                            return Request.CreateResponse(HttpStatusCode.OK, data);
                                        }
                                        else
                                        {
                                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Items Information"));
                                        }
                                    }
                                    else
                                    {
                                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Containers Information"));
                                    }
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Packages Information"));
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Data GD Information"));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Data GD Import Export Info"));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Data GD General Info"));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Data Found Against The Financial Instrument No : " + EIF_REQ_NO));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message));
            }
        }
        #endregion

        #region ProcCode 102
        HttpResponseMessage ShareGDAndFinancialTransactionWithADExport(dynamic RequestData)
        {
            try
            {
                string GD_NO = "";
                string collectorate = "";
                string portOfDelivery = "";
                string terminalLocation = "";
                string consignmentCategory = "";
                string gdType = "";
                string gdStatus = "";
                string portOfDischarge = "";
                string ntnFtn = "";
                string strn = "";
                string consigneeName = "";
                string consigneeAddress = "";
                string consignorName = "";
                string consignorAddress = "";
                string grossWeight = "";
                string blAwbNumber = "";
                string blAwbDate = "";
                string clearanceDate = "";
                string virAirNumber = "";
                string portOfShipment = "";
                string netWeight = "";

                string exporterIban = "";
                string modeOfPayment = "";
                string finInsUniqueNumber = "";
                string currency = "";
                string invoiceNumber = "";
                string invoiceDate = "";
                string deliveryTerm = "";
                string consignmentType = "";
                string shippingLine = "";

                GD_NO = RequestData.data.gdNumber;
                collectorate = RequestData.data.collectorate;
                portOfDelivery = RequestData.data.generalInformation.placeOfDelivery;
                terminalLocation = RequestData.data.generalInformation.terminalLocation;
                consignmentCategory = RequestData.data.consignmentCategory;
                gdType = RequestData.data.gdType;
                gdStatus = RequestData.data.gdStatus;
                portOfDischarge = RequestData.data.generalInformation.portOfDischarge;
                ntnFtn = RequestData.data.consignorConsigneeInfo.ntnFtn;
                strn = RequestData.data.consignorConsigneeInfo.strn;
                consigneeName = RequestData.data.consignorConsigneeInfo.consigneeName;
                consigneeAddress = RequestData.data.consignorConsigneeInfo.consigneeAddress;
                consignorName = RequestData.data.consignorConsigneeInfo.consignorName;
                consignorAddress = RequestData.data.consignorConsigneeInfo.consignorAddress;
                grossWeight = RequestData.data.generalInformation.grossWeight;
                blAwbNumber = RequestData.data.blAwbNumber;
                if (RequestData.data.blAwbDate != null)
                {
                    blAwbDate = DateTime.ParseExact(Convert.ToString(RequestData.data.blAwbDate),
                                                      "yyyyMMdd",
                                                       CultureInfo.InvariantCulture).ToString();
                    virAirNumber = RequestData.data.virAirNumber;
                }
                if (RequestData.data.clearanceDate != null)
                {
                    clearanceDate = DateTime.ParseExact(Convert.ToString(RequestData.data.clearanceDate),
                                                      "yyyyMMdd",
                                                       CultureInfo.InvariantCulture).ToString();
                }
                else
                    clearanceDate = null;
                portOfShipment = RequestData.data.generalInformation.portOfShipment;
                netWeight = RequestData.data.generalInformation.netWeight;
                exporterIban = RequestData.data.financialInformation.financialInstrument[0].exporterIban;
                modeOfPayment = RequestData.data.financialInformation.financialInstrument[0].modeOfPayment;
                finInsUniqueNumber = RequestData.data.financialInformation.financialInstrument[0].finInsUniqueNumber;
                currency = RequestData.data.financialInformation.currency;
                invoiceNumber = RequestData.data.financialInformation.invoiceNumber;
                if (RequestData.data.financialInformation.invoiceDate != null)
                {
                    invoiceDate = DateTime.ParseExact(Convert.ToString(RequestData.data.financialInformation.invoiceDate),
                                  "yyyyMMdd",
                                   CultureInfo.InvariantCulture).ToString();
                }
                deliveryTerm = RequestData.data.financialInformation.deliveryTerm;
                consignmentType = RequestData.data.generalInformation.consignmentType;
                shippingLine = RequestData.data.generalInformation.shippingLine;

                var totalDeclaredValue = RequestData.data.financialInformation.totalDeclaredValue;
                var fobValueUsd = RequestData.data.financialInformation.fobValueUsd;
                var freightUsd = RequestData.data.financialInformation.freightUsd;
                var cfrValueUsd = RequestData.data.financialInformation.cfrValueUsd;
                var insuranceUsd = RequestData.data.financialInformation.insuranceUsd;
                var landingChargesUsd = RequestData.data.financialInformation.landingChargesUsd;
                var assessedValueUsd = RequestData.data.financialInformation.assessedValueUsd;
                var OtherCharges = RequestData.data.financialInformation.OtherCharges;
                var exchangeRate = RequestData.data.financialInformation.exchangeRate;
                PSW_EEF_GD_GENERAL_INFO GeneralInfo = new PSW_EEF_GD_GENERAL_INFO();
                GeneralInfo.EFGDGI_ID = Convert.ToInt32(context.PSW_EEF_GD_GENERAL_INFO.Max(m => (decimal?)m.EFGDGI_ID)) + 1;
                GeneralInfo.EFGDGI_AGENT_NAME = "";
                GeneralInfo.EFGDGI_TRADE_TYPE = "Export";
                GeneralInfo.EFGDGI_GD_NO = GD_NO;
                GeneralInfo.EFGDGI_COLLECTORATE = collectorate;
                GeneralInfo.EFGDGI_DESTINATION_COUNTRY = "";
                GeneralInfo.EFGDGI_PLACE_OF_DELIVERY = portOfDelivery;
                GeneralInfo.EFGDGI_GENERAL_DESCRIPTION = "";
                GeneralInfo.EFGDGI_SHED_TERMINAL_LOCATION = terminalLocation;
                GeneralInfo.EFGDGI_AGENT_LICENSE_NO = "";
                GeneralInfo.EFGDGI_DECLARATION_TYPE = "";
                GeneralInfo.EFGDGI_CONSIGNMENT_CATE = consignmentCategory;
                GeneralInfo.EFGDGI_GD_TYPE = gdType;
                GeneralInfo.EFGDGI_GD_STATUS = gdStatus.Trim();
                GeneralInfo.EFGDGI_PORT_OF_DISCHARGE = portOfDischarge;
                GeneralInfo.EFGDGI_1ST_EXAMINATION = "";
                GeneralInfo.EFGDGI_ENTRY_DATETIME = DateTime.Now;
                GeneralInfo.EFGDGI_MAKER_ID = "PSW";
                GeneralInfo.EFGDGI_VIR_AIR_NUMBER = virAirNumber;
                GeneralInfo.EFGDGI_EXPORTER_IBAN = exporterIban;
                GeneralInfo.EFGDGI_MODE_OF_PAYMENT = modeOfPayment;
                GeneralInfo.EFGDGI_CURRENCY = currency;
                GeneralInfo.EFGDGI_TOTAL_DECLARED_VALUE = totalDeclaredValue;
                GeneralInfo.EFGDGI_INVOICE_NUMBER = invoiceNumber;
                if (invoiceDate != "" && invoiceDate != null)
                    GeneralInfo.EFGDGI_INVOICE_DATE = Convert.ToDateTime(invoiceDate);
                GeneralInfo.EFGDGI_DELIVERY_TERM = deliveryTerm;
                GeneralInfo.EFGDGI_FOBVALUE_USD = fobValueUsd;
                GeneralInfo.EFGDGI_FREIGHT_USD = freightUsd;
                GeneralInfo.EFGDGI_CFR_VALUE_USD = cfrValueUsd;
                GeneralInfo.EFGDGI_INSURANCE_USD = insuranceUsd;
                GeneralInfo.EFGDGI_LANDING_CHARGES_USD = landingChargesUsd;
                GeneralInfo.EFGDGI_ASSESSED_VALUE_USD = assessedValueUsd;
                GeneralInfo.EFGDGI_OTHER_CHARGES = OtherCharges;
                GeneralInfo.EFGDGI_EXCHANGE_RATE = exchangeRate;
                GeneralInfo.EFGDGI_CONSIGNMENT_TYPE = consignmentType;
                GeneralInfo.EFGDGI_SHIPPING_LINE = shippingLine;
                if (clearanceDate != null && clearanceDate != "")
                    GeneralInfo.EFGDGI_CLEARANCE_DATE = Convert.ToDateTime(clearanceDate);
                else
                    GeneralInfo.EFGDGI_CLEARANCE_DATE = null;
                GeneralInfo.EFGDGI_AMENDMENT_NO = Convert.ToInt32(context.PSW_EEF_GD_GENERAL_INFO.Where(m => m.EFGDGI_GD_NO == GeneralInfo.EFGDGI_GD_NO).Max(m => (decimal?)m.EFGDGI_AMENDMENT_NO)) + 1;
                if (RequestData.data.negativeList != null)
                {
                    GeneralInfo.EFGDGI_NEG_COUNTRY = RequestData.data.negativeList.country;
                    GeneralInfo.EFGDGI_NEG_SUPPLIER = RequestData.data.negativeList.supplier;
                    Array NEGCOMS = RequestData.data.negativeList.commodities;
                    int ComCount = 0;
                    string NEGCOMString = "";
                    foreach (string Coms in NEGCOMS)
                    {
                        if (ComCount == 0)
                            NEGCOMString += Coms;
                        else
                            NEGCOMString += "," + Coms;
                        ComCount += 1;
                    }
                    GeneralInfo.EFGDGI_NEG_COMMS = NEGCOMString;
                }
                context.PSW_EEF_GD_GENERAL_INFO.Add(GeneralInfo);
                int RowCount = context.SaveChanges();
                if (RowCount > 0)
                {
                    {
                        int CurrRow = 0;
                        foreach (dynamic i in RequestData.data.financialInformation.financialInstrument)
                        {
                            PSW_GD_ASSIGN_EFORMS AssignedForms = new PSW_GD_ASSIGN_EFORMS();
                            int MAXID = Convert.ToInt32(context.PSW_GD_ASSIGN_EFORMS.Max(m => (decimal?)m.GDAEF_ID)) + 1;
                            if (CurrRow == 0)
                            {
                                AssignedForms.GDAEF_ID = MAXID;
                            }
                            else
                            {
                                AssignedForms.GDAEF_ID = (MAXID + CurrRow);
                            }
                            var FORM_E_NO = i.finInsUniqueNumber;
                            AssignedForms.GDAEF_E_FORM = FORM_E_NO;
                            var consumedValue = i.finInsConsumedValue;
                            AssignedForms.GDAEF_VALUE_CONSUMED = consumedValue;
                            AssignedForms.GDAEF_GD_NO = GD_NO;
                            AssignedForms.GDAEF_ENTRY_DATETIME = DateTime.Now;
                            AssignedForms.GDAEF_AMENDMENT_NO = GeneralInfo.EFGDGI_AMENDMENT_NO;
                            context.PSW_GD_ASSIGN_EFORMS.Add(AssignedForms);
                            CurrRow += 1;
                        }
                        RowCount += context.SaveChanges();
                        PSW_EEF_GD_IMPORT_EXPORT_INFO IMPORTEXPORTInfo = new PSW_EEF_GD_IMPORT_EXPORT_INFO();
                        IMPORTEXPORTInfo.EFGDIE_ID = Convert.ToInt32(context.PSW_EEF_GD_IMPORT_EXPORT_INFO.Max(m => (decimal?)m.EFGDIE_ID)) + 1;
                        IMPORTEXPORTInfo.EFGDIE_NTN_FTN = ntnFtn;
                        IMPORTEXPORTInfo.EFGDIE_STRN = strn;
                        IMPORTEXPORTInfo.EFGDIE_GD_NO = GD_NO;
                        IMPORTEXPORTInfo.EFGDIE_CONSIGNEE_NAME = consigneeName;
                        IMPORTEXPORTInfo.EFGDIE_CONSIGNEE_ADDRESS = consigneeAddress;
                        IMPORTEXPORTInfo.EFGDIE_CONSIGNOR_NAME = consignorName;
                        IMPORTEXPORTInfo.EFGDIE_CONSIGNOR_ADDRESS = consignorAddress;
                        IMPORTEXPORTInfo.EFGDIE_ENTRY_DATETIME = DateTime.Now;
                        IMPORTEXPORTInfo.EFGDIE_MAKER_ID = "PSW";
                        IMPORTEXPORTInfo.EFGDIE_AMENDMENT_NO = GeneralInfo.EFGDGI_AMENDMENT_NO;
                        context.PSW_EEF_GD_IMPORT_EXPORT_INFO.Add(IMPORTEXPORTInfo);
                        RowCount = context.SaveChanges();
                        if (RowCount > 0)
                        {
                            PSW_EEF_GD_INFORMATION GDInfo = new PSW_EEF_GD_INFORMATION();
                            GDInfo.EFGDI_ID = Convert.ToInt32(context.PSW_EEF_GD_INFORMATION.Max(m => (decimal?)m.EFGDI_ID)) + 1;
                            GDInfo.EFGDI_EGM_COLLECTORATE = collectorate;
                            GDInfo.EFGDI_GD_NO = GD_NO;
                            GDInfo.EFGDI_EGM_NUMBER = "-";
                            GDInfo.EFGDI_EGM_EXPORTER_NAME = "-";
                            GDInfo.EFGDI_VESSEL_NAME = "";
                            GDInfo.EFGDI_GROSS_WEIGHT = grossWeight;
                            GDInfo.EFGDI_COSIGNMENT_TYPE = "";
                            GDInfo.EFGDI_SECTION = "";
                            GDInfo.EFGDI_INDEX_NO = "";
                            GDInfo.EFGDI_BL_NO = blAwbNumber;
                            if (blAwbDate != null && blAwbDate != "")
                                GDInfo.EFGDI_BL_DATE = Convert.ToDateTime(blAwbDate);
                            GDInfo.EFGDI_PORT_OF_SHIPMENT = portOfShipment;
                            GDInfo.EFGDI_NET_WEIGHT = netWeight;
                            GDInfo.EFGDI_SHIPPING_LINE = "";
                            GDInfo.EFGDI_BERTHING_TERMINAL = "";
                            GDInfo.EFGDI_ENTRY_DATETIME = DateTime.Now;
                            GDInfo.EFGDI_MAKER_ID = "PSW";
                            GDInfo.EFGDI_AMENDMENT_NO = GeneralInfo.EFGDGI_AMENDMENT_NO;
                            context.PSW_EEF_GD_INFORMATION.Add(GDInfo);
                            RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                int CurrentRow = 0;
                                if (RequestData.data.generalInformation.packagesInformation != null)
                                {
                                    if (RequestData.data.generalInformation.packagesInformation.Count > 0)
                                    {
                                        foreach (dynamic i in RequestData.data.generalInformation.packagesInformation)
                                        {
                                            PSW_GD_EXPORT_PACKAGE_INFORMATION Packages = new PSW_GD_EXPORT_PACKAGE_INFORMATION();
                                            int MAXID = Convert.ToInt32(context.PSW_GD_EXPORT_PACKAGE_INFORMATION.Max(m => (decimal?)m.EPI_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                Packages.EPI_ID = MAXID;
                                            }
                                            else
                                            {
                                                Packages.EPI_ID = (MAXID + CurrentRow);
                                            }
                                            var EPI_NO_OF_PACKAGE = i.numberOfPackages;
                                            Packages.EPI_NO_OF_PACKAGE = EPI_NO_OF_PACKAGE;
                                            var IPI_PACKAGE_TYPE = i.packageType;
                                            Packages.EPI_PACKAGE_TYPE = IPI_PACKAGE_TYPE;
                                            Packages.EPI_GD_NO = GD_NO;
                                            Packages.EPI_ENTRY_DATETIME = DateTime.Now;
                                            Packages.EPI_MAKER_ID = "PSW";
                                            Packages.EPI_AMENDMENT_NO = GeneralInfo.EFGDGI_AMENDMENT_NO;
                                            context.PSW_GD_EXPORT_PACKAGE_INFORMATION.Add(Packages);
                                            CurrentRow += 1;
                                        }
                                        RowCount = context.SaveChanges();
                                    }
                                }
                                if (RowCount > 0)
                                {
                                    List<PSW_GD_EXPORT_CONTAINER_INFORMATION> DeletePreviousContainers = context.PSW_GD_EXPORT_CONTAINER_INFORMATION.Where(w => w.CIE_GD_NO == GD_NO).ToList();
                                    foreach (PSW_GD_EXPORT_CONTAINER_INFORMATION row in DeletePreviousContainers)
                                    {
                                        context.PSW_GD_EXPORT_CONTAINER_INFORMATION.Remove(row);
                                    }
                                    CurrentRow = 0;
                                    if (RequestData.data.generalInformation.containerVehicleInformation != null)
                                    {
                                        if (RequestData.data.generalInformation.containerVehicleInformation.Count > 0)
                                        {
                                            foreach (dynamic i in RequestData.data.generalInformation.containerVehicleInformation)
                                            {
                                                PSW_GD_EXPORT_CONTAINER_INFORMATION Containers = new PSW_GD_EXPORT_CONTAINER_INFORMATION();
                                                int MAXID = Convert.ToInt32(context.PSW_GD_EXPORT_CONTAINER_INFORMATION.Max(m => (decimal?)m.CIE_ID)) + 1;
                                                if (CurrentRow == 0)
                                                {
                                                    Containers.CIE_ID = MAXID;
                                                }
                                                else
                                                {
                                                    Containers.CIE_ID = (MAXID + CurrentRow);
                                                }
                                                var CIE_CONTAINER_OR_TRUCK_NO = i.containerOrTruckNumber;
                                                Containers.CIE_CONTAINER_OR_TRUCK_NO = CIE_CONTAINER_OR_TRUCK_NO;
                                                var CIE_SERIAL_NO = i.sealNumber;
                                                Containers.CIE_SERIAL_NO = CIE_SERIAL_NO;
                                                var CIE_CONTAINER_TYPE = i.containerType;
                                                Containers.CIE_CONTAINER_TYPE = CIE_CONTAINER_TYPE;
                                                Containers.CIE_GD_NO = GD_NO;
                                                Containers.CIE_ENTRY_DATETIME = DateTime.Now;
                                                Containers.CIE_MAKER_ID = "PSW";
                                                Containers.CIE_AMENDMENT_NO = GeneralInfo.EFGDGI_AMENDMENT_NO;
                                                context.PSW_GD_EXPORT_CONTAINER_INFORMATION.Add(Containers);
                                                CurrentRow += 1;
                                            }
                                            RowCount = context.SaveChanges();
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        CurrentRow = 0;
                                        foreach (dynamic i in RequestData.data.itemInformation)
                                        {
                                            PSW_GD_EXPORT_HS_CODE Items = new PSW_GD_EXPORT_HS_CODE();
                                            int MAXID = Convert.ToInt32(context.PSW_GD_EXPORT_HS_CODE.Max(m => (decimal?)m.EHC_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                Items.EHC_ID = MAXID;
                                            }
                                            else
                                            {
                                                Items.EHC_ID = (MAXID + CurrentRow);
                                            }
                                            var EHC_HS_CODE = i.hsCode;
                                            Items.EHC_HS_CODE = Convert.ToDecimal(EHC_HS_CODE);
                                            var EHC_QUANTITY = i.quantity;
                                            Items.EHC_QUANTITY = Convert.ToDecimal(EHC_QUANTITY);
                                            var EHC_UNIT_PRICE = i.unitPrice;
                                            Items.EHC_UNIT_PRICE = Convert.ToDecimal(EHC_UNIT_PRICE);
                                            var EHC_TOTAL_VALUE = i.totalValue;
                                            Items.EHC_TOTAL_VALUE = Convert.ToDecimal(EHC_TOTAL_VALUE);
                                            var EHC_EXPORTER_VALUE = i.exportValue;
                                            Items.EHC_EXPORTER_VALUE = Convert.ToDecimal(EHC_EXPORTER_VALUE);
                                            var EHC_UOM = i.uom;
                                            Items.EHC_UOM = EHC_UOM;
                                            Items.EHC_GD_NO = GD_NO;
                                            Items.EHC_ENTRY_DATETIME = DateTime.Now;
                                            Items.EHC_MAKER_ID = "PSW";
                                            Items.EHC_AMENDMENT_NO = GeneralInfo.EFGDGI_AMENDMENT_NO;
                                            context.PSW_GD_EXPORT_HS_CODE.Add(Items);
                                            CurrentRow += 1;
                                        }
                                        RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            string success = EMAILING.EEF_GD(exporterIban);
                                            SHARE_GDNFinancialInfoWithADExportResponse data = new SHARE_GDNFinancialInfoWithADExportResponse();
                                            data.messageId = Guid.NewGuid().ToString();
                                            data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                                            data.senderId = "CBN";
                                            data.receiverId = "PSW";
                                            data.processingCode = "102";
                                            data.data = new SHARE_GDNFinancialInfoWithADExportResponse.DetailData { };
                                            data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                                            data.message = new SHARE_GDNFinancialInfoWithADExportResponse.MessageData
                                            {
                                                code = "200",
                                                description = "Updated GD and financial information."
                                            };
                                            ConvertMessageToEntity(data);
                                            return Request.CreateResponse(HttpStatusCode.OK, data);
                                        }
                                        else
                                        {
                                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Items Information"));
                                        }
                                    }
                                    else
                                    {
                                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Containers Information"));
                                    }

                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Packages Information"));
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Data GD Information"));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured While Saving Data GD Import Export Info"));
                        }
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error While Saving GD General Information"));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message));
            }
        }
        #endregion

        #region ProcCode 306
        HttpResponseMessage ShareCOBWithAD(dynamic RequestData)
        {
            try
            {
                string cobUniqueIdNumber = RequestData.data.cobUniqueIdNumber;
                string tradeTranType = RequestData.data.tradeTranType;
                string iban = RequestData.data.iban;
                string traderNTN = RequestData.data.traderNTN;
                string traderName = RequestData.data.traderName;
                string finInsUniqueNumber = RequestData.data.finInsUniqueNumber;

                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == iban && m.C_NTN_NO == traderNTN).FirstOrDefault();
                if (client != null)
                {
                    if (tradeTranType == "01")
                    {
                        PSW_EIF_BASIC_INFO eifbasic = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == finInsUniqueNumber).FirstOrDefault();
                        if (eifbasic != null)
                        {
                            PSW_CHANGE_OF_BANK_REQUEST NewReq = new PSW_CHANGE_OF_BANK_REQUEST();
                            NewReq.COB_ID = Convert.ToInt32(context.PSW_CHANGE_OF_BANK_REQUEST.Max(m => (decimal?)m.COB_ID)) + 1;
                            NewReq.COB_UNIQUE_NO = cobUniqueIdNumber;
                            NewReq.COB_TRANS_TYPE = tradeTranType;
                            NewReq.COB_IBAN = iban;
                            NewReq.COB_REQUEST_NO = finInsUniqueNumber;
                            NewReq.COB_STATUS = "";
                            NewReq.COB_DATA_ENTRY_DATETIME = DateTime.Now;
                            NewReq.METHODID = 1538;
                            context.PSW_CHANGE_OF_BANK_REQUEST.Add(NewReq);
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                ShareChangeOfBankRequestWithADReponse data = new ShareChangeOfBankRequestWithADReponse();
                                data.messageId = Guid.NewGuid().ToString();
                                data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                                data.senderId = "CBN";
                                data.receiverId = "PSW";
                                data.processingCode = "306";
                                data.data = new ShareChangeOfBankRequestWithADReponse.DetailData { };
                                data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                                data.message = new ShareChangeOfBankRequestWithADReponse.MessageData
                                {
                                    code = "200",
                                    description = "Change of bank request status acknowledged."
                                };
                                ConvertMessageToEntity(data);
                                return Request.CreateResponse(HttpStatusCode.OK, data);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured while saving the change of bank request."));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No EIF Exist in the system with the provided ref # " + finInsUniqueNumber + " ."));
                        }
                    }
                    else if (tradeTranType == "02")
                    {
                        PSW_FORM_E_DOCUMENT_INFO eefdoc = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == finInsUniqueNumber).FirstOrDefault();
                        if (eefdoc != null)
                        {
                            PSW_CHANGE_OF_BANK_REQUEST NewReq = new PSW_CHANGE_OF_BANK_REQUEST();
                            NewReq.COB_ID = Convert.ToInt32(context.PSW_CHANGE_OF_BANK_REQUEST.Max(m => (decimal?)m.COB_ID)) + 1;
                            NewReq.COB_UNIQUE_NO = cobUniqueIdNumber;
                            NewReq.COB_TRANS_TYPE = tradeTranType;
                            NewReq.COB_IBAN = iban;
                            NewReq.COB_REQUEST_NO = finInsUniqueNumber;
                            NewReq.COB_STATUS = "";
                            NewReq.COB_DATA_ENTRY_DATETIME = DateTime.Now;
                            NewReq.METHODID = 1538;
                            context.PSW_CHANGE_OF_BANK_REQUEST.Add(NewReq);

                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                ShareChangeOfBankRequestWithADReponse data = new ShareChangeOfBankRequestWithADReponse();
                                data.messageId = Guid.NewGuid().ToString();
                                data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                                data.senderId = "CBN";
                                data.receiverId = "PSW";
                                data.processingCode = "306";
                                data.data = new ShareChangeOfBankRequestWithADReponse.DetailData { };
                                data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                                data.message = new ShareChangeOfBankRequestWithADReponse.MessageData
                                {
                                    code = "200",
                                    description = "Change of bank request status acknowledged."
                                };
                                ConvertMessageToEntity(data);
                                return Request.CreateResponse(HttpStatusCode.OK, data);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured while saving the change of bank request."));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No E Form Exist in the system with the provided ref # " + finInsUniqueNumber + " ."));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Invalid tradeTranType provided."));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Sharing Data Failed, No Client Exist in the system with provided iban : " + iban + " , NTN : " + traderNTN + " ."));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message));
            }
        }
        #endregion

        #region ProcCode 307
        HttpResponseMessage ShareGDNFIWithOtherAD(dynamic RequestData)
        {
            try
            {
                string cobUniqueIdNumber = RequestData.data.cobUniqueIdNumber;
                string tradeTranType = RequestData.data.tradeTranType;
                if (tradeTranType == "01")
                {
                    // IMPORT 
                    using (PSWEntities cont = new PSWEntities())
                    {
                        using (var transaction = cont.Database.BeginTransaction())
                        {
                            try
                            {
                                int CurrentRow = 0;
                                string importerNtn = RequestData.data.financialInstrumentInfo.importerNtn;
                                string importerName = RequestData.data.financialInstrumentInfo.importerName;
                                string importerIban = RequestData.data.iban;
                                PSW_CLIENT_MASTER_DETAIL client = cont.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == importerIban && m.C_NTN_NO == importerNtn).FirstOrDefault();
                                if (client != null)
                                {
                                    int advPayPercentage = 0;
                                    int docAgainstPayPercentage = 0;
                                    int docAgainstAcceptancePercentage = 0;
                                    int days = 0;
                                    int totalPercentage = 0;
                                    int sightPercentage = 0;
                                    int usancePercentage = 0;
                                    int cashmarginPercentage = 0;
                                    decimal cashmarginValue = 0;
                                    var modeOfPayment = RequestData.data.financialInstrumentInfo.modeOfPayment;
                                    int PayModeCode = 0;
                                    int.TryParse(modeOfPayment, out PayModeCode);
                                    string finInsUniqueNumber = RequestData.data.financialInstrumentInfo.finInsUniqueNumber;
                                    string advPayPercentageFormated = "0";
                                    string daysFormatted = "0";
                                    string totalPercentageFormatted = "0";
                                    string docAgainstPayPercentageFormatted = "0";
                                    string sightPercentageFormated = "0";
                                    string usancePercentageFormated = "0";
                                    string docAgainstAcceptancePercentageFormatted = "0";
                                    string cashmarginPercentageFormatted = "0";
                                    string cashmarginValueFormatted = "0";
                                    string GD_NUMBER = "";
                                    // payment information
                                    string beneficiaryName = RequestData.data.financialInstrumentInfo.paymentInformation.beneficiaryName;
                                    string beneficiaryAddress = RequestData.data.financialInstrumentInfo.paymentInformation.beneficiaryAddress;
                                    string beneficiaryCountry = RequestData.data.financialInstrumentInfo.paymentInformation.beneficiaryCountry;
                                    string beneficiaryIban = RequestData.data.financialInstrumentInfo.paymentInformation.beneficiaryIban;
                                    string exporterName = RequestData.data.financialInstrumentInfo.paymentInformation.exporterName;
                                    string exporterAddress = RequestData.data.financialInstrumentInfo.paymentInformation.exporterAddress;
                                    string exporterCountry = RequestData.data.financialInstrumentInfo.paymentInformation.exporterCountry;
                                    string portOfShipment = RequestData.data.financialInstrumentInfo.paymentInformation.portOfShipment;
                                    string deliveryTerms = RequestData.data.financialInstrumentInfo.paymentInformation.deliveryTerms;
                                    var financialInstrumentValue = RequestData.data.financialInstrumentInfo.paymentInformation.financialInstrumentValue;
                                    string financialInstrumentCurrency = RequestData.data.financialInstrumentInfo.paymentInformation.financialInstrumentCurrency;
                                    var exchangeRate = RequestData.data.financialInstrumentInfo.paymentInformation.exchangeRate;
                                    string lcContractNo = RequestData.data.financialInstrumentInfo.paymentInformation.lcContractNo;
                                    string intendedPayDate = "";
                                    if (RequestData.data.financialInstrumentInfo.financialTranInformation.intendedPayDate != null)
                                    {
                                        intendedPayDate = DateTime.ParseExact(
                                    Convert.ToString(
                                    RequestData.data.financialInstrumentInfo.financialTranInformation.intendedPayDate)
                                    , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                    }
                                    string transportDocDate = "";
                                    if (RequestData.data.financialInstrumentInfo.financialTranInformation.transportDocDate != null)
                                    {
                                        transportDocDate = DateTime.ParseExact(
                                    Convert.ToString(
                                    RequestData.data.financialInstrumentInfo.financialTranInformation.transportDocDate)
                                    , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                    }
                                    string finalDateOfShipment = "";
                                    if (RequestData.data.financialInstrumentInfo.financialTranInformation.finalDateOfShipment != null)
                                    {
                                        finalDateOfShipment = DateTime.ParseExact(
                                    Convert.ToString(
                                    RequestData.data.financialInstrumentInfo.financialTranInformation.finalDateOfShipment)
                                    , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                    }
                                    string expiryDate = "";
                                    if (RequestData.data.financialInstrumentInfo.financialTranInformation.expiryDate != null)
                                    {
                                        expiryDate = DateTime.ParseExact(
                                       Convert.ToString(
                                       RequestData.data.financialInstrumentInfo.financialTranInformation.expiryDate)
                                       , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                    }
                                    string remarks = RequestData.data.financialInstrumentInfo.remarks;

                                    PSW_AUTH_PAY_MODES PayMode = cont.PSW_AUTH_PAY_MODES.Where(m => m.APM_CODE == PayModeCode).FirstOrDefault();
                                    PSW_EIF_BASIC_INFO basic = cont.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == finInsUniqueNumber).FirstOrDefault();
                                    PSW_PAYMENT_MODES_PARAMETERS PayParam = cont.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == finInsUniqueNumber).FirstOrDefault();
                                    PSW_EIF_DOCUMENT_REQUEST_INFO docinfo = cont.PSW_EIF_DOCUMENT_REQUEST_INFO.Where(m => m.EIF_DRI_BI_EIF_REQUEST_NO == finInsUniqueNumber).FirstOrDefault();
                                    PSW_EIF_BANK_INFO bankInfo = cont.PSW_EIF_BANK_INFO.Where(m => m.EIF_BA_EIF_REQUEST_NO == finInsUniqueNumber).FirstOrDefault();
                                    PSW_LIST_OF_COUNTRIES BeneCountry = cont.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_CODE == beneficiaryCountry).FirstOrDefault();
                                    PSW_LIST_OF_COUNTRIES ExporterCountry = cont.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_CODE == exporterCountry).FirstOrDefault();
                                    PSW_DELIVERY_TERMS DeliveryTerm = cont.PSW_DELIVERY_TERMS.Where(m => m.DT_CODE == deliveryTerms).FirstOrDefault();
                                    PSW_CURRENCY FICCY = cont.PSW_CURRENCY.Where(m => m.CUR_NAME == financialInstrumentCurrency).FirstOrDefault();
                                    if (basic == null)
                                    {
                                        basic = new PSW_EIF_BASIC_INFO();
                                        basic.EIF_BI_ID = Convert.ToInt32(cont.PSW_EIF_BASIC_INFO.Max(m => (decimal?)m.EIF_BI_ID)) + 1;
                                        basic.EIF_BI_REQUEST_DATE = DateTime.Now;
                                        basic.EIF_BI_EIF_REQUEST_NO = finInsUniqueNumber;
                                        basic.EIF_BI_APPROVAL_STATUS = "APPROVED";
                                        basic.EIF_BI_STATUS_DATE = DateTime.Now;
                                        basic.EIF_BI_MAKER_ID = "PSW";
                                        basic.EIF_BI_NTN = client.C_NTN_NO;
                                        basic.EIF_BI_CERTIFIED = false;
                                        basic.EIF_BI_BUSSINESS_NAME = client.C_IBAN_NO.ToString();
                                        basic.EIF_BI_BUSSINESS_ADDRESS = client.C_ADDRESS;
                                        basic.EIF_BI_IS_REMITTANCE_FROM_PAKISTAN = true;
                                        basic.EIF_BI_STRN = client.C_STRN;
                                        basic.EIF_BI_MODE_OF_IMPORT_PAYMENT = PayMode.APM_ID;
                                        basic.EIF_BI_ENTRY_DATETIME = DateTime.Now;
                                        cont.PSW_EIF_BASIC_INFO.Add(basic);
                                        cont.SaveChanges();
                                    }
                                    else
                                    {
                                        basic.EIF_BI_REQUEST_DATE = DateTime.Now;
                                        basic.EIF_BI_EIF_REQUEST_NO = finInsUniqueNumber;
                                        basic.EIF_BI_APPROVAL_STATUS = "APPROVED";
                                        basic.EIF_BI_STATUS_DATE = DateTime.Now;
                                        basic.EIF_BI_MAKER_ID = "PSW";
                                        basic.EIF_BI_NTN = client.C_NTN_NO;
                                        basic.EIF_BI_CERTIFIED = false;
                                        basic.EIF_BI_BUSSINESS_NAME = client.C_IBAN_NO.ToString();
                                        basic.EIF_BI_BUSSINESS_ADDRESS = client.C_ADDRESS;
                                        basic.EIF_BI_IS_REMITTANCE_FROM_PAKISTAN = true;
                                        basic.EIF_BI_STRN = client.C_STRN;
                                        basic.EIF_BI_MODE_OF_IMPORT_PAYMENT = PayMode.APM_ID;
                                        basic.EIF_BI_EDIT_DATETIME = DateTime.Now;
                                        cont.Entry(basic).State = System.Data.Entity.EntityState.Modified;
                                        cont.SaveChanges();
                                    }
                                    if (PayParam == null)
                                    {
                                        PayParam = new PSW_PAYMENT_MODES_PARAMETERS();
                                        PayParam.EIF_PMP_ID = Convert.ToInt32(cont.PSW_PAYMENT_MODES_PARAMETERS.Max(m => (decimal?)m.EIF_PMP_ID)) + 1;
                                        PayParam.EIF_PMP_EIF_REQUEST_NO = finInsUniqueNumber;
                                        switch (modeOfPayment)
                                        {
                                            case "301":
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                sightPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.sightPercentage);
                                                int.TryParse(sightPercentageFormated, out sightPercentage);
                                                PayParam.EIF_PMP_SIGHT_PERCENT = sightPercentage;
                                                usancePercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.usancePercentage);
                                                int.TryParse(usancePercentageFormated, out usancePercentage);
                                                PayParam.EIF_PMP_USANCE_PERCENT = usancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "302":
                                                GD_NUMBER = RequestData.data.financialInstrumentInfo.openAccountData.gdNumber;
                                                PayParam.EIF_PMP_GD_NO = GD_NUMBER;
                                                break;
                                            case "303":
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                docAgainstPayPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstPayPercentage);
                                                int.TryParse(docAgainstPayPercentageFormatted, out docAgainstPayPercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = docAgainstPayPercentage;
                                                docAgainstAcceptancePercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstAcceptancePercentage);
                                                int.TryParse(docAgainstAcceptancePercentageFormatted, out docAgainstAcceptancePercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = docAgainstAcceptancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "304":
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                docAgainstPayPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstPayPercentage);
                                                int.TryParse(docAgainstPayPercentageFormatted, out docAgainstPayPercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = docAgainstPayPercentage;
                                                docAgainstAcceptancePercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstAcceptancePercentage);
                                                int.TryParse(docAgainstAcceptancePercentageFormatted, out docAgainstAcceptancePercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = docAgainstAcceptancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "309":
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                sightPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.sightPercentage);
                                                int.TryParse(sightPercentageFormated, out sightPercentage);
                                                PayParam.EIF_PMP_SIGHT_PERCENT = sightPercentage;
                                                usancePercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.usancePercentage);
                                                int.TryParse(usancePercentageFormated, out usancePercentage);
                                                PayParam.EIF_PMP_USANCE_PERCENT = usancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "310":
                                                cashmarginPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.cashMargin.cashmarginPercentage);
                                                int.TryParse(cashmarginPercentageFormatted, out cashmarginPercentage);
                                                PayParam.EIF_PMP_CASH_MARGIN_PERCENT = cashmarginPercentage;
                                                cashmarginValueFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.cashMargin.cashmarginValue);
                                                Decimal.TryParse(cashmarginValueFormatted, out cashmarginValue);
                                                PayParam.EIF_PMP_CASH_MARGIN_VALUE = cashmarginValue;
                                                break;
                                            default:
                                                break;
                                        }
                                        PayParam.EIF_PMP_COMMISION_CHARGES = 0;
                                        PayParam.EIF_PMP_FED_CHARGES = 0;
                                        PayParam.EIF_PMP_ENTRY_DATETIME = basic.EIF_BI_ENTRY_DATETIME;
                                        PayParam.EIF_PMP_MAKER_ID = basic.EIF_BI_MAKER_ID;
                                        cont.PSW_PAYMENT_MODES_PARAMETERS.Add(PayParam);
                                        cont.SaveChanges();
                                    }
                                    else
                                    {
                                        switch (modeOfPayment)
                                        {
                                            case "301":
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                sightPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.sightPercentage);
                                                int.TryParse(sightPercentageFormated, out sightPercentage);
                                                PayParam.EIF_PMP_SIGHT_PERCENT = sightPercentage;
                                                usancePercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.usancePercentage);
                                                int.TryParse(usancePercentageFormated, out usancePercentage);
                                                PayParam.EIF_PMP_USANCE_PERCENT = usancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "302":
                                                GD_NUMBER = RequestData.data.financialInstrumentInfo.openAccountData.gdNumber;
                                                PayParam.EIF_PMP_GD_NO = GD_NUMBER;
                                                break;
                                            case "303":
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                docAgainstPayPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstPayPercentage);
                                                int.TryParse(docAgainstPayPercentageFormatted, out docAgainstPayPercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = docAgainstPayPercentage;
                                                docAgainstAcceptancePercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstAcceptancePercentage);
                                                int.TryParse(docAgainstAcceptancePercentageFormatted, out docAgainstAcceptancePercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = docAgainstAcceptancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "304":
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                docAgainstPayPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstPayPercentage);
                                                int.TryParse(docAgainstPayPercentageFormatted, out docAgainstPayPercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = docAgainstPayPercentage;
                                                docAgainstAcceptancePercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstAcceptancePercentage);
                                                int.TryParse(docAgainstAcceptancePercentageFormatted, out docAgainstAcceptancePercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = docAgainstAcceptancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "309":
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                sightPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.sightPercentage);
                                                int.TryParse(sightPercentageFormated, out sightPercentage);
                                                PayParam.EIF_PMP_SIGHT_PERCENT = sightPercentage;
                                                usancePercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.usancePercentage);
                                                int.TryParse(usancePercentageFormated, out usancePercentage);
                                                PayParam.EIF_PMP_USANCE_PERCENT = usancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "310":
                                                cashmarginPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.cashMargin.cashmarginPercentage);
                                                int.TryParse(cashmarginPercentageFormatted, out cashmarginPercentage);
                                                PayParam.EIF_PMP_CASH_MARGIN_PERCENT = cashmarginPercentage;
                                                cashmarginValueFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.cashMargin.cashmarginValue);
                                                Decimal.TryParse(cashmarginValueFormatted, out cashmarginValue);
                                                PayParam.EIF_PMP_CASH_MARGIN_VALUE = cashmarginValue;
                                                break;
                                            default:
                                                break;
                                        }
                                        PayParam.EIF_PMP_COMMISION_CHARGES = 0;
                                        PayParam.EIF_PMP_FED_CHARGES = 0;
                                        PayParam.EIF_PMP_ENTRY_DATETIME = DateTime.Now;
                                        PayParam.EIF_PMP_MAKER_ID = "PSW";
                                        cont.Entry(PayParam).State = System.Data.Entity.EntityState.Modified;
                                    }
                                    if (docinfo == null)
                                    {
                                        docinfo = new PSW_EIF_DOCUMENT_REQUEST_INFO();
                                        docinfo.EIF_DRI_ID = Convert.ToInt32(cont.PSW_EIF_DOCUMENT_REQUEST_INFO.Max(m => (decimal?)m.EIF_DRI_ID)) + 1;
                                        docinfo.EIF_DRI_BI_EIF_REQUEST_NO = finInsUniqueNumber;
                                        docinfo.EIF_DRI_BENEFICIARY_NAME = beneficiaryName;
                                        docinfo.EIF_DRI_BENEFICIARY_IBAN = beneficiaryIban;
                                        docinfo.EIF_DRI_BENEFICIARY_COUNTRY = BeneCountry.LC_ID;
                                        docinfo.EIF_DRI_BENEFICIARY_ADDRESS = beneficiaryAddress;
                                        docinfo.EIF_DRI_EXPORTER_NAME = exporterName;
                                        docinfo.EIF_DRI_EXPORTER_ADDRESS = exporterAddress;
                                        docinfo.EIF_DRI_EXPORTER_COUNTRY = ExporterCountry.LC_ID;
                                        docinfo.EIF_DRI_PORT_OF_SHIPMENT = portOfShipment;
                                        docinfo.EIF_DRI_DELIVERY_TERM = DeliveryTerm.DT_ID;
                                        docinfo.EIF_DRI_LC_CONT_NO = lcContractNo;
                                        docinfo.EIF_DRI_TOTAL_INVOICE_VALUE = financialInstrumentValue;
                                        docinfo.EIF_DRI_CCY = FICCY.CUR_ID;
                                        docinfo.EIF_DRI_EXCHANGE_RATE = exchangeRate;
                                        docinfo.EIF_DRI_ENTRY_DATETIME = DateTime.Now;
                                        docinfo.EIF_DRI_MAKER_ID = "PSW";
                                        cont.PSW_EIF_DOCUMENT_REQUEST_INFO.Add(docinfo);
                                        cont.SaveChanges();
                                    }
                                    else
                                    {
                                        docinfo.EIF_DRI_BI_EIF_REQUEST_NO = finInsUniqueNumber;
                                        docinfo.EIF_DRI_BENEFICIARY_NAME = beneficiaryName;
                                        docinfo.EIF_DRI_BENEFICIARY_IBAN = beneficiaryIban;
                                        docinfo.EIF_DRI_BENEFICIARY_COUNTRY = BeneCountry.LC_ID;
                                        docinfo.EIF_DRI_BENEFICIARY_ADDRESS = beneficiaryAddress;
                                        docinfo.EIF_DRI_EXPORTER_NAME = exporterName;
                                        docinfo.EIF_DRI_EXPORTER_ADDRESS = exporterAddress;
                                        docinfo.EIF_DRI_EXPORTER_COUNTRY = ExporterCountry.LC_ID;
                                        docinfo.EIF_DRI_PORT_OF_SHIPMENT = portOfShipment;
                                        docinfo.EIF_DRI_DELIVERY_TERM = DeliveryTerm.DT_ID;
                                        docinfo.EIF_DRI_LC_CONT_NO = lcContractNo;
                                        docinfo.EIF_DRI_TOTAL_INVOICE_VALUE = financialInstrumentValue;
                                        docinfo.EIF_DRI_CCY = FICCY.CUR_ID;
                                        docinfo.EIF_DRI_EXCHANGE_RATE = exchangeRate;
                                        docinfo.EIF_DRI_EDIT_DATETIME = DateTime.Now;
                                        docinfo.EIF_DRI_MAKER_ID = "PSW";
                                        cont.Entry(docinfo).State = System.Data.Entity.EntityState.Modified;
                                        cont.SaveChanges();
                                    }
                                    if (bankInfo == null)
                                    {
                                        bankInfo = new PSW_EIF_BANK_INFO();
                                        bankInfo.EIF_BA_ID = Convert.ToInt32(cont.PSW_EIF_BANK_INFO.Max(m => (decimal?)m.EIF_BA_ID)) + 1;
                                        bankInfo.EIF_BA_EIF_REQUEST_NO = finInsUniqueNumber;
                                        if (expiryDate != "")
                                            bankInfo.EIF_BA_EXPIRY_DATE = Convert.ToDateTime(expiryDate);
                                        else
                                            bankInfo.EIF_BA_EXPIRY_DATE = null;
                                        if (transportDocDate != "")
                                            bankInfo.EIF_BA_TRANSPORT_DOC_DATE = Convert.ToDateTime(transportDocDate);
                                        else
                                            bankInfo.EIF_BA_TRANSPORT_DOC_DATE = null;
                                        if (intendedPayDate != "")
                                            bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE = Convert.ToDateTime(intendedPayDate);
                                        else
                                            bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE = null;
                                        if (finalDateOfShipment != "")
                                            bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT = Convert.ToDateTime(finalDateOfShipment);
                                        else
                                            bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT = null;
                                        bankInfo.EIF_BA_IBAN = client.C_IBAN_NO;
                                        bankInfo.EIF_BA_REMARK = remarks;
                                        bankInfo.EIF_BA_SBP_APPROVAL_SUBJECT = "";
                                        bankInfo.EIF_BA_SBP_APPROVAL_NO = "";
                                        bankInfo.EIF_BA_SBP_APPROVAL_DATE = DateTime.Now;
                                        bankInfo.EIF_BA_BANK = "1";
                                        bankInfo.EIF_BA_CITY = "1";
                                        bankInfo.EIF_BA_BRANCH = "AWT Plaza Karachi";
                                        bankInfo.EIF_BA_APPROVAL_REMARKS = "Recieved From PSW (307)";
                                        bankInfo.EIF_BA_ENTRY_DATETIME = DateTime.Now;
                                        bankInfo.EIF_BA_MAKER_ID = "PSW";
                                        cont.PSW_EIF_BANK_INFO.Add(bankInfo);
                                    }
                                    else
                                    {
                                        bankInfo.EIF_BA_EIF_REQUEST_NO = finInsUniqueNumber;
                                        if (expiryDate != "")
                                            bankInfo.EIF_BA_EXPIRY_DATE = Convert.ToDateTime(expiryDate);
                                        else
                                            bankInfo.EIF_BA_EXPIRY_DATE = null;
                                        if (transportDocDate != "")
                                            bankInfo.EIF_BA_TRANSPORT_DOC_DATE = Convert.ToDateTime(transportDocDate);
                                        else
                                            bankInfo.EIF_BA_TRANSPORT_DOC_DATE = null;
                                        if (intendedPayDate != "")
                                            bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE = Convert.ToDateTime(intendedPayDate);
                                        else
                                            bankInfo.EIF_BA_INNTENDED_PAYMENT_DATE = null;
                                        if (finalDateOfShipment != "")
                                            bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT = Convert.ToDateTime(finalDateOfShipment);
                                        else
                                            bankInfo.EIF_BA_FINAL_DATE_OF_SHIPMENT = null;
                                        bankInfo.EIF_BA_IBAN = client.C_IBAN_NO;
                                        bankInfo.EIF_BA_REMARK = remarks;
                                        bankInfo.EIF_BA_SBP_APPROVAL_SUBJECT = "";
                                        bankInfo.EIF_BA_SBP_APPROVAL_NO = "";
                                        bankInfo.EIF_BA_SBP_APPROVAL_DATE = DateTime.Now;
                                        bankInfo.EIF_BA_BANK = "1";
                                        bankInfo.EIF_BA_CITY = "1";
                                        bankInfo.EIF_BA_BRANCH = "AWT Plaza Karachi";
                                        bankInfo.EIF_BA_APPROVAL_REMARKS = "Recieved From PSW (307)";
                                        bankInfo.EIF_BA_EDIT_DATETIME = DateTime.Now;
                                        bankInfo.EIF_BA_MAKER_ID = "PSW";
                                        cont.Entry(bankInfo).State = System.Data.Entity.EntityState.Modified;
                                    }

                                    // HS CODE Information
                                    CurrentRow = 0;
                                    foreach (dynamic i in RequestData.data.financialInstrumentInfo.itemInformation)
                                    {
                                        string hsCode = i.hsCode;
                                        string goodsDescription = i.goodsDescription;
                                        decimal quantity = i.quantity;
                                        string uom = i.uom;
                                        string countryOfOrigin = i.countryOfOrigin;
                                        string sample = i.sample;
                                        decimal sampleValue = i.sampleValue;

                                        PSW_EIF_HS_CODE ItemInformation = cont.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == finInsUniqueNumber && m.EIF_HC_CODE == hsCode).FirstOrDefault();
                                        if (ItemInformation == null)
                                        {
                                            ItemInformation = new PSW_EIF_HS_CODE();
                                            int MAXID = Convert.ToInt32(cont.PSW_EIF_HS_CODE.Max(m => (decimal?)m.EIF_HC_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                ItemInformation.EIF_HC_ID = MAXID;
                                            }
                                            else
                                            {
                                                ItemInformation.EIF_HC_ID = (MAXID + CurrentRow);
                                            }
                                            ItemInformation.EIF_HC_EIF_REQUEST_NO = finInsUniqueNumber;
                                            ItemInformation.EIF_HC_CODE = hsCode;
                                            ItemInformation.EIF_HC_DESCRIPTION = goodsDescription;
                                            ItemInformation.EIF_HC_QUANTITY = quantity;
                                            ItemInformation.EIF_HC_UOM = uom;
                                            ItemInformation.EIF_HC_ORGIN = countryOfOrigin;
                                            ItemInformation.EIF_HC_IS_SAMPLE = sample;
                                            ItemInformation.EIF_HC_SAMPLE_VALUE = sampleValue;
                                            ItemInformation.EIF_HC_ENTRTY_DATETIME = DateTime.Now;
                                            ItemInformation.EIF_HC_MAKER_ID = "PSW";
                                            cont.PSW_EIF_HS_CODE.Add(ItemInformation);
                                        }
                                        else
                                        {
                                            ItemInformation.EIF_HC_DESCRIPTION = goodsDescription;
                                            ItemInformation.EIF_HC_QUANTITY = quantity;
                                            ItemInformation.EIF_HC_UOM = uom;
                                            ItemInformation.EIF_HC_ORGIN = countryOfOrigin;
                                            ItemInformation.EIF_HC_IS_SAMPLE = sample;
                                            ItemInformation.EIF_HC_SAMPLE_VALUE = sampleValue;
                                            ItemInformation.EIF_HC_EDIT_DATETIME = DateTime.Now;
                                            ItemInformation.EIF_HC_MAKER_ID = "PSW";
                                            cont.Entry(ItemInformation).State = System.Data.Entity.EntityState.Modified;
                                        }

                                        cont.SaveChanges();
                                        CurrentRow += 1;
                                    }
                                    // HS CODE ItemInformation End

                                    // GD INFORMATION BLOCK
                                    CurrentRow = 0;
                                    foreach (dynamic i in RequestData.data.gdInfo)
                                    {
                                        string GD_NO = i.gdNumber;

                                        // General Information
                                        PSW_EIF_GD_GENERAL_INFO GeneralInfo = cont.PSW_EIF_GD_GENERAL_INFO.Where(m => m.EIGDGI_GD_NO == GD_NO).FirstOrDefault();
                                        if (GeneralInfo == null)
                                        {
                                            GeneralInfo = new PSW_EIF_GD_GENERAL_INFO();
                                            int MAXID = Convert.ToInt32(cont.PSW_EIF_GD_GENERAL_INFO.Max(m => (decimal?)m.EIGDGI_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                GeneralInfo.EIGDGI_ID = MAXID;
                                            }
                                            else
                                            {
                                                GeneralInfo.EIGDGI_ID = (MAXID + CurrentRow);
                                            }
                                            GeneralInfo.EIGDGI_EIF_REQUEST_NO = finInsUniqueNumber;
                                            GeneralInfo.EIGDGI_AGENT_NAME = "";
                                            GeneralInfo.EIGDGI_TRADE_TYPE = "Import";
                                            GeneralInfo.EIGDGI_GD_NO = GD_NO;
                                            GeneralInfo.EIGDGI_COLLECTORATE = i.collectorate;
                                            GeneralInfo.EIGDGI_DESTINATION_COUNTRY = "";
                                            GeneralInfo.EIGDGI_PLACE_OF_DELIVERY = i.generalInformation.portOfDelivery;
                                            GeneralInfo.EIGDGI_GENERAL_DESCRIPTION = "";
                                            GeneralInfo.EIGDGI_SHED_TERMINAL_LOCATION = i.generalInformation.terminalLocation;
                                            GeneralInfo.EIGDGI_AGENT_LICENSE_NO = "";
                                            GeneralInfo.EIGDGI_DECLARATION_TYPE = "";
                                            GeneralInfo.EIGDGI_CONSIGNMENT_CATE = i.consignmentCategory;
                                            GeneralInfo.EIGDGI_GD_TYPE = i.gdType;
                                            GeneralInfo.EIGDGI_GD_STATUS = i.gdStatus.Trim();
                                            GeneralInfo.EIGDGI_PORT_OF_DISCHARGE = i.generalInformation.portOfDischarge;
                                            GeneralInfo.EIGDGI_1ST_EXAMINATION = "";
                                            GeneralInfo.EIGDGI_MODE_OF_PAYMENT = i.financialInfo.modeOfPayment.Trim();
                                            GeneralInfo.EIGDGI_IMPORTER_IBAN = client.C_IBAN_NO;
                                            GeneralInfo.EIGDGI_VIR_AIR_NUM = i.virAirNumber;
                                            GeneralInfo.EIGDGI_INVOICE_NUM = i.financialInfo.invoiceNumber;
                                            string clearanceDate = "";
                                            if (i.clearanceDate != null)
                                            {
                                                clearanceDate = DateTime.ParseExact(Convert.ToString(i.clearanceDate),
                                                              "yyyyMMdd",
                                                               CultureInfo.InvariantCulture).ToString();
                                                GeneralInfo.EIGDGI_CLEARANCE_DATE = Convert.ToDateTime(clearanceDate);
                                            }
                                            else
                                                clearanceDate = null;
                                            string invoiceDate = "";
                                            if (i.financialInfo.invoiceDate != null)
                                            {
                                                invoiceDate = DateTime.ParseExact(Convert.ToString(i.financialInfo.invoiceDate),
                                                              "yyyyMMdd",
                                                               CultureInfo.InvariantCulture).ToString();
                                                GeneralInfo.EIGDGI_INVOICE_DATE = Convert.ToDateTime(invoiceDate);
                                            }
                                            GeneralInfo.EIGDGI_TOTAL_DECLARED_VALUE = Convert.ToDecimal(i.financialInfo.totalDeclaredValue);
                                            GeneralInfo.EIGDGI_DELIVERY_TERM = i.financialInfo.deliveryTerm.Trim();
                                            GeneralInfo.EIGDGI_FOBVALUEUSD = Convert.ToDecimal(i.financialInfo.fobValueUsd);
                                            GeneralInfo.EIGDGI_FRIEGHTUSD = Convert.ToDecimal(i.financialInfo.freightUsd);
                                            GeneralInfo.EIGDGI_CRFVALUEUSD = Convert.ToDecimal(i.financialInfo.cfrValueUsd);
                                            GeneralInfo.EIGDGI_INSURANCE_VALUE_USD = Convert.ToDecimal(i.financialInfo.insuranceUsd);
                                            GeneralInfo.EIGDGI_LANDING_CHARGES = Convert.ToDecimal(i.financialInfo.landingChargesUsd);
                                            GeneralInfo.EIGDGI_ASSESSED_VALUE_USD = Convert.ToDecimal(i.financialInfo.assessedValueUsd);
                                            GeneralInfo.EIGDGI_OTHER_CHARGES = Convert.ToDecimal(i.financialInfo.otherCharges);
                                            GeneralInfo.EIGDGI_EXCHANGE_RATE = Convert.ToDecimal(i.financialInfo.exchangeRate);
                                            GeneralInfo.EIGDGI_ENTRY_DATETIME = DateTime.Now;
                                            GeneralInfo.EIGDGI_MAKER_ID = "PSW";
                                            if (i.negativeList != null)
                                            {
                                                GeneralInfo.EIGDGI_NEG_COUNTRY = RequestData.data.negativeList.country;
                                                GeneralInfo.EIGDGI_NEG_SUPPLIER = RequestData.data.negativeList.supplier;
                                                Array NEGCOMS = RequestData.data.negativeList.commodities;
                                                int ComCount = 0;
                                                string NEGCOMString = "";
                                                if (NEGCOMS != null)
                                                {
                                                    foreach (string Coms in NEGCOMS)
                                                    {
                                                        if (ComCount == 0)
                                                            NEGCOMString += Coms;
                                                        else
                                                            NEGCOMString += "," + Coms;
                                                        ComCount += 1;
                                                    }
                                                }
                                                GeneralInfo.EIGDGI_NEG_COMMS = NEGCOMString;
                                            }
                                            cont.PSW_EIF_GD_GENERAL_INFO.Add(GeneralInfo);
                                            cont.SaveChanges();
                                        }
                                        else
                                        {
                                            GeneralInfo.EIGDGI_EIF_REQUEST_NO = finInsUniqueNumber;
                                            GeneralInfo.EIGDGI_AGENT_NAME = "";
                                            GeneralInfo.EIGDGI_TRADE_TYPE = "Import";
                                            GeneralInfo.EIGDGI_GD_NO = GD_NO;
                                            GeneralInfo.EIGDGI_COLLECTORATE = i.collectorate;
                                            GeneralInfo.EIGDGI_DESTINATION_COUNTRY = "";
                                            GeneralInfo.EIGDGI_PLACE_OF_DELIVERY = i.generalInformation.portOfDelivery;
                                            GeneralInfo.EIGDGI_GENERAL_DESCRIPTION = "";
                                            GeneralInfo.EIGDGI_SHED_TERMINAL_LOCATION = i.generalInformation.terminalLocation;
                                            GeneralInfo.EIGDGI_AGENT_LICENSE_NO = "";
                                            GeneralInfo.EIGDGI_DECLARATION_TYPE = "";
                                            GeneralInfo.EIGDGI_CONSIGNMENT_CATE = i.consignmentCategory;
                                            GeneralInfo.EIGDGI_GD_TYPE = i.gdType;
                                            GeneralInfo.EIGDGI_GD_STATUS = i.gdStatus.Trim();
                                            GeneralInfo.EIGDGI_PORT_OF_DISCHARGE = i.generalInformation.portOfDischarge;
                                            GeneralInfo.EIGDGI_1ST_EXAMINATION = "";
                                            GeneralInfo.EIGDGI_MODE_OF_PAYMENT = i.financialInfo.modeOfPayment.Trim();
                                            GeneralInfo.EIGDGI_IMPORTER_IBAN = client.C_IBAN_NO;
                                            GeneralInfo.EIGDGI_VIR_AIR_NUM = i.virAirNumber;
                                            GeneralInfo.EIGDGI_INVOICE_NUM = i.financialInfo.invoiceNumber;
                                            string clearanceDate = "";
                                            if (i.clearanceDate != null)
                                            {
                                                clearanceDate = DateTime.ParseExact(Convert.ToString(i.clearanceDate),
                                                              "yyyyMMdd",
                                                               CultureInfo.InvariantCulture).ToString();
                                                GeneralInfo.EIGDGI_CLEARANCE_DATE = Convert.ToDateTime(clearanceDate);
                                            }
                                            else
                                                clearanceDate = null;
                                            string invoiceDate = "";
                                            if (i.financialInfo.invoiceDate != null)
                                            {
                                                invoiceDate = DateTime.ParseExact(Convert.ToString(i.financialInfo.invoiceDate),
                                                              "yyyyMMdd",
                                                               CultureInfo.InvariantCulture).ToString();
                                                GeneralInfo.EIGDGI_INVOICE_DATE = Convert.ToDateTime(invoiceDate);
                                            }
                                            GeneralInfo.EIGDGI_TOTAL_DECLARED_VALUE = Convert.ToDecimal(i.financialInfo.totalDeclaredValue);
                                            GeneralInfo.EIGDGI_DELIVERY_TERM = i.financialInfo.deliveryTerm.Trim();
                                            GeneralInfo.EIGDGI_FOBVALUEUSD = Convert.ToDecimal(i.financialInfo.fobValueUsd);
                                            GeneralInfo.EIGDGI_FRIEGHTUSD = Convert.ToDecimal(i.financialInfo.freightUsd);
                                            GeneralInfo.EIGDGI_CRFVALUEUSD = Convert.ToDecimal(i.financialInfo.cfrValueUsd);
                                            GeneralInfo.EIGDGI_INSURANCE_VALUE_USD = Convert.ToDecimal(i.financialInfo.insuranceUsd);
                                            GeneralInfo.EIGDGI_LANDING_CHARGES = Convert.ToDecimal(i.financialInfo.landingChargesUsd);
                                            GeneralInfo.EIGDGI_ASSESSED_VALUE_USD = Convert.ToDecimal(i.financialInfo.assessedValueUsd);
                                            GeneralInfo.EIGDGI_OTHER_CHARGES = Convert.ToDecimal(i.financialInfo.otherCharges);
                                            GeneralInfo.EIGDGI_EXCHANGE_RATE = Convert.ToDecimal(i.financialInfo.exchangeRate);
                                            GeneralInfo.EIGDGI_EDIT_DATETIME = DateTime.Now;
                                            GeneralInfo.EIGDGI_MAKER_ID = "PSW";
                                            if (i.negativeList != null)
                                            {
                                                GeneralInfo.EIGDGI_NEG_COUNTRY = RequestData.data.negativeList.country;
                                                GeneralInfo.EIGDGI_NEG_SUPPLIER = RequestData.data.negativeList.supplier;
                                                Array NEGCOMS = RequestData.data.negativeList.commodities;
                                                int ComCount = 0;
                                                string NEGCOMString = "";
                                                if (NEGCOMS != null)
                                                {
                                                    foreach (string Coms in NEGCOMS)
                                                    {
                                                        if (ComCount == 0)
                                                            NEGCOMString += Coms;
                                                        else
                                                            NEGCOMString += "," + Coms;
                                                        ComCount += 1;
                                                    }
                                                }
                                                GeneralInfo.EIGDGI_NEG_COMMS = NEGCOMString;
                                            }
                                            cont.Entry(GeneralInfo).State = System.Data.Entity.EntityState.Modified;
                                            cont.SaveChanges();
                                        }
                                        // General Information

                                        // IMPORT EXPORT INFORMATION    
                                        PSW_EIF_GD_IMPORT_EXPORT_INFO IMPORTEXPORTInfo = cont.PSW_EIF_GD_IMPORT_EXPORT_INFO.Where(m => m.EIGDIE_GD_NO == GD_NO).FirstOrDefault();
                                        if (IMPORTEXPORTInfo == null)
                                        {
                                            IMPORTEXPORTInfo = new PSW_EIF_GD_IMPORT_EXPORT_INFO();
                                            int MAXID = Convert.ToInt32(cont.PSW_EIF_GD_IMPORT_EXPORT_INFO.Max(m => (decimal?)m.EIGDIE_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                IMPORTEXPORTInfo.EIGDIE_ID = MAXID;
                                            }
                                            else
                                            {
                                                IMPORTEXPORTInfo.EIGDIE_ID = (MAXID + CurrentRow);
                                            }
                                            IMPORTEXPORTInfo.EIGDIE_EIF_REQUEST_NO = finInsUniqueNumber;
                                            IMPORTEXPORTInfo.EIGDIE_NTN_FTN = i.consignorConsigneeInfo.ntnFtn;
                                            IMPORTEXPORTInfo.EIGDIE_STRN = i.consignorConsigneeInfo.strn;
                                            IMPORTEXPORTInfo.EIGDIE_GD_NO = GD_NO;
                                            IMPORTEXPORTInfo.EIGDIE_CONSIGNEE_NAME = i.consignorConsigneeInfo.consigneeName;
                                            IMPORTEXPORTInfo.EIGDIE_CONSIGNEE_ADDRESS = i.consignorConsigneeInfo.consigneeAddress;
                                            IMPORTEXPORTInfo.EIGDIE_CONSIGNOR_NAME = i.consignorConsigneeInfo.consignorName;
                                            IMPORTEXPORTInfo.EIGDIE_CONSIGNOR_ADDRESS = i.consignorConsigneeInfo.consignorAddress;
                                            IMPORTEXPORTInfo.EIGDIE_ENTRY_DATETIME = DateTime.Now;
                                            IMPORTEXPORTInfo.EIGDIE_MAKER_ID = "PSW";
                                            cont.PSW_EIF_GD_IMPORT_EXPORT_INFO.Add(IMPORTEXPORTInfo);
                                            cont.SaveChanges();
                                        }
                                        else
                                        {
                                            IMPORTEXPORTInfo.EIGDIE_EIF_REQUEST_NO = finInsUniqueNumber;
                                            IMPORTEXPORTInfo.EIGDIE_NTN_FTN = i.consignorConsigneeInfo.ntnFtn;
                                            IMPORTEXPORTInfo.EIGDIE_STRN = i.consignorConsigneeInfo.strn;
                                            IMPORTEXPORTInfo.EIGDIE_GD_NO = GD_NO;
                                            IMPORTEXPORTInfo.EIGDIE_CONSIGNEE_NAME = i.consignorConsigneeInfo.consigneeName;
                                            IMPORTEXPORTInfo.EIGDIE_CONSIGNEE_ADDRESS = i.consignorConsigneeInfo.consigneeAddress;
                                            IMPORTEXPORTInfo.EIGDIE_CONSIGNOR_NAME = i.consignorConsigneeInfo.consignorName;
                                            IMPORTEXPORTInfo.EIGDIE_CONSIGNOR_ADDRESS = i.consignorConsigneeInfo.consignorAddress;
                                            IMPORTEXPORTInfo.EIGDIE_EDIT_DATETIME = DateTime.Now;
                                            IMPORTEXPORTInfo.EIGDIE_MAKER_ID = "PSW";
                                            cont.Entry(IMPORTEXPORTInfo).State = System.Data.Entity.EntityState.Modified;
                                            cont.SaveChanges();
                                        }
                                        // IMPORT EXPORT INFORMATION


                                        // GD INFORMATION
                                        PSW_EIF_GD_INFORMATION GDInfo = cont.PSW_EIF_GD_INFORMATION.Where(m => m.EIGDI_GD_NO == GD_NO).FirstOrDefault();
                                        if (GDInfo == null)
                                        {
                                            GDInfo = new PSW_EIF_GD_INFORMATION();
                                            int MAXID = Convert.ToInt32(cont.PSW_EIF_GD_INFORMATION.Max(m => (decimal?)m.EIGDI_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                GDInfo.EIGDI_ID = MAXID;
                                            }
                                            else
                                            {
                                                GDInfo.EIGDI_ID = (MAXID + CurrentRow);
                                            }
                                            GDInfo.EIGDI_EIF_REQUEST_NO = finInsUniqueNumber;
                                            GDInfo.EIGDI_EGM_COLLECTORATE = i.collectorate;
                                            GDInfo.EIGDI_GD_NO = GD_NO;
                                            GDInfo.EIGDI_EGM_NUMBER = "-";
                                            GDInfo.EIGDI_EGM_EXPORTER_NAME = "-";
                                            GDInfo.EIGDI_VESSEL_NAME = "";
                                            GDInfo.EIGDI_GROSS_WEIGHT = i.generalInformation.grossWeight;
                                            GDInfo.EIGDI_COSIGNMENT_TYPE = "";
                                            GDInfo.EIGDI_SECTION = "";
                                            GDInfo.EIGDI_INDEX_NO = "";
                                            GDInfo.EIGDI_BL_NO = i.blAwbNumber;
                                            if (i.financialInfo.invoiceDate != null)
                                            {
                                                string blAwbDate = "";
                                                blAwbDate = DateTime.ParseExact(Convert.ToString(i.blAwbDate),
                                                              "yyyyMMdd",
                                                               CultureInfo.InvariantCulture).ToString();
                                                GDInfo.EIGDI_BL_DATE = Convert.ToDateTime(blAwbDate);
                                            }
                                            GDInfo.EIGDI_PORT_OF_SHIPMENT = i.generalInformation.portOfShipment;
                                            GDInfo.EIGDI_NET_WEIGHT = i.generalInformation.netWeight;
                                            GDInfo.EIGDI_SHIPPING_LINE = "";
                                            GDInfo.EIGDI_BERTHING_TERMINAL = "";
                                            GDInfo.EIGDI_ENTRY_DATETIME = DateTime.Now;
                                            GDInfo.EIGDI_MAKER_ID = "PSW";
                                            cont.PSW_EIF_GD_INFORMATION.Add(GDInfo);
                                            cont.SaveChanges();
                                        }
                                        else
                                        {
                                            GDInfo.EIGDI_EIF_REQUEST_NO = finInsUniqueNumber;
                                            GDInfo.EIGDI_EGM_COLLECTORATE = i.collectorate;
                                            GDInfo.EIGDI_GD_NO = GD_NO;
                                            GDInfo.EIGDI_EGM_NUMBER = "-";
                                            GDInfo.EIGDI_EGM_EXPORTER_NAME = "-";
                                            GDInfo.EIGDI_VESSEL_NAME = "";
                                            GDInfo.EIGDI_GROSS_WEIGHT = i.generalInformation.grossWeight;
                                            GDInfo.EIGDI_COSIGNMENT_TYPE = "";
                                            GDInfo.EIGDI_SECTION = "";
                                            GDInfo.EIGDI_INDEX_NO = "";
                                            GDInfo.EIGDI_BL_NO = i.blAwbNumber;
                                            if (i.financialInfo.invoiceDate != null)
                                            {
                                                string blAwbDate = "";
                                                blAwbDate = DateTime.ParseExact(Convert.ToString(i.blAwbDate),
                                                              "yyyyMMdd",
                                                               CultureInfo.InvariantCulture).ToString();
                                                GDInfo.EIGDI_BL_DATE = Convert.ToDateTime(blAwbDate);
                                            }
                                            GDInfo.EIGDI_PORT_OF_SHIPMENT = i.generalInformation.portOfShipment;
                                            GDInfo.EIGDI_NET_WEIGHT = i.generalInformation.netWeight;
                                            GDInfo.EIGDI_SHIPPING_LINE = "";
                                            GDInfo.EIGDI_BERTHING_TERMINAL = "";
                                            GDInfo.EIGDI_EDIT_DATETIME = DateTime.Now;
                                            GDInfo.EIGDI_MAKER_ID = "PSW";
                                            cont.Entry(GDInfo).State = System.Data.Entity.EntityState.Modified;
                                            cont.SaveChanges();
                                        }
                                        // GD INFORMATION


                                        // GD PACKAGE INFORMATION
                                        List<PSW_GD_IMPORT_PACKAGE_INFORMATION> DeletePrevious = cont.PSW_GD_IMPORT_PACKAGE_INFORMATION.Where(w => w.IPI_GD_NO == GD_NO).ToList();
                                        foreach (PSW_GD_IMPORT_PACKAGE_INFORMATION row in DeletePrevious)
                                        {
                                            cont.PSW_GD_IMPORT_PACKAGE_INFORMATION.Remove(row);
                                        }
                                        cont.SaveChanges();
                                        int CurrentRowPackage = 0;
                                        foreach (dynamic j in i.generalInformation.packagesInformation)
                                        {
                                            PSW_GD_IMPORT_PACKAGE_INFORMATION Packages = new PSW_GD_IMPORT_PACKAGE_INFORMATION();
                                            int MAXID = Convert.ToInt32(cont.PSW_GD_IMPORT_PACKAGE_INFORMATION.Max(m => (decimal?)m.IPI_ID)) + 1;
                                            if (CurrentRowPackage == 0)
                                            {
                                                Packages.IPI_ID = MAXID;
                                            }
                                            else
                                            {
                                                Packages.IPI_ID = (MAXID + CurrentRowPackage);
                                            }
                                            var IPI_NO_OF_PACKAGE = i.numberOfPackages;
                                            Packages.IPI_NO_OF_PACKAGE = IPI_NO_OF_PACKAGE;
                                            var IPI_PACKAGE_TYPE = i.packageType;
                                            Packages.IPI_PACKAGE_TYPE = IPI_PACKAGE_TYPE;
                                            Packages.IPI_GD_NO = GD_NO;
                                            Packages.IPI_ENTRY_DATETIME = DateTime.Now;
                                            Packages.IPI_MAKER_ID = "PSW";
                                            cont.PSW_GD_IMPORT_PACKAGE_INFORMATION.Add(Packages);
                                            CurrentRowPackage += 1;
                                        }
                                        cont.SaveChanges();
                                        // GD PACKAGE INFORMATION


                                        // GD CONTAINER INFORMATION
                                        List<PSW_GD_IMPORT_CONTAINER_INFORMATION> DeletePreviousContainers = cont.PSW_GD_IMPORT_CONTAINER_INFORMATION.Where(w => w.CII_GD_NO == GD_NO).ToList();
                                        foreach (PSW_GD_IMPORT_CONTAINER_INFORMATION row in DeletePreviousContainers)
                                        {
                                            cont.PSW_GD_IMPORT_CONTAINER_INFORMATION.Remove(row);
                                        }
                                        cont.SaveChanges();
                                        int CurrentRowContainer = 0;
                                        foreach (dynamic j in i.generalInformation.containerVehicleInformation)
                                        {
                                            PSW_GD_IMPORT_CONTAINER_INFORMATION Containers = new PSW_GD_IMPORT_CONTAINER_INFORMATION();
                                            int MAXID = Convert.ToInt32(cont.PSW_GD_IMPORT_CONTAINER_INFORMATION.Max(m => (decimal?)m.CII_ID)) + 1;
                                            if (CurrentRowContainer == 0)
                                            {
                                                Containers.CII_ID = MAXID;
                                            }
                                            else
                                            {
                                                Containers.CII_ID = (MAXID + CurrentRowContainer);
                                            }
                                            var CII_CONTAINER_OR_TRUCK_NO = i.containerOrTruckNumber;
                                            Containers.CII_CONTAINER_OR_TRUCK_NO = CII_CONTAINER_OR_TRUCK_NO;
                                            var CII_SERIAL_NO = i.sealNumber;
                                            Containers.CII_SERIAL_NO = CII_SERIAL_NO;
                                            var CII_CONTAINER_TYPE = i.containerType;
                                            Containers.CII_CONTAINER_TYPE = CII_CONTAINER_TYPE;
                                            Containers.CII_GD_NO = GD_NO;
                                            Containers.CII_ENTRY_DATETIME = DateTime.Now;
                                            Containers.CII_MAKER_ID = "PSW";
                                            cont.PSW_GD_IMPORT_CONTAINER_INFORMATION.Add(Containers);
                                            CurrentRowContainer += 1;
                                        }
                                        cont.SaveChanges();
                                        // GD CONTAINER INFORMATION

                                        // GD HS CODE INFORMATION
                                        List<PSW_GD_IMORT_HS_CODE> DeletePreviousItems = cont.PSW_GD_IMORT_HS_CODE.Where(w => w.IHC_GD_NO == GD_NO).ToList();
                                        foreach (PSW_GD_IMORT_HS_CODE row in DeletePreviousItems)
                                        {
                                            cont.PSW_GD_IMORT_HS_CODE.Remove(row);
                                        }
                                        int CurrentRowHSCODES = 0;
                                        foreach (dynamic j in i.itemInformation)
                                        {
                                            PSW_GD_IMORT_HS_CODE Items = new PSW_GD_IMORT_HS_CODE();
                                            int MAXID = Convert.ToInt32(cont.PSW_GD_IMORT_HS_CODE.Max(m => (decimal?)m.IHC_ID)) + 1;
                                            if (CurrentRowHSCODES == 0)
                                            {
                                                Items.IHC_ID = MAXID;
                                            }
                                            else
                                            {
                                                Items.IHC_ID = (MAXID + CurrentRowHSCODES);
                                            }
                                            var IHC_HS_CODE = i.hsCode;
                                            Items.IHC_HS_CODE = Convert.ToDecimal(IHC_HS_CODE);
                                            var IHC_QUANTITY = i.quantity;
                                            Items.IHC_QUANTITY = Convert.ToDecimal(IHC_QUANTITY);
                                            var IHC_UNIT_PRICE = i.unitPrice;
                                            Items.IHC_UNIT_PRICE = Convert.ToDecimal(IHC_UNIT_PRICE);
                                            var IHC_TOTAL_VALUE = i.totalValue;
                                            Items.IHC_TOTAL_VALUE = Convert.ToDecimal(IHC_TOTAL_VALUE);
                                            var IHC_IMPORTER_VALUE = i.importValue;
                                            Items.IHC_IMPORTER_VALUE = Convert.ToDecimal(IHC_IMPORTER_VALUE);
                                            var IHC_UOM = i.uom;
                                            Items.IHC_UOM = IHC_UOM;
                                            Items.IHC_GD_NO = GD_NO;
                                            Items.IHC_ENTRY_DATETIME = DateTime.Now;
                                            Items.IHC_MAKER_ID = "PSW";
                                            cont.PSW_GD_IMORT_HS_CODE.Add(Items);
                                            CurrentRowHSCODES += 1;
                                        }
                                        cont.SaveChanges();
                                        // GD HS CODE INFORMATION
                                    }
                                    cont.SaveChanges();
                                    // GD INFORMATION BLOCK

                                    // BDA INFORMATION BLOCK
                                    CurrentRow = 0;
                                    foreach (dynamic i in RequestData.data.bankAdviceInfo)
                                    {
                                        string bdaUniqueIdNumber = i.bdaUniqueIdNumber;
                                        string bdaDocumentRefNumber = i.bdaDocumentRefNumber;
                                        PSW_EIF_BDA BDAInfo = cont.PSW_EIF_BDA.Where(m => m.EIF_BDA_EIF_REQUEST_NO == finInsUniqueNumber && m.EIF_BDA_NO == bdaUniqueIdNumber).FirstOrDefault();
                                        if (BDAInfo == null)
                                        {
                                            BDAInfo = new PSW_EIF_BDA();
                                            int MAXID = Convert.ToInt32(cont.PSW_EIF_BDA.Max(m => (decimal?)m.EIF_BDA_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                BDAInfo.EIF_BDA_ID = MAXID;
                                            }
                                            else
                                            {
                                                BDAInfo.EIF_BDA_ID = (MAXID + CurrentRow);
                                            }
                                            BDAInfo.EIF_BDA_EIF_REQUEST_NO = finInsUniqueNumber;
                                            BDAInfo.EIF_BDA_NO = bdaUniqueIdNumber;
                                            BDAInfo.EIF_BDA_DOCUMENT_REQUEST_NO = bdaDocumentRefNumber;
                                            BDAInfo.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY = i.bdaInformation.totalBdaAmountFcy;
                                            string EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_Name = i.bdaInformation.totalBdaAmountCurrency;
                                            string EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_ID = cont.PSW_CURRENCY.Where(m => m.CUR_NAME == EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_Name).Select(m => m.CUR_ID).FirstOrDefault().ToString();
                                            BDAInfo.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY = EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_ID;
                                            BDAInfo.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE = i.bdaInformation.sampleAmountExclude;
                                            string EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY_Name = i.bdaInformation.sampleAmountCurrency;
                                            string EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY_CCY_ID = cont.PSW_CURRENCY.Where(m => m.CUR_NAME == EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY_Name).Select(m => m.CUR_ID).FirstOrDefault().ToString();
                                            BDAInfo.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY = EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY_CCY_ID;
                                            BDAInfo.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY = i.bdaInformation.netBdaAmountFcy;
                                            string EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY_Name = i.bdaInformation.netBdaAmountCurrency;
                                            string EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY_ID = cont.PSW_CURRENCY.Where(m => m.CUR_NAME == EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY_Name).Select(m => m.CUR_ID).FirstOrDefault().ToString();
                                            BDAInfo.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY = EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY_ID;
                                            BDAInfo.EIF_BDA_NET_BDA_AMOUNT_PKR = i.bdaInformation.netBdaAmountPkr;
                                            BDAInfo.EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY = i.bdaInformation.exchangeRateFcy;
                                            BDAInfo.EIF_BDA_AMOUNT_IN_WORDS = i.bdaInformation.amountInWords;
                                            BDAInfo.EIF_BDA_CURRENCY_FCY = EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_ID;
                                            BDAInfo.EIF_BDA_EXCHANGE_RATE_FCY = i.bdaInformation.exchangeRateFcy;
                                            BDAInfo.EIF_BDA_AMOUNT_FCY = i.bdaInformation.bdaAmountFcy;
                                            BDAInfo.EIF_BDA_Balance_Amount_FCY = i.bdaInformation.balanceBdaAmountFcy;
                                            BDAInfo.EIF_BDA_AMOUNT_PKR = i.bdaInformation.bdaAmountPkr;
                                            BDAInfo.EIF_BDA_COMMMISSION_FCY = i.bdaInformation.commisionAmountFcy;
                                            BDAInfo.EIF_BDA_COMMISSION_PKR = i.bdaInformation.commisionAmountPkr;
                                            BDAInfo.EIF_BDA_FED_AMOUNT_FCY = i.bdaInformation.fedFcy;
                                            BDAInfo.EIF_BDA_FED_AMOUNT_PKR = i.bdaInformation.fedAmountPkr;
                                            BDAInfo.EIF_BDA_SWIFT_CHARGES_PKR = i.bdaInformation.swiftChargesPkr;
                                            BDAInfo.EIF_BDA_OTHER_CHARGES_PKR = i.bdaInformation.otherChargesPkr;
                                            BDAInfo.EIF_BDA_REMARKS = i.bdaInformation.remarks;
                                            BDAInfo.EIF_BDA_MAKER_ID = "PSW";
                                            BDAInfo.EIF_BDA_ENTRY_DATETIME = DateTime.Now;
                                            BDAInfo.EIF_BDA_STATUS = "APPROVED";
                                            BDAInfo.EIF_BDA_GD_NO = i.gdNumber;
                                            cont.PSW_EIF_BDA.Add(BDAInfo);
                                            cont.SaveChanges();
                                        }
                                        else
                                        {
                                            BDAInfo.EIF_BDA_EIF_REQUEST_NO = finInsUniqueNumber;
                                            BDAInfo.EIF_BDA_NO = bdaUniqueIdNumber;
                                            BDAInfo.EIF_BDA_DOCUMENT_REQUEST_NO = bdaDocumentRefNumber;
                                            BDAInfo.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY = i.bdaInformation.totalBdaAmountFcy;
                                            string EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_Name = i.bdaInformation.totalBdaAmountCurrency;
                                            string EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_ID = cont.PSW_CURRENCY.Where(m => m.CUR_NAME == EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_Name).Select(m => m.CUR_ID).FirstOrDefault().ToString();
                                            BDAInfo.EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY = EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_ID;
                                            BDAInfo.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE = i.bdaInformation.sampleAmountExclude;
                                            string EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY_Name = i.bdaInformation.sampleAmountCurrency;
                                            string EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY_CCY_ID = cont.PSW_CURRENCY.Where(m => m.CUR_NAME == EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY_Name).Select(m => m.CUR_ID).FirstOrDefault().ToString();
                                            BDAInfo.EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY = EIF_BDA_SAMPLE_AMOUNT_EXCLUDE_CCY_CCY_ID;
                                            BDAInfo.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY = i.bdaInformation.netBdaAmountFcy;
                                            string EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY_Name = i.bdaInformation.netBdaAmountCurrency;
                                            string EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY_ID = cont.PSW_CURRENCY.Where(m => m.CUR_NAME == EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY_Name).Select(m => m.CUR_ID).FirstOrDefault().ToString();
                                            BDAInfo.EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY = EIF_BDA_NET_BDA_AMOUNT_EIF_FCY_CCY_ID;
                                            BDAInfo.EIF_BDA_NET_BDA_AMOUNT_PKR = i.bdaInformation.netBdaAmountPkr;
                                            BDAInfo.EIF_BDA_BDA_EXCHANGE_RATE_EIF_FCY = i.bdaInformation.exchangeRateFcy;
                                            BDAInfo.EIF_BDA_AMOUNT_IN_WORDS = i.bdaInformation.amountInWords;
                                            BDAInfo.EIF_BDA_CURRENCY_FCY = EIF_BDA_TOTAL_BDA_AMOUNT_EIF_FCY_CCY_ID;
                                            BDAInfo.EIF_BDA_EXCHANGE_RATE_FCY = i.bdaInformation.exchangeRateFcy;
                                            BDAInfo.EIF_BDA_AMOUNT_FCY = i.bdaInformation.bdaAmountFcy;
                                            BDAInfo.EIF_BDA_Balance_Amount_FCY = i.bdaInformation.balanceBdaAmountFcy;
                                            BDAInfo.EIF_BDA_AMOUNT_PKR = i.bdaInformation.bdaAmountPkr;
                                            BDAInfo.EIF_BDA_COMMMISSION_FCY = i.bdaInformation.commisionAmountFcy;
                                            BDAInfo.EIF_BDA_COMMISSION_PKR = i.bdaInformation.commisionAmountPkr;
                                            BDAInfo.EIF_BDA_FED_AMOUNT_FCY = i.bdaInformation.fedFcy;
                                            BDAInfo.EIF_BDA_FED_AMOUNT_PKR = i.bdaInformation.fedAmountPkr;
                                            BDAInfo.EIF_BDA_SWIFT_CHARGES_PKR = i.bdaInformation.swiftChargesPkr;
                                            BDAInfo.EIF_BDA_OTHER_CHARGES_PKR = i.bdaInformation.otherChargesPkr;
                                            BDAInfo.EIF_BDA_REMARKS = i.bdaInformation.remarks;
                                            BDAInfo.EIF_BDA_MAKER_ID = "PSW";
                                            BDAInfo.EIF_BDA_EDIT_DATETIME = DateTime.Now;
                                            BDAInfo.EIF_BDA_STATUS = "APPROVED";
                                            BDAInfo.EIF_BDA_GD_NO = i.gdNumber;
                                            cont.Entry(BDAInfo).State = System.Data.Entity.EntityState.Modified;
                                            cont.SaveChanges();
                                        }
                                        CurrentRow += 1;
                                    }
                                    // BDA INFORMATION BLOCK


                                    PSW_CHANGE_OF_BANK_REQUEST COBReq = new PSW_CHANGE_OF_BANK_REQUEST();
                                    COBReq.COB_ID = Convert.ToInt32(cont.PSW_CHANGE_OF_BANK_REQUEST.Max(m => (decimal?)m.COB_ID)) + 1;
                                    COBReq.COB_UNIQUE_NO = cobUniqueIdNumber;
                                    COBReq.COB_TRANS_TYPE = "01";
                                    COBReq.COB_IBAN = importerIban;
                                    COBReq.COB_REQUEST_NO = finInsUniqueNumber;
                                    COBReq.COB_STATUS = null;
                                    COBReq.COB_DATA_ENTRY_DATETIME = DateTime.Now;
                                    COBReq.METHODID = 1539;
                                    cont.PSW_CHANGE_OF_BANK_REQUEST.Add(COBReq);
                                    cont.SaveChanges();

                                    transaction.Commit();

                                    // Success Response 
                                    ShareGDNFinancialInfoWithOtherADResponse data = new ShareGDNFinancialInfoWithOtherADResponse();
                                    data.messageId = Guid.NewGuid().ToString();
                                    data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                                    data.senderId = "CBN";
                                    data.receiverId = "PSW";
                                    data.processingCode = "307";
                                    data.data = new ShareGDNFinancialInfoWithOtherADResponse.DetailData { };
                                    data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                                    data.message = new ShareGDNFinancialInfoWithOtherADResponse.MessageData
                                    {
                                        code = "200",
                                        description = "Change of bank request with GD and financial information received."
                                    };
                                    ConvertMessageToEntity(data);
                                    return Request.CreateResponse(HttpStatusCode.OK, data);
                                    // Success Response 
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Importer Exist in the system with the provided IBAN : " + importerIban + ", NTN : " + importerNtn + "."));
                                }
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Exception Occurred While Saving Data : " + ex.Message));
                            }
                        }
                    }
                }
                else if (tradeTranType == "02")
                {
                    using (PSWEntities cont = new PSWEntities())
                    {
                        using (var transaction = cont.Database.BeginTransaction())
                        {
                            try
                            {
                                int CurrentRow = 0;
                                string exporterNtn = RequestData.data.financialInstrumentInfo.exporterNtn;
                                string exporterName = RequestData.data.financialInstrumentInfo.exporterName;
                                string exporterIban = RequestData.data.iban;
                                PSW_CLIENT_MASTER_DETAIL client = cont.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == exporterIban && m.C_NTN_NO == exporterNtn).FirstOrDefault();
                                if (client != null)
                                {

                                    string modeOfPayment = RequestData.data.financialInstrumentInfo.modeOfPayment;
                                    int PayModeCode = 0;
                                    int.TryParse(modeOfPayment, out PayModeCode);
                                    string finInsUniqueNumber = RequestData.data.financialInstrumentInfo.finInsUniqueNumber;
                                    int advPayPercentage = 0;
                                    int docAgainstPayPercentage = 0;
                                    int docAgainstAcceptancePercentage = 0;
                                    int days = 0;
                                    int totalPercentage = 0;
                                    int sightPercentage = 0;
                                    int usancePercentage = 0;
                                    string advPayPercentageFormated = "0";
                                    string daysFormatted = "0";
                                    string totalPercentageFormatted = "0";
                                    string docAgainstPayPercentageFormatted = "0";
                                    string sightPercentageFormated = "0";
                                    string usancePercentageFormated = "0";
                                    string docAgainstAcceptancePercentageFormatted = "0";
                                    string consigneeName = RequestData.data.financialInstrumentInfo.paymentInformation.consigneeName;
                                    string consigneeAddress = RequestData.data.financialInstrumentInfo.paymentInformation.consigneeAddress;
                                    string consigneeCountry = RequestData.data.financialInstrumentInfo.paymentInformation.consigneeCountry;
                                    string consigneeIban = RequestData.data.financialInstrumentInfo.paymentInformation.consigneeIban;
                                    string portOfDischarge = RequestData.data.financialInstrumentInfo.paymentInformation.portOfDischarge;
                                    string deliveryTerms = RequestData.data.financialInstrumentInfo.paymentInformation.deliveryTerms;
                                    string financialInstrumentCurrency = RequestData.data.financialInstrumentInfo.paymentInformation.financialInstrumentCurrency;
                                    decimal financialInstrumentValue = RequestData.data.financialInstrumentInfo.paymentInformation.financialInstrumentValue;
                                    string expiryDate = DateTime.ParseExact(
                                    Convert.ToString(RequestData.data.financialInstrumentInfo.paymentInformation.expiryDate)
                                        , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                    PSW_AUTH_PAY_MODES PayMode = cont.PSW_AUTH_PAY_MODES.Where(m => m.APM_CODE == PayModeCode).FirstOrDefault();
                                    PSW_FORM_E_DOCUMENT_INFO docInfo = cont.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == finInsUniqueNumber).FirstOrDefault();
                                    PSW_PAYMENT_MODES_PARAMETERS PayParam = cont.PSW_PAYMENT_MODES_PARAMETERS.Where(m => m.EIF_PMP_EIF_REQUEST_NO == finInsUniqueNumber).FirstOrDefault();
                                    PSW_FORM_E_DOCUMENT_REQUEST_INFO docreqinfo = cont.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Where(m => m.FDRI_FORM_E_NO == finInsUniqueNumber).FirstOrDefault();
                                    PSW_LIST_OF_COUNTRIES ConsigneeCountryId = cont.PSW_LIST_OF_COUNTRIES.Where(m => m.LC_CODE == consigneeCountry).FirstOrDefault();
                                    PSW_CURRENCY FDRI_CURRENCYID = cont.PSW_CURRENCY.Where(m => m.CUR_NAME == financialInstrumentCurrency).FirstOrDefault();
                                    PSW_DELIVERY_TERMS DeliveryTerm = cont.PSW_DELIVERY_TERMS.Where(m => m.DT_CODE == deliveryTerms).FirstOrDefault();

                                    if (docInfo == null)
                                    {
                                        docInfo = new PSW_FORM_E_DOCUMENT_INFO();
                                        docInfo.FDI_ID = Convert.ToInt32(cont.PSW_FORM_E_DOCUMENT_INFO.Max(m => (decimal?)m.FDI_ID)) + 1;
                                        docInfo.FDI_FORM_E_NO = finInsUniqueNumber;
                                        docInfo.FDI_STATUS = "APPROVED";
                                        docInfo.FDI_STATUS_DATE = DateTime.Now;
                                        docInfo.FDI_MODE_OF_EXPORT_PAYMENT = PayMode.APM_ID;
                                        docInfo.FDI_PHASE_OF_ISSUE = "Recieved From PSW";
                                        docInfo.FDI_ISSUE_BANK = "Citi Bank";
                                        docInfo.FDI_NTN = client.C_NTN_NO;
                                        docInfo.FDI_TRADER_NAME = client.C_IBAN_NO;
                                        docInfo.FDI_TRADER_ADDRESS = client.C_ADDRESS;
                                        docInfo.FDI_ENTRY_DATETIME = DateTime.Now;
                                        docInfo.FDI_MAKER_ID = "PSW";
                                        docInfo.FDI_REMARKS = "";
                                        docInfo.FDI_CERTIFIED = false;
                                        docInfo.FDI_DOC_SUBMISSION = false;
                                        docInfo.FDI_LC_CONTRACT_NO = "-";
                                        cont.PSW_FORM_E_DOCUMENT_INFO.Add(docInfo);
                                        cont.SaveChanges();
                                    }
                                    else
                                    {
                                        docInfo.FDI_FORM_E_NO = finInsUniqueNumber;
                                        docInfo.FDI_STATUS = "APPROVED";
                                        docInfo.FDI_STATUS_DATE = DateTime.Now;
                                        docInfo.FDI_MODE_OF_EXPORT_PAYMENT = PayMode.APM_ID;
                                        docInfo.FDI_PHASE_OF_ISSUE = "Recieved From PSW";
                                        docInfo.FDI_ISSUE_BANK = "Citi Bank";
                                        docInfo.FDI_NTN = client.C_NTN_NO;
                                        docInfo.FDI_TRADER_NAME = exporterIban;
                                        docInfo.FDI_TRADER_ADDRESS = client.C_ADDRESS;
                                        docInfo.FDI_ENTRY_DATETIME = DateTime.Now;
                                        docInfo.FDI_MAKER_ID = "PSW";
                                        docInfo.FDI_REMARKS = "";
                                        docInfo.FDI_CERTIFIED = false;
                                        docInfo.FDI_DOC_SUBMISSION = false;
                                        docInfo.FDI_LC_CONTRACT_NO = "-";
                                        cont.Entry(docInfo).State = System.Data.Entity.EntityState.Modified;
                                        cont.SaveChanges();
                                    }
                                    string GD_NUMBER = "";
                                    if (PayParam == null)
                                    {
                                        PayParam = new PSW_PAYMENT_MODES_PARAMETERS();
                                        PayParam.EIF_PMP_ID = Convert.ToInt32(cont.PSW_PAYMENT_MODES_PARAMETERS.Max(m => (decimal?)m.EIF_PMP_ID)) + 1;
                                        PayParam.EIF_PMP_EIF_REQUEST_NO = finInsUniqueNumber;
                                        switch (PayMode.APM_CODE.ToString())
                                        {
                                            case "305":
                                                GD_NUMBER = RequestData.data.financialInstrumentInfo.openAccountData.gdNumber;
                                                PayParam.EIF_PMP_GD_NO = GD_NUMBER;
                                                break;
                                            case "307": /// LC 
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                sightPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.sightPercentage);
                                                int.TryParse(sightPercentageFormated, out sightPercentage);
                                                PayParam.EIF_PMP_SIGHT_PERCENT = sightPercentage;
                                                usancePercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.usancePercentage);
                                                int.TryParse(usancePercentageFormated, out usancePercentage);
                                                PayParam.EIF_PMP_USANCE_PERCENT = usancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "308": /// Contract Collection
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                docAgainstPayPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstPayPercentage);
                                                int.TryParse(docAgainstPayPercentageFormatted, out docAgainstPayPercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = docAgainstPayPercentage;
                                                docAgainstAcceptancePercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstAcceptancePercentage);
                                                int.TryParse(docAgainstAcceptancePercentageFormatted, out docAgainstAcceptancePercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = docAgainstAcceptancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            default:
                                                break;
                                        }
                                        PayParam.EIF_PMP_COMMISION_CHARGES = 0;
                                        PayParam.EIF_PMP_FED_CHARGES = 0;
                                        PayParam.EIF_PMP_ENTRY_DATETIME = DateTime.Now;
                                        PayParam.EIF_PMP_MAKER_ID = "PSW";
                                        cont.PSW_PAYMENT_MODES_PARAMETERS.Add(PayParam);
                                    }
                                    else
                                    {
                                        PayParam.EIF_PMP_EIF_REQUEST_NO = finInsUniqueNumber;
                                        switch (PayMode.APM_CODE.ToString())
                                        {
                                            case "305":
                                                GD_NUMBER = RequestData.data.financialInstrumentInfo.openAccountData.gdNumber;
                                                PayParam.EIF_PMP_GD_NO = GD_NUMBER;
                                                break;
                                            case "307": /// LC 
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                sightPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.sightPercentage);
                                                int.TryParse(sightPercentageFormated, out sightPercentage);
                                                PayParam.EIF_PMP_SIGHT_PERCENT = sightPercentage;
                                                usancePercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.usancePercentage);
                                                int.TryParse(usancePercentageFormated, out usancePercentage);
                                                PayParam.EIF_PMP_USANCE_PERCENT = usancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.lcData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            case "308": /// Contract Collection
                                                advPayPercentageFormated = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.advPayPercentage);
                                                int.TryParse(advPayPercentageFormated, out advPayPercentage);
                                                PayParam.EIF_PMP_ADV_PAYPERCENT = advPayPercentage;
                                                docAgainstPayPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstPayPercentage);
                                                int.TryParse(docAgainstPayPercentageFormatted, out docAgainstPayPercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_PAYPERCENT = docAgainstPayPercentage;
                                                docAgainstAcceptancePercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.docAgainstAcceptancePercentage);
                                                int.TryParse(docAgainstAcceptancePercentageFormatted, out docAgainstAcceptancePercentage);
                                                PayParam.EIF_PMP_DOCUMENT_AGAINST_ACCEPT_PERCENT = docAgainstAcceptancePercentage;
                                                daysFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.days);
                                                int.TryParse(daysFormatted, out days);
                                                PayParam.EIF_PMP_DAYS = days;
                                                totalPercentageFormatted = Convert.ToString(RequestData.data.financialInstrumentInfo.contractCollectionData.totalPercentage);
                                                int.TryParse(totalPercentageFormatted, out totalPercentage);
                                                PayParam.EIF_PMP_PERCENTAGE = totalPercentage;
                                                break;
                                            default:
                                                break;
                                        }
                                        PayParam.EIF_PMP_COMMISION_CHARGES = 0;
                                        PayParam.EIF_PMP_FED_CHARGES = 0;
                                        PayParam.EIF_PMP_ENTRY_DATETIME = DateTime.Now;
                                        PayParam.EIF_PMP_MAKER_ID = "PSW";
                                        cont.Entry(PayParam).State = System.Data.Entity.EntityState.Modified;
                                    }

                                    if (docreqinfo == null)
                                    {
                                        docreqinfo = new PSW_FORM_E_DOCUMENT_REQUEST_INFO();
                                        docreqinfo.FDRI_ID = Convert.ToInt32(cont.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Max(m => (decimal?)m.FDRI_ID)) + 1;
                                        docreqinfo.FDRI_FORM_E_NO = docInfo.FDI_FORM_E_NO;
                                        docreqinfo.FDRI_CONSIG_NAME = consigneeName;
                                        docreqinfo.FDRI_CONSID_ADDRESS = consigneeAddress;
                                        docreqinfo.FDRI_CONSIGNEE_IBAN = consigneeIban == null ? null : consigneeIban;
                                        docreqinfo.FDRI_COUNTRY = ConsigneeCountryId.LC_ID;
                                        docreqinfo.FDRI_PORT_OF_DISCHARGE = portOfDischarge == null ? null : portOfDischarge;
                                        docreqinfo.FDRI_CURRENCY = FDRI_CURRENCYID.CUR_ID;
                                        docreqinfo.FDRI_DELIVERY_TERMS = DeliveryTerm.DT_ID;
                                        docreqinfo.FDRI_TOTAL_VALUE_OF_SHIPMENT = financialInstrumentValue;
                                        docreqinfo.FDRI_FINAN_EXPIRY = Convert.ToDateTime(expiryDate);
                                        docreqinfo.FDRI_ENTRY_DATETIME = DateTime.Now;
                                        docreqinfo.FDRI_MAKER_ID = docInfo.FDI_MAKER_ID;
                                        cont.PSW_FORM_E_DOCUMENT_REQUEST_INFO.Add(docreqinfo);
                                        cont.SaveChanges();
                                    }
                                    else
                                    {
                                        docreqinfo.FDRI_FORM_E_NO = docInfo.FDI_FORM_E_NO;
                                        docreqinfo.FDRI_CONSIG_NAME = consigneeName;
                                        docreqinfo.FDRI_CONSID_ADDRESS = consigneeAddress;
                                        docreqinfo.FDRI_CONSIGNEE_IBAN = consigneeIban == null ? null : consigneeIban;
                                        docreqinfo.FDRI_COUNTRY = ConsigneeCountryId.LC_ID;
                                        docreqinfo.FDRI_PORT_OF_DISCHARGE = portOfDischarge == null ? null : portOfDischarge;
                                        docreqinfo.FDRI_CURRENCY = FDRI_CURRENCYID.CUR_ID;
                                        docreqinfo.FDRI_DELIVERY_TERMS = DeliveryTerm.DT_ID;
                                        docreqinfo.FDRI_TOTAL_VALUE_OF_SHIPMENT = financialInstrumentValue;
                                        docreqinfo.FDRI_FINAN_EXPIRY = Convert.ToDateTime(expiryDate);
                                        docreqinfo.FDRI_ENTRY_DATETIME = DateTime.Now;
                                        docreqinfo.FDRI_MAKER_ID = docInfo.FDI_MAKER_ID;
                                        cont.Entry(docreqinfo).State = System.Data.Entity.EntityState.Modified;
                                    }

                                    // HS CODE Information
                                    CurrentRow = 0;
                                    foreach (dynamic i in RequestData.data.financialInstrumentInfo.itemInformation)
                                    {
                                        string hsCode = i.hsCode;
                                        var goodsDescription = i.goodsDescription;
                                        var quantity = i.quantity;
                                        var uom = i.uom;
                                        var countryOfOrigin = i.countryOfOrigin;
                                        var itemInvoiceValue = i.itemInvoiceValue;

                                        PSW_EEF_HS_CODE ItemInformation = cont.PSW_EEF_HS_CODE.Where(m => m.EEF_HC_EEF_REQUEST_NO == finInsUniqueNumber && m.EEF_HC_CODE == hsCode).FirstOrDefault();
                                        if (ItemInformation == null)
                                        {
                                            ItemInformation = new PSW_EEF_HS_CODE();
                                            int MAXID = Convert.ToInt32(cont.PSW_EIF_HS_CODE.Max(m => (decimal?)m.EIF_HC_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                ItemInformation.EEF_HC_ID = MAXID;
                                            }
                                            else
                                            {
                                                ItemInformation.EEF_HC_ID = (MAXID + CurrentRow);
                                            }
                                            ItemInformation.EEF_HC_EEF_REQUEST_NO = finInsUniqueNumber;
                                            ItemInformation.EEF_HC_CODE = hsCode;
                                            ItemInformation.EEF_HC_DESCRIPTION = goodsDescription;
                                            ItemInformation.EEF_HC_QUANTITY = quantity;
                                            ItemInformation.EEF_HC_UOM = uom;
                                            ItemInformation.EEF_HC_ORGIN = countryOfOrigin;
                                            ItemInformation.EEF_HC_INVOICE_VALUE = itemInvoiceValue;
                                            ItemInformation.EEF_HC_ENTRTY_DATETIME = DateTime.Now;
                                            ItemInformation.EEF_HC_MAKER_ID = "PSW";
                                            cont.PSW_EEF_HS_CODE.Add(ItemInformation);
                                        }
                                        else
                                        {
                                            ItemInformation.EEF_HC_DESCRIPTION = goodsDescription;
                                            ItemInformation.EEF_HC_QUANTITY = quantity;
                                            ItemInformation.EEF_HC_UOM = uom;
                                            ItemInformation.EEF_HC_ORGIN = countryOfOrigin;
                                            ItemInformation.EEF_HC_INVOICE_VALUE = itemInvoiceValue;
                                            ItemInformation.EEF_HC_ENTRTY_DATETIME = DateTime.Now;
                                            ItemInformation.EEF_HC_MAKER_ID = "PSW";
                                            cont.Entry(ItemInformation).State = System.Data.Entity.EntityState.Modified;
                                        }
                                        cont.SaveChanges();
                                        CurrentRow += 1;
                                    }
                                    // HS CODE ItemInformation End


                                    // GD INFORMATION BLOCK
                                    CurrentRow = 0;
                                    foreach (dynamic i in RequestData.data.gdInfo)
                                    {
                                        string GD_NO = "";
                                        string collectorate = "";
                                        string portOfDelivery = "";
                                        string terminalLocation = "";
                                        string consignmentCategory = "";
                                        string gdType = "";
                                        string gdStatus = "";
                                        string GdportOfDischarge = "";
                                        string ntnFtn = "";
                                        string strn = "";
                                        string GdconsigneeName = "";
                                        string GdconsigneeAddress = "";
                                        string consignorName = "";
                                        string consignorAddress = "";
                                        string grossWeight = "";
                                        string blAwbNumber = "";
                                        string blAwbDate = "";
                                        string clearanceDate = "";
                                        string virAirNumber = "";
                                        string portOfShipment = "";
                                        string netWeight = "";

                                        string GdexporterIban = "";
                                        string GdmodeOfPayment = "";
                                        string currency = "";
                                        string invoiceNumber = "";
                                        string invoiceDate = "";
                                        string deliveryTerm = "";
                                        string consignmentType = "";
                                        string shippingLine = "";


                                        GD_NO = i.gdNumber;
                                        collectorate = i.collectorate;
                                        portOfDelivery = i.generalInformation.placeOfDelivery;
                                        terminalLocation = i.generalInformation.terminalLocation;
                                        consignmentCategory = i.consignmentCategory;
                                        gdType = i.gdType;
                                        gdStatus = i.gdStatus;
                                        GdportOfDischarge = i.generalInformation.portOfDischarge;
                                        ntnFtn = i.consignorConsigneeInfo.ntnFtn;
                                        strn = i.consignorConsigneeInfo.strn;
                                        GdconsigneeName = i.consignorConsigneeInfo.consigneeName;
                                        consigneeAddress = i.consignorConsigneeInfo.consigneeAddress;
                                        consignorName = i.consignorConsigneeInfo.consignorName;
                                        consignorAddress = i.consignorConsigneeInfo.consignorAddress;
                                        grossWeight = i.generalInformation.grossWeight;
                                        blAwbNumber = i.blAwbNumber;
                                        blAwbDate = (i.blAwbDate == null ? null : DateTime.ParseExact(Convert.ToString(i.blawbDate),
                                                          "yyyyMMdd",
                                                           CultureInfo.InvariantCulture).ToString());
                                        virAirNumber = i.virAirNumber;
                                        if (i.financialInfo.clearanceDate != null)
                                        {
                                            clearanceDate = DateTime.ParseExact(Convert.ToString(i.financialInfo.clearanceDate),
                                                          "yyyyMMdd",
                                                           CultureInfo.InvariantCulture).ToString();
                                        }
                                        else
                                            clearanceDate = null;
                                        portOfShipment = i.generalInformation.portOfShipment;
                                        netWeight = i.generalInformation.netWeight;
                                        exporterIban = client.C_IBAN_NO;
                                        modeOfPayment = i.financialInfo.modeOfPayment;
                                        currency = i.financialInfo.currency;
                                        invoiceNumber = i.financialInfo.invoiceNumber;
                                        invoiceDate = (i.financialInfo.invoiceDate == null ? null : DateTime.ParseExact(Convert.ToString(i.financialInfo.invoiceDate),
                                                          "yyyyMMdd",
                                                           CultureInfo.InvariantCulture).ToString());
                                        deliveryTerm = i.financialInfo.deliveryTerm;
                                        consignmentType = i.generalInformation.consignmentType;
                                        shippingLine = i.generalInformation.shippingLine;


                                        var totalDeclaredValue = i.financialInfo.totalDeclaredValue;
                                        var fobValueUsd = i.financialInfo.fobValueUsd;
                                        var freightUsd = i.financialInfo.freightUsd;
                                        var cfrValueUsd = i.financialInfo.cfrValueUsd;
                                        var insuranceUsd = i.financialInfo.insuranceUsd;
                                        var landingChargesUsd = i.financialInfo.landingChargesUsd;
                                        var assessedValueUsd = i.financialInfo.assessedValueUsd;
                                        var OtherCharges = i.financialInfo.OtherCharges;
                                        var exchangeRate = i.financialInfo.exchangeRate;
                                        PSW_EEF_GD_GENERAL_INFO GeneralInfo = cont.PSW_EEF_GD_GENERAL_INFO.Where(m => m.EFGDGI_GD_NO == GD_NO).FirstOrDefault();
                                        PSW_EEF_GD_IMPORT_EXPORT_INFO IMPORTEXPORTInfo = cont.PSW_EEF_GD_IMPORT_EXPORT_INFO.Where(m => m.EFGDIE_GD_NO == GD_NO).FirstOrDefault();
                                        if (GeneralInfo == null)
                                        {
                                            GeneralInfo = new PSW_EEF_GD_GENERAL_INFO();
                                            GeneralInfo.EFGDGI_ID = Convert.ToInt32(cont.PSW_EEF_GD_GENERAL_INFO.Max(m => (decimal?)m.EFGDGI_ID)) + 1;
                                            GeneralInfo.EFGDGI_AGENT_NAME = "";
                                            GeneralInfo.EFGDGI_TRADE_TYPE = "Export";
                                            GeneralInfo.EFGDGI_GD_NO = GD_NO;
                                            GeneralInfo.EFGDGI_COLLECTORATE = collectorate;
                                            GeneralInfo.EFGDGI_DESTINATION_COUNTRY = "";
                                            GeneralInfo.EFGDGI_PLACE_OF_DELIVERY = portOfDelivery;
                                            GeneralInfo.EFGDGI_GENERAL_DESCRIPTION = "";
                                            GeneralInfo.EFGDGI_SHED_TERMINAL_LOCATION = terminalLocation;
                                            GeneralInfo.EFGDGI_AGENT_LICENSE_NO = "";
                                            GeneralInfo.EFGDGI_DECLARATION_TYPE = "";
                                            GeneralInfo.EFGDGI_CONSIGNMENT_CATE = consignmentCategory;
                                            GeneralInfo.EFGDGI_GD_TYPE = gdType;
                                            GeneralInfo.EFGDGI_GD_STATUS = gdStatus.Trim();
                                            GeneralInfo.EFGDGI_PORT_OF_DISCHARGE = GdportOfDischarge;
                                            GeneralInfo.EFGDGI_1ST_EXAMINATION = "";
                                            GeneralInfo.EFGDGI_ENTRY_DATETIME = DateTime.Now;
                                            GeneralInfo.EFGDGI_MAKER_ID = "PSW";
                                            GeneralInfo.EFGDGI_VIR_AIR_NUMBER = virAirNumber;
                                            if (clearanceDate != null)
                                                GeneralInfo.EFGDGI_CLEARANCE_DATE = Convert.ToDateTime(clearanceDate);
                                            else
                                                GeneralInfo.EFGDGI_CLEARANCE_DATE = null;
                                            GeneralInfo.EFGDGI_EXPORTER_IBAN = client.C_IBAN_NO;
                                            GeneralInfo.EFGDGI_MODE_OF_PAYMENT = modeOfPayment;
                                            GeneralInfo.EFGDGI_CURRENCY = currency;
                                            GeneralInfo.EFGDGI_TOTAL_DECLARED_VALUE = totalDeclaredValue;
                                            GeneralInfo.EFGDGI_INVOICE_NUMBER = invoiceNumber;
                                            if (invoiceDate != null)
                                                GeneralInfo.EFGDGI_INVOICE_DATE = Convert.ToDateTime(invoiceDate);
                                            GeneralInfo.EFGDGI_DELIVERY_TERM = deliveryTerm;
                                            GeneralInfo.EFGDGI_FOBVALUE_USD = fobValueUsd;
                                            GeneralInfo.EFGDGI_FREIGHT_USD = freightUsd;
                                            GeneralInfo.EFGDGI_CFR_VALUE_USD = cfrValueUsd;
                                            GeneralInfo.EFGDGI_INSURANCE_USD = insuranceUsd;
                                            GeneralInfo.EFGDGI_LANDING_CHARGES_USD = landingChargesUsd;
                                            GeneralInfo.EFGDGI_ASSESSED_VALUE_USD = assessedValueUsd;
                                            GeneralInfo.EFGDGI_OTHER_CHARGES = OtherCharges;
                                            GeneralInfo.EFGDGI_EXCHANGE_RATE = exchangeRate;
                                            GeneralInfo.EFGDGI_CONSIGNMENT_TYPE = consignmentType;
                                            GeneralInfo.EFGDGI_SHIPPING_LINE = shippingLine;
                                            var NegListExist = RequestData.data.GetType().GetProperty("negativeList");
                                            if (NegListExist != null)
                                            {
                                                GeneralInfo.EFGDGI_NEG_COUNTRY = RequestData.data.negativeList.country;
                                                GeneralInfo.EFGDGI_NEG_SUPPLIER = RequestData.data.negativeList.supplier;
                                                Array NEGCOMS = RequestData.data.negativeList.commodities;
                                                int ComCount = 0;
                                                string NEGCOMString = "";
                                                foreach (string Coms in NEGCOMS)
                                                {
                                                    if (ComCount == 0)
                                                        NEGCOMString += Coms;
                                                    else
                                                        NEGCOMString += "," + Coms;
                                                    ComCount += 1;
                                                }
                                                GeneralInfo.EFGDGI_NEG_COMMS = NEGCOMString;
                                            }
                                            cont.PSW_EEF_GD_GENERAL_INFO.Add(GeneralInfo);

                                        }
                                        else
                                        {
                                            GeneralInfo.EFGDGI_AGENT_NAME = "";
                                            GeneralInfo.EFGDGI_TRADE_TYPE = "Export";
                                            GeneralInfo.EFGDGI_GD_NO = GD_NO;
                                            GeneralInfo.EFGDGI_COLLECTORATE = collectorate;
                                            GeneralInfo.EFGDGI_DESTINATION_COUNTRY = "";
                                            GeneralInfo.EFGDGI_PLACE_OF_DELIVERY = portOfDelivery;
                                            GeneralInfo.EFGDGI_GENERAL_DESCRIPTION = "";
                                            GeneralInfo.EFGDGI_SHED_TERMINAL_LOCATION = terminalLocation;
                                            GeneralInfo.EFGDGI_AGENT_LICENSE_NO = "";
                                            GeneralInfo.EFGDGI_DECLARATION_TYPE = "";
                                            GeneralInfo.EFGDGI_CONSIGNMENT_CATE = consignmentCategory;
                                            GeneralInfo.EFGDGI_GD_TYPE = gdType;
                                            GeneralInfo.EFGDGI_GD_STATUS = gdStatus.Trim();
                                            GeneralInfo.EFGDGI_PORT_OF_DISCHARGE = GdportOfDischarge;
                                            GeneralInfo.EFGDGI_1ST_EXAMINATION = "";
                                            GeneralInfo.EFGDGI_ENTRY_DATETIME = DateTime.Now;
                                            GeneralInfo.EFGDGI_MAKER_ID = "PSW";
                                            GeneralInfo.EFGDGI_VIR_AIR_NUMBER = virAirNumber;
                                            if (clearanceDate != null)
                                                GeneralInfo.EFGDGI_CLEARANCE_DATE = Convert.ToDateTime(clearanceDate);
                                            else
                                                GeneralInfo.EFGDGI_CLEARANCE_DATE = null;
                                            GeneralInfo.EFGDGI_EXPORTER_IBAN = exporterIban;
                                            GeneralInfo.EFGDGI_MODE_OF_PAYMENT = modeOfPayment;
                                            GeneralInfo.EFGDGI_CURRENCY = currency;
                                            GeneralInfo.EFGDGI_TOTAL_DECLARED_VALUE = totalDeclaredValue;
                                            GeneralInfo.EFGDGI_INVOICE_NUMBER = invoiceNumber;
                                            if (invoiceDate != null)
                                                GeneralInfo.EFGDGI_INVOICE_DATE = Convert.ToDateTime(invoiceDate);
                                            GeneralInfo.EFGDGI_DELIVERY_TERM = deliveryTerm;
                                            GeneralInfo.EFGDGI_FOBVALUE_USD = fobValueUsd;
                                            GeneralInfo.EFGDGI_FREIGHT_USD = freightUsd;
                                            GeneralInfo.EFGDGI_CFR_VALUE_USD = cfrValueUsd;
                                            GeneralInfo.EFGDGI_INSURANCE_USD = insuranceUsd;
                                            GeneralInfo.EFGDGI_LANDING_CHARGES_USD = landingChargesUsd;
                                            GeneralInfo.EFGDGI_ASSESSED_VALUE_USD = assessedValueUsd;
                                            GeneralInfo.EFGDGI_OTHER_CHARGES = OtherCharges;
                                            GeneralInfo.EFGDGI_EXCHANGE_RATE = exchangeRate;
                                            GeneralInfo.EFGDGI_CONSIGNMENT_TYPE = consignmentType;
                                            GeneralInfo.EFGDGI_SHIPPING_LINE = shippingLine;
                                            var NegListExist = RequestData.data.GetType().GetProperty("negativeList");
                                            if (NegListExist != null)
                                            {
                                                GeneralInfo.EFGDGI_NEG_COUNTRY = RequestData.data.negativeList.country;
                                                GeneralInfo.EFGDGI_NEG_SUPPLIER = RequestData.data.negativeList.supplier;
                                                Array NEGCOMS = RequestData.data.negativeList.commodities;
                                                int ComCount = 0;
                                                string NEGCOMString = "";
                                                foreach (string Coms in NEGCOMS)
                                                {
                                                    if (ComCount == 0)
                                                        NEGCOMString += Coms;
                                                    else
                                                        NEGCOMString += "," + Coms;
                                                    ComCount += 1;
                                                }
                                                GeneralInfo.EFGDGI_NEG_COMMS = NEGCOMString;
                                            }
                                            cont.Entry(GeneralInfo).State = System.Data.Entity.EntityState.Modified;
                                        }

                                        if (IMPORTEXPORTInfo == null)
                                        {
                                            IMPORTEXPORTInfo = new PSW_EEF_GD_IMPORT_EXPORT_INFO();
                                            int MAXID = Convert.ToInt32(cont.PSW_EEF_GD_IMPORT_EXPORT_INFO.Max(m => (decimal?)m.EFGDIE_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                IMPORTEXPORTInfo.EFGDIE_ID = MAXID;
                                            }
                                            else
                                            {
                                                IMPORTEXPORTInfo.EFGDIE_ID = (MAXID + CurrentRow);
                                            }
                                            IMPORTEXPORTInfo.EFGDIE_NTN_FTN = ntnFtn;
                                            IMPORTEXPORTInfo.EFGDIE_STRN = strn;
                                            IMPORTEXPORTInfo.EFGDIE_GD_NO = GD_NO;
                                            IMPORTEXPORTInfo.EFGDIE_CONSIGNEE_NAME = GdconsigneeName;
                                            IMPORTEXPORTInfo.EFGDIE_CONSIGNEE_ADDRESS = GdconsigneeAddress;
                                            IMPORTEXPORTInfo.EFGDIE_CONSIGNOR_NAME = consignorName;
                                            IMPORTEXPORTInfo.EFGDIE_CONSIGNOR_ADDRESS = consignorAddress;
                                            IMPORTEXPORTInfo.EFGDIE_ENTRY_DATETIME = DateTime.Now;
                                            IMPORTEXPORTInfo.EFGDIE_MAKER_ID = "PSW";
                                            cont.PSW_EEF_GD_IMPORT_EXPORT_INFO.Add(IMPORTEXPORTInfo);
                                        }
                                        else
                                        {
                                            IMPORTEXPORTInfo.EFGDIE_NTN_FTN = ntnFtn;
                                            IMPORTEXPORTInfo.EFGDIE_STRN = strn;
                                            IMPORTEXPORTInfo.EFGDIE_GD_NO = GD_NO;
                                            IMPORTEXPORTInfo.EFGDIE_CONSIGNEE_NAME = GdconsigneeName;
                                            IMPORTEXPORTInfo.EFGDIE_CONSIGNEE_ADDRESS = GdconsigneeAddress;
                                            IMPORTEXPORTInfo.EFGDIE_CONSIGNOR_NAME = consignorName;
                                            IMPORTEXPORTInfo.EFGDIE_CONSIGNOR_ADDRESS = consignorAddress;
                                            IMPORTEXPORTInfo.EFGDIE_ENTRY_DATETIME = DateTime.Now;
                                            IMPORTEXPORTInfo.EFGDIE_MAKER_ID = "PSW";
                                            cont.Entry(IMPORTEXPORTInfo).State = System.Data.Entity.EntityState.Modified;
                                        }


                                        PSW_EEF_GD_INFORMATION GDInfo = cont.PSW_EEF_GD_INFORMATION.Where(m => m.EFGDI_GD_NO == GD_NO).FirstOrDefault();
                                        if (GDInfo == null)
                                        {
                                            GDInfo = new PSW_EEF_GD_INFORMATION();
                                            int MAXID = Convert.ToInt32(cont.PSW_EEF_GD_INFORMATION.Max(m => (decimal?)m.EFGDI_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                GDInfo.EFGDI_ID = MAXID;
                                            }
                                            else
                                            {
                                                GDInfo.EFGDI_ID = (MAXID + CurrentRow);
                                            }
                                            GDInfo.EFGDI_EGM_COLLECTORATE = collectorate;
                                            GDInfo.EFGDI_GD_NO = GD_NO;
                                            GDInfo.EFGDI_EGM_NUMBER = "-";
                                            GDInfo.EFGDI_EGM_EXPORTER_NAME = "-";
                                            GDInfo.EFGDI_VESSEL_NAME = "";
                                            GDInfo.EFGDI_GROSS_WEIGHT = grossWeight;
                                            GDInfo.EFGDI_COSIGNMENT_TYPE = "";
                                            GDInfo.EFGDI_SECTION = "";
                                            GDInfo.EFGDI_INDEX_NO = "";
                                            GDInfo.EFGDI_BL_NO = blAwbNumber == null ? null : blAwbNumber;
                                            if (blAwbDate != null)
                                                GDInfo.EFGDI_BL_DATE = Convert.ToDateTime(blAwbDate);
                                            GDInfo.EFGDI_PORT_OF_SHIPMENT = portOfShipment;
                                            GDInfo.EFGDI_NET_WEIGHT = netWeight;
                                            GDInfo.EFGDI_SHIPPING_LINE = "";
                                            GDInfo.EFGDI_BERTHING_TERMINAL = "";
                                            GDInfo.EFGDI_ENTRY_DATETIME = DateTime.Now;
                                            GDInfo.EFGDI_MAKER_ID = "PSW";
                                            cont.PSW_EEF_GD_INFORMATION.Add(GDInfo);
                                        }
                                        else
                                        {
                                            GDInfo.EFGDI_EGM_COLLECTORATE = collectorate;
                                            GDInfo.EFGDI_GD_NO = GD_NO;
                                            GDInfo.EFGDI_EGM_NUMBER = "-";
                                            GDInfo.EFGDI_EGM_EXPORTER_NAME = "-";
                                            GDInfo.EFGDI_VESSEL_NAME = "";
                                            GDInfo.EFGDI_GROSS_WEIGHT = grossWeight;
                                            GDInfo.EFGDI_COSIGNMENT_TYPE = "";
                                            GDInfo.EFGDI_SECTION = "";
                                            GDInfo.EFGDI_INDEX_NO = "";
                                            GDInfo.EFGDI_BL_NO = blAwbNumber == null ? null : blAwbNumber;
                                            if (blAwbDate != null)
                                                GDInfo.EFGDI_BL_DATE = Convert.ToDateTime(blAwbDate);
                                            GDInfo.EFGDI_PORT_OF_SHIPMENT = portOfShipment;
                                            GDInfo.EFGDI_NET_WEIGHT = netWeight;
                                            GDInfo.EFGDI_SHIPPING_LINE = "";
                                            GDInfo.EFGDI_BERTHING_TERMINAL = "";
                                            GDInfo.EFGDI_ENTRY_DATETIME = DateTime.Now;
                                            GDInfo.EFGDI_MAKER_ID = "PSW";
                                            cont.Entry(GDInfo).State = System.Data.Entity.EntityState.Modified;
                                        }
                                        cont.SaveChanges();

                                        // GD PACKAGE INFORMATION
                                        List<PSW_GD_EXPORT_PACKAGE_INFORMATION> DeletePrevious = cont.PSW_GD_EXPORT_PACKAGE_INFORMATION.Where(w => w.EPI_GD_NO == GD_NO).ToList();
                                        foreach (PSW_GD_EXPORT_PACKAGE_INFORMATION row in DeletePrevious)
                                        {
                                            cont.PSW_GD_EXPORT_PACKAGE_INFORMATION.Remove(row);
                                        }
                                        cont.SaveChanges();
                                        int CurrentRowPackage = 0;
                                        foreach (dynamic j in i.generalInformation.packagesInformation)
                                        {
                                            PSW_GD_EXPORT_PACKAGE_INFORMATION Packages = new PSW_GD_EXPORT_PACKAGE_INFORMATION();
                                            int MAXID = Convert.ToInt32(cont.PSW_GD_EXPORT_PACKAGE_INFORMATION.Max(m => (decimal?)m.EPI_ID)) + 1;
                                            if (CurrentRowPackage == 0)
                                            {
                                                Packages.EPI_ID = MAXID;
                                            }
                                            else
                                            {
                                                Packages.EPI_ID = (MAXID + CurrentRowPackage);
                                            }
                                            var IPI_NO_OF_PACKAGE = j.numberOfPackages;
                                            Packages.EPI_NO_OF_PACKAGE = IPI_NO_OF_PACKAGE;
                                            var IPI_PACKAGE_TYPE = j.packageType;
                                            Packages.EPI_PACKAGE_TYPE = IPI_PACKAGE_TYPE;
                                            Packages.EPI_GD_NO = GD_NO;
                                            Packages.EPI_ENTRY_DATETIME = DateTime.Now;
                                            Packages.EPI_MAKER_ID = "PSW";
                                            cont.PSW_GD_EXPORT_PACKAGE_INFORMATION.Add(Packages);
                                            CurrentRowPackage += 1;
                                        }
                                        cont.SaveChanges();
                                        // GD PACKAGE INFORMATION


                                        // GD CONTAINER INFORMATION
                                        List<PSW_GD_EXPORT_CONTAINER_INFORMATION> DeletePreviousContainers = cont.PSW_GD_EXPORT_CONTAINER_INFORMATION.Where(w => w.CIE_GD_NO == GD_NO).ToList();
                                        foreach (PSW_GD_EXPORT_CONTAINER_INFORMATION row in DeletePreviousContainers)
                                        {
                                            cont.PSW_GD_EXPORT_CONTAINER_INFORMATION.Remove(row);
                                        }
                                        cont.SaveChanges();
                                        int CurrentRowContainer = 0;
                                        foreach (dynamic j in i.generalInformation.containerVehicleInformation)
                                        {
                                            PSW_GD_EXPORT_CONTAINER_INFORMATION Containers = new PSW_GD_EXPORT_CONTAINER_INFORMATION();
                                            int MAXID = Convert.ToInt32(cont.PSW_GD_EXPORT_CONTAINER_INFORMATION.Max(m => (decimal?)m.CIE_ID)) + 1;
                                            if (CurrentRowContainer == 0)
                                            {
                                                Containers.CIE_ID = MAXID;
                                            }
                                            else
                                            {
                                                Containers.CIE_ID = (MAXID + CurrentRowContainer);
                                            }
                                            var CII_CONTAINER_OR_TRUCK_NO = j.containerOrTruckNumber;
                                            Containers.CIE_CONTAINER_OR_TRUCK_NO = CII_CONTAINER_OR_TRUCK_NO;
                                            var CII_SERIAL_NO = j.sealNumber;
                                            Containers.CIE_SERIAL_NO = CII_SERIAL_NO;
                                            var CII_CONTAINER_TYPE = j.containerType;
                                            Containers.CIE_CONTAINER_TYPE = CII_CONTAINER_TYPE;
                                            Containers.CIE_GD_NO = GD_NO;
                                            Containers.CIE_ENTRY_DATETIME = DateTime.Now;
                                            Containers.CIE_MAKER_ID = "PSW";
                                            cont.PSW_GD_EXPORT_CONTAINER_INFORMATION.Add(Containers);
                                            CurrentRowContainer += 1;
                                        }
                                        cont.SaveChanges();
                                        // GD CONTAINER INFORMATION

                                        // GD HS CODE INFORMATION
                                        List<PSW_GD_EXPORT_HS_CODE> DeletePreviousItems = cont.PSW_GD_EXPORT_HS_CODE.Where(w => w.EHC_GD_NO == GD_NO).ToList();
                                        foreach (PSW_GD_EXPORT_HS_CODE row in DeletePreviousItems)
                                        {
                                            cont.PSW_GD_EXPORT_HS_CODE.Remove(row);
                                        }
                                        cont.SaveChanges();
                                        int CurrentRowHSCODES = 0;
                                        foreach (dynamic j in i.itemInformation)
                                        {
                                            PSW_GD_EXPORT_HS_CODE Items = new PSW_GD_EXPORT_HS_CODE();
                                            int MAXID = Convert.ToInt32(cont.PSW_GD_EXPORT_HS_CODE.Max(m => (decimal?)m.EHC_ID)) + 1;
                                            if (CurrentRowHSCODES == 0)
                                            {
                                                Items.EHC_ID = MAXID;
                                            }
                                            else
                                            {
                                                Items.EHC_ID = (MAXID + CurrentRowHSCODES);
                                            }
                                            var IHC_HS_CODE = j.hsCode;
                                            Items.EHC_HS_CODE = Convert.ToDecimal(IHC_HS_CODE);
                                            var IHC_QUANTITY = j.quantity;
                                            Items.EHC_QUANTITY = Convert.ToDecimal(IHC_QUANTITY);
                                            var IHC_UNIT_PRICE = j.unitPrice;
                                            Items.EHC_UNIT_PRICE = Convert.ToDecimal(IHC_UNIT_PRICE);
                                            var IHC_TOTAL_VALUE = j.totalValue;
                                            Items.EHC_TOTAL_VALUE = Convert.ToDecimal(IHC_TOTAL_VALUE);
                                            var IHC_IMPORTER_VALUE = j.importValue;
                                            Items.EHC_EXPORTER_VALUE = Convert.ToDecimal(IHC_IMPORTER_VALUE);
                                            var IHC_UOM = j.uom;
                                            Items.EHC_UOM = IHC_UOM;
                                            Items.EHC_GD_NO = GD_NO;
                                            Items.EHC_ENTRY_DATETIME = DateTime.Now;
                                            Items.EHC_MAKER_ID = "PSW";
                                            cont.PSW_GD_EXPORT_HS_CODE.Add(Items);
                                            CurrentRowHSCODES += 1;
                                            cont.SaveChanges();
                                        }
                                        // GD HS CODE INFORMATION

                                    }
                                    cont.SaveChanges();
                                    // GD INFORMATION BLOCK

                                    // BCA Information Block
                                    CurrentRow = 0;
                                    foreach (dynamic i in RequestData.data.bankAdviceInfo)
                                    {
                                        string bcaUniqueIdNumber = i.bcaUniqueIdNumber;
                                        string bdaDocumentRefNumber = i.bdaDocumentRefNumber;
                                        PSW_BCA_INFO BCAInfo = cont.PSW_BCA_INFO.Where(m => m.BCAD_FORM_E_NO == finInsUniqueNumber && m.BCAD_BCA_NO == bcaUniqueIdNumber).FirstOrDefault();
                                        string currency = i.netAmountRealized.currency;
                                        PSW_CURRENCY BCAD_CURRENCY_ID = cont.PSW_CURRENCY.Where(m => m.CUR_NAME == currency).FirstOrDefault();
                                        string bcaEventName = i.bcaInformation.bcaEventName;
                                        PSW_BCA_EVENT EventDetails = cont.PSW_BCA_EVENT.Where(m => m.BCAE_NAME == bcaEventName).FirstOrDefault();
                                        if (BCAInfo == null)
                                        {
                                            BCAInfo = new PSW_BCA_INFO();
                                            int MAXID = Convert.ToInt32(cont.PSW_BCA_INFO.Max(m => (decimal?)m.BCAD_ID)) + 1;
                                            if (CurrentRow == 0)
                                            {
                                                BCAInfo.BCAD_ID = MAXID;
                                            }
                                            else
                                            {
                                                BCAInfo.BCAD_ID = (MAXID + CurrentRow);
                                            }
                                            BCAInfo.BCAD_ENTRY_DATETIME = DateTime.Now;
                                            BCAInfo.BCAD_FORM_E_NO = finInsUniqueNumber;
                                            BCAInfo.BCAD_BCA_NO = bcaUniqueIdNumber;
                                            BCAInfo.BCAD_RUNNING_SERIAL_DETAIL = i.bcaInformation.runningSerialNumber;
                                            BCAInfo.BCAD_SWIFT_REFERENCES = i.bcaInformation.swiftReference;
                                            BCAInfo.BCAD_BILL_NO = i.bcaInformation.billNumber;
                                            if (i.bcaInformation.billDated != null)
                                            {
                                                string billDated = DateTime.ParseExact(
                                               Convert.ToString(i.bcaInformation.billDated)
                                                   , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                                BCAInfo.BCAD_BILL_DATE = Convert.ToDateTime(billDated);
                                            }
                                            BCAInfo.BCAD_BILL_AMOUNT = i.bcaInformation.billAmount;
                                            BCAInfo.BCAD_INVOICE_NO = i.bcaInformation.invoiceNumber;
                                            if (i.bcaInformation.invoiceDate != null)
                                            {
                                                string invoiceDate = DateTime.ParseExact(
                                               Convert.ToString(i.bcaInformation.invoiceDate)
                                                   , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                                BCAInfo.BCAD_INVOICE_DATE = Convert.ToDateTime(invoiceDate);
                                            }
                                            BCAInfo.BCAD_INVOICE_AMOUNT = i.bcaInformation.invoiceAmount;
                                            BCAInfo.BCAD_FORIEGN_BANK_CHARGES_FCY = i.deductions.foreignBankChargesFcy;
                                            BCAInfo.BCAD_AGENT_BROKE_COMMISSION_FCY = i.deductions.agentCommissionFcy;
                                            BCAInfo.BCAD_WITH_HOLDING_TAX_PKR = i.deductions.withholdingTaxPkr;
                                            BCAInfo.BCAD_EDS_PKR = i.deductions.edsPkr;
                                            BCAInfo.BCAD_BCA_FC = i.netAmountRealized.bcaFc;
                                            BCAInfo.BCAD_FCY_EXCHANGE_RATE = i.netAmountRealized.fcyExchangeRate;
                                            BCAInfo.BCAD_BCA_PKR = i.netAmountRealized.bcaPkr;
                                            if (i.netAmountRealized.dateOfRealized != null)
                                            {
                                                string dateOfRealized = DateTime.ParseExact(
                                             Convert.ToString(i.netAmountRealized.dateOfRealized)
                                                 , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                                BCAInfo.BCAD_DATE_OF_REALIZED = Convert.ToDateTime(dateOfRealized);
                                            }
                                            BCAInfo.BCAD_AFJUSTMENT_FROM_SPECIAL_FCY_AMOUNT = i.netAmountRealized.adjustFromSpecialFcyAcc;
                                            BCAInfo.BCAD_CURR_OF_REALIZATION = false;
                                            BCAInfo.BCAD_CURRENCY = BCAD_CURRENCY_ID.CUR_ID;
                                            BCAInfo.BCAD_FULL_AMOUNT_IS_NOT_REALIZED = false;
                                            BCAInfo.BCAD_FORM_AMOUNT_REALIZED = i.netAmountRealized.amountRealized;
                                            BCAInfo.BCAD_BALANCE = i.netAmountRealized.balance;
                                            BCAInfo.BCAD_ALLOWED_DISCOUNT = i.netAmountRealized.allowedDiscount;
                                            BCAInfo.BCAD_ALLOWED_DISCOUNT_PERCENT = Convert.ToInt32(i.netAmountRealized.allowedDiscountPercentage);
                                            BCAInfo.BCAD_CERTIFIED = false;
                                            BCAInfo.BCAD_REMARKS = i.remarks;
                                            BCAInfo.BCAD_MAKER_ID = "PSW";
                                            BCAInfo.BCAD_CHECKER_ID = null;
                                            BCAInfo.BCAD_STATUS = "APPROVED";
                                            BCAInfo.BCAD_GD_NO = i.gdNumber;
                                            if (i.bcaInformation.eventDate != null)
                                            {
                                                string eventDate = DateTime.ParseExact(Convert.ToString(i.bcaInformation.eventDate)
                                                , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                                BCAInfo.BCAD_EVENT_DATE = Convert.ToDateTime(eventDate);
                                            }
                                            if (EventDetails != null)
                                                BCAInfo.BCAD_EVENTID = EventDetails.BCAE_ID;
                                            else
                                                BCAInfo.BCAD_EVENTID = 1;
                                            BCAInfo.BCAD_EVENT_Remarks = "";
                                            cont.PSW_BCA_INFO.Add(BCAInfo);
                                        }
                                        else
                                        {
                                            BCAInfo.BCAD_ENTRY_DATETIME = DateTime.Now;
                                            BCAInfo.BCAD_FORM_E_NO = finInsUniqueNumber;
                                            BCAInfo.BCAD_BCA_NO = bcaUniqueIdNumber;
                                            BCAInfo.BCAD_RUNNING_SERIAL_DETAIL = i.bcaInformation.runningSerialNumber;
                                            BCAInfo.BCAD_SWIFT_REFERENCES = i.bcaInformation.swiftReference;
                                            BCAInfo.BCAD_BILL_NO = i.bcaInformation.billNumber;
                                            if (i.bcaInformation.billDated != null)
                                            {
                                                string billDated = DateTime.ParseExact(
                                               Convert.ToString(i.bcaInformation.billDated)
                                                   , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                                BCAInfo.BCAD_BILL_DATE = Convert.ToDateTime(billDated);
                                            }
                                            BCAInfo.BCAD_BILL_AMOUNT = i.bcaInformation.billAmount;
                                            BCAInfo.BCAD_INVOICE_NO = i.bcaInformation.invoiceNumber;
                                            if (i.bcaInformation.invoiceDate != null)
                                            {
                                                string invoiceDate = DateTime.ParseExact(
                                               Convert.ToString(i.bcaInformation.invoiceDate)
                                                   , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                                BCAInfo.BCAD_INVOICE_DATE = Convert.ToDateTime(invoiceDate);
                                            }
                                            BCAInfo.BCAD_INVOICE_AMOUNT = i.bcaInformation.invoiceAmount;
                                            BCAInfo.BCAD_FORIEGN_BANK_CHARGES_FCY = i.deductions.foreignBankChargesFcy;
                                            BCAInfo.BCAD_AGENT_BROKE_COMMISSION_FCY = i.deductions.agentCommissionFcy;
                                            BCAInfo.BCAD_WITH_HOLDING_TAX_PKR = i.deductions.withholdingTaxPkrdbtable;
                                            BCAInfo.BCAD_EDS_PKR = i.deductions.edsPkr;
                                            BCAInfo.BCAD_BCA_FC = i.netAmountRealized.bcaFc;
                                            BCAInfo.BCAD_FCY_EXCHANGE_RATE = i.netAmountRealized.fcyExchangeRate;
                                            BCAInfo.BCAD_BCA_PKR = i.netAmountRealized.bcaPkr;
                                            if (i.netAmountRealized.dateOfRealized != null)
                                            {
                                                string dateOfRealized = DateTime.ParseExact(
                                             Convert.ToString(i.netAmountRealized.dateOfRealized)
                                                 , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                                BCAInfo.BCAD_DATE_OF_REALIZED = Convert.ToDateTime(dateOfRealized);
                                            }
                                            BCAInfo.BCAD_AFJUSTMENT_FROM_SPECIAL_FCY_AMOUNT = i.netAmountRealized.adjustFromSpecialFcyAcc;
                                            BCAInfo.BCAD_CURR_OF_REALIZATION = false;
                                            BCAInfo.BCAD_CURRENCY = BCAD_CURRENCY_ID.CUR_ID;
                                            BCAInfo.BCAD_FULL_AMOUNT_IS_NOT_REALIZED = false;
                                            BCAInfo.BCAD_FORM_AMOUNT_REALIZED = i.netAmountRealized.amountRealized;
                                            BCAInfo.BCAD_BALANCE = i.netAmountRealized.balance;
                                            BCAInfo.BCAD_ALLOWED_DISCOUNT = i.netAmountRealized.allowedDiscount;
                                            BCAInfo.BCAD_ALLOWED_DISCOUNT_PERCENT = Convert.ToInt32(i.netAmountRealized.allowedDiscountPercentage);
                                            BCAInfo.BCAD_CERTIFIED = true;
                                            BCAInfo.BCAD_REMARKS = i.remarks;
                                            BCAInfo.BCAD_MAKER_ID = "PSW";
                                            BCAInfo.BCAD_CHECKER_ID = null;
                                            BCAInfo.BCAD_STATUS = "APPROVED";
                                            if (i.bcaInformation.eventDate != null)
                                            {
                                                string eventDate = DateTime.ParseExact(Convert.ToString(i.bcaInformation.eventDate)
                                                , "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                                BCAInfo.BCAD_EVENT_DATE = Convert.ToDateTime(eventDate);
                                            }
                                            BCAInfo.BCAD_EVENT_Remarks = "";
                                            BCAInfo.BCAD_GD_NO = i.gdNumber;
                                            cont.Entry(BCAInfo).State = System.Data.Entity.EntityState.Modified;
                                        }
                                        CurrentRow += 1;
                                    }
                                    // BCA Information Block


                                    PSW_CHANGE_OF_BANK_REQUEST COBReq = new PSW_CHANGE_OF_BANK_REQUEST();
                                    COBReq.COB_ID = Convert.ToInt32(cont.PSW_CHANGE_OF_BANK_REQUEST.Max(m => (decimal?)m.COB_ID)) + 1;
                                    COBReq.COB_UNIQUE_NO = cobUniqueIdNumber;
                                    COBReq.COB_TRANS_TYPE = "02";
                                    COBReq.COB_IBAN = exporterIban;
                                    COBReq.COB_REQUEST_NO = finInsUniqueNumber;
                                    COBReq.COB_STATUS = null;
                                    COBReq.COB_DATA_ENTRY_DATETIME = DateTime.Now;
                                    COBReq.METHODID = 1539;
                                    cont.PSW_CHANGE_OF_BANK_REQUEST.Add(COBReq);
                                    cont.SaveChanges();

                                    transaction.Commit();

                                    // Success Response 
                                    ShareGDNFinancialInfoWithOtherADResponse data = new ShareGDNFinancialInfoWithOtherADResponse();
                                    data.messageId = Guid.NewGuid().ToString();
                                    data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                                    data.senderId = "CBN";
                                    data.receiverId = "PSW";
                                    data.processingCode = "307";
                                    data.data = new ShareGDNFinancialInfoWithOtherADResponse.DetailData { };
                                    data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                                    data.message = new ShareGDNFinancialInfoWithOtherADResponse.MessageData
                                    {
                                        code = "200",
                                        description = "Change of bank request with GD and financial information received."
                                    };
                                    ConvertMessageToEntity(data);
                                    return Request.CreateResponse(HttpStatusCode.OK, data);
                                    // Success Response 

                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Exporter Exist in the system with the provided IBAN : " + exporterIban + ", NTN : " + exporterNtn + "."));
                                }

                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Exception Occurred While Saving Data : " + ex.Message));
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Invalid tradeTranType provided. "));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message));
            }
        }
        #endregion

        #region ProcCode 308
        HttpResponseMessage ShareCOBAprovalRejection(dynamic RequestData)
        {
            try
            {
                string cobUniqueIdNumber = RequestData.data.cobUniqueIdNumber;
                string tradeTranType = RequestData.data.tradeTranType;
                string iban = RequestData.data.iban;
                string traderNTN = RequestData.data.traderNTN;
                string traderName = RequestData.data.traderName;
                string cobStatus = RequestData.data.cobStatus;
                string finInsUniqueNumber = RequestData.data.finInsUniqueNumber;

                PSW_CLIENT_MASTER_DETAIL client = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == iban && m.C_NTN_NO == traderNTN).FirstOrDefault();
                if (client != null)
                {
                    if (tradeTranType == "01")
                    {
                        PSW_EIF_BASIC_INFO eifbasic = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == finInsUniqueNumber).FirstOrDefault();
                        if (eifbasic != null)
                        {
                            PSW_CHANGE_OF_BANK_REQUEST CobRequest = context.PSW_CHANGE_OF_BANK_REQUEST.Where(m => m.COB_UNIQUE_NO == cobUniqueIdNumber).FirstOrDefault();
                            if (CobRequest != null)
                            {
                                CobRequest.COB_TRANS_TYPE = tradeTranType;
                                CobRequest.COB_IBAN = iban;
                                CobRequest.COB_REQUEST_NO = finInsUniqueNumber;
                                CobRequest.COB_STATUS = cobStatus;
                                context.Entry(CobRequest).State = System.Data.Entity.EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    ShareChangeOfBankRequestWithADReponse data = new ShareChangeOfBankRequestWithADReponse();
                                    data.messageId = Guid.NewGuid().ToString();
                                    data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                                    data.senderId = "CBN";
                                    data.receiverId = "PSW";
                                    data.processingCode = "308";
                                    data.data = new ShareChangeOfBankRequestWithADReponse.DetailData { };
                                    data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                                    data.message = new ShareChangeOfBankRequestWithADReponse.MessageData
                                    {
                                        code = "200",
                                        description = "Change of bank request status acknowledged."
                                    };
                                    ConvertMessageToEntity(data);
                                    return Request.CreateResponse(HttpStatusCode.OK, data);
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured while saving the change of bank request."));
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Such COB request exist in the system with unique number :" + cobUniqueIdNumber));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No EIF Exist in the system with the provided ref # " + finInsUniqueNumber + " ."));
                        }
                    }
                    else if (tradeTranType == "02")
                    {
                        PSW_FORM_E_DOCUMENT_INFO eefdoc = context.PSW_FORM_E_DOCUMENT_INFO.Where(m => m.FDI_FORM_E_NO == finInsUniqueNumber).FirstOrDefault();
                        if (eefdoc != null)
                        {
                            PSW_CHANGE_OF_BANK_REQUEST CobRequest = context.PSW_CHANGE_OF_BANK_REQUEST.Where(m => m.COB_UNIQUE_NO == cobUniqueIdNumber).FirstOrDefault();
                            if (CobRequest != null)
                            {
                                CobRequest.COB_TRANS_TYPE = tradeTranType;
                                CobRequest.COB_IBAN = iban;
                                CobRequest.COB_REQUEST_NO = finInsUniqueNumber;
                                CobRequest.COB_STATUS = cobStatus;
                                context.Entry(CobRequest).State = System.Data.Entity.EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    ShareChangeOfBankRequestWithADReponse data = new ShareChangeOfBankRequestWithADReponse();
                                    data.messageId = Guid.NewGuid().ToString();
                                    data.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                                    data.senderId = "CBN";
                                    data.receiverId = "PSW";
                                    data.processingCode = "308";
                                    data.data = new ShareChangeOfBankRequestWithADReponse.DetailData { };
                                    data.signature = DAL.GetSignature(System.Text.Json.JsonSerializer.Serialize(data));
                                    data.message = new ShareChangeOfBankRequestWithADReponse.MessageData
                                    {
                                        code = "200",
                                        description = "Change of bank request status acknowledged."
                                    };
                                    ConvertMessageToEntity(data);
                                    return Request.CreateResponse(HttpStatusCode.OK, data);
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Error Occured while saving the change of bank request."));
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No Such COB request exist in the system with unique number :" + cobUniqueIdNumber));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "No E Form Exist in the system with the provided ref # " + finInsUniqueNumber + " ."));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Invalid tradeTranType provided."));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CreateResposeWithCode("208", "Sharing Data Failed, No Client Exist in the system with provided iban : " + iban + " , NTN : " + traderNTN + " ."));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, CreateResposeWithCode("500", ex.Message));
            }
        }
        #endregion

    }
}
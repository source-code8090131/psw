﻿#define productoin
//#define dev
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using PSWWebApi.Models;

namespace PSWWebApi.Controllers
{
    public class ediSendController : ApiController
    {
        [HttpGet]
        [Route("api/dealers/citi/ediSend/CheckStatus")]
        public string CheckStatus()
        {
            return "Sending Controller Is Running";
        }

        [HttpPost]
        [Route("api/dealers/citi/ediSend/")]
        // POST api/<controller>
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            try
            {
#if productoin

                string ForwardDomain = System.Configuration.ConfigurationManager.AppSettings["PSWApiDomain"].ToString();
                string redirectUri = ForwardDomain + "/api/dealers/a/d/i/edi";
                HttpRequestMessage clone = new HttpRequestMessage(request.Method, redirectUri);
                if (request.Method != HttpMethod.Get)
                {
                    clone.Content = request.Content;
                }
                clone.Version = request.Version;

                foreach (KeyValuePair<string, object> prop in request.Properties)
                {
                    clone.Properties.Add(prop);
                }
                foreach (KeyValuePair<string, IEnumerable<string>> header in request.Headers)
                {
                    clone.Headers.TryAddWithoutValidation(header.Key, header.Value);
                }
                clone.Headers.Host = new Uri(redirectUri).Authority;
                HttpRequestMessage forwardRequest = clone;
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                    ServicePointManager.Expect100Continue = false;
                    HttpClient client = new HttpClient(clientHandler);
                    client.DefaultRequestHeaders.ExpectContinue = false;
                    Task<HttpResponseMessage> response = client.SendAsync(forwardRequest);
                    Task.WaitAll(new Task[] { response });
                    HttpResponseMessage result = response.Result;
                    return result;
                }

#endif

#if dev
                return CreateCustomTestResponse("200","Test Success");
#endif

            }
            catch (Exception ex)
            {
                string message = "Exception Occurred on the server ediSend - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                }
                if (ex.InnerException.InnerException != null)
                {
                    message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                }
                return CreateCustomTestResponse("500", message);
            }
        }

        [HttpPost]
        [Route("api/dealers/citi/ediSend/token/")]
        // POST api/<controller>
        public HttpResponseMessage token(HttpRequestMessage request)
        {
            try
            {
                string ForwardDomain = System.Configuration.ConfigurationManager.AppSettings["PSWApiDomain"].ToString();
                string redirectUri = ForwardDomain + "/auth/connect/token";
                HttpRequestMessage clone = new HttpRequestMessage(request.Method, redirectUri);
                if (request.Method != HttpMethod.Get)
                {
                    clone.Content = request.Content;
                }
                clone.Version = request.Version;

                foreach (KeyValuePair<string, object> prop in request.Properties)
                {
                    clone.Properties.Add(prop);
                }
                foreach (KeyValuePair<string, IEnumerable<string>> header in request.Headers)
                {
                    clone.Headers.TryAddWithoutValidation(header.Key, header.Value);
                }
                clone.Headers.Host = new Uri(redirectUri).Authority;
                HttpRequestMessage forwardRequest = clone;
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    HttpClient client = new HttpClient(clientHandler);
                    ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                    ServicePointManager.Expect100Continue = false;
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                    client.DefaultRequestHeaders.ExpectContinue = false;
                    Task<HttpResponseMessage> response = client.SendAsync(forwardRequest);
                    Task.WaitAll(new Task[] { response });
                    HttpResponseMessage result = response.Result;
                    return result;
                }
            }
            catch (Exception ex)
            {
                string message = "Exception Occurred on the server ediSend - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                }
                if (ex.InnerException.InnerException != null)
                {
                    message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                }
                return CreateCustomTestResponse("500", message);
            }
        }


        public HttpResponseMessage CreateCustomTestResponse(string code, string desc)
        {
            TestCustomResponseFromPSW data = new TestCustomResponseFromPSW();
            data.message = new TestCustomResponseFromPSW.DetailMessage
            {
                code = code,
                description = desc
            };
           return Request.CreateResponse(HttpStatusCode.OK, data);
        }


        ResonseMessageFromSBP.MessageData CreateResposeWithCode(string Code, string descr = "")
        {
            ResonseMessageFromSBP.MessageData Message = new ResonseMessageFromSBP.MessageData();
            Message.code = Code;
            Message.description = descr;
            return Message;
        }













    }
}
﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.Json;
using System.Web;

namespace PSWWebApi.Models
{
    public class DAL
    {

        public static void WriteLogs(string Text)
        {
            string message = "";
            try
            {
                string complete_path = System.Web.HttpRuntime.AppDomainAppPath + "\\Logs\\";
                string filename = "RequestLogsForSignature.txt";
                string path = complete_path + filename;
                if (!System.IO.Directory.Exists(complete_path))
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                }
                using (System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                {
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                    writer.WriteLine(Text + Environment.NewLine);
                    writer.Close();
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                message = "Exception Occured While Writing Logs " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
            }
            finally
            {
                if (message != "")
                {
                    string complete_path = System.Web.HttpRuntime.AppDomainAppPath + "\\Logs\\";
                    string filename = "RequestLogsForSignature.txt";
                    string path = complete_path + filename;
                    using (System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                    {
                        System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                        writer.WriteLine(message);
                        writer.Close();
                        fs.Close();
                    }
                }
            }
        }
        public static string GetSignature(string data)
        {
            var options = new JsonSerializerOptions()
            {
                WriteIndented = false
            };
            options.Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping;
            string JsonStr = System.Text.Json.JsonSerializer.Serialize(data, options);
            var doc = System.Text.Json.JsonSerializer.Deserialize<object>(JsonStr);
            string JsonStr1 = System.Text.Json.JsonSerializer.Serialize(doc);
            JsonDocument JsonDoc1 = JsonDocument.Parse(doc.ToString());
            JsonElement RootDoc = JsonDoc1.RootElement;
            JsonElement DataDoc = RootDoc.GetProperty("data");
            string signature = Sign(DataDoc);
            return signature;
        }
        public static string Sign(JsonElement data)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
            {
                string ABC = data.ToString();
                return System.Convert.ToBase64String(algorithm.ComputeHash(System.Text.UTF8Encoding.UTF8.GetBytes(data.ToString())));
            }
        }

        public static bool IsPropertyExist(dynamic settings, string name)
        {
            if (settings is ExpandoObject)
                return ((IDictionary<string, object>)settings).ContainsKey(name);

            return settings.GetType().GetProperty(name) != null;
        }

    }
}
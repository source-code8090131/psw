﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace PSWWebApi.Models
{
    public class EMAILING
    {
        #region if Production 
        public static bool Application_Mode = true;
        #endregion

        //#region if Developer 
        //public static bool Application_Mode = false;
        //#endregion


        //Emailing Starts

        #region Import Emails
        public static string EIF_Approve(string EIF_REQ_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                    if (CheckEIF != null)
                    {
                        int ClientID = Convert.ToInt32(CheckEIF.EIF_BI_BUSSINESS_NAME);
                        PSW_CLIENT_MASTER_DETAIL GetClientInfo = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null)
                        {
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                    mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Testuser0592";
                                    mail.From = new MailAddress(FromEmail);
                                    mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                    mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been approved at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message;
            }
            return (message);
        }

        public static string EIF_Edit(string EIF_REQ_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                    if (CheckEIF != null)
                    {
                        int ClientID = Convert.ToInt32(CheckEIF.EIF_BI_BUSSINESS_NAME);
                        PSW_CLIENT_MASTER_DETAIL GetClientInfo = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null)
                        {
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                    mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been amended at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Testuser0592";
                                    mail.From = new MailAddress(FromEmail);
                                    mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                    mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been amended at our end as per your request.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message;
            }
            return (message);
        }

        public static string EIF_Cancel(string EIF_REQ_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                    if (CheckEIF != null)
                    {
                        int ClientID = Convert.ToInt32(CheckEIF.EIF_BI_BUSSINESS_NAME);
                        PSW_CLIENT_MASTER_DETAIL GetClientInfo = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null)
                        {
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                    mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been cancelled.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Testuser0592";
                                    mail.From = new MailAddress(FromEmail);
                                    mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                    mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note the requested EIF <span style='font-family:Arial;font-size:13px'>" + EIF_REQ_NO + "</span>  has been cancelled.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message;
            }
            return (message);
        }

        public static string EIF_HsCode(string EIFHsCode, string EIF_REQ_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_HS_CODE GetHsCode = context.PSW_EIF_HS_CODE.Where(m => m.EIF_HC_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                    if (GetHsCode != null)
                    {
                        PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                        if (CheckEIF != null)
                        {
                            int ClientID = Convert.ToInt32(CheckEIF.EIF_BI_BUSSINESS_NAME);
                            PSW_CLIENT_MASTER_DETAIL GetClientInfo = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_ID == ClientID).FirstOrDefault();
                            if (GetClientInfo != null)
                            {
                                if (Application_Mode == true)
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        mail.From = new MailAddress(FromEmail);
                                        mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.To.Add(MultiEmail);
                                        }
                                        mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note you have selected the HS Code <span style='font-family:Arial;font-size:13px'>" + EIFHsCode + "</span>  Fall under SBP cash margin held requirement and will be processed accordingly.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                                else
                                {
                                    using (MailMessage mail = new MailMessage())
                                    {
                                        string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                        string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                        string HostName = EmailConfig.EC_SEREVER_HOST;
                                        int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                        NetworkCredentials.UserName = FromEmail;
                                        //////#if dev
                                        NetworkCredentials.Password = "Testuser0592";
                                        mail.From = new MailAddress(FromEmail);
                                        mail.To.Add(new MailAddress(ToEmail));
                                        string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                        foreach (string MultiEmail in CC_Email)
                                        {
                                            mail.To.Add(MultiEmail);
                                        }
                                        mail.Subject = "EIF " + EIF_REQ_NO + " Status";
                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>Please note you have selected the HS Code <span style='font-family:Arial;font-size:13px'>" + EIFHsCode + "</span>  Fall under SBP cash margin held requirement and will be processed accordingly.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                        mail.IsBodyHtml = true;

                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                        {
                                            smtp.Credentials = NetworkCredentials;
                                            //if dev
                                            smtp.EnableSsl = true;
                                            smtp.Send(mail);
                                        }
                                        message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                    }
                                }
                            }
                            else
                            {
                                message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                            }
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Hs Code For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message;
            }
            return (message);
        }

        public static string EIF_GD(string EIF_REQ_NO)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_EIF_BASIC_INFO CheckEIF = context.PSW_EIF_BASIC_INFO.Where(m => m.EIF_BI_EIF_REQUEST_NO == EIF_REQ_NO).FirstOrDefault();
                    if (CheckEIF != null)
                    {
                        int ClientID = Convert.ToInt32(CheckEIF.EIF_BI_BUSSINESS_NAME);
                        PSW_CLIENT_MASTER_DETAIL GetClientInfo = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_ID == ClientID).FirstOrDefault();
                        if (GetClientInfo != null)
                        {
                            if (Application_Mode == true)
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    mail.From = new MailAddress(FromEmail);
                                    mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                    mail.Subject = "GD UPDATE";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>This is to update that you have submitted Goods Declaration in Pakistan Single window.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                            else
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                    string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                    string HostName = EmailConfig.EC_SEREVER_HOST;
                                    int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                    NetworkCredentials.UserName = FromEmail;
                                    //////#if dev
                                    NetworkCredentials.Password = "Testuser0592";
                                    mail.From = new MailAddress(FromEmail);
                                    mail.To.Add(new MailAddress(ToEmail));
                                    string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                    mail.Subject = "GD UPDATE";
                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>This is to update that you have submitted Goods Declaration in Pakistan Single window.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                    mail.IsBodyHtml = true;

                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                    {
                                        smtp.Credentials = NetworkCredentials;
                                        //if dev
                                        smtp.EnableSsl = true;
                                        smtp.Send(mail);
                                    }
                                    message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            message += "Exception : Problem while Fetching Client Detail For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For EIF Req No : " + EIF_REQ_NO + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message;
            }
            return (message);
        }
        #endregion

        #region Export Emails
        public static string EEF_GD(string EXPORTER_IBAN)
        {
            string message = "";
            try
            {
                PSWEntities context = new PSWEntities();
                PSW_EMAIL_CONFIGRATION EmailConfig = context.PSW_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (EmailConfig != null)
                {
                    PSW_CLIENT_MASTER_DETAIL GetClientInfo = context.PSW_CLIENT_MASTER_DETAIL.Where(m => m.C_IBAN_NO == EXPORTER_IBAN).FirstOrDefault();
                    if (GetClientInfo != null)
                    {
                        if (Application_Mode == true)
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                string HostName = EmailConfig.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                mail.From = new MailAddress(FromEmail);
                                mail.To.Add(new MailAddress(ToEmail));
                                string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                foreach (string MultiEmail in CC_Email)
                                {
                                    mail.To.Add(MultiEmail);
                                }
                                mail.Subject = "GD UPDATE";
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>This is to update that you have submitted Goods Declaration in Pakistan Single window.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.Send(mail);
                                }
                                message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                            }
                        }
                        else
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string ToEmail = GetClientInfo.C_EMAIL_ADDRESS;
                                string FromEmail = EmailConfig.EC_CREDENTIAL_ID;
                                string HostName = EmailConfig.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(EmailConfig.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                //////#if dev
                                NetworkCredentials.Password = "Testuser0592";
                                mail.From = new MailAddress(FromEmail);
                                mail.To.Add(new MailAddress(ToEmail));
                                string[] CC_Email = EmailConfig.EC_CC_EMAIL.Split(',');
                                foreach (string MultiEmail in CC_Email)
                                {
                                    mail.To.Add(MultiEmail);
                                }
                                mail.Subject = "GD UPDATE";
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:15px'>Dear Valued Customer <b> " + GetClientInfo.C_NAME + " </b>,</p><p style='font-family:Candara;font-size:15px'>This is to update that you have submitted Goods Declaration in Pakistan Single window.<br />  </p><p style='font-family:Candara;font-size:15px'>Please direct any inquiries, quoting our reference number, to your designated Trade Service Professional (TSP) team at:<br /> <br /> </p> <p style='font-family:Arial;font-size:13px'>Tel: + 92 21 111 777 777<br />  + 92 21 111 999 999 </p>			<p style='font-family:Candara;font-size:15px'>Email: citi.pk.tsp.team@citi.com <br /> </p><p style='font-family:Candara;font-size:15px'>Regards, <br /> </p><p style='font-family:Candara;font-size:15px'>Citibank N.A. <br /> </p><p style='font-family:Candara;font-size:15px'>Trade Services Unit <br /> </p><p style='font-family:Candara;font-size:15px'>A.W.T plaza I.I. Chundrigar Road, <br /> </p>			<p style='font-family:Candara;font-size:15px'>Karachi, Pakistan P.O. Box 4889 <br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    //if dev
                                    smtp.EnableSsl = true;
                                    smtp.Send(mail);
                                }
                                message += "Email has been sent to the client : " + GetClientInfo.C_NAME + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Client Detail on the given IBAN No : " + EXPORTER_IBAN + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message;
            }
            return (message);
        }
        #endregion
    }
}
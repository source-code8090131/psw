//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSWWebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSW_AUTH_PAY_MODES
    {
        public int APM_ID { get; set; }
        public Nullable<int> APM_CODE { get; set; }
        public string APM_STATUS { get; set; }
        public string APM_DESCRIPTION { get; set; }
        public Nullable<int> APM_TYPE_ID { get; set; }
        public Nullable<System.DateTime> APM_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> APM_EDIT_DATETIME { get; set; }
        public string APM_MAKER_ID { get; set; }
        public string APM_CHECKER_ID { get; set; }
        public string APM_NO_OF_DAYS { get; set; }
        public Nullable<bool> APM_ISAUTH { get; set; }
    }
}

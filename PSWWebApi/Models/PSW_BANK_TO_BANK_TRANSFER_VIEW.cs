//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSWWebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSW_BANK_TO_BANK_TRANSFER_VIEW
    {
        public Nullable<int> BTBT_CITY { get; set; }
        public string LOCT_NAME { get; set; }
        public Nullable<int> BTBT_BANK { get; set; }
        public string LB_BANK_NAME { get; set; }
        public int BTBT_ID { get; set; }
        public string BTBT_BRANCH { get; set; }
        public string BTBT_REMARKS { get; set; }
        public Nullable<int> BTBT_REQUEST_NO { get; set; }
        public string EIF_BI_EIF_REQUEST_NO { get; set; }
    }
}

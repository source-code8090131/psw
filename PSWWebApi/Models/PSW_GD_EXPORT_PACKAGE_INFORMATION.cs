//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSWWebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSW_GD_EXPORT_PACKAGE_INFORMATION
    {
        public int EPI_ID { get; set; }
        public Nullable<decimal> EPI_NO_OF_PACKAGE { get; set; }
        public string EPI_PACKAGE_TYPE { get; set; }
        public Nullable<System.DateTime> EPI_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> EPI_EDIT_DATETIME { get; set; }
        public string EPI_MAKER_ID { get; set; }
        public string EPI_CHECKER_ID { get; set; }
        public string EPI_GD_NO { get; set; }
        public int EPI_AMENDMENT_NO { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSWWebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSW_LIST_OF_CITIES
    {
        public int LOCT_ID { get; set; }
        public string LOCT_NAME { get; set; }
        public Nullable<bool> LOCT_STATUS { get; set; }
        public Nullable<System.DateTime> LOCT_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> LOCT_EDIT_DATETIME { get; set; }
        public string LOCT_MAKER_ID { get; set; }
        public string LOCT_CHECKER_ID { get; set; }
        public string LOCT_CODE { get; set; }
        public Nullable<bool> LOCT_ISAUTH { get; set; }
    }
}

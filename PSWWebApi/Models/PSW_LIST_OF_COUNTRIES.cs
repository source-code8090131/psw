//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSWWebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSW_LIST_OF_COUNTRIES
    {
        public int LC_ID { get; set; }
        public string LC_NAME { get; set; }
        public string LC_CODE { get; set; }
        public Nullable<System.DateTime> LC_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> LC_EDIT_DATETIME { get; set; }
        public string LC_MAKER_ID { get; set; }
        public string LC_CHECKER_ID { get; set; }
        public Nullable<bool> LC_STATUS { get; set; }
        public Nullable<bool> LC_ISAUTH { get; set; }
    }
}

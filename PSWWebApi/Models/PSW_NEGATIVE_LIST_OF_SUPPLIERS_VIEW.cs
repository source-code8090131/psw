//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSWWebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PSW_NEGATIVE_LIST_OF_SUPPLIERS_VIEW
    {
        public Nullable<int> NLOS_LOS_ID { get; set; }
        public string LOS_NAME { get; set; }
        public string LOS_CODE { get; set; }
        public Nullable<int> NLOS_PROFILE_TYPE { get; set; }
        public string P_NAME { get; set; }
        public int NLOS_ID { get; set; }
        public Nullable<System.DateTime> NLOS_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> NLOS_EDIT_DATETIME { get; set; }
        public string NLOS_MAKER_ID { get; set; }
        public string NLOS_CHECKER_ID { get; set; }
        public Nullable<bool> NLOS_STATUS { get; set; }
        public bool NLOS_IS_SHARED { get; set; }
        public Nullable<bool> LOS_ISAUTH { get; set; }
        public string LOS_CHECKER_ID { get; set; }
        public string LOS_MAKER_ID { get; set; }
        public Nullable<bool> NLOS_ISAUTH { get; set; }
    }
}

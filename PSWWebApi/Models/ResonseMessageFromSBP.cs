﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWWebApi.Models
{
    public class ResonseMessageFromSBP
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public DATA data { get; set; }
        public string signature { get; set; }
        public MessageData message { get; set; }
        public class MessageData
        {
            public string code { get; set; }
            public string description { get; set; }

        }
        public class DATA
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWWebApi.Models
{
    public class SHARE_NEGATIVE_LIST_OF_COMMODITIES
    {

        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string processingCode { get; set; }
        public DetailData data { get; set; }
        public string signature { get; set; }
        public MessageData message { get; set; }

        public class DetailData
        {
            public string iban { get; set; }
            public List<string> restrictedCommoditiesForImport { get; set; }
            public List<string> restrictedCommoditiesForExport { get; set; }

        }
        public class MessageData
        {
            public string code { get; set; }
            public string description { get; set; }

        }

    }
}
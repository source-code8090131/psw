﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWWebApi.Models
{
    public class ShareDetailsAndAuthPayModes
    {

        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string processingCode { get; set; }
        public DetailData data { get; set; }
        public string signature { get; set; }
        public MessageData message { get; set; }

        public class DetailData
        {
            public string iban { get; set; }
            public string accountTitle { get; set; }
            public string accountNumber { get; set; }
            public string accountStatus { get; set; }
            public string ntn { get; set; }
            public string cnic { get; set; }
            public List<string> authorizedPaymentModesForImport { get; set; }
            public List<string> authorizedPaymentModesForExport { get; set; }

        }
        public class MessageData
        {
            public string code { get; set; }
            public string description { get; set; }

        }
    
    }
}
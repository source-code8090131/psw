﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWWebApi.Models
{
    public class ShareGDNFinancialinfoWithADNegNull
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string processingCode { get; set; }
        public DetailData data { get; set; }
        public string signature { get; set; }
        public class DetailData
        {
            public string gdNumber { get; set; }
            public string gdStatus { get; set; }
            public string consignmentCategory { get; set; }
            public string gdType { get; set; }
            public string collectorate { get; set; }
            public string blAwbNumber { get; set; }
            public string blAwbDate { get; set; }
            public string virAirNumber { get; set; }
            public DetalConsignorConsigneeInfo consignorConsigneeInfo { get; set; }
            public DetalFinancialInfo financialInfo { get; set; }
            public DetailGeneralInformation generalInformation { get; set; }
            public List<DetailItemInformation> itemInformation { get; set; }
            public DetailNegativeList negativeList { get; set; }
        }
        public class DetalConsignorConsigneeInfo
        {
            public string ntnFtn { get; set; }
            public string strn { get; set; }
            public string consigneeName { get; set; }
            public string consigneeAddress { get; set; }
            public string consignorName { get; set; }
            public string consignorAddress { get; set; }
        }
        public class DetalFinancialInfo
        {
            public string importerIban { get; set; }
            public string modeOfPayment { get; set; }
            public string finInsUniqueNumber { get; set; }
            public string currency { get; set; }
            public string invoiceNumber { get; set; }
            public string invoiceDate { get; set; }
            public decimal totalDeclaredValue { get; set; }
            public string deliveryTerm { get; set; }
            public decimal fobValueUsd { get; set; }
            public decimal freightUsd { get; set; }
            public decimal cfrValueUsd { get; set; }
            public decimal insuranceUsd { get; set; }
            public decimal landingChargesUsd { get; set; }
            public decimal assessedValueUsd { get; set; }
            public decimal otherCharges { get; set; }
            public decimal exchangeRate { get; set; }
        }
        public class DetailGeneralInformation
        {
            public List<DetailPackagesInformation> packagesInformation { get; set; }
            public List<DetailContainerVehicleInformation> containerVehicleInformation { get; set; }
            public string netWeight { get; set; }
            public string grossWeight { get; set; }
            public string portOfShipment { get; set; }
            public string portOfDelivery { get; set; }
            public string portOfDischarge { get; set; }
            public string terminalLocation { get; set; }
        }
        public class DetailPackagesInformation
        {
            public Decimal numberOfPackages { get; set; }
            public string packageType { get; set; }
        }
        public class DetailContainerVehicleInformation
        {
            public string containerOrTruckNumber { get; set; }
            public string sealNumber { get; set; }
            public string containerType { get; set; }
        }
        public class DetailItemInformation
        {
            public string hsCode { get; set; }
            public decimal quantity { get; set; }
            public decimal unitPrice { get; set; }
            public decimal totalValue { get; set; }
            public decimal importValue { get; set; }
            public string uom { get; set; }
        }
        public class DetailNegativeList
        {
            //public string country { get; set; }
            //public string supplier { get; set; }
            //public List<string> commodities { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWWebApi.Models
{
    public class TestCustomResponseFromPSW
    {
        public string messageId { get; set; }
        public string timestamp { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string methodId { get; set; }
        public DetailData data { get; set; }
        public string signature { get; set; }
        public DetailMessage message { get; set; }
        public class DetailData
        {


        }
        public class DetailMessage
        {
            public string code { get; set; }
            public string description { get; set; }
        }
    }
}
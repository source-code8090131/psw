﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using PSWWebApi.Models;
using System.Security.Claims;
using Microsoft.Owin.Security;

namespace PSWWebApi.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.TryGetFormCredentials(out var clientId, out var clientSecret);
            using (PSWEntities con = new PSWEntities())
            {
                PSW_API_AUTH auth = con.PSW_API_AUTH.Where(m => m.PAA_USERNAME == clientId && m.PAA_PASSWORD == clientSecret).FirstOrDefault();
                if (auth != null)
                {
                    context.Validated(clientId);
                }
            }
            return base.ValidateClientAuthentication(context);
        }

        public override Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var oAuthIdentity = new ClaimsIdentity(context.Options.AuthenticationType);
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, "TestClient"));
            var ticket = new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties());
            context.Validated(ticket);
            return base.GrantClientCredentials(context);

        }

    }
}
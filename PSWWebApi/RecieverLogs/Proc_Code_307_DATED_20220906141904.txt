{
               "messageId": "ae551adc-c7a5-470d-9f58-61a2b681aae1",
               "timestamp": "20220804100042",
               "senderId": "PSW",
               "receiverId": "CBN",
               "processingCode": "307",
               "data": {
                              "cobUniqueIdNumber": "PSW-COB-800215-03082022",
                              "tradeTranType": "01",
                              "iban": "PK31CITI1000000100386011",
                              "financialInstrumentInfo": {
                                             "importerNtn": "0710501",
                                             "importerName": "GLAXO SMITHKLINE PAKISTAN LIMITED",
                                             "importerIban": "PK02SCBL0000001106308102",
                                             "modeOfPayment": "302",
                                             "finInsUniqueNumber": "SCB-IMP-010895-02082022",
                                             "openAccountData": {
                                                            "gdNumber": "KAPW-HC-943-04-07-2022"
                                             },
                                             "paymentInformation": {
                                                            "beneficiaryName": "INTERNATIONAL FLAVOR AND FRAGNANCES I.F.F (NEDERLAND) B.V.",
                                                            "beneficiaryAddress": "ZEVENHEUVELENWEG 60., P.0 BOX 5021, 5004 EA, TILBURG, THE NETHERLANDS",
                                                            "beneficiaryCountry": "NLD",
                                                            "beneficiaryIban": null,
                                                            "exporterName": "INTERNATIONAL FLAVOR AND FRAGNANCES I.F.F (NEDERLAND) B.V.",
                                                            "exporterAddress": "ZEVENHEUVELENWEG 60., P.0 BOX 5021, 5004 EA, TILBURG, THE NETHERLANDS",
                                                            "exporterCountry": "NLD",
                                                            "portOfShipment": "S23165",
                                                            "deliveryTerms": "CFR",
                                                            "financialInstrumentValue": 47639.413,
                                                            "remainingInvoiceValue": 0.0,
                                                            "financialInstrumentCurrency": "EUR",
                                                            "exchangeRate": 239.3744,
                                                            "lcContractNo": "415030856724-A"
                                             },
                                             "itemInformation": [
                                                            {
                                                                           "hsCode": "3302.1090",
                                                                           "goodsDescription": "STRAWVERRY FLAVOUR",
                                                                           "quantity": 2010.0,
                                                                           "uom": "KG",
                                                                           "countryOfOrigin": "NLD",
                                                                           "sample": "N",
                                                                           "sampleValue": 0.0
                                                            },
                                                            {
                                                                           "hsCode": "3302.1090",
                                                                           "goodsDescription": "ORANGE BLOOD",
                                                                           "quantity": 400.0,
                                                                           "uom": "KG",
                                                                           "countryOfOrigin": "NLD",
                                                                           "sample": "N",
                                                                           "sampleValue": 0.0
                                                            }
                                             ],
                                             "financialTranInformation": {
                                                            "intendedPayDate": "20220930",
                                                            "transportDocDate": "20220518",
                                                            "finalDateOfShipment": "20220930",
                                                            "expiryDate": "20220930"
                                             },
                                             "remarks": "OPEN ACCOUNT"
                              },
                              "bankAdviceInfo": [],
                              "gdInfo": [
                                             {
                                                            "gdNumber": "KAPW-HC-943-04-07-2022",
                                                            "gdStatus": "05",
                                                            "consignmentCategory": "Commercial",
                                                            "gdType": "Home Consumption",
                                                            "collectorate": "MCC Appraisement Karachi West - Import ",
                                                            "blAwbNumber": "RTMA55717",
                                                            "blAwbDate": "20220520",
                                                            "virAirNumber": "KPPI-0299-18062022",
                                                            "clearanceDate": "20220705",
                                                            "consignorConsigneeInfo": {
                                                                           "ntnFtn": "0710501",
                                                                           "strn": "0211170200128",
                                                                           "consigneeName": "GLAXO SMITHKLINE PAKISTAN LIMITED",
                                                                           "consigneeAddress": "35-SYKES BUILDING WEST WHARF DOCKYARD ROAD  ",
                                                                           "consignorName": "I.F.F. (NEDERLAND) BV",
                                                                           "consignorAddress": "ZEVENHEUVELENWEG 605048 AN TILBURGNETHERLANDS"
                                                            },
                                                            "financialInfo": {
                                                                           "importerIban": "PK02SCBL0000001106308102",
                                                                           "modeOfPayment": "302",
                                                                           "finInsUniqueNumber": "SCB-IMP-010895-02082022",
                                                                           "currency": "EUR",
                                                                           "invoiceNumber": "9600785567",
                                                                           "invoiceDate": null,
                                                                           "totalDeclaredValue": 47639.413,
                                                                           "deliveryTerm": "CFR",
                                                                           "fobValueUsd": 0.0,
                                                                           "freightUsd": 0.0,
                                                                           "cfrValueUsd": 47639.413,
                                                                           "insuranceUsd": 0.0,
                                                                           "landingChargesUsd": 1.0,
                                                                           "assessedValueUsd": 47639.413,
                                                                           "OtherCharges": 0.0,
                                                                           "exchangeRate": 215.96
                                                            },
                                                            "generalInformation": {
                                                                           "packagesInformation": [
                                                                                          {
                                                                                                         "numberOfPackages": 8.0,
                                                                                                         "packageType": "PALLET              "
                                                                                          }
                                                                           ],
                                                                           "containerVehicleInformation": [
                                                                                          {
                                                                                                         "containerOrTruckNumber": "DFSU2246656",
                                                                                                         "sealNumber": "DHL0002287",
                                                                                                         "containerType": ""
                                                                                          }
                                                                           ],
                                                                           "netWeight": "2.77310 MT",
                                                                           "grossWeight": "2.77310 MT",
                                                                           "portOfShipment": "S1651",
                                                                           "portOfDelivery": null,
                                                                           "portOfDischarge": null,
                                                                           "terminalLocation": "Burma Oil"
                                                            },
                                                            "itemInformation": [
                                                                           {
                                                                                          "hsCode": "3302.1090",
                                                                                          "quantity": 2010.0,
                                                                                          "unitPrice": 19.1813,
                                                                                          "totalValue": 38554.413,
                                                                                          "importValue": 8409473.0,
                                                                                          "uom": "KG"
                                                                           },
                                                                           {
                                                                                          "hsCode": "3302.1090",
                                                                                          "quantity": 400.0,
                                                                                          "unitPrice": 22.7125,
                                                                                          "totalValue": 9085.0,
                                                                                          "importValue": 1981617.0,
                                                                                          "uom": "KG"
                                                                           }
                                                            ]
                                             }
                              ]
               },
               "signature": "glZluUXWv7n2jM7mDBTt+TgREyHPtSlrVAZtNPtpZJo="
}
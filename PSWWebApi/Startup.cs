﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using PSWWebApi.Providers;
using System.Configuration;

namespace PSWWebApi
{
    public class Startup
    {

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            ConfigureOAuth(app);
            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            int TimeOut = 3600;
            string KeyValue = System.Configuration.ConfigurationManager.AppSettings["TokenExpiryInSeconds"].ToString();
            bool ISCast = int.TryParse(KeyValue, out TimeOut);
            if (!ISCast)
                TimeOut = 300;
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/citi/connect/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromSeconds(TimeOut),
                Provider = new SimpleAuthorizationServerProvider()
            };
            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions(

                ));

        }

    }
}